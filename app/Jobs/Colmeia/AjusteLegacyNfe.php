<?php

namespace App\Jobs\Colmeia;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Models\Colmeia\Nfe;
use App\Models\Colmeia\Produto;
use App\Models\Colmeia\Legacy\Nfe as LegacyNfe;
use App\Models\Colmeia\Legacy\Detalhe as LegacyDetalhe;

class AjusteLegacyNfe implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	public $deleteWhenMissingModels = true;
	public $tries = 5;

	protected $nfe;

	private $chunk = 500;
	
	public function __construct (LegacyNfe $nfe)
	{
		$this->nfe = $nfe;
	}

	public function handle ()
	{
		\set_time_limit(0);

		\ini_set('max_execution_time', 0);
		\ini_set('memory_limit', '2G');

		// dd($this->nfe);

		$this->nfe->loadMissing('detalhes');
		$this->nfe->loadMissing('detalhes.produtos');

		$currentNfe = Nfe::find($this->nfe->ide_nNF);

		$queries = [];

		foreach ($this->nfe->detalhes as $detalhe)
		{
			foreach ($currentNfe->detalhes as $index => $currentDetalhe)
			{
				if ($detalhe->det_prod_cEAN != @$currentDetalhe->prod->cEAN)
					continue;

				$iccids = $detalhe->produtos->pluck('ID_CD');
				
				$queries[] = [\App\Helpers\QueryBuilder::ToSqlWithBindings(Produto
						::where('nfe', $currentNfe->id)
						->whereIn('serial', $iccids)
						// ->update(['nfe_detalhe'=>$index])
				), ['nfe_detalhe'=>$index]];

				// Produto
				// 	::where('nfe', $currentNfe->id)
				// 	->whereIn('serial', $iccids)
				// 	->update(['nfe_detalhe'=>$index])
				// ;
			}
		}

		dd($queries);
	}

	public function failed (Exception $exception)
	{
		\Log::error('Erro ao Ajustar a Nota : ' . $this->nfe->Codigo . PHP_EOL . $exception->getMessage());
	}
}