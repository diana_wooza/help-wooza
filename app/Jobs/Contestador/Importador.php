<?php

namespace App\Jobs\Contestador;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;
use App\Models\Contestador\Processo;

class Importador implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	public $deleteWhenMissingModels = true;
	public $tries = 5;
	public $timeout = 7200;
	
	protected $processo;

	private $chunk = 500;
	private $csvSeparator = ';';

	//php artisan queue:work contestador --queue=general --timeout=3600
	public function __construct (Processo $processo)
	{
		$this->processo = $processo;
	}

	public function handle ()
	{
		if ($this->processo->tipo != Processo::TIPO_UPLOAD)
			throw new \Exception('Processo incorreto enviado para o tratamento de uploads');

		ini_set('memory_limit', '5G');
		ini_set('max_execution_time', $this->timeout);
		set_time_limit($this->timeout);

		$this->processo->loadMissing('arquivo','competencia','usuario');

		$this->processo->resetResultados();
		$this->processo->setIniciado();

		$time_start = \microtime(true);
		$marcacaoAutomatica = json_encode(['score'=>null, 'regras'=>[['tipo'=>'automatica', 'configuracoes'=>['processo' => $this->processo->id]]]]);

		if ($this->processo->configuracao->estrategia == 'sobreescrever')
		{
			Receita
				::PorCompetencia($this->processo->competencia)
				->PorArquivo($this->processo->arquivo)
				->delete()
			;

			$this->processo->addResultado('Registros anteriores excluídos');
		}
		

		$file = \fopen(Storage::disk('contestador_arquivos')->path($this->processo->configuracao->caminho), 'r');

		$headers = \fgetcsv($file, 0, $this->csvSeparator);
		$headers = \array_map('\App\Helpers\Utils::CleanupDataEncoding', $headers);

		$chunkToInsert = [];
		$registros = 0;
		$linhasRejeitadas = 0;

		while (($line = \fgetcsv($file, 0, $this->csvSeparator)) !== FALSE)
		{
			try
			{
				if ($this->isEmptyLine($line))
					throw new RejectLine("Linha vazia", 1);

				$dados = \array_combine($headers, \array_map('\App\Helpers\Utils::CleanupDataEncoding', $line));
				$dados['_colunas_originais'] = \array_combine($headers, $headers);

				foreach ($this->processo->arquivo->tratamentos as $tratamento)
					$dados = $tratamento->class::Executar($dados, $tratamento->configuracao);
			}
			catch (RejectLine $line)
			{
				$linhasRejeitadas++;
				continue;
			}
			catch (RejectFile $line)
			{
				Receita::PorProcesso($this->processo)->delete();

				$this->processo->setConcluido();
				$this->processo->addResultado($line->getMessage());
				$this->processo->save();
				return;
			}

			$pedido_id = null;
			$marcacao = null;

			if (isset($dados['_pedido']) && !empty($dados['_pedido']))
			{
				$pedido_id = DB::raw('(SELECT id FROM pedidos WHERE increment_id = \'' . $dados['_pedido'] . '\')');
				$marcacao = $marcacaoAutomatica;
			}

			unset($dados['']);

			$chunkToInsert[] = [
				'processo_id'		=> $this->processo->id,
				'pedido_id'			=> $pedido_id,
				'arquivo_id'		=> $this->processo->arquivo_id,
				'operadora_id'		=> $this->processo->arquivo->operadora_id,
				'competencia_id'	=> $this->processo->competencia_id,
				'marcacao'			=> $marcacao,
				'dados'				=> \json_encode($dados),
			];

			if (\count($chunkToInsert) >= $this->chunk)
			{
				Receita::insert($chunkToInsert);
				$chunkToInsert = [];
			}

			$registros++;
		}

		#Insere os arquivos finais, caso o looping tenha finalizado antes dos 500
		if (\count($chunkToInsert) > 0)
			Receita::insert($chunkToInsert);
		
		$time_end = \microtime(true);

		$resultado = [
			'mensagem'	=> ['%s registros analisados'],
			'variaveis'	=> [\number_format($registros, 0, ',', '.')],
		];

		if ($linhasRejeitadas > 0)
		{
			$resultado['mensagem'][] = '%s receitas não foram inseridas por não estarem preenchidas ou rejeitadas por tratamentos';
			$resultado['variaveis'][] = \number_format($linhasRejeitadas, 0, ',', '.');

		}

		if ($this->processo->configuracao->marcar && !$this->processo->is_status_concluido)
		{
			foreach ($this->processo->arquivo->regras as $regra)
			{
				$regra = Regra::findOrFail($regra);

				$marcacao = new Processo();

				$marcacao->arquivo()->associate($this->processo->arquivo);
				$marcacao->competencia()->associate($this->processo->competencia);
				$marcacao->usuario()->associate($this->processo->usuario);
				$marcacao->tipo = Processo::TIPO_MARCACAO;
				$marcacao->configuracao = (object) [
					'regra'		=> $regra->id,
					'exportar'	=> @$this->processo->configuracao->exportar == true,
				];
				
				$marcacao->save();
				$marcacao->Dispatch();

				$resultado['mensagem'][] = 'Um processo de marcação usando a regra ' . $regra->nome . ' foi colocado na fila';
			}
		}

		$this->processo->addResultado(implode('<br />', $resultado['mensagem']), ...$resultado['variaveis']);
		$this->processo->setConcluido();
		$this->processo->addResultado('Processo concluído em ' . number_format(($time_end - $time_start), 4, ',', '') . ' segundo(s)');
		$this->processo->save();
	}

	private function isEmptyLine ($line)
	{
		foreach ($line as $value)
		{
			if (!empty($value))
				return false;
		}

		return true;
	}

	public function failed ($exception)
	{
		Receita::PorProcesso($this->processo)->delete();

		$this->processo->setConcluido();
		$this->processo->addResultado($exception->getMessage());
		$this->processo->save();
	}
}