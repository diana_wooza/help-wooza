<?php

namespace App\Jobs\Contestador;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;
use App\Models\Contestador\Processo;
use App\Mail\Contestador\DownloadCompleto as MyMail;

class Exportador implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	public $deleteWhenMissingModels = true;
	public $tries = 5;
	public $timeout = 7200;
	
	protected $processo;

	private $chunk = 5000;
	private $separator = ';';
	private $filename = null;

	//php artisan queue:work contestador --queue=general --timeout=3600
	public function __construct (Processo $processo)
	{
		$this->processo = $processo;
		$this->filename = \uniqid('export_') . '.csv';
	}

	public function handle ()
	{
		if ($this->processo->tipo != Processo::TIPO_DOWNLOAD)
			throw new \Exception('Processo incorreto enviado para o tratamento de uploads');

		ini_set('memory_limit', '5G');
		ini_set('max_execution_time', $this->timeout);
		set_time_limit($this->timeout);

		$this->processo->loadMissing('arquivo','competencia','usuario');

		$this->processo->resetResultados();
		$this->processo->setIniciado();

		$time_start = \microtime(true);
		$current = 0;
		$total = Receita
			::PorCompetencia($this->processo->competencia)
			->PorArquivo($this->processo->arquivo)
			->count()
		;

		$file = \fopen(Storage::disk('contestador_arquivos')->path($this->filename), 'w');

		\fputcsv($file, Receita::PorCompetencia($this->processo->competencia)->PorArquivo($this->processo->arquivo)->take(1)->first()->GetExportedTitle(), $this->separator);

		do
		{
			$receitas = Receita
				::with('pedido', 'pedido.pedido')
				->PorCompetencia($this->processo->competencia)
				->PorArquivo($this->processo->arquivo)
				->take($this->chunk)
				->skip($current)
				->get()
			;

			foreach ($receitas as $receita)
				\fputcsv($file, $receita->GetExportedData(), $this->separator);
			
			$current += $this->chunk;
		}
		while ($current <= $total);

		$this->processo->setConfiguracao('arquivo', $this->filename);

		\fclose($file);
		$time_end = \microtime(true);

		$this->processo->setConcluido();
		$this->processo->addResultado('%s receitas exportadas em arquivo<br />Processo concluído em %s segundo(s)',
			\number_format($total, 0, ',', '.'),
			\number_format(($time_end - $time_start), 4, ',', '')
		);
		$this->processo->save();
		$this->processo->fresh();

		Mail
			::to($this->processo->usuario->email)
			->send(new MyMail($this->processo))
		;
	}

	public function failed ($exception)
	{
		Storage::disk('contestador_arquivos')->delete($this->filename);

		$this->processo->setConcluido();
		$this->processo->addResultado($exception->getMessage());
		$this->processo->save();
	}
}