<?php

namespace App\Jobs\Contestador;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Helpers\Utils;
use App\Models\Contestador\Processo;
use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;

class Marcador implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	public $deleteWhenMissingModels = true;
	public $tries = 5;
	public $timeout = 18000;

	protected $processo;

	private $chunk = 1000;

	//php artisan queue:work contestador --queue=general --timeout=3600
	public function __construct(Processo $processo)
	{
		$this->processo = $processo;
	}

	public function handle ()
	{
		if ($this->processo->tipo != Processo::TIPO_MARCACAO)
			throw new \Exception('Processo incorreto enviado para o tratamento de marcação');

		if (is_null($regra =  Regra::find($this->processo->configuracao->regra)))
			throw new \Exception('Regra inválida [ID: ' . $this->processo->configuracao->regra . ']');

		ini_set('memory_limit', '5G');
		ini_set('max_execution_time', $this->timeout);
		set_time_limit($this->timeout);

		$time_start = \microtime(true);

		$this->processo->loadMissing('competencia','arquivo','arquivo.operadora','usuario');
		$this->processo->setIniciado();

		$operadora = $this->processo->arquivo->operadora;
		$receitas = Receita
			::PorCompetencia($this->processo->competencia)
			->PorArquivo($this->processo->arquivo)
			->ApenasDesmarcados()
			->get()
		;

		$resultado = $regra->ExecutarQuery($receitas, $operadora);

		$queryParserPedidos = function ($pedido) use ($regra) {
			return '('.implode(',', [
				Utils::SqlNullString($pedido->pedido_id),
				Utils::SqlNullString($pedido->increment_id),
				Utils::SqlNullString($pedido->activated_user),
				Utils::SqlNullString($pedido->plan_price),
				Utils::SqlNullString($regra->pedido_type),
			]).')';
		};

		$queryParserReceitas = function ($receita) {
			
			$select  = 'SELECT ';
			$select .= Utils::SqlNullString($receita->receita) . ' AS id, ';
			$select .= Utils::SqlNullString($receita->pedido) . ' AS pedido_id, ';
			$select .= Utils::SqlNullString(addslashes(json_encode($receita->comparacao))) . ' AS marcacao';

			return $select;
		};

		foreach ($resultado->pedidos->chunk($this->chunk) as $chunk)
		{
			$sql = 
			
				"INSERT INTO
					pedidos
					(
						pedido_id,
						increment_id,
						usuario_ativacao_id,
						valor,
						pedido_type
					)
				VALUES
					{$chunk->map($queryParserPedidos)->implode(',' . PHP_EOL)}
				ON DUPLICATE KEY UPDATE
					pedido_id = VALUES(pedido_id),
					increment_id = VALUES(increment_id),
					usuario_ativacao_id = VALUES(usuario_ativacao_id),
					valor = VALUES(valor),
					pedido_type = VALUES(pedido_type)
				"
			;

			Log::channel('contestador')->warning($sql);
			DB::connection('contestador')->insert($sql);

		}

		foreach ($resultado->receitas->chunk($this->chunk) as $chunk)
		{
			$sql =
			
				"UPDATE
					receitas
					JOIN (
						{$chunk->map($queryParserReceitas)->implode(' UNION' . PHP_EOL)}
					) AS marcadas ON marcadas.id = receitas.id
					JOIN pedidos ON
						pedidos.pedido_id = marcadas.pedido_id
				SET
					receitas.pedido_id = pedidos.id,
					receitas.marcacao = marcadas.marcacao
				WHERE
					receitas.id != 0"

			;

			Log::channel('contestador')->warning($sql);
			DB::connection('contestador')->insert($sql);
		}

		$time_end = \microtime(true);

		$this->processo->addResultado('<strong>Processadas</strong>: %s, <strong>Fora do período válido</strong>: %s, <strong>Marcadas</strong>: %s (%s).<br />Processo concluído em %s segundo(s).',
			$resultado->estatisticas->GetTotalFormatado(),
			$resultado->estatisticas->GetExpiradoFormatado(),
			$resultado->estatisticas->GetMarcadosFormatado(),
			$resultado->estatisticas->GetPercentualMarcado(),
			number_format(($time_end - $time_start), 4, ',', '')
		);

		if (@$this->processo->configuracao->exportar && !$this->processo->is_status_concluido)
		{
			$processo = new Processo();
			
			$processo->arquivo()->associate($this->processo->arquivo);
			$processo->competencia()->associate($this->processo->competencia);
			$processo->usuario()->associate($this->processo->usuario);
			
			$processo->tipo = Processo::TIPO_DOWNLOAD;
			$processo->setConfiguracao('arquivo', null);
			
			$processo->save();
			$processo->Dispatch();
		}

		$this->processo->setConcluido();
		$this->processo->save();
	}

	public function failed (\Exception $exception)
	{
		$this->processo->setConcluido();
		$this->processo->addResultado($exception->getMessage());
		$this->processo->save();
	}
}