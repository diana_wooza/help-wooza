<?php

namespace App\Jobs\Contestador;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Models\Contestador\Competencia;
use App\Models\Contestador\Pedido;
use App\Models\Magento\Consumer\Order;
use App\Models\Operadora;

class BuscarPedidosCompetenciaConsumer implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	public $deleteWhenMissingModels = true;
	public $tries = 5;
	public $timeout = 7200;

	protected $competencia;

	private $chunk = 500;
	
	public function __construct (Competencia $competencia)
	{
		$this->competencia = $competencia;
	}

	public function handle ()
	{
		ini_set('memory_limit', '5G');
		ini_set('max_execution_time', $this->timeout);
		set_time_limit($this->timeout);

		$total = Order::PorCompetencia($this->competencia)->count();
		$current = 0;

		while ($current - $this->chunk < $total)
		{
			$orders = Order::with('admin_base')->PorCompetencia($this->competencia)->skip(max($current, 0))->take($this->chunk)->get();
			$current += $this->chunk;

			if ($orders->count() <= 0)
				return;

			$orders = $orders->map(function ($order) {

				return '(' . implode(',', array_map('\App\Helpers\Utils::SqlNullString', [
					$this->competencia->id,
					Pedido::GetSegmentoPedidoConsumer($order),
					$order->entity_id,
					\addslashes(Order::class),
					$order->increment_id,
					@Operadora::GetOperadoraPedidoMagentoConsumer($order)->id,
					$order->activated_user,
					$order->plan_price,
				])) . ')';

			})->implode(',');

			DB::connection('contestador')->insert(
				"INSERT INTO
					pedidos
					(
						competencia_id,
						segmento,
						pedido_id,
						pedido_type,
						increment_id,
						operadora_id,
						usuario_ativacao_id,
						valor
					)
				VALUES
					{$orders}
				ON DUPLICATE KEY UPDATE
					competencia_id = VALUES(competencia_id),
					segmento = VALUES(segmento),
					pedido_id = VALUES(pedido_id),
					pedido_type = VALUES(pedido_type),
					increment_id = VALUES(increment_id),
					operadora_id = VALUES(operadora_id),
					usuario_ativacao_id = VALUES(usuario_ativacao_id),
					valor = VALUES(valor)"
			);
		}
	}

	public function failed (Exception $exception)
	{
		\Log::error('Erro ao Buscar Pedidos Consumer: ' . $exception->getMessage());
	}
}