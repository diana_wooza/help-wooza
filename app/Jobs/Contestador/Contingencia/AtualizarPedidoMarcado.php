<?php

namespace App\Jobs\Contestador;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

use App\Models\Contestador\Obtencao;
use App\Models\Contestador\Marcacao;
use App\Models\Contestador\Pedido;
use App\Models\Contestador\Upload;
use App\Models\Magento\Order;
use Maatwebsite\Excel\Facades\Excel;

class AtualizarPedidoMarcado implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $upload;

	private $chunk = 500;
	
	public function __construct (Upload $upload)
	{
		$this->upload = $upload;
	}

	public function handle ()
	{
		\set_time_limit(0);

		\ini_set('max_execution_time', 0);
		\ini_set('memory_limit', '5G');

		\Config::set('excel.csv.delimiter', ';');
		// \Config::set('excel.import.heading', 'numeric');
		\Config::set('import.dates.enabled', false);

		$dados = Excel::selectSheetsByIndex(0)->load($this->upload->arquivo_path, function($reader) {

			$reader->formatDates(false, 'd/m/Y');
		
		}, 'UTF-8');

		dd($dados);
	}

	public function failed (Exception $exception)
	{
		
	}
}