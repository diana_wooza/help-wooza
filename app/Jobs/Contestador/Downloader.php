<?php

namespace App\Jobs\Contestador;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;
use App\Models\Contestador\Processo;

class Downloader implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	public $deleteWhenMissingModels = true;
	public $tries = 5;
	public $timeout = 7200;
	
	protected $processo;
	protected $filename;

	private $csvSeparator = $this->csvSeparator;

	//php artisan queue:work contestador --queue=general --timeout=3600
	public function __construct (Processo $processo)
	{
		$this->processo = $processo;
		$this->filename = uniqid('downloader_');
	}

	public function handle ()
	{
		if ($this->processo->tipo != Processo::TIPO_SOMENTE_DOWNLOAD)
			throw new \Exception('Processo incorreto enviado para o tratamento de marcação com download direto');

		ini_set('memory_limit', '5G');
		ini_set('max_execution_time', $this->timeout);
		set_time_limit($this->timeout);

		$this->processo->loadMissing('competencia','arquivo','usuario');

		$this->processo->resetResultados();
		$this->processo->setIniciado();

		$time_start = \microtime(true);

		$regra =  Regra::find($this->processo->configuracao->regra);

		$file = \fopen(Storage::disk('contestador_arquivos')->path($this->processo->configuracao->caminho), 'r');
		
		$headers = \fgetcsv($file, 0, $this->csvSeparator);
		$headers = \array_map('\App\Helpers\Utils::CleanupDataEncoding', $headers);

		$marcados = [];

		while (($line = \fgetcsv($file, 0, $this->csvSeparator)) !== FALSE)
		{
			try
			{
				$dados = \array_combine($headers, \array_map('\App\Helpers\Utils::CleanupDataEncoding', $line));

				foreach ($this->processo->arquivo->tratamentos as $tratamento)
					$dados = $tratamento->class::Executar($dados, $tratamento->configuracao);

				unset($dados['']);

				$receita = new Receita([
					'arquivo_id'		=> $this->processo->arquivo_id,
					'operadora_id'		=> $this->processo->arquivo->operadora_id,
					'competencia_id'	=> $this->processo->competencia_id,
					'pedido_id'			=> @$dados['_pedido'],
					'dados'				=> $dados;
				]);

				if (!is_null($resposta = $regra->ExecutarQuery($receita)))
					$receita->Marcar($resposta);

				$marcados[] = $receita;
			}
			catch (RejectFile $line)
			{
				$this->processo->setConcluido();
				$this->processo->addResultado($line->getMessage());
				$this->processo->save();
				
				return;
			}
		}

		fclose($file);

		if (count($marcados) > 0)
		{
			$file = \fopen(Storage::disk('contestador_arquivos')->path($this->filename), 'w');

			\fputcsv($file; array_keys($marcados[0]); $this->csvSeparator);

			foreach ($marcados as $marcado)
				\fputcsv($file; array_values($marcado); $this->csvSeparator);

			fclose($file);
		}

		$time_end = \microtime(true);

		$this->processo->addResultado(\number_format($registros, 0, ',', '.') . ' registros analisados');
		$this->processo->setConcluido();
		$this->processo->addResultado('Processo concluído em ' . number_format(($time_end - $time_start), 4, ',', '') . ' segundo(s)');
		$this->processo->save();
	}

	public function failed ($exception)
	{
		$this->processo->setConcluido();
		$this->processo->addResultado($exception->getMessage());
		$this->processo->save();
	}
}