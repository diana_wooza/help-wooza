<?php

namespace App\Jobs\Filas;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FilasMagento implements ShouldQueue {
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	private $pedido;
	private $fila;
	private $rotas = [
		'geral.primeira_etapa_fluxo' => '/api/v1/filas/primeira_etapa_fluxo'
	];

	public function __construct($request) {
		$this->pedido = $request->pedido;
		$this->fila = $request->fila;
	}

	public function handle() {
		$ch = curl_init(substr(env('MAGENTO_API_HOST').$this->rotas[$this->fila], 0, -1));
		$header[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['pedido' => $this->pedido]));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

		curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if ($httpcode != 200) {
			throw new \Exception();
		}
	}

}