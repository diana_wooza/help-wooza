<?php

namespace App\Http\Middleware;
use Closure;

class CheckPermissao
{
	public function handle ($request, Closure $next, $permissoes)
	{
		try
		{
			if (!\Auth::check())
				throw new \Exception("Não logado", 1);

			$permissoes = explode('|', $permissoes);
			$temPermissao = false;

			foreach ($permissoes as $permissao)
			{
				if (\Auth::user()->checkPermissao($permissao))
					$temPermissao = true;
			}

			if (!$temPermissao)
				throw new \Exception("Não tem permissão", 1);
		}
		catch (\Exception $exception)
		{
			return redirect()->route('home');
		}
		
		return $next($request);
	}
}