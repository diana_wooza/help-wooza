<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
	/**
	* Indicates whether the XSRF-TOKEN cookie should be set on the response.
	*
	* @var bool
	*/
	protected $addHttpCookie = true;

	/**
	* The URIs that should be excluded from CSRF verification.
	*
	* @var array
	*/
	protected $except = [
		'alarmistica/api/listaApis',
		'alarmistica/api/selectFilho',
		'alarmistica/api/exibeIndicadores',
		'alarmistica/api/listaLogApisGrafico',
		'alarmistica/api/listaLogs',
		'alarmistica/api/selectFilhoApi',
		'alarmistica/api/exportarDadosLogs',
		'alarmistica/configuracao/selectFilho',
		'alarmistica/configuracao/selectFilhoApi',
		'alarmistica/configuracao/listaPessoasSms',
		'alarmistica/configuracao/alterar',
		'alarmistica/configuracao/getChannel',
		'alarmistica/configuracao/cadastrar',
		'indicadores/graficosindicadores/getDadosGraficoPrazoAtivacao',
		'indicadores/graficosindicadores/getDadosGraficoTaxaAtivacao',
		'indicadores/graficosindicadores/getDadosGraficoMeta',
		'indicadores/graficosindicadores/cadastrarMetas',
		'colmeia/api/*',
		'colmeia/produto/entrada/*/*',
		'configuracao/*',
		'posvenda/*',
		'cron/*'
	];
}
