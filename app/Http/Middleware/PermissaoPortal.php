<?php

namespace App\Http\Middleware;
use Closure;

class PermissaoPortal {
	public function handle ($request, Closure $next) {
		try {
			if (!\Auth::guard('portal')->check()) {		
				throw new \Exception("Não logado", 1);
			}

			if (\Auth::guard('portal')->user()->senha_valida == 0) {
				return redirect()->route('portal.alterar_senha_portal');
			}
		}
		catch (\Exception $exception) {
			return redirect()->route('portal.login_portal');
		}
		
		return $next($request);
	}
}