<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;

class Authenticate extends Middleware
{
	/**
	* Define o método que vai gerenciar o login de cada pessoa
	* @param  \Illuminate\Http\Request  $request
	* @param  \Illuminate\Http\Request  $next
	* @return string
	*/
	public function handle ($request, Closure $next, ...$guards)
	{
		$this->authenticate($request, $guards);

		if (is_null(\Auth::user()->telefone) && $request->path() != 'cadastro')
			return redirect()->route('hub.cadastro');

		return $next($request);
	}

	/**
	* Define o caminho que vai ser passado caso o usuário não tenha autenticado
	* @param  \Illuminate\Http\Request  $request
	* @return string
	*/
	protected function redirectTo ($request)
	{
		\Session::put('url_back', request()->path());
		return route('login');
	}
}
