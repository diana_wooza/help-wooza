<?php

namespace App\Http\Controllers\Configuracao;

use App\Http\Controllers\Controller;
use App\Models\Configuracao\Acoes;
use App\Models\Configuracao\AcoesTratativas;
use App\Models\Configuracao\CondicoesEspeciais;
use App\Models\Configuracao\Configuracao;
use App\Models\Configuracao\ConfiguracaoHistorico;
use App\Models\Configuracao\Labels;
use App\Models\Configuracao\Tratativas;
use App\Models\Posvenda\Acao;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ConfiguracaoController extends Controller {

    private $labels;
    private $condicoes;
    private $camposDadosAdicionais;

    public function __construct() {
        // define valores e labels para os campos que são exibidos nos selects
        $labels = \App\Models\Configuracao\Labels::get();

        foreach ($labels as $l) {
            $this->labels[] = ['value' => $l['valor'], 'text' => $l['label']];
        }

        // define os campos do pedido que são exibidos ao adicionar condicao
        $this->condicoes = \App\Models\Configuracao\Labels::where(['exibir_como_condicao' => 1])->pluck('label')->toArray();
        // define os dados adicioanais a serem exibidos
        $this->camposDadosAdicionais = \App\Models\Configuracao\Labels::where(['exibir_como_dado_adicional' => 1])->pluck('label')->toArray();
    }

    public function listaTratativas() {
		return view('_generico.tabela', [
			'titulo'			=> 'Lista de Tratativas automáticas',
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:auto; min-width:150px;', 'rotulo' => 'Tratativa', 'campo'=>'label'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo' => 'Editar', 'campo' => function($item) {
					$botoes = [];
					$botoes[] = '<a href="' . route('configuracao.editar_tratativa', ['id' => $item->id]).'" class="btn btn-primary fa fa-edit" title="Editar"></a>';
					return '<div class="btn-group">'.implode('', $botoes).'</div>';
				}],
			],
            'dados'	=> Tratativas::orderBy('label')->get(),
            'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Nova Tratativa', 'url' => route('configuracao.editar_tratativa')],
			]
		]);
    }

    public function listaAcoes() {
        return view('_generico.tabela', [
			'titulo'			=> 'Lista de Ações',
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:auto; min-width:150px;', 'rotulo' => 'Ações', 'campo'=>'label'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo' => 'Editar', 'campo' => function($item) {
					$botoes = [];
					$botoes[] = '<a href="' . route('configuracao.editar_acao', ['id' => $item->id]).'" class="btn btn-primary fa fa-edit" title="Editar"></a>';
					return '<div class="btn-group">'.implode('', $botoes).'</div>';
				}],
			],
            'dados'	=> Acoes::orderBy('label')->get(),
            'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Nova Ação', 'url' => route('configuracao.editar_acao')],
			]
		]);
    }

    public function listaCondicoesEspeciais() {
        return view('_generico.tabela', [
			'titulo'			=> 'Lista de Condições Especiais',
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:auto; min-width:150px;', 'rotulo' => 'Condições Especiais', 'campo'=>'label'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo' => 'Editar', 'campo' => function($item) {
					$botoes = [];
					$botoes[] = '<a href="' . route('configuracao.editar_condicao_especial', ['id' => $item->id]).'" class="btn btn-primary fa fa-edit" title="Editar"></a>';
					return '<div class="btn-group">'.implode('', $botoes).'</div>';
				}],
			],
            'dados'	=> CondicoesEspeciais::orderBy('label')->get(),
            'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Condição Especial', 'url' => route('configuracao.editar_condicao_especial')],
			]
		]);
    }

    public function listaCampos() {
        return view('_generico.tabela', [
			'titulo'			=> 'Lista de Campos',
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:auto; min-width:150px;', 'rotulo' => 'Campos', 'campo'=>'label'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo' => 'Editar', 'campo' => function($item) {
					$botoes = [];
					$botoes[] = '<a href="' . route('configuracao.editar_campo', ['id' => $item->id]).'" class="btn btn-primary fa fa-edit" title="Editar"></a>';
					return '<div class="btn-group">'.implode('', $botoes).'</div>';
				}],
			],
            'dados'	=> Labels::orderBy('label')->get(),
            'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Campo', 'url' => route('configuracao.editar_campo')],
			]
		]);
    }
    
    public function editarTratativa(Request $request, $id=null) {
        $salvo = null;
        $tratativa = Tratativas::find($id);
        if ($tratativa == '') {
            $tratativa = new Tratativas();
        }
        $acoes = Acoes::orderBy('label')->get();
        $acoes_tratativa = AcoesTratativas::where(['id_tratativas' => $tratativa->id])->pluck('id_acoes')->toArray();

        if ($request->isMethod('post')) {
            if ($request->submit == 'salvar') {
                $tratativa->nome = $request['nome'];
                $tratativa->label = $request['rotulo'];
                $tratativa->save();
                
                foreach ($acoes_tratativa as $at) {
                    if (is_null($request['acoes'])) {
                        AcoesTratativas::where(['id_tratativas' => $tratativa->id])->delete();
                    }
                    else {
                        if (is_array($request['acoes'])) {
                            if (!in_array($at, $request['acoes'])) {
                                AcoesTratativas::where(['id_tratativas' => $tratativa->id, 'id_acoes' => $at])->delete();
                            }
                        }
                    }
                }

                if (!is_null($request['acoes'])) {
                    foreach ($request['acoes'] as $acao) {
                        $acoes_tratativa = AcoesTratativas::where(['id_tratativas' => $tratativa->id, 'id_acoes' => $acao])->first();
                        if (!$acoes_tratativa) {
                            $acoes_tratativa = new AcoesTratativas();
                            $acoes_tratativa->id_tratativas = $tratativa->id;
                            $acoes_tratativa->id_acoes = $acao;
                            $acoes_tratativa->save();
                        }
                    }
                }

                $acoes_tratativa = AcoesTratativas::where(['id_tratativas' => $tratativa->id])->pluck('id_acoes')->toArray();
                $salvo = true;
            }

            if ($request->submit == 'excluir') {
                AcoesTratativas::where(['id_tratativas' => $tratativa->id])->delete();
                $tratativa->delete();
                
                return redirect()->route('configuracao.lista_tratativas');
            }
        }

		return view('configuracao/editar_tratativa', ['tratativa' => $tratativa, 'acoes' => $acoes, 'acoes_tratativa' => $acoes_tratativa, 'salvo' => $salvo]);
    }

    public function editarAcao(Request $request, $id=null) {
        $salvo = null;
        $acao = Acoes::find($id);
        if ($acao == '') {
            $acao = new Acoes();
        }
        
        if ($request->isMethod('post')) {
            if ($request->submit == 'salvar') {
                $acao->valor = $request['valor'];
                $acao->label = $request['rotulo'];
                $acao->descricao = $request['descricao'];
                $acao->dados_adicionais = $request['dados_adicionais'];
                $acao->save();
                $salvo = true;
            }

            if ($request->submit == 'excluir') {
                AcoesTratativas::where(['id_acoes' => $acao->id])->delete();
                $acao->delete();

                return redirect()->route('configuracao.lista_acoes');
            }
        }

		return view('configuracao/editar_acao', ['acao' => $acao, 'salvo' => $salvo]);
    }

    public function editarCondicaoEspecial(Request $request, $id=null) {
        $salvo = null;
        $condicao_especial = CondicoesEspeciais::find($id);
        if ($condicao_especial == '') {
            $condicao_especial = new CondicoesEspeciais();
        }
        
        $operadoras = $this->getValoresSelect('Operadora')->toArray();
        
        if ($request->isMethod('post')) {
            if ($request->submit == 'salvar') {
                $condicao_especial->valor = $request['valor'];
                $condicao_especial->label = $request['rotulo'];
                if ($request['operadora'] == '') {
                    $condicao_especial->operadora = null;
                }
                else {
                    $condicao_especial->operadora = mb_strtolower($request['operadora']);
                }
                $condicao_especial->save();
                $salvo = true;
            }

            if ($request->submit == 'excluir') {
                $condicao_especial->delete();

                return redirect()->route('configuracao.lista_condicoes_especiais');
            }
        }

		return view('configuracao/editar_condicao_especial', ['condicao_especial' => $condicao_especial, 'operadoras' => $operadoras, 'salvo' => $salvo]);
    }

    public function editarCampo(Request $request, $id=null) {
        $salvo = null;
        $campo = Labels::find($id);
        if ($campo == '') {
            $campo = new Labels();
        }
        
        if ($request->isMethod('post')) {
            if ($request->submit == 'salvar') {
                $campo->valor = $request['valor'];
                $campo->label = $request['rotulo'];
                $campo->exibir_como_condicao = ($request['exibir_como_condicao']) ? 1 : 0;
                $campo->exibir_como_dado_adicional = ($request['exibir_como_dado_adicional']) ? 1 : 0;
                $campo->exibir_como_comparativo = ($request['exibir_como_comparativo']) ? 1 : 0;
                $campo->save();
                $salvo = true;
            }

            if ($request->submit == 'excluir') {
                $campo->delete();

                return redirect()->route('configuracao.lista_campos');
            }
        }

		return view('configuracao/editar_campo', ['campo' => $campo, 'salvo' => $salvo]);
    }
	
	public function exibir() {
        $tratativas = Tratativas::OrderBy('label')->get();

		return view('configuracao/index', ['tratativas' => $tratativas]);
    }
    
    public function exibirHistorico() {
        $tratativas = Tratativas::OrderBy('label')->get();

		return view('configuracao/historico', ['tratativas' => $tratativas]);
    }
    
    public function datasHistorico($nome) {
        $versoes = ConfiguracaoHistorico::where('nome', $nome)->get();

        foreach ($versoes as $versao) {
            $versao->usuario = User::find($versao->usuario)->login;
        }

		return $versoes;
    }

    public function carregarVersaoHistorico($versao) {
		return ConfiguracaoHistorico::find($versao)['interface'];
	}

	private function buscarConfiguracaoMongo($nome, $tipo) {
		$resultado = Configuracao::where('nome', $nome)->first();
        
        return $resultado[$tipo];
	}
    
    public function getCamposCondicoes($operadora=null) {
        $campos = array();

        foreach ($this->condicoes as $campo) {
            $campos[] = array('value' => $campo, 'text' => $campo);
        }

        foreach ($this->getCondicoesEspeciais() as $campo) {
            $campos[] = array('value' => $campo, 'text' => $campo);
        }

        if ($operadora != null && $operadora != 'todas') {
            foreach ($this->getCondicoesEspeciais($operadora) as $campo) {
                $campos[] = array('value' => $campo, 'text' => $campo);
            }   
        }

        return $this->ordenar($campos);
    }
	
	public function getCamposDadosAdicionaisInterface() {
        $campos = array();

        foreach ($this->camposDadosAdicionais as $campo) {
            $campos[] = array('value' => $campo, 'text' => $campo);
        }

        return $this->ordenar($campos);
	}

    private function getAcoes($tratativa) {
        $acoes = [];
        $db = DB::connection('configuracao');

        $busca_acoes = $db->table('acoes')
                            ->select('acoes.valor','acoes.label')
                                ->join('acoes_tratativas', 'acoes_tratativas.id_acoes', '=', 'acoes.id')
                                ->join('tratativas', 'tratativas.id', '=', 'acoes_tratativas.id_tratativas')
                                    ->where('tratativas.nome', '=', $tratativa)
                                        ->get();

        foreach ($busca_acoes as $c) {
            $acoes[$c->valor] = $c->label;
        }

        return $acoes;
    }

	// private function getAcoes($operadora = null) {
    //     $acoes = [];

    //     if ($operadora == 'todas') {
    //         $operadora = null;
    //     }
        
    //     $busca_acoes = \App\Models\Configuracao\Acoes::where(['operadora' => $operadora])->get();
    //     foreach ($busca_acoes as $c) {
    //         $acoes[$c['valor']] = $c['label'];
    //     }

    //     return $acoes;
    // }

    public function getCamposComparativosOrder() {
        $camposComparativos = [];
        
        $campos = \App\Models\Configuracao\Labels::where(['exibir_como_comparativo' => 1])->get();
        foreach ($campos as $c) {
            $camposComparativos[] = array('value' =>$c['valor'], 'text' => $c['label']);
        }

        return $camposComparativos;
    }
	
	public function getAcoesInterface($tratativa) {
        $acoes = array();

        foreach ($this->getAcoes($tratativa) as $acao) {
            $acoes[] = array('value' => $acao, 'text' => $acao);
        }
        
        return $this->ordenar($acoes);
    }

    public function getDescricoesAcoes() {
        $acoes = [];
        $buscaAcoes = \App\Models\Configuracao\Acoes::get();

        foreach ($buscaAcoes as $acao) {
            $acoes[]  = [
                'acao' => $acao['label'],
                'descricao' => $acao['descricao'],
                'dados_adicionais' => $acao['dados_adicionais']
            ];
        }

        return $acoes;
    }

    private function getCondicoesEspeciais($operadora = null) {
        $condicoesEspeciais = [];
        
        $condicoes = \App\Models\Configuracao\CondicoesEspeciais::where(['operadora' => $operadora])->get();
        foreach ($condicoes as $c) {
            $condicoesEspeciais[$c['valor']] = $c['label'];
        }

        return $condicoesEspeciais;
    }

	public function getInterfaceConfiguracao($robo) {
		return $this->buscarConfiguracaoMongo($robo, 'interface');
	}
	
	public function getJsonRobo($robo) {
		return $this->buscarConfiguracaoMongo($robo, 'json_robo');
	}

	public function getCamposRobo($robo) {
        $array_robo = json_decode($this->buscarConfiguracaoMongo($robo, 'json_robo'), true);
        $this->camposRobo = array();
        $this->raizCamposRobo = '';
        foreach ($array_robo as $campo => $valor) {
            if (!is_array($valor)) {
                $this->camposRobo[] = array(
                    'value' => $campo,
                    'text' => $campo
                );
            }
            else {
                $this->raizCamposRobo = $campo;
                $this->getDadosRoboArray($valor);
            }
        }

        return $this->ordenar($this->camposRobo);
	}
	
	public function getUsuarioPadrao($robo) {
		return $this->buscarConfiguracaoMongo($robo, 'usuario_padrao');
	}
	
	public function getValoresSelect($campo) {
        $dados = array();
		$db = DB::connection('magento');
		
        switch ($campo) {
			case 'base_id':
			case 'Base':
				$dados = $db->table('admin_user_base')
				->select('id as value','nome as text')->get();
            break;
			case 'cod_cancelamento':
			case 'Código de Cancelamento':
				$dados = $db->table('cdireto_cod_processo')
				->select('nome as value',DB::raw("concat(nome,' ',motivo) as text"))->get();
            break;
			case 'store_name':
			case 'DDD':
				$dados = $db->table('core_store')
				->select('store_id as value','name as text')->get();
            break;
			case 'plano_forma_pagamento_id':
			case 'Forma de Pagamento':
				$dados = $db->table('sales_flat_order_plano_forma_pagamento')
				->select('id as value','nome as text')->get();
            break;
			case 'plan_modality':
			case 'Modalidade':
                $dados = array(
                    array('value' => 'Migração', 'text' => 'Migração'),
                    array('value' => 'Nova linha', 'text' => 'Nova linha'),
                    array('value' => 'Portabilidade', 'text' => 'Portabilidade'),
                    array('value' => 'Upgrade', 'text' => 'Upgrade'),
                    array('value' => 'SVA', 'text' => 'SVA')
                );
            break;
			case 'plan_operator':
			case 'Operadora':
				$dados = $db->table('eav_attribute')
				->select('value as value','value as text')
				->join('eav_attribute_option', 'eav_attribute.attribute_id', '=', 'eav_attribute_option.attribute_id')
				->join('eav_attribute_option_value', 'eav_attribute_option.option_id', '=', 'eav_attribute_option_value.option_id')
				->where('attribute_code',  'operator')
				->get();
            break;
			case 'status':
			case 'Status':
				$dados = $db->table('sales_order_status')
				->select('status as value','label as text')->get();
            break;
			case 'plan_tipo':
			case 'Tipo de Plano':
                $dados = array(
                    array('value' => 'Controle', 'text' => 'Controle'),
                    array('value' => 'Fixo', 'text' => 'Fixo'),
                    array('value' => 'Internet', 'text' => 'Internet'),
                    array('value' => 'Internet Fixa', 'text' => 'Internet Fixa'),
                    array('value' => 'Pós', 'text' => 'Pós'),
                    array('value' => 'Pré', 'text' => 'Pré'),
                    array('value' => 'TV', 'text' => 'TV'),
                    array('value' => 'SVA', 'text' => 'SVA'),
                    array('value' => 'Combo', 'text' => 'Combo')
                );
            break;
			case 'turno_instalacao':
			case 'Turno de Instalação':
                $dados = array(
                    array('value' => 'Manhã', 'text' => 'Manhã'),
                    array('value' => 'Tarde', 'text' => 'Tarde'),
                    array('value' => 'Integral', 'text' => 'Integral')
                );
            break;
			case 'activated_user':
			case 'Usuário de Ativação':
			case 'cancelamento_user':
			case 'Usuário de Cancelamento':
			case 'preactivated_user':
			case 'Usuário de Pré-ativação':
            case 'user_id':
                $dados = $db->table('admin_user')
                ->select('user_id as value',DB::raw("concat(firstname,' ',lastname) as text"))->where('robo', 1)->orderBy('firstname')->get();
            break;
            case 'seller_id':
			case 'Vendedor':
				$dados = $db->table('admin_user')
				->select('user_id as value',DB::raw("concat(firstname,' ',lastname) as text"))->orderBy('firstname')->get();
            break;
			case 'region':
			case 'UF':
				$dados = $db->table('directory_country_region')
				->select('code as value','code as text')
				->where('country_id', 'BR')
				->get();
            break;
			case 'isVarejo':
			case 'Pedido de varejo':
			case 'isPlanoExpress':
            case 'Plano express':
            case 'temSva':
            case 'Pedido tem SVA':
            case 'retornoCrivoPendente':
            case 'Retorno Crivo Pendente':
            case 'temDataAtivacao':
            case 'Pedido tem Data de Ativação':
            case 'pagamentoCartaoCredito':
            case 'Pagamento com Cartão de Crédito':
                $dados = array(
                    array('value' => 'true', 'text' => 'Sim'),
                    array('value' => 'false', 'text' => 'Não')
                );
            break;
			case 'getTipoMovimentacao':
			case 'Tipo de movimentação':
				$dados = $db->table('downgrade_tim')
				->select('tipo_movimentacao as value','tipo_movimentacao as text')
				->whereNotNull('tipo_movimentacao')
				->groupBy('tipo_movimentacao')
				->get();
            break;
			case 'motivo':
			case 'Motivo':
				$dados = $db->table('sales_order_motivo')
				->select('motivo as value','label as text')->get();
            break;
			case 'email':
			case 'E-mail':
				$dados = $db->table('core_email_template')
				->select('template_id as value','template_code as text')->get();
            break;
            case 'evento_comunicacao_esteira':
            case 'Evento de Comunicação da Esteira':
                $dados = array(
                    array('value' => 'agendamento_portabilidade', 'text' => 'Agendamento de Portabilidade')
                );
            break;
            case 'segmentoPedido':
            case 'Segmento do pedido':
                $dados = array(
                    array('value' => 'consumer', 'text' => 'Consumer'),
                    array('value' => 'varejo_online', 'text' => 'Varejo Online'),
                    array('value' => 'varejo_loja', 'text' => 'Varejo Loja')
                );
            break;
            case 'robo_ajuste':
            case 'Nome do Robô de Ajuste':
                $dados = array(
                    array('value' => 'adiciona_sva', 'text' => 'adiciona_sva'),
                    array('value' => 'adiciona_debito_automatico', 'text' => 'adiciona_debito_automatico')
                );
            break;
        }

        return $dados;
    }

    private function getDadosRoboArray($valor) {
        if (is_array($valor)) {
            foreach ($valor as $c => $v) {
                if (!is_array($v)) {
                    $this->camposRobo[] = array(
                        'value' => $this->raizCamposRobo.'|'.$c,
                        'text' => $this->raizCamposRobo.'|'.$c
                    );
                }
                else {
                    $raizAtual = $this->raizCamposRobo;
                    $this->raizCamposRobo .= '|'.$c;
                    $this->getDadosRoboArray($v);
                    $this->raizCamposRobo = $raizAtual;
                }
            }
        }
    }

    private function getValueByLabel($texto) {
        foreach ($this->labels as $label) {
            if ($label['text'] == $texto) {
                return $label['value'];
            }
        }

        return $texto;
    }

    private function getValorCampoOrder($campoOrder, $valorCampoOrder) {
        $valores = $this->getValoresSelect($campoOrder);
        if (!is_array($valores)) {
            $valores = json_decode($valores, true);
        }

        foreach ($valores as $valor) {
            if ($valor['text'] == $valorCampoOrder) {
                return $valor['value'];
            }
        }
        
        return $valorCampoOrder;
    }

    private function getValorCondicoesEspeciais($textoCondicaoEspecial) {
        return \App\Models\Configuracao\CondicoesEspeciais::where(['label' => $textoCondicaoEspecial])->value('valor');
    }

    private function getValorAcao($textoAcao) {
        return \App\Models\Configuracao\Acoes::where(['label' => $textoAcao])->first()['valor'];
    }

    private function getjsonConfiguracao($interface) {
        $configuracao = array();
        if ($interface != null) {
            foreach ($interface as $tratativa) {
                if ($tratativa['ativo'] == 'true') {
                    $grupos = array();
                    if (isset($tratativa['condicoes'])) {
                        foreach ($tratativa['condicoes'] as $grupoCondicoes) {
                            $condicoes = array();
                            foreach ($grupoCondicoes as $condicao) {
                                if ($condicao['ativo'] == 'true') {
                                    $valores = array();
                                    foreach ($condicao['valor'] as $valores_condicao) {
                                        $valores[] = $this->getValorCampoOrder($condicao['campo'], $valores_condicao);
                                    }
                                    $nomeCondicao = $this->getValueByLabel($condicao['campo']);
                                    if ($nomeCondicao == $condicao['campo']) {
                                        $condicoes['condicoes_especiais'][$this->getValorCondicoesEspeciais($condicao['campo'])] = implode("|", $valores);
                                    }
                                    else {
                                        $condicoes[$this->getValueByLabel($condicao['campo'])] = implode("|", $valores);
                                    }
                                }
                            }
                            $grupos[] = $condicoes;
                        }
                    }
                    else {
                        $grupos = null;
                    }
                    
                    $regras = array();
                    if (isset($tratativa['regras'])) {
                        foreach ($tratativa['regras'] as $regra) {
                            if ($regra['ativo'] == 'true') {
                                $grupoValidacoes = array();
                                if (isset($regra['validacoes'])) {
                                    foreach ($regra['validacoes'] as $grupoValidacao) {
                                        $validacoes = array();
                                        foreach ($grupoValidacao as $validacao) {
                                            if ($validacao['ativo'] == 'true') {
                                                if (isset($validacao['valor'])) {
                                                    foreach ($validacao['valor'] as $i => $v) {
                                                        $value = $this->getValueByLabel($v);
                                                        if ($value != $v) {
                                                            $value = 'order.'.$value;
                                                        }
                                                        $validacao['valor'][$i] = $value;
                                                    }
                                                }
                                                $validacoes[] = [
                                                    'campo' => $validacao['campo'],
                                                    'valor' => @implode("|", $validacao['valor']),
                                                    'comparacao' => $validacao['comparacao']
                                                ];
                                            }
                                        }
                                        $grupoValidacoes[] = $validacoes;
                                    }
                                }
                                else {
                                    $grupoValidacoes = null;
                                }

                                $acoes = array();
                                foreach ($regra['acoes'] as $acao) {
                                    if ($acao['ativo'] == 'true') {
                                        $acoes[] = $this->getValorAcao($acao['acao']);
                                    }
                                }

                                $dados_adicionais = array();
                                if (isset($regra['dados_adicionais'])) {
                                    foreach ($regra['dados_adicionais'] as $dados) {
                                        if ($dados['ativo'] == 'true') {
                                            $dados_adicionais[$this->getValueByLabel($dados['campo'])] = $this->getValorCampoOrder($dados['campo'], $dados['valor']);
                                        }
                                    }
                                }
                                else {
                                    $dados_adicionais = null;
                                }
                            
                                $regras[] = [
                                    'validacoes' => $grupoValidacoes,
                                    'acoes' => $acoes,
                                    'dados_adicionais' => $dados_adicionais
                                ];
                            }
                        }
                    }
                    
                    $configuracao[] = [
                        "condicoes" => $grupos,
                        "regras" => $regras
                    ];
                    
                    if ($grupos == null) {
                        unset($configuracao[count($configuracao)-1]['condicoes']);
                    }
                }
            }
        }
        
        return $configuracao;
    }

    public function salvarConfiguracao(\Illuminate\Http\Request $request) {
        try {
            $mongo = Configuracao::where('nome', $request['nome'])->get();
            if (count($mongo) > 0) {
                $mongo = $mongo[0];
            }
            else {
                $mongo = new Configuracao();
            }
            $ultimo_update = $mongo->updated_at;
            $mongo->nome = $request['nome'];
            $interface = $this->limparArrayInterface($request['interface']);

            $mongo->interface = $interface;
            if (isset($request['interface']) > 0) {
                $mongo->configuracao = $this->getjsonConfiguracao($interface);
            }
            $mongo->json_robo = $request['json_robo'];
            $mongo->usuario_padrao = $request['usuario_padrao'];

            $mongo->save();

            if ($ultimo_update != $mongo->updated_at) {
                $historico = new ConfiguracaoHistorico();
                $historico->nome = $request['nome'];
                $historico->interface = $interface;
                $historico->json_robo = $request['json_robo'];
                $historico->usuario_padrao = $request['usuario_padrao'];
                $historico->usuario = \Auth::user()->id;
                $historico->save();
            }

            return 'ok';
        }
        catch (\Exception $ex) {
            return $ex->getMessage().' '.$ex->getLine();
        }
        catch (\Error $ex) {
            return $ex->getMessage().' '.$ex->getLine();
        }
    }

    private function limparArrayInterface($array) {
        if ($array != null) {
            $novo = array();

            foreach ($array as $i => $item) {
                if (!isset($item['excluido'])) {
                    if (is_array($item)) {
                        if (!is_numeric($i)) {
                            $novo[$i] = $this->limparArrayInterface($item);
                        }
                        else {
                            $novo[] = $this->limparArrayInterface($item);
                        }
                    }
                    else {
                        if (!is_numeric($i)) {
                            $novo[$i] = $item;
                        }
                        else {
                            $novo[] = $item;
                        }
                    }
                }
            }

            $array = $novo;
        }

        return $array;
    }

    private function ordenar($array) {
        usort($array, function($a, $b) {
			if ($a['text'] == $b['text']) {
				return 0;
			}
			return ($a['text'] < $b['text']) ? -1 : 1;
        });
        
        return $array;
    }
	
}