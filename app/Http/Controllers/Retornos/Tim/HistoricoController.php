<?php
namespace App\Http\Controllers\Retornos\Tim;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Retornos\Tim\Historicos\ConsultaOrdem;



class HistoricoController extends Controller  {
    public function consultaOrdem( ?Request $request ) {
        $graph          = ConsultaOrdem::findResumoHistory();
        $model          = ConsultaOrdem::listByOrder( $request->get( 'ordem' ) );
        $JSONchartData  = $this->montaJSONChart( $graph );

        return view( 'retornos.tim.historicos.consultaordem', [ 'model' => $model, 'graph' => $JSONchartData, 'result' => $graph, ] );
    }

    private function montaJSONChart( $graph ): String {
        $dataChart  = [];
        $colors     = [ '#f39c12', '#988da3', '#3c8dbc', '#fd3a07', '#417c7c', '#FFD700','#336699', '#6B8E23', '#BDB76B','#ec008c','#2e3d49','#000000','#003366','#2f2f2f','#071900','#fac152','#191308','#8e6873','#553e45' ];
        $red        = "#FF0000";
        $green      = "#4ffd07";
        $cnt        = 0;

        foreach( $graph as $item ){
            if( $item->status_ordem == 'CANCELADO' ) {
                $color = $red;
            }
            else if( $item->status_ordem == 'CONCLUíDO' ) {
                $color = $green;
            }
            else {
                $color = $colors[$cnt++]; 
            }

            $dataChart[] = (Object)[ "value" => $item->total_por_status, "color" => $color, "highlight" => "#45463e", "label" => $item->status_ordem ];
        }

        return json_encode( $dataChart, true );
    }
}