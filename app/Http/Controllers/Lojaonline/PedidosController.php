<?php

namespace App\Http\Controllers\Lojaonline;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PedidosController extends Controller
{
	public function listar (Request $request, $operadora)
	{
		return 'Pedidos da Operadora ' . $operadora;
	}
}