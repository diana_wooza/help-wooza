<?php

namespace App\Http\Controllers\Lojaonline;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Lojaonline\Order;

class ApiController extends Controller
{
	public function pedidos_claro ($dataDe, $dataAte)
	{
		return $this->_get_pedidos_claro([Order::STORE_CLARO_PROSPECT, Order::STORE_CLARO_PRE, Order::STORE_CLARO_POS], $dataDe, $dataAte);
	}

	public function pedidos_claro_prospect ($dataDe, $dataAte)
	{
		return $this->_get_pedidos_claro([Order::STORE_CLARO_PROSPECT], $dataDe, $dataAte);
	}

	public function pedidos_claro_pre ($dataDe, $dataAte)
	{
		return $this->_get_pedidos_claro([Order::STORE_CLARO_PRE], $dataDe, $dataAte);
	}

	public function pedidos_claro_pos ($dataDe, $dataAte)
	{
		return $this->_get_pedidos_claro([Order::STORE_CLARO_POS], $dataDe, $dataAte);
	}

	public function pedidos_tim ($dataDe, $dataAte)
	{
		return $this->_get_pedidos_tim($dataDe, $dataAte);
	}

	private function _get_pedidos_claro (array $storeId, $dataDe, $dataAte)
	{
		return Order::claro($dataDe, $dataAte, $storeId)->get();
	}

	private function _get_pedidos_tim ($dataDe, $dataAte)
	{
		return Order::tim($dataDe, $dataAte)->get();
	}
}