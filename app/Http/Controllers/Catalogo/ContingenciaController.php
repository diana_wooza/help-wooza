<?php

namespace App\Http\Controllers\Catalogo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContingenciaController extends Controller
{
	public function analizador_bundle (Request $request)
	{
		$skus = null;

		if ($request->isMethod('post'))
		{
			$config = (object) $request->only(['arquivo','ambiente', 'url_default']);

			if ($config->url_default == 'hub_default')
				$config->arquivo = $this->_parseFileUrl($config->arquivo);

			$skus = $this->_getSkus($config->arquivo);

			if (!is_array($skus))
				return $skus;

			$skus = array_map(function ($sku) {return 'SELECT \'' . $sku . '\' AS value';}, $skus);
			$skus = implode(PHP_EOL . 'UNION ALL' . PHP_EOL, $skus);

			$sql = "SELECT
						myValues.value
					FROM
						(
							{$skus}
						) AS myValues
						LEFT JOIN catalog_product_entity
							ON catalog_product_entity.sku = myValues.value
					WHERE
						catalog_product_entity.entity_id IS NULL
						AND myValues.value != ''
			";

			switch ($config->ambiente)
			{
				case 'consumer_prd':

					$builder = DB::connection('magento');
					break;

				case 'consumer_hmg':

					$builder = DB::connection('magento_qa');
					break;

				case 'corp_prd':

					$builder = DB::connection('magento_corp');
					break;

				case 'corp_hmg':

					$builder = DB::connection('magento_corp_qa');
					break;

				default:

					return redirect()->back()->withErrors(['Ambiente não selecionado ou desconhecido']);
			}

			$skus = array_map(function ($sku) {return $sku->value;}, $builder->select($sql));
		}

		return view('catalogo.contingencia_analisador', [
			'ambientes'		=> [
				'consumer_prd'	=> 'Gestão Consumer Producao',
				'consumer_hmg'	=> 'Gestao Consumer Homologação',
				'corp_prd'		=> 'Corp Producao',
				'corp_hmg'		=> 'Corp Homologação',
			],
			'url_default'	=> [
				#a primeira será a padrão
				'hub_default'	=> 'Usar Ajuste de URL',
				'nenhuma'		=> 'Não usar ajuste de URL',
			],
			'skus'			=> $skus,
		]);
	}

	private function _parseFileUrl ($url)
	{
		return 'http://magmi.celulardireto.com.br/import/' . preg_replace('/(http:\/\/)?(www)?(magmi\.celulardireto\.com\.br)?(\/import)?(\/)?/', '', $url);
	}

	private function _getSkus ($arquivo)
	{
		try
		{
			$content = file_get_contents($arquivo);
			$skus = [];

			foreach (explode(PHP_EOL, $content) as $lineNumber => $line)
			{
				if ($lineNumber == 0)
					continue;

				foreach (@explode(';', str_getcsv($line, ';', '"', '\\')[3]) as $sku)
					$skus[] = preg_replace('/(CHIP:|Planos:|(?<!_)SVA:|:1.0000:0:0:1:0.0000:0|:1.0000:0:0:0:0.0000:0|:1.0000:0:0:0:.0000:0)/', '', $sku);
			}

			return $skus;
		}
		catch (\Throwable $throwable)
		{
			return redirect()->back()->withErrors(['Ocorreu um erro ao tentar buscar o arquivo selecionado. Verifique a URL e tente novamente']);
		}
	}
}