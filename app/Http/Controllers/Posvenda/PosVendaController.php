<?php

namespace App\Http\Controllers\PosVenda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Posvenda\Acao;
use App\Models\Posvenda\Atendimentos;
use App\Models\Posvenda\CanalAtendimento;
use App\Models\Posvenda\Motivo1;
use App\Models\Posvenda\Motivo2;
use App\Models\Posvenda\OperadoraProduto;
use App\Models\Posvenda\Origem;
use App\Models\Posvenda\Notificacoes;
use App\Models\Magento\Consumer\Order;
use App\Models\Magento\Consumer\OrderHistory;
use App\Helpers\Magento;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;

class PosVendaController extends Controller {

    // public function registrarAtendimentos_old(Request $request) {
    //     $origens = \App\Models\Posvenda\Origem::where('status', 1)->orderBy('origem');
    //     $canais = \App\Models\Posvenda\CanalAtendimento::where('status', 1)->orderBy('canal_atendimento')->get();
    //     $operadorasProdutos = \App\Models\Posvenda\OperadoraProduto::where('status', 1)->orderBy('operadora_produto');
    //     $motivos1 = \App\Models\Posvenda\Motivo1::where('status', 1)->orderBy('motivo')->get();
    //     $motivos2 = \App\Models\Posvenda\Motivo2::where('status', 1)->orderBy('motivo')->get();
	// 	$acoes = \App\Models\Posvenda\Acao::where('status', 1)->orderBy('acao')->get();
		
		

	// 	if (\Auth::user()->checkPermissao('posvenda.claro'))
	// 	{
    //         $origens = $origens->whereIn('origem.origem', ['Loja Online', 'Móvel', 'Varejo']);
    //         $operadorasProdutos = $operadorasProdutos->where('operadora_produto.operadora_produto', 'Claro');
    //     }
        
    //     $origens = $origens->get();
    //     $operadorasProdutos = $operadorasProdutos->get();

    //     $jsonMotivos2 = array();
    //     foreach ($motivos2 as $motivo) {
    //         $jsonMotivos2[] = ['value' => $motivo->id, 'label' => $motivo->motivo];
    //     }

    //     $jsonAcoes = array();
    //     foreach ($acoes as $acao) {
    //         $jsonAcoes[] = ['value' => $acao->id, 'label' => $acao->acao];
    //     }

	// 	// return view('posvenda.registrar_atendimentos', [
    //     //     'protocolo' => $request->protocolo,
    //     //     'origens' => $origens,
    //     //     'canais' => $canais,
    //     //     'operadorasProdutos' => $operadorasProdutos,
    //     //     'motivos1' => $motivos1,
    //     //     'motivos2' => $jsonMotivos2,
    //     //     'acoes' => $jsonAcoes
    //     // ]);
    //     return view('posvenda.registro', [
    //         'protocolo' => $request->protocolo,
    //         'origens' => $origens,
    //         'canais' => $canais,
    //         'operadorasProdutos' => $operadorasProdutos,
    //         'motivos1' => $motivos1,
    //         'motivos2' => $jsonMotivos2,
    //         'acoes' => $jsonAcoes
    //     ]);
    // }

    
    public function buscarAtendimentoPorPedido( Request $request ) {
        $model = Atendimentos::where(['numero_pedido' => $request->query('pedido')])->with(['origem','acao','motivo1','motivo2','canal', 'operadora'])->orderBy('id','DESC')->get();

        return response()->json(['model'=> $model]);
    }

    public function buscarOrigens( Request $request ) {
        $origens    = Origem::where('status', 1);
        $criterios  = [];

        // if( $request->query('tipo') == 1 )
        //     $criterios = (\Auth::user()->checkPermissao('posvenda.claro'))  ? [1]       : [1,6,7,13];   //pedidos não oriundos do magento
        // else
        //     $criterios = (\Auth::user()->checkPermissao('posvenda.claro'))  ? [3,13]    : [3,4,13];  //pedidos oriundos do magento

        if( $request->query('tipo') == 1 )
            $criterios = [1,6,7,13];   //pedidos não oriundos do magento
        else
            $criterios = [3,4,13];  //pedidos oriundos do magento
        
        $origens = $origens->whereIn('id', $criterios)->orderBy('origem')->get();

        return response()->json(['model'=> $origens]);
    }

    public function salvarAviso( Request $request ) {
        try {
            $model  = new Notificacoes();
            $result = $model->salvarNotificacao( $request );
        }
        catch( \Throwable $exception ) {
            throw new \Exception($exception->getMessage());
        }

        return response()->json(['model'=> $result,]);
    }

    public function registrarAtendimentos(Request $request) {
        $order                  = [];
        $chamados               = [];
        $canais                 = CanalAtendimento::where('status', 1)->orderBy('canal_atendimento')->get();
        $acoes                  = Acao::where('status', 1)->orderBy('acao')->get();
        $operadorasProdutos     = OperadoraProduto::where('status', 1)->orderBy('operadora_produto');
        $motivos                = Motivo1::where(['status' => 1])->get();
        $notificacoes           = Notificacoes::notificacoes();

        if( $request->query('cpf_busca') || $request->query('pedido_busca') ) {
            $busca = (object)$request->query();
            
            if( true == is_numeric($busca->cpf_busca) ) {
                $order = Order::where(['customer_cpf' => $busca->cpf_busca]);
                $chamados = Atendimentos::where(['cpf_cliente' => $busca->cpf_busca]);
            }
            else if( 0 < strlen( $busca->pedido_busca ) ) {
                if( true == is_array($chamados) ) $chamados = Atendimentos::where(['numero_pedido' => $busca->pedido_busca]);
                else $chamados = $chamados->orWhere(['numero_pedido' => $busca->pedido_busca]);
                
                if( true == is_array($order) ) $order = Order::where(['increment_id' => $busca->pedido_busca]);
                else $order->orWhere(['increment_id' => $busca->pedido_busca]);
            }

            if( false === is_array($order) ) {
                $order = $order->with(['customer']);
                
                $order = $order->simplePaginate(5)->appends($request->all());
                if( false === is_numeric($busca->cpf_busca) && count($order) ) $chamados->orWhere(['cpf_cliente' => $order[0]->CustomerCpf]);
            }
        }


        if( false == is_array($chamados)) $chamados = $chamados->with(['origem','acao','motivo1','motivo2','canal', 'operadora'])->get();

        if (\Auth::user()->checkPermissao('posvenda.claro'))
            $operadorasProdutos = $operadorasProdutos->where('operadora_produto.operadora_produto', 'Claro');

        // $jsonAcoes = array();
        // foreach ($acoes as $acao)
        //     $jsonAcoes[] = ['value' => $acao->id, 'label' => $acao->acao];
		
        return view('posvenda.registro', [
            'pedidos'       => $order,
            'chamados'      => $chamados,
            'canais'        => $canais,
            'acoes'         => $acoes,
            'produtos'      => $operadorasProdutos->get(),
            'motivos'       => $motivos,
            'notificacoes'  => $notificacoes,
        ]);
    }

    public function salvarAtendimento( Request $request ){
        if( false === $request->isMethod('post') ) throw new \Exception("Request Inválido.");

        $resultado          = (object)['mensagemAtendimento' => 'Atedimento registrado com sucesso.', 'status' => 200, 'retornoApi' => false];
        $resultadoMagento   = (object)['mensagemAtendimento' => 'Atedimento registrado com sucesso.', 'status' => 200];
        $result             = false;

        try {
            $model  = new Atendimentos();
            $model  = $model->salvarAtendimento( $request );
            $result = Atendimentos::where(['id' => $model->id])->with(['origem','canal','operadora','motivo1','motivo2','acao'])->first();
            $resultadoMagento->mensagemAtendimento = "Atedimento registrado com sucesso sob o protocolo {$result->protocolo}.";

            if( strlen($request->input('pedido')) && false == in_array($request->input('pedido'), ["Não Cliente", "Não Informado"]) ) {
                switch( $request->input('acao') ):
                    case Acao::ACAO_CANCELAR_PEDIDO :
                        $resultado->retornoApi = json_decode($this->cancelarPedidoMagento( $request->input('pedido'), $request->input('codCancelamento'), $request->input('chamado') ));
                    break;
                    case Acao::ACAO_STATUS_MOTIVO :
                        $resultado->retornoApi = json_decode($this->statusMotivoMagento( $request->input('pedido'), 'prioridade', $request->input('chamado') ));
                    break;
                    default:
                        $resultado->retornoApi = json_decode($this->addComentario( $request->input('pedido'), $request->input('chamado'), "Atedimento registrado com sucesso sob o protocolo {$result->protocolo}." ));
                endswitch;
            }
        }
        catch( \Throwable $exception ) {
            $resultadoMagento->mensagemAtendimento = "Erro ao tentar registrar atendimento ({$exception->getMessage()})";
            $resultadoMagento->status = 403;

            throw new \Exception($exception->getMessage());
        }

        $resultado->mensagemAtendimento = $resultadoMagento->mensagemAtendimento;
        $resultado->status = $resultadoMagento->status;
        return response()->json(['model'=> $result, 'resultado' => $resultado,]);
    }

    private function addComentario( $pedido, $chamado, $mensagem ) {
        $result = (object)['status' => 200, 'mensagemAtendimento' => $mensagem];

        try {
            if( 0 == strlen($pedido) || $pedido == "Não Cliente" ) return $result;
            
            $url        = env("MAGENTO_API_HOST") . "/api_cd/addComentarioHistoricoMagentoPosVenda.php";
            $client     = new Client([ 'headers' => [ 'Content-Type' => 'application/json' ] ] );
            $request    = [ "pedido" => $pedido, "comentario" => $chamado, "username" => \Auth::user()->login ];
            $response   = $client->post( $url, ['body' => json_encode( $request )] );

            $resultado = json_decode($response->getBody()->getContents());

            if( 200 != $resultado->status ) {
                $resultado->mensagem     = $mensagem;
                $resultado->status       = 400;
            }
        }
        catch( \Trowable $e ) {
            $result->mensagem    = "Porém não foi possível adicionar comentário no magento, favor solicitar a atualização manual. (" . $e->getMessage() . ")";
            $result->status                 = 500;

            return $result;
        }

        return json_encode($resultado, JSON_UNESCAPED_UNICODE);
    }

    private function cancelarPedidoMagento( $pedido, $codigoCancelamento, $chamado ) {
        $result = (object)['status' => 200, 'message' => ''];

        try {
            $url        = env("MAGENTO_API_HOST") ."/api_cd/cancelar_pedido_posvenda.php";
            $client     = new Client([ 'headers' => [ 'Content-Type' => 'application/json' ] ] );
            $request    = [ "pedido" => $pedido, "codigoCancelamento" => $codigoCancelamento, 'comentario' => $chamado, "username" => \Auth::user()->login ];
            $response   = $client->post( $url, ['body' => json_encode( $request )] );
        }
        catch( \Trowable $e ) {
            $result->status     = 500;
            $result->message    = "Falha ao cancelar pedido no Magento, favor solicitar a atualização manual. (" . $e->getMessage() . ")";

            return $result;
        }

        return $response->getBody()->getContents();
    }

    private function statusMotivoMagento( $pedido, $motivo, $chamado ) {
        $result = (object)['status' => 200, 'message' => 'Motivo atualizado com sucesso.'];

        try {
            $url        = env("MAGENTO_API_HOST") ."/api_cd/add_status_motivo_pedido_posvenda.php";
            $client     = new Client([ 'headers' => [ 'Content-Type' => 'application/json' ] ] );
            $request    = [ "pedido" => $pedido, "motivo" => $motivo, 'comentario' => $chamado, "username" => \Auth::user()->login ];
            $response   = $client->post( $url, ['body' => json_encode( $request )] );
        }
        catch( ClientErrorResponseException  $e ) {
            $result->status     = $e->getResponse()->getStatusCode();
            $result->message    = $e->getMessage();

            return $result;
        }

        return $response->getBody()->getContents();
    }

    
    // public function salvarAtendimento_old(Request $request) {
    //     $numero_pedido = $request->input('numero_pedido');
    //     if ($request->check_cliente) {
    //         $numero_pedido = 'Não Cliente';
    //     }
    //     $cpf_cliente = $request->input('cpf_cliente');
    //     if ($request->check_cpf) {
    //         $cpf_cliente = 'Não Informado';
    //     }
    //     $atendimento = new Atendimentos();
    //     $atendimento->protocolo = $atendimento->proximoProtocolo();
    //     $atendimento->id_origem = $request->input('origem');
    //     $atendimento->id_canal_atendimento = $request->input('canal_atendimento');
    //     $atendimento->id_operadora_produto = $request->input('operadora_produto');
    //     $atendimento->id_atendimento = $request->input('id_atendimento');
    //     $atendimento->numero_pedido = $numero_pedido;
    //     $atendimento->nome_cliente = $request->input('nome_cliente');
    //     $atendimento->cpf_cliente = $cpf_cliente;
    //     $atendimento->id_motivo1 = $request->input('motivo1');
    //     $atendimento->id_motivo2 = $request->input('motivo2');
    //     $atendimento->id_acao = $request->input('acao');
    //     $atendimento->chamado = $request->input('chamado');
    //     $atendimento->id_usuario = \Auth::user()->id;
    //     $atendimento->save();

    //     return redirect()->route('posvenda.registrar_atendimentos', ['protocolo' => $atendimento->protocolo]);
    // }

    public function adicionarOpcoes(Request $request) {
		return view('posvenda.adicionar_opcoes', ['s' => $request->s]);
    }

	public function listarAtendimentos (Request $request)
	{
		$filtros = $request->only(['periodo','protocolo','atendimento','origem','canal_atendimento','operadora_produto','numero_pedido','nome_cliente','cpf_cliente','motivo1','motivo2_label','motivo2','acoes','acao', 'chamado','usuario']);
        $atendimentos   = Atendimentos::with(['origem','canal','operadora','motivo1','motivo2','acao'])->orderBy('created_at', 'DESC')->FiltroListaAtendimento($filtros);
        

		if (\Auth::user()->checkPermissao('posvenda.claro'))
			$atendimentos->SomenteClaro();
		
		if ($request->submit == 'Exportar')
		{
            $lista = $atendimentos->select()->limit(50000)->get()->map(function ($atendimento) {

				return [
					'data'				=> date('d/m/Y H:i:s', strtotime($atendimento->created_at)),
					'protocolo'			=> utf8_decode($atendimento->protocolo),
					'id_atendimento'	=> utf8_decode($atendimento->id_atendimento),
					'origem'			=> utf8_decode(@$atendimento->origem->origem),
					'canal_atendimento'	=> utf8_decode(@$atendimento->canal->canal_atendimento),
					'operadora_produto'	=> utf8_decode(@$atendimento->operadora->operadora_produto),
					'numero_pedido'		=> utf8_decode($atendimento->numero_pedido),
					'nome_cliente'		=> utf8_decode($atendimento->nome_cliente),
					'cpf_cliente'		=> utf8_decode($atendimento->cpf_cliente),
					'motivo1'			=> utf8_decode(@$atendimento->motivo1->motivo),
					'motivo2'			=> utf8_decode(@$atendimento->motivo2->motivo),
					'acao'				=> utf8_decode(@$atendimento->acao->acao),
					'chamado'			=> utf8_decode(@$atendimento->chamado),
					'nome'				=> utf8_decode(@$atendimento->usuario->nome),
				];

            })->toArray();
            
            if( 0 < count($lista) )  return $this->exportarCsv($lista);
        }
        
		return view('posvenda.listar_atendimentos', [
			'atendimentos'			=> $atendimentos->paginate(20),
			'filtros'				=> $filtros,
			'isAdmin'				=> \Auth::user()->checkPermissao('posvenda.admin'),
			'acoes'					=> Acao::orderBy('acao')->get(),
			'motivos1'				=> Motivo1::orderBy('motivo')->get(),
			'motivos2'				=> Motivo2::orderBy('motivo')->get(),
			'canais'				=> CanalAtendimento::orderBy('canal_atendimento')->get(),
			'origens'				=> Origem::where(function ($query) {

				if (\Auth::user()->checkPermissao('posvenda.claro'))
					$query->whereIn('origem.origem', ['Loja Online', 'Móvel', 'Varejo']);

				return $query;

			})->orderBy('origem')->get(),
			'operadorasProdutos'	=> OperadoraProduto::where(function ($query) {

				if (\Auth::user()->checkPermissao('posvenda.claro'))
					$query->where('operadora_produto.operadora_produto', 'Claro');

				return $query;

			})->orderBy('operadora_produto')->get(),
		]);


		// $origens = \App\Models\Posvenda\Origem::orderBy('origem');
		// $canais = \App\Models\Posvenda\CanalAtendimento::orderBy('canal_atendimento')->get();
		// $operadorasProdutos = \App\Models\Posvenda\OperadoraProduto::orderBy('operadora_produto');
		// $motivos1 = \App\Models\Posvenda\Motivo1::orderBy('motivo')->get();
		// $motivos2 = \App\Models\Posvenda\Motivo2::orderBy('motivo')->get();
		// $acoes = \App\Models\Posvenda\Acao::orderBy('acao')->get();
		// $atendimentos = new \App\Models\Posvenda\Atendimentos();
		// $listaAtendimentos = $atendimentos->listarAtendimentos($request);

		// if (\Auth::user()->checkPermissao('posvenda.claro')) {
		// 	$origens = $origens->whereIn('origem.origem', ['Loja Online', 'Móvel', 'Varejo']);
		// 	$operadorasProdutos = $operadorasProdutos->where('operadora_produto.operadora_produto', 'Claro');
		// }
		
		// $origens = $origens->get();
		// $operadorasProdutos = $operadorasProdutos->get();

		// $jsonMotivos2 = array();
		// foreach ($motivos2 as $motivo) {
		// 	$jsonMotivos2[] = ['value' => $motivo->id, 'label' => $motivo->motivo];
		// }

		// $jsonAcoes = array();
		// foreach ($acoes as $acao) {
		// 	$jsonAcoes[] = ['value' => $acao->id, 'label' => $acao->acao];
		// }

		// $selecionados = [
		// 	'periodo' => $request->periodo,
		// 	'protocolo' => $request->protocolo,
		// 	'id_atendimento' => $request->id_atendimento,
		// 	'origem' => $request->origem,
		// 	'canal' => $request->canal_atendimento,
		// 	'operadora_produto' => $request->operadora_produto,
		// 	'numero_pedido' => $request->numero_pedido,
		// 	'nome_cliente' => $request->nome_cliente,
		// 	'cpf_cliente' => $request->cpf_cliente,
		// 	'motivo1' => $request->motivo1,
		// 	'motivo2' => $request->motivo2,
		// 	'motivo2_label' => $request->motivo2_label,
		// 	'acoes' => $request->acoes,
		// 	'acao' => $request->acao,
		// 	'usuario' => $request->usuario
		// ];

		// if ($request->submit == 'Exportar') {
		// 	return $this->exportarCsv($listaAtendimentos->limit(10000)->get()->toArray());
		// }
		
		// return view('posvenda.listar_atendimentos', [
		// 	'atendimentos' => $listaAtendimentos,
		// 	'origens' => $origens,
		// 	'canais' => $canais,
		// 	'operadorasProdutos' => $operadorasProdutos,
		// 	'motivos1' => $motivos1,
		// 	'motivos2' => $jsonMotivos2,
		// 	'acoes' => $jsonAcoes,
		// 	'selecionados' => $selecionados,
		// 	'perfils' => \Auth::user()->perfis,
		// ]);
	}

    public function administrarRelacoesMotivos( Request $request, $id ) {
        if( $request->isMethod('post') ) {
            $selecionados = explode(",", $request->input('motivos_2'));

            try{
                \App\Models\Posvenda\SelecaoMotivos::salvarMotivos( $selecionados, $id );
            }
            catch( \Exception $e ) {
                throw new \Exception($e->getMessage());
            }
        }

        $model = \App\Models\Posvenda\SelecaoMotivos::where(['motivo1_id' => $id])->with(['motivo2'])->get();
        $motivo = \App\Models\Posvenda\Motivo1::find($id);
        $exc   = \App\Models\Posvenda\SelecaoMotivos::where(['motivo1_id' => $id])->pluck('motivo2_id')->toArray();
        $motivos = \App\Models\Posvenda\Motivo2::whereNotIn('id', $exc)->get();

        return view('posvenda.administrar_motivos', ['dados' => $model, 'motivo' => $motivo, 'motivos' => $motivos, 'motivosSelecionados' => implode(",", $exc)]);
    }

    public function buscarMotivos( Request $request, $id ) {
        $model = \App\Models\Posvenda\SelecaoMotivos::where(['motivo1_id' => $id])->with(['motivo2'])->get();

        $jsonMotivos2 = array();
        foreach ($model as $motivo) {
            $jsonMotivos2[] = ['value' => $motivo->motivo2_id, 'label' => $motivo->motivo2->motivo];
        }

        return response()->json(['model'=> $jsonMotivos2]);
    }

    public function seachMotivos( Request $request, $id ) {
        $model = \App\Models\Posvenda\SelecaoMotivos::where(['motivo1_id' => $id])->with(['motivo2'])->get();

        $jsonMotivos2 = [];
        foreach ($model as $motivo)
            $jsonMotivos2[] = ['value' => $motivo->motivo2_id, 'label' => $motivo->motivo2->motivo];

        return response()->json(['model'=> $jsonMotivos2]);
    }

    public function buscarComentarios( Request $request, $id ) {
        $model = OrderHistory::where(['parent_id' => $id])->with(['usuario'])->get();

        return response()->json(['model'=> $model]);
    }

    public function listarMotivos( Request $request ) {
        $model = \App\Models\Posvenda\Motivo1::get();
        
        return view('posvenda.listar_motivos', ['dados' => $model]);
    }

    public function deletarAtendimento( Request $request, $id ) {
        $model = \App\Models\Posvenda\Atendimentos::find($id);
        if( $model ) {
            $model->delete();
        }

        return redirect('posvenda\listar_atendimentos')->with(['message' => 'Atendimento removido com sucesso.']);
    }

    private function exportarCsv($lista) {
		ini_set('max_execution_time', 3600);
		set_time_limit(3600);
		ini_set('memory_limit', '3G');

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-  check=0'
			,   'Content-type'        => 'text/csv'
			,   'charset'             => 'utf-8'
            ,   'Content-Disposition' => 'attachment; filename=atendimentos.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        
        array_unshift($lista, array_keys($lista[0]));
        
        $callback = function() use ($lista)  {
            $fh = fopen('php://output', 'w');
            foreach ($lista as $row) { 
                fputcsv($fh, $row, ';');
            }
            fclose($fh);
         };
        
        return \Response::stream($callback, 200, $headers);
    }

    public function alterarOpcoes(Request $request) {
        $origens = \App\Models\Posvenda\Origem::orderBy('origem')->get();
        $canais = \App\Models\Posvenda\CanalAtendimento::orderBy('canal_atendimento')->get();
        $operadorasProdutos = \App\Models\Posvenda\OperadoraProduto::orderBy('operadora_produto')->get();
        $motivos1 = \App\Models\Posvenda\Motivo1::orderBy('motivo')->get();
        $motivos2 = \App\Models\Posvenda\Motivo2::orderBy('motivo')->get();
        $acoes = \App\Models\Posvenda\Acao::orderBy('acao')->get();

        $jsonMotivos2 = array();
        foreach ($motivos2 as $motivo) {
            $jsonMotivos2[] = ['value' => $motivo->id, 'label' => $motivo->motivo];
        }

        $jsonAcoes = array();
        foreach ($acoes as $acao) {
            $jsonAcoes[] = ['value' => $acao->id, 'label' => $acao->acao];
        }

		return view('posvenda.alterar_opcoes', [
            's' => $request->s,
            'origens' => $origens,
            'canais' => $canais,
            'operadorasProdutos' => $operadorasProdutos,
            'motivos1' => $motivos1,
            'motivos2' => $jsonMotivos2,
            'acoes' => $jsonAcoes
        ]);
    }

    public function salvarAlterarOpcoes(Request $request) {
        $request->campo_selecionado = str_replace("_label", "", $request->campo_selecionado);
        if ($request->campo_selecionado == 'acoes') {
            $request->campo_selecionado = 'acao';
        }
        $model = $this->getModel($request->campo)->find($request->{$request->campo_selecionado});
        
        $model->status = $request->status;
        $model->save();

        return redirect()->route('posvenda.alterar_opcoes', ['s' => true]);
    }

    public function salvarOpcoes(Request $request) {
        $request->campo_selecionado = str_replace("_label", "", $request->campo_selecionado);
        if ($request->campo_selecionado == 'acoes') {
            $request->campo_selecionado = 'acao';
        }
        $model = $this->getModel($request->campo);
        $model->find($request->{$request->campo_selecionado});
        
        $model->{$this->campo} = $request->valor;
        if ($request->status == null) {
            $request->status = 1;
        }
        $model->status = $request->status;
        $model->save();

        return redirect()->route('posvenda.adicionar_opcoes', ['s' => true]);
    }

    public function buscarStatus(Request $request) {
        $model = $this->getModel($request->campo);

        return $model->find($request->valor)->status;
    }

    private function getModel($campo) {
        switch ($campo) {
            case 'origem';
                $this->campo = 'origem';
                return new \App\Models\Posvenda\Origem();
            break;
            case 'canal_atendimento':
                $this->campo = 'canal_atendimento';
                return new \App\Models\Posvenda\CanalAtendimento();
            break;
            case 'operadora_produto':
                $this->campo = 'operadora_produto';
                return new \App\Models\Posvenda\OperadoraProduto();
            break;
            case 'motivo1':
                $this->campo = 'motivo';
                return new \App\Models\Posvenda\Motivo1();
            break;
            case 'motivo2_label':
                $this->campo = 'motivo';
                return new \App\Models\Posvenda\Motivo2();
            break;
            case 'acoes':
                $this->campo = 'acao';
                return new \App\Models\Posvenda\Acao();
            break;
        }

        return null;
    }

}