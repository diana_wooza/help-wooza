<?php

namespace App\Http\Controllers\Contestador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contestador\Pedido;
use App\Models\Contestador\Competencia;
use App\Models\Operadora;

class PedidoController extends Controller
{
	public function listar (Request $request)
	{
		ini_set('max_execution_time', 120);
		ini_set('memory_limit', '1G');

		$data = Pedido
			::with(['competencia','operadora','remuneracoes','churns'])
			// ->withSum([])
			->ComFiltro($request)
			->ComOrdenacao($request)
		;

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Lista de Pedidos',
			'filtros'			=> [
				['chave'=>'segmento', 'rotulo'=>'Segmentos', 'tipo'=>'multiselect', 'itens'=>Pedido::$segmentos, 'colunas'=>4],
				['chave'=>'competencia', 'rotulo'=>'Competências', 'tipo'=>'multiselect', 'itens'=>Competencia::orderBy('data', 'DESC')->get()->pluck('data_string','id'), 'colunas'=>4],
				['chave'=>'operadora', 'rotulo'=>'Operadora', 'tipo'=>'multiselect', 'itens'=>Operadora::get()->pluck('nome','id'), 'colunas'=>4],
				['chave'=>'marcacao', 'rotulo'=>'Marcação', 'tipo'=>'select', 'itens'=>['marcados'=>'Apenas Marcados', 'desmarcados'=>'Apenas Não Marcados'], 'colunas'=>3],
				['chave'=>'pedido', 'rotulo'=>'Pedido(s)', 'tipo'=>'text', 'colunas'=>9, 'detalhes'=>'Caso queira pesquisar por mais de um pedido separe usando vírgula, ponto e vírgula, espaços, etc.'],
			],
			'colunas'			=> [
				// ['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'#','campo'=>'id', 'sort'=>'id'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Pedido','campo'=>'increment_id', 'sort'=>'increment_id'],
				['classe'=>'text-center', 'style'=>'width:120px;', 'rotulo'=>'Segmento','campo'=>'segmento_bullet', 'sort'=>'segmento'],
				['classe'=>'text-center', 'style'=>'width:80px;', 'rotulo'=>'Competência','campo'=>function ($model) {

					if (is_null($model->competencia))
						return '<span class="text-gray">indisponível</span>';

					return $model->competencia->data_string;
				
				}, 'sort'=>'competencia_id'],
				['classe'=>'text-center', 'style'=>'width:70px;', 'rotulo'=>'Operadora','campo'=>function ($model) {return $model->operadora_bullet;},],
				['classe'=>'text-center', 'style'=>'width:80px;', 'rotulo'=>'Valor Devido','campo'=>'valor_string'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Remuneração','campo'=>'remuneracao_value_string'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Churn','campo'=>'churn_value_string'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Comissões','campo'=>'comissoes_value_string'],
				['classe'=>'text-center no-print','style'=> 'width:150px;', 'rotulo'=>'Opções','campo'=>function($model) {

					$actions = [
						'<a class="btn btn-info" href="' . route('contestador.pedido.ver', [$model->id]) . '" title="Ver detalhes desse pedido"><i class="fa fa-eye"></i></a>',
					];

					if ($model->remuneracoes->count() > 0 || $model->churns->count() > 0)
						$actions[] = '<a class="btn btn-default" title="ver receitas para o pedido ' . $model->increment_id . '" href="' . route('contestador.receita.listar', ['pedido' => $model->increment_id]) . '"><i class="fa fa-list"></i></a>';

					if (!is_null($model->competencia) && (\Auth::user()->checkPermissao('contestador.marcacao.operador', 'contestador.admin')))
					{
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.receita.marcar', ['competencia' => $model->competencia->id]) . '" title="Agendar marcação para a competência ' . $model->competencia->data_string . '"><i class="fa fa-bullseye"></i></a>';
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.arquivo.download', ['competencia' => $model->id]) . '" title="Fazer download das receitas do arquivo: ' . $model->nome . '"><i class="fa fa-download"></i></a>';
					}

					// if (\Auth::user()->checkPermissao('contestador.admin'))
					// 	$actions[] = '<a class="btn btn-danger" href="' . route('contestador.ciclo.deletar', [$model->id]) . '" onClick="javascript:return confirm(\'Tem certeza que deseja deletar o ciclo ' . $model->data_string . '?\')"><i class="fa fa-trash"></i></a>';

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function ver (Request $request, $pedido)
	{
		return $pedido = Pedido::findOrFail($pedido);
	}
}