<?php

namespace App\Http\Controllers\Contestador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contestador\Regra;

class RegraController extends Controller
{
	public function marcacao_listar (Request $request)
	{
		$data = Regra::ApenasMarcacao()->orderBy($request->query('sort_by', 'id'), $request->query('sort_order', 'desc'));

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Lista de Regras de Marcação',
			'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Nova Regra', 'url' => route('contestador.regra.marcacao.nova')],
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'ID','campo'=>'id', 'sort'=>'id'],
				['classe'=>'text-center', 'style'=>'width:80px;', 'rotulo'=>'Status','campo'=>'status_bullet', 'sort'=>'status'],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Nome','campo'=>'nome', 'sort'=>'nome'],
				['classe'=>'text-center no-print','style'=> 'width:100px;', 'rotulo'=>'Ações','campo'=>function($model) {

					$actions = [];

					if (\Auth::user()->checkPermissao('contestador.marcacao.operador') || \Auth::user()->checkPermissao('contestador.admin'))
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.receita.marcar', ['regra' => $model->id]) . '" title="Agendar marcação para utilizando a regra: ' . $model->nome . '"><i class="fa fa-bullseye"></i></a>';

					if (\Auth::user()->checkPermissao('contestador.marcacao.editor') || \Auth::user()->checkPermissao('contestador.admin'))
						$actions[] = '<a class="btn btn-warning" href="' . route('contestador.regra.marcacao.editar', [$model->id]) . '" title="Editar marcador: '. $model->nome . '"><i class="fa fa-edit"></i></a>';

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function marcacao_editar (Request $request, $regra = null)
	{
		$regra = Regra::firstOrNew(['id' => $regra]);

		#define as regras de validação do POST
		$regrasValidacao = ['regra' => ['required', function ($attribute, $value, $fail) {

			$value = json_decode($value);

			if (empty(@$value->nome))
				return $fail('A regra precisa ter um nome');

			if (empty(@$value->configuracoes->query))
				return $fail('Você precisa escolher uma query base para as suas configurações');

			if (!is_array(@$value->configuracoes->comparacoes) || count($value->configuracoes->comparacoes) <= 0)
				return $fail('Configurações inválidas');

			$query = $value->configuracoes->query;

			foreach ($value->configuracoes->comparacoes as $index => $comparacao)
			{
				$errors = $query::ValidateConfiguracao($comparacao, ++$index);

				if ($errors !== false)
					return $fail('Comparação #' . $index . ' os seguintes erros de configuração: '. implode(', ', $errors) . '.');
			}
			
		}]];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$postData = json_decode($request->input('regra'));

			$regra->tipo			= Regra::TIPO_MARCACAO;
			$regra->status			= $postData->status === true ? 1 : 0;
			$regra->nome			= $postData->nome;
			$regra->configuracoes	= $postData->configuracoes;

			$regra->save();

			return redirect()->route('contestador.regra.marcacao.listar')->with(['message' => 'Regra de Marcação <strong>' . $regra->nome . '</strong> salva com sucesso']);
		}

		if (!$regra->exists)
			$regra->popularBaseMarcacao();

		return view('contestador.regra_marcacao_editar', [
			'regra'			=> $regra,
			'queries'		=> \App\Helpers\Utils::CriarChaves(function ($item) {return $item->tipo;}, [
				\App\Models\Contestador\Queries\SalesFlatOrderLoteConsumer::getInfo(),
				\App\Models\Contestador\Queries\SalesFlatOrderPadraoConsumer::getInfo(),
			]),
		]);
	}

	public function marcacao_deletar ()
	{
		return 'marcacao_deletar';
	}

	public function apuracaoreceita_listar ()
	{
		return 'apuracaoreceita_listar';
	}

	public function apuracaoreceita_editar ()
	{
		return 'apuracaoreceita_editar';
	}

	public function apuracaoreceita_deletar ()
	{
		return 'apuracaoreceita_deletar';
	}
}