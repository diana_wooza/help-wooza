<?php

namespace App\Http\Controllers\Contestador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contestador\Arquivo;
use App\Models\Contestador\Competencia;
use App\Models\Contestador\Pedido;
use App\Models\Contestador\Processo;
use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;
use App\Models\Operadora;

class ReceitaController extends Controller
{
	public function listar (Request $request)
	{
		ini_set('max_execution_time', 120);
		ini_set('memory_limit', '1G');

		$data = Receita::with(['competencia','operadora','pedido','arquivo'])
			->comFiltro($request)
			->comOrdenacao($request)
		;

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Lista de Receitas',
			'filtros'			=> [
				['chave'=>'tipo', 'rotulo'=>'Tipo', 'tipo'=>'select', 'itens'=>Receita::$tipos, 'colunas'=>2],
				['chave'=>'operadora', 'rotulo'=>'Operadora', 'tipo'=>'multiselect', 'itens'=>Operadora::get()->pluck('nome','id'), 'colunas'=>4],
				['chave'=>'competencia', 'rotulo'=>'Competências', 'tipo'=>'multiselect', 'itens'=>Competencia::orderBy('data', 'DESC')->get()->pluck('data_string','id'), 'colunas'=>6],
				['chave'=>'arquivo', 'rotulo'=>'Arquivos', 'tipo'=>'multiselect', 'itens'=>Arquivo::orderBy('nome', 'ASC')->get()->pluck('nome','id'), 'colunas'=>3],
				['chave'=>'marcacao', 'rotulo'=>'Marcação', 'tipo'=>'select', 'itens'=>['marcados'=>'Apenas Marcados', 'desmarcados'=>'Apenas Não Marcados'], 'colunas'=>3],
				['chave'=>'pedido', 'rotulo'=>'Pedido(s)', 'tipo'=>'text', 'colunas'=>6, 'detalhes'=>'Caso queira pesquisar por mais de um pedido separe usando vírgula, ponto e vírgula, espaços, etc.'],
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:80px;', 'rotulo'=>'Tipo','campo'=>'tipo_bullet', 'sort'=>'tipo'],
				['classe'=>'text-center', 'style'=>'width:70px;', 'rotulo'=>'Operadora','campo'=>function ($model) {return @$model->operadora->bullet;},],
				['classe'=>'text-center', 'style'=>'width:70px;', 'rotulo'=>'Competência','campo'=>function ($model) {return @$model->competencia->data_string;}, 'sort'=>'ciclo_id'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Pedido','campo'=>'pedido_string'],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Dados','campo'=>function ($model) {
					
					$dados = [
						'<strong>Terminal:</strong> ' . $model->terminal_string,
						'<strong>Protocolo:</strong> ' . $model->protocolo_string,
						'<strong>Documento:</strong> ' . $model->documento_string,
						'<strong>Data do Serviço:</strong> ' . $model->data_string,
					];

					return implode('<br />', $dados);
				}],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Detalhes','campo'=>function ($model) {
					
					$dados = [
						'<strong>Plano Adquirido:</strong> ' . $model->servico_string,
						'<strong>Motivo da Receita:</strong> ' . $model->motivo_string,
						'<strong>Detalhe Adicional:</strong> ' . $model->detalhe_string,
					];

					return implode('<br />', $dados);
				}],
				// ['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'Terminal','campo'=>'terminal_string'],
				// ['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'Protocolo','campo'=>'protocolo_string'],
				// ['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'Documento','campo'=>'documento_string'],
				// ['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'Data do Serviço','campo'=>'data_string'],
				['classe'=>'text-right', 'style'=>'width:80px;', 'rotulo'=>'Valor','campo'=>'valor_formatado'],
				['classe'=>'text-center no-print','style'=> 'width:150px;', 'rotulo'=>'Opções','campo'=>function($model) {

					$actions = [
						'<a class="btn btn-info" href="' . route('contestador.receita.ver', [$model->id]) . '" title="Ver detalhes dessa receita"><i class="fa fa-eye"></i></a>',
					];

					if (\Auth::user()->checkPermissao('contestador.marcacao.operador', 'contestador.admin'))
					{
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.receita.marcar', ['arquivo' => $model->arquivo->id, 'competencia' => $model->competencia->id]) . '" title="Agendar marcação para o arquivo ' . $model->arquivo->nome . ' na competência ' . $model->competencia->data_string . '"><i class="fa fa-bullseye"></i></a>';
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.arquivo.download', ['arquivo' => $model->arquivo->id, 'competencia' => $model->competencia->id]) . '" title="Fazer download das receitas do arquivo ' . $model->arquivo->nome . ' na competência ' . $model->competencia->data_string . '"><i class="fa fa-download"></i></a>';
					}

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function ver (Request $request, $receita)
	{
		return $receita = Receita::findOrFail($receita);
	}

	public function desmarcar_manual (Request $request, $receita)
	{
		$receita = Receita::find($receita);
	}

	public function marcar_manual (Request $request, $receita, $pedido)
	{
		$receita = Receita::find($receita);
		$pedido = Pedido::find($pedido);

		// $marcacaoAutomatica = json_encode(['score'=>null, 'regras'=>[['tipo'=>'automatica', 'configuracoes'=>['processo' => $this->processo->id]]]]);
	}

	public function marcar_regra (Request $request)
	{
		$regrasValidacao = [
			'arquivo'		=> ['required', function ($attribute, $value, $fail) {

				if (is_null(Arquivo::find($value)))
					$fail('O arquivo especificado não foi encontrado');

			}],
			'competencia'	=> ['required', function ($attribute, $value, $fail) {

				if (is_null(Competencia::find($value)))
					$fail('A competência especificada não foi encontrado');

			}],
			'regra'			=> ['required', function ($attribute, $value, $fail) {

				if (is_null(Regra::find($value)))
					$fail('A regra especificada não foi encontrado');

			}],
		];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$arquivo = Arquivo::findOrFail($request->input('arquivo'));
			$competencia = Competencia::findOrFail($request->input('competencia'));
			$regra = Regra::findOrFail($request->input('regra'));

			$processo = new Processo();
			
			$processo->arquivo()->associate($arquivo);
			$processo->competencia()->associate($competencia);
			$processo->usuario()->associate(\Auth::user());
			$processo->tipo = Processo::TIPO_MARCACAO;
			$processo->configuracao = (object) [
				'regra'		=> $regra->id,
				'exportar'	=> $request->input('exportar') == 'sim',
			];
			
			$processo->save();
			$processo->Dispatch();

			return redirect()->route('contestador.receita.listar')->with(['message' => 'Marcação do arquivo <strong>' . $arquivo->nome . '</strong> na competência <strong>' . $competencia->data_string . '</strong> utilizando a regra <strong>' . $regra->nome . '</strong> foi solicitada com sucesso!']);
		}

		return view('contestador.receita_marcar', [
			'arquivos'		=> Arquivo::get(),
			'competencias'	=> Competencia::get(),
			'regras'		=> Regra::get(),
			'defaults'		=> $request->query(),
		]);
	}
}