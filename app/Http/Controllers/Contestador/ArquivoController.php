<?php

namespace App\Http\Controllers\Contestador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Operadora;
use App\Models\Contestador\Arquivo;
use App\Models\Contestador\Competencia;
use App\Models\Contestador\Processo;
use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;
use App\Jobs\Contestador\InsertReceitaCompetenciaFromFile as Job;

class ArquivoController extends Controller
{
	public function listar (Request $request)
	{
		$arquivos = Arquivo::with(['operadora'])->orderBy($request->query('sort_by', 'id'), $request->query('sort_order', 'desc'));

		$actions = [];

		if (\Auth::user()->checkPermissao('contestador.marcacao.editor'))
			$actions[] = ['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Novo Arquivo', 'url' => route('contestador.arquivo.novo')];

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Lista de Arquivos Para Upload',
			'actions'			=> $actions,
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'ID','campo'=>'id', 'sort'=>'id'],
				['classe'=>'text-center', 'style'=>'width:120px;', 'rotulo'=>'Status','campo'=>'status_bullet',],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Operadora','campo'=>function ($model) {return $model->operadora->bullet;}],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Nome','campo'=>'nome', 'sort'=>'nome'],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'Armazenamento Padrão','campo'=>'armazenamento_string'],
				['classe'=>'text-center no-print','style'=> 'width:250px;', 'rotulo'=>'Opções','campo'=>function($model) {

					$actions = [
						'<a class="btn btn-default" title="ver receitas para o arquivo ' . $model->nome . '" href="' . route('contestador.receita.listar', ['arquivo[]' => $model->id]) . '"><i class="fa fa-list"></i></a>'
					];

					if ($model->pode_fazer_upload)
						$actions[] = '<a class="btn btn-primary" href="' . route('contestador.arquivo.upload', [$model->id]) . '"><i class="fa fa-cloud-upload"></i></a>';

					if (\Auth::user()->checkPermissao('contestador.marcacao.operador','contestador.admin'))
					{
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.receita.marcar', ['arquivo' => $model->id]) . '" title="Agendar marcação o arquivo: ' . $model->nome . '"><i class="fa fa-bullseye"></i></a>';
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.arquivo.download', ['arquivo' => $model->id]) . '" title="Fazer download das receitas do arquivo: ' . $model->nome . '"><i class="fa fa-download"></i></a>';
						$actions[] = '<a class="btn btn-warning" href="' . route('contestador.arquivo.editar', [$model->id]) . '"><i class="fa fa-edit"></i></a>';
					}
					
					if (\Auth::user()->checkPermissao('contestador.marcacao.admin'))
						$actions[] = '<a class="btn btn-danger" href="' . route('contestador.arquivo.deletar', [$model->id]) . '" onClick="javascript:return confirm(\'Tem certeza que deseja deletar o arquivo ' . $model->nome . '?\')"><i class="fa fa-trash"></i></a>';
					
					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $arquivos->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function upload (Request $request, $arquivo = null)
	{
		#define as regras de validação do POST
		$validacao = [
			'arquivo'		=> ['required','integer', function ($field, $value, $fail) {

				$arquivo = Arquivo::find($value);
				
				if (!$arquivo)
					return $fail('Tipo de Arquivo Inválido');

				if (!$arquivo->pode_fazer_upload)
					return $fail('Uploads do arquivo ' . $arquivo->nome . ' estão desativados no momento');

			}],
			'csv'			=> 'required|file',
			'competencia'	=> ['required','integer', function ($field, $value, $fail) {

				if (!Competencia::where('id', $value)->exists())
					return $fail('Competência não encontrada');

			}],
			'estrategia'	=> 'required',
			'exportar'		=> [function ($field, $value, $fail) use ($request) {

				if ($value == 'sim' && $request->input('marcar') != 'sim')
					return $fail('A opção de exportação não pode estar ativa caso a opção de marcação também não esteja ativa');

			}],
		];

		if ($request->isMethod('post') && $request->validate($validacao))
		{
			$arquivo = Arquivo::findOrFail($request->input('arquivo'));
			$competencia = Competencia::findOrFail($request->input('competencia'));

			$processo = new Processo();

			$processo->competencia()->associate($competencia);
			$processo->arquivo()->associate($arquivo);
			$processo->usuario()->associate(\Auth::user());
			$processo->tipo = Processo::TIPO_UPLOAD;
			$processo->configuracao = (object) [
				'caminho'				=> $request->file('csv')->store(null, 'contestador_arquivos'),
				'estrategia'			=> $request->input('estrategia'),
				'filename'				=> $request->file('csv')->getClientOriginalName(),
				'marcar'				=> $request->input('marcar') == 'sim',
				'exportar'				=> $request->input('exportar') == 'sim',
			];
			$processo->save();
			$processo->Dispatch();

			return redirect()->route('contestador.arquivo.listar')->with(['message' => 'Upload do arquivo <strong>' . $processo->configuracao->filename . '</strong> realizado com sucesso!']);
		}

		return view('contestador.arquivo_upload', [
			'tipoArquivo'			=> $arquivo,
			'arquivos'				=> Arquivo::ativos()->orderBy('nome','asc')->get(),
			'competencias'			=> Competencia::orderBy('data', 'DESC')->get(),
			#'formats'				=> 'application/vnd.ms-excel,.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'formats'				=> '.csv',
			'estrategias'			=> Processo::GetEstrategiasUploadList(),
		]);
	}

	public function download (Request $request)
	{
		$regrasValidacao = [
			'arquivo'		=> ['required', function ($attribute, $value, $fail) {

				if (is_null(Arquivo::find($value)))
					$fail('O arquivo especificado não foi encontrado');

			}],
			'competencia'	=> ['required', function ($attribute, $value, $fail) use ($request) {

				if (is_null($competencia = Competencia::find($value)))
					$fail('A competência especificada não foi encontrado');

				$arquivo = Arquivo::find($request->input('arquivo'));

				if (Receita::porCompetencia($competencia)->porArquivo($arquivo)->count() <= 0)
					$fail('Não existem registros com essa configuração para serem exportados');

			}],
		];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$arquivo = Arquivo::findOrFail($request->input('arquivo'));
			$competencia = Competencia::findOrFail($request->input('competencia'));

			$processo = new Processo();
			
			$processo->arquivo()->associate($arquivo);
			$processo->competencia()->associate($competencia);
			$processo->usuario()->associate(\Auth::user());
			
			$processo->tipo = Processo::TIPO_DOWNLOAD;
			$processo->setConfiguracao('arquivo', null);
			
			$processo->save();
			$processo->Dispatch();

			return redirect()->route('contestador.receita.listar')->with(['message' => 'Download de receitas do arquivo <strong>' . $arquivo->nome . '</strong> na competência <strong>' . $competencia->data_string . '</strong> foi solicitada com sucesso!']);
		}

		return view('contestador.arquivo_download', [
			'arquivos'		=> Arquivo::get(),
			'competencias'	=> Competencia::get(),
			'defaults'		=> $request->query(),
		]);
	}

	public function editar (Request $request, $arquivo = null)
	{
		$arquivo = Arquivo::firstOrNew(['id' => $arquivo]);

		#define as regras de validação do POST
		$regrasValidacao = ['arquivo' => ['required', function ($attribute, $value, $fail) {

			$postData = json_decode($value);

			if (empty($postData->operadora_id))
				return $fail('É necessário especificar uma operadora');

			if (empty($postData->nome))
				return $fail('O nome do arquivo não pode estar em branco');

			if (!is_array($postData->tratamentos))
				return $fail('Lista inválida de tratamentos');

			if (!is_array($postData->regras))
				return $fail('Lista inválida de regras de marcação');

			if (!isset($postData->configuracao))
				return $fail('Erro inválido ao gerar as configurações');

			if (!isset($postData->configuracao->armazenamento) || !isset(Arquivo::$armazenamentos[$postData->configuracao->armazenamento]))
				return $fail('Regra de armazenamento não detectada ou inválida');
			
		}]];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$postData = json_decode($request->input('arquivo'));

			$arquivo->status		= $postData->status;
			$arquivo->operadora_id	= $postData->operadora_id;
			$arquivo->nome			= $postData->nome;
			$arquivo->tratamentos	= $postData->tratamentos;
			$arquivo->regras		= $postData->regras;
			$arquivo->configuracao	= $postData->configuracao;

			$arquivo->save();

			return redirect()->route('contestador.arquivo.listar')->with(['message' => 'Arquivo <strong>' . $arquivo->nome . '</strong> salvo com sucesso']);
		}

		return view('contestador.arquivo_editar', [
			'arquivo'		=> $arquivo,
			'tratamentos'	=> \App\Helpers\Utils::CriarChaves(function ($item) {return $item->tipo;}, [
				\App\Helpers\Arquivos\Tratamentos\ColunaExiste::GetInfo(),
				\App\Helpers\Arquivos\Tratamentos\ConverterData::GetInfo(),
				\App\Helpers\Arquivos\Tratamentos\ConverterNumero::GetInfo(),
				\App\Helpers\Arquivos\Tratamentos\SanitizarColuna::GetInfo(),
				\App\Helpers\Arquivos\Tratamentos\NotacaoNumerica::GetInfo(),
				\App\Helpers\Arquivos\Tratamentos\PreencherZeros::GetInfo(),
				\App\Helpers\Arquivos\Tratamentos\RenomearColuna::GetInfo(),
			]),
			'regras'		=> Regra::get()->keyBy('id'),
			'operadoras'	=> Operadora::get()->keyBy('id'),
			'armazenamentos'=> Arquivo::$armazenamentos,
		]);
	}

	public function deletar (Request $request, $arquivo)
	{
		$arquivo = Arquivo::findOrFail($arquivo);
		$arquivo->delete();

		return redirect()->route('contestador.arquivo.listar')->with(['message' => 'Arquivo <strong>' . $arquivo->nome . '</strong> foi excluído com sucesso!']);
	}
}