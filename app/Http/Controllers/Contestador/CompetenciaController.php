<?php

namespace App\Http\Controllers\Contestador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contestador\Competencia;
use App\Jobs\Contestador\BuscarPedidosCompetenciaConsumer;
use App\Jobs\Contestador\BuscarPedidosCompetenciaCorp;

class CompetenciaController extends Controller
{
	public function listar (Request $request)
	{
		$data = Competencia::withCount(['pedidos','receitas'])->orderBy($request->query('sort_by', 'id'), $request->query('sort_order', 'desc'));

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Competências',
			'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Nova Competência', 'url' => route('contestador.competencia.novo'), 'onclick'=>'javascript:return confirm(\'Tem certeza que deseja adicionar uma nova competência?\')'],
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'ID','campo'=>'id', 'sort'=>'id'],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Período','campo'=>'data_string', 'sort'=>'data'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Pedidos','campo'=>'pedidos_count_formatado'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Receitas','campo'=>'receitas_count_formatado'],
				['classe'=>'text-center no-print','style'=> 'width:250px;', 'rotulo'=>'Detalhes','campo'=>function($model) {

					$actions = [
						'<a class="btn btn-default" title="ver pedidos para esta competência ' . $model->data_string . '" href="' . route('contestador.pedido.listar', ['competencia[]' => $model->id]) . '"><i class="fa fa-list"></i></a>',
						'<a class="btn btn-info" title="ver receitas para esta competência ' . $model->data_string . '" href="' . route('contestador.receita.listar', ['competencia[]' => $model->id]) . '"><i class="fa fa-list"></i></a>',
					];

					if (\Auth::user()->checkPermissao('contestador.marcacao.operador') || \Auth::user()->checkPermissao('contestador.admin'))
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.receita.marcar', ['competencia' => $model->id]) . '" title="Agendar marcação utilizando a competência: ' . $model->data_string . '"><i class="fa fa-bullseye"></i></a>';

					if (\Auth::user()->checkPermissao('contestador.marcacao.operador','contestador.admin'))
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.arquivo.download', ['competencia' => $model->id]) . '" title="Fazer download das receitas do arquivo: ' . $model->nome . '"><i class="fa fa-download"></i></a>';

					if (\Auth::user()->checkPermissao('contestador.admin'))
						$actions[] = '<a class="btn btn-danger" href="' . route('contestador.competencia.reprocessar', [$model->id]) . '" title="reprocessar todos os pedidos da competência ' . $model->data_string . '" onClick="javascript:return confirm(\'Tem certeza que deseja reprocessar todos os pedidos da competência ' . $model->data_string . '?\')"><i class="fa fa-cogs"></i></a>';

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 5)),
		]);
	}

	public function novo ()
	{
		$ultimaCompetencia = Competencia::orderBy('data', 'desc')->first();

		$mes = $ultimaCompetencia->mes + 1;
		$ano = $ultimaCompetencia->ano;

		if ($mes > 12)
		{
			$mes = 1;
			$ano++;
		}

		$novaCompetencia = new Competencia();

		$novaCompetencia->mes = $mes;
		$novaCompetencia->ano = $ano;

		$novaCompetencia->save();

		BuscarPedidosCompetenciaConsumer::dispatch($novaCompetencia)->onConnection('contestador');
		BuscarPedidosCompetenciaCorp::dispatch($novaCompetencia)->onConnection('contestador');

		return redirect()->route('contestador.competencia.listar')->with(['message' => 'Nova Competência <strong>' . $novaCompetencia->data_string . '</strong> criada com sucesso!']);
	}

	public function reprocessar ($competencia)
	{
		$competencia = Competencia::findOrFail($competencia);

		BuscarPedidosCompetenciaConsumer::dispatch($competencia)
			->onConnection('contestador')
			->onQueue('general')
		;

		BuscarPedidosCompetenciaCorp::dispatch($competencia)
			->onConnection('contestador')
			->onQueue('general')
		;

		return redirect()->route('contestador.competencia.listar')->with(['message' => 'A competência <strong>' . $competencia->data_string . '</strong> foi agendada para reprocessamento de pedidos com sucesso!']);
	}

	// public function editar (Request $request, $competencia = null)
	// {
	// 	$competencia = Competencia::firstOrNew(['id'=>$competencia]);

	// 	return $competencia;
	// }

	// public function deletar ($competencia)
	// {
	// 	$competencia = Competencia::findOrFail($competencia);
	// 	$competencia->delete();

	// 	return redirect()->route('contestador.competencia.listar')->with(['message' => 'A competência <strong>' . $competencia->data . '</strong> foi deletada com sucesso!']);
	// }
}