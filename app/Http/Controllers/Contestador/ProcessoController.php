<?php

namespace App\Http\Controllers\Contestador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\Contestador\Arquivo;
use App\Models\Contestador\Processo;

class ProcessoController extends Controller
{
	public function listar (Request $request)
	{
		ini_set('max_execution_time', 120);
		ini_set('memory_limit', '1G');

		$data = Processo::with(['competencia','arquivo','arquivo.operadora'])
			->comFiltro($request)
			->comOrdenacao($request)
		;

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Lista de Processos',
			'filtros'			=> [
				['chave'=>'arquivo', 'rotulo'=>'Arquivos', 'tipo'=>'multiselect', 'itens'=>Arquivo::orderBy('nome', 'ASC')->get()->pluck('nome','id'), 'colunas'=>3],
				['chave'=>'tipo', 'rotulo'=>'Tipo de Processo', 'tipo'=>'select', 'itens'=> Processo::$tipos, 'colunas'=>3],
				['chave'=>'status', 'rotulo'=>'Status do Processo', 'tipo'=>'select', 'itens'=> ['pendente' => 'Processos Pendentes', 'andamento' => 'Processo em Andamento', 'completo' => 'Processo Completo'], 'colunas'=>3],
			],
			'configuracao'		=> [
				'row_style'			=> function ($model) {return $model->is_expired ? 'background:rgba(255,100,100,0.1);' : '';},
			],
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:200px;', 'rotulo'=>'Arquivo','campo'=>'full_name',],
				['classe'=>'text-center', 'style'=>'width:80px;', 'rotulo'=>'Operadora','campo'=>function ($model) {return $model->arquivo->operadora->bullet;}],
				['classe'=>'text-center', 'style'=>'width:80px;', 'rotulo'=>'Status','campo'=>'status_bullet'],
				['classe'=>'text-left', 'style'=>'width:250px;', 'rotulo'=>'Configurações','campo'=>'configuracao_string'],
				['classe'=>'text-left', 'style'=>'width:auto; max-width:400px;', 'rotulo'=>'Log','campo'=> function ($model) {
					return '<div style="width:100%; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;" title="' . $model->resultado_string_stripped . '">' . $model->resultado_formatado_string . '</div>';
				}],
				['classe'=>'text-center no-print','style'=> 'width:100px;', 'rotulo'=>'Detalhes','campo'=>function($model) {

					$actions = [];

					if (\Auth::user()->checkPermissao('contestador.marcacao.operador','contestador.admin') && $model->tipo == Processo::TIPO_DOWNLOAD && $model->is_status_concluido)
						$actions[] = '<a class="btn btn-info" href="' . Storage::disk('contestador_arquivos')->url(@$model->configuracao->arquivo) . '" title="Fazer download das deste arquivo"><i class="fa fa-download"></i></a>';

					if (\Auth::user()->checkPermissao('contestador.marcacao.operador','contestador.admin') && $model->tipo == Processo::TIPO_UPLOAD)
						$actions[] = '<a class="btn btn-success" href="' . route('contestador.arquivo.download', ['arquivo' => $model->arquivo->id, 'competencia' => $model->competencia->id]) . '" title="Fazer download das receitas deste arquivo"><i class="fa fa-download"></i></a>';

					if (\Auth::user()->checkPermissao('contestador.admin') && !$model->is_expired)
						$actions[] = '<a class="btn btn-danger" href="' . route('contestador.processo.reprocessar', [$model->id]) . '" title="reprocessar o item ' . $model->label . '" onClick="javascript:return confirm(\'Tem certeza que deseja reprocessar ' . $model->label . '?\')"><i class="fa fa-cogs"></i></a>';

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function reprocessar ($processo)
	{
		$processo = Processo::findOrFail($processo);

		if ($processo->is_expired)
			return redirect()->back()->withError('Este item já expirou a data de reprocessamento.');

		$processo->Dispatch();

		return redirect()->back()->with(['message' => 'O item ' . $processo->label . ' foi recolocado na fila de reprocessamento com sucesso!']);
	}

	public function download ($processo)
	{
		$processo = Processo::findOrFail($processo);

		if ($processo->is_expired)
			return abort(403, 'Este item já expirou e não pode ser mais baixado');

		try
		{
			Storage::disk('contestador_arquivos')->download($processo->Filename, $processo->download_filename);
		}
		catch (\League\Flysystem\FileNotFoundException $exception)
		{
			return abort(403, 'Não foi possível encontrar o arquivo para download');
		}
		catch (\Throwable $throwable)
		{
			return abort(403, 'Ocorreu um erro ao processar sua requisição. Tente novamente mais tarde');
		}

		return abort(404);
	}
}