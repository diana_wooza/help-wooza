<?php

namespace App\Http\Controllers\Contestador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contestador\Atributo;

class AtributoController extends Controller
{
	public function listar (Request $request)
	{
		$data = Atributo::orderBy($request->query('sort_by', 'id'), $request->query('sort_order', 'desc'));

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Atributos para Verificação',
			'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Novo Atributo', 'url' => route('contestador.atributo.novo')],
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'ID','campo'=>'id', 'sort'=>'id'],
				['classe'=>'text-center', 'style'=>'width:80px;', 'rotulo'=>'Status','campo'=>'status_bullet', 'sort'=>'status'],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Nome','campo'=>'nome', 'sort'=>'nome'],
				['classe'=>'text-left', 'style'=>'width:200px;', 'rotulo'=>'Configuração','campo'=>'tipo'],
				['classe'=>'text-center no-print','style'=> 'width:100px;', 'rotulo'=>'Detalhes','campo'=>function($model) {

					$actions = [
						'<a class="btn btn-warning" href="' . route('contestador.atributo.editar', [$model->id]) . '" ><i class="fa fa-edit"></i></a>',
					];

					if (\Auth::user()->checkPermissao('contestador.admin'))
						$actions[] = '<a class="btn btn-danger" href="' . route('contestador.atributo.deletar', [$model->id]) . '" onClick="javascript:return confirm(\'Tem certeza que deseja deletar o atributo ' . $model->nome . '?\')"><i class="fa fa-trash"></i></a>';

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function editar (Request $request, $atributo = null)
	{
		$atributo = Atributo::firstOrNew(['id'=>$atributo]);

		return $atributo;
	}

	public function deletar ($atributo)
	{
		$atributo = Atributo::findOrFail($atributo);
		$atributo->delete();

		return redirect()->route('contestador.atributo.listar')->with(['message' => 'O atributo <strong>' . $atributo->data . '</strong> foi deletado com sucesso!']);
	}
}