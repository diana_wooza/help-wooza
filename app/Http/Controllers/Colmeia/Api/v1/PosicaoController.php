<?php

namespace App\Http\Controllers\Colmeia\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Colmeia\Posicao;
use \App\Models\Colmeia\Log\Produto as LogProduto;
use Illuminate\Support\Facades\DB;

class PosicaoController extends Controller
{
	public function reservar (Request $request, $pedido, $skus, $serial = null)
	{
		// \Log::info("============================INICIO=============================");
		// \Log::info(is_null($serial) ? "========= Chamada normal ============" : "========= Chamada com retorno ============");
		// \Log::info("url - /chamada-chip-colmeia-pedido/{$pedido}/{$sku}" . (!is_null($serial) ? "/{serial}" : ""));

		$resposta = ['Serial'=>'', 'SKU'=>'', 'Armario'=>'', 'Gaveta'=>'', 'Posicao'=>'', 'Valor_Unitario'=>10, 'ean' => null, 'sku_allied' => null, 'id_jund' => null];
		$skus = array_map(function ($item) {return str_replace('sku[]=', '', $item);}, explode("&sku[]=", $skus));

		// DB::connection('colmeia')->raw('LOCK TABLES armario_posicao READ;');

		do
		{
			$posicao = Posicao
				::with(['entidade_produto','entidade_produto.jundsoft'])
				->disponivelParaAlocacao()
				->whereIn('sku', $skus)
				->first()
			;

			if (empty($posicao))
				break;
		}
		while (!$posicao->reservarParaPedidoSeguro($pedido));

		// DB::connection('colmeia')->raw('UNLOCK TABLES;');

		if (!empty($posicao))
		{
			$resposta['Serial']		= $posicao->serial;
			$resposta['SKU']		= $posicao->sku;
			$resposta['Armario']	= $posicao->armario;
			$resposta['Gaveta']		= $posicao->gaveta;
			$resposta['Posicao']	= $posicao->posicao;
			$resposta['ean']		= @$posicao->entidade_produto->jundsoft->ean;
			$resposta['sku_allied']	= @$posicao->entidade_produto->jundsoft->allied_sku;
			$resposta['id_jund']	= @$posicao->entidade_produto->jundsoft->jund_id;
			
			LogProduto::selecao_pedido($posicao->produto, $pedido);
		}
		
		if(!is_null($serial) && !empty($posicaoAntiga = Posicao::where('serial', $serial)->first()))
		{
			$posicaoAntiga->checkDescarteFazLiberacao($request->query('gera_ocorrencia', '1') !== '0');
			$posicaoAntiga->save();
		}

		return $resposta;
	}

	public function desalocar_pedido (Request $request, $pedido)
	{
		return $this->desalocar($request, null, $pedido);
	}

	public function desalocar (Request $request, $serial = null, $pedido = null)
	{
		if (is_null($serial) || is_null($pedido))
		{
			// \Log::alert("Retorna chip com falha não voltou para disponivel com o serial [{$serial}] e pedido [{$pedido}]");
			return ['Return'=>'false'];
		}

		$posicao = Posicao
			::where('serial', $serial)
			->where('pedido', $pedido)
			->first()
		;

		if (empty($posicao))
		{
			// \Log::warning("Retorna chip com falha não voltou para disponivel com o serial [{$serial}] e pedido [{$pedido}]");
			return ['Return'=>'false'];
		}

		$posicao->checkDescarteFazLiberacao();
		$posicao->save();

		// \Log::info("Retorna chip com sucesso para disponível com o serial [{$serial}] e pedido [{$pedido}]");
		return ['Return'=>'true'];
	}
}