<?php

namespace App\Http\Controllers\Colmeia\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Colmeia\Nfe;
use \App\Models\Colmeia\Produto;
use \App\Models\Colmeia\Log\Produto as LogProduto;

class ProdutoController extends Controller
{
	public function busca_sku ($serial)
	{
		$produto = Produto::where('serial', $serial)->first();

		return ['SKU' => @$produto->sku];
	}

	public function liberar_chip ($nota, $pedido, $serial, $sku, $imei = null)
	{
		return $this->_libera_produto($nota, $pedido, $serial, $sku, $imei);
	}

	public function liberar_aparelho ($nota, $pedido, $imei)
	{
		return $this->_libera_produto($nota, $pedido, null, null, $imei);
	}

	private function _libera_produto ($nota, $pedido, $serial, $sku, $imei)
	{
		try
		{
			$nota = Nfe::create(['id'=>$nota, 'tipo'=>Nfe::TIPO_SAIDA, 'nota'=>[
				'pedido'	=> $pedido,
			]]);
		}
		catch (\Exception $exception)
		{
			return ['Return'=>'Número da nota fiscal já existente'];
		}

		if (!is_null($serial) && !is_null($sku) && empty($chip = Produto::where('serial', $serial)->where('sku', $sku)->first()))
			return ['Return' => 'Este chip não foi encontrado no estoque'];

		if (!is_null($imei) && empty($aparelho = Produto::where('serial', $imei)->first()))
			return ['Return' => 'Este aparelho não foi encontrado no estoque'];

		if (!empty($chip))
		{
			if (empty($posicao = $chip->getPosicao()))
				return ['Return' => 'Este chip não estava alocado na colmeia'];

			$posicao->liberar();
			$posicao->save();

			LogProduto::saida([$chip], $nota);
			
			$chip->retirar();
			$chip->save();
		}

		if (!empty($aparelho))
		{
			LogProduto::saida([$aparelho], $nota);
			
			$aparelho->retirar();
			$aparelho->save();
		}

		return ['Return' => 'Success'];
	}

	public function check_picking ($serial = null, $sku = null)
	{
		$sku = array_map(function ($item) {return str_replace('sku[]=','',$item);}, explode("&",$sku));
		
		return ['return' => Produto
			::where('serial', $serial)
			->whereIn('sku', $sku)
			->count() > 0
		];
	}
	
	public function check_serial_exists ($serial)
	{
		return Produto::bySerial($serial);
	}
}