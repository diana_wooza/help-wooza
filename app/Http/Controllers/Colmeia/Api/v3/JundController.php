<?php

namespace App\Http\Controllers\Colmeia\Api\v3;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Colmeia\Produto;

class JundController extends Controller
{
	public function get_data (Request $request)
	{
		$produto = Produto
			::with('jundsoft')
			->where('serial', $request->input('iccid'))
			->firstOrFail()
		;

		return [
			'iccid'			=> $produto->serial,
			'ean'			=> @$produto->jundsoft->ean,
			'sku_allied'	=> @$produto->jundsoft->allied_sku,
			'id_jund'		=> @$produto->jundsoft->jund_id,
		];
	}

	public function get_data_batch (Request $request)
	{
		$iccids = array_flip($request->input('iccids'));
		$produtos = Produto
			::with('jundsoft')
			->whereIn('serial', $request->input('iccids'))
			->get()
			->keyBy('serial')
		;

		foreach ($iccids as $iccid => &$value)
		{
			if (!isset($produtos[$iccid]))
			{
				$value = null;
				continue;
			}

			$produto = $produtos[$iccid];

			$value = [
				'iccid'			=> $produto->serial,
				'ean'			=> @$produto->jundsoft->ean,
				'sku_allied'	=> @$produto->jundsoft->allied_sku,
				'id_jund'		=> @$produto->jundsoft->jund_id,
			];
		}

		return $iccids;
	}
}