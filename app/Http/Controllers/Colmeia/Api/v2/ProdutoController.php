<?php

namespace App\Http\Controllers\Colmeia\Api\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Colmeia\Produto;
use \App\Models\Colmeia\Log\Produto as LogProduto;
use \App\Models\Colmeia\Log\Nfe as LogNfe;
use \App\Models\User;

class ProdutoController extends Controller
{
	public function detalhe (Request $request)
	{
		$resultado		= [];
		$produto		= Produto::find($request->input('produto'));
		$nota			= LogNfe::find($produto->nfe);
		$historicos		= $produto->historicoCompleto();
		$usuarios		= User::get()->keyBy('id');

		$resultado[]	= [
			'data'			=> date('Y-m-d\TH:i:sP', strtotime(@$nota->data)),
			'mensagem'		=> 'Nota fiscal ' . @$produto->nfe . ' importada pelo usuário <strong>' . @$usuarios[@$nota->usuario]->nome . '</strong>',
		];

		foreach ($historicos as $historico)
		{
			switch ($historico->tipo)
			{
				case LogProduto::TIPO_ENTRADA:

					$mensagem = 'O usuário <strong>' . @$usuarios[@$historico->usuario]->nome . '</strong> deu entrada no estoque';
					break;

				case LogProduto::TIPO_ENTRADA_LOTE:

					$mensagem = 'O usuário <strong>' . @$usuarios[@$historico->usuario]->nome . '</strong> deu entrada no estoque através do processo em LOTE';
					break;

				case LogProduto::TIPO_PRE_ALOCACAO:

					$mensagem = 'O usuário <strong>' . @$usuarios[@$historico->usuario]->nome . '</strong> selecionou este chip para pré-alocação na posição <strong>' . @$historico->informacao->armario . '-' . @$historico->informacao->gaveta . '-' . @$historico->informacao->posicao . '</strong>';
					break;
				
				case LogProduto::TIPO_ALOCACAO:

					$mensagem = 'O usuário <strong>' . @$usuarios[@$historico->usuario]->nome . '</strong> confirmou a alocação na posição <strong>' . @$historico->informacao->armario . '-' . @$historico->informacao->gaveta . '-' . @$historico->informacao->posicao . '</strong>';
					break;

				case LogProduto::TIPO_SELECAO_PEDIDO:

					$mensagem = 'O item foi selecionado para o pedido <strong>' . @$historico->informacao->pedido . '</strong>';
					break;
				
				case LogProduto::TIPO_VOLTA_COLMEIA:

					$mensagem = 'O item foi descartado do pedido e marcado para voltar para a colmeia';
					break;
				
				case LogProduto::TIPO_SAIDA:

					$mensagem = 'O chip foi selecionado e será enviado ao cliente';
					break;

				case LogProduto::TIPO_DEVOLUCAO:

					$mensagem = 'O item foi devolvido ao estoque pelo cliente';
					break;
				
				case LogProduto::TIPO_DESCARTE:

					$mensagem = 'O usuário <strong>' . @$usuarios[@$historico->usuario]->nome . '</strong> marcou este item como descarte';
					break;

					case LogProduto::TIPO_DESALOCAR:

					$mensagem = 'O usuário <strong>' . @$usuarios[@$historico->usuario]->nome . '</strong> desalocou da posição <strong>' . @$historico->informacao->armario . '-' . @$historico->informacao->gaveta . '-' . @$historico->informacao->posicao . '</strong>';
					break;

				case LogProduto::TIPO_INVENTARIO_DESCARTE:

					$mensagem = 'O produto foi descartado em uma atualização de inventário';
					break;

				default:

					$mensagem = 'Evento não catalogado';
			}

			$resultado[] 	= [
				'tipo'			=> @$historico->tipo,
				'data'			=> date('Y-m-d\TH:i:sP', strtotime(@$historico->data)),
				'mensagem'		=> @$mensagem,
			];
		}

		return $resultado;
	}

	public function pode_alocar ($serial)
	{
		return ['serial'=>$serial, 'status' => Produto::podeSerAlocado($serial)];
	}

	public function check_serial (Request $request, $serial = '')
	{
		if ($request->has('serial'))
			$serial = $request->input('serial');

		$status = !empty($serial) ? Produto::bySerial($serial)->count() > 0 : false;
		
		return ['serial'=>$serial, 'status' => $status];
	}
}