<?php

namespace App\Http\Controllers\Colmeia;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \App\Models\Colmeia\Operadora;

class MigracaoController extends Controller
{
	private $connection = 'colmeia_old';

	public function __invoke ()
	{
		echo '<p><a href="' . url('colmeia/migracao/operadoras') . '">Operadoras</a></p>';
		exit;
	}

	public function operadoras ()
	{
		$operadoras = DB::connection($this->connection)->table('Operadora');

		foreach ($operadoras->get() as $item)
		{
			$operadora = Operadora::where(['id' => $item->Codigo])->first();

			if (is_null($operadora))
				$operadora = new Operadora();

			$operadora->id = $item->Codigo;
			$operadora->status = $item->Status_Operadora;
			$operadora->nome = $item->Descricao;
			$operadora->codigo = $item->Bloqueio;

			$operadora->save();
		}

		echo '<p>Operadoras Migradas com sucesso</p>';

		echo '<p><a href="' . url('colmeia/migracao') . '">Voltar</a></p>';
		exit;
	}
}