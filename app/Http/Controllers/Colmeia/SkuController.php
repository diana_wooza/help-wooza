<?php

namespace App\Http\Controllers\Colmeia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Helpers\Sku;

class SkuController extends Controller
{
	public function gerador_regex (Request $request)
	{
		$sku = new Sku();

		if (is_array($request->query('modelo')))
		{
			foreach ($request->query('modelo') as $modelo)
				$sku->addModelo($modelo);
		}

		if (is_array($request->query('banda')))
		{
			foreach ($request->query('banda') as $banda)
				$sku->addBanda($banda);
		}

		if (is_array($request->query('operadora')))
		{
			foreach ($request->query('operadora') as $operadora)
				$sku->addOperadora($operadora);
		}

		if (is_array($request->query('estado')))
		{
			foreach ($request->query('estado') as $estado)
				$sku->addEstado($estado);
		}

		$regex = $sku->toRegex();

		return view('colmeia.sku_geradorregex', [
			'regex'			=> $regex,
			'estados'		=> Sku::$estados,
			'bandas'		=> Sku::$bandas,
			'operadoras'	=> Sku::$operadoras,
			'modelos'		=> Sku::$modelos,
		]);
	}
}