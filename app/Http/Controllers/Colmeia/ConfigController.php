<?php

namespace App\Http\Controllers\Colmeia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \App\Models\Colmeia\Config;

class ConfigController extends Controller
{
	public function geral (Request $request)
	{
		$configs = Config::get();

		if ($request->isMethod('post'))
		{
			foreach ($configs as $config)
			{
				$config->valor = $config->tratarValorInput($request->input($config['chave']));;
				$config->save();
			}

			return redirect()->route('colmeia.config')->with(['message' => 'Configurações atualizadas com sucesso']);
		}

		return view('_generico.config', [
			'title'			=> 'Gerenciar Configurações da Colmeia',
			'configs'		=> $configs,
			'list'			=> [
				'email_cobertura'		=> Config::getListaUsuariosColmeiaParaSelect(),
				'alocacao_lote_limitar'	=> [
					'nao_limitar'			=> 'Não Limitar',
					'maximo'				=> 'Limitar a quantidade máxima por caixa',
					'exato'					=> 'Limitar a quantidade exata',
				],
			],
		]);
	}
}