<?php

namespace App\Http\Controllers\Colmeia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Helpers\Iccid;
use App\Models\Colmeia\Alocacao;
use App\Models\Colmeia\Config;
use App\Models\Colmeia\Armario;
use App\Models\Colmeia\EntradaLote;
use App\Models\Colmeia\Produto;
use App\Models\Colmeia\Nfe;
use App\Models\Colmeia\Operadora;
use App\Models\Colmeia\Posicao;
use App\Models\Colmeia\Log\Alocacao as LogAlocacao;
use App\Models\Colmeia\Log\Produto as LogProduto;

class ProdutoController extends Controller
{
	public function entrada (Request $request, string $nota, int $index)
	{
		$nota = Nfe::find($nota);

		if (is_null($magentoProduto = $nota->detalhes[$index]->produto))
			return redirect()->route('colmeia.nfe.produto', ['nota' => $nota->id, 'detalhe' => $index]);

		$produtos = Produto::getFromNotaIndex($nota, $index, Config::getValor('entrada_itens'));
		$digitos = Produto::getSerialDigitosPorSku($magentoProduto->sku);

		$validationRules = [
			'serial'	=> [
				'required',
				'array',
				$this->_validationDigitos($digitos),
				$this->_validationProdutosEmEstoque(),
				$this->_validationItensDuplicados()
			],
		];

		if ($request->isMethod('post') && $request->validate($validationRules))
		{
			$seriais = $request->input('serial');
			$operadora = Operadora::getFromSku($magentoProduto->sku);
			$editados = [];
			
			foreach ($seriais as $serial)
			{
				if (empty($serial))
					continue;

				$editado = Produto::AddSerialToBlankItem($nota, $index, $serial, $magentoProduto->sku, $operadora->id);

				if (!is_null($editado))
					$editados[] = $editado;
			}

			LogProduto::entrada($editados);
			
			return redirect()->route('colmeia.produto.entrada', ['nota' => $nota->id, 'detalhe' => $index])->with(['message' => count (array_filter($seriais)) . ' produtos foram inseridos no estoque']);
		}

		return view('colmeia.produto_entrada', [
			'index'			=> $index,
			'nota'			=> $nota,
			'detalhe'		=> $nota->detalhes[$index],
			'produtos'		=> $produtos,
			'digitos'		=> $digitos,
		]);
	}

	public function entrada_lote (Request $request, $nota)
	{
		$nota = Nfe::findOrFail($nota);

		if (count($produtos = $nota->GetListaProdutos()) <= 0)
			return redirect()->back()->withErrors(['A nota não possui nenhum SKU associado ainda']);

		$validationRules = [
			'entradas' => [
				'required',
				'array',
				function ($attribute, $values, $fail) {

					foreach ($values as $index => $value)
					{
						$value = json_decode($value);
						$index++;

						if (empty($value->produto))
							return $fail('Produto inválido em ICCIDs ' . $index);

						if (!Iccid::Verify($value->inicial))
							return $fail('O ICCID Inicial ' . $value->inicial . ' é inválido em ICCIDs ' . $index);

						if (!Iccid::Verify($value->final))
							return $fail('O ICCID Final ' . $value->final . ' é inválido em ICCIDs ' . $index);

						if (!Iccid::IsSequencia($value->inicial, $value->final))
							return $fail('O ICCID Final ' . $value->final . ' não é uma sequência do ICCID inicial ' . $value->inicial . ' em ICCIDs ' . $index);

						$lista = Iccid::Lista($value->inicial, $value->final);

						switch (Config::getValor('alocacao_lote_limitar'))
						{
							case 'maximo':

								if (count($lista) > Config::getValor('alocacao_lote_quantidade'))
									return $fail ('A quantidade de chips cadastradas ultrapassa o limite permitido. Chips cadastrados: ' . count($lista) . '. Limite Permitido: ' . Config::getValor('alocacao_lote_quantidade'));
								
								break;

							case 'exato':

								if (count($lista) != Config::getValor('alocacao_lote_quantidade'))
									return $fail ('A quantidade de chips cadastradas à diferente da quantidade de chips que precisam existir em uma caixa. Chips cadastrados: ' . count($lista) . '. Quantidade Definida: ' . Config::getValor('alocacao_lote_quantidade'));

								break;
						}

						$produtos = Produto::whereIn('serial', $lista)->get();

						if ($produtos->count() > 0)
							return $fail('Os ICCIDs: ' . $produtos->pluck('serial')->implode(', ') . ' já estão cadastrados');
					}
				},
			]
		];

		if ($request->isMethod('post') && $request->validate($validationRules))
		{
			$entradas = \array_map(function ($item) {
				
				$item = \json_decode($item);
				$item->lista = Iccid::Lista($item->inicial, $item->final);

				return $item;
			
			}, $request->input('entradas'));

			$entrada = new EntradaLote(['produtos' => $entradas]);
			$entrada->nfe()->associate($nota);
			$entrada->usuario()->associate(\Auth::user());
			$entrada->save();

			return redirect()->route('colmeia.produto.entrada_lote_validar', [$entrada->id]);
		}

		return view('colmeia.produto_entrada_lote', [
			'nota'				=> $nota,
			'produtos'			=> $produtos,
			'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Entrada em Lote', 'url' => 'javascript:entradaLotePage.addEntrada();'],
			],
		]);
	}

	public function entrada_lote_validar (Request $request, $entrada)
	{
		$entrada = EntradaLote::with('nfe')->findOrFail($entrada);
		$validationRules = ['validacao' => ['required']];

		if ($request->isMethod('post') && $request->validate($validationRules))
		{
			$totalProdutos = 0;

			foreach ($entrada->produtos as $produto)
			{
				$operadora = Operadora::getFromSku($produto->produto->sku);
				$editados = [];

				foreach ($produto->lista as $iccid)
				{
					$editado = Produto::AddSerialToBlankItem($entrada->nfe, $produto->produto->index, $iccid, $produto->produto->sku, $operadora->id);

					if (!is_null($editado))
						$editados[] = $editado;
				}

				$totalProdutos += count($editados);

				LogProduto::entradaLote($editados);
			}

			$entrada->delete();
			
			return redirect()->route('colmeia.nfe.listardetalhe', [$entrada->nfe->id])->with(['message' => $totalProdutos . ' produtos foram inseridos no estoque']);
		}

		return view('colmeia.produto_entrada_lote_validar', [
			'entrada'			=> $entrada,
		]);
	}

	public function chip_alocar (Request $request, $seriais = [])
	{
		if (is_null($seriais = $request->input('serial')))
			$seriais = [];

		$produtos = Produto::with('jundsoft')->DisponivelParaAlocacao()->whereIn('serial', $seriais)->get();
		$limiteAlocacao = Config::getValor('alocacao_limite');

		$validationRules = ['serial' => [
			'required',
			'array',
			$this->_validationItensDuplicados(),
			function ($attribute, $values, $fail) use ($produtos) {

				if (Config::getValor('alocacao_permitir_naointegrada'))
					return;

				foreach ($produtos as $produto)
				{
					if (!$produto->is_integracao_jundsoft_completa)
						$fail('Apenas seriais com EAN cadastrado com os dados do Jundsoft podem ser alocados. O ICCID ' . $produto->serial . ' não pode ser alocado');
				}

			},
			function ($attribute, $values, $fail) use ($produtos) {

				foreach ($produtos as $produto)
				{
					if ($produto->tipo != Produto::TIPO_SIMCARD)
						$fail('Apenas Sim Cards podem ser alocados na colmeia. O produto ' . $produto->serial . ' é um ' . $produto->tipoString);
				}
			},
			function ($attribute, $values, $fail) use ($produtos) {

				if ($produtos->count() < count(array_filter($values)))
					$fail('Não foi possível alocar os seriais: ' . implode(', ', array_diff(array_filter($values), $produtos->all())) . '. O(s) mesmo(s) não possui(em) entrada na colmeia ou já foi(foram) alocado(s).');
			
			},
			function ($attribute, $values, $fail) use ($produtos) {

				if (Config::getValor('alocacao_permitir_operadora'))
					return;

				$operadoras = [];

				foreach ($produtos as $produto)
				{
					if (!in_array($produto->operadora, $operadoras))
						$operadoras[] = $produto->operadora;
				}

				if (count($operadoras) > 1)
					$fail('Você só pode alocar chips de uma mesma operadora');
			},
			function ($attribute, $values, $fail) use ($produtos) {

				if (Config::getValor('alocacao_permitir_multinotas'))
					return;

				$operadoras = [];

				foreach ($produtos as $produto)
				{
					if (!in_array($produto->operadora, $operadoras))
						$operadoras[] = $produto->operadora;
				}

				if (count($operadoras) > 1)
					$fail('Você só pode alocar chips que vieram de uma mesma nota fiscal');
			},
			function ($attribute, $values, $fail) use ($produtos) {

				if (Config::getValor('alocacao_permitir_parcial'))
					return;

				$notas = [];

				foreach ($produtos as $produto)
				{
					if (!in_array($produto->nfe, $notas))
						$notas[] = $produto->nfe;
				}

				$notas = Nfe::find($notas);

				foreach ($notas as $nota)
				{
					if (!$nota->todosProdutosComEntradaEstoque())
						$fail('Você só pode alocar chips em que a nota tenha sido completamente dada entrada em estoque. A nota <a href="' . route('colmeia.nfe.listardetalhe', ['id' => $nota->id]) . '" target="_blank">' . $nota->codigo . '</a> ainda possui produtos pendentes de entrada.');
				}
			},
			function ($attribute, $values, $fail) use ($limiteAlocacao) {

				if (Config::getValor('alocacao_quantidade_minima'))
					return;

				if (count($values) < $limiteAlocacao)
					$fail('Você precisa alocar ' . $limiteAlocacao . ' chips de cada vez. Não é permitido alocar uma quantidade menor.');
			},
		]];

		if ($request->isMethod('post') && $request->validate($validationRules))
		{
			try
			{
				$alocacoes = [];
				$posicoesEncontradas = [];

				foreach ($produtos as $produto)
				{
					$alocacao = (object) [
						'produto'		=> $produto,
						'posicao'		=> Armario::acharPosicao([$produto->operadora], 1, $posicoesEncontradas)->first(),
					];

					if (is_null($alocacao->posicao))
						throw new \Exception("Não estistem posições disponíveis para alocar essa quantidade de chips", 1);

					$posicoesEncontradas[] = $alocacao->posicao->id;
					$alocacoes[$produto->serial] = $alocacao;
				}

				$itensParaLocacao = [];

				foreach ($request->input('serial') as $serial)
				{
					if (is_null($serial))
						continue;

					$alocacao = $alocacoes[$serial];

					$alocacao->produto->status = Produto::STATUS_PRE_ALOCADO;
					$alocacao->produto->save();
					
					$alocacao->posicao->status = Posicao::STATUS_DISPONIVEL;
					$alocacao->posicao->save();

					$itensParaLocacao[] = [
						'produto'	=> $alocacao->produto->id,
						'posicao'	=> $alocacao->posicao->id,
					];
				}

				$alocacao = Alocacao::create([
					'usuario'		=> \Auth::user()->id,
					'tag'			=> Alocacao::getTagProximoNumero(),
					'produtos'		=> $itensParaLocacao,
				]);

				LogProduto::pre_alocacao($alocacoes);

				return redirect()->route('colmeia.produto.alocacao', ['id' => $alocacao->id]);
			}
			catch (\Exception $exception)
			{
				return redirect()->route('colmeia.produto.chip.alocar')->withErrors(['Message' => $exception->getMessage()]);
			}
		}

		return view('colmeia.produto_chipalocar', [
			'seriais'		=> $request->input('serial', @explode(',', @$seriais)),
			'limite'		=> $limiteAlocacao,
		]);
	}

	public function chip_desalocar (Request $request)
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Descarta chip na Colméia',
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'ID','campo'=>'id'],
				['classe'=>'text-center', 'style'=> 'width:100px;', 'rotulo'=>'Status','campo'=>function ($item) {

					switch ($item->status)
					{
						case 1:

							$class = 'label-success';
							break;
							
						case 2:

							$class = 'label-warning';
							break;
						
						default:
						
							$class = 'label-danger';

					}

					return '<span class="label ' . $class . '">' . Armario::$statuses[$item->status] . '</span>';
				}],
				['classe'=>'text-left','style'=> 'width:auto;', 'rotulo'=>'Armário', 'campo'=>'nome'],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Itens para Descarte', 'campo'=>'descartes'],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Ações', 'campo'=>function ($item) {
					return '<a class="btn btn-primary" href="' . route('colmeia.armario.desalocar', ['id'=>$item->id]) . '" onClick="return confirm(\'Tem certeza que deseja descartar os chips do armário: ' . $item->nome . '?\nEssa ação não poderá ser desfeita.\')">Descartar todos os Chips</a>';
				}],
			],
			'dados'				=> Armario::descarte(),
		]);
	}

	public function alocacao (Request $request, $id)
	{
		$alocacao = Alocacao::find($id);
		$seriais = [];

		foreach ($alocacao->alocacoes as $item)
			$seriais[] = $item->produto->serial;

		$validationRules = ['validacao' => [
			'required',
			function ($attribute, $values, $fail) {

				if ($values != 'passou')
					$fail('Ocorreu um erro na validação de dados. Tente novamente');

			},
		]];

		if ($request->isMethod('post') && $request->validate($validationRules))
		{
			$alocacoes = [];

			foreach ($alocacao->alocacoes as $item)
			{
				$item->produto->alocar();
				$item->posicao->alocar($item->produto);
				
				$item->produto->save();
				$item->posicao->save();

				$alocacoes[] = $item;
			}

			$alocacao->status = Alocacao::StATUS_COMPLETO;
			$alocacao->save();

			LogProduto::alocacao($alocacoes);

			return redirect()->route('home');
		}

		return view('colmeia.produto_alocacao', [
			'actions'			=> [
				#['rotulo' => '<i class="fa fa-barcode"></i> &nbsp; Imprimir Etiquetas', 'url' => route('colmeia.produto.etiquetas', ['id' => $alocacao->id]), 'blank' => true],
			],
			'alocacao'		=> $alocacao,
			'seriais'		=> $seriais,
		]);
	}

	public function etiquetas (Request $request, $id)
	{
		$alocacao = Alocacao::find($id);

		return view('colmeia.produto_etiqueta', [
			'alocacao'		=> $alocacao,
		]);
	}

	#TODO: Esse método
	public function devolucao ($id)
	{
		$produto = Produto::find($id);

		if (empty($produto))
			return redirect()->back()->withErrors(['Produto não encontrado']);

		return redirect()->back()->with('message','Produto ' . $produto->serial . ' Atualizado [FAKE]');
	}

	public function lista_descarte ($data)
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Lista de Chips para descarte',
			'colunas'			=> [
				['classe'=>'text-center', 'style'=> 'width:100px;', 'rotulo'=>'Posição','campo'=>function ($item) {

					return $item->informacao->armario . '-' . $item->informacao->gaveta . '-' . $item->informacao->posicao;
				
				}],
				['classe'=>'text-center','style'=> 'width:auto;', 'rotulo'=>'Serial', 'campo'=>function ($item) {

					return $item->informacao->serial;

				}],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Sku', 'campo'=>function ($item) {

					return $item->informacao->sku;

				}],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Pedido', 'campo'=>function ($item) {

					return $item->informacao->pedido;

				}],
			],
			'dados'				=> LogProduto::where('tipo', LogProduto::TIPO_DESCARTE)->where('data', date('Y-m-d H:i:s', $data))->get(),
		]);
	}

	public function descartar ($id)
	{
		$produto = Produto::find($id);
		$posicao = $produto->getPosicao();

		$produto->descartar();

		LogProduto::descartar($produto, $posicao);

		$produto->save();
		$message = 'Produto <strong>' . $produto->serial . '</strong> colocado para descarte com sucesso';

		if (!empty($posicao))
		{
			$message .= ' e liberou a posição ' . $posicao->codigo;

			$posicao->liberar();
			$posicao->save();
		}

		return redirect()->back()->with(['message' => $message]);
	}

	public function desalocar ($id)
	{
		$produto = Produto::find($id);
		$posicao = $produto->getPosicao();

		if (empty($posicao))
			return redirect()->back()->withErrors(['posicao' => 'Produto não encontrado na colmeia. Impossível desalocar']);

		$produto->liberar();
		$posicao->liberar();

		LogProduto::desalocar($produto, $posicao);

		$produto->save();
		$posicao->save();

		return redirect()->back()->with(['message' => 'Produto <strong>' . $produto->serial . '</strong> desalocado com sucesso da posicao <strong>' . $posicao->codigo . '</strong>']);
	}

	/**
	 * Método que retorna uma closure de acordo com as regras de validação normais do laravel
	 * Este método identifica todos os seriais que fogem a esta regra
	 * @method _validationDigitos
	 * @return Closure
	 */
	private function _validationDigitos ($quantidade)
	{
		return function ($attribute, $values, $fail) use ($quantidade) {

			foreach ($values as $value)
			{
				if (!is_null($value) && strlen($value) != $quantidade)
					$fail('Cada serial precisa conter exatamente ' . $quantidade . ' dígitos. O item ' . $value . ' está diferente');
			}
	
		};
	}

	/**
	 * Método que retorna uma closure de acordo com as regras de validação normais do laravel
	 * Este método identifica se alguns dos seriais enviados já constam em estoque (e porventura não podem ser inseridos novamente)
	 * @method _validationProdutosEmEstoque
	 * @return Closure
	 */
	 private function _validationProdutosEmEstoque ()
	{
		return function ($attribute, $values, $fail) {

			$seriais = Produto::getSeriaisInDatabase($values);

			if (count($seriais) > 0)
				$fail('Os seriais ' . implode(', ', array_unique($seriais)) . ' já foram inseridos no estoque');
		
		};
	}

	/**
	 * Método que retorna uma closure de acordo com as regras de validação normais do laravel
	 * Este método identifica todos os itens duplicados
	 * @method _validationItensDuplicados
	 * @return Closure
	 */
	 private function _validationItensDuplicados ()
	{
		return function ($attribute, $values, $fail) {

			$duplicados = [];

			while (count($values) > 0)
			{
				$current = array_pop($values);

				if (!is_null($current) && in_array($current, $values))
					$duplicados[] = $current;
			}

			if (count($duplicados))
				$fail('Seriai(s) ' . implode(', ', array_unique($duplicados)) . ' inserido(s) em duplicidade');

		};
	}
}