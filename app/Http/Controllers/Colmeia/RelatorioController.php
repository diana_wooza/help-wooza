<?php

namespace App\Http\Controllers\Colmeia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Helpers\Magento;
use App\Helpers\Sku;
use App\Models\Colmeia\Alocacao;
use App\Models\Colmeia\Config;
use App\Models\Colmeia\EntradaLote;
use App\Models\Colmeia\Produto;
use App\Models\Colmeia\Posicao;
use App\Models\Colmeia\Nfe;
use App\Models\Colmeia\Operadora;

class RelatorioController extends Controller
{
	public function movimentacoes (Request $request)
	{
		config(['database.connections.colmeia.strict' => false]);

		$data = collect(DB::connection('colmeia')->SELECT("
			SELECT
				p.sku AS sku,
				DATE_FORMAT(lp.data, '%b/%Y') AS data,
				SUM(CASE
					WHEN lp.tipo = 1 THEN 1
					WHEN lp.tipo = 11 THEN 1
					ELSE 0
				END) AS entrada_count,
				SUM(CASE
					WHEN lp.tipo = 6 THEN 1
					ELSE 0
				END) AS saida_count
			FROM
				log_produto lp
				JOIN produto p ON
					p.id = lp.produto
			WHERE
				lp.tipo IN (1,6,11)
				AND lp.data BETWEEN '2019-01-01 00:00:00' AND '2019-12-31 23:59:59'
			GROUP BY
				p.sku,
				DATE_FORMAT(lp.data, '%b/%Y')
			ORDER BY
				lp.data DESC
		"));

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Histórico de Movimentação Chip Wooza',
			// 'filtros'			=> [
			// 	['chave'=>'status', 'rotulo'=>'Status', 'tipo'=>'select', 'itens'=>['concluidos'=>'Apenas Concluídos', 'ambos' => 'Concluídos e Pendentes'], 'colunas'=>4],
			// 	['chave'=>'usuario', 'rotulo'=>'Usuário', 'tipo'=>'multiselect', 'itens'=>Config::getListaUsuariosColmeiaParaSelect(), 'colunas'=>4],
			// 	['chave'=>'nota', 'rotulo'=>'Nota Fiscal', 'tipo'=>'text', 'colunas'=>4, 'detalhes'=>'Caso queira pesquisar por mais de uma nota separe usando vírgula, ponto e vírgula, espaços, etc.'],
			// ],
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'SKU','campo'=>'sku'],
				['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'Mês','campo'=>'data'],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'Entrada','campo'=>'entrada_count'],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'Saída','campo'=>'saida_count'],
			],
			'dados'				=> $data,
		]);
	}

	public function entrada_lote (Request $request)
	{
		$data = EntradaLote::with(['nfe','usuario'])->ComFiltro($request)->ComOrdenacao($request);

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Lista de Entradas em Lote',
			'filtros'			=> [
				['chave'=>'status', 'rotulo'=>'Status', 'tipo'=>'select', 'itens'=>['concluidos'=>'Apenas Concluídos', 'ambos' => 'Concluídos e Pendentes'], 'colunas'=>4],
				['chave'=>'usuario', 'rotulo'=>'Usuário', 'tipo'=>'multiselect', 'itens'=>Config::getListaUsuariosColmeiaParaSelect(), 'colunas'=>4],
				['chave'=>'nota', 'rotulo'=>'Nota Fiscal', 'tipo'=>'text', 'colunas'=>4, 'detalhes'=>'Caso queira pesquisar por mais de uma nota separe usando vírgula, ponto e vírgula, espaços, etc.'],
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'ID','campo'=>'id', 'sort'=>'id'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Status','campo'=>'status_bullet', 'sort'=>'status'],
				['classe'=>'text-center', 'style'=>'width:auto;', 'rotulo'=>'Usuário','campo'=>'usuario_string', 'sort'=>'usuario'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Nota', 'sort'=>'nfe', 'campo'=>function ($model) {
					return $model->nfe->id;
				}],
				['classe'=>'text-left', 'style'=>'width:400px;', 'rotulo'=>'Entradas', 'sort'=>'produto','campo'=>function ($model) {

					return collect($model->produtos)->map(function ($item) {
						
						$texto  = '<strong>' . count($item->lista) . ' Chips</strong>';
						$texto .= ' - ';
						$texto .= $item->produto->sku;
						$texto .= ': ';
						$texto .= $item->produto->name;

						return $texto;
					
					})->implode('<br />');

				}],
				['classe'=>'text-center no-print','style'=> 'width:100px;', 'rotulo'=>'Detalhes','campo'=>function($model) {

					$actions = [];

					if ($model->status == EntradaLote::STATUS_PENDENTE)
						$actions[] = '<a class="btn btn-success" href="' . route('colmeia.produto.entrada_lote_validar', [$model->id]) . '" title="Confirmar alocação em lote"><i class="fa fa-check-circle"></i></a>';

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function nfe_lista (Request $request, $pagina = 1)
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Relatório de Notas Fiscais',
			'filtros'			=> [
				['tipo'=>'multiselect', 'largura'=>3, 'nome'=>'tipo', 'placeholder'=>'Tipo de Nota', 'opcoes'=> Nfe::$tipos, 'valor'=>$request->query('tipo')],
				['tipo'=>'text', 'largura'=>7, 'nome'=>'nota', 'placeholder'=>'Nota Fiscal', 'valor'=>$request->query('nota')],
			],
			'sort_by'			=> $request->query('sort_by', 'id'),
			'sort_order'		=> $request->query('sort_order', 'desc'),
			'urlDaPagina'		=> function ($pagina) use ($request) {
				return route('colmeia.relatorio.nfe.lista', [
					'pagina'		=> $pagina,
					'tipo'			=> $request->query('tipo'),
					'nota'			=> $request->query('nota'),
				]);
			},
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', /*'sort'=>'nota',*/ 'rotulo'=>'NF', 'campo'=>'id'],
				['classe'=>'text-center', 'style'=>'width:100px;', /*'sort'=>'tipo',*/ 'rotulo'=>'Tipo', 'campo'=>'tipoString'],
				['classe'=>'text-center', 'style'=> 'width:200px;', 'rotulo'=>'Chave', 'campo'=>'codigo'],
				['classe'=>'text-center', 'style'=> 'width:auto;', 'rotulo'=>'Fornecedor', 'campo'=>'emissor'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Data Emissão', 'campo'=>'emissao'],
				['classe'=>'text-center no-print','style'=> 'width:10px;', 'rotulo'=>'Detalhes', 'campo'=>function ($item) {

					$modal = view('_generico.modal', [
						'id'		=> 'nfe-detalhe-' . $item->id,
						'titulo'	=> 'Detalhe Nota Fiscal: ' . $item->id,
						'mensagem'	=> view('colmeia.relatorio_nfedetalhe', ['nfe' => $item])->render(),
					])->render();
					$botao = '<button type="button" class="btn btn-primary glyphicon glyphicon-list-alt" data-toggle="modal" data-target="#modal-nfe-detalhe-' . $item->id . '"></button>';

					return $modal . $botao;
				
				}],
			],
			'pagina'			=> $pagina,
			'por_pagina'		=> $this->tratar_paginacao($request->query('por_pagina')),
			'totalPaginas'		=> Nfe::listaNotasItens($this->tratar_paginacao($request->query('por_pagina')), $request->query('tipo'), $request->query('nota')),
			'dados'				=> Nfe::listaNotasPaginada($pagina, $this->tratar_paginacao($request->query('por_pagina')), $request->query('sort_by', 'id'), $request->query('sort_order', 'desc'), $request->query('tipo'), $request->query('nota')),
		]);
	}

	public function produtos_lista (Request $request, $pagina = 1)
	{
		ini_set('max_execution_time', 3000);
		ini_set('memory_limit', '3G');

		$data = Produto::with(['jundsoft','nota'])->emEstoque()->comFiltro($request)->comOrdenacao($request);

		$filtroSku = ['chave'=>'sku', 'rotulo'=>'SKU', 'tipo'=>'text', 'colunas'=>6, 'detalhes'=>'Você pode colocar mais de um SKU separado por vírgula, espaço, etc'];

		if ($request->has('nota') && !empty($request->query('nota')))
		{
			$filtroSku = ['chave'=>'sku', 'rotulo'=>'SKU', 'tipo'=>'multiselect', 'itens'=>Produto::skusParaNotParaSelect($request->query('nota')), 'colunas'=>6];
		}

		return view('colmeia.relatorio_produtolista', [
			'titulo'			=> 'Relatório de Produtos em Estoque',
			'actions'			=> [
				['rotulo' => '<i class="fa fa-barcode"></i> &nbsp; Copiar Seriais', 'url' => 'javascript:void(0);copy_clipboard();'],
			],
			'filtros'			=> [
				['chave'=>'produto', 'rotulo'=>'Produto', 'tipo'=>'text', 'colunas'=>3],
				['chave'=>'status', 'rotulo'=>'Status', 'tipo'=>'multiselect', 'itens'=>Produto::$statuses, 'colunas'=>3],
				['chave'=>'tipo', 'rotulo'=>'Tipo', 'tipo'=>'multiselect', 'itens'=>Produto::$tipos, 'colunas'=>3],
				['chave'=>'nota', 'rotulo'=>'Nota de Entrada', 'tipo'=>'text', 'colunas'=>3],
				['chave'=>'serial', 'rotulo'=>'Serial (ICCID)', 'tipo'=>'text', 'colunas'=>6, 'detalhes'=>'Você pode colocar mais de um ICCID separado por vírgula, espaço, etc'],
				$filtroSku,
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:80px;', 'rotulo'=>'Status','campo'=>'status_bullet', 'sort'=>'status'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Tipo','campo'=>'tipo_string', 'sort'=>'tipo'],
				['classe'=>'text-left', 'style'=> 'width:auto;', 'rotulo'=>'Produto', 'campo'=>'produto_nome_string'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Nota', 'sort'=>'nfe', 'campo'=>'nfe'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'SKU', 'sort'=>'sku', 'campo'=>'sku'],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'EAN', 'campo'=>'jundsoft_descricao'],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Serial', 'sort'=>'serial', 'campo'=>'serial'],
				['classe'=>'text-center no-print','style'=> 'width:136px;', 'rotulo'=>'Detalhes','campo'=>function($model) {

					$actions = [
						'<button type="button" class="btn btn-primary glyphicon glyphicon-list-alt" onClick="carrega_produto(' . $model->id . ');"></button>',
					];

					if (\Auth::user()->checkPermissao('colmeia.admin'))
					{
						if ($model->consta_em_estoque)
							$actions[] = '<a href="' . route('colmeia.produto.descartar', [$model->id]) . '" class="btn btn-danger glyphicon glyphicon-trash" title="Colocar produto para descarte" onClick="javascript:return confirm(\'Esta ação irá colocar este produto para descarte\nTem certeza de que deseja fazer isso?\');"></a>';

						if ($model->consta_alocado)
							$buttons[] = '<a href="' . route('colmeia.produto.desalocar', [$model->id]) . '" class="btn btn-warning glyphicon glyphicon-remove-circle" title="Desalocar produto da colmeia"  onClick="javascript:return confirm(\'Esta ação irá desalocar este produto individualmente da colmeia\nTem certeza de que deseja fazer isso?\');"></a>';

					}

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function produtos_descarte (Request $request, $pagina = 1)
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Relatório de Descartes',
			'filtros'			=> [
				['tipo'=>'text', 'largura'=>10, 'nome'=>'serial', 'placeholder'=>'Serial', 'valor'=>$request->query('serial')]
			],
			'urlDaPagina'		=> function ($pagina) {
				return route('colmeia.relatorio.produto.devolucao', ['pagina'=>$pagina]);
			},
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Serial', 'campo'=>'serial'],
				['classe'=>'text-center no-print', 'style'=> 'width:150px;', 'rotulo'=>'Status', 'campo'=>'statusString'],
				['classe'=>'text-center no-print','style'=> 'width:50px;', 'rotulo'=>'Ações', 'campo'=>function ($item) {
					return '<a href="' . route('colmeia.produto.devolucao', ['produto' => $item->id]) . '" class="btn btn-primary">Retornar para Disponível</a>';
				}],
			],
			'dados'				=> Produto::getListaDevolucao($request->query('serial')),
		]);
	}

	public function produtos_devolucao (Request $request, $pagina = 1)
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Item para devolução',
			'filtros'			=> [
				['tipo'=>'text', 'largura'=>10, 'nome'=>'serial', 'placeholder'=>'Serial', 'valor'=>$request->query('serial')]
			],
			'urlDaPagina'		=> function ($pagina) {
				return route('colmeia.relatorio.produto.devolucao', ['pagina'=>$pagina]);
			},
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Serial', 'campo'=>'serial'],
				['classe'=>'text-center no-print', 'style'=> 'width:150px;', 'rotulo'=>'Status', 'campo'=>'statusString'],
				['classe'=>'text-center no-print','style'=> 'width:50px;', 'rotulo'=>'Ações', 'campo'=>function ($item) {
					return '<a href="' . route('colmeia.produto.devolucao', ['produto' => $item->id]) . '" class="btn btn-primary">Retornar para Disponível</a>';
				}],
			],
			'dados'				=> Produto::getListaDevolucao($request->query('serial')),
		]);
	}

	public function produtos_seriais (Request $request, $pagina = 1)
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Seriais Disponíveis',
			'subtitulo'			=> 'Relatório de seriais alocados em condição de disponibilidade para atrelamento com pedido',
			'export_filename'	=> 'seriais_disponiveis',
			'filtros'			=> [
				['tipo'=>'text', 'largura'=>7, 'nome'=>'sku', 'placeholder'=>'SKU', 'valor'=>$request->query('sku')],
				['tipo'=>'multiselect', 'largura'=>2, 'nome'=>'uf', 'placeholder'=>'Filtrar por UF', 'valor'=>$request->query('uf'), 'opcoes'=>Sku::$estados],
				['tipo'=>'multiselect', 'largura'=>2, 'nome'=>'operadora', 'placeholder'=>'Filtrar por Operadora', 'valor'=>$request->query('operadora'), 'opcoes'=>Sku::$operadoras],
			],
			'urlDaPagina'		=> function ($pagina, $export = null) {

				$params = ['pagina'=>$pagina];

				if (!is_null($export))
					$params['export'] = $export;

				return route('colmeia.relatorio.produto.seriais', $params);
			},
			'colunas'			=> [
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'SKU', 'campo'=>'sku'],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'Operadora', 'campo'=>function ($item) {

					$info = Sku::info($item->sku);

					if (is_null($info) || !isset($info->operadora) || !isset($info->modelo) || !isset($info->banda) || !isset($info->hlr) || !is_array($info->hlr))
						return '<i class="text-gray">desconhecido<i>';

					return $info->operadora;
				
				}],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'Modelo', 'campo'=>function ($item) {

					$info = Sku::info($item->sku);

					if (is_null($info) || !isset($info->operadora) || !isset($info->modelo) || !isset($info->banda) || !isset($info->hlr) || !is_array($info->hlr))
						return '<i class="text-gray">desconhecido<i>';

					return $info->modelo . ' - ' . $info->banda;
				
				}],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'HLR', 'campo'=>function ($item) {

					$info = Sku::info($item->sku);

					if (is_null($info) || !isset($info->operadora) || !isset($info->modelo) || !isset($info->banda) || !isset($info->hlr) || !is_array($info->hlr))
						return '<i class="text-gray">desconhecido<i>';

					return implode(', ', $info->hlr);
				
				}],
				['classe'=>'text-center','style'=> 'width:50px;', 'rotulo'=>'Fora do Estoque', 'campo'=>'fora_estoque'],
				['classe'=>'text-center','style'=> 'width:50px;', 'rotulo'=>'Em Estoque', 'campo'=>'em_estoque'],
				['classe'=>'text-center','style'=> 'width:50px;', 'rotulo'=>'Pendente', 'campo'=>'em_alocacao'],
				['classe'=>'text-center','style'=> 'width:50px;', 'rotulo'=>'Disponíveis', 'campo'=>'disponiveis'],
				['classe'=>'text-center warning','style'=> 'width:100px;', 'rotulo'=>'Total Estoque', 'campo'=>function ($item) {
					return $item->fora_estoque + $item->em_estoque + $item->em_alocacao + $item->disponiveis;
				}],
				['classe'=>'text-center','style'=> 'width:50px;', 'rotulo'=>'Reservados', 'campo'=>'reservado'],
				['classe'=>'text-center','style'=> 'width:50px;', 'rotulo'=>'Descarte', 'campo'=>'descarte'],
				['classe'=>'text-center warning','style'=> 'width:100px;', 'rotulo'=>'Total Geral', 'campo'=>function ($item) {
					return $item->fora_estoque + $item->em_estoque + $item->em_alocacao + $item->disponiveis + $item->reservado + $item->descarte;
				}],
				
			],
			'dados'				=> Produto::getListaSeriais($request->query('sku'), $request->query('operadora', []), $request->query('uf', [])),
		]);
	}

	public function posicoes_lista (Request $request, $pagina = 1)
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Consulta Colméia',
			'filtros'			=> [
				['tipo'=>'multiselect', 'largura'=>2, 'nome'=>'status', 'placeholder'=>'Filtrar por Status', 'opcoes'=> Posicao::$statuses, 'valor'=>$request->query('status', [Posicao::STATUS_DISPONIVEL])],
				['tipo'=>'text', 'largura'=>2, 'nome'=>'colmeia', 'placeholder'=>'{Armario} - {Gaveta} - {Posição}', 'valor'=>$request->query('colmeia'), 'titulo' => 'Você pode usar o caracter curinga {*} para ignorar uma parte do nome.' . PHP_EOL . PHP_EOL . 'Ex:' . PHP_EOL . '*-G-* busca gavetas G em todos os armários e em todas as posições'],
				['tipo'=>'text', 'largura'=>2, 'nome'=>'serial', 'placeholder'=>'Serial', 'valor'=>$request->query('serial')],
				['tipo'=>'text', 'largura'=>2, 'nome'=>'sku', 'placeholder'=>'SKU', 'valor'=>$request->query('sku')],
				['tipo'=>'text', 'largura'=>2, 'nome'=>'pedido', 'placeholder'=>'Pedido', 'valor'=>$request->query('pedido')],
			],
			'sort_by'			=> $request->query('sort_by', 'id'),
			'sort_order'		=> $request->query('sort_order', 'desc'),
			'urlDaPagina'		=> function ($pagina, $sortBy = null, $sortOrder = null) use ($request) {
				return route('colmeia.relatorio.posicoes.lista', [
					'pagina'		=> $pagina,
					'por_pagina'	=> $this->tratar_paginacao($request->query('por_pagina')),
					'colmeia'		=> $request->query('colmeia'),
					'status'		=> $request->query('status', [Posicao::STATUS_DISPONIVEL]),
					'serial'		=> $request->query('serial'),
					'sku'			=> $request->query('sku'),
					'pedido'		=> $request->query('pedido'),
					'sort_by'		=> $sortBy,
					'sort_order'	=> $sortOrder,
				]);
			},
			'colunas'			=> [
				['classe'=>'text-center no-border-print', 'style'=> 'width:auto;', 'rotulo'=>'Status', 'sort'=>'status', 'campo'=>function ($item) {
					
					switch ($item->status)
					{
						case Posicao::STATUS_VAGA:

							$class = 'label-primary';
							break;
							
						case Posicao::STATUS_DISPONIVEL:

							$class = 'label-success';
							break;
						
						case Posicao::STATUS_RESERVADO:
						
							$class = 'label-warning';
							break;

						case Posicao::STATUS_DESCARTE:

							$class = 'label-warning';
							break;

					}

					return '<span class="label ' . $class . '">' . $item->statusString . '</span>';

				}],
				['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'Colméia', 'sort'=>'colmeia', 'campo'=>function ($item) {
					return implode('-', [$item->armario, $item->gaveta, $item->posicao]);
				}],
				['classe'=>'text-center','style'=> 'width:auto;', 'rotulo'=>'Serial', 'sort'=>'serial', 'campo'=>function ($item) {
					return !empty($item->serial) ? $item->serial : '<i class="text-gray">nenhum</i>';
				}],
				['classe'=>'text-center no-print','style'=> 'width:auto;', 'rotulo'=>'SKU', 'sort'=>'sku', 'campo'=>function ($item) {
					return !empty($item->sku) ? $item->sku : '<i class="text-gray">nenhum</i>';
				}],
				['classe'=>'text-center no-print','style'=> 'width:auto;', 'rotulo'=>'Pedido', 'sort'=>'pedido', 'campo'=>function ($item) {
					return !empty($item->pedido) ? $item->pedido : '<i class="text-gray">nenhum</i>';
				}],
			],
			'pagina'			=> $pagina,
			'por_pagina'		=> $this->tratar_paginacao($request->query('por_pagina')),
			'totalPaginas'		=> Posicao::listaPosicoesPaginadaPaginas($this->tratar_paginacao($request->query('por_pagina')), $request->query('colmeia'), $request->query('status', [Posicao::STATUS_DISPONIVEL]), $request->query('serial'), $request->query('sku'), $request->query('pedido'), $request->query('sort_by', 'id'), $request->query('sort_order', 'desc')),
			'dados'				=> Posicao::listaPosicoesPaginadaItens($pagina, $this->tratar_paginacao($request->query('por_pagina')), $request->query('colmeia'), $request->query('status', [Posicao::STATUS_DISPONIVEL]), $request->query('serial'), $request->query('sku'), $request->query('pedido'), $request->query('sort_by', 'id'), $request->query('sort_order', 'desc')),
		]);
	}

	public function alocacoes_lista (Request $request, $pagina = 1)
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Histórico de Alocações',
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Data', 'campo'=>function ($item) {
					return date('d/m/Y H:i:s', strtotime($item->data));
				}],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Status', 'campo'=>function ($item) {
					
					switch ($item->status)
					{
						case Alocacao::StATUS_COMPLETO:

							$class = 'label-success';
							break;
							
						case Alocacao::STATUS_INCOMPLETO:

							$class = 'label-warning';
							break;

					}

					return '<span class="label ' . $class . '">' . $item->statusString . '</span>';
				}],
				['classe'=>'text-left', 'style'=> 'width:auto;', 'rotulo'=>'Alocação', 'campo'=>function ($item) {
					return 'Alocação ' . $item->tag;
				}],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Produtos', 'campo'=>function ($item) {
					return count($item->produtos) . ' produto(s)';
				}],
			],
			'dados'				=> Alocacao::get(),
		]);
	}

	#TODO: levar este método para o controller magento
	public function magento_produtos ()
	{
		$produtos = Magento::getSimpleProductsList();
		$blacklist = Config::getValor('magento_produto_blacklist');

		return view('_generico.tabela', [
			'titulo'			=> 'Lista de Produtos Gerada pelo Magento',
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'#', 'campo'=>'id'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Status', 'campo'=>function ($item) use ($blacklist) {

					return !in_array($item->sku, $blacklist)
						? '<span class="label label-success">Habilitado</span>'
						: '<span class="label label-danger">Bloqueado</span>'
					;

				}],
				['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'SKU', 'campo'=>'sku'],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Produto', 'campo'=>'name'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Tipo', 'campo'=>'tipo_produto'],
			],
			'dados'				=> $produtos,
		]);
	}

	private function tratar_paginacao ($porPagina)
	{
		if (in_array($porPagina, ['todos','todas','all','-1']))
		{
			$porPagina = null;
		}
		elseif (empty($porPagina) || !is_numeric($porPagina))
		{
			$porPagina = '20';
		}

		return $porPagina;
	}
}