<?php

namespace App\Http\Controllers\Colmeia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class MagentoController extends Controller
{
	public function atualizar_pedidos (Request $request)
	{
		try
		{
			if (!$request->isMethod('post'))
				throw new \Exception('');

			if (!$request->hasFile('arquivo'))
				throw new \Exception('Arquivo não encontrado');

			if (count($arquivo = $this->_convertInput($request->file('arquivo'))) <= 0)
				throw new \Exception('Nenhuma linha de pedido foi especificada');

			foreach ($arquivo as $pedido)
			{
				DB::connection('magento')
					->table('sales_flat_order')
					->where('increment_id', $pedido['increment_id'])
					->update('nf', $pedido['nota'])
				;
			}

			return redirect()->route('colmeia.magento.pedido.atualizar')->with(['message'=>'Pedidos do magento atualizados de acordo com o arquivo <strong>' . $request->file('arquivo')->getClientOriginalName() . '</strong> com sucesso']);
		}
		catch (\Exception $exception)
		{
			#if (!empty($exception->getMessage()))
				return view('colmeia.magento_pedidoatualizar')->withErrors([$exception->getMessage()]);
		}

		return view('colmeia.magento_pedidoatualizar');
	}

	private function _convertInput ($path)
	{
		try {
			$excel = Excel::load($path, function ($result) {})->get();
		} catch (\Exception $th) {
			throw new \Exception('Não foi possível converter o arquivo. Verifique se o formato está correto');
		}

		if (!$excel)
			throw new \Exception('Não foi possível ler a planilha do excel');

		return array_map(function ($item) {

			if (!isset($item['increment_id']) || !isset($item['nota']))
				throw new \Exception('É necessário que o arquivo contenha as colunas "increment_id" e "nota"');
			
				return [
				'increment_id'	=> $item['increment_id'],
				'nota'			=> $item['nota'],
			];
		
		}, $excel->toArray());
	}
}