<?php

namespace App\Http\Controllers\Colmeia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Colmeia\Integracao\Jundsoft;

class JundsoftController extends Controller
{
	public function listar (Request $request)
	{
		$data = Jundsoft::comFiltro($request)->orderBy($request->query('sort_by', 'id'), $request->query('sort_order', 'desc'));

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'EAN + Sku Jundsoft',
			'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Novo Relacionamento Jundsoft', 'url' => route('colmeia.jundsoft.novo')],
			],
			'filtros'			=> [
				['chave'=>'ean', 'rotulo'=>'EAN', 'tipo'=>'text', 'colunas'=>4],
				['chave'=>'allied_sku', 'rotulo'=>'Sku Allied', 'tipo'=>'text', 'colunas'=>4],
				['chave'=>'jund_id', 'rotulo'=>'ID Jundsoft', 'tipo'=>'text', 'colunas'=>4],
				['chave'=>'status', 'rotulo'=>'Status', 'tipo'=>'multiselect', 'itens'=>[0=>'Não', 1=>'Sim'], 'colunas'=>3],
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'ID','campo'=>'id', 'sort'=>'id'],
				['classe'=>'text-center', 'style'=>'width:auto;', 'rotulo'=>'EAN','campo'=>'ean', 'sort'=>'ean'],
				['classe'=>'text-left', 'style'=>'width:300px;', 'rotulo'=>'Produto','campo'=>'produto_string', 'sort'=>'produto'],
				['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'SKU','campo'=>'allied_sku_string', 'sort'=>'allied_sku'],
				['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'SKU ID','campo'=>'jund_id_string', 'sort'=>'sku_id'],
				['classe'=>'text-center no-print','style'=> 'width:200px;', 'rotulo'=>'Detalhes','campo'=>function($model) {

					$actions = [
						'<a class="btn btn-primary" href="' . route('colmeia.jundsoft.editar', [$model->id]) . '"><i class="fa fa-edit"></i></a>',
					];

					return '<div class="btn-group">' . implode('', $actions) . '</div>';
				}],
			],
			'dados'				=> $data->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function editar (Request $request, $jundsoft = null)
	{
		$jundsoft = Jundsoft::firstOrNew(['id' =>$jundsoft]);

		#define as regras de validação do POST
		$regrasValidacao = [
			'ean'			=> ['nullable','unique:colmeia.integracao_jundsoft,ean', function ($value, $field, $fail) use ($request) {

				if ($request->has('ean'))
				{
					if (!$request->has('produto') || empty($request->input('produto')))
						$fail('Você precisa preencher o nome do produto');
				}

			}],
			'produto'		=> ['nullable'],
			'allied_sku'	=> ['nullable','max:20'],
			'jund_id'		=> ['nullable','min:0','max:9999999999'],
		];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$campos = ['allied_sku', 'jund_id'];

			if (!$jundsoft->exists)
			{
				$campos[] = 'ean';
				$campos[] = 'produto';
			}

			$jundsoft->fill($request->only($campos));
			$jundsoft->save();

			return redirect()->route('colmeia.jundsoft.listar')->with(['message' => 'Relacionamento Jundsoft para o EAN <strong>' . $jundsoft->ean . '</strong> salvo com sucesso!']);
		}

		return view('colmeia.jundsoft_editar', [
			'jundsoft'		=> $jundsoft,
		]);
	}

	public function deletar ($jundsoft)
	{
		$jundsoft = Jundsoft::findOrFail($jundsoft);
		$jundsoft->delete();

		return redirect()->route('colmeia.jundsoft.listar')->with(['message' => 'Relacionamento Jundsoft para o EAN <strong>' . $jundsoft->ean . '</strong> foi excluído com sucesso!']);
	}
}