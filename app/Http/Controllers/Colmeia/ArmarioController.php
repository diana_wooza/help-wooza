<?php

namespace App\Http\Controllers\Colmeia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Colmeia\Armario;
use \App\Models\Colmeia\Operadora;
use \App\Models\Colmeia\Produto;
use \App\Models\Colmeia\Log\Produto as LogProduto;

class ArmarioController extends Controller
{
	public function lista ()
	{
		$operadoras = Operadora::listaParaSelect();
		$posicoesLivres = Armario::getTodasPosicoesLivres();

		return view('_generico.tabela', [
			'titulo'			=> 'Gerenciar Lista de Armários',
			'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Novo Armário', 'url' => route('colmeia.armario.adicionar')],
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'ID','campo'=>'id'],
				['classe'=>'text-center no-border-print', 'style'=> 'width:100px;', 'rotulo'=>'Status','campo'=>function ($item) {

					switch ($item->status)
					{
						case Armario::STATUS_ATIVO:

							$class = 'label-success';
							break;
							
						case Armario::STATUS_NAOALOCANDO:

							$class = 'label-warning';
							break;
						
						case Armario::STATUS_INATIVO:
						
							$class = 'label-danger';
							break;

					}

					return '<span class="label ' . $class . '">' . $item->statusString . '</span>';
				}],
				['classe'=>'text-center no-border-print', 'style'=> 'width:100px;', 'rotulo'=>'Sobressalente','campo'=>function ($item) {

					return '<span class="label ' . ($item->sobressalente == true ? 'label-success' : 'label-warning') . '">' . ($item->sobressalente == true ? 'Sim' : 'Não') . '</span>';
				}],
				['classe'=>'text-center','style'=> 'width:50px;', 'rotulo'=>'Prioridade', 'campo'=>'prioridade'],
				['classe'=>'text-left','style'=> 'width:auto;', 'rotulo'=>'Nome', 'campo'=>'nome'],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Gavetas', 'campo'=>'gavetas'],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Posições', 'campo'=>'posicoes'],
				['classe'=>'text-center','style'=> 'width:100px;', 'rotulo'=>'Operadora', 'campo'=>function ($item) use ($operadoras) {

					if (empty($item->operadora))
						return '<i class="text-muted">Nenhuma</i>';

					return $operadoras[$item->operadora];

				}],
				['classe'=>'text-center','style'=> 'width:120px;', 'rotulo'=>'Posições Livres','campo'=>function ($item) {
					return number_format($item->posicoeslivres,0,',','.');
				}],
				['classe'=>'text-center no-print','style'=> 'width:50px;', 'rotulo'=>'Ações','campo'=>function($item) {
					return '<div class="btn-group">'
							. '<a class="btn btn-default" href="' . route('colmeia.armario.editar', ['id' => $item->id]) . '"><i class="fa fa-edit"></i></a>'
						. '</div>'
					;
				}],
			],
			'dados'				=> Armario::orderBy('prioridade', 'desc')->get(),
		]);
	}

	public function editar (Request $request, $id = null)
	{
		$armario = Armario::where(['id' => $id])->first();

		if (is_null($armario))
			$armario = new Armario();

		#define as regras de validação do POST
		$regrasValidacao = [
			'status'		=> 'required',
			'nome'			=> 'required|max:50',
			'operadora'		=> 'required|integer',
			'gaveta_tipo'	=> [
				'required',
				function ($attribute, $value, $fail) {

					if ($value != 'automatico')
						$fail('Apenas o método de criação de gavetas automáticas está funcional no momento');
				}
			],
		];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$armario->fill($request->only('status', 'nome', 'sobressalente', 'gavetas', 'posicoes', 'operadora'));
			$armario->save();

			// switch ($request->input('gaveta_tipo'))
			// {
			// 	case 'automatico':

			// 		$armario->updateGavetasPosicoes($request->input('gavetas_automatico_gavetas'), $request->input('gavetas_automatico_posicoes'));
			// 		break;

			// 	case 'manual':

			// 		break;
			// }

			return redirect()->route('colmeia.armario.lista')->with(['message' => 'Armário <strong>' . $armario->nome . '</strong> salvo com sucesso']);
		}

		return view('colmeia.armario_editar', [
			'armario'			=> $armario,
			'statuses'			=> Armario::$statuses,
			'operadoras'		=> Operadora::listaParaSelect(),
			'ocupationBarColor'	=> function ($ocupation) {

				if ($ocupation < 0.3)
					return 'green';

				if ($ocupation < 0.6)
					return 'yellow';

				return 'red';
			},
		]);
	}

	public function desalocar ($id)
	{
		$armario = Armario::find($id);
		
		if (empty($armario))
			return redirect()->route('colmeia.produto.chip.desalocar')->withErrors(['Armário ID ' . $id . ' não encontrado']);

		$produtos = [];
		$logs = [];
		
		foreach ($armario->getTodasPosicoesDescarte() as $posicao)
		{
			if (!is_null($posicao->produto))
			{
				$produtos[] = $posicao->produto;
				$logs[] = (object) [
					'produto'		=> $posicao->produto,
					'armario'		=> $posicao->armario,
					'gaveta'		=> $posicao->gaveta,
					'posicao'		=> $posicao->posicao,
					'serial'		=> $posicao->serial,
					'sku'			=> $posicao->sku,
					'pedido'		=> $posicao->pedido,
				];
			}
			
			$posicao->liberar();
			$posicao->save();
		}

		Produto
			::whereIn('id', $produtos)
			->update(['status' => Produto::STATUS_DESCARTE])
		;
		
		$dataDescarte = LogProduto::descarte($logs);
		
		return redirect()->route('colmeia.produto.lista.descarte', ['data' => strtotime($dataDescarte)]);
	}
}