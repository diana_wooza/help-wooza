<?php

namespace App\Http\Controllers\Colmeia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Helpers\Magento;
use \App\Models\Colmeia\Config;
use \App\Models\Colmeia\Nfe;
use \App\Models\Colmeia\Log\Nfe as Log;

class NfeController extends Controller
{
	/**
	 * Controller de importação de nota fiscal
	 * @method importar
	 * @return
	 */
	public function importar (Request $request)
	{
		if ($request->isMethod('post'))
		{
			try
			{
				$nfe = $this->importar_nota($request->input('tipo'), $request->file('nota'));

				return redirect()->route('colmeia.nfe.listardetalhe', ['id' => $nfe->id]);
			}
			catch (\Exception $exception)
			{
				return view('colmeia.nfe_importar')->withErrors(['error'=>$exception->getMessage()]);
			}
		}

		return view('colmeia.nfe_importar');
	}

	private function importar_nota ($tipo, $arquivo)
	{
		$nfe = Nfe::criarViaUpload($tipo, $arquivo);

		switch ($tipo)
		{
			case 'entrada':

				$nfe->tipo = Nfe::TIPO_ENTRADA;
				$nfe->processarTodosOsEans();
				$nfe->save();
				
				$nfe->processarTodosOsDetalhes();
				break;

			case 'saida':

				// $nfe->tipo = Nfe::TIPO_SAIDA;
				// $nfe->save();
				// break;

			case 'devolucao_entrada':
			case 'devolucao_saida':

				throw new \Exception('Apenas as notas de entrada podem ser inseridas no momento');
				break;

		}

		return $nfe;
	}

	/**
	 * Método que lista os detalhes da nota fiscal
	 * @method listar
	 * @return
	 */
	private function listar (Nfe $nota)
	{
		if (is_null($nota))
			return redirect()->route('home')->withErrors(['error' => 'Não foi possível encontrar a nota com o ID ' . $id]);

		return view('colmeia.nfe_listardetalhe', [
			'actions'			=> [
				['rotulo' => '<i class="fa fa-plus"></i> &nbsp; Adicionar Chips em Lote', 'url' => route('colmeia.produto.entrada_lote', [$nota->id])],
			],
			'nota'				=> $nota,
		]);
	}

	/**
	 * Define o produto associado à nota
	 * @method produto
	 * @return 
	 */
	public function produto (Request $request, $id, $detalheIndex)
	{
		$nota = Nfe::find($id);

		if (empty($nota))
			return redirect()->route('home')->withError('Nota Inválida');
			
		$detalhe = @$nota->detalhes[$detalheIndex];
			
		if (is_null($detalhe) || !is_object($detalhe))
			return redirect()->route('home')->withError('Nota Inválida');

		if (!is_null($detalhe->produto))
			return redirect()->route('colmeia.produto.entrada', ['nota' => $nota->id, 'detalhe' => $detalheIndex]);

		$blacklist = Config::getValor('magento_produto_blacklist');

		if (!is_array($blacklist))
			$blacklist = [];

		if ($request->isMethod('post'))
		{
			$produto = Magento::getProductListIndexdBySku($request->input('sku'));

			$nota
				->updateProduto($detalheIndex, $produto)
				->save()
			;

			$nota->updateTipoProdutos($detalheIndex);

			#Log::detalhe($nota, $detalheIndex, $produto);
			return redirect()->route('colmeia.nfe.listardetalhe', [$nota->id]);
		}

		return view('colmeia.nfe_produto', [
			'nota'		=> $nota,
			'detalhe'	=> $nota->detalhes[$detalheIndex],
			'index'		=> $detalheIndex,
			'produtos'	=> array_filter(Magento::getProductListIndexdBySku(), function ($item) use ($blacklist) {
				return !in_array($item->sku, $blacklist);
			}),
		]);
	}

	/**
	 * Método que lista as notas fiscais com pendência de item
	 * @method pendente
	 * @return
	 */
	public function pendente ()
	{
		return view('colmeia.nfe_pendente');
	}

	public function listar_detalhe_buscar (Request $request)
	{
		$nota = Nfe
			::where('id', $request->input('nota'))
			#->orWhereRaw('json_extract(nota, \'$.NFe.infNFe."@attributes".Id\') like ?', [$request->input('nota')])
			->first()
		;

		if (empty($nota))
			return redirect()->route('home')->withErrors('Não foi encontrada nenhuma nota para a busca: ' . $request->input('nota'));

		return $this->listar($nota);
	}

	/**
	 * Método que carrega a nota baseado no ID especificado
	 * @method #listar_detalhe_id
	 * @return
	 */
	public function listar_detalhe_id ($id)
	{
		return $this->listar(Nfe::find($id));
	}

	/**
	 * Método que carrega a nota baseado no código da nota especificado
	 * @method #listar_detalhe_codigo
	 * @return
	 */
	 public function listar_detalhe_codigo ($codigo)
	{
		if (\substr($codigo, 0, 3) != 'NFe')
			$codigo = 'NFe' . $codigo;

		return $this->listar(Nfe::where('codigo', $codigo)->first());
	}
}