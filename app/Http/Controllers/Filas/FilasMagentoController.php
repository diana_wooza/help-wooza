<?php

namespace App\Http\Controllers\Filas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Filas\MensagemPedidoMagento;

use App\Jobs\Filas\FilasMagento;

class FilasMagentoController extends Controller {

	public function GravarPedidoFila(Request $request) {
		$mensagem = new MensagemPedidoMagento($request['pedido'], $request['fila']);
		// for ($i=1;$i<=1000;$i++) {
			FilasMagento::dispatch($mensagem)->onConnection('filas')->onQueue($request['fila']);
		// }

		return ['OK' => true];
	}

}