<?php

namespace App\Http\Controllers\Importador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Importador\Importador;
use App\Models\Importador\DowngradeTim;
use App\Models\Importador\DowngradeVivo;
use App\Models\Importador\StatusOrdem;
use App\Models\Importador\Log;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Importador\Cpfvendedor;
use App\Models\Importador\Phonenumber;
use \App\Models\Magento\Consumer\Order;
use App\Models\VarejoLoja\VarejoLojas;

class ImportadorController extends Controller
{
	public function decrypt (Request $request)
	{
		if($request->isMethod('post') && $request->has('pedidos') && preg_match_all('/.*?([a-zA-Z]{1,3}[0-9]{7}).*?/', $request->input('pedidos'), $matches))
		{
			ini_set('max_execution_time', 1200);
			ini_set('memory_limit', '3G');

			$fields = ['increment_id', 'customer_email','customer_firstname','customer_lastname'];

			return Excel::create('export_pedido_decriptado_' . date('YmdHis'), function($excel) use ($fields, $matches) {

				$excel
					->setCreator('HUB')
					->setCompany('Wooza')
					->setDescription('Dados exportados de pedidos no dia ' . date('d/m/Y \à\s H:i:s'))
				;

				$excel->sheet('Pedidos', function($sheet) use ($fields, $matches) {

					$sheet->appendRow($fields);
					
					foreach (array_chunk($matches[1], 1000) as $chunk)
					{
						$pedidos = Order
							::select($fields)
							->whereIn('increment_id', $chunk)
							->get()
							->toArray()
						;

						foreach ($pedidos as $pedido)
							$sheet->appendRow($pedido);
					}
				});
			

			})->download('xlsx');
		}

		return view('importador.decrypt');
	}

	public function listar (Request $request)
	{	
		$term = false;
		$models = [];
		if( $request->input('q') )
			$term = $request->input('q');

		if( $request->input('operadora') ){
			$operadora = $request->input('operadora');
			if( $operadora == 'tim' ){
				if( $term ) 
					$models = DowngradeTim::where('plano_atual', 'LIKE', '%' . $term . '%')->orWhere('plano_destino', 'LIKE', '%' . $term . '%')->paginate(20)->appends($request->only('q','operadora'));
				else
					$models = DowngradeTim::paginate(20)->appends($request->only('q','operadora'));
			}

			if( $operadora == 'vivo' ){
				if( $term ) 
					$models = DowngradeVivo::where('plano_atual', 'LIKE', '%' . $term . '%')->orWhere('plano_destino', 'LIKE', '%' . $term . '%')->paginate(20)->appends($request->only('q','operadora'));
				else
					$models = DowngradeVivo::paginate(20)->appends($request->only('q','operadora'));
			}
		}

		return view('importador.lista_downgrade', ['dados' => $models]);
	}

	public function importar (Request $request, $id = null)
	{
		if (is_null($id))
		{
			return view('importador.selecionar', [
				'importadores' => Importador::disponiveisUsuarioLogado(),
			]);
		}

		$importador = Importador::find($id);
		$errors = [];

		if ($request->isMethod('post'))
		{
			try
			{
				ini_set('max_execution_time', 0);
				ini_set('memory_limit', '1024M');

				$log = $importador->executar($request);
			}
			catch (\Exception $exception)
			{
				$errors[] = $exception->getMessage();
			}
		}

		return view('importador.importar', [
			'importador' => $importador,
		])->withErrors($errors);
	}

	public function cpf_vendedor (Request $request)
	{
		if($request->isMethod('post') && $request->has('pedidos'))
		{
			$pedidos = explode(PHP_EOL, $request->input('pedidos'));
			$novosPedidos = [];
			$erros = [];

			foreach ($pedidos as $pedido)
			{
				$matches = null;
				
				if (preg_match('/.*?([a-zA-Z]{1,3}[0-9]{7}).*?([0-9]{3}[\. ]?[0-9]{3}[\. ]?[0-9]{3}[\- ]?[0-9]{2}).*?/', $pedido, $matches))
				{
					$novosPedidos[] = array(
						'pedido'	=> $matches[1],
						'cpf'		=> preg_replace('/[\.\-]/', '', $matches[2]),
					);
				}
				else
				{
					$erros[] = $pedido;
				}
			}

			$redirect = redirect()->route('importador.cpfvendedor');

			if (count($novosPedidos) > 0)
			{
				$redirect->with(['message' => count($novosPedidos) . ' item(s) atualizado(s) com sucesso']);
				Cpfvendedor::atualizarBase($novosPedidos);
			}

			if (count($erros) > 0)
				$redirect->withErrors(['Um total de ' . count($erros) . ' não foi(foram) atualizado(s):<br /> - ' . implode('<br /> - ', $erros)]);

			return $redirect;
		}

		return view('importador.cpfvendedor');
	}

	public function phone_number (Request $request)
	{
		if($request->isMethod('post') && $request->has('pedidos'))
		{
			$pedidos = explode(PHP_EOL, $request->input('pedidos'));
			$novosPedidos = [];
			$erros = [];

			foreach ($pedidos as $pedido)
			{
				$matches = null;
				
				if (preg_match('/.*?([a-zA-Z]{1,3}[0-9]{7}).*?(0?[0-9]{11}).*?/', $pedido, $matches))
				{
					$novosPedidos[] = array(
						'pedido'	=> $matches[1],
						'phone'		=> preg_replace('/[\.\-]/', '', $matches[2]),
					);
				}
				else
				{
					$erros[] = $pedido;
				}
			}

			$redirect = redirect()->route('importador.phonenumber');

			if (count($novosPedidos) > 0)
			{
				$redirect->with(['message' => count($novosPedidos) . ' item(s) atualizado(s) com sucesso']);
				Phonenumber::atualizarBase($novosPedidos);
			}

			if (count($erros) > 0)
				$redirect->withErrors(['Um total de ' . count($erros) . ' não foi(foram) atualizado(s):<br /> - ' . implode('<br /> - ', $erros)]);

			return $redirect;
		}

		return view('importador.phonenumber');
	}

    public function downgradeTimImportar(Request $request) {
        $result = null;
        $log    = Log::lastLog('downgrade_tim_importacao');

		if($request->isMethod('post'))
		{
			try
			{
				if(!Input::hasFile('planilha'))
					throw new \Exception('A seleção de um arquivo excel é obrigatório.');

				$file   = Input::file('planilha')->getRealPath();
				$data   = Excel::load($file, function ($rs) {})->get();
				$result = DowngradeTim::criarViaUpload($data->toArray());
			}
			catch (\Exception $exception)
			{
				Log::logIt( 'downgrade_tim_importacao', 'Importação falhou.', $exception->getMessage(), Log::BAD_REQUEST, Log::LEVEL_ERROR);
				return view('importador.downgradetim', ['result'=>$result,'lastImport'=>$this->lastImportedAt($log)])->withErrors(['error'=>$exception->getMessage()]);
			}
		}

		$log = Log::lastLog('downgrade_tim_importacao');
		
		return view('importador.downgradetim', ['result'=>$result,'lastImport'=>$this->lastImportedAt($log)]);
	}

	public function downgradeVivoImportar(Request $request) {
        $result = null;
        $log    = Log::lastLogVivo('downgrade_vivo_importacao');

		if($request->isMethod('post'))
		{
			try
			{
				if(!Input::hasFile('planilha'))
					throw new \Exception('A seleção de um arquivo excel é obrigatório.');

				$file   = Input::file('planilha')->getRealPath();
				$data   = Excel::load($file, function ($rs) {})->get();
				$result = DowngradeVivo::criarViaUpload($data->toArray());
			}
			catch (\Throable $exception)
			{
				Log::logItVivo( 'downgrade_vivo_importacao', 'Importação falhou.', $exception->getMessage(), Log::BAD_REQUEST, Log::LEVEL_ERROR);
				return view('importador.downgradeVivo', ['result'=>$result,'lastImport'=>$this->lastImportedAt($log)])->withErrors(['error'=>$exception->getMessage()]);
			}
		}

		$log = Log::lastLogVivo('downgrade_vivo_importacao');
		
		return view('importador.downgradeVivo', ['result'=>$result,'lastImport'=>$this->lastImportedAt($log)]);
	}

	public function statusPedidoImportar(Request $request)
	{
		$now 		= new \DateTime('NOW');
		$result		= null;
		
		if($request->isMethod('post'))
		{
			try
			{
				if(!Input::hasFile('planilha'))
					throw new \Exception('A seleção de um arquivo excel é obrigatório.');
					
				$file   = Input::file('planilha')->getRealPath();
				$data   = Excel::load($file, function ($rs) {})->get();
				$result = StatusOrdem::criarViaUpload($data->toArray());
			}
			catch (\Exception $exception)
			{
				return view('importador.statuspedido', ['result' => $result,])->withErrors(['error'=>$exception->getMessage()]);
			}
		}

		return view('importador.statuspedido', ['result' => $result,]);
	}

	public function downloadModeloPlanilha()
	{
		Excel::create('ModeloImportacaoStatusPedido', function($excel) {

			$excel->sheet('Sheet', function($sheet) {
				$sheet->fromArray(['increment_id', 'status' , 'activated_date']);
			});
		
		})->export('xls');
	}

	/**
	* Get the last imported log from mongoDb channel
	* @access Private
	* @param Log
	* @return String
	*/
	private function lastImportedAt(?\App\Models\Importador\Log $log): String
	{
		if(!isset($log->datetime))
			return '';
			
		$datetime	= new \DateTime($log->datetime);
		// $locale		= new \DateTimeZone('America/Sao_Paulo');
		// $datetime->setTimezone($locale);

		return $datetime->format('d/m/Y H:i:s');
	}

	public function importarVarejoLoja(Request $request)
	{
		$now 		= new \DateTime('NOW');
		$result		= null;
		
		if($request->isMethod('post'))
		{
			try
			{
				if(!Input::hasFile('planilha'))
					throw new \Exception('A seleção de um arquivo excel é obrigatório.');
					
				$file   = Input::file('planilha')->getRealPath();
				$data   = Excel::load($file, function ($rs) {})->get();
				$result = VarejoLojas::criarViaUpload($data->toArray());
			}
			catch (\Exception $exception)
			{
				return view('importador.varejoloja', ['result' => $result,])->withErrors(['error'=>$exception->getMessage()]);
			}
		}

		return view('importador.varejoloja', ['result' => $result,]);
	}
}