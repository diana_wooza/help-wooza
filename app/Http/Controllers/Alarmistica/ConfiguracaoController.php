<?php

namespace App\Http\Controllers\Alarmistica;

use \Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Alarmistica\Api;
use App\Models\Alarmistica\PedidoMongo;
use App\Models\User;
use App\Models\Alarmistica\SegmentosApi;
use App\Http\Controllers\Alarmistica\ApiController;

Class ConfiguracaoController extends Controller
{
    /**
     * selectFilho - retorna o segmento no select
     *
     * @param  mixed $request
     *
     * @return json
     */
    public function selectFilho (Request $request)
    {
        if($request->isMethod('post') && $request->filled('collection')){
			$collection		 = $request->input('collection');
			$result 		 = Api::apiSelect($collection);
            $apisSelectFilho = [];
            
			foreach($result as $item):
				array_push($apisSelectFilho, $item);
			endforeach;
            
			return $apisSelectFilho;
		}
        
		return false;
    }
    
	/**
     * selectFilhoApi -  Retona todas as apis conforme a collection e o segmento
	 *
     * @return json
	 */
    public function selectFilhoApi (Request $request) 
	{
        if($request->isMethod('post') && $request->filled('collection') && $request->filled('segmento')){
            
			$collection    = $request->input('collection');
            $segmento      = $request->input('segmento');
            $statusAll     = $request->input('statusAll');
            $ativaInativa  = $request->input('ativaInativa');

			$resultApis	= Api::selectFilhoApi($collection, $segmento, $statusAll, $ativaInativa);
			$apis		= [];
            
			foreach($resultApis as $item):
				array_push($apis, $item);
			endforeach;
            
			return $apis;
		}
        
		return false;
        
    }
    
    /**
     * getChannel - retorna o channel do mongo pela Api
     *
     * @param string $colletion
     * @return object
     */
    public function getChannel (Request $collection)
    {
        if($collection->isMethod('post') && $collection->filled('collection')){
            return PedidoMongo::getChannel($collection->input('collection'));
        }
    }
    
    /**
     * chamaCadastrar - chama a tela de cadastro de api e tb recebe os dados desta 
     *
     * @return void
     */
    public function cadastrar (Request $request)
    {   
        if($request) {
     
            $pessoasSms    = explode(',', $request->input('pessoas-sms'));
            $pessoas       = [];
            $status        = 1;

            $dados = [
                    'operadora'         => 'required',
                    'channel'           => 'required|unique:alarmistica.api,consulta',
                    'segmento'          => 'required',
                    'range-positivo'    => 'required|numeric',
                    'range-negativo'    => 'required|numeric|lt:range-positivo',
                    'hora-inicio'       => 'required',
                    'hora-final'        => 'required',
                    'tempo-verificacao' => 'required',
                    'itens-alarmantes'  => 'required'
                    
                    ];
                    
            $validaDados = Validator::make($request->all(), $dados);
            
            if ($validaDados->fails()) {

                return redirect('alarmistica/configuracao/cadastro')
                ->with('message', 'Confira todos os dados informados!')
                ->withErrors($validaDados)
                ->withInput();
                
            } else {

                if($request->isMethod('post')){

                    $dadosAlteracaoApi = $this->tratarDados($request);
                    $todosSegmento     = SegmentosApi::orderBy('nm_segmento')->get();
                    $pessoasNaoRecebem = User::get();
        
                    try{

                        $api = new Api;
                        $api->fill($dadosAlteracaoApi);
                        $api->save();
        
                        return redirect()->route('alarmistica.configuracao.cadastro')
                                        ->with(['message' => 'O monitoramento da api '. $request->input('nome'). ' foi criado com sucesso!'])
                                        ->with(compact('channel', 'todosSegmento', 'pessoasNaoRecebem'));
        
                    } catch (Exception $e) {
                        return redirect()->route('alarmistica.configuracao.cadastro')
                        ->with(['message' => 'O erro ' .$e. ' esta ocorrendo, avise ao suporte!'])
                        ->with(compact('channel', 'todosSegmento', 'pessoasNaoRecebem'));
        
                    } 
                }    
            }
        }
    }
    
    /**
     * cadastro
     *
     * @return void
     */
    public function cadastro ()
    {   
        $todosSegmento           = SegmentosApi::orderBy('nm_segmento')->get();
        $pessoasNaoRecebem       = User::get();
        return view('alarmistica.cadastrarApi', compact('channel', 'todosSegmento', 'pessoasNaoRecebem'));
    }

    /**
     * chamarAjuste - chama a tela de alteração e retorna as apis no select
     *
     * @return Array
     */
    public function ajustar ()
    {
        $apisSelect  = ApiController::apisSelect();

        return view('alarmistica.alteraApi', compact('apisSelect'));
    }

    /**
     * editar - carrega a tele de alteração passando seus dados
     *
     * @param  mixed $request
     * @return array 
     */
    public function editar (Request $request)
    {
        if($request->isMethod('get') && $request->filled('api')) {

            $id            = [$request->input('api')];
            $apisSelect    = ApiController::apisSelect();
            $api           = Api::todasAtivas($id, '', 'TodasAtivasInativas');

            ## Retorna as pessas que estao no momento recebendo sms para a api em questão
            $pessoasRecebem          = ApiController::listaPessoasSms($api);
            $horarioDisparoSms       = ApiController::listaHorarioSms($api);
            $consultaTempoSchedule   = ApiController::listaTempoVerificacaoSms($api);
            $consultaItensAlarmantes = ApiController::listaItensAlarmantesSms($api);
            $todosSegmento           = SegmentosApi::orderBy('nm_segmento')->get();
            $segmento                = ApiController::segmentoApi($api);
            $channel                 = PedidoMongo::getChannel($api[0]['collection']);
            $idRecebem               = [];
            $ambosReceberam          = [];

            if($pessoasRecebem[0] != null) {
                foreach($pessoasRecebem as $recebem):
                    array_push($idRecebem, $recebem->id);
                endforeach;

            } else {
                $pessoasRecebem = []; //ninguem esta recebendo
            }

            $pessoasNaoRecebem       = User::especificos($idRecebem)->get();
       
            return view('alarmistica.alteraApi', compact('pessoasNaoRecebem','apisSelect', 'pessoasRecebem', 'api', 'horarioDisparoSms', 'consultaTempoSchedule', 'consultaItensAlarmantes', 'channel', 'todosSegmento'));
            
        } else {
            $apisSelect  = ApiController::apisSelect();

            return view('alarmistica.alteraApi', compact('apisSelect'));
        }
    }

    /**
     * alterar - é chamado na hora que se conclui a alteração
     *
     * @param  mixed $request
     *
     * @return string
     */
    public function alterar (Request $request)
    {
        if($request->isMethod('post')){

            $dadosAlteracaoApi = $this->tratarDados($request);

            try{
                $api = Api::find($request->input('id'));
                $api->fill($dadosAlteracaoApi);
                $api->save();

                return redirect()->route('alarmistica.configuracao.editar')->with(['message' => $request->input('nome'). ' modificada com sucesso!']);

            } catch (Exception $e) {
                return redirect()->route('alarmistica.configuracao.editar')->with(['message' => 'O erro ' .$e. ' esta ocorrendo, avise ao suporte!']);

            } 
        }

    }

    /**
     * tratarDados - recebe os dados de cadastro da api para tratados e serem incluidos ou alterados
     *
     * @param  mixed $request
     *
     * @return array
     */
    public function tratarDados($request)
    {
        $rangeNegativo = $request->input('range-negativo');
        $rangePositivo = $request->input('range-positivo');
        $status        = 1;
        $pessoasSms    = explode(',', $request->input('pessoas-sms'));
        $pessoas       = [];

        $configuracao = ['restricao' => ['horario' => [['de'  => $request->input('hora-inicio'),
                                                        'ate' => $request->input('hora-final')]]],
                        'consulta_schedule' => $request->input('tempo-verificacao'),
                        'itens_alarmantes'  => $request->input('itens-alarmantes')];

        foreach($pessoasSms as $pessoa):   
            $pessoa = ['broker' => 'vivo', 'usuario' => $pessoa];             
            array_push($pessoas, $pessoa);
        endforeach;

        if($request->input('status') == 'on'){
            $status = -1;
        }
        
        return $dadosAlteracaoApi = ['nome'           => $request->input('nome'),
                                    'collection'      => $request->input('operadora'),
                                    'segmento_id'     => $request->input('segmento'),
                                    'consulta'        => $request->input('channel'),
                                    'range_negativo'  => ($rangeNegativo / 100),
                                    'range_positivo'  => ($rangePositivo / 100),
                                    'sms'             => $pessoas,
                                    'configuracao'    => $configuracao,
                                    'status'          => $status];
    }
  
}