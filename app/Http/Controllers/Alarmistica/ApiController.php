<?php

namespace App\Http\Controllers\Alarmistica;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Alarmistica\Api;
use App\Models\Alarmistica\PedidoMongo;
use App\Models\Alarmistica\Log;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\Runner\Exception;

class ApiController extends Controller
{
	/**
	 * exibeIndicadores
	 *
	 * @param  mixed $request
	 *
	 * @return mixed
	 */
	public function exibeIndicadores (Request $request)
	{		
		if($request->isMethod('post') && $request->filled('log') == 'consuulta_apis'){

			$statusApi = [];

			$arrIdLogInstavel   = [];
			$statusApiInstavel  = 0;
			$dadosApiinstavel   = [];
			
			$arrIdLogOscilando  = [];
			$statusApiOscilando = 0; 
			$dadosApiOscilando  = []; 
			
			$arrIdLogEstavel    = [];
			$statusApiEstavel   = 0; 
			$dadosApiEstavel    = []; 
			$range_negativo    = []; 
			
			## Verifico todas as apis
			foreach(Api::todasAtivas() as $dadosApi):

				$dados = $this->recuperaPedidos($dadosApi);
				array_push($statusApi, $dados);

			endforeach;
		
			if($dados != false) {
				## Verifica qtd de cada status para exibir no "cartão" (verde, amarelo e vermelho)
				foreach($statusApi as $status):
					
					switch($status['statusAtual']):	
						case 'Positivo': $statusApiEstavel += 1;
										 array_push($arrIdLogEstavel, $status['id_api'].'-'.$status['percPositivo']); 
							break;
						case 'Negativo' || 'Not pedido!': $statusApiInstavel += 1;
										 array_push($arrIdLogInstavel, $status['id_api'].'-'.$status['percPositivo']);  
							break;
						default: $statusApiOscilando += 1;
								 array_push($arrIdLogOscilando, $status['id_api'].'-'.$status['percPositivo']);
							break;
					endswitch;
					##Descomentar para testar a saída!!	
						//array_push($range_negativo, $status['rangeNegativo'].'-'.$status['id_api'].'-'.$status['percPositivo'].'-'.$status['totalPedidos'].'-'.$status['totalPositivo'].'-'.$status['statusAtual']);

				endforeach;
			
				$statusAtual = ['qtd_api_estavel'    => $statusApiEstavel,
								'ids_estaveis'  	 => $arrIdLogEstavel,
								'qtd_api_instavel'   => $statusApiInstavel,
								'ids_instaveis' 	 => $arrIdLogInstavel,
								'qtd_api_oscilando'  => $statusApiOscilando,
								'range_negativo'     => $range_negativo,
								'ids_oscilando' 	 => $arrIdLogOscilando]; 
			
				## envio os status     
				return json_encode($statusAtual);
			}
			## Not api
			return json_encode(false);
		}
	}

	/**
	 * exibeIndicadoresMenu chama a view para carregar o dashboard
	 *
	 * @return view 
	 */
	public function exibeIndicadoresMenu ()
	{
		 return view('alarmistica.dash_alarmistica');
	}

	/**
	 * Verifica os pedidos que retornou status 200 de 5 minutos da data/hora atual
	 * @param mixed $dm, $conn
	 * @return void
	 */    
	public function recuperaPedidos(Api $dadosApi)
	{
		## Pedidos positivos são pedidos com level 200 e negativos com level 400
		$arrDadosApi   = [];
		$statusPedidos = ['positivos' => [], 'negativos' => []];
		$dataInicial   = date('Y-m-d H:i:s', strtotime('+1 hour, 55 minutes'));
		$dataFinal     = date('Y-m-d H:i:s', strtotime('+2 hour'));
		$pedidosNegativos = [[1],[2],[3]];

		$pedidos = PedidoMongo::fromApi($dadosApi, $dataInicial, $dataFinal); // Pedido no mongo
		
		foreach($pedidos as $pedido):
			$pedido->level == 200 ? array_push($statusPedidos['positivos'], $pedido['context']['codigo_pedido']) : array_push($statusPedidos['negativos'], $pedido['context']['codigo_pedido']);
		endforeach;

		$arrDadosApi = $this->calculaPercentual($statusPedidos, $dadosApi);
		
		return $arrDadosApi;
	}

	/**
	 * caculaPercentual, calcula o percentual sincrono e não dos logs, pois e exibido no dashboard em tempo real de como os pediddos estão no mongo
	 * @param int $positivos, negativos
	 * @param mixed  $conn
	 * @return array
	 */
	function calculaPercentual($statusPedidos, Api $dadosApi)
	{
		$dataAtual = date('Y-m-d H:i:s');
		$arrDadosApi = [];
		
		## Se tiver api				
		if($dadosApi){
			# Percentuais range
			$rangePositivo = intval($dadosApi->range_positivo * 100);
			$rangeNegativo = intval($dadosApi->range_negativo * 100);

			# QTD 
			$qtdPositivos = count($statusPedidos['positivos']);
			$qtdNegativos = count($statusPedidos['negativos']);
					
			# Da qtd total de pedidos qual é o % de positivos?
			$totalPedidos  = $qtdPositivos + $qtdNegativos;
			$jsonPositivos = !empty($statusPedidos['positivos']) ? json_encode($statusPedidos['positivos']) : null;
			$jsonNegativos = !empty($statusPedidos['negativos']) ? json_encode($statusPedidos['negativos']) : null;
			
			$qtdPositivos = $qtdPositivos != 0 ? $qtdPositivos : 0;
			$statusAtual  = $totalPedidos == 0 ? "Not pedido!" : "Negativo";
			$percentualPositivo   = 0;
			
			## Se total de pedidos e qtd de pedidos positivos não forem zerados e a api existir
			if($totalPedidos && ($qtdPositivos && $rangePositivo != 0)){
				$percentualPositivo = intval(($qtdPositivos / $totalPedidos) * 100);

					switch($percentualPositivo):
						case ($percentualPositivo >= $rangePositivo): $statusAtual = "Positivo"; 
							break;
						case ($percentualPositivo < $rangeNegativo): $statusAtual = "Negativo"; 
							break;
						default: $statusAtual = "Oscilando";
							break;
					endswitch;      

				// $status   = StatusApi::fromStatus($statusAtual);

				// $dadosLog = ['id_api'       => $dadosApi->id,
				// 			'id_status'     => $status->id,
				// 			'jsonPositivos' => $jsonPositivos,
				// 			'jsonNegativos' => $jsonNegativos,
				// 			'percentual'    => $percentualPositivo,
				// 			'dataAtual'     => $dataAtual];    
							
				## Caso tenha pedidos haverá log
				if($statusAtual != 'Not pedido!'){
					## Comentei para não gravar log por enquanto o idLog fica vazio. Se quiser e só descomentar abaixo e receber lá em cima no retorno
					// $idLog = $this->insereLogApi($dadosLog);   
					// $arrDadosApi = ['id_log' => $idLog];
					$arrDadosApi = ['id_log' => ""];
					
				} else {
					$arrDadosApi = ['id_log' => ""];
				}

			} else {
				$arrDadosApi = ['id_log' => ""];
			}	
			
			## Monta o array com todos os dados de percentual, da api etc
			$arrDadosApi += ['nomeApi'            => $dadosApi->nome,
							'id_api' 			  => $dadosApi->id,
							'codPedidosPositivos' => $jsonPositivos,
							'codPedidosNegativos' => $jsonNegativos,
							'totalPositivo'       => $qtdPositivos,
							'totalNegativo'       => $qtdNegativos,
							'percPositivo'        => $percentualPositivo,
							'statusAtual'         => $statusAtual,
							'rangeNegativo'       => $rangeNegativo,
							'totalPedidos'        => $totalPedidos,
							'dataAtual'           => $dataAtual];

			return $arrDadosApi;
		}
		return;
	}

	/**
	 * insereLogApi cria log da consulta
	 * @param array
	 * @return array id_log
	 */
	public function insereLogApi($dadosLog)
	{
		$insertLogConsultaApi = LogDashboard::create(['id_api'              => $dadosLog['id_api'],
													  'id_status_api'       => $dadosLog['id_status'],
													  'pedidos_positivos'   => $dadosLog['jsonPositivos'],
													  'pedidos_negativos'   => $dadosLog['jsonNegativos'],
													  'percentual_positivo' => $dadosLog['percentual'],
													  'dt_consulta'		    => $dadosLog['dataAtual']]);
		return $insertLogConsultaApi->id;
	}

	/**
	 * Carrega as apis no lista api e o select principal 'pai'
	 *
	 * @param  mixed $request
	 *
	 * @return view
	 */
	public function listaApis (Request $request)
	{	
		$tipo		 = 'TodasAtivasInativas';
		$apisGrid 	 = Api::todasAtivas('', '', $tipo);
		$apisSelect  = $this->apisSelect();
		$inibiCampos = false;
		$idSegmento  = explode(',', $request->input('segmento'));
		$collection  = explode(',', $request->input('collection'));
		$status = $request->input('status');

		if($request->isMethod('get') && $request->filled('collection') && $request->filled('segmento') && $idSegmento[0] != '0' && $collection[0] != '0'){
			// $inibiCampos = true;

			$apisGrid = Api::listaApiSelect($collection, $idSegmento);

			return view('alarmistica.lista_api', compact('apisGrid', 'apisSelect', 'inibiCampos'));

		} elseif($request->filled('status') && $status != '0') {
			$apisGrid = Api::listaApiSelect('', '', $status);

			return view('alarmistica.lista_api', compact('apisGrid', 'apisSelect', 'inibiCampos'));
		}

		## Caso busque por apis especificas
		if($request->isMethod('get') && $request->filled('ids')){
			$idsApis 	 		  = explode(',', $request->input('ids'));
			$inibiCampos 		  = true;
			$apiPercentualApurado = [];

			foreach($idsApis as $valor):
				$id = explode('-', $valor);
				$dados = [$id[0] => $id[1]];
				array_push($apiPercentualApurado, $dados);
			endforeach;

			$resultado = Api::todasAtivas($idsApis, $group = true); 
			$apisGrid = [];

			foreach($resultado as $item):
				foreach($apiPercentualApurado as $api):

					if(key($api) == $item['api']){
						$item['percentual_atual'] = $api[$item['api']];
						array_push($apisGrid, $item);
						continue;
					}

				endforeach;
			endforeach;

		} else if(empty($idsApis) || $idsApis == '') { // caso venha ids das apis
			$apisGrid = [];

		}
		
		return view('alarmistica.lista_api', compact('apisGrid', 'apisSelect', 'inibiCampos'));
		
	}
	
	/**
	 * Recupera as operadoras no select principal 'pai'
	 *
	 * @return array
	 */
	public static function apisSelect ()
	{
		$apiSelect 		 = Api::todasAtivas('', '', 'TodasAtivasInativas');
		$arrApis	 	 = [];
		$apisSelect 	 = [];
		$todasApis 		 = [];
		$apisSelectFilho = [];

		foreach($apiSelect as $key => $api):
			$a = $api->collection;

			if($key != 0){
				if(!in_array($a, $arrApis)){
					array_push($apisSelect, $api);
				} 							

			} else {
				array_push($apisSelect, $api);
			}

			array_push($arrApis, $api->collection);
			array_push($todasApis, $api);
		endforeach;	

		return $apisSelect = ['apisSelect' 	    => $apisSelect,
							  'apisSelectFilho' => $apisSelectFilho];
		
	}

	/**
	 * Carrega o select secundario 'filho' do lista api conforme escolhe uma operadora
	 *
	 * @param  mixed $request
	 *
	 * @return json
	 */
	public function selectFilho (Request $request)
	{
		if($request->isMethod('post') && $request->filled('collection')){
			$collection		 = $request->input('collection');
			$result 		 = Api::apiSelect($collection);
			$apisSelectFilho = [];

			foreach($result as $item):
				array_push($apisSelectFilho, $item);
			endforeach;

			return $apisSelectFilho;
		}

		return false;
	}

	/**
	 * selectFilhoApi -  Retona todas as apis conforme a collection e o segmento
	 *
	 * @return json
	 */
	public function selectFilhoApi(Request $request) 
	{
		if($request->isMethod('post') && $request->filled('collection') && $request->filled('segmento')){
			
			$collection = $request->input('collection');
			$segmento   = $request->input('segmento');
			$resultApis	= Api::selectFilhoApi($collection, $segmento);
			$apis		= [];

			foreach($resultApis as $item):
				array_push($apis, $item);
			endforeach;

			return $apis;
		}

		return false;
	
	}

	/**
	 * Recupera dados referente aos ultimos log com o status das apis instaveis
	 *
	 * @return json
	 */
	public function listaLogApisGrafico ()
	{
		$log 		  = Log::retornaLogGrafico();
		$dadosGrafico = ['data' 	  => [],
						 'lineColors' => []];
		$dados_data = [];
		$data_nova = [];
		$data = [];

		foreach($log as $item):

			// $dadosGrafico['data'] = [=>];

			// if(!in_array($data_nova, $item->data_hora))
			//    array_push($data_nova, $item->data_hora);

			// if()   

			$dados_data = ['data_hora' 	   	   => $item->data_hora,
							'api_nome'  	   => $item->nome,
							'api_ykeys'  	   => $item->consulta,
							'estado_atual'	   => $item->estado_atual,
							'percentual_atual' => $item->percentual_atual];

			// array_push($dadosGrafico['data'], $dados_data); 

			// $lineColors = ['colors' => $item->estado_atual == 'positivo' ? '#2cbe4e' : '#cb2431'];

			// array_push($dadosGrafico['lineColors'], $lineColors);
		endforeach;

			// dd($dadosGrafico);

		return json_encode($log);
		// dd($log);
	}

	/**
	 * listaLogs - pega os logs de status das apis
	 *
	 * @param  mixed $request
	 *
	 * @return json
	 */
	public function listaLogs (Request $request)
	{
		$apisSelect  = $this->apisSelect();
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '1024M');
		if($request->isMethod('get') && $request->filled('collection') && $request->filled('segmento') && $request->filled('dataInicio')){
			$idSegmento  = explode(',', $request->input('segmento'));
			$collection  = explode(',', $request->input('collection'));
			$dataInicio  = explode(',', $request->input('dataInicio'));
			$dataFim     = explode(',', $request->input('dataFim'));
			
			$situacao    = explode(',', $request->input('situacao'));
			$api         = explode(',', $request->input('api'));
			$percentual  = '';

			if($situacao[0] != '0'){

				switch($situacao[0]):
					case '1': $percentual = 'percentual >= range_positivo';
						break;
					case '2': $percentual = 'percentual < range_positivo AND percentual > range_negativo';
						break;
					case '3': $percentual = 'percentual < range_negativo';
						break;
				endswitch;						
			}
			
			$logsGrid['data'] = Log::listaLogsSelect($collection, $idSegmento, $api, $percentual, $dataInicio, $dataFim);
		} else {
		   return view('alarmistica.lista_logs', compact('apisSelect'));
		}

		return json_encode($logsGrid);
		// return view('alarmistica.lista_logs', compact('logsGrid', 'apisSelect'));
	}

	 /**
     * listaPessoasSms - recupera os usuarios que vão receber sms quando a api estiver ruim.
     *
     * @return void
     */
    public static function listaPessoasSms ($api)
    {
		$pessoasSms = [];
		
        if($api) {
                
            foreach($api as $sms):
				foreach($sms->sms as $userSms):
                    $pessoa = User::find($userSms->usuario);
                    array_push($pessoasSms, json_decode($pessoa));
                endforeach;
            endforeach;

        }
        
        return $pessoasSms;
	}
	
	/**
	 * listaHorarioSms - retorna o intervalo de horario da api em questão
	 *
	 * @return void
	 */
	public static function listaHorarioSms ($api)
	{	
		$horarios = [];

		if($api) {
			foreach($api as $horario):
				foreach($horario->configuracao->restricao->horario as $hora):
					
						$de  = new \DateTime($hora->de, new \DateTimeZone('America/Sao_Paulo'));
						$ate = new \DateTime($hora->ate, new \DateTimeZone('America/Sao_Paulo'));

						$de = $de->format('H:i:s');
						$ate = $ate->format('H:i:s');
			
					$horarios = ['de'  => $de,
								 'ate' => $ate];
				endforeach;
			endforeach;
		}

		return $horarios;
	}

	/**
	 * listaTempoVerificacaoSms - retorna o tempo do cron (schedule) que esta configurado para verificar a api em questão
	 *
	 * @return void
	 */
	public static function listaTempoVerificacaoSms ($api)
	{
		if($api) {
			foreach($api as $schedule):
				return $schedule->configuracao->consulta_schedule;
			endforeach;
		}
	}

	/**
	 * listaTempoVerificacaoSms - retorna o tempo do cron (schedule) que esta configurado para verificar a api em questão
	 *
	 * @return void
	 */
	public static function listaItensAlarmantesSms ($api)
	{
		if($api) {
			foreach($api as $itens):
				return $itens->configuracao->itens_alarmantes;
			endforeach;
		}
	}

	/**
	 * segmentoApi - retorna o segmento da api em questáo
	 *
	 * @return void
	 */
	public static function segmentoApi ($api)
	{
		
	}	
	
	/**
	 * Exporta em csv resultados do grid com logs
	 *
	 * @return csv
	 */
	public function exportarDadosLogs (Request $request)
	{
		// dd($request);
		$dataAtual = date('d-m-Y H:i:s');
		$ids_log   = explode(',', $request->todos_id_log);
		$logs 	   = Log::retornaDadosCsv($ids_log);

		Excel::create('logs_'. $dataAtual, function($excel) use ($logs) {
			
			$excel->sheet('Sheet', function($sheet) use ($logs) {
				$sheet->fromArray($logs);
			});
			
		})->download('xlsx');
		
		// if(empty($logs)){
		// 	return true;
		// } else {
		// 	return false;
		// }
		
	}

	public function backlog() {
		return view('alarmistica.backlog', ['result' => null]);
	}

	public function backlogImportar(Request $request) {
		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		try {
			$request->planilha->storeAs('planilhas', 'alarmistica.csv');
			$file = \Storage::get('planilhas/alarmistica.csv');
			$arquivo = explode("\n", $file);
			\App\Models\Alarmistica\LimitesBacklog::query()->delete();
			array_shift($arquivo);
			
			$dados = array();
			$i=0;
			
			foreach ($arquivo as $linha) {
				$colunas = explode(";", $linha);
				if ($colunas[0] != '') {
					$dados[$i]['operadora'] = str_replace('"', '', utf8_encode($colunas[0]));
					$dados[$i]['modalidade'] = str_replace('"', '', utf8_encode($colunas[1]));
					$dados[$i]['tipo_plano'] = str_replace('"', '', utf8_encode($colunas[2]));
					$dados[$i]['segmento'] = str_replace('"', '', utf8_encode($colunas[3]));
					$dados[$i]['status'] = str_replace('"', '', utf8_encode($colunas[4]));
					$dados[$i]['dia_semana'] = str_replace('"', '', utf8_encode($colunas[5]));
					$dados[$i]['hora'] = $colunas[6];
					$dados[$i]['q1'] = $colunas[7];
					$dados[$i]['q2'] = $colunas[8];
					$dados[$i]['q3'] = $colunas[9];
					$dados[$i]['ls'] = $colunas[10];
					$i++;
				}
			}

			$backlog = new \App\Models\Alarmistica\LimitesBacklog();
			foreach (array_chunk($dados,1000) as $d) {
				$backlog->insert($d);
			}

			return view('alarmistica.backlog', ['result' => 'Arquivo importado com sucesso']);
		}
		catch (Exception $ex) {
			return view('alarmistica.backlog', ['result' => 'Erro ao importar']);
		}
		catch (Error $ex) {
			return view('alarmistica.backlog', ['result' => 'Erro ao importar']);
		}
	}
}