<?php

namespace App\Http\Controllers\Despachador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Despachador\Exportador;
use App\Jobs\Despachador\Exportacao as Job;

class ExportadorController extends Controller
{
	public function exportar (Request $request, $exportador)
	{
		if (!$exportador = Exportador::find($exportador))
			return abort(404);

		if ($request->isMethod('post'))
		{
			ini_set('max_execution_time', 0);
			ini_set('memory_limit', '1G');

			return $exportador->GerarExportacao($request);

			Job
				::dispatch(...$exportador->GerarExportacao($request))
				->onConnection('despachador')
			;

			return redirect()->route('despachador.exportar', [$exportador->id])->with(['message' => 'Dados enviados para exportação com sucesso!']);
		}

		return view('despachador.exportar', [
			'exportador'	=> $exportador,
			'obtencao'		=> $exportador->obtencao->tipo::info(),
		]);
	}
}