<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CronController extends Controller {

    public function Lista(Request $request) {
        $resultado = null;

        if ($request->isMethod('post')) {
            $resultado = $this->atualizar();
        }

        $crons = \App\Models\Cron\Crons::orderBy('categoria')->orderBy('nome')->get();
        $lista = [];
        foreach ($crons as $cron) {
            $lista[$cron->categoria][] = $cron;
        }

        return view('cron/lista', [
                'resultado' => $resultado,
                'lista' => $lista,
                'format' => new \App\Helpers\Crons\Format()
            ]
        );
    }

    private function atualizar() {
        $dados = \App\Models\Cron\Crons::where('status', 1)->orderBy('categoria')->orderBy('nome')->get();
        $envio = [];

        foreach ($dados as $d) {
            $envio[$d['categoria']][] = $d;
        }

        $ch = curl_init(env('MAGENTO_API_HOST_INTERNO_CRON').'/api/v1/cron/redefinir');
        $header[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($envio));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        $res = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $msg = null;
        if ($httpcode != 200) {
            $msg = json_decode($res, true)['mensagem'];
            \Log::channel('cron')->error('Erro ao atualizar crons: '.$msg);
        }

        return ($httpcode == 200) ? 'Atualizado com sucesso' : 'Erro ao atualizar: '.$msg;
    }

    public function editar(Request $request, $id=null) {
        $salvo = null;
        $cron = \App\Models\Cron\Crons::find($id);
        if ($cron == null) {
            $cron = new \App\Models\Cron\Crons();
        }
        
        if ($request->isMethod('post')) {
            if ($request->submit == 'salvar') {
                $cron->nome = $request['nome'];
                $cron->hora = $this->getValorAgendamento($request['tipo_hora'], 'hora', $request);
                $cron->minuto = $this->getValorAgendamento($request['tipo_minuto'], 'minuto', $request);
                $cron->dia_semana =  @implode(",", $request['dia_semana']);
                $cron->dia_mes = @implode(",", $request['dia_mes']);
                $cron->mes = @implode(",", $request['mes']);
                $cron->timeout = $request['timeout'];
                $cron->comando = $request['comando'];
                $cron->parametro_adicional = $request['parametro_adicional'];
                $cron->descricao = $request['descricao'];
                $cron->categoria = (!empty($request['categoria'])) ? \mb_strtoupper($request['categoria']) : 'GERAL';
                $cron->status = $request['status'];
                $cron->save();
                $salvo = true;
            }

            if ($request->submit == 'excluir') {
                $cron->delete();

                return redirect()->route('cron.lista');
            }
        }
        
        return view('cron/editar', ['cron' => $cron, 'salvo' => $salvo]);
    }

    public function ativar(Request $request, $id=null) {
        $cron = \App\Models\Cron\Crons::find($id);
        $cron->status = !$cron->status;
        $cron->save();
        
        return ['OK' => true];
    }

    private function getValorAgendamento($tipo, $campo, $request) {
        if ($tipo == 'intervalo') {
            return "*/".$request[$campo.'_intervalo'];
        }

        if ($tipo == 'fixo') {
            return @implode(",", $request[$campo.'_fixo']);
        }
    }

}