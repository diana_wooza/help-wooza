<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Collection;

class TestController extends Controller
{
	//\App\Helpers\QueryBuilder::ToSqlWithBindings()	
	public function __invoke(Request $request)
	{
		$file = \fopen(Storage::disk('contestador_arquivos')->path('lPpqlMQH5sFAtiPQDGgp90Q6Ohu1VuLUPjWVPvgL.txt'), 'r');

		$headers = \fgetcsv($file, 0, ';');
		$headers = \array_map('\App\Helpers\Utils::CleanupDataEncoding', $headers);

		dd($headers);

		// return \App\Models\Contestador\Processo::NaoExpirados()->get()->pluck('filenames')->flatten();

		// $operadora = \App\Models\Operadora::find(2);
		// $receitas = \App\Models\Contestador\Receita::whereIn('id', [
		// 	12828690,
		// ])->get();
		// $regra = \App\Models\Contestador\Regra::Find(3);

		// dd($regra->ExecutarQuery($receitas, $operadora));

		// $processo = \App\Models\Contestador\Processo::find(152);

		// \App\Jobs\Contestador\Marcador::dispatchNow($processo);
		
		return 'Completou';
	}
}