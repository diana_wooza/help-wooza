<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class PortalController extends Controller {

    public function grid(Request $request) {
        ini_set('max_execution_time', 0);
        
        if (empty($request->get('codigo_pedido')) && empty($request->get('cpf')) && empty($request->get('linha'))) {
            return view('portal.grid', ['pedidos' => null]);
        }

        $paginatedItems = null;

        if (count($request->all()) > 0) {
            $ch = curl_init(env('ROTA_PORTAL_MAGENTO').'/pedidos');
            $header[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request->all()));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);

            $pedidos = json_decode(curl_exec($ch), true);

            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
    
            // Create a new Laravel collection from the array data
            $itemCollection = collect($pedidos);
    
            // Define how many items we want to be visible in each page
            $perPage = 20;
    
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
    
            // Create our paginator and pass it to the view
            $paginatedItems = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
    
            // set url path for generted links
            $paginatedItems->setPath($request->url());
        }

        return view('portal.grid', ['pedidos' => $paginatedItems]);
    }

    public function dadosPedido(Request $request) {
        $ch = curl_init(env('ROTA_PORTAL_MAGENTO').'/dados_pedido/'.$request['pedido']);
        $header[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request->post()));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $dados = json_decode(curl_exec($ch), true);

        $html = '';
        $html .= "<tr>";
        $html .= "    <td>Pedido</td>";
        $html .= "    <td>".$request['pedido']."</td>";
        $html .= "</tr>";
        $html .= "    <td>Data</td>";
        $html .= "    <td>".$dados['data']."</td>";
        $html .= "</tr>";
        $html .= "    <td>Hora</td>";
        $html .= "    <td>".$dados['hora']."</td>";
        $html .= "</tr>";
        $html .= "    <td>IP</td>";
        $html .= "    <td>".$dados['ip']."</td>";
        $html .= "</tr>";
        $html .= "    <td>Linha</td>";
        $html .= "    <td>".$dados['linha']."</td>";
        $html .= "</tr>";
        $html .= "    <td>Plano solicitado</td>";
        $html .= "    <td>".$dados['plano']."</td>";
        $html .= "</tr>";
        $html .= "    <td>Nome solicitante</td>";
        $html .= "    <td>".$dados['nome']."</td>";
        $html .= "</tr>";
        $html .= "    <td>CPF</td>";
        $html .= "    <td>".$dados['cpf']."</td>";
        $html .= "</tr>";
        $html .= "    <td>CEP</td>";
        $html .= "    <td>".$dados['cep']."</td>";
        $html .= "</tr>";
        $html .= "    <td>Endereço</td>";
        $html .= "    <td>".$dados['endereco']."</td>";
        $html .= "</tr>";
        $html .= "    <td>Cidade</td>";
        $html .= "    <td>".$dados['cidade']."</td>";
        $html .= "</tr>";
        $html .= "    <td>Estado</td>";
        $html .= "    <td>".$dados['estado']."</td>";
        $html .= "</tr>";

        return $html;
    }

    protected function guard() {
        return Auth::guard('portal');
    }

    public function login(Request $request) {
        try {
			if ($request->isMethod('post')) {
                $login = [
                    'username' => $request['username'],
                    'password' => $request['password']
                ];

                if (\Auth::guard('portal')->attempt($login)) {
                    if (\Auth::guard('portal')->user()->senha_valida == 0) {
                        return redirect()->route('portal.alterar_senha_portal');
                    }
                    
                    return redirect()->route('portal.grid_portal');
                }

                throw new \Exception("Erro ao autenticar");
            }

            return view('portal.login');
		}
		catch (\Exception $exception) {
            return view('portal.login')->withErrors([$exception->getMessage()]);
		}
    }

    public function alterarSenha(Request $request) {
        if ($request->isMethod('post')) {
            $usuario = \App\Models\Portal\UsuarioPortal::find(\Auth::guard('portal')->user()->id);
            
            if (!\Hash::check($request->senha_atual, $usuario->password)) {
                return view('portal.senha')->withErrors("Senha atual incorreta");
            }

            if ($request->nova_senha != $request->confirmacao) {
                return view('portal.senha')->withErrors("confirmação de senha incorreta");
            }

            $usuario->password = \Hash::make($request->nova_senha);
            $usuario->senha_valida = 1;
            $usuario->save();

            return redirect()->route('portal.grid_portal');
        }

        return view('portal.senha');
    }

    public function logout (Request $request) {
		$request->session()->flush();
		\Auth::guard('portal')->logout();

		return redirect()->route('portal.login_portal');
    }
    
}