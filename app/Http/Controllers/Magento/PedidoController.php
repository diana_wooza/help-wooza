<?php

namespace App\Http\Controllers\Magento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use \App\Models\Magento\Consumer\Order;

class PedidoController extends Controller
{
	public function atualizar_nomenclatura (Request $request)
	{
		$processaPedidos = function ($pedidos) {

			$pedidos = preg_split('/[.;,\s|\n]/', $pedidos);
			$pedidos = array_values(array_filter($pedidos, function ($pedido) {
				return preg_match('/[a-zA-Z]{1,3}[0-9]{7}/', $pedido);
			}));

			return $pedidos;

		};

		#define as regras de validação do POST
		$regrasValidacao = [
			'pedidos'		=> [function ($attribute, $value, $fail) use ($processaPedidos) {

				if (empty($value))
					return $fail('Você precisa definir uma lista de pedidos que serão atualizados');

				$value = $processaPedidos($value);
				
				if (empty($value) || count($value) <= 0)
					return $fail('Nenhum pedido foi encontrado nos dados enviados');
			}],
		];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$pedidos = collect($processaPedidos($request->input('pedidos')));
			
			\Log::channel('nomenclaturasap')->notice('O usuário ' . \Auth::user()->nome . ' atualizou a nomenclatura dos seguintes pedidos: ' . $pedidos->toJson());

			foreach ($pedidos->chunk(2500) as $chunk)
			{
				DB::connection('magento')->update(

					"UPDATE
						webjump_sales_additional_information
						INNER JOIN sales_flat_order ON sales_flat_order.entity_id = webjump_sales_additional_information.parent_id
						INNER JOIN sales_flat_order_item ON sales_flat_order_item.order_id = sales_flat_order.entity_id AND sales_flat_order_item.product_type = 'virtual' AND sales_flat_order_item.product_id = webjump_sales_additional_information.product_id
						INNER JOIN catalog_product_entity ON catalog_product_entity.sku = sales_flat_order_item.sku
						INNER JOIN catalog_product_entity_varchar ON catalog_product_entity_varchar.entity_id = catalog_product_entity.entity_id AND catalog_product_entity_varchar.attribute_id = '170'
					SET
						webjump_sales_additional_information.sap_identifier = catalog_product_entity_varchar.value
					WHERE
						catalog_product_entity_varchar.value != webjump_sales_additional_information.sap_identifier
						AND sales_flat_order.increment_id IN ({$chunk})
					"
				
				);
			}

			return redirect()->route('magento.nomenclaturasap')->with(['message' => '<strong>' . $pedidos->count() . ' pedidos</strong> tiveram sua nomenclatura sap atualidas com sucesso!']);
		}

		return view('magento.nomenclaturasap');
	}

	public function dia_vencimento (Request $request)
	{
		$loggedUser = \Auth::user();

		if (!$loggedUser->estaVinculadoComMagento() && !$loggedUser->vincularAutomaticamenteMagento(true))
			redirect()->route('magento.vincular');

		$processaPedidos = function ($pedidos) {

			$pedidos = preg_split('/[.;,\s|\n]/', $pedidos);
			$pedidos = array_values(array_filter($pedidos, function ($pedido) {
				return preg_match('/[a-zA-Z]{1,3}[0-9]{7}/', $pedido);
			}));

			return $pedidos;

		};

		#define as regras de validação do POST
		$regrasValidacao = [
			'dia'			=> [function ($attribute, $value, $fail) {

				if (empty($value))
					return $fail('Você precisa especificar um dia');
			}],
			'pedidos'		=> [function ($attribute, $value, $fail) use ($processaPedidos) {

				if (empty($value))
					return $fail('Você precisa definir uma lista de pedidos que terão seus dias modificados');

				$value = $processaPedidos($value);
				
				if (empty($value) || count($value) <= 0)
					return $fail('Nenhum pedido foi encontrado nos dados enviados');
			}],
		];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$dia = $request->input('dia');
			$magentoUser = $loggedUser->GetUsuarioMagento();
			$pedidos = collect($processaPedidos($request->input('pedidos')));
			$pedidosAtualizados = 0;

			\Log::channel('dia_vencimento')->notice('O usuário ' . \Auth::user()->nome . ' editou a data de vencimento para o dia ' . $dia . ' dos pedidos: ' . $pedidos->toJson());

			foreach ($pedidos->chunk(500) as $chunk)
			{
				$listaPedidos = \App\Models\Magento\Consumer\Order::whereIn('increment_id', $chunk->toArray())->get()->pluck('entity_id');
				$insertLog = [];

				foreach ($listaPedidos as $itemPedido)
				{
					$insertLog[] = [
						'order_id'		=> $itemPedido,
						'admin_user_id'	=> $magentoUser->user_id,
						'chave'			=> 'dia_vencimento',
						'valor'			=> $dia,
					];
				}

				\App\Models\Magento\Consumer\Log\Order::insert($insertLog);
				\App\Models\Magento\Consumer\Order::whereIn('entity_id', $listaPedidos->toArray())->update(['dia_vencimento' => $dia]);
				$pedidosAtualizados += $listaPedidos->count();
			}

			return redirect()->route('magento.dia_vencimento')->with(['message' => '<strong>' . $pedidosAtualizados . ' pedidos</strong> tiveram sua data de vencimento atualizadas para o dia <strong>' . $dia . '</strong> com sucesso!']);
		}

		return view('magento.dia_vencimento', [
			'dias'	=> [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
		]);
	}

	public function get_json_api_tim ($engine, $versao, $json, $pedido, $simulacao = false)
	{
		$pedido = Order::with(['billing_address','customer','order_custcode','banco'])->where('increment_id', $pedido)->first();

		if (is_null($pedido))
			abort(404);

		$simulacao = $simulacao !== 'avera';

		switch ($json)
		{
			case 'elegibilidade':
		
				switch ($engine)
				{
					case 'cubo':

						return '<pre>' . str_replace('\/', '/', json_encode($pedido->Tim_GetData_JSON_Elegibilidade_Robo($versao), JSON_PRETTY_PRINT)) . '</pre>';

					case 'tim':
						
						return '<pre>' . str_replace('\/', '/', json_encode($pedido->Tim_GetData_JSON_Elegibilidade_Tim($versao), JSON_PRETTY_PRINT)) . '</pre>';
				}

				break;

			case 'order':

				switch ($engine)
				{
					case 'tim':
					
						return '<pre>' . str_replace('\/', '/', json_encode($pedido->Tim_GetData_JSON_Order_Tim($versao, $simulacao), JSON_PRETTY_PRINT)) . '</pre>';
					
					case 'cubo':
						
						break;
				}

				break;
		}
		
		abort('Request inválido');
	}

	public function listar (Request $request)
	{
		$dados = Order::orderBy('entity_id', 'DESC');

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Lista de Pedidos do Magento',
			'colunas'			=> [
				['classe'=>'text-left','style'=> 'width:50px;', 'rotulo'=>'Pedido', 'campo'=>'increment_id'],
				['classe'=>'text-center', 'style'=> 'width:50px;', 'rotulo'=>'Operadora', 'campo'=>'plan_operator_bullet'],
				['classe'=>'text-center', 'style'=> 'width:100px;', 'rotulo'=>'Status', 'campo'=>function ($model) {
					
					if (is_null($model->status))
						return '<i class="text-gray text-muted">não informado</i>';

					return $model->status;
				
				}],
				['classe'=>'text-center', 'style'=> 'width:100px;', 'rotulo'=>'Modalidade', 'campo'=>'plan_modality'],
				['classe'=>'text-center', 'style'=> 'width:100px;', 'rotulo'=>'Data', 'campo'=>function ($model) {
					return \date('d/m/Y H:i:s', strtotime($model->created_at));
				}],
				['classe'=>'text-left', 'style'=> 'width:auto;', 'rotulo'=>'Cliente', 'campo'=>'customer_fullname'],
				['classe'=>'text-center', 'style'=> 'width:100px;', 'rotulo'=>'CPF', 'campo'=>'customer_cpf'],
				['classe'=>'text-left', 'style'=> 'width:150px;', 'rotulo'=>'Nome do Plano', 'campo'=>'plan_name'],
				['classe'=>'text-center', 'style'=> 'width:80px;', 'rotulo'=>'Linha', 'campo'=>function ($model) {

					if (is_null($model->phone_service))
						return '<i class="text-gray text-muted">não informado</i>';
					
					return $model->phone_service;
				
				}],
				['classe'=>'text-center', 'style'=> 'width:60px;', 'rotulo'=>'CEP', 'campo'=>'postcode'],
				['classe'=>'text-center', 'style'=> 'width:20px;', 'rotulo'=>'UF', 'campo'=>'region'],
				['classe'=>'text-center', 'style'=> 'width:100px;', 'rotulo'=>'Origem', 'campo'=>'origem_pedido'],
				['classe'=>'text-center no-print','style'=> 'width:50px;', 'rotulo'=>'Ações','campo'=>function($item) {
					$acoes = [
						'<a class="btn btn-default" href="' . route('magento.pedido.ver', [$item->entity_id]) . '" title="Ver informações do pedido"><i class="fa fa-eye"></i></a>',
						#'<a class="btn btn-default" href="' . route('colmeia.armario.editar', ['id' => $item->id]) . '"><i class="fa fa-edit"></i></a>',
					];
					
					return '<div class="btn-group">' . implode('', $acoes) . '</div>';
				}],
			],
			'dados'				=> $dados->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function ver (Request $request, $pedido)
	{
		$pedido = Order::with(['history','store'])->findOrFail($pedido);

		return view('magento.pedido_ver', [
			'pedido'			=> $pedido,
		]);
	}
}