<?php

namespace App\Http\Controllers\Magento;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Motivos\Motivos;

class MotivoController extends Controller
{
	public function listar (Request $request)
	{
		$motivos = Motivos::listarStatusMotivos();

        return view('magento.status_motivo_listar', ['dados' => $motivos,]);
    }

    public function criar(Request $request) 
    {
        $status = Motivos::listarStatus();
        $r = null;

        if( $request->isMethod('post') ){
           Motivos::criar((object)$request->only('status', 'motivo', 'label'));

            return redirect()->route('magento.motivos.listar');
        }

        return view('magento.status_motivo_criar', ['dados' => $status]);
    }
}