<?php

namespace App\Http\Controllers\Magento;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\AdminUserBase\AdminUserBase;

class AdminUserBaseController extends Controller
{
	public function listar (Request $request)
	{
		$model = AdminUserBase::listar();

        return view('magento.adminuser_listar', ['dados' => $model,]);
    }

    public function criar(Request $request) 
    {
        if( $request->isMethod('post') ){
            AdminUserBase::criar((object)$request->only('nome', 'usuario_loja', 'senha_loja','codigo_loja','segmento'));

            return redirect()->route('magento.adminuser.listar');
        }

        return view('magento.adminuser_criar', []);
    }
}