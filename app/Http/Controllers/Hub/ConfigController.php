<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Config;

class ConfigController extends Controller
{
	public function geral (Request $request)
	{
		$configs = Config::get();

		if ($request->isMethod('post'))
		{
			foreach ($configs as $config)
			{
				$config->valor = $config->tratarValorInput($request->input($config['chave']));
				$config->save();
			}

			return redirect()->route('hub.config')->with(['message' => 'Configurações atualizadas com sucesso']);
		}

		return view('_generico.config', [
			'title'			=> 'Gerenciar Configurações do Hub',
			'configs'		=> $configs,
			'list'			=> [
				'contato_hub'	=> Config::getListaUsuariosHubParaSelect(),
			],
		]);
	}
}