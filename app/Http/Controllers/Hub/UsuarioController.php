<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Mail\Hub\NovaSenha as EmailNovaSenha;
use App\Models\User;
use App\Models\Perfil;
use Illuminate\Support\Facades\URL;

class UsuarioController extends Controller
{
	public function lista (Request $request)
	{
		$dados = User
			::with(['perfis'])
			->comOrdenacao($request)
			->comFiltro($request)
		;

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Lista de Usuários do Sistema',
			'filtros'			=> [
				['chave'=>'nome', 'rotulo'=>'Nome do Usuário', 'tipo'=>'text', 'colunas'=>9, 'detalhes'=>'Caso queira pesquisar por mais de um nome separe usando vírgula, ponto e vírgula, espaços, etc.'],
				['chave'=>'perfil', 'rotulo'=>'Perfil', 'tipo'=>'multiselect', 'itens'=>Perfil::orderBy('nome', 'ASC')->get()->pluck('nome','id'), 'colunas'=>3],
			],
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'#', 'campo'=>'id', 'sort'=>'id'],
				['classe'=>'text-left', 'style'=>'width:auto; min-width:150px;', 'rotulo'=>'Nome', 'campo'=>'nome', 'sort'=>'nome'],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'E-Mail', 'campo'=>'email'],
				['classe'=>'text-center', 'style'=>'width:150px;', 'rotulo'=>'Login', 'campo'=>'login', 'sort'=>'login'],
				['classe'=>'text-center', 'style'=>'width:auto;', 'rotulo'=>'Perfil', 'campo'=>function ($item) {
					return $item->getNomesPerfis();
				}],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Ações', 'campo'=>function($item) {

					$botoes = [];

					#$botoes[] = '<a href="' . route('hub.usuario.gerar.senha', ['usuario' => $item->id]) . '" class="btn btn-default fa fa-key" title="Enviar link para geração se senha"></a>';
					$botoes[] = '<a href="' . route('hub.usuario.editar', ['id' => $item->id]) . '" class="btn btn-primary fa fa-edit" title="Editar"></a>';

					return '<div class="btn-group">' . implode('', $botoes) . '</div>';
				}],
			],
			'dados'				=> $dados->paginate($request->query('por_pagina', 50)),
		]);
	}

	public function editar (Request $request, $id = null)
	{
		if (\Auth::user()->id != $id && !\Auth::user()->checkPermissao('hub.admin'))
			return redirect()->route('home');

		$usuario = User::firstOrNew(['id' => $id]);

		#define as regras de validação do POST
		$regrasValidacao = [
			'nome'			=> 'required|max:100',
			'telefone'		=> 'required|max:12',
		];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$usuario->fill($request->only('nome', 'login', 'email', 'telefone'));
			$usuario->save();

			if (\Auth::user()->checkPermissao('hub.admin'))
				$usuario->perfis()->sync($request->input('perfis'));
			
			return redirect()->route(\Auth::user()->checkPermissao('hub.admin') ? 'hub.usuario.lista' : 'hub.usuario.editar', [$usuario->id])->with(['message' => 'Usuário <strong>' . $usuario->nome . '</strong> salvo com sucesso']);
		}

		return view('hub.usuario_editar', [
			'usuario'			=> $usuario,
			'perfis'			=> Perfil::orderBy('nome')->get()->pluck('nome','id'),
		]);
	}

	public function permissionar (Request $request, $perfilKey)
	{
		$perfil = Perfil::where('key', $perfilKey)->first();
		
		if (!in_array($perfil->id, \Auth::user()->getPerfisIds()))
			return redirect()->back()->withErrors(['Apenas administradores podem acessar essa página']);

		$filhos = Perfil::where('admin_id', $perfil->id)->get();

		if ($request->isMethod('post') && $request->validate(['perfil'=>'required|array']))
		{
			foreach ($request->input('perfil') as $usuario => $toAttach)
			{
				$toDetach = [];

				foreach ($filhos as $filho)
				{
					if (!in_array($filho->id, $toAttach))
						$toDetach[] = $filho->id;
				}

				$usuario = User::find($usuario);

				$usuario->perfis()->syncWithoutDetaching($toAttach);
				$usuario->perfis()->detach($toDetach);
			}

			return redirect()->route('hub.usuario.permissionar', [$perfilKey])->with(['message' => 'Permissões salvas com sucesso!']);
		}

		return view('_generico.tabela_paginada', [
			'titulo'			=> 'Usuários e Permissões',
			'form'				=> '',
			'colunas'			=> [
				['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'#', 'campo'=>'id'],
				['classe'=>'text-center', 'style'=>'width:200px;', 'rotulo'=>'Nome', 'campo'=>function ($item) {
					return $item->nome . ' <span class="text-muted">(' . $item->login . ')</span>';
				}],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Perfil', 'campo'=>function ($item) use ($perfil, $filhos) {

					$perfis = $item->getPerfisIds();

					$select = '<select name="perfil[' . $item->id . '][]" class="form-control selectpicker" multiple>';

					foreach ($filhos as $filho)
						$select .= '<option value="' . $filho->id . '" ' . (in_array($filho->id, $perfis) ? 'selected' : '') . '>' . $filho->nome . '</option>';

					$select .= '</select>';

					return $select;
				}],
			],
			'dados'				=> User::get(),
		]);
	}

	public function gerar_senha ($usuario)
	{
		$usuario = User::findOrFail($usuario);
		$passwordUrl = URL::temporarySignedRoute('hub.usuario.mudar.senha', now()->addDay(1), ['usuario' => $usuario->id]);

		\Mail::to($usuario->email)->send(new EmailNovaSenha($usuario, $passwordUrl));

		return redirect()->back()->with(['message' => 'Um email foi enviado para ' . $usuario->email . ' com instruções para o cadastro da senha']);
	}

	public function mudar_senha (Request $request, $usuario)
	{
		if (!$request->hasValidSignature())
			return redirect()->route('login')->withErrors(['Tentativa de acesso com assinatura incorreta']);

		#define as regras de validação do POST
		$regrasValidacao = ['password' => [
			'required',
			'confirmed',
		]];

		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			$usuario = User::find($usuario);
			$usuario->password = $request->input('password');
			$usuario->save();

			return redirect()->route('login')->with(['message' => 'Senha do usuário ' . $usuario->nome . ' atualizada com sucesso']);
		}

		return view('hub.usuario_mudarsenha');
	}
}