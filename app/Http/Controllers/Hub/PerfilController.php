<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Perfil;

class PerfilController extends Controller
{
	public function lista ()
	{
		return view('_generico.tabela', [
			'titulo'			=> 'Lista de Perfís cadastrados',
			'colunas'			=> [
				#['classe'=>'text-center', 'style'=>'width:10px;', 'rotulo'=>'#', 'campo'=>'id'],
				['classe'=>'text-left', 'style'=>'width:auto;', 'rotulo'=>'Nome', 'campo'=>'nome'],
				['classe'=>'text-center', 'style'=>'width:100px;', 'rotulo'=>'Ações', 'campo'=>function($item) {

					$botoes = [];

					$botoes[] = '<a href="" class="btn btn-primary fa fa-edit" title="Editar perfil" onClick="return alert(\'Função desativada\');"></a>';

					return '<div class="btn-group">' . implode('', $botoes) . '</div>';
				}],
			],
			'dados'				=> Perfil::get(),
		]);
	}
}