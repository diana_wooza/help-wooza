<?php

namespace App\Http\Controllers\Hub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Utils;
use App\Auth\WoozaUserProvider;

class LoginController extends Controller
{
	/**
	 * Método que gera o cadastro inicial de cada pessoa
	 * @param \Illuminate\Http\Request  $request
	 * @return mixed
	 */
	public function cadastro (Request $request)
	{
		#define as regras de validação do POST
		$regrasValidacao = [
			'nome'		=> 'required|max:100',
			'telefone'	=> 'required|max:12',
		];
		
		#Pega o usuário atualmente logado
		$user = \Auth::user();

		#Remove caracters 
		if($request->isMethod('post') && $request->filled('telefone')){

			$telefone = $request->input('telefone');
			$telefone  = Utils::removeCaracterTelefone($telefone);

			$request->merge(['telefone' => $telefone]);
		}
		
		#verifica atualização de cadastro
		if ($request->isMethod('post') && $request->validate($regrasValidacao))
		{
			#pega os dados do post e salva o usuário
			$user->fill($request->only(['nome', 'telefone']))
				 ->save();

			#redireciona o usuário de volta para a página inicial da aplicação
			return redirect()->route('home');
		}
		
		#define como padrão de nome o mesmo texto do login separado por expaço (ao invés de ponto)
		$user->nome = str_replace('.', ' ', $user->login);

		#carrega a view de cadastro
		return view('hub.cadastro', ['user'=>$user]);
	}

	/**
	 * Método que recebe o post de login e tenta logar o usuário
	 * @param \Illuminate\Http\Request  $request
	 * @return mixed
	 */
	public function entrar (Request $request)
	{
		try
		{
			if ($request->isMethod('post') && \Auth::attempt($request->only('username', 'password')))
				return !empty($oldUrl = \Session::pull('url_back')) ? redirect($oldUrl) : redirect()->route('home');
		}
		catch (\Exception $exception)
		{
			return view('hub.login')->withErrors([$exception->getMessage()]);
		}

		return view('hub.login');
	}

	/**
	 * Método que desloga o usuário
	 * @param Request $request injeção do próprio laravel, não é necessário fazer mais nada
	 * @return mixed
	 */
	public function sair (Request $request)
	{
		#Limpa todos os dados de sessão e desloga o usuário
		$request->session()->flush();
		\Auth::logout();

		#Redireciona de volta para a tela de login
		return redirect()->route('login');
	}
}