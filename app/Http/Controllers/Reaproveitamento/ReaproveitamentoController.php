<?php

namespace App\Http\Controllers\Reaproveitamento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Configuracao\ConfiguracaoController;
use App\Models\Reaproveitamento\Agendamento;
use function GuzzleHttp\json_encode;
use function GuzzleHttp\json_decode;

class ReaproveitamentoController extends Controller {
	
	public function AgendarReaproveitamento(Request $request) {
        $agendamento = new Agendamento();
        $agendamentos = $agendamento->get();

        $configuracao = new ConfiguracaoController();
        $cancelamentos = implode("','", $configuracao->getValoresSelect('cod_cancelamento')->pluck('value')->toArray());
        $operadoras = implode("','", $configuracao->getValoresSelect('plan_operator')->pluck('value')->toArray());
        $modalidades = implode("','", array_column($configuracao->getValoresSelect('plan_modality'), 'value'));
        $tipos = implode("','", array_column($configuracao->getValoresSelect('plan_tipo'), 'value'));
        
		return view('reaproveitamento.agendar_reaproveitamento', [
            'agendamentos' => $agendamentos,
            'cancelamentos' => $cancelamentos,
            'operadoras' => $operadoras,
            'modalidades' => $modalidades,
            'tipos' => $tipos,
            'mensagem' => $request->mensagem
        ]);
    }

    public static function formatarData($data) {
        $d = date_create_from_format("Y-m-d H:i:s", $data);
        if (!$d) {
            $d = date_create_from_format("Y-m-d", $data);
            return $d->format('d/m/Y');
        }

        return $d->format('d/m/Y H:i');
    }

    public static function formatarDados($dados) {
        $dados = json_decode($dados, true);
        $retorno = array();

        if (isset($dados['inicio_cancelamento'])) {
            $retorno[] = "Data Inicial de Cancelamento: ".ReaproveitamentoController::formatarData($dados['inicio_cancelamento']);
        }
        if (isset($dados['fim_cancelamento'])) {
            $retorno[] = "Data Final de Cancelamento: ".ReaproveitamentoController::formatarData($dados['fim_cancelamento']);
        }
        if (isset($dados['periodo_cancelamento'])) {
            $retorno[] = "Dias a partir da data de início: ".$dados['periodo_cancelamento'];
        }
        $retorno[] = "Códigos de Cancelamento: ".ReaproveitamentoController::ValoresArray($dados['codigos_cancelamento']);
        $retorno[] = "Operadoras: ".ReaproveitamentoController::ValoresArray($dados['operadoras']);
        $retorno[] = "Modalidades: ".ReaproveitamentoController::ValoresArray($dados['modalidades']);
        $retorno[] = "Tipos: ".ReaproveitamentoController::ValoresArray($dados['tipos']);
        $retorno[] = "Segmentos: ".ReaproveitamentoController::ValoresArray($dados['segmentos']);
        $retorno[] = "Pagamentos excluídos: ".ReaproveitamentoController::ValoresArray($dados['pagamentos']);
        
        return $retorno;
    }

    private static function ValoresArray($array) {
        if (is_array($array)) {
            return implode(", ",$array);
        }
        else {
            return $array;
        }
    }

    public function SalvarAgendamento(Request $request) {
        $dados = [
            'codigos_cancelamento' => $request->codigos_cancelamento,
            'operadoras' => $request->operadoras,
            'modalidades' => $request->modalidades,
            'tipos' => $request->tipos,
            'segmentos' => $request->segmentos,
            'pagamentos' => $request->pagamentos
        ];

        switch($request->recorrencia) {
            case 'Uma Vez':
                $datas_cancelamento = explode(" - ", $request->data_cancelamento);
                $inicio_cancelamento = date_create_from_format('d/m/Y', $datas_cancelamento[0]);
                $fim_cancelamento = date_create_from_format('d/m/Y', $datas_cancelamento[1]);        
                $dados['inicio_cancelamento'] = $inicio_cancelamento->format('Y-m-d');
                $dados['fim_cancelamento'] = $fim_cancelamento->format('Y-m-d');
            break;
            default:
                $dados['periodo_cancelamento'] = $request['periodo_cancelamento'];
            break;
        }

        $data_inicio = date_create_from_format('d/m/Y H:i:s', $request->data_inicio);

        $agendamento = new Agendamento();
        $agendamento->data_inicio = $data_inicio->format('Y-m-d H:i:s');
        $agendamento->dados = json_encode($dados);
        $agendamento->recorrencia = $request->recorrencia;
        $agendamento->save();
        
		return redirect()->route('reaproveitamento.agendar_reaproveitamento', ['mensagem' => 'Agendamento salvo']);
    }

    public function ExcluirAgendamento(Request $request) {
        $id = explode("_", $request->submit);
        $agendamento = Agendamento::find($id[1]);

        if ($agendamento != null) {
            $agendamento->delete();
        }
        
        return redirect()->route('reaproveitamento.agendar_reaproveitamento', ['mensagem' => 'Agendamento excluído']);
    }

}