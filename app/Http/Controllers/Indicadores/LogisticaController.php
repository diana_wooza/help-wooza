<?php

namespace App\Http\Controllers\Indicadores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Indicadores\Relatorio;

class LogisticaController extends Controller
{
	public function consolidado (Request $request)
	{
		#dados base para preencher o subtitulo
		$ano		= $request->get('ano', date('Y'));
		$mes		= $request->get('name', date('n'));
		$hora		= $request->get('hora', 0);
		$operadora	= $request->get('operadora', false);
		$modalidade = $request->get('modalidade', false);
		$segmento	= $request->get('segmento', false);
		$tipo		= $request->get('tipo', false);

		#preencher o subtítulo da página
		$subtitulo		 = "Relatório Gerado do ano: <strong>{$ano}</strong>";
		$subtitulo		.= ", mês: <strong>{$mes}</strong>";
		$subtitulo		.= ", com base na hora: <strong>{$hora}</strong>";

		if ($operadora)
			$subtitulo	.= ", operadora: <strong>{$operadora}</strong>";

		if ($modalidade)
			$subtitulo	.= ", modalidade: <strong>{$modalidade}</strong>";
			
		if ($segmento)
			$subtitulo	.= ", seguimento:<strong>{$segmento}</strong>";

		#tratando os dados que serão exibidos
		$diasOperadora          = [];
		$collectionOperadora    = [];
		$collectionModalidade   = [];
		$diasModalidade         = [];
		$collectionTipo         = [];
		$diasTipo               = [];
		$collectionSegmento     = [];
		$diasSegmento           = [];

		// 1. forcei os labels a serem maiúsculos pois na "modalidade" estava separando uns itens de nova linha
		//    devido a terem registros com diferença de letras maiúsculas e minúsculas
		foreach (Relatorio::getLogisticaConsolidado($mes, $ano, $hora, $operadora, $tipo, $modalidade, $segmento) as $row)
		{
			$collectionModalidade[strtoupper($row->modalidade)][$row->status][$row->dia] = $row->total;
			$collectionTipo[strtoupper($row->tipo)][$row->status][$row->dia] = $row->total;
			$collectionSegmento[strtoupper($row->segmento)][$row->status][$row->dia] = $row->total;
			$collectionOperadora[strtoupper($row->operadora)][$row->status][$row->dia] = $row->total;

			// mapeia modalidades
			if( !array_key_exists( strtoupper($row->modalidade), $diasModalidade ) ) {
				$diasModalidade[ strtoupper($row->modalidade) ] = array($row->dia);
			}
			else if( !in_array( $row->dia, $diasModalidade[strtoupper($row->modalidade)] ) ) {
				array_push($diasModalidade[strtoupper($row->modalidade)], $row->dia);
			}

			// mapeia tipos
			if( !array_key_exists( strtoupper($row->tipo), $diasTipo ) ) {
				$diasTipo[ strtoupper($row->tipo) ] = array($row->dia);
			}
			else if( !in_array( $row->dia, $diasTipo[strtoupper($row->tipo)] ) ) {
				array_push($diasTipo[strtoupper($row->tipo)], $row->dia);
			}

			// mapeia segmentos
			if( !array_key_exists( strtoupper($row->segmento), $diasSegmento ) ) {
				$diasSegmento[ strtoupper($row->segmento) ] = array($row->dia);
			}
			else if( !in_array( $row->dia, $diasSegmento[strtoupper($row->segmento)] ) ) {
				array_push($diasSegmento[strtoupper($row->segmento)], $row->dia);
			}

			// mapeia operadora
			if( !array_key_exists( strtoupper($row->operadora), $diasOperadora ) ) {
				$diasOperadora[ strtoupper($row->operadora) ] = array($row->dia);
			}
			else if( !in_array( $row->dia, $diasOperadora[strtoupper($row->operadora)] ) ) {
				array_push($diasOperadora[strtoupper($row->operadora)], $row->dia);
			}
		}

		return view('indicadores.operadoras_evolucao', [

			#informações da página
			'titulo'				=> 'Logística Consolidado',
			'subtitulo'				=> $subtitulo,

			#filtros
			'ano'					=> $ano,
			'mes'					=> $mes,
			'hora'					=> $hora,
			'operadora'				=> $operadora,
			'modalidade'			=> $modalidade,
			'tipo'					=> $tipo,
			'segmento'				=> $segmento,

			#dados básicos oriundos do banco
			'anos'					=> Relatorio::getAnosLogisticaConsolidado(),
			'diasOperadora'			=> $diasOperadora,
			'collectionOperadora'	=> $collectionOperadora,
			'collectionModalidade'	=> $collectionModalidade,
			'diasModalidade'		=> $diasModalidade,
			'collectionTipo'		=> $collectionTipo,
			'diasTipo'				=> $diasTipo,
			'collectionSegmento'	=> $collectionSegmento,
			'diasSegmento'			=> $diasSegmento,

		]);
	}
}