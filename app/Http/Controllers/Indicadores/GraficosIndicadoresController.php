<?php

namespace App\Http\Controllers\Indicadores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Magento\Consumer\AdminUserBase as MagentoUserBase;
use App\Models\Indicadores\GraficoPrazoAtivacao;
use App\Models\Indicadores\GraficoTaxaAtivacao;
use App\Models\Indicadores\GraficoMetas;
use App\Models\Magento\Consumer\Order;

Class GraficosIndicadoresController extends Controller
{
    /**
     * index - chama a view do grafico
     *
     * @return mixed
     */
    public function index()
    {
        $segmentos = MagentoUserBase::getSegmentos();
        
        return view('indicadores.grafico_indicadores', compact('segmentos'));
    }

    /**
     * getDadosGraficos - recupera os dados para serrem tratados. Tambem é possivel chamar a consulta direto 
     * do banco do magento para fazer consolidação dos dados na tabela grafico_ativacao_indicadores!
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function getDadosGraficoPrazoAtivacao(Request $request)
    {
        if($request->isMethod('post')){
            $operadora = $request->input('operadora');
            $segmento  = $request->input('segmento');
            $format    = 'Y-m-00';
            $dataIncio = \DateTime::createFromFormat('d/m/Y H:i:s', $request->input('dataInicio'))->format($format);
            $dataFim   = \DateTime::createFromFormat('d/m/Y H:i:s', $request->input('dataFim'))->format($format);

            $prazoAtivacao = GraficoPrazoAtivacao::getDadosGrafico($operadora, $segmento, $dataIncio, $dataFim);

            ## Atenção: so chamar aqui se quiser atualizar(popular) a tabela pedidos_grafico_inidcadores do BD alarmistica, 
            ## pois chama a função que é chamada pelo cron diariamente. Cuidado com a data
//          $dadosMagento = Order::getDadosGraficoPrazoAtivacao($dataIncio, $dataFim);

            if(count($prazoAtivacao) == 0)
                return [];

            $dadosGrafico = $this->tratarDadosGraficoPrazoAtivacao($prazoAtivacao);
            $dadosGrafico = $this->calculaPercentual($dadosGrafico, 'prazo ativacao');

            if($dadosGrafico[0]['quantidade2Horas'] == '0,00' && $dadosGrafico[0]['quantidade6Horas'] == '0,00' )
                $dadosGrafico = [];
            
        }
        return $dadosGrafico;
    }

    /**
     * getDadosGraficoTaxaAtivacao
     *
     * @return void
     */
    public function getDadosGraficoTaxaAtivacao(Request $request)
    {

        if($request->isMethod('post')){
            $operadora = $request->input('operadora');
            $segmento  = $request->input('segmento');
            $format    = 'Y-m-00';
            $dataIncio = \DateTime::createFromFormat('d/m/Y H:i:s', $request->input('dataInicio'))->format($format);
            $dataFim   = \DateTime::createFromFormat('d/m/Y H:i:s', $request->input('dataFim'))->format($format);
            
            ## Chama a função que popula a tabela grafico_taxa_ativacao que tb é chamada pelo cron
            // $taxaAtivacao = Order::getDadosGraficoTaxaAtivacao($operadora, $segmento, $dataIncio, $dataFim);

            $taxaAtivacao = GraficoTaxaAtivacao::getDadosTaxaAtivacao($operadora, $segmento, $dataIncio, $dataFim);

            $retornoTratamento = $this->tratardadosGraficoTaxaAtivacao($taxaAtivacao);
            $retornoTratamento = $this->calculaPercentual($retornoTratamento, 'taxa ativacao');
            
        }

        return $retornoTratamento;
    }

    /**
     * tratardadosGraficoTaxaAtivacao
     *
     * @return void
     */
    private function tratardadosGraficoTaxaAtivacao($pedidos)
    {
        $dadosGrafico      = [];
        $totalPedidosGeral = 0;
        $anoMesAdd         = [];
        
        ## Separa os anos sem repetir
        foreach($pedidos as $key => $pedido):
            // dd($pedido);
            if($key == 0){
                $dadosGrafico[$key]['ano_mes']                         = $pedido->ano_mes_criacao_pedido; 
                $dadosGrafico[$key]['total_pedidos']                   = 0;
                $dadosGrafico[$key]['total_pedidos_ativados']          = 0;
                $dadosGrafico[$key]['total_pedidos_ativados_perc']     = 0;
                $dadosGrafico[$key]['total_pedidos_cancelados']        = 0;
                $dadosGrafico[$key]['total_pedidos_cancelados_perc']   = 0;
                $dadosGrafico[$key]['plano_cartao']                    = 0;
                $dadosGrafico[$key]['bot_ativacao']                    = 0;
                $dadosGrafico[$key]['bko_ativacao']                    = 0;
                $dadosGrafico[$key]['outros_ativados']                 = 0;
                $dadosGrafico[$key]['analise_preliminar_cancelamento'] = 0;
                $dadosGrafico[$key]['bot_cancelamento']                = 0;
                $dadosGrafico[$key]['bko_cancelamento']                = 0;
                $dadosGrafico[$key]['outros_cancelamento']             = 0;
                $dadosGrafico[$key]['backlog']                         = 0;
                $dadosGrafico[$key]['total_geral_perc']                = 0;
                $anoMesAdd[]                                           = $pedido->ano_mes_criacao_pedido; 

            } else {

                if(!in_array($pedido->ano_mes_criacao_pedido, $anoMesAdd)){
                    $dadosGrafico[$key]['ano_mes']                         = $pedido->ano_mes_criacao_pedido; 
                    $dadosGrafico[$key]['total_pedidos']                   = 0;
                    $dadosGrafico[$key]['total_pedidos_ativados']          = 0;
                    $dadosGrafico[$key]['total_pedidos_ativados_perc']     = 0;
                    $dadosGrafico[$key]['total_pedidos_cancelados']        = 0;
                    $dadosGrafico[$key]['total_pedidos_cancelados_perc']   = 0;
                    $dadosGrafico[$key]['plano_cartao']                    = 0;
                    $dadosGrafico[$key]['bot_ativacao']                    = 0;
                    $dadosGrafico[$key]['bko_ativacao']                    = 0;
                    $dadosGrafico[$key]['outros_ativados']                 = 0;
                    $dadosGrafico[$key]['analise_preliminar_cancelamento'] = 0;
                    $dadosGrafico[$key]['bot_cancelamento']                = 0;
                    $dadosGrafico[$key]['bko_cancelamento']                = 0;
                    $dadosGrafico[$key]['outros_cancelamento']             = 0;
                    $dadosGrafico[$key]['backlog']                         = 0;
                    $dadosGrafico[$key]['total_geral_perc']                = 0;
                    $anoMesAdd[]                                           = $pedido->ano_mes_criacao_pedido;
                    
                }
            }
            
            ## Agrupa o total de pedidos por mes
            foreach($dadosGrafico as $k => $ano_mes):
                
                if($ano_mes['ano_mes'] == $pedido->ano_mes_criacao_pedido){

                    $dadosGrafico[$k]['total_pedidos']                   += $pedido->total_pedidos;
                    $dadosGrafico[$k]['plano_cartao']                    = $pedido->tipo_usuario == 'plano_cartao' && $dadosGrafico[$k]['plano_cartao'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['plano_cartao'];
                    $dadosGrafico[$k]['bot_ativacao']                    = $pedido->tipo_usuario == 'bot_ativacao' && $dadosGrafico[$k]['bot_ativacao'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['bot_ativacao'];
                    $dadosGrafico[$k]['bko_ativacao']                    = $pedido->tipo_usuario == 'bko_ativacao' && $dadosGrafico[$k]['bko_ativacao'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['bko_ativacao'];
                    $dadosGrafico[$k]['outros_ativados']                 = $pedido->tipo_usuario == 'outros_ativados' && $dadosGrafico[$k]['outros_ativados'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['outros_ativados'];
                    $dadosGrafico[$k]['analise_preliminar_cancelamento'] = $pedido->tipo_usuario == 'analise_preliminar_cancelamento' && $dadosGrafico[$k]['analise_preliminar_cancelamento'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['analise_preliminar_cancelamento'];
                    $dadosGrafico[$k]['bot_cancelamento']                = $pedido->tipo_usuario == 'bot_cancelamento' && $dadosGrafico[$k]['bot_cancelamento'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['bot_cancelamento'];
                    $dadosGrafico[$k]['bko_cancelamento']                = $pedido->tipo_usuario == 'bko_cancelamento' && $dadosGrafico[$k]['bko_cancelamento'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['bko_cancelamento'];
                    $dadosGrafico[$k]['outros_cancelamento']             = $pedido->tipo_usuario == 'outros_cancelamento' && $dadosGrafico[$k]['outros_cancelamento'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['outros_cancelamento'];
                    $dadosGrafico[$k]['backlog']                         = $pedido->tipo_usuario == 'backlog' && $dadosGrafico[$k]['backlog'] == 0 ? $pedido->total_pedidos : $dadosGrafico[$k]['backlog'];
                    
                    if($pedido->tipo_usuario == 'bot_ativacao' || $pedido->tipo_usuario == 'bko_ativacao' || $pedido->tipo_usuario == 'outros_ativados' || $pedido->tipo_usuario == 'plano_cartao') { 
                        $dadosGrafico[$k]['total_pedidos_ativados'] += $pedido->total_pedidos;
                        $dadosGrafico[$k]['total_pedidos_ativados_perc'] += $pedido->total_pedidos;
                    }
                    
                    if($pedido->tipo_usuario == 'bot_cancelamento' || $pedido->tipo_usuario == 'analise_preliminar_cancelamento' || $pedido->tipo_usuario == 'bko_cancelamento' || $pedido->tipo_usuario == 'outros_cancelamento') { 
                        $dadosGrafico[$k]['total_pedidos_cancelados'] += $pedido->total_pedidos;
                        $dadosGrafico[$k]['total_pedidos_cancelados_perc'] += $pedido->total_pedidos;
                    }
                }

            endforeach;

            ## Total de pedidos
            $totalPedidosGeral += $pedido->total_pedidos;

        endforeach;
        
        return $retorno = ['dadosGrafico'      => $dadosGrafico,
                            'totalPedidosGeral' => $totalPedidosGeral];
    }
    
    /**
     * tratarDadosGrafico - Separa todos os anos da pesquesa e depois faz o filtro de todos dos que se encaixam na regra de 2 horas ou 6 horas
     *
     * @param  mixed $pedidosMagento
     *
     * @return void
     */
    private function tratarDadosGraficoPrazoAtivacao($pedidosMagento)
    {     
        $dadosGrafico      = [];
        $totalPedidosGeral = 0;
        $anoMesAdd         = [];

        ## Separa os anos sem repetir
        foreach($pedidosMagento as $key => $pedido):
            
            if($key == 0){
                $dadosGrafico[$key]['ano_mes'] = $pedido->ano_mes_ativacao; 
                $dadosGrafico[$key]['total_pedidos'] = 0;
                $anoMesAdd[] = $pedido->ano_mes_ativacao; 

            } else {

                if(!in_array($pedido->ano_mes_ativacao, $anoMesAdd)){
                    $dadosGrafico[$key]['ano_mes'] = $pedido->ano_mes_ativacao; 
                    $dadosGrafico[$key]['total_pedidos'] = 0;
                    $anoMesAdd[] = $pedido->ano_mes_ativacao;
                    
                }
            }
            
            ## Agrupa o total de pedidos por mes
            foreach($dadosGrafico as $k => $ano_mes):
                
                if($ano_mes['ano_mes'] == $pedido->ano_mes_ativacao){
                    $dadosGrafico[$k]['total_pedidos'] += $pedido->soma_horas_de_ativacao;

                }

            endforeach;

            ## Total de pedidos
            $totalPedidosGeral += $pedido->soma_horas_de_ativacao;

        endforeach;

        foreach($pedidosMagento as $chave => $pedido):

            foreach($dadosGrafico as $key => $data):
            
                if($pedido->horas_de_ativacao <= 2 && $data['ano_mes'] == $pedido->ano_mes_ativacao) { ## caso tenha ocorrido em ate 2h a ativacao
                   $somaAtual = 0;
                    
                   if(!empty($dadosGrafico[$key]['quantidade2Horas'])) ## verifica se tem algum valor pra somar com o novo valor
                        $somaAtual = $dadosGrafico[$key]['quantidade2Horas'];
                    
                    $dadosGrafico[$key]['quantidade2Horas'] = $somaAtual + $pedido->soma_horas_de_ativacao;

                }
                
                if($pedido->horas_de_ativacao <= 6 && $data['ano_mes'] == $pedido->ano_mes_ativacao) {
                    $somaAtual = 0;

                    if(!empty($dadosGrafico[$key]['quantidade6Horas'])) ## verifica se tem algum valor pra somar com o novo valor
                        $somaAtual = $dadosGrafico[$key]['quantidade6Horas'];
                    
                    $dadosGrafico[$key]['quantidade6Horas'] = $somaAtual + $pedido->soma_horas_de_ativacao; 
                }
                
                if(empty($dadosGrafico[$key]['quantidade2Horas']) && $data['ano_mes'] == $pedido->ano_mes_ativacao) { ## caso não tenha nenhum pedido ativado em 2h coloca zero
                    $dadosGrafico[$key]['quantidade2Horas'] = 0;
                }
                
                if(empty($dadosGrafico[$key]['quantidade6Horas']) && $data['ano_mes'] == $pedido->ano_mes_ativacao) { ## caso não tenha nenhum pedido ativado em 6h coloca zero
                    $dadosGrafico[$key]['quantidade6Horas'] = 0;
                }

            endforeach;
            
        endforeach;

        return $retorno = ['dadosGrafico'      => $dadosGrafico,
                           'totalPedidosGeral' => $totalPedidosGeral];

    }

    /**
     * calculaPercentual - calcula o percentual para os soma das horas em relação ao total de pedidos
     *
     * @param  array $dados
     *
     * @return array 
     */
    public function calculaPercentual($dadosGrafico, $tipoGrafico)
    {
        if(!empty($dadosGrafico)){

            if($tipoGrafico == 'prazo ativacao'){
                $dadoGrafico = $dadosGrafico['dadosGrafico'];

                foreach($dadoGrafico as $key => $data):
        
                    $percentual = number_format(($data['quantidade2Horas'] / $data['total_pedidos']) * 100, 2, ',', '');
                    $dadoGrafico[$key]['quantidade2Horas'] = $percentual;
        
                    $percentual = number_format(($data['quantidade6Horas'] / $data['total_pedidos']) * 100, 2, ',', '');
                    $dadoGrafico[$key]['quantidade6Horas'] = $percentual;
                    
                endforeach;

            } else {
                
                $dadoGrafico = $dadosGrafico['dadosGrafico'];
                // dd($dadosGrafico);
                foreach($dadoGrafico as $key => $data): 
                    
                    ## Subtrai o total da analise_preliminar_cancelamento dos total de pedidos, bot cancelamento, total pedidos cancelados, total_pedidos_cancelados_perc e total pedidos geral 
                    ## pq a query de analise_preliminar_cancelamento e a mesma que busca os valores dessas varieveis, logo o valor ja esta comtabilizado.

                    $data['bot_cancelamento']              = $data['bot_cancelamento'] - $data['analise_preliminar_cancelamento'];
                    $data['total_pedidos']                 = $data['total_pedidos'] - $data['analise_preliminar_cancelamento'];
                    $data['total_pedidos_cancelados']      = $data['total_pedidos_cancelados'] - $data['analise_preliminar_cancelamento'];
                    $dadosGrafico['totalPedidosGeral']     = $dadosGrafico['totalPedidosGeral'] - $data['analise_preliminar_cancelamento'];
                    $data['total_pedidos_cancelados_perc'] = $data['total_pedidos_cancelados_perc'] - $data['analise_preliminar_cancelamento'];
                    
                    $dadoGrafico[$key]['total_pedidos'] = $data['total_pedidos'];
                    $dadoGrafico[$key]['total_pedidos_ativados'] = $data['total_pedidos_ativados'];
                    $dadoGrafico[$key]['total_pedidos_cancelados'] = $data['total_pedidos_cancelados'];
// 
                    $percentual = $data['total_pedidos'] == 0 ? 0 : round(number_format(($data['total_pedidos_ativados_perc'] / $data['total_pedidos']) * 100, 1, '.', ''),0, PHP_ROUND_HALF_EVEN);
                    $percentualGeral = round(number_format(($data['total_pedidos_ativados_perc'] / $data['total_pedidos']) * 100, 3, '.', ''), 1, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['total_pedidos_ativados_perc'] = $percentual;
                    // $dadoGrafico[$key]['total_geral_perc'] += $percentualGeral;
                    
                    $percentual = $data['total_pedidos'] == 0 ? 0 : round(number_format(($data['total_pedidos_cancelados_perc'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $percentualGeral = round(number_format(($data['total_pedidos_cancelados_perc'] / $data['total_pedidos']) * 100, 3, '.', ''), 1, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['total_pedidos_cancelados_perc'] = $percentual;
                    // $dadoGrafico[$key]['total_geral_perc'] += $percentualGeral;
                    
                    $percentual = $data['total_pedidos_ativados'] == 0 || $data['plano_cartao'] == 0 ? 0 : round(number_format(($data['plano_cartao'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['plano_cartao'] = $percentual;
                                        
                    $percentual = $data['total_pedidos_ativados'] == 0 || $data['bot_ativacao'] == 0 ? 0 : round(number_format(($data['bot_ativacao'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['bot_ativacao'] = $percentual;
                    
                    // dd($percentual, $data['total_pedidos_ativados_perc'], $data['bot_ativacao'], $data['bko_ativacao'], $data['total_pedidos'] );
                    $percentual = $data['total_pedidos_ativados'] == 0 || $data['bko_ativacao'] == 0 ? 0 : round(number_format(($data['bko_ativacao'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['bko_ativacao'] = $percentual;
                    
                    $percentual = $data['total_pedidos_ativados'] == 0 || $data['outros_ativados'] == 0 ? 0 : round(number_format(($data['outros_ativados'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['outros_ativados'] = $percentual;

                    $percentual = $data['total_pedidos_cancelados'] == 0 || $data['analise_preliminar_cancelamento'] == 0 ? 0 : round(number_format(($data['analise_preliminar_cancelamento'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['analise_preliminar_cancelamento'] = $percentual;
              
                    $percentual = $data['total_pedidos_cancelados'] == 0 || $data['bot_cancelamento'] == 0 ? 0 : round(number_format(($data['bot_cancelamento'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['bot_cancelamento'] = $percentual;

                    $percentual = $data['total_pedidos_cancelados'] == 0 || $data['bko_cancelamento'] == 0 ? 0 : round(number_format(($data['bko_cancelamento'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['bko_cancelamento'] = $percentual;
                    
                    $percentual = $data['total_pedidos_cancelados'] == 0 || $data['outros_cancelamento'] == 0 ? 0 : round(number_format(($data['outros_cancelamento'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $dadoGrafico[$key]['outros_cancelamento'] = $percentual;

                    $percentual = round(number_format(($data['backlog'] / $data['total_pedidos']) * 100, 1, '.', ''), 0, PHP_ROUND_HALF_EVEN);
                    $percentualGeral = round(number_format(($data['backlog'] / $data['total_pedidos']) * 100, 3, '.', ''), 1, PHP_ROUND_HALF_EVEN); //colocar no total_geral_perc
                    $dadoGrafico[$key]['backlog'] = $percentual;
                    $dadoGrafico[$key]['total_geral_perc'] += 100;

                endforeach;

            }
           
        } else {
            $dadoGrafico = 0;
        }

        return $dadoGrafico;
    }

    public function cadastrarMetas(Request $request)
    {
        
        if($request->isMethod('post') && $request->filled('tribo') && $request->filled('dtMeta')){
            
            $tribo        = $request->input('tribo');
            $dtMeta       = $request->input('dtMeta');
            $tipos        = $request->input('tipos');
            $metricas     = $request->input('metrica');
            $valoresMetas = [];
            $metas        = [];

            ## Verifica itens duplicados
            $verificaDuplicados = array_unique( array_diff_assoc( $tipos, array_unique( $tipos ) ) );

            if(empty($verificaDuplicados)){

                foreach( $tipos as $k => $tipo ):

                    foreach( $metricas as $v => $metrica ):
                        if( $k == $v ){
                            $arr = [$tipo => $metrica];
                            array_push($valoresMetas, $arr);

                        } 
                    endforeach;

                endforeach;

                $dtMeta = str_replace("/", "-", $dtMeta);
                $dtMeta = date('Y-m-d', strtotime('01-'.$dtMeta));

                sort($valoresMetas);

                foreach($valoresMetas as $valor):
                    array_push($metas, $valor);
                endforeach;

                $arrMetas = ['tribo'   => $tribo,
                             'ano_mes' => $dtMeta,
                             'metrica' => json_encode($metas)];

                $meta = new GraficoMetas;
                $meta->fill($arrMetas);
                $meta->save();

                return redirect()->route('indicadores.graficosindicadores.cadastrarMetas')
                                ->with(['message' => 'O cadastro da meta '. $tribo. ' foi criada com sucesso!']);
            } 
        }
        
            return view('indicadores.cadastrarMetas');

    }

        /**
     * getDadosGraficoMeta - recupera as informações para o grafico de meta
     *
     * @param  mixed $request
     *
     * @return json
     */
    public function getDadosGraficoMeta(Request $request)
    {
        if(!($request->isMethod('post') && $request->filled('periodo'))){
            $segmentos = MagentoUserBase::getSegmentos();
            
            return view('indicadores.grafico_indicadores', compact('segmentos'));

        }

        $periodo = \DateTime::createFromFormat('m/Y', $request->input('periodo'))->format('Y-m-01');
        
        $metas    = GraficoMetas::getDadosGrafico($periodo);
        $dados    = [];

        foreach($metas as $x => $itens):

            $metricas = [];
            $tribo    = [];

            foreach(json_decode($itens['metrica']) as $k => $item):
                
                if($k == 0){
                    array_push($metricas, $itens['tribo']);
                    array_push($metricas, $itens['ano_mes']);
                }
                array_push($metricas, $item);

            endforeach;

            array_push($dados, $metricas);   
            
        endforeach;
        
        // dd($dados);
        
        return json_encode($dados); 
        // return view('indicadores.grafico_indicadores', compact('segmentos', 'metas', 'metricas'));

    }

    public function setMetas(Request $request)
    {
        dd($request);
    }
}