<?php

namespace App\Models\Magento\Consumer\Jundsoft;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Models\Alarmistica\Api;

class Log extends Model
{
	protected $table			= 'log_jundsoft';
	protected $connection		= 'alarmistica_mongo';

	/**
	 * Scope
	 */

	public function scopePorPedido ($query, array $pedidos)
	{
		if (count($pedidos) >= 500)
			throw new Exception('Muitos pedidos tentando ser processados de uma única vez');

		return $query->whereIn('context.codigo_pedido', $pedidos);
	}

	public function scopeApenasEnvio ($query)
	{
		return $query->where('channel', 'envio_jundsoft');
	}

	public function scopeApenasAutenticacao ($query)
	{
		return $query->where('channel', 'autenticacao_jundsoft');
	}

	public function scopeApenasAtualizacao ($query)
	{
		return $query->where('channel', 'atualizacao_jundsoft');
	}

	public function scopeApenasFaturado ($query)
	{
		return $query
			->ApenasAtualizacao()
			->where('context.dados_requisicao.requisicao.corpo.status', 'invoiced')
		;
	}

	public function scopeApenasConsulta ($query)
	{
		return $query->where('channel', 'consulta_jundsoft');
	}
}