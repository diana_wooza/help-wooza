<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class RoboReturn extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'robo_return';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}