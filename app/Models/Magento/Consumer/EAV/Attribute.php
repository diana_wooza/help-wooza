<?php

namespace App\Models\Magento\Consumer\EAV;

use Illuminate\Database\Eloquent\Model;

use \App\Helpers\Magento as MagentoHelper;

class Attribute extends Model
{
	public $timestamps			= true;
	protected $primaryKey		= 'attribute_id';
	protected $table			= 'eav_attribute';
	protected $connection		= 'magento';

	protected $fillable			= ['attribute_id', 'entity_type_id','attribute_code','attribute_model','backend_model','backend_type','backend_table','frontend_model','frontend_input','frontend_label','frontend_class','source_model','is_required','is_user_defined','default_value','is_unique','note','is_visible_on_front','sorting_order','checkout_step','show_on_grid','store_ids','save_selected','parent_dropdown','include_pdf','apply_default','customer_groups','size_text','required_on_front_only'];
	protected $hidden			= ['entity_type_id', 'attribute_id', 'attribute_model','backend_model','backend_table','frontend_model','frontend_class','source_model','is_user_defined','is_visible_on_front','checkout_step','show_on_grid','store_ids','save_selected','parent_dropdown','include_pdf','apply_default','customer_groups','size_text','required_on_front_only'];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
	protected $with				= [];

	protected function getValueAttribute ($value)
	{
		return MagentoHelper::decrypt($value);
	}
}