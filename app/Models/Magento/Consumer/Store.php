<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class Store extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'store_id';
	protected $table			= 'core_store';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}