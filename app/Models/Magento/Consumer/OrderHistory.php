<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class OrderHistory extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'entity_id';
	protected $table			= 'sales_flat_order_status_history';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];


	public function usuario ()
	{
		return $this->hasOne(\App\Models\Magento\Consumer\AdminUser::class, 'user_id', 'admin_user_id');
	}
}