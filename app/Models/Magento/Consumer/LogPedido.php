<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

class LogPedido extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_pedidos';
	protected $connection		= 'magento';

	protected $fillable			= ['order_id','data','admin_user_id','chave','valor'];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}