<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class OrderAdicional extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'sales_flat_order_dados_adicionais';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}