<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class CustCode extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'dsf';
	protected $table			= 'sales_order_custcode';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}