<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Helpers\Magento as MagentoHelper;

class AdminUserBase extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'admin_user_base';
	protected $connection		= 'magento';

	protected $fillable			= [''];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];

	/**
	 * getSegmento - recupera somente os segmentos
	 *
	 * @return array
	 */
	public static function getSegmentos()
	{
		$segmento = self::select(DB::raw('DISTINCT(segmento)'))
					->orderby('segmento', 'desc')
					->get();

		return $segmento;					
	}
}