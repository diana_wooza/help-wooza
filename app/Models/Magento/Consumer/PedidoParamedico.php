<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class PedidoParamedico extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'pedidos_paramedico';
	protected $connection		= 'magento';

	protected $fillable			= ['pedido','enviado','enviado_at'];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [
		'enviado'					=> 'int',
	];
	protected $appends			= [];

	/**
	 * Scope
	 */

	public function scopeApenasEnviados ($query)
	{
		return $query->where('enviado', 1);
	}

	public function scopeApenasPendente ($query)
	{
		return $query->where('enviado', 0);
	}
}