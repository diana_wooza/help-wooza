<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use \App\Helpers\Magento as MagentoHelper;
use App\Models\Magento\Consumer\EAV\Attribute;

class CustomerVarchar extends Model
{
	public $timestamps			= true;
	protected $primaryKey		= 'value_id';
	protected $table			= 'customer_entity_varchar';
	protected $connection		= 'magento';

	protected $fillable			= ['entity_type_id', 'attribute_id', 'entity_id', 'value'];
	protected $hidden			= ['entity_type_id', 'attribute_id', 'entity_id'];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
	protected $with				= ['attribute'];

	protected function getValueAttribute ($value)
	{
		return MagentoHelper::decrypt($value);
	}

	public function attribute ()
	{
		return $this->belongsTo(Attribute::class, 'attribute_id', 'attribute_id');
	}
}