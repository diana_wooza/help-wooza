<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class OrderBanco extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'sales_flat_order_banco';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];

	/**
	 * Mutators
	 */

	protected function getCodigoAttribute ()
	{
		return @explode(' - ', $this->nome)[0];
	}

	protected function getBancoAttribute ()
	{
		return @explode(' - ', $this->nome)[1];
	}
}