<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class Webjump extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'entity_id';
	protected $table			= 'webjump_sales_additional_information';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}