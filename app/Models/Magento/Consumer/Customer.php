<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use \App\Helpers\Magento as MagentoHelper;
use \App\Models\Magento\Consumer\CustomerVarchar;

class Customer extends Model
{
	public $timestamps			= true;
	protected $primaryKey		= 'entity_id';
	protected $table			= 'customer_entity';
	protected $connection		= 'magento';

	protected $fillable			= ['entity_type_id', 'attribute_set_id', 'website_id', 'email', 'group_id', 'increment_id', 'store_id', 'is_active', 'disable_auto_group_change'];
	protected $hidden			= ['entity_type_id', 'attribute_set_id', 'website_id', 'group_id', 'store_id','varchar'];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= ['created_in','firstname','lastname','mother_full_name','cpf','cellphone','telephone','cnpj','razao_social','data_nascimento'];
	protected $with				= ['varchar'];
	
	private $attributos			= null;

	protected function getEmailAttribute ($value)
	{
		return MagentoHelper::decrypt($value);
	}

	protected function getCreatedInAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['created_in'];
	}

	protected function getFirstnameAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['firstname'];
	}

	protected function getLastnameAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['lastname'];
	}

	protected function getMotherFullNameAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['mother_full_name'];
	}

	protected function getCpfAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['cpf'];
	}

	protected function getCellphoneAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['cellphone'];
	}

	protected function getTelephoneAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['telephone'];
	}

	protected function getCnpjAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['cnpj'];
	}

	protected function getRazaoSocialAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['razao_social'];
	}

	protected function getDataNascimentoAttribute ()
	{
		$this->OrganizeAttributes();

		return @$this->attributos['data_nascimento'];
	}

	private function OrganizeAttributes ()
	{
		if (!is_null($this->attributos))
			return;

		foreach ($this->varchar as $attribute)
			$this->attributos[$attribute->attribute->attribute_code] = $attribute->value;
	}

	public function varchar ()
	{
		return $this->hasMany(CustomerVarchar::class, 'entity_id', 'entity_id');
	}
}