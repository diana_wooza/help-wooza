<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class OrderAgendaPortabilidade extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'entity_id';
	protected $table			= 'sales_flat_order_agenda_portabilidade';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}