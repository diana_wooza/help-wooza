<?php

namespace App\Models\Magento\Consumer\Log;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_pedidos';
	protected $connection		= 'magento';

	protected $fillable			= ['order_id','admin_user_id','chave','valor'];
	protected $hidden			= ['order_id','admin_user_id'];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];

	/**
	 * Relationship
	 */

	public function pedido ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\Order::class, 'order_id', 'entity_id');
	}

	public function usuario ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\AdminUser::class, 'admin_user_id', 'user_id');
	}
}