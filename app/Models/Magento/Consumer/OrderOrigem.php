<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class OrderOrigem extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'sales_flat_order_origem_pedido';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}