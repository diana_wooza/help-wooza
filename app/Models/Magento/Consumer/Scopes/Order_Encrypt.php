<?php

namespace App\Models\Magento\Consumer\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Helpers\Magento as MagentoHelper;

class Order_Encrypt implements Scope
{
	private static $encryptedFields = [
		'customer_cpf',
		'customer_email',
		'customer_firstname',
		'customer_middlename',
		'customer_lastname',
		'customer_gender',
		'customer_cellphone',
		'customer_telephone',	
		'postcode',	
	];

	public function apply (Builder $builder, Model $model)
	{
		$query = $builder->getQuery();

		$this->_processQuery($query);
	}

	private function _processQuery (&$query, $parentBindings = [], $parentKey = 0)
	{
		foreach ($query->wheres as $key => $where)
		{
			switch ($where['type'])
			{
				case 'Nested':

					$parentBindings[] = $query;

					$this->_processQuery($where['query'], $parentBindings, $parentKey + $key);
					continue;

				case 'Basic':

					if (!in_array($where['column'], self::$encryptedFields))
						continue;

					$query->bindings['where'][$key] = $where["value"] = MagentoHelper::encrypt($where["value"]);

					foreach ($parentBindings as $parentBinding)
						$parentBinding->bindings['where'][$parentKey + $key] = $where['value'];
					
					$query->wheres[$key] = $where;

					break;

			}
		}

		return $query;
	}

	// public function apply (Builder $builder, Model $model)
	// {
	// 	$query = $builder->getQuery();
	// 	$query = $this->_processQuery($query, $query->bindings);
		
	// 	$builder->setQuery($query);
	// }

	// private function _processQuery (&$query, &$parentBinding)
	// {
	// 	foreach ($query->wheres as $key => &$where)
	// 	{
	// 		switch ($where['type'])
	// 		{
	// 			case 'Nested':

	// 				$where['query'] = $this->_processQuery($where['query'], $parentBinding);
	// 				break;

	// 			case 'Basic':

	// 				if (!in_array($where['column'], self::$encryptedFields))
	// 					continue;

	// 				$bindingIndex = array_search($where["value"], $parentBinding['where'], true);
					
	// 				$parentBinding['where'][$bindingIndex] =
	// 				$query->bindings['where'][$key] =
	// 				$where["value"] = MagentoHelper::encrypt($where["value"]);

	// 				break;

	// 			case 'In':

	// 				if (!in_array($where['column'], self::$encryptedFields))
	// 					continue;

	// 				foreach ($where['values'] as &$value)
	// 				{
	// 					$bindingIndex = array_search($value, $parentBinding['where'], true);

	// 					$query->bindings['where'][$key] =
	// 					$parentBinding['where'][$bindingIndex] =
	// 					$value = MagentoHelper::encrypt($value);
	// 				}

	// 				break;

	// 		}

	// 		$query->wheres[$key] = $where;
	// 	}

	// 	return $query;
	// }
}