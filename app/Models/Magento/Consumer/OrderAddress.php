<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class OrderAddress extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'entity_id';
	protected $table			= 'sales_flat_order_address';
	protected $connection		= 'magento';

	protected $fillable			= ['parent_id','customer_address_id','quote_address_id','region_id','customer_id','country_id','fax','region','postcode','lastname','street','city','email','telephone','firstname','address_type'];
	protected $hidden			= ['parent_id','customer_address_id','quote_address_id','region_id','customer_id','country_id','prefix','middlename','suffix','company','vat_id','vat_is_valid','vat_request_id','vat_request_date','vat_request_success','sales_flat_order_address'];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= ['street_type','street_name','street_number','street_neighborhood','street_complement'];

	protected function getStreetTypeAttribute ()
	{
		return 'Rua';
		return \explode("\n", $this->street)[0];
	}

	protected function getStreetNameAttribute ()
	{
		$address = \explode("\n", $this->street);

		if (!is_array($address) || !isset($address[0]))
			return null;

		return $address[0];
	}

	protected function getStreetNumberAttribute ()
	{
		$address = \explode("\n", $this->street);

		if (!is_array($address) || !isset($address[1]))
			return null;

		return $address[1];
	}

	protected function getStreetNeighborhoodAttribute ()
	{
		$address = \explode("\n", $this->street);

		if (!is_array($address) || !isset($address[3]))
			return null;

		return $address[3];
	}

	protected function getStreetComplementAttribute ()
	{
		$address = \explode("\n", $this->street);

		if (!is_array($address) || !isset($address[2]))
			return null;

		return $address[2];;
	}


	protected function getPostcodeAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['postcode']);
	}

	protected function getStreetAttribute ($value)
	{
		return \implode("\n", \array_map(function ($item) {return MagentoHelper::decrypt($item);}, \explode("\n", $this->attributes['street'])));
	}

	protected function getFirstnameAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['firstname']);
	}

	protected function getLastnameAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['lastname']);
	}
}