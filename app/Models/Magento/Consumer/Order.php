<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Models\Magento\Consumer\Scopes\Order_Encrypt as EncryptScope;
use App\Helpers\Magento as MagentoHelper;
use App\Helpers\Tim\ApiOrdem as Tim_ApiOrdem;
use App\Helpers\Tim\Robo as Tim_Robo;
use App\Models\Indicadores\GraficoPrazoAtivacao;
use App\Models\Indicadores\GraficoTaxaAtivacao;

class Order extends Model
{
	const OPERADORA_TIM			= 'TIM';
	const OPERADORA_VIVO		= 'VIVO';
	const OPERADORA_CLARO		= 'CLARO';
	const OPERADORA_OI			= 'OI';
	const OPERADORA_NET			= 'NET';
	const OPERADORA_SKY			= 'SKY';
	const OPERADORA_NEXTEL		= 'NEXTEL';

	public $timestamps			= false;
	protected $primaryKey		= 'entity_id';
	protected $table			= 'sales_flat_order';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= ['coupon_code','protect_code','shipping_description','base_discount_amount','base_discount_canceled','base_discount_invoiced','base_discount_refunded','base_grand_total','base_shipping_amount','base_shipping_canceled','base_shipping_invoiced','base_shipping_refunded','base_shipping_tax_amount','base_shipping_tax_refunded','base_subtotal','base_subtotal_canceled','base_subtotal_invoiced','base_subtotal_refunded','base_tax_amount','base_tax_canceled','base_tax_invoiced','base_tax_refunded','base_to_global_rate','base_to_order_rate','base_total_canceled','base_total_invoiced','base_total_invoiced_cost','base_total_offline_refunded','base_total_online_refunded','base_total_paid','base_total_qty_ordered','base_total_refunded','discount_amount','discount_canceled','discount_invoiced','discount_refunded','grand_total','shipping_amount','shipping_canceled','shipping_invoiced','shipping_refunded','shipping_tax_amount','shipping_tax_refunded','store_to_base_rate','store_to_order_rate','subtotal','subtotal_canceled','subtotal_invoiced','subtotal_refunded','tax_amount','tax_canceled','tax_invoiced','tax_refunded','total_canceled','total_invoiced','total_offline_refunded','total_online_refunded','total_paid','total_qty_ordered','total_refunded','can_ship_partially','can_ship_partially_item','customer_is_guest','customer_note_notify','billing_address_id','customer_group_id','edit_increment','email_sent','forced_shipment_with_invoice','payment_auth_expiration','quote_address_id','quote_id','shipping_address_id','adjustment_negative','adjustment_positive','base_adjustment_negative','base_adjustment_positive','base_shipping_discount_amount','base_subtotal_incl_tax','base_total_due','payment_authorization_amount','shipping_discount_amount','subtotal_incl_tax','total_due','weight','customer_dob','applied_rule_ids','base_currency_code','hidden_tax_amount','base_hidden_tax_amount','shipping_hidden_tax_amount','base_shipping_hidden_tax_amnt','hidden_tax_invoiced','base_hidden_tax_invoiced','hidden_tax_refunded','base_hidden_tax_refunded','shipping_incl_tax','base_shipping_incl_tax','coupon_rule_name','paypal_ipn_customer_notified','gift_message_id','customer_prefix','customer_suffix','customer_taxvat','discount_description','ext_customer_id','ext_order_id','global_currency_code','hold_before_state','hold_before_status','order_currency_code','original_increment_id','relation_child_id','relation_child_real_id','relation_parent_id','relation_parent_real_id','shipping_method','store_currency_code','extended_response_message'];
	protected $attributes		= [];
	protected $casts			= [
		'entity_id'					=> 'integer',
		'is_virtual'				=> 'boolean',
		'store_id'					=> 'integer',
		'customer_id'				=> 'integer',
		'created_at'				=> 'datetime:d/m/Y H:i:s',
		'updated_at'				=> 'datetime:d/m/Y H:i:s',
		'cancelamento_date'			=> 'datetime:d/m/Y H:i:s',
		'plan_price'				=> 'decimal:2',
		'cost_chip'					=> 'decimal:2',
	];
	protected $appends			= [];

	private $tim				= [
		'api_ordem'					=> ['elegibilidade'=>null],
		'robo'						=> ['consultalinha'=>null],
	];

	public function GetPedidoContestador ()
	{
		$pedido = \App\Models\Contestador\Pedido
			::firstOrNew(
				['increment_id' => $this->increment_id],
				['pedido_id' => $this->entity_id]
			)
			->fill([
				'segmento'				=> \App\Models\Contestador\Pedido::GetSegmentoPedidoConsumer($this),
				'pedido_id'				=> $this->entity_id,
				'pedido_type'			=> self::class,
				'increment_id'			=> $this->increment_id,
				'operadora_id'			=> @\App\Models\Operadora::GetOperadoraPedidoMagentoConsumer($this)->id,
				'competencia_id'		=> @\App\Models\Contestador\Competencia::GetByDataYYYYMM(date('Ym', strtotime($this->activated_date)))->id,
				'usuario_ativacao_id'	=> $this->activated_user,
				'valor'					=> $this->plan_price,
			])
		;

		$pedido->save();

		return $pedido;
	}

	public function Tim_ConsultaLinhaPre ()
	{
		if ($this->plan_operator != self::OPERADORA_TIM)
			throw new \Exception('Tentando consultar linha em um pedido que não é TIM usando o robô de consulta de linha da TIM');

		$data = $this->Tim_GetData_ConsultaLinha();

		return $this->tim['robo']['consultalinha'] = Tim_Robo::ConsultaLinhaPre($data);
	}

	public function Tim_ConsultaLinhaPos ()
	{
		if ($this->plan_operator != self::OPERADORA_TIM)
			throw new \Exception('Tentando consultar linha em um pedido que não é TIM usando o robô de consulta de linha da TIM');

		$data = $this->Tim_GetData_ConsultaLinha();

		return $this->tim['robo']['consultalinha'] = Tim_Robo::ConsultaLinhaPos($data);
	}

	private function Tim_GetData_ConsultaLinha ()
	{
		$this->loadMissing('addresses','customer');

		return [
			'Celular'				=> $this->phone_service,
			'CEP'					=> $this->postcode,
			'DDD'					=> substr($this->phone_service, 0, 2),
			'Estado'				=> $this->region,
			'Cidade'				=> $this->city,
			'RuaNumeroComplBairro'	=> @$this->addresses[0]->streetstreet,
			'NomeCompleto'			=> $this->customer_fullname,
			'TelefoneContato'		=> $this->customer_contactnumber,
			'CPF'					=> $this->customer_cpf,
			'Email'					=> $this->customer_email,
			'DataNascimento'		=> $this->customer->data_nascimento,
			'NomeCompletoMae'		=> $this->customer->mother_full_name,
			'Sexo'					=> $this->customer_gender,
		];
	}

	public function Tim_ConsultaElegibilidade ()
	{
		if ($this->plan_operator != self::OPERADORA_TIM)
			throw new \Exception('Tentando consultar elegibilidade em um pedido que não é TIM usando a API de Ordem da TIM');

		return $this->tim['api_ordem']['elegibilidade'] = Tim_ApiOrdem::ConsultaElegibilidade($this->Tim_GetData_JSON_Elegibilidade_Robo());
	}

	public function Tim_GetData_JSON_Elegibilidade_Robo (string $version)
	{
		$this->loadMissing('addresses','customer','webjump','order_custcode');

		return [
			'NomenclaturaSapPlano'	=> \trim(@explode('+', $this->webjump->sap_identifier)[0]),
			'PdvCustCode'			=> @$this->order_custcode->custcode,
			'PdvStateCode'			=> @$this->region,
			'Cpf'					=> @$this->customer->cpf,
			'Nome'					=> \mb_strtoupper(@$this->customer->firstname . ' ' . @$this->customer->lastname),
			'NomeMae'				=> \mb_strtoupper(@$this->customer->mother_full_name),
			'Msisdn'				=> substr(@$this->customer->telephone,0, 2),
			'DataNascimento'		=> date('d/m/Y', strtotime(@$this->customer->data_nascimento)),
			'Cep'					=> @$this->addresses[0]->postcode,
			'PlanSegment'			=> $this->Tim_GetData_GetSegmento(),
		];
	}

	public function Tim_GetData_JSON_Elegibilidade_Tim (string $version)
	{
		$this->loadMissing('billing_address','customer','order_custcode');

		$data = [
			'pdv'			=> [
				'custCode'		=> @$this->order_custcode->custcode,
				'stateCode'		=> @$this->order_custcode->ufs,
			],
			'customer'		=> [
				'socialSecNo'	=> @$this->customer->cpf,
				'name'			=> \mb_strtoupper(@$this->customer->firstname . ' ' . @$this->customer->lastname),
				'motherName'	=> \mb_strtoupper(@$this->customer->mother_full_name),
				'birthDate'		=> date('d/m/Y', strtotime(@$this->customer->data_nascimento)),
				'address'		=> [
					'postalCode'	=> @$this->billing_address->postcode,
				],
			],
			'plan'			=> [
				'segment'		=> $this->Tim_GetData_GetSegmento(),
			],
		];

		switch ($this->plan_modality)
		{
			case 'Migração':
				
				$data['contract'] = ['msisdn' => @$this->customer->telephone];
				break;
			
			case 'Upgrade':
			case 'Nova linha':
			case 'Portabilidade':
			case 'Troca':

				$data['newContract'] = ['ddd' => substr(@$this->customer->telephone, 0, 2)];
				break;
			
			case 'SVA':

				break;
		}
		
		return $data;
	}

	public function Tim_GetData_JSON_Order_Tim (string $version, bool $simulation)
	{
		$this->loadMissing('billing_address','customer','order_custcode', 'banco');
		
		switch ($version)
		{
			case 'v5':

				$address = [
					'postalCode'		=> @$this->billing_address->postcode,
					'streetType'		=> @$this->billing_address->street_type,
					'streetName'		=> @$this->billing_address->street_name,
					'number'			=> @$this->billing_address->street_number,
					'neighborhood'		=> @$this->billing_address->street_neighborhood,
					'cityName'			=> @$this->billing_address->city,
					'stateCode'			=> @$this->billing_address->region,
					'country'			=> 'Brasil',
					'complement'		=> @$this->billing_address->street_complement,
				];
		
				switch ($this->plan_modality)
				{
					case 'Migração':
						
						$data['contract'] = ['msisdn' => @$this->customer->telephone];
						break;
					
					case 'Portabilidade':
		
						$data['portability'] = [
							'msisdn'		=> @$this->customer->telephone
							// 'scheduledDate'	=> 
						];
		
					case 'Upgrade':
					case 'Nova linha':
					case 'Troca':
		
						$data['newContract'] = ['ddd' => substr(@$this->customer->telephone, 0, 2)];
						break;
					
					case 'SVA':
		
						break;
				}
		
				$billingProfile = [
					'dueDate'			=> @$this->dia_vencimento,
					'email'				=> @$this->customer_email,
					'address'			=> $address,
				];
		
				
					// 	"paymentMethod" : "Débito Automatico",
					
					
					// },
		
				switch ($this->plano_forma_pagamento_id)
				{
					case 1: //Fatura Digital
					case 2: //Conta Pelos Correios
					case 4: //Conta Online
		
						$billingProfile['paymentMethod'] = 'Fatura';
		
						break;
					
					case 3: //Debito Automático
		
						$billingProfile['paymentMethod']	= 'Débito Automatico';
						$billingProfile['directDebit']		= [
							'bankCode'							=> @$this->banco->codigo,
							// 'operationCode'						=> null,
							'accountNumber'						=> @$this->plano_pagamento_conta,
							'agencyCode'						=> @$this->plano_pagamento_agencia,
						];
		
						break;
					
					case 5: //Cartão de Crédito
		
						$billingProfile['paymentMethod'] = 'Cartão de Crédito';
		
						// {
						// "type": "TCOM_LOV_MEIO_PAGAMENTO",
						// "value": "",
						// "label": "Cartao de Credito"
						// },
		
						break;
					
					case 6: //Boleto
		
						$billingProfile['paymentMethod'] = 'Boleto';
		
						// {
						// "type": "TCOM_LOV_MEIO_PAGAMENTO",
						// "value": "",
						// "label": "Boleto"
						// },
		
						break;
				}
		
				switch ($this->plano_pagamento_recebimento)
				{
					case 'correios':
					case 'conta_correios':
		
						$billingProfile['billType'] = 'Detalhada';
						break;
						
					case 'fatura_digital':
					case 'email':
		
						$billingProfile['billType'] = 'Conta Online';
						break;
					
					case 'whatsapp':
		
						$billingProfile['billType'] = null;
						break;
		
					default:
		
						$billingProfile['billType'] = null;
						break;
				}
		
				$data = ['order' => [
					'eligibilityToken'	=> null,
					'plan'				=> [
						'segment'			=> null,
						'id'				=> null,
					],
					'isSimulation'		=> $simulation,
					'pdv'				=> [
						'custCode'			=> @$this->order_custcode->custcode,
						'stateCode'			=> @$this->order_custcode->ufs,
					],
					'customer'			=> [
						'socialSecNo'		=> @$this->customer->cpf,
						'customerType'		=> 'PF',
						'name'				=> \mb_strtoupper(@$this->customer->firstname . ' ' . @$this->customer->lastname),
						'motherName'		=> \mb_strtoupper(@$this->customer->mother_full_name),
						'birthDate'			=> date('d/m/Y', strtotime(@$this->customer->data_nascimento)),
						'gender'			=> @$this->customer_gender,
						'email'				=> @$this->customer_email,
						'country'			=> 'Brasil',
						'contactNumber'		=> @$this->customer_telephone,
						'isIlliterate'		=> false,
						'identityDocument'	=> [
							'type'				=> 'Carteira de Identidade',
							'number'			=> '123456789',
							'issueDate'			=> '13/02/2008',
							'issuerAgency'		=> 'SSP',
							'issuerStateCode'	=> 'RJ',
						],
						'address'			=> $address,
					],
					'billingProfile'	=> $billingProfile,
					'optin'				=> [[
						'blockMessage'		=> 'Mensagem MKT',
						'option'			=> true,
					]],
					'acceptanceType'	=> 'OPTIN',
				]];
		
				return $data;

			case 'v3':

				throw new \Exception("Ainda não tem a v3 de ordem da TIM", 1);

		}
		
		throw new \Exception("Versão inválida", 1);
	}

	private function Tim_GetData_GetSegmento ()
	{
		switch (\mb_strtoupper($this->plan_tipo))
		{
			case 'CONTROLE':

				return Tim_ApiOrdem::ELEGIBILIDADE_SEGMENTO_CONTROLE;

			case 'PÓS':

				return Tim_ApiOrdem::ELEGIBILIDADE_SEGMENTO_POS;

			case 'COMBO':
			case 'FIXO':
			case 'INTERNET':
			case 'INTERNET FIXA':
			case 'SVA':
		}

		throw new \Exception('Plan tipo inválido');
	}

	private function Tim_GetData_GetPlano ()
	{
		if (is_null($this->tim['api_ordem']['elegibilidade']) || !is_array($this->tim['api_ordem']['elegibilidade']->retorno->planosDisponiveis) || count($this->tim['api_ordem']['elegibilidade']->retorno->planosDisponiveis) <= 0)
			return null;

		$identificador = \trim(@explode('+', $this->order->webjump->sap_identifier)[0]);

		foreach ($this->tim['api_ordem']['elegibilidade']->retorno->planosDisponiveis as $plano)
		{
			if ($plano->Nome != $identificador)
				continue;

			return $plano;
		}

		return null;
	}

	private function Tim_GetData_BillingProfile ()
	{
		$profile = [];

		$profile['billType']		= "Conta Online";
		$profile['paymentMethod']	= "Débito Automático";
		$profile['dueDate']			= 1;

		if (false)
		{
			$profile['directDebit'] = [
				'bankCode'				=> @$this->plano_pagamento_banco_id,
				'accountNumber'			=> @$this->plano_pagamento_conta,
				'agencyCode'			=> @$this->plano_pagamento_agencia,
			];
		}

		$profile['email']			= @$this->customer_email;
		$profile['address']			= $this->getData_Address();

		return $profile;
	}

	private function Tim_GetData_Address ()
	{
		return [
			'postalCode'			=> @$this->postcode,
			'streetType'			=> @$this->addresses[0]->street_type,
			'streetName'			=> @$this->addresses[0]->street_name,
			'number'				=> @$this->addresses[0]->street_number,
			'neighborhood'			=> @$this->addresses[0]->street_neighborhood,
			'cityName'				=> @$this->addresses[0]->city,
			'stateCode'				=> @$this->region,
			'country'				=> 'Brasil',
			'complement'			=> @$this->addresses[0]->street_complement,
		];
	}

	/**
	 * getDadosGraficoPrazoAtivacao - recupera pedidos para o grafico do prazo de ativação dos indicadores (não guardei codigo do pedido ou madalidade pois estou dando um count nos pedidos)
	 *
	 * @param  string $dataInicio
	 * @param  string $dataFim
	 *
	 * @return void
	 */
	public static function getDadosGraficoPrazoAtivacao($dataInicio, $dataFim)
	{		
		$dados = self::select(DB::raw("resultados.plan_operator, resultados.segmento,
										TIMESTAMPDIFF(HOUR, resultados.created_at, resultados.activated_date) AS horas_de_ativacao, 
										COUNT(TIMESTAMPDIFF(hour, resultados.created_at, resultados.activated_date)) as soma_horas_de_ativacao,
										DATE_FORMAT(resultados.activated_date, '%Y-%m') AS ano_mes_ativacao"))
								->from(DB::raw("(SELECT created_at,
														activated_date,
														plan_operator,
														segmento
												FROM
												sales_flat_order SFO
												JOIN
													admin_user_base base ON base.id = SFO.base_id AND base.segmento = 'consumer'
												WHERE
													created_at BETWEEN '2019-01-01'
													AND '2019-01-31'
													AND (plan_modality = 'migracao' OR plan_modality = 'upgrade')
													AND plan_operator in ('TIM') 
													AND SFO.plan_tipo != 'SVA'
													AND activated_date IS NOT NULL) as resultados"))
								->groupBy('ano_mes_ativacao', 'horas_de_ativacao')
								->orderBy('ano_mes_ativacao', 'asc')
								// ->limit(1000)
								->get();

		if($dados) {
		
			foreach($dados as $dado):
				## Caso ja exista esse resgistro ele vai somar, se não irá inserir um novo
				$sql = "INSERT INTO grafico_prazo_ativacao(plan_operator, segmento, ano_mes_ativacao, horas_de_ativacao, soma_horas_de_ativacao) 
						VALUES ('$dado->plan_operator','$dado->segmento', concat('$dado->ano_mes_ativacao','-00'), $dado->horas_de_ativacao, $dado->soma_horas_de_ativacao) 
						ON DUPLICATE KEY UPDATE soma_horas_de_ativacao = $dado->soma_horas_de_ativacao;";

				## descomentar quando o cron rodar em prod
				// $sql = "INSERT INTO grafico_prazo_ativacao(plan_operator, segmento, ano_mes_ativacao, horas_de_ativacao, soma_horas_de_ativacao) 
				// VALUES ('$dado->plan_operator','$dado->segmento', concat('$dado->ano_mes_ativacao','-00'), $dado->horas_de_ativacao, $dado->soma_horas_de_ativacao) 
				// ON DUPLICATE KEY UPDATE soma_horas_de_ativacao = soma_horas_de_ativacao + $dado->soma_horas_de_ativacao;";		

				DB::Connection('alarmistica')->insert($sql);

			endforeach;						

		}

		return $dados;					
	
	}

	public static function getDadosGraficoTaxaAtivacao($operadora, $segmento, $dataInicio, $dataFim) 
	{

		$plan_operator = ['CLARO', 'OI', 'TIM', 'VIVO'];
		$segmento = 'consumer';
		// $plan_operator = 'OI';
		// $datas = [
			// ['2018-08-01 00:00:00','2018-08-31 23:59:59', '2018-08'],
			// ['2018-08-16','2018-08-31', '2018-08'],
			// ['2018-09-01 00:00:00', '2018-09-30 23:59:59', '2018-09'],
			// ['2018-09-16', '2018-09-30', '2018-09'],
			// ['2018-10-01 00:00:00', '2018-10-31 23:59:59', '2018-10'],
			// ['2018-10-16', '2018-10-31', '2018-10'],
			// ['2018-11-01 00:00:00', '2018-11-30 23:59:59', '2018-11'],
		// 	['2018-11-16', '2018-11-30', '2018-11'],
			// ['2018-12-01 00:00:00', '2018-12-31 23:59:59', '2018-12'],
		// 	['2018-12-16', '2018-12-31', '2018-12'],
			// ['2019-01-01 00:00:00', '2019-01-31 23:59:59', '2019-01'],
		// 	['2019-01-16', '2019-01-31', '2019-01 23:59:59'],
			// ['2019-02-01 00:00:00', '2019-02-28 23:59:59', '2019-02'],
		// 	['2019-02-16', '2019-02-28', '2019-02'],
			// ['2019-03-01 00:00:00', '2019-03-31 23:59:59', '2019-03'],
		// 	['2019-03-16', '2019-03-31', '2019-03']
		// ];

		$datas = [
			// ['2019-04-01 00:00:00','2019-04-30 23:59:59', '2019-04'],
			// ['2019-04-16','2019-04-30', '2019-04'],
			// ['2019-05-01 00:00:00', '2019-05-31 23:59:59', '2019-05'],
			// ['2019-05-16', '2019-05-31', '2019-05'],
			// ['2019-06-01 00:00:00', '2019-06-30 23:59:59', '2019-06'],
			// ['2019-06-16', '2019-06-30', '2019-06'],
			// ['2019-07-01 00:00:00', '2019-07-31 23:59:59', '2019-07'],
			// ['2019-07-16', '2019-07-31', '2019-07'],
			['2019-08-01 00:00:00', '2019-08-31 23:59:59', '2019-08'],
			// ['2019-08-16', '2019-08-31', '2019-08'],
			['2019-09-01 00:00:00', '2019-09-30 23:59:59', '2019-09'],
			// ['2019-09-16', '2019-09-30', '2019-09'],
			// ['2019-10-01 00:00:00', '2019-10-15 23:59:59', '2019-10'],
			// ['2019-10-16', '2019-10-31', '2019-10'],
			// ['2019-11-01 00:00:00', '2019-11-15 23:59:59', '2019-11'],
			// ['2019-11-16', '2019-11-30', '2019-11'],
			// ['2019-12-01 00:00:00', '2019-12-15 23:59:59', '2019-12'],
			// ['2019-12-16', '2019-12-31', '2019-12']
		];

		foreach($plan_operator as $operadora):
			foreach($datas as $k => $data):

				$sql = "select 
						distinct(entity_id),
								plan_operator,
								segmento,
								DATE_FORMAT(sales_flat_order.created_at, '%Y-%m') AS ano_mes_criacao_pedido,
								COUNT(sales_flat_order.entity_id) total_pedidos,
								CASE
									WHEN
									(sales_flat_order.activated_user IS NOT NULL
									AND (user_ativa.robo = 1
									OR user_ativa.robo <> 1)
									AND sales_flat_order.plano_forma_pagamento_id = '5')
									THEN
									'plano_cartao'
									WHEN
									(sales_flat_order.cancelamento_user IS NOT NULL
									AND (user_cancel.robo NOT IN (0 , 1)
									OR user_cancel.robo IS NULL))
									THEN
									'outros_cancelamento'
									WHEN
									(sales_flat_order.cancelamento_user IS NOT NULL
									AND user_cancel.robo = 1)
									THEN
									'bot_cancelamento'
									WHEN
									(sales_flat_order.cancelamento_user IS NOT NULL
									AND user_cancel.robo = 0)
									THEN
									'bko_cancelamento'
									WHEN
									(sales_flat_order.activated_user IS NOT NULL
									AND user_ativa.robo = 1)
									THEN
									'bot_ativacao'
									WHEN
									(sales_flat_order.activated_user IS NOT NULL
									AND user_ativa.robo <> 1)
									THEN
									'bko_ativacao'
								ELSE 'desconhecido'
								END AS tipo_usuario
						from sales_flat_order
						left join admin_user user_ativa ON user_ativa.user_id = sales_flat_order.activated_user
						left join admin_user user_cancel On user_cancel.user_id = sales_flat_order.cancelamento_user
						left Join admin_user_base on admin_user_base.id = sales_flat_order.base_id
						where
							created_at between '{$data[0]}' AND '{$data[1]}'
							AND (plan_modality = 'migracao'
							OR plan_modality = 'upgrade')
							AND admin_user_base.segmento = '{$segmento}'
							AND sales_flat_order.status IN ('cancelado' , 'completo')
							AND plan_operator = '{$operadora}'
							AND sales_flat_order.plan_tipo != 'SVA'
						group by ano_mes_criacao_pedido , tipo_usuario";
// dd($sql);
	
$sql = DB::connection('magento')->select($sql);
// print_r($sql);
// 	dd($sql);
				if(count($sql) > 0){

					self::setGraficoTaxaAtivacao($sql);

					$sqlE = "SELECT 
								'{$operadora}' AS plan_operator,
								'consumer' AS segmento,
								'{$data[2]}' as ano_mes_criacao_pedido,
								COUNT(sales_flat_order_sla.parent_id) AS total_pedidos,
								'analise_preliminar_cancelamento' as tipo_usuario
							FROM
								sales_flat_order_sla
							WHERE
								status_de = 'analise_de_viabilidade'
									AND status_para = 'cancelado'
									AND parent_id IN (SELECT 
										entity_id
									FROM
										sales_flat_order
										inner join
										admin_user_base ON admin_user_base.id = sales_flat_order.base_id
									WHERE
										cancelamento_user IS NOT NULL
											AND created_at BETWEEN '{$data[0]}' AND '{$data[1]}'
											AND (plan_modality = 'migracao'
											OR plan_modality = 'upgrade')
											AND segmento = 'consumer'
											AND plan_operator = '{$operadora}'
											AND plan_tipo != 'SVA')
							ORDER BY id ASC;";

					$sqlE = DB::connection('magento')->select($sqlE);
					
					self::setGraficoTaxaAtivacao($sqlE);
				
					// $sqlBacklog = self::select(DB::raw("if(plan_operator is null, '{$plan_operator}', plan_operator) as plan_operator,
					// 									if(admin_user_base.segmento is null, '{$segmento}', admin_user_base.segmento) as segmento,
					// 									if(DATE_FORMAT(sales_flat_order.created_at, '%Y-%m') is null, '{$data[2]}', DATE_FORMAT(sales_flat_order.created_at, '%Y-%m')) AS ano_mes_criacao_pedido,
					// 									COUNT(entity_id) as total_pedidos, 
					// 									'backlog' as tipo_usuario"))
					// 						//->leftJoin('sales_flat_order_sla', 'sales_flat_order_sla.parent_id', '=', 'sales_flat_order.entity_id')
					// 						->leftJoin('admin_user_base', 'admin_user_base.id', '=', 'sales_flat_order.base_id')
					// 						->whereRaw("created_at >= '{$data[0]}'
					// 										AND created_at <= '{$data[1]}'
					// 										AND (plan_modality = 'migracao'
					// 										OR plan_modality = 'upgrade')
					// 										AND admin_user_base.segmento = '{$segmento}'
					// 										AND sales_flat_order.status NOT IN ('cancelado' , 'completo')
					// 										AND sales_flat_order.plan_operator = '{$plan_operator}'
					// 										AND sales_flat_order.plan_tipo != 'SVA'")
					// 						->get();					
					
					// self::setGraficoTaxaAtivacao($sqlBacklog);

					## Usar quando o cron estiver ok, sevira para o backlog
					// $data_atual = date('Y-m-d');

					// 	$sqlBacklog = " SELECT 
					// 						IF(plan_operator IS NULL,
					// 							'{$plan_operator}',
					// 							plan_operator) AS plan_operator,
					// 						IF(admin_user_base.segmento IS NULL,
					// 							'{$segmento}',
					// 							admin_user_base.segmento) AS segmento,
					// 						IF(DATE_FORMAT(sales_flat_order.created_at, '%Y-%m') IS NULL,
					// 							DATE_FORMAT($data_atual, '%Y-%m'),
					// 							DATE_FORMAT(sales_flat_order.created_at, '%Y-%m')) AS ano_mes_criacao_pedido,
					// 						COUNT(entity_id) AS total_pedidos,
					// 						'backlog' AS tipo_usuario
					// 					FROM
					// 						sales_flat_order
					// 							LEFT JOIN
					// 						admin_user_base ON id = base_id
					// 					WHERE
					// 						DATE_FORMAT(sales_flat_order.created_at, '%Y-%m') BETWEEN DATE_FORMAT(NOW(), '%Y-%m') AND DATE_FORMAT(NOW(), '%Y-%m')
					// 							AND (plan_modality = 'migracao'
					// 							OR plan_modality = 'upgrade')
					// 							AND segmento = 'consumer'
					// 							AND status NOT IN ('cancelado' , 'completo')
					// 							AND plan_operator = '{$operadora}'
					// 							AND plan_tipo != 'SVA';";

					// $sqlBacklog = DB::connection('magento')->select($sqlBacklog);
						// self::setGraficoTaxaAtivacao($sqlBacklog, true);

					
				}
			
			endforeach;
		endforeach;

	}

	# passar o tipo == true quando sqlBacklog estiver sendo chamado pelo cron
	public static function setGraficoTaxaAtivacao($obj, $tipo = false)
	{
		if($obj) {
			
			$insere = 'total_pedidos + ';

			if($tipo == true){
				dd('Não soma!');
				$insere = '';
			}

			foreach($obj as $dado):
				## Caso ja exista esse resgistro ele vai somar, se não irá inserir um novo
				$sql = "INSERT INTO grafico_taxa_ativacao(plan_operator, segmento, ano_mes_criacao_pedido, total_pedidos, tipo_usuario) 
						VALUES ('{$dado->plan_operator}','{$dado->segmento}', concat('{$dado->ano_mes_criacao_pedido}','-00'), {$dado->total_pedidos}, '{$dado->tipo_usuario}') 
						ON DUPLICATE KEY UPDATE total_pedidos = $insere {$dado->total_pedidos};";

				DB::connection('alarmistica')->insert($sql);
				
			endforeach;						
			
		}

	}

	// public function Tim_Tim_EnviarApiOrdem ()
	// {
	// 	if ($this->plan_operador != self::OPERADORA_TIM)
	// 		throw new \Exception('Tentando consultar elegibilidade em um pedido que não é TIM usando a API de Ordem da TIM');

	// 	#faz consulta de elegibilidade caso não tenha sido feita
	// 	if (is_null($this->tim['api_ordem']['elegibilidade']))
	// 		$this->Tim_ConsultaElegibilidade();

	// 	$plano =  $this->Tim_GetData_GetPlano();

	// 	$data = ['order'=>[
	// 		'eligibilityToken'		=> $this->elegibilidade->retorno->tokenElegibilidade,
	// 		'isSimulation'			=> $simulacao === true,
	// 		'pdv'					=> [
	// 			'custCode'				=> @$this->order->order_custcode->custcode,
	// 			'stateCode'				=> @$this->order->region,
	// 		],
	// 		'customer'				=> [
	// 			'socialSecNo'			=> @$this->order->customer_cpf,
	// 			'customerType'			=> 'PF',
	// 			'name'					=> @$this->order->customer_fullname,
	// 			'motherName'			=> @$this->order->customer->mother_full_name,
	// 			'birthDate'				=> \date('d/m/Y', \strtotime(@$this->order->customer->data_nascimento)),
	// 			'gender'				=> @$this->order->customer_gender,
	// 			'email'					=> @$this->order->customer_email,
	// 			'country'				=> 'Brasil',
	// 			'contactnumberNumber'			=> @$this->order->customer_telephone,
	// 			'isIlliterate'			=> false,
	// 			'identityDocument'		=> [
	// 				'type'					=> 'Carteira de Identidade',
	// 				'number'				=> '123456789',
	// 				'issueDate'				=> '13/02/2008',
	// 				'issuerAgency'			=> 'SSP',
	// 				'issuerStateCode'		=> 'RJ'
	// 			],
	// 			'address'				=> $this->getData_Address(),
	// 		//    "newContract":{
	// 		// 	   "ddd" : "21",
	// 		// 	   "simCard" : {
	// 		// 		   "id" : "89550318002545866679"
	// 		// 	   }
	// 		//    },
	// 			'plan'					=> [
	// 				'segment'				=> 'POS_PAGO',
	// 				'id'					=> $plano->IdPlano,
	// 			],
	// 			'billingProfile'		=> $this->GetData_BillingProfile(),
	// 			'optin'					=> [
	// 				['blockMessage'=>'Mensagem MKT', 'option'=>true],
	// 			],
	// 			'acceptanceType'		=> 'OPTIN',
	// 		],
	// 	]];
	// }

	/**
	 * Laravel Mutators
	 */

	protected function getCustomerContactnumberAttribute ()
	{
		if (!empty($this->customer_telephone))
			return $this->customer_telephone;
			
		return $this->customer_cellphone;
	}

	protected function getPlanOperatorBulletAttribute ()
	{
		$layout = (object) ['background'=>'#000000FF', 'text'=>'#FFFFFFFF'];

		switch ($this->plan_operator)
		{
			case 'TIM':

				$layout->background = '#0062B1FF';
				$layout->text = '#FFFFFFFF';
				break;

			case 'CLARO':

				$layout->background = '#9F0500FF';
				$layout->text = '#FFFFFFFF';
				break;

			case 'VIVO':

				$layout->background = '#AB149EFF';
				$layout->text = '#FFFFFFFF';
				break;

			case 'OI':

				$layout->background = '#FE9200FF';
				$layout->text = '#FFFFFFFF';
				break;
				
		}

		return '<span class="label" style="background-color:' . $layout->background . '; color:' . $layout->text . '">' . $this->plan_operator . '<span>';
	}

	protected function getCreatedAtFormatadoAttribute ()
	{
		return date('d/m/Y H:i:s', strtotime($this->attributes['created_at']));
	}

	protected function getActivatedDateFormatadoAttribute ()
	{
		return date('d/m/Y H:i:s', strtotime($this->attributes['activated_date']));
	}

	protected function getCustomerEmailAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['customer_email']);
	}

	protected function getCustomerFirstnameAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['customer_firstname']);
	}

	protected function getCustomerLastnameAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['customer_lastname']);
	}

	protected function getCustomerFullnameAttribute ()
	{
		$nomeCompleto = [$this->customer_firstname];

		if (!empty($sobrenome = $this->customer_lastname))
			$nomeCompleto[] = $sobrenome;

		return implode(' ', $nomeCompleto);
	}

	protected function getCustomerCpfAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['customer_cpf']);
	}

	protected function getCustomerGenderAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['customer_gender']);
	}

	protected function getCustomerCellphoneAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['customer_cellphone']);
	}

	protected function getCustomerTelephoneAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['customer_telephone']);
	}

	protected function getPostcodeAttribute ()
	{
		return MagentoHelper::decrypt($this->attributes['postcode']);
	}

	protected function getPlanPriceAttribute ($value)
	{
		return \number_format($value, 2);
	}

	protected function getCostChipAttribute ($value)
	{
		return \number_format($value, 2);
	}

	protected function getCustomerFullAddressAttribute() {
		return ['postcode' => @$this->postcode, 'streetName' => @$this->addresses[0]->street_name, 'number' => @$this->addresses[0]->street_number, 'neighborhood' => @$this->addresses[0]->street_neighborhood, 'city' => @$this->addresses[0]->city];
	}

	/**
	 * Custom Scopes
	 */

	public function scopeApenasAtivos ($query)
	{
		return $query->whereNotNull('activated_date');
	}

	public function scopePorOperadora ($query, \App\Models\Operadora $operadora)
	{
		return $query->where('plan_operator', @$operadora->adicionais->nome_operadora_magento);
	}

	public function scopePorCompetencia ($query, \App\Models\Contestador\Competencia $competencia)
	{
		return $query
			->whereRaw("activated_date IS NOT NULL")
			->whereRaw("DATE_FORMAT(activated_date, '%Y%m') = '" . $competencia->ano_string . $competencia->mes_string . "'")
		;
	}

	public function scopeIsPortabilidade ($query)
	{
		return $query->where('plan_modality', 'Portabilidade');
	}

	/**
	 * Laravel Relationship
	 */

	public function agenda_portabilidade ()
	{
		return $this->hasOne(\App\Models\Magento\Consumer\OrderAgendaPortabilidade::class, 'entity_id', 'entity_id');
	}
	
	public function banco ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\OrderBanco::class, 'plano_pagamento_banco_id', 'id');
	}

	public function billing_address ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\OrderAddress::class, 'billing_address_id', 'entity_id');
	}

	public function contestador_pedido ()
	{
		return $this->morphOne(\App\Models\Contestador\Pedido::class, 'pedido');
	}
	
	// public function admin_user ()
	// {
	// 	return $this->belongsTo(AdminUser::class);
	// }

	public function admin_base ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\AdminUserBase::class, 'base_id');
	}
	
	public function store ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\Store::class, 'store_id', 'store_id');
	}

	public function history ()
	{
		return $this->hasMany(\App\Models\Magento\Consumer\OrderHistory::class, 'parent_id', 'entity_id')->orderBy('created_at', 'DESC');
	}

	public function order_custcode ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\CustCode::class, 'custcode', 'dsf');
	}

	public function webjump ()
	{
		return $this->hasOne(\App\Models\Magento\Consumer\Webjump::class, 'parent_id', 'entity_id');
	}

	public function customer ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\Customer::class, 'customer_id', 'entity_id');
	}

	public function adicionais ()
	{
		return $this->hasOne(\App\Models\Magento\Consumer\OrderAdicional::class, 'order_id', 'entity_id');
	}

	public function addresses ()
	{
		return $this->hasMany(\App\Models\Magento\Consumer\OrderAddress::class, 'parent_id', 'entity_id');
	}

	public function itens ()
	{
		return $this->hasMany(\App\Models\Magento\Consumer\OrderItem::class, 'order_id', 'entity_id');
	}

	public function origem ()
	{
		return $this->hasOne(\App\Models\Magento\Consumer\OrderOrigem::class, 'order_id', 'entity_id');
	}

	public function slas ()
	{
		return $this->hasMany(\App\Models\Magento\Consumer\OrderSla::class, 'parent_id', 'entity_id');
	}

	public function svas ()
	{
		return $this->hasMany(\App\Models\Magento\Consumer\OrderSva::class, 'order_id', 'entity_id');
	}

	public function retorno_robo ()
	{
		return $this->hasMany(\App\Models\Magento\Consumer\RoboReturn::class, 'order_id', 'entity_id');
	}

	public function dependentes ()
	{
		return $this->hasMany(\App\Models\Magento\Consumer\OrderDependente::class, 'order_id', 'entity_id');
	}

	public function activated_user ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\AdminUser::class, 'activated_user', 'user_id');
	}
	
	public function seller ()
	{
		return $this->belongsTo(\App\Models\Magento\Consumer\AdminUser::class, 'seller_id', 'user_id');
	}

	protected static function boot()
	{
		parent::boot();
		static::addGlobalScope(new EncryptScope);
	}
}