<?php

namespace App\Models\Magento\Consumer;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Magento as MagentoHelper;

class OrderItem extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'item_id';
	protected $table			= 'sales_flat_order_item';
	protected $connection		= 'magento';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}