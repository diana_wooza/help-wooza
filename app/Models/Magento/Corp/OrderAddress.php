<?php

namespace App\Models\Magento\Corp;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'sales_flat_order_address';
	protected $connection		= 'magento_corp';

	protected $fillable			= [];
	protected $hidden			= ['parent_id','customer_address_id','quote_address_id','fax','region','prefix','suffix','company','vat_id','vat_is_valid','vat_request_id','vat_request_date','vat_request_success','sales_flat_order_address'];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= ['street_logradouro','street_numero','street_complemento','street_bairro'];

	/**
	 * Mutators
	 */

	protected function getStreetArrayAttribute ()
	{
		return @explode("\n", $this->street);
	}
	 
	protected function getStreetLogradouroAttribute ()
	{
		return $this->street_array[0];
	}

	protected function getStreetNumeroAttribute ()
	{
		return $this->street_array[1];
	}

	protected function getStreetComplementoAttribute ()
	{
		return $this->street_array[2];
	}

	protected function getStreetBairroAttribute ()
	{
		return $this->street_array[3];
	}

	/**
	 * Relationships
	**/

	public function order ()
	{
		return $this->belongsTo(\App\Models\Magento\Corp\Order::class, 'parent_id', 'entity_id');
	}

	// public function customer ()
	// {
	// 	return $this->belongsTo(\App\Models\Magento\Corp\Customer::class, 'customer_id', 'entity_id');
	// }

}