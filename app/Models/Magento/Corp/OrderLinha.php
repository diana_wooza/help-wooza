<?php

namespace App\Models\Magento\Corp;

use Illuminate\Database\Eloquent\Model;

class OrderLinha extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'sales_flat_order_linha';
	protected $connection		= 'magento_corp';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= ['passaportes'];

	/**
	 * Mutators
	 */

	protected function getPassaportesAttribute ()
	{
		if (is_null($this->sva))
			return null;

		return collect(explode(',', $this->sva))
			->filter(function ($item) {return \strstr(\mb_strtoupper($item), 'PASSAPORTE') !== false;})
			->implode(' + ')
		;
	}
	 
	protected function getStreetLogradouroAttribute ()
	{
		return @explode(PHP_EOL, $this->street)[0];
	}

	protected function getStreetNúmeroAttribute ()
	{
		return @explode(PHP_EOL, $this->street)[1];
	}

	protected function getStreetComplementoAttribute ()
	{
		return @explode(PHP_EOL, $this->street)[2];
	}

	protected function getStreetBairroAttribute ()
	{
		return @explode(PHP_EOL, $this->street)[3];
	}

	/**
	 * Scopes
	 */

	public function scopeClaro ($query)
	{
		return $query->whereHas('pedido', function ($query) {
			return $query->where('plan_operator', 'CLARO');
		});
	}

	public function scopeImputarDados ($query)
	{
		return $query->whereHas('pedido', function ($query) {
			return $query->where('status', 'inputar_dados');
		});
	}

	public function scopePorPeriodo ($query, $de = null, $ate = null)
	{
		return $query->whereHas('pedido', function ($query) use ($de, $ate) {
		
			if (!is_null($de))
				$query->where('created_at', '>=', $de);

			if (!is_null($ate))
				$query->where('created_at', '<=', $ate);

			return $query;
		
		});
	}

	/**
	 * Relationship
	 */

	public function pedido ()
	{
		return $this->belongsTo(\App\Models\Magento\Corp\Order::class, 'order_id', 'entity_id');
	} 
}