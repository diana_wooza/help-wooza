<?php

namespace App\Models\Magento\Corp;

use Illuminate\Database\Eloquent\Model;

class ConsultaReceitaAtividade extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'consulta_receita_atividades';
	protected $connection		= 'magento_corp';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}