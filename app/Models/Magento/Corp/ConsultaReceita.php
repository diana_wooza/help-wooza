<?php

namespace App\Models\Magento\Corp;

use Illuminate\Database\Eloquent\Model;

class ConsultaReceita extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'consulta_receita';
	protected $connection		= 'magento_corp';

	protected $fillable			= [];
	protected $hidden			= ['increment_id'];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
	protected $with				= ['atividades'];

	/**
	 * Relationships
	**/

	public function order ()
	{
		return $this->belongsTo(\App\Models\Magento\Corp\Order::class, 'increment_id', 'increment_id');
	}

	public function atividades ()
	{
		return $this->belongsToMany(\App\Models\Magento\Corp\ConsultaReceitaAtividade::class, 'consulta_receita_atividades_secundarias', 'id_receita', 'id_atividade')->withPivot('id')->orderBy('pivot_id');
	}
}