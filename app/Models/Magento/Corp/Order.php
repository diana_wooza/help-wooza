<?php

namespace App\Models\Magento\Corp;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'entity_id';
	protected $table			= 'sales_flat_order';
	protected $connection		= 'magento_corp';

	protected $fillable			= [];
	protected $hidden			= ['shipping_description','is_virtual','store_id','customer_id','base_discount_amount','base_discount_canceled','base_discount_invoiced','base_discount_refunded','base_grand_total','base_shipping_amount','base_shipping_canceled','base_shipping_invoiced','base_shipping_refunded','base_shipping_tax_amount','base_shipping_tax_refunded','base_subtotal','base_subtotal_canceled','base_subtotal_invoiced','base_subtotal_refunded','base_tax_amount','base_tax_canceled','base_tax_invoiced','base_tax_refunded','base_to_global_rate','base_to_order_rate','base_total_canceled','base_total_invoiced','base_total_invoiced_cost','base_total_offline_refunded','base_total_online_refunded','base_total_paid','base_total_qty_ordered','base_total_refunded','discount_amount','discount_canceled','discount_invoiced','discount_refunded','grand_total','shipping_amount','shipping_canceled','shipping_invoiced','shipping_refunded','shipping_tax_amount','shipping_tax_refunded','store_to_base_rate','store_to_order_rate','subtotal','subtotal_canceled','subtotal_invoiced','subtotal_refunded','tax_amount','tax_canceled','tax_invoiced','tax_refunded','total_canceled','total_invoiced','total_offline_refunded','total_online_refunded','total_paid','total_qty_ordered','total_refunded','can_ship_partially','can_ship_partially_item','customer_is_guest','customer_note_notify','billing_address_id','customer_group_id','edit_increment','email_sent','forced_shipment_with_invoice','payment_auth_expiration','quote_address_id','quote_id','shipping_address_id','adjustment_negative','adjustment_positive','base_adjustment_negative','base_adjustment_positive','base_shipping_discount_amount','base_subtotal_incl_tax','base_total_due','payment_authorization_amount','shipping_discount_amount','subtotal_incl_tax','total_due','weight','customer_dob','applied_rule_ids','customer_prefix','customer_suffix','customer_taxvat','discount_description','ext_customer_id','ext_order_id','global_currency_code','hold_before_state','hold_before_status','order_currency_code','original_increment_id','relation_child_id','relation_child_real_id','relation_parent_id','relation_parent_real_id','shipping_method','store_currency_code','x_forwarded_for','customer_note','total_item_count','hidden_tax_amount','base_hidden_tax_amount','shipping_hidden_tax_amount','base_shipping_hidden_tax_amnt','hidden_tax_invoiced','base_hidden_tax_invoiced','hidden_tax_refunded','base_hidden_tax_refunded','shipping_incl_tax','base_shipping_incl_tax','coupon_rule_name','paypal_ipn_customer_notified','gift_message_id','extended_response_message','seller_id','base_id','cod_cancelamento','id_lead','origem_lead','fila','ramal'];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= ['CNAESecundario'];

	public function GetPedidoContestador ()
	{
		$pedido = \App\Models\Contestador\Pedido
			::where('segmento', \App\Models\Contestador\Pedido::SEGMENTO_CORP)
			->firstOrNew(
				['increment_id' => $this->increment_id],
				['pedido_id' => $this->entity_id]
			)
			->fill([
				'segmento'				=> \App\Models\Contestador\Pedido::SEGMENTO_CORP,
				'pedido_id'				=> $this->entity_id,
				'pedido_type'			=> self::class,
				'increment_id'			=> $this->increment_id,
				'operadora_id'			=> @\App\Models\Operadora::GetOperadoraPedidoMagentoCorp($this)->id,
				'competencia_id'		=> @\App\Models\Contestador\Competencia::GetByDataYYYYMM(date('Ym', strtotime($this->activated_date)))->id,
				'usuario_ativacao_id'	=> $this->activated_user,
				'valor'					=> $this->plan_price,
			])
		;
		
		$pedido->save();

		return $pedido;
	}

	/**
	 * Mutators
	 */

	protected function getTipoSolicitacaoStringAttribute ()
	{
		if (is_null($this->linhas))
			return '';

		return $this->linhas->pluck('plan_modality')->implode(', ');
	}

	protected function getRazaoSocialRevendaAttribute ()
	{
		return 'Wooza Tecnologia e Representacoes S.A.';
	}

	protected function getCNPJrevendaAttribute ()
	{
		return '09344031000134';
	}

	protected function getCodigoRevendaAttribute ()
	{
		return 'OUW0';
	}

	protected function getNomeCompletoConsultorAttribute ()
	{
		return 'Natálly Mendes de Govea Ribeiro';
	}

	protected function getCodigoConsultorAttribute ()
	{
		return 'YCXRL';
	}

	protected function getCanalVendaAttribute ()
	{
		return 'Loja Online';
	}

	protected function getInscricaoEstadualAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getCNAESecundarioAttribute ()
	{
		return @$this->receita->atividades[0]->codigo;
	}

	protected function getSegundoAdministradorContaAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getCPFSegundoAdministradorAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTelefoneContatoSegundoAdministradorAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTelefoneContatoCelularSegundoAdministradorAttribute ()
	{
		return ''; #não preenchido
	}

	protected function getEmailSegundoAdministradorAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTempoVigenciaContratoAttribute ()
	{
		return '24 meses';
	}

	protected function getClientePossuiServicosEMBRATELAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getClientePossuiServicosNETAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getClientepossuiofertaNETEmpresasMAISAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getIdentificacaoClienteNETAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getKITAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getValorPassaporteAvulsoAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getModeloSimcardAttribute ()
	{
		return 'TRIPLE';
	}

	protected function getValorSimCardAttribute ()
	{
		return 'R$ 1,00';
	}

	protected function getFormaPagamentoAttribute ()
	{
		return 'A vista';
	}

	protected function getTT_PFPJ_NomeDoadorAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTT_PFPJ_CPFDoadorAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTT_PFPJ_PessoaContatoAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTT_PFPJ_TelContatoAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTT_PFPJ_CelContatoAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTT_PFPJ_NumerolinhaAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getTT_PFPJ_SimcardAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getStatusAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getMotivoReprovaAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getIPPAttribute ()
	{
		return  ''; #não preenchido
	}

	protected function getDddAttribute ()
	{
		if (count($this->linhas) <= 0)
			return null;

		return $this->linhas[0]->ddd;
	}

	protected function getDiaVencimentoAttribute ()
	{
		if (count($this->linhas) <= 0)
			return null;

		return $this->linhas[0]->dia_vencimento;
	}

	protected function getBonusAttribute ()
	{
		if (count($this->linhas) <= 0)
			return null;

		$plano = explode('+', $this->linhas[0]->plan_name);

		if (!isset($plano[1]))
			return 'NÂO';

		return 'SIM';
	}

	protected function getCreatedAtFormatadoAttribute ()
	{
		return date('d/m/Y H:i:s', strtotime($this->created_at));
	}

	protected function getCustomerFullnameAttribute ()
	{
		$name = $this->customer_firstname;
		
		if (!empty($this->customer_lastname))
			$name .= ' ' . $this->customer_lastname;

		return $name;
	}

	/**
	 * Custom Scopes
	 */

	public function scopeApenasAtivos ($query)
	{
		return $query->whereNotNull('activated_date');
	}
	
	public function scopeClaro ($query)
	{
		return $query->where('plan_operator', 'CLARO');
	}

	public function scopeImputarDados ($query)
	{
		return $query->where('status', 'inputar_dados');
	}

	public function scopePorPeriodo ($query, $de = null, $ate = null)
	{
		if (!is_null($de))
			$query->where('created_at', '>=', $de);

		if (!is_null($ate))
			$query->where('created_at', '<=', $ate);

		return $query;
	}

	public function scopePorCompetencia ($query, \App\Models\Contestador\Competencia $competencia)
	{
		return $query
			->whereRaw("activated_date IS NOT NULL")
			->whereRaw("DATE_FORMAT(activated_date, '%Y%m') = '" . $competencia->ano_string . $competencia->mes_string . "'")
		;
	}

	/**
	 * Relationship
	 */

	public function linhas ()
	{
		return $this->hasMany(\App\Models\Magento\Corp\OrderLinha::class, 'order_id', 'entity_id');
	}

	public function contestador_pedido ()
	{
		return $this->morphOne(\App\Models\Contestador\Pedido::class, 'pedido');
	}
	
	public function receita ()
	{
		return $this->hasOne(\App\Models\Magento\Corp\ConsultaReceita::class, 'increment_id', 'increment_id');
	}

	public function contato ()
	{
		return $this->hasOne(\App\Models\Magento\Corp\OrderContato::class, 'order_id', 'entity_id');
	}

	public function addresses ()
	{
		return $this->hasMany(\App\Models\Magento\Corp\OrderAddress::class, 'parent_id', 'entity_id');
	}
	
	public function billing_address ()
	{
		return $this->belongsTo(\App\Models\Magento\Corp\OrderAddress::class, 'billing_address_id', 'entity_id');
	}

	public function shipping_address ()
	{
		return $this->belongsTo(\App\Models\Magento\Corp\OrderAddress::class, 'shipping_address_id', 'entity_id');
	}
}