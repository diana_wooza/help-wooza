<?php

namespace App\Models\Magento\Corp;

use Illuminate\Database\Eloquent\Model;

class OrderContato extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'sales_flat_order_contato';
	protected $connection		= 'magento_corp';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];
}