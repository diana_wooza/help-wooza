<?php

namespace App\Models\Importador;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Importador\Exception\CancelUpload;
use App\Models\Importador\Exception\RemoveLine;
use App\Models\Importador\Log\Importacao as LogImportador;

class Importador extends Model
{
	#Dados básicos do Model Eloquent
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'importador';
	protected $connection		= 'importador';
	protected $fillable			= ['nome', 'regras', 'usuario'];
	protected $casts			= [
		'layout'					=> 'object',
		'regras'					=> 'object',
		'usuarios'					=> 'object',
	];

	private static $cache_DuplicidadeBancodedados = [];

	public function usuarioLogadoPodeUsar ()
	{
		return in_array(\Auth::user()->id, $this->usuarios);
	}

	public function executar (Request $request)
	{
		if (!$request->hasFile('file'))
			throw new \Exception('Não foi possível prosseguir com a importação. Arquivo não reconhecido');

		if (!isset($this->regras->obter))
			throw new \Exception('Nenhuma regra de obtenção de arquivo foi definida para esta importação. Não é possível obter o arquivo sem uma regra definida');

		if (!isset($this->regras->gravar))
			throw new \Exception('Nenhuma regra de gravação de importação foi definida. Não é possível continuar o tratamento do arquivo');

		$inseridos = [];
		$removidos = [];

		switch (@$this->regras->obter->arquivo->tipo)
		{
			case 'csv':

				\Config::set('excel.csv.delimiter', ';');
				$dados = Excel::load($request->file('file')->getRealPath(), null, 'UTF-8')->toArray();
				break;

			default:

				throw new \Exception('Erro de configuração de regra. Não foi possível fazer a leitura do tipo de arquivo especificado. Regra de obtenção de arquivo com formato especificado inexistente ou inválido');
		}

		if (is_array(@$this->regras->editar_inicial))
		{
			foreach ($dados as &$dado)
			{
				$insert = true;
				$dado['__original'] = json_encode($dado);

				foreach ($this->regras->editar_inicial as &$regra)
				{
					try
					{
						$dado = $this->getRegra($regra, $dado);
					}
					catch (RemoveLine $line)
					{
						$removidos[] = [
							"regra"	=> $line->regra,
							"dado"	=> json_decode($line->dado['__original']),
						];
						$insert = false;
						break;
					}
					catch (CancelUpload $cancel)
					{
						throw new \Exception($cancel->getMessage());
					}
				}

				if ($insert)
					$inseridos[] = $dado;
			}
		}

		$filePath = $request->file('file')->storeAs('', date('YmdHis') . '_' . $request->file('file')->getClientOriginalName(), 'importador');

		switch (@$this->regras->gravar->tipo)
		{
			case 'bancodedados':

				if (!isset($this->regras->gravar->conexao))
					throw new \Exception('A regra de gravação sem definição de conexão. Favor redefinir as configurações de importação');

				if (!isset($this->regras->gravar->tabela))
					throw new \Exception('A regra de gravação sem definição de tabela. Favor redefinir as configurações de importação');

				$paraOBancodedados = [];
				$dadosVazios = [];

				foreach ($this->regras->gravar->campos as $campo)
					$dadosVazios[$campo->banco] = '';
				
				foreach ($inseridos as $inserir)
				{
					$novoRegistro = [];

					foreach ($this->regras->gravar->campos as $campo)
					{
						if (isset($inserir[$campo->arquivo]))
							$novoRegistro[$campo->banco] = $inserir[$campo->arquivo];
					}

					if (!empty($novoRegistro))
						$paraOBancodedados[] = array_merge($dadosVazios, $novoRegistro);
				}

				if (empty($paraOBancodedados))
					throw new \Exception('O processo eliminou todos os dados do registro original, não restou nada para ser inserido no banco de dados');
					
				#if (!DB::connection($this->regras->conexao)->table($this->regras->gravar->tabela)->insert($dados))
				#	throw new Exception('Ocorreu um erro ao inserir os registros no banco de dados. Se os dados enviados correspondem aos campos da tabla escolhida');
				break;

			default:

				throw new \Exception('Erro de configuração de regra. Não foi possível fazer a leitura do tipo de arquivo especificado. Regra de obtenção de arquivo com formato especificado inexistente ou inválido');
		}

		return LogImportador::gerar($this, $filePath, $inseridos, $removidos);
	}

	private function getRegra ($regra, &$dado)
	{
		switch (@$regra->tipo)
		{
			case 'verifica_existencia';

				return $this->regra_VerificaExistencia($regra, $dado);
				break;

			case 'substitui_regex':

				return $this->regra_substituiRegex($regra, $dado);
				break;

			case 'remover_espacos':

				return $this->regra_removerEspacos($regra, $dado);
				break;

			case 'verifica_duplicidade_bancodedados':

				return $this->regra_verificaDuplicidadeBancodedados($regra, $dado);
				break;

			case 'inclusao_valor_data':

				return $this->regra_inclusaoValorData($regra, $dado);
				break;

			case 'preencher_zero':

				return $this->regra_preencherZero($regra, $dado);
				break;

			case 'verifica_valores_possiveis':

				return $this->regra_verificaValoresPossiveis($regra, $dado);
				break;

			case 'condicional_coluna':

				return $this->regra_condicionalColuna($regra, $dado);
				break;

			default:

				throw new CancelUpload('Foi definida uma regra inexistente', $dado, $regra);
		}
	}

	/**
	 *
	 * @json {"tipo":"condicional_coluna","campo":"tipo","operacao":"equal","valor":"cnpj","regra":{regra}}
	 */
	private function regra_condicionalColuna (&$regra, &$dado)
	{
		switch ($regra->operacao)
		{
			case 'equal':

				if ($dado[$regra->campo] == $regra->valor)
					$dado = $this->getRegra($regra->regra, $dado);
				
				break;

			default:

				throw new CancelUpload('Foi definida uma operação inválida para a coluna', $regra, $dado);
		}

		return $dado;
	}

	/**
	 *
	* @json {"tipo":"substitui_regex", "campo":"valor", "expressao":"/[ ()-]/i", "substituto":""}
	 */
	private function regra_substituiRegex (&$regra, &$dado)
	{
		$dado[$regra->campo] = preg_replace($regra->expressao, $regra->substituto, $dado[$regra->campo]);

		return $dado;
	}

	/**
	 *
	 * @json {"tipo":"remover_espacos","campo":"endereco"}
	 */
	private function regra_removerEspacos (&$regra, &$dado)
	{
		$dado[$regra->campo] = str_replace(' ', '', $dado[$regra->campo]);

		return $dado;
	}

	/**
	 *
	 * @json {"tipo":"verifica_valores_possiveis","campo":"tipo","valores":["cpf","ip","email","endereco","telefone_origem","telefone_contato"]}
	 * @json {"tipo":"verifica_valores_possiveis","campo":"tipo","valores":["cnpj","cpf","email","ip","telefone"]}
	 */
	private function regra_verificaValoresPossiveis (&$regra, &$dado)
	{
		if (!in_array($dado[$regra->campo], $regra->valores))
			throw new RemoveLine('Coluna veio com o valor incorreto', $regra, $dado);

		return $dado;
	}
 
	 /**
	 *
	 * @json {"tipo":"verifica_duplicidade_bancodedados","coluna_dados":"tipo","coluna_banco":"valor","conexao_banco":"magento","tabela_banco":"blacklist"}
	 */
	private function regra_verificaDuplicidadeBancodedados (&$regra, &$dado)
	{
		$chave = implode('|', array_map(function ($item) {return $item->banco;}, $regra->campos));
		$tabela = DB::connection($regra->conexao)->table($regra->tabela);

		if (!isset(self::$cache_DuplicidadeBancodedados[$chave]))
		{
			self::$cache_DuplicidadeBancodedados[$chave] = [];

			foreach ($tabela->selectRaw('CONCAT_WS(\'|\', ' . implode(',', array_map(function ($item) {return $item->banco;}, $regra->campos)) . ') AS valor')->get() as $item)
				self::$cache_DuplicidadeBancodedados[$chave][] = $item->valor;
		}

		if (in_array(implode('|', array_map(function ($campo) use (&$dado) {return $dado[$campo->arquivo];}, $regra->campos)), self::$cache_DuplicidadeBancodedados[$chave]))
			throw new RemoveLine("Item já existente no banco de dados", $regra, $dado);

		return $dado;
	}

	private function regra_verificaDuplicidadeBancodedados_montagemChave (&$regra, &$dado)
	{

	}

	/**
	 *
	 * @json {"tipo":"preencher_zero","tamanho":14,"campo":"valor"}
	 */
	private function regra_preencherZero (&$regra, &$dado)
	{
		$zeroes = '';

		for ($i = 0; $i < $regra->tamanho; $i++)
			$zeroes .= '0';

		$dado[$regra->campo] = substr($zeroes . $dado[$regra->campo], $regra->tamanho * -1);

		return $dado;
	}

	/**
	 *
	 * @json {"tipo":"inclusao_valor_data", "coluna":"data", "formato":"Y-m-d"}
	 */
	private function regra_inclusaoValorData (&$regra, &$dado)
	{
		$data = date($regra->formato);

		$dado[$regra->coluna] = $data;

		return $dado;
	}

	/**
	 *
	 * @json {"tipo":"verifica_existencia", "campos":["Valor","Tipo"], "casesensitive":false}
	 */
	private function regra_VerificaExistencia (&$regra, &$dado)
	{
		$colunasRequeridas = $regra->campos;
		$colunasExistentes = array_keys($dado);

		if ($regra->casesensitive === false)
		{
			$colunasRequeridas = array_map('strtolower', $colunasRequeridas);
			$colunasExistentes = array_map('strtolower', $colunasExistentes);
		}

		foreach ($colunasRequeridas as $coluna)
		{
			if (!in_array($coluna, $colunasExistentes))
				throw new CancelUpload('Não foi possível encontrar a coluna <strong>' . $coluna . '</strong> no arquivo enviado. Esta coluna é obrigatória para essa importação', 1);
		}

		return $dado;
	}

	public static function disponiveisUsuarioLogado ()
	{
		$importadores = [];
		
		foreach (self::get() as $importador)
		{
			if (!$importador->usuarioLogadoPodeUsar())
				continue;

			$importadores[] = $importador;
		}

		return $importadores;
	}
}