<?php

namespace App\Models\Importador\Log;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Importacao extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log';
	protected $connection		= 'importador';
	protected $fillable			= ['data', 'importador', 'usuario', 'arquivo','resultado'];
	
	public function getResultadoAttribute ($valor)
	{
		if (is_null($valor))
			return null;

		return \json_decode(\gzuncompress($valor));
	}

	public function setResultadoAttribute ($valor)
	{
		if (!is_null($valor))
			$valor = \gzcompress(\json_encode($valor));

		$this->attributes['resultado'] = $valor;
	}

	public function clearAndSave ()
	{
		$this->resultado = null;
		Storage::disk('importador')->delete($this->arquivo);

		$this->save();
	}

	public static function getOlderThan (int $timeInDays = 30)
	{
		return self
			::whereNull('resultado')
			->where('data', '<', date('Y-m-d H:i:S', strtotime('-' . $timeInDays . ' days')))
			->get()
		;
	}

	public static function gerar (\App\Models\Importador\Importador $importador, string $arquivo, $inseridos, $removidos)
	{
		return self::create([
			'data'			=> Date('Y-m-d H:i:s'),
			'importador'	=> $importador->id,
			'usuario'		=> @\Auth::user()->id,
			'arquivo'		=> $arquivo,
			'resultado'		=> ['inseridos' => \count($inseridos), 'removidos' => $removidos]
		]);
	}
}