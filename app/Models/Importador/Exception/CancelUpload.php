<?php

namespace App\Models\Importador\Exception;

class CancelUpload implements \Exception
{
	public $regra;
	public $dado;

	public function __construct ($mensagem, $regra, $dado)
	{
		$this->regra = $regra;
		$this->dado = $dado;

		parent::__construct($mensagem);
	}
}