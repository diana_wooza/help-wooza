<?php

namespace App\Models\Importador;

use Illuminate\Support\Facades\DB;

class Cpfvendedor
{
	public static function atualizarBase (array $itens)
	{
		foreach ($itens as $item)
		{
			DB::connection('magento')->table('sales_flat_order_origem_pedido')
				->join('sales_flat_order', 'sales_flat_order_origem_pedido.order_id', 'sales_flat_order.entity_id')
				->where('sales_flat_order.increment_id', $item['pedido'])
				->update(['cpf_vendedor_parceiro' => $item['cpf']])
			;
		}
	}
}