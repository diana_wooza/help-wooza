<?php 
namespace App\Models\Importador;

use Jenssegers\Mongodb\Eloquent\Model;

class Log extends Model
{
	protected $connection                   = 'alarmistica_mongo';
	protected $collection                   = 'log_tim';
	protected $fillable                     = ['message', 'context', 'level','level_name', 'channel', 'datetime', 'extra'];

	const OK                                = 200;
	const BAD_REQUEST                       = 500;
	const LEVEL_INFO                        = 'INFO';
	const LEVEL_ERROR                       = 'ERROR';
	const CHANNEL_DOWNGRADE_TIM             = 'downgrade_tim_importacao';
	const CHANNEL_DOWNGRADE_VIVO            = 'downgrade_vivo_importacao';
	const CHANNEL_IMPORTACAO_STATUS_ORDER   = 'importacao_status_order_hub';

	public static function logIt ( $channel, $message, $data, $level = self::OK, $levelName = self::LEVEL_INFO)
	{
		(new self)->setCollection("log_vivo");
		$log = [
			'message'       => $message,
			'context'       => json_encode($data),
			'level'         => $level,
			'level_name'    => $levelName,
			'channel'       => $channel,
			'datetime'      => date('Y-m-d H:i:s'),
			'extra'         => [],
		];

		self::create($log);
	}

	public static function logItVivo ( $channel, $message, $data, $level = self::OK, $levelName = self::LEVEL_INFO)
	{
		(new self)->setCollection("log_vivo");
		$log = [
			'message'       => $message,
			'context'       => json_encode($data),
			'level'         => $level,
			'level_name'    => $levelName,
			'channel'       => $channel,
			'datetime'      => date('Y-m-d H:i:s'),
			'extra'         => [],
		];

		self::create($log);
	}

	public static function lastLog( $channel ) : ?\App\Models\Importador\Log {
		(new self)->setCollection("log_tim");
		return self::where('channel',$channel)->where('level', self::OK)->orderBy('datetime', 'DESC')->first();
	}

	public static function lastLogVivo( $channel ) : ?\App\Models\Importador\Log {
		(new self)->setCollection("log_vivo");
		
		return self::where('channel',$channel)->where('level', self::OK)->orderBy('datetime', 'DESC')->first();
	}

	public function setCollection($collection) {
		$this->collection = $collection;
	}
}
