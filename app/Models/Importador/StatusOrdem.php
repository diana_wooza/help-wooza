<?php

namespace App\Models\Importador;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Importador\Log;
use Auth;

class StatusOrdem {
	const SUCCESS_MESSAGE 		    = 'Importação realizada com sucesso.';
    const FAIL_MESSAGE 			    = 'Não foi possível fazer a importação desta planilha.';
    const LIMITE_EXCEEDED           = 'Limite de 100 linhas na planilha excedido.';
    const ACCESS_DENIED             = 'Acesso restrito a administradores.';
    const LIMITE_IMPORTACAO         = 100;
    const USUARIO_SISTEMA_MAGENTO   = 'sistema.magento';
	
    public static function criarViaUpload($data) : Array
	{
        ini_set("max_execution_time", 120);
        
        if( !Auth::check() ) {
            throw new \Exception( self::ACCESS_DENIED );
        }
        
		try{
            $totalItens 		= count($data);
            $linhasEmBranco     = 0;
            $itemsNaoEditados   = 0;
            
            if( $totalItens > self::LIMITE_IMPORTACAO ) {
                throw new \Exception( self::LIMITE_EXCEEDED );    
            }

            foreach( $data as $item ) {
                if( !$item['increment_id'] && !$item['activated_date'] && !$item['status'] ) {
                    $linhasEmBranco++;
                    continue;
                }

                if( !$item['increment_id'] || !$item['activated_date'] || !$item['status'] ) {
                    $itemsNaoEditados++;
                    continue;
				}
				
				$item['increment_id'] = trim($item['increment_id']);

                $activated_date = date('Y-m-d H:i',strtotime('+2 hour',strtotime($item['activated_date'])));
                $sql = "UPDATE sales_flat_order SET 
                            status = '" . $item['status'] . "', 
                            activated_date = '" . $activated_date . "', 
                            activated_user = IF(preactivated_user IS NULL, activated_user, preactivated_user) 
                        WHERE increment_id = '" . $item['increment_id'] . "';";

                $grid = "UPDATE sales_flat_order_grid SET 
                            status = '" . $item['status'] . "'
                        WHERE 
                            increment_id = '" . $item['increment_id'] . "';";

                $sqlPedido = "SELECT 
                                s.entity_id, s.status, s.plan_operator
                              FROM 
                                sales_flat_order s 
                              WHERE increment_id = '" . $item['increment_id'] . "'";

                $pedido = DB::connection('magento')->select(DB::raw($sqlPedido))[0];

                $sqlUserId = "SELECT user_id FROM admin_user where username = '" . self::USUARIO_SISTEMA_MAGENTO . "'";

                $user = DB::connection('magento')->select(DB::raw($sqlUserId))[0];

                $mensagem = "Alteração de Status " . $item['status'] . " com a activated_date em: " . $item['activated_date'];
                $history = "INSERT INTO sales_flat_order_status_history (parent_id, is_visible_on_front, comment, status, admin_user_id, plan_operator, created_at) 
                VALUES(".$pedido->entity_id.", 0, '".$mensagem."','" . $item['status'] . "', ". $user->user_id .", '". $pedido->plan_operator ."', CURRENT_TIMESTAMP());";


                DB::connection('magento')->update($sql);
                DB::connection('magento')->update($grid);
                DB::connection('magento')->insert($history);
			}
		}
		catch( \Exception $e ) {
			throw new \Exception( $e );
		}
		
		$result = [
            'total_itens_planilha' 	=> ($totalItens-$linhasEmBranco),
            'total_itens_editados'  => ($totalItens-$linhasEmBranco) - $itemsNaoEditados,
            'total_itens_ignorados' => $itemsNaoEditados,
			'imported_at' 			=> date('Y-m-d H:i:s'),
			'user'					=> (isset(Auth::user()->id)) ? ['id' => Auth::user()->id, 'login' => Auth::user()->login] : '',
        ];
        		
		return $result;
    }
}
  