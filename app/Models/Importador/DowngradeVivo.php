<?php

namespace App\Models\Importador;

use Illuminate\Database\Eloquent\Model;
use App\Models\Importador\Log;
use Auth;

class DowngradeVivo extends Model {
    protected $connection 		= 'magento';
	protected $table 			= 'downgrade_vivo';
	protected $fillable 		= ['plano_atual', 'plano_destino', 'tipo_movimentacao',];
	public $timestamps 			= false;
	

	const SUCCESS_MESSAGE 		= 'Importação realizada com sucesso.';
	const FAIL_MESSAGE 			= 'Não foi possível fazer a importação desta planilha.';
	
    public static function criarViaUpload($data) : Array
	{
		$user = Auth::check() ? Auth::user() : null;

		try{
			$contagemInseridos 	= 0;
			$contagemEditados 	= 0;
			$contagemNulos 		= 0;
			$totalItens 		= count($data);
			
			foreach( $data as $item ) {
				$model = self::where('plano_atual',$item['plano_atual'])->where('plano_destino',$item['plano_destino'])->get();

				if( 0 == count($model) ) {
					if( 0 == strlen( $item['tipo_movimentacao'] ) || 0 == strlen( $item['plano_destino'] ) || 0 == strlen( $item['plano_atual'] ) ) {
						$contagemNulos++;
						continue;
					}
					
					
					$contagemInseridos++;
					$plano = [
						'plano_atual' 	=> $item['plano_atual'],
						'plano_destino'	=> $item['plano_destino'],
						'tipo_movimentacao'	=> $item['tipo_movimentacao'],
					];
					
					self::create($plano);
					
					continue;
				}

				foreach( $model as $row ) {
					if( 0 == strlen( $item['tipo_movimentacao'] ) || 0 == strlen( $item['plano_destino'] ) || 0 == strlen( $item['plano_atual'] ) ) {
						$contagemNulos++;
						continue;
					}
					
					if( $row->tipo_movimentacao != $item['tipo_movimentacao'] ) {
						$contagemEditados++;

						$plano = self::find($row->id);
						$plano->tipo_movimentacao = $item['tipo_movimentacao'];
						$plano->save();
					}
				}
			}
		}
		catch( \Throwable $e ) {
			Log::logItVivo( Log::CHANNEL_DOWNGRADE_VIVO, self::FAIL_MESSAGE, $e->getMessage(), Log::BAD_REQUEST, Log::LEVEL_ERROR  );

			throw new \Exception($e->getMessage());
		}
		
		$result = [
			'itens_inseridos' 		=> $contagemInseridos,
			'itens_editados' 		=> $contagemEditados,
			'itens_ignorados' 		=> $contagemNulos,
			'total_importado' 		=> ($contagemInseridos + $contagemEditados),
			'total_itens_planilha' 	=> $totalItens,
			'imported_at' 			=> date('Y-m-d H:i:s'),
			'user'					=> (isset($user->id)) ? ['id' => $user->id, 'login' => $user->login] : '',
		];
		
		Log::logItVivo( Log::CHANNEL_DOWNGRADE_VIVO, self::SUCCESS_MESSAGE, $result );

		return $result;
	}
}
  