<?php

namespace App\Models\Importador;

use Illuminate\Support\Facades\DB;

class Phonenumber
{
	public static function atualizarBase (array $itens)
	{
		foreach ($itens as $item)
		{
			DB::connection('magento')
				->table('sales_flat_order')
				->join('sales_flat_order_grid', 'sales_flat_order_grid.entity_id', 'sales_flat_order.entity_id')
				->where('sales_flat_order.increment_id', $item['pedido'])
				->update([
					'sales_flat_order.phone_service' => $item['phone'],
					'sales_flat_order_grid.phone_service' => $item['phone'],
				])
			;
		}
	}
}