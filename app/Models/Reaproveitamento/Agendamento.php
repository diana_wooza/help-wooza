<?php

namespace App\Models\Reaproveitamento;

use Illuminate\Database\Eloquent\Model;

class Agendamento extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'agendamento';
	protected $connection		= 'reaproveitamento';
	public $timestamps = false;
	
}