<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Magento\Consumer\Order as MagentoOrderConsumer;
use App\Models\Magento\Corp\Order as MagentoOrderCorp;

class Operadora extends Model
{
	use SoftDeletes;
	
	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'operadoras';
	protected $connection			= 'hub';
	protected $attributes			= [
		'layout'						=> '{"background":"#000000FF","text":"#FFFFFFFF"}',
		'adicionais'					=> '{"nome_operadora_magento":null, "regex_emissor_xml_nota": null}',
	];
	protected $fillable				= ['status','nome','codigo','layout'];
	protected $hidden				= ['adicionais', 'deleted_at'];
	protected $casts				= [
		'layout'						=> 'object',
		'adicionais'					=> 'object',
	];
	protected $appends				= [];

	protected static $all			= null;

	public function CheckIfEmissorNota (\App\Models\Colmeia\Nfe $nfe) : bool
	{
		if (is_null(@$this->adicionais->regex_emissor_xml_nota))
			return false;

		return preg_match($this->adicionais->regex_emissor_xml_nota, $nfe->emissor) == 1;
	}

	public function CheckIfPedidoMagentoConsumer (MagentoOrderConsumer $order) : bool
	{
		if (is_null(@$this->adicionais->nome_operadora_magento))
			return false;

		return $order->plan_operator == $this->adicionais->nome_operadora_magento;
	}

	public function CheckIfPedidoMagentoCorp (MagentoOrderCorp $order) : bool
	{
		if (is_null(@$this->adicionais->nome_operadora_magento))
			return false;

		return $order->plan_operator == $this->adicionais->nome_operadora_magento;
	}

	public static function GetOperadoraPedidoMagentoConsumer (MagentoOrderConsumer $order) : ?self
	{
		if (!self::$all)
			self::$all = self::get();

		foreach (self::$all as $operadora)
		{
			if ($operadora->CheckIfPedidoMagentoConsumer($order))
				return $operadora;
		}

		return null;
	}

	public static function GetOperadoraPedidoMagentoCorp (MagentoOrderCorp $order) : ?self
	{
		if (!self::$all)
			self::$all = self::get();

		foreach (self::$all as $operadora)
		{
			if ($operadora->CheckIfPedidoMagentoCorp($order))
				return $operadora;
		}

		return null;
	}

	/**
	 * Sobre escreve a funcionalidade para gerar automaticamente LOG em cada edição do model
	 * @method boot
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}

	/**
	 * Mutators
	 */
	protected function getBulletAttribute ()
	{
		return '<span class="label label-default" style="background-color:' . @$this->layout->background . '; color: ' . @$this->layout->text . ';">' . $this->nome . '</span>';
	}
}
