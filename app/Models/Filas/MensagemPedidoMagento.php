<?php
namespace App\Models\Filas;

class MensagemPedidoMagento {
    public $pedido;
    public $fila;

    public function __construct($pedido, $fila) {
        $this->pedido = $pedido;
        $this->fila = $fila;
    }
}