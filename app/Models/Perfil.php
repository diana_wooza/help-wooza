<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use \App\Models\User;
use \App\Models\Hub\Log\Perfil as Log;

class Perfil extends Model
{
	use SoftDeletes;
	
	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'perfis';
	protected $connection			= 'hub';
	protected $attributes			= [];
	protected $fillable				= ['nome','key','admin_id','permissoes'];
	protected $hidden				= ['deleted_at'];
	protected $casts				= [
		'permissoes'					=> 'object',
	];
	protected $appends				= [];

	public function checkPermissao ($permissao)
	{
		return in_array($permissao, $this->permissoes);
	}

	/**
	 * Sobre escreve a funcionalidade para gerar automaticamente LOG em cada edição do model
	 * @method boot
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}

	/**
	 * Mutators
	 */

	

	/**
	 * Scopes
	 */
	public function scopePorPermissao ($query, $permissao)
	{
		return $query->whereRaw('JSON_SEARCH(permissoes, \'one\', \'' . $permissao . '\') IS NOT NULL');
	}

	/**
	 * Relationships
	 */

	public function usuarios ()
	{
		return $this->belongsToMany(User::class, 'usuarios_perfis')->using(\App\Models\Hub\Pivots\UsuarioPerfil::class);
	}

	public function admin ()
	{
		return $this->belongsTo(Self::class, 'admin_id');
	}

	public function children ()
	{
		return $this->hasMany(self::class, 'admin_id');
	}
}
