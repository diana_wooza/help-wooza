<?php

namespace App\Models\Alarmistica;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Alarmistica\Log;
use App\Models\Alarmistica\Historico\Api as Historico;

class Api extends Model
{
	protected $table	  = 'api';
	protected $connection = 'alarmistica';
	public $timestamps	  = false;

	protected $fillable	= ['id', 'status', 'nome', 'collection', 'tabela', 'consulta', 'range_positivo', 'range_negativo', 'sms', 'segmento_id', 'configuracao'];
	protected $casts	= ['range_negativo'	=> 'float',
						   'range_positivo'	=> 'float',
						   'sms'			=> 'object',
						   'configuracao'   => 'object'];

	/**
	 * Verifica apartir da chamada do cron se os 2 ultimos logs de cada api esta com percentual menor que o range negativo estipulado
	 *
	 * @param int $api
	 *
	 * @return boolean
	 */
	public function estaComPercentualAlarmante ()
	{
		/*$tempoPesquisaLog - essa configuração é para determinar o tempo de pesquisa no 
		 * mongo e no log que é gerado, geralmente tem o mesmo valor que a configuracao 
		 */
		$tempoPesquisaLog = $this->configuracao->consulta_schedule;

		switch($tempoPesquisaLog):
			case '5min': $query = 'log_consulta_api.data > NOW() - INTERVAL 6 minute';
			break;
			case '30min': $query = 'log_consulta_api.data > NOW() - INTERVAL 31 minute';
			break;
			case '1h': $query = 'log_consulta_api.data > NOW() - INTERVAL 61 minute';
			break;
			
		endswitch;

		$itensAlarmantes =  DB::connection('alarmistica')
			->table('api')
			->join('log_consulta_api', 'log_consulta_api.api', '=', 'api.id')
			->where('api.id', $this->id)
			->whereRaw($query)
			->whereRaw('log_consulta_api.percentual < api.range_negativo')
			->count();

		return isset($this->configuracao->itens_alarmantes) ? $this->configuracao->itens_alarmantes <= $itensAlarmantes : 2 <= $itensAlarmantes;
	}

	public function gerarLog ()
	{
		#Definir timezone do mongo para evitar problemas
		date_default_timezone_set('UTC');

		/*$tempoPesquisaLog - essa configuração é para determinar o tempo de pesquisa no 
		 * mongo e no log que é gerado, geralmente tem o mesmo valor que a configuracao 
		 */
		$tempoPesquisaLog = $this->configuracao->consulta_schedule;

		switch($tempoPesquisaLog):
			case '5min' : $tempo = '-5 minutes';
			break;
			case '30min': $tempo = '-30 minutes';
			break;
			case '1h'   : $tempo = '-60 minutes';
			break;
			
		endswitch;

		$dataInicial = date('Y-m-d H:i:s', strtotime($tempo));
		$dataFinal   = date('Y-m-d H:i:s');

		$pedidos = PedidoMongo::fromApi($this, $dataInicial, $dataFinal);
		$classificacao = ['positivos' => array(), 'negativos' => array()];

		foreach ($pedidos as $pedido)
			$classificacao[$pedido->level == '200' ? 'positivos' : 'negativos'][] = $pedido->_id;
		
		return Log::create([
			'api'			=> $this->id,
			'pedidos'		=> $classificacao,
			'percentual'	=> $pedidos->count() > 0 ? count($classificacao['positivos']) / $pedidos->count() : 0,
		]);
	}

	public function estaComUsoRestrito ()
	{
		if (!isset($this->configuracao->restricao))
			return false; //envia

		if ($this->estaComUsoRestritoPorHorario())
			return true; //não envia

		return false;
	}

	private function estaComUsoRestritoPorHorario ()
	{
		if (!isset($this->configuracao->restricao->horario))
			return false; //envia

		$now = new \DateTime('NOW', new \DateTimeZone('America/Sao_Paulo'));
		
		foreach ($this->configuracao->restricao->horario as $horario)
		{
			$de = new \DateTime($horario->de, new \DateTimeZone('America/Sao_Paulo'));
			$ate = new \DateTime($horario->ate, new \DateTimeZone('America/Sao_Paulo'));

			if ($now >= $de && $now <= $ate)
				return false; //envia
		}

		return true; //não envia
	}

	public static function todasAtivas ($api = '', $group = false, $tipo = '')
	{
		if(!empty($api) && $tipo != 'TodasAtivasInativas'){
			$qtdApis = count($api);

			$apis = self::select(DB::raw("api.id, api.nome, api.collection, api.status, log_consulta_api.data, log_consulta_api.id, log_consulta_api.api, api.sms, api.configuracao, api.segmento_id,
											CONCAT(REPLACE(ROUND(log_consulta_api.percentual, 2) * 100, '.00', ''), '%') AS Percentual_atual,
											CONCAT(CEIL(api.range_negativo * 100), '%') AS range_negativo_api,
											CONCAT(CEIL(api.range_positivo * 100), '%') AS range_positivo_api"))
						->join('log_consulta_api', 'api.id', '=', 'log_consulta_api.api')
						->whereIn('api.id', $api)
						->where(function ($query) use ($group) {

							if ($group == true)
								$query->groupBy('api.id');
	
							return $query;
						
						})
						->orderBy('log_consulta_api.id', 'desc')
						->limit($qtdApis)
						->get();

		} else if ($tipo == 'TodasAtivasInativas'){

			$apis = self::select(DB::raw("api.id, api.nome, api.consulta, api.collection, api.status, api.sms, api.configuracao, api.segmento_id, 
			                      CEIL(api.range_negativo * 100) AS range_negativo_api,
								  CEIL(api.range_positivo * 100) AS range_positivo_api"))
						->where(function ($query) use ($api) {

							if (!empty($api))
								$query->where('api.id', $api);
	
							return $query;
						
							})			  
						->orderBy('collection')->get();

		} else {
			$apis = self::where('status', 1)->orderBy('collection')->get();
		}

		return $apis;
	}

	public static function listaApiCartao ($api = '', $group = false)
	{
		if(!empty($api) && $api != 'TodasAtivasInativas'){
			$qtdApis = count($api);

			return self::select(DB::raw("api.nome, api.collection, api.status, log_consulta_api.data, log_consulta_api.id, log_consulta_api.api, api.sms, api.configuracao, api.segmento_id,
											CONCAT(REPLACE(ROUND(log_consulta_api.percentual, 2) * 100, '.00', ''), '%') AS Percentual_atual,
											CONCAT(CEIL(api.range_negativo * 100), '%') AS range_negativo_api,
											CONCAT(CEIL(api.range_positivo * 100), '%') AS range_positivo_api"))
						->join('log_consulta_api', 'api.id', '=', 'log_consulta_api.api')
						->whereIn('api.id', $api)
						->where(function ($query) use ($group) {

							if ($group == true)
								$query->groupBy('api.id');
	
							return $query;
						
						})
						->orderBy('log_consulta_api.id', 'desc')
						->limit($qtdApis)
						->get();		
		}
	}

	public static function listaApiSelect ($collection = '', $segmento = '', $status = '')
	{
		if($collection != 0 && $segmento != 0) {
			$apis = self::select(DB::raw("api.nome, api.collection, api.status,
										  segmento_api.nm_segmento as segmento,
										  CONCAT(CEIL(api.range_negativo * 100), '%') AS range_negativo_api"))
										  ->join('segmento_api', 'api.segmento_id', '=', 'segmento_api.id')
										  ->where('api.segmento_id', $segmento)
										  ->where('api.collection', $collection)
										  ->orderBy('api.id', 'desc')
										  ->get();
										  
										  return $apis;

		} elseif($status != '' && $status != 0) {
			
			$apis = self::select(DB::raw("api.nome, api.collection, api.status,
										  segmento_api.nm_segmento as segmento,
										  CONCAT(CEIL(api.range_negativo * 100), '%') AS range_negativo_api"))
						->join('segmento_api', 'api.segmento_id', '=', 'segmento_api.id')
						->where('api.status', $status)
						->orderBy('api.id', 'desc')
						->get();

			return $apis;
		}
	}

	public static function apiSelect ($collection, $segmento = '')
	{
		if($collection != ''){
			$lista = self::select(DB::raw("DISTINCT(api.collection), segmento_api.id, segmento_api.nm_segmento"))
						->join('segmento_api', 'segmento_api.id', '=', 'api.segmento_id')
						->where('collection', $collection)
						->where('segmento_api.flag_ativo', 1)
						->where(function ($query) use ($segmento) {
							
							if ($segmento == true)
								$query->where('segmento', $segmento);
	
							return $query;
						
						})
						->orderBy('nm_segmento')
						->get();						

			return $lista;
		}
	}

	public static function selectFilhoApi ($collection, $segmento, $statusAll = '', $ativasInativas = '')
	{
		if($collection != '' && $segmento != ''){
			
			if(empty($statusAll) && empty($ativasInativas)){ 
				
				$lista = self::select(DB::raw("api.id, api.nome"))
							->join('segmento_api', 'segmento_api.id', '=', 'api.segmento_id')
							->where('api.collection', $collection)
							->where('segmento_api.id', $segmento)
							->where('segmento_api.flag_ativo', 1)
							->where('api.status', 1)
							->orderBy('nm_segmento')
							->get();						

			} elseif(empty($statusAll) && $ativasInativas) { 
				$lista = self::where('api.collection', $collection)
							->where('segmento_id', $segmento)
							->orderBy('nome')->get();

			} else { 
				$lista = self::where('api.collection', $collection)
							->orderBy('collection')->get();
			}

			return $lista;
		}
	}

	/**
	 * boot - cria log automaticamente casa seja alterado ou criada uma api
	 *
	 * @return void
	 */
	public static function boot ()
	{
		parent::boot();

		self::created(function ($model) {Historico::gerar('criar', $model);});
		self::saving(function ($model) {Historico::gerar('editar', $model);});
	}

}