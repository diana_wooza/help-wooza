<?php

namespace App\Models\Alarmistica;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	protected $table			= 'log_consulta_api';
	protected $connection		= 'alarmistica';
	public $timestamps			= false;

	protected $fillable			= ['api', 'percentual', 'pedidos'];
	protected $casts			= ['pedidos'    => 'array',
								   'percentual'	=> 'float',
								   'data'		=> 'datetime'];

	public static function retornaLogGrafico ()
	{

		return self::select(DB::raw("api, api.nome, api.consulta, CONCAT(CEIL(range_negativo * 100), '%') AS range_negativo_api, 
									 CONCAT(REPLACE(ROUND(log_consulta_api.percentual, 2) * 100, '.00', ''), '%') AS percentual_atual,
									 DATE_FORMAT(log_consulta_api.data, '%Y-%m-%d %H:%i') AS data_hora, 
									 (CASE WHEN (log_consulta_api.percentual < api.range_negativo) THEN 'negativo' ELSE 'positivo' END) AS estado_atual"))
					->join('api', 'api.id', '=', 'log_consulta_api.api')
					->whereRaw('log_consulta_api.percentual < api.range_negativo')
					// ->whereRaw('data > NOW() - INTERVAL 30 minute AND api.status = 1')
					->whereRaw('(data > NOW() - INTERVAL 40 hour) AND api.status = 1')
					->orderBy('data_hora')
					->limit(18)
					->get();
	}

	// public static function listaLogs () 
	// {
	// 	return self::select(DB::raw("api.nome, api.collection, log_consulta_api.id,
	// 						CONCAT(CEIL(log_consulta_api.percentual * 100), '%') AS percentual_atual,
	// 						CONCAT(CEIL(range_negativo * 100), '%') AS range_negativo_api,
	// 						CONCAT(CEIL(range_positivo * 100), '%') AS range_positivo_api,
	// 						log_consulta_api.data"))
	// 				->join('api', 'api.id', '=', 'log_consulta_api.api')
	// 				->whereRaw('(data > NOW() - INTERVAL 40 day) AND api.status = 1')
	// 				->orderBy('log_consulta_api.data')
	// 				->get();
	// }

	public static function retornaDadosCsv ($ids = '') 
	{
		return self::select(DB::raw("log_consulta_api.id AS id_log, api.nome AS Nome, SUBSTRING(api.collection, 5)  AS Operadora, 
							segmento_api.nm_segmento AS Segmento,
							CONCAT(REPLACE(ROUND(log_consulta_api.percentual, 2) * 100, '.00', ''), '%') AS Percentual_atual,
							CONCAT(CEIL(range_negativo * 100), '%') AS Percentual_negativo,
							CONCAT(CEIL(range_positivo * 100), '%') AS Percentual_positivo,
							log_consulta_api.data AS Data_hora"))
					->join('api', 'api.id', '=', 'log_consulta_api.api')
					->join('segmento_api', 'segmento_api.id', '=', 'api.segmento_id')
					->whereIn('log_consulta_api.id', $ids)
					->get();
	}

	public static function listaLogsSelect($collection, $idSegmento, $api, $percentual, $dataI, $dataF) 
	{
		$dataInicio = str_replace('/', '-', $dataI[0]);
		$dataFim    = str_replace('/', '-', $dataF[0]);
		$dataInicio = date('Y-m-d H:i:s', strtotime($dataInicio . "+ 3 hour"));
		$dataFim 	= date('Y-m-d H:i:s', strtotime($dataFim . "+ 3 hour"));
		// $api = explode(',', $api);
		
		return self::select(DB::raw("log_consulta_api.id AS id_log, api.nome, SUBSTRING(api.collection, 5)  AS operadora, 
							segmento_api.nm_segmento AS segmento,
							CONCAT(REPLACE(ROUND(log_consulta_api.percentual, 2) * 100, '.00', ''), '%') AS percentual_atual,
							CONCAT(CEIL(range_negativo * 100), '%') AS range_negativo_api,
							CONCAT(CEIL(range_positivo * 100), '%') AS range_positivo_api,
							DATE_SUB(log_consulta_api.data, INTERVAL 3 hour) AS data"))
					->join('api', 'api.id', '=', 'log_consulta_api.api')
					->join('segmento_api', 'segmento_api.id', '=', 'api.segmento_id')
					->whereIn('segmento_api.id', $idSegmento)
					->where('api.collection', $collection)
					->where('data', '>=', $dataInicio)
					->where('data', '<=', $dataFim)
					->where(function ($query) use ($api, $percentual) {

						if ($api[0] != 0)
							$query->where('api.id', '=', $api);

						if($percentual != '')	
							$query->whereRaw($percentual);

						return $query;
					
					})
					->get();
	}

}