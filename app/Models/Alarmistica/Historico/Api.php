<?php

namespace App\Models\Alarmistica\Historico;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_alteracao_api';
	protected $connection		= 'alarmistica';
	protected $fillable			= ['api', 'usuario','informacao'];
	protected $casts			= [
		'data_alteracao'		=> 'datetime',
		'informacao'			=> 'object',
	];

	public static function gerar (string $tipo, \App\Models\Alarmistica\Api $entidade)
	{
		if (!$entidade->isDirty() || is_null($entidade->id))
			return;

		$titulo = 'daddos_alterados';	
		if($tipo == 'criar')	
			$titulo = 'dados_criados';

			return self::create([
				'usuario'	   => @\Auth::user()->id,
				'api'		   => $entidade->id,
				'informacao'   => ['tipo' => $tipo, $titulo => $entidade->getDirty()]
		]);
	}
}