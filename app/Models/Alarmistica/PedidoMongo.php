<?php

namespace App\Models\Alarmistica;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Models\Alarmistica\Api;

class PedidoMongo extends Model
{
	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= null;
	protected $connection		= 'alarmistica_mongo';

	public static function fromApi (Api $api, $dataInicial, $dataFinal)
	{
		// return "Pedido";DB::enableQueryLog();
		\DB::enableQueryLog();

		return (new self())
				->setTable($api->collection)
				->select('_id', 'level', 'context.codigo_pedido', 'datetime')
				->where('datetime', '>=', $dataInicial)
				->where('datetime', '<=', $dataFinal)
				->where('channel', '=', $api->consulta)
				->get();
				// ->limit(100)
				// \DB::getQueryLog();
	}

	
	/**
	 * getChannel - recupera os channel de uma collection
	 *
	 * @return object
	 * @param string $collection
	 */
	public static function getChannel ($collection)
	{
		return (new self())
				->setTable($collection)
				->select('channel')
				->distinct('channel')
				->orderBy('channel', 'desc')
				->get();

	}
}