<?php
namespace App\Models\Alarmistica;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Alarmistica\Log;

class StatusApi extends Model {

    protected $connection = "alarmistica";
    protected $table      = "status_api";
    protected $fillable   = ['id', 'nm_status', 'flag_status'];

    public $timestamps    = false;

    public static function fromStatus ($nm_status = '', $id = '', $flag = '')
    {
        // return $nm_status;
        if($nm_status != '') {
            $retorno = self::where('nm_status', $nm_status)
                           ->first();

        } elseif($id != '') {
            $retorno = self::find($id);

        } elseif($flag != '') {
            $retorno = self::where('flag_status', $flag)
                            ->get();
        }

        return $retorno;

    }

    // public static function 

}