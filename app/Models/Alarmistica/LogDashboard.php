<?php
namespace App\Models\Alarmistica;

use Illuminate\Database\Eloquent\Model;

class LogDashboard extends Model {
    protected $connection = 'alarmistica';
    protected $table      = 'log_consulta_api_dashboard';
    protected $fillable   = ['id', 'id_api', 'id_status_api', 'pedidos_positivos', 'pedidos_negativos', 'percentual_positivo', 'dt_consulta'];
    public $timestamps	  = false;
}