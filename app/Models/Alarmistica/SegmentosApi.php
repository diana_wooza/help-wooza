<?php

namespace App\Models\Alarmistica;

use Illuminate\Database\Eloquent\Model;

class SegmentosApi extends Model
{
	protected $table	  = 'segmento_api';
	protected $connection = 'alarmistica';
	public $timestamps	  = false;

	protected $fillable	= ['id', 'nm_segmento', 'flag_ativo'];
                           
    public static function fromStatus ($flag_ativo = '', $id = '', $nm_segmento = '')
    {
        // return $nm_status;
        if($flag_ativo != '') {
            $retorno = self::where('flag_ativo', $flag_ativo)
                            ->first();

        } elseif($id != '') {
            $retorno = self::find($id);

        } elseif($nm_segmento != '') {
            $retorno = self::where('nm_segmento', $nm_segmento)
                            ->get();
        }

        return $retorno;

    }
}