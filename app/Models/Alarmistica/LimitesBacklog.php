<?php
namespace App\Models\Alarmistica;

use Illuminate\Database\Eloquent\Model;

class LimitesBacklog extends Model {
    protected $connection = 'alarmistica';
    protected $table = 'limites_backlog';
    public $timestamps = false;
}