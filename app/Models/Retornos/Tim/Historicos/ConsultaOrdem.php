<?php

namespace App\Models\Retornos\Tim\Historicos;

use Illuminate\Database\Eloquent\Model;

class ConsultaOrdem extends Model {
    protected $connection 		= 'magento';
	protected $table 			= 'consulta_ordem_historico';
    public $timestamps 			= false;
    
    const TIM = 'TIM';
    
    public static function listByOrder( $id ) {
        $model = self::whereIn('pedido', explode(",",$id))->where('operadora', self::TIM)->orderBy('pedido', 'ASC')->orderBy('id', 'ASC')->get();

        return $model;
    }

    public static function findResumoHistory() {
        $model = self::selectRaw('COUNT(DISTINCT(pedido)) AS total_por_status, status_ordem')->whereRaw('Date(created_at) = CURDATE()')->groupBy('status_ordem')->orderBy('total_por_status', 'DESC')->get();

        return $model;
    }
}