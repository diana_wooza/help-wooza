<?php

namespace App\Models\AdminUserBase;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class AdminUserBase {
	const SUCCESS_MESSAGE 		    = 'Importação realizada com sucesso.';
    const FAIL_MESSAGE 			    = 'Não foi possível fazer a importação desta planilha.';
    const ACCESS_DENIED             = 'Acesso restrito a administradores.';
    
    public static function listar() : Array
	{
        if( !Auth::check() ) {
            throw new \Exception( self::ACCESS_DENIED );
        }
        
        $result = null;

		try{
                $sql = "SELECT
                                id,
                                nome, 
                                usuario_loja, 
                                senha_loja, 
                                codigo_loja, 
                                segmento
                            FROM
                                admin_user_base
                            ORDER BY id DESC;";

                $result = DB::connection('magento')->select(DB::raw($sql));
		}
		catch( \Exception $e ) {
			throw new \Exception( $e );
		}
				
		return $result;
    }

    public static function criar( $model )
    {
        if( !Auth::check() ) {
            throw new \Exception( self::ACCESS_DENIED );
        }

        try{
            $sql = "INSERT INTO admin_user_base 
                    (nome, usuario_loja, senha_loja, codigo_loja, segmento) 
                    VALUES('" . $model->nome . "','" . $model->usuario_loja . "','" . $model->senha_loja . "','" . $model->codigo_loja . "','" . $model->segmento . "');";
    
            DB::connection('magento')->update($sql);
            DB::connection('magento_qa')->update($sql);
        }
		catch( \Exception $e ) {
			throw new \Exception( $e );
		}
    }
}
  