<?php

namespace App\Models\Configuracao;

use Illuminate\Database\Eloquent\Model;

class AcoesTratativas extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'acoes_tratativas';
	protected $connection		= 'configuracao';
	public $timestamps = false;
	
}