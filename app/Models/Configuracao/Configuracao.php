<?php

namespace App\Models\Configuracao;

use Jenssegers\Mongodb\Eloquent\Model;

class Configuracao extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'configuracao';
	protected $connection		= 'alarmistica_mongo';
	
}