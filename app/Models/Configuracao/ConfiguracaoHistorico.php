<?php

namespace App\Models\Configuracao;

use Jenssegers\Mongodb\Eloquent\Model;

class ConfiguracaoHistorico extends Model {

    const UPDATED_AT = null; 

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'configuracao_historico';
	protected $connection		= 'alarmistica_mongo';
	
}