<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \App\Models\User;
use \App\Models\Perfil;

class Config extends Model
{
	public $incrementing		= false;
	public $timestamps			= false;
	protected $primaryKey		= 'chave';
	protected $table			= 'config';
	protected $connection		= 'hub';
	protected $casts			= [
		'valor'						=> 'array',
	];

	public function tratarValorInput ($valor)
	{
		switch ($this->tipo)
		{
			case 4:

				$valor = explode(',', $valor);
				break;

			case 5:

				$valor = $valor === '1';
				break;
		}

		return $valor;
	}

	public static function getValor ($key)
	{
		return @self::where('chave', $key)->first()->valor;
	}

	public static function getListaUsuariosHubParaSelect ()
	{
		return User::porPermissao('hub.admin')->pluck('nome', 'id');
	}

	public static function getUsuariosContatoHub ()
	{
		$usuarios = self::getValor('contato_hub');

		if (is_null($usuarios))
			$usuarios = [];

		return User::whereIn('id', $usuarios)->get();
	}
}