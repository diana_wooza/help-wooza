<?php

namespace App\Models\Portal;

use Illuminate\Notifications\Notifiable;
use \Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsuarioPortal extends Authenticatable {
	// use SoftDeletes;
	
	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'usuarios';
	protected $connection			= 'portal';
	// protected $attributes			= [
	// 	'configuracoes'					=> '{}',
	// 	'dados'							=> '{"telefone":null, "email":null}',
	// ];
	// protected $fillable				= ['nome','login','token','configuracoes','dados','email','telefone'];
	// protected $hidden				= ['deleted_at','dados'];
	// protected $casts				= [
	// 	'configuracoes'					=> 'object',
	// 	'dados'							=> 'object',
	// ];
	// protected $appends				= ['email','email_original','telefone'];

	// private $permissoes				= null;

}
