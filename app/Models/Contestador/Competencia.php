<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Competencia extends Model
{
	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'competencias';
	protected $connection			= 'contestador';
	protected $attributes			= [];
	protected $fillable				= ['data'];
	protected $hidden				= ['deleted_at'];
	protected $casts				= [];
	protected $appends				= ['ano', 'mes'];

	private static $competencias	= null;

	public static function GetByDataYYYYMM ($data)
	{
		if (is_null(self::$competencias))
			self::$competencias = self::get()->keyBy(function ($competencia) {return $competencia->ano_string . $competencia->mes_string;});

		return @self::$competencias[$data];
	}

	/**
	 * Mutators
	 */

	protected function getMesAttribute ()
	{
		return intVal(substr($this->data, 5, 2));
	}

	protected function getMesStringAttribute ()
	{
		return substr('00' . $this->mes, -2);
	}
	
	protected function setMesAttribute ($value)
	{
		if (!is_numeric($value) || $value < 1 || $value > 12)
			throw new \Exception('Tentando adicionar um ano inválido ' . $value);

		$this->data = substr('0000' . $this->ano, -4) . '-' . substr('00' . $value, -2) . '-00';
	}

	protected function getAnoAttribute ()
	{
		return intVal(substr($this->data, 0, 4));
	}

	protected function getAnoStringAttribute ()
	{
		return substr('0000' . $this->ano, -4);
	}
	
	protected function setAnoAttribute ($value)
	{
		if (!is_numeric($value) || $value < 1000 || $value > 9999)
			throw new \Exception('Tentando adicionar um ano inválido ' . $value);

		$this->data = substr('0000' . $value, -4) . '-' . substr('00' . $this->mes, -2) . '-00';
	}

	protected function getDataStringAttribute ()
	{
		return Carbon::createFromFormat('Y-m-d H:i:s', substr($this->data, 0, 7) . '-01 00:00:00')->format('M/Y');
	}

	protected function getDataStringCleanAttribute ()
	{
		return str_replace('/', '-', $this->data_string);
	}

	protected function getPedidosCountFormatadoAttribute ()
	{
		return number_format($this->pedidos_count, 0, ',', '.');
	}

	protected function getReceitasCountFormatadoAttribute ()
	{
		return number_format($this->receitas_count, 0, ',', '.');
	}

	/**
	 * Relationship
	 */

	public function pedidos ()
	{
		return $this->hasMany(\App\Models\Contestador\Pedido::class);
	}

	public function receitas ()
	{
		return $this->hasMany(\App\Models\Contestador\Receita::class);
	}
}