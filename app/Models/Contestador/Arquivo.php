<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arquivo extends Model
{
	use SoftDeletes;

	const STATUS_ATIVO				= 1;
	const STATUS_INATIVO			= 0;

	const ARMAZENAMENTO_DB			= 1;
	const ARMAZENAMENTO_FILE		= 2;

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'arquivos';
	protected $connection			= 'contestador';
	protected $attributes			= [
		'status'						=> self::STATUS_ATIVO,
		'layout'						=> '{}',
		'tratamentos'					=> '[]',
		'regras'						=> '[]',
		'configuracao'					=> '{"armazenamento":' . self::ARMAZENAMENTO_DB . '}',
	];
	protected $fillable				= ['status','nome','layout','tratamentos','regras','configuracao'];
	protected $hidden				= ['deleted_at'];
	protected $casts				= [
		'status'						=> 'integer',
		'layout'						=> 'object',
		'tratamentos'					=> 'object',
		'regras'						=> 'object',
		'configuracao'					=> 'object',
	];
	protected $appends				= [];
	protected $with					= [];

	public static $statuses			= [
		self::STATUS_ATIVO				=> 'Ativo',
		self::STATUS_INATIVO			=> 'Inativo',
	];

	public static $armazenamentos	= [
		self::ARMAZENAMENTO_DB			=> 'Armazenar no Banco de Dados',
		self::ARMAZENAMENTO_FILE		=> 'Amazenar em Arquivo',
	];

	/**
	 * Mutators
	 */

	protected function getArmazenamentoStringAttribute ()
	{
		if (!isset(self::$armazenamentos[@$this->configuracao->armazenamento]))
			return '<i class="text-grey">Desconhecido</i>';

		return self::$armazenamentos[@$this->configuracao->armazenamento];
	}

	protected function getPodeFazerUploadAttribute ()
	{
		return $this->status == self::STATUS_ATIVO;
	}

	protected function getStatusBulletAttribute ()
	{
		switch ($this->status)
		{
			case self::STATUS_ATIVO:

				$class = 'label-success';
				break;
				
			case self::STATUS_INATIVO:

				$class = 'label-warning';
				break;
			
			default:
			
				$class = 'label-default';
				break;

		}

		return '<span class="label ' . $class . '">' . $this->status_string . '</span>';
	}

	protected function getStatusStringAttribute ()
	{
		if (!isset(self::$statuses[$this->status]))
			return 'desconhecido';

		return self::$statuses[$this->status];
	}

	/**
	 * Scopes
	 */

	public function scopeAtivos ($query)
	{
		return $query->where('status', self::STATUS_ATIVO);
	}

	/**
	 * Relationship
	 */
	
	public function operadora ()
	{
		return $this->belongsTo(\App\Models\Operadora::class);
	}
}