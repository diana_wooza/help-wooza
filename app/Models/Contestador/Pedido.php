<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
	const SEGMENTO_MOVEL			= 1;
	const SEGMENTO_BANDALARGA		= 2;
	const SEGMENTO_SVA				= 3;
	const SEGMENTO_VAREJO_LOJA		= 4;
	const SEGMENTO_VAREJO_ONLINE	= 5;
	const SEGMENTO_CORP				= 6;

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'pedidos';
	protected $connection			= 'contestador';
	protected $attributes			= [];
	protected $fillable				= ['competencia_id','operadora_id','base','segmento','pedido_id','pedido_type','increment_id','usuario_ativacao_id','valor'];
	protected $hidden				= ['deleted_at'];
	protected $casts				= [];
	protected $appends				= [];

	public static $segmentos			= [
		self::SEGMENTO_MOVEL				=> 'Móvel',
		self::SEGMENTO_BANDALARGA			=> 'Banda Larga',
		self::SEGMENTO_SVA					=> 'SVA',
		self::SEGMENTO_VAREJO_LOJA			=> 'Varejo Físico',
		self::SEGMENTO_VAREJO_ONLINE		=> 'Varejo Digital',
		self::SEGMENTO_CORP					=> 'Corporativo',
	];

	public static function GetSegmentoPedidoConsumer (\App\Models\Magento\Consumer\Order $order)
	{
		if (is_null($order->admin_base))
			return null;

		if ($order->admin_base->segmento == 'varejo_loja')
			return self::SEGMENTO_VAREJO_LOJA;

		if ($order->admin_base->segmento == 'varejo_online')
			return self::SEGMENTO_VAREJO_ONLINE;

		if ($order->plan_tipo == 'SVA')
			return self::SEGMENTO_SVA;

		if (in_array($order->plan_tipo, ['Combo','Internet Fixa']))
			return self::SEGMENTO_BANDALARGA;

		return self::SEGMENTO_MOVEL;
	}

	/**
	 * Mutators
	 */

	protected function getOperadoraBulletAttribute ()
	{
		if (is_null($this->operadora))
			return '<span class="text-gray">não definida</span>';

		return $this->operadora->bullet;
	}

	protected function getRemuneracaoValueStringAttribute ()
	{
		$this->loadMissing('remuneracoes');
		
		if (count($this->remuneracoes) <= 0)
			return '<span class="text-gray">nenhuma</span>';

		return 'R$ ' . number_format($this->remuneracoes->sum('valor'), 2, ',', '.');
	}

	protected function getChurnValueStringAttribute ()
	{
		$this->loadMissing('churns');

		if (count($this->churns) <= 0)
			return '<span class="text-gray">nenhuma</span>';

		return 'R$ ' . number_format($this->churns->sum('valor'), 2, ',', '.');
	}

	protected function getComissoesValueStringAttribute ()
	{
		$this->loadMissing('comissoes');

		if (count($this->comissoes) <= 0)
			return '<span class="text-gray">nenhuma</span>';

		return 'R$ ' . number_format($this->comissoes->sum('valor'), 2, ',', '.');
	}

	protected function getValorStringAttribute ()
	{
		if (is_null($this->valor))
			return '<span class="text-gray">indisponível</span>';

		return number_format($this->valor, 2, ',', '.');
	}

	protected function getIncrementIdAttribute ($value)
	{
		return \mb_strtoupper($value);
	}

	protected function getSegmentoStringAttribute ()
	{
		if (!isset(self::$segmentos[$this->attributes['segmento']]))
			return 'Desconhecido';

		return self::$segmentos[$this->attributes['segmento']];
	}

	protected function getSegmentoBulletAttribute ()
	{
		$colors = ['#454545FF', '#FFFFFFFF'];

		switch ($this->attributes['segmento'])
		{
			case self::SEGMENTO_MOVEL:

				$colors[0] = '#E4FFDBFF';
				$colors[1] = '#454545FF';
				break;

			case self::SEGMENTO_CORP:
			
				$colors[0] = '#DBFFFBFF';
				$colors[1] = '#454545FF';
				break;

			case self::SEGMENTO_VAREJO_LOJA:
			
				$colors[0] = '#F5CA95FF';
				$colors[1] = '#454545FF';
				break;

			case self::SEGMENTO_VAREJO_ONLINE:
			
				$colors[0] = '#F5AA95FF';
				$colors[1] = '#454545FF';
				break;
				
		}

		return '<span class="label" style="background-color:' . $colors[0] . '; color: ' . $colors[1] . ';">' . $this->segmento_string . '</span>';
	}

	/**
	 * Custom Scopes
	 */

	public function scopeApenasMarcados ($query)
	{
		return $query->where(function ($innerQuery) use ($query) {

			$innerQuery
				->orWhereHas('remuneracoes')
				->orWhereHas('churns')
			;

		});
	}

	public function scopeApenasDesmarcados ($query)
	{
		return $query->where(function ($innerQuery) use ($query) {

			$innerQuery
				->doesntHave('remuneracoes')
				->doesntHave('churns')
			;

		});
	}

	public function scopeComFiltro ($query, \Illuminate\Http\Request $request)
	{

		if ($request->has('com_remuneracao') && $request->com_remuneracao == 'sim')
			$query->whereHas('remuneracoes');

		if ($request->has('com_churn') && $request->com_churn == 'sim')
			$query->whereHas('churns');

		if ($request->has('operadora') && !empty($request->operadora))
			$query->whereIn('operadora_id', $request->operadora);

		if ($request->has('competencia') && !empty($request->competencia))
			$query->whereIn('competencia_id', $request->competencia);

		if ($request->has('base') && !empty($request->base))
			$query->whereIn('base', $request->base);

		if ($request->has('segmento') && !empty($request->segmento))
			$query->whereIn('segmento', $request->segmento);

		if ($request->has('marcacao') && !empty($request->marcacao))
		{
			switch ($request->marcacao)
			{
				case 'marcados':

					$query->ApenasMarcados();
					break;

				case 'desmarcados':

					$query->ApenasDesmarcados();
					break;
			}
		}

		return $query;
	}

	public function scopeApenasConsumer ($query)
	{
		return $query->where('pedido_type', \App\Models\Magento\Consumer\Order::class);
	}

	public function scopeApenasCorp ($query)
	{
		return $query->where('pedido_type', \App\Models\Magento\Corp\Order::class);
	}

	public function scopeComOrdenacao ($query, \Illuminate\Http\Request $request)
	{
		$sortBy = $request->has('sort_by') && !empty($request->sort_by) ? $request->sort_by : 'id';
		$sortOrder = $request->has('sort_order') && !empty($request->sort_order) ? $request->sort_order : 'desc';

		switch ($sortBy)
		{
			case 'segmento':

				$cases = [];

				foreach (self::$segmentos as $key => $segmento)
					$cases[] = 'WHEN segmento = ' . $key . ' THEN \''  . $segmento . '\'';

				$cases = implode(PHP_EOL, $cases);

				return $query->orderByRaw("
					CASE
						{$cases}
					END {$sortOrder}
				");
		}
		
		return $query->orderBy($sortBy, $sortOrder);
	}

	/**
	 * Relationship
	 */

	public function remuneracoes ()
	{
		return $this->hasMany(\App\Models\Contestador\Receita::class)->ApenasRemuneracao();
	}

	public function churns ()
	{
		return $this->hasMany(\App\Models\Contestador\Receita::class)->ApenasChurn();
	}

	public function competencia ()
	{
		return $this->belongsTo(\App\Models\Contestador\Competencia::class);
	}

	public function operadora ()
	{
		return $this->belongsTo(\App\Models\Operadora::class);
	}

	public function comissoes ()
	{
		return $this->hasMany(\App\Models\Contestador\Comissao::class);
	}

	public function pedido ()
	{
		return $this->morphTo();
	}
}