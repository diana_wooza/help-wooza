<?php

namespace App\Models\Contestador\Queries\Base;

use Illuminate\Support\Collection;

use App\Models\Contestador\Queries\Base\Resultado;
use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;
use App\Models\Contestador\Pedido;
use App\Models\Operadora;

abstract class Query
{
	public static function GetInfo ()
	{
		$info = new \stdClass();

		$info->tipo				= static::class;
		$info->rotulo			= static::$rotulo;
		$info->descricao		= static::$descricao;
		$info->colunas			= static::$colunas;
		$info->comparacoes		= static::$comparacoes;

		return $info;
	}

	public static function Executar (Collection &$receitas, Operadora $operadora, Regra $regra) : Resultado
	{
		throw new \Exception(static::class . ' não implementou Executar');
	}

	public static function ValidateConfiguracao ($comparacao, $index)
	{
		throw new \Exception(static::class . ' não implementou Validação');
	}

	public static function GetPedidoType ()
	{
		throw new \Exception(static::class . ' não implementou get pedido type');
	}
}