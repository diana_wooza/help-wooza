<?php

namespace App\Models\Contestador\Queries\Base;

use Illuminate\Support\Collection;

use App\Models\Contestador\Pedido;
use App\Models\Contestador\Receita;
use App\Models\Contestador\Queries\Base\Estatisticas;

class Resultado
{
	public $receitas;
	public $pedidos;

	public function __construct ()
	{
		$this->pedidos = new Collection();
		$this->receitas = new Collection();
		$this->estatisticas = new Estatisticas();
	}
}