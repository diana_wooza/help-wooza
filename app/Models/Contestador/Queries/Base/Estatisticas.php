<?php

namespace App\Models\Contestador\Queries\Base;

class Estatisticas
{
	public $receitas	= 0;
	public $expiradas	= 0;
	public $marcadas	= 0;

	public function GetPercentualMarcado ()
	{
		$receitas = $this->receitas - $this->expiradas;
		
		if ($receitas <= 0)
			return '--';

		$percent = $this->marcadas / $receitas;
		$percent *= 100;
				
		return $this->_formatNumber($percent, 2) . '%';
	}

	public function GetTotalFormatado ()
	{
		return $this->_formatNumber($this->receitas);
	}

	public function GetMarcadosFormatado ()
	{
		return $this->_formatNumber($this->marcadas);
	}

	public function GetExpiradoFormatado ()
	{
		return $this->_formatNumber($this->expiradas);
	}

	private function _formatNumber ($number, $decimals = 0)
	{
		return \number_format($number, $decimals, ',', '.');
	}
}