<?php

namespace App\Models\Contestador\Queries;

use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Models\Contestador\Queries\Base\Query;
use App\Models\Contestador\Queries\Base\Resultado;
use App\Models\Magento\Consumer\Order;
use App\Models\Magento\Consumer\OrderDependente;
use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;
use App\Models\Contestador\Pedido;
use App\Models\Operadora;
use App\Helpers\Magento;

class SalesFlatOrderLoteConsumer extends Query
{
	public static $rotulo			= 'Sales Flat Order em Lote (Consumer)';
	public static $descricao		= 'Utiliza apenas a tabela sales_flat_order na busca';
	public static $colunas			= [
		'protocolo'							=> [
			'rotulo'						=> 'Protocolo da Operação',
			'parser'						=> [],
			'query'							=> ['method' => 'where', 'field' => 'protocolo_servico', 'data' => 'protocolo'],
		],
		'linha'							=> [
			'rotulo'						=> 'Número da Linha',
			'parser'						=> [],
			'query'							=> ['method' => 'where', 'field' => 'phone_service', 'data' => 'terminal'],
		],
		'dependente'					=> [
			'rotulo'						=> 'Número do Dependente',
			'parser'						=> ['split_telefone'],
			'query'							=> ['method' => 'where_dependente', 'field' => 'linha_dependente', 'data' => 'terminal'],
		],
		'provisorio'					=> [
			'rotulo'						=>'Número Provisório',
			'parser'						=> [],
			'query'							=> ['method' => 'where_provisorio', 'field' => 'provisional_number', 'data' => 'terminal'],
		],
		'ativacao'						=> [
			'rotulo'						=> 'Data de Ativaçao',
			'parser'						=> [],
			'query'							=> ['method' => 'between', 'field' => 'activated_date', 'data' => 'data'],
		],
		'cricao'						=> [
			'rotulo'						=> 'Data de Criação',
			'parser'						=> [],
			'query'							=> ['method' => 'between', 'field' => 'created_at', 'data' => 'data'],
		],
		// 'portabilidade'					=> [
		// 	'rotulo'						=> 'Data de Pré-Ativação',
		// 	'parser'						=> [],
		// 	'query'							=> ['method' => 'between', 'field' => 'preactivated_date', 'data' => 'data'],
		// ],
		// 'agendamento'					=> [
		// 	'rotulo'						=> 'Data de Agendamento da Portabilidade',
		// 	'parser'						=> [],
		// 	'query'							=> ['method' => 'Query_between_agendamento', 'field' => 'datahora', 'data' => 'data'],
		// ],
		'cpf'							=> [
			'rotulo'						=> 'CPF do Cliente',
			'parser'						=> [],
			'parser'						=> ['encrypt'],
			'query'							=> ['method' => 'where', 'field' => 'customer_cpf', 'data' => 'documento'],
		],
	];
	public static $comparacoes		= [
		'exata'							=> [
			'rotulo'						=> 'Comparar Valor',
			'descricao'						=> 'Compara um valor específico com o mesmo valor no banco de dados',
			'colunas'						=> ['linha','dependente','provisorio','cpf','protocolo'],
			'configuracoes'					=> [
				'nome'							=> ['tipo' => 'text', 'rotulo' => 'Nome da Comparação', 'default' => '', 'colunas' => 4],
				'score'							=> ['tipo' => 'score', 'rotulo' => 'Score', 'default' => 0, 'colunas' => 2],
				'coluna'						=> ['tipo' => 'columns', 'rotulo' => 'Coluna da Base', 'default' => null, 'colunas' => 4],
				'com_data'						=> ['tipo' => 'bool', 'rotulo' => 'Incluir Data', 'default' => true, 'colunas' => 2],
			],
		],
		'periodo'						=> [
			'rotulo'						=> 'Comparar um Período de Data',
			'descricao'						=> 'Compara um valor específico com o mesmo valor no banco de dados descriminando separadamente quantos dias antes e quantos dias depois da data específica são aceitos',
			'colunas'						=> ['ativacao','cricao','portabilidade','agendamento'],
			'configuracoes'					=> [
				'nome'							=> ['tipo' => 'text', 'rotulo' => 'Nome da Comparação', 'default' => '', 'colunas' => 4],
				'score'							=> ['tipo' => 'score', 'rotulo' => 'Score', 'default' => 0, 'colunas' => 2],
				'coluna'						=> ['tipo' => 'columns', 'rotulo' => 'Coluna da Base', 'default' => null, 'colunas' => 4],
				'antes'							=> ['tipo' => 'number', 'rotulo' => 'Antes', 'default' => 0, 'colunas' => 1],
				'depois'						=> ['tipo' => 'number', 'rotulo' => 'Depois', 'default' => 0, 'colunas' => 1],
			],
		],
	];

	private static $corte			= '2018-08-01 00:00:00';
	private static $formats			= ['Y-m-d', 'Y-m-d H:i:s', 'Y-m-d\TH:i:s','Y-m-d\tH:i:s'];
	private static $chunkReceitas	= 5000;
	private static $chunkValores	= 500;

	public static function Executar (Collection &$receitas, Operadora $operadora, Regra $regra) : Resultado
	{
		$timeCorte = strtotime(self::$corte);
		$resultado = new Resultado();
		$resultado->estatisticas->receitas = $receitas->count();
		$comparacoes = $regra->GetComparacoesOrdenada();
		$receitas = $receitas->filter(function ($receita) use ($timeCorte) {return strtotime($receita->data) >= $timeCorte;})->all();
		$resultado->estatisticas->expiradas = $resultado->estatisticas->receitas - count($receitas);

		// Log::channel('contestador')->info('=====================================================================');
		// Log::channel('contestador')->info('= INICIANDO MARCAÇÂO ================================================');
		// Log::channel('contestador')->info('=====================================================================');

		foreach ($comparacoes as $comparacao)
		{
			// Log::channel('contestador')->info('Selecionando Comparação');
			// Log::channel('contestador')->info(json_encode($comparacao));

			$comparacao->regras = new Collection($comparacao->regras);

			$regraExata = $comparacao->regras->filter(function($regra) {return $regra->tipo == 'exata';})->first();
			$regraExata->coluna = self::$colunas[$regraExata->configuracoes->coluna];
			
			$regraPeriodo = $comparacao->regras->filter(function($regra) {return $regra->tipo == 'periodo';})->first();
			$regraPeriodo->coluna = self::$colunas[$regraPeriodo->configuracoes->coluna];

			// Log::channel('contestador')->info(count($receitas) . ' receitas restantes');

			foreach (array_chunk($receitas, self::$chunkReceitas, true) as $index => $grupoReceitas)
			{
				// Log::channel('contestador')->info('Chunk ' . ($index + 1));

				$query = Order
					::on('magento::write')
					->select('entity_id AS pedido_id','increment_id','activated_user','plan_price')
					->addSelect($regraPeriodo->coluna['query']['field'])
					->PorOperadora($operadora)
					->whereNotNull($regraPeriodo->coluna['query']['field'])
				;

				$valoresExatos = array_map(function ($receita) use ($regraExata) {return $receita->{$regraExata->coluna['query']['data']};}, $grupoReceitas);
				$query = self::{'Query_' . $regraExata->coluna['query']['method']}($query, $regraExata, $valoresExatos);

				// Log::channel('contestador')->info(\App\Helpers\QueryBuilder::ToSqlWithBindings($query));

				$valoresDatabase = $query->get()->groupBy($regraExata->coluna['query']['field']);

				foreach ($grupoReceitas as $indexReceita => $receita)
				{
					$valorIndex = $receita->{$regraExata->coluna['query']['data']};

					if (!$valoresDatabase->has($valorIndex))
						continue;

					$pedidoMaisProximo = null;
					$dataReceita = new \DateTime($receita->{$regraPeriodo->coluna['query']['data']} . ' 00:00:00');

					foreach ($valoresDatabase->get($valorIndex)->sortBy($regraPeriodo->coluna['query']['field']) as $valorDatabase)
					{
						$diferencaDatas = $dataReceita->diff(new \DateTime(substr($valorDatabase->{$regraPeriodo->coluna['query']['field']}, 0, 10) . ' 00:00:00'));

						if ($diferencaDatas->invert && $diferencaDatas->days > $regraPeriodo->configuracoes->antes)
							continue;
						
						if (!$diferencaDatas->invert && $diferencaDatas->days > $regraPeriodo->configuracoes->depois)
							continue;

						if ($pedidoMaisProximo !== null && $pedidoMaisProximo->diff->days <= $diferencaDatas->days)
							continue;

						$pedidoMaisProximo = (object) [
							'diff'		=> $diferencaDatas,
							'pedido'	=> $valorDatabase,
						];
					}

					if ($pedidoMaisProximo !== null)
					{
						unset($receitas[$indexReceita]);
						$resultado->pedidos->push($pedidoMaisProximo->pedido);
						$resultado->receitas->push((object) [
							'receita' => $receita->id,
							'pedido' => $pedidoMaisProximo->pedido->pedido_id,
							'comparacao' => $comparacao
						]);
					}
				}

				// Log::channel('contestador')->info('Até o momento ' . $resultado->receitas->count() . ' receitas encontradas');
			}
		};

		$resultado->estatisticas->marcadas = $resultado->receitas->count();

		return $resultado;
	}

	public static function ValidateConfiguracao ($comparacao, $index)
	{
		$errors = [];

		if (!in_array($comparacao->tipo, ['exata', 'periodo']))
		{
			$errors[] = 'Tipo de configuração inválida';

			return $errors;
		}

		switch ($comparacao->tipo)
		{
			case 'exata':
				
				if (empty(@$comparacao->configuracoes->nome))
					$errors[] = 'O nome da configuração não pode estar vazio';

				if (!is_numeric(@$comparacao->configuracoes->score))
					$errors[] = @$comparacao->configuracoes->score . ' é um valor inválido para o score';

				if (empty(@$comparacao->configuracoes->coluna))
					$errors[] = 'É necessário especificar um nome para a coluna';

				break;

			case 'periodo':

				if (empty(@$comparacao->configuracoes->nome))
					$errors[] = 'O nome da configuração não pode estar vazio';

				if (!is_numeric(@$comparacao->configuracoes->score))
					$errors[] = @$comparacao->configuracoes->score . ' é um valor inválido para o score';

				if (empty(@$comparacao->configuracoes->coluna))
					$errors[] = 'É necessário especificar um nome para a coluna';

				if (!is_numeric($comparacao->configuracoes->antes))
					$errors[] = 'É necessário especificar um número de dias para antes, mesmo que seja zero';

				if (!is_numeric($comparacao->configuracoes->depois))
					$errors[] = 'É necessário especificar um número de dias para depois, mesmo que seja zero';

				break;
		}

		if (count($errors) <= 0)
			return false;

		return $errors;
	}

	public static function GetPedidoType ()
	{
		return Order::class;
	}

	private static function Query_where (&$query, &$regra, $valores)
	{
		foreach ($regra->coluna['parser'] as $parser)
			$valores = array_map(Closure::fromCallable('self::Parser_' . $parser), $valores);

		return $query
			->addSelect($regra->coluna['query']['field'])
			->where(function ($query) use ($regra, $valores) {

				foreach (array_chunk($valores, self::$chunkValores) as $chunkValores)
					$query->orWhereIn($regra->coluna['query']['field'], $chunkValores);

			})
		;
	}

	private static function Query_where_provisorio (&$query, &$regra, $valores)
	{
		foreach ($regra->coluna['parser'] as $parser)
			$valores = array_map(Closure::fromCallable('self::Parser_' . $parser), $valores);

		return $query
			->IsPortabilidade()
			->addSelect($regra->coluna['query']['field'])
			->where(function ($query) use ($regra, $valores) {

				foreach (array_chunk($valores, self::$chunkValores) as $chunk)
					$query->orWhereIn($regra->coluna['query']['field'], $chunk);

			})
		;
	}

	private static function Query_where_dependente (&$query, &$regra, $valores)
	{
		foreach ($regra->coluna['parser'] as $parser)
			$valores = array_map(Closure::fromCallable('self::Parser_' . $parser), $valores);

		return $query
			->addSelect(DB::raw('CONCAT(sales_flat_order_dependente.ddd, sales_flat_order_dependente.linha) AS linha_dependente'))
			->join('sales_flat_order_dependente', function ($join) use ($valores) {

				$join
					->on('sales_flat_order_dependente.order_id', '=', 'sales_flat_order.entity_id')
					->where(function ($query) use ($valores) {

						foreach (array_chunk($valores, self::$chunkValores, true) as $chunk)
						{
							foreach ($chunk as $valor)
							{
								$query->orWhere(function ($query) use ($valor) {

									$query
										->where('sales_flat_order_dependente.ddd', $valor[0])
										->where('sales_flat_order_dependente.linha', $valor[1])
									;

								});
							}
						}
					})
				;

			})
		;
	}

	private static function Parser_encrypt ($value)
	{
		return Magento::encrypt($value);
	}

	private static function Parser_split_telefone ($value)
	{
		return [substr($value, 0, 2), substr($value, 2)];
	}
}