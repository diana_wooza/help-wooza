<?php

namespace App\Models\Contestador\Queries;

use Illuminate\Support\Collection;

use App\Models\Contestador\Queries\Base\Query;
use App\Models\Contestador\Queries\Base\Resultado;
use App\Models\Magento\Consumer\Order;
use App\Models\Magento\Consumer\OrderDependente;
use App\Models\Contestador\Receita;
use App\Models\Contestador\Regra;
use App\Models\Contestador\Pedido;
use App\Models\Operadora;
use App\Helpers\Magento;

class SalesFlatOrderPadraoConsumer extends Query
{
	public static $rotulo			= 'Sales Flat Order Padrão (Consumer)';
	public static $descricao		= 'Utiliza apenas a tabela sales_flat_order na busca';
	public static $colunas			= [
		'protocolo'							=> [
			'rotulo'						=> 'Protocolo da Operação',
			'parser'						=> [],
			'query'							=> ['method' => 'where', 'field' => 'protocolo_servico', 'data' => 'protocolo'],
		],
		'linha'							=> [
			'rotulo'						=> 'Número da Linha',
			'parser'						=> [],
			'query'							=> ['method' => 'where', 'field' => 'phone_service', 'data' => 'terminal'],
		],
		'dependente'					=> [
			'rotulo'						=> 'Número do Dependente',
			'parser'						=> ['split_telefone'],
			'query'							=> ['method' => 'where_dependente', 'field' => ['ddd', 'linha'], 'data' => 'terminal'],
		],
		'provisorio'					=> [
			'rotulo'						=>'Número Provisório',
			'parser'						=> [],
			'query'							=> ['method' => 'where_provisorio', 'field' => 'provisional_number', 'data' => 'terminal'],
		],
		'ativacao'						=> [
			'rotulo'						=> 'Data de Ativaçao',
			'parser'						=> [],
			'query'							=> ['method' => 'between', 'field' => 'activated_date', 'data' => 'data'],
		],
		'cricao'						=> [
			'rotulo'						=> 'Data de Criação',
			'parser'						=> [],
			'query'							=> ['method' => 'between', 'field' => 'created_at', 'data' => 'data'],
		],
		'portabilidade'					=> [
			'rotulo'						=> 'Data de Pré-Ativação',
			'parser'						=> [],
			'query'							=> ['method' => 'between', 'field' => 'preactivated_date', 'data' => 'data'],
		],
		'agendamento'					=> [
			'rotulo'						=> 'Data de Agendamento da Portabilidade',
			'parser'						=> [],
			'query'							=> ['method' => 'Query_between_agendamento', 'field' => 'datahora', 'data' => 'data'],
		],
		'cpf'							=> [
			'rotulo'						=> 'CPF do Cliente',
			'parser'						=> [],
			// 'parser'						=> ['encrypt'],
			'query'							=> ['method' => 'where', 'field' => 'customer_cpf', 'data' => 'documento'],
		],
	];
	public static $comparacoes		= [
		'exata'							=> [
			'rotulo'						=> 'Comparar Valor',
			'descricao'						=> 'Compara um valor específico com o mesmo valor no banco de dados',
			'colunas'						=> ['linha','dependente','provisorio','cpf','protocolo'],
			'configuracoes'					=> [
				'nome'							=> ['tipo' => 'text', 'rotulo' => 'Nome da Comparação', 'default' => '', 'colunas' => 4],
				'score'							=> ['tipo' => 'score', 'rotulo' => 'Score', 'default' => 0, 'colunas' => 2],
				'coluna'						=> ['tipo' => 'columns', 'rotulo' => 'Coluna da Base', 'default' => null, 'colunas' => 4],
				'com_data'						=> ['tipo' => 'bool', 'rotulo' => 'Incluir Data', 'default' => true, 'colunas' => 2],
			],
		],
		'periodo'						=> [
			'rotulo'						=> 'Comparar um Período de Data',
			'descricao'						=> 'Compara um valor específico com o mesmo valor no banco de dados descriminando separadamente quantos dias antes e quantos dias depois da data específica são aceitos',
			'colunas'						=> ['ativacao','cricao','portabilidade','agendamento'],
			'configuracoes'					=> [
				'nome'							=> ['tipo' => 'text', 'rotulo' => 'Nome da Comparação', 'default' => '', 'colunas' => 4],
				'score'							=> ['tipo' => 'score', 'rotulo' => 'Score', 'default' => 0, 'colunas' => 2],
				'coluna'						=> ['tipo' => 'columns', 'rotulo' => 'Coluna da Base', 'default' => null, 'colunas' => 4],
				'antes'							=> ['tipo' => 'number', 'rotulo' => 'Antes', 'default' => 0, 'colunas' => 1],
				'depois'						=> ['tipo' => 'number', 'rotulo' => 'Depois', 'default' => 0, 'colunas' => 1],
			],
		],
	];
	
	private static $corte			= '2018-08-01 00:00:00';
	private static $formats			= ['Y-m-d', 'Y-m-d H:i:s', 'Y-m-d\TH:i:s','Y-m-d\tH:i:s'];

	public static function Executar (Collection	&$receitas, Operadora $operadora, Regra $regra) : Resultado
	{
		$timeCorte = strtotime(self::$corte);
		$resultado = new Resultado();
		$resultado->estatisticas->receitas = $receitas->count();

		$comparacoes = $regra->GetComparacoesOrdenada();

		foreach ($receitas as $receita)
		{
			if (strtotime($receita->data) < $timeCorte)
			{
				$resultado->estatisticas->expiradas++;
				continue;
			}

			foreach ($comparacoes as $comparacao)
			{	
				$resposta = self::_Executar($receita, $operadora, $comparacao);

				if (!is_null($resposta))
				{
					$resultado->pedidos->push($resposta->pedido);
					$resultado->receitas->push((object) ['receita' => $receita->id, 'pedido' => $resposta->pedido->entity_id, 'comparacao' => $resposta->comparacao]);
					break;
				}
			}
		}

		$resultado->estatisticas->marcadas = $resultado->receitas->count();

		return $resultado;
	}
	
	private static function _Executar (Receita $receita, Operadora $operadora, $comparacao)
	{
		$query = Order
			::on('magento::write')
			->select('entity_id','increment_id','activated_user','plan_price')
			->PorOperadora($operadora)
		;

		foreach ($comparacao->regras as $regraComparacao)
		{
			$coluna = self::$colunas[$regraComparacao->configuracoes->coluna];
			
			try
			{
				self::{'Query_' . $coluna['query']['method']}($query, $coluna, $receita, $regraComparacao->configuracoes);
			}
			catch (\throwable $throwable)
			{
				\Log::channel('contestador')->info('Erro de Processamento: ' . $throwable->getMessage());
				return null;
			}
		}

		if (!is_null($resultado = $query->first()))
			return (object) ['pedido' => $resultado, 'comparacao' => $comparacao];
		
		return null;
	}

	public static function ValidateConfiguracao ($comparacao, $index)
	{
		$errors = [];

		if (!in_array($comparacao->tipo, ['exata', 'periodo']))
		{
			$errors[] = 'Tipo de configuração inválida';

			return $errors;
		}

		switch ($comparacao->tipo)
		{
			case 'exata':
				
				if (empty(@$comparacao->configuracoes->nome))
					$errors[] = 'O nome da configuração não pode estar vazio';

				if (!is_numeric(@$comparacao->configuracoes->score))
					$errors[] = @$comparacao->configuracoes->score . ' é um valor inválido para o score';

				if (empty(@$comparacao->configuracoes->coluna))
					$errors[] = 'É necessário especificar um nome para a coluna';

				break;

			case 'periodo':

				if (empty(@$comparacao->configuracoes->nome))
					$errors[] = 'O nome da configuração não pode estar vazio';

				if (!is_numeric(@$comparacao->configuracoes->score))
					$errors[] = @$comparacao->configuracoes->score . ' é um valor inválido para o score';

				if (empty(@$comparacao->configuracoes->coluna))
					$errors[] = 'É necessário especificar um nome para a coluna';

				if (!is_numeric($comparacao->configuracoes->antes))
					$errors[] = 'É necessário especificar um número de dias para antes, mesmo que seja zero';

				if (!is_numeric($comparacao->configuracoes->depois))
					$errors[] = 'É necessário especificar um número de dias para depois, mesmo que seja zero';

				break;
		}

		if (count($errors) <= 0)
			return false;

		return $errors;
	}

	public static function GetPedidoType ()
	{
		return Order::class;
	}

	private static function Query_where (&$query, &$coluna, &$receita, &$configuracoes)
	{
		$field = $coluna['query']['field'];
		$data = $receita->{$coluna['query']['data']};

		foreach ($coluna['parser'] as $parser)
			$data = self::{'Parser_' . $parser}($data);

		return $query->where($coluna['query']['field'], $data);
	}

	private static function Query_where_provisorio (&$query, &$coluna, &$receita, &$configuracoes)
	{
		$field = $coluna['query']['field'];
		$data = $receita->{$coluna['query']['data']};

		foreach ($coluna['parser'] as $parser)
			$data = self::{'Parser_' . $parser}($data);

		return $query
			->IsPortabilidade()
			->where($coluna['query']['field'], $data)
		;
	}

	private static function Query_where_dependente (&$query, &$coluna, &$receita, &$configuracoes)
	{
		$field = $coluna['query']['field'];
		$data = $receita->{$coluna['query']['data']};

		foreach ($coluna['parser'] as $parser)
			$data = self::{'Parser_' . $parser}($data);

		$dependente = OrderDependente::select('order_id');
		
		foreach ($field as $index => $value)
			$dependente->where($value, $data[$index]);
		
		return $query->whereIn('entity_id', $dependente);
	}

	private static function Query_between (&$query, &$coluna, &$receita, &$configuracoes)
	{
		$field = $coluna['query']['field'];
		$data = $receita->{$coluna['query']['data']};

		foreach ($coluna['parser'] as $parser)
			$data = self::{'Parser_' . $parser}($data);

		$left = self::tryToConvertDate($data)->modify('-' . $configuracoes->antes . ' days');
		$right = self::tryToConvertDate($data)->modify('+' . $configuracoes->depois . ' days');

		return $query->whereBetween($field, [
			$left->format('Y-m-d 00:00:00'),
			$right->format('Y-m-d 23:59:59'),
		]);
	}

	private static function Query_between_agendamento (&$query, &$coluna, &$receita, &$configuracoes)
	{
		$data = $receita->{$coluna['query']['data']};

		foreach ($coluna['parser'] as $parser)
			$data = self::{'Parser_' . $parser}($data);

		$left = self::tryToConvertDate($data)->modify('-' . $configuracoes->antes . ' days');
		$right = self::tryToConvertDate($data)->modify('+' . $configuracoes->depois . ' days');

		return $query->whereHas('agenda_portabilidade', function ($query) use ($left, $right) {

			return $query->whereBetween('datahora', [
				$left->format('Y-m-d 00:00:00'),
				$right->format('Y-m-d 23:59:59'),
			]);
		
		});
	}

	private static function Parser_encrypt ($value)
	{
		return Magento::encrypt($value);
	}

	private static function Parser_split_telefone ($value)
	{
		return [substr($value, 0, 2), substr($value, 2)];
	}

	private static function tryToConvertDate ($value)
	{
		foreach (self::$formats as $format)
		{
			if (!$date = \DateTime::createFromFormat($format, $value))
				continue;

			return $date;
		}

		throw new \Exception('Erro tentando processar a data (' . $value . ') usando nenhum dos formados disponíveis: ' . json_encode(self::$formats));
	}
}