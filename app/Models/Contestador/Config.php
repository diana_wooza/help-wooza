<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;
use \App\Models\User;
use \App\Models\Contestador\Log\Config as Log;

class Config extends \App\Models\Config
{
	protected $connection		= 'contestador';
	
	/**
	 * Sobre escreve a funcionalidade para gerar automaticamente LOG em cada edição do model
	 * @method boot
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}

	public static function getListaUsuariosMarcadorParaSelect ()
	{
		return User::porPermissao('contestador.marcacao')->pluck('nome', 'id');
	}
}