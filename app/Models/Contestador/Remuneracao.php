<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Contestador\Ciclo;
use App\Models\Contestador\Grupo;

class Remuneracao extends Model
{
	use SoftDeletes;

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'remuneracoes';
	protected $connection			= 'contestador';
	protected $attributes			= [
		'regra'							=> '{}',
	];
	protected $fillable				= ['competencia_id','regra'];
	protected $hidden				= ['competencia_id','deleted_at'];
	protected $casts				= [
		'regra'							=> 'object',
	];
	protected $appends				= [];
}