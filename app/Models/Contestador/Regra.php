<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Contestador\Log\Regra as Log;
use App\Models\Contestador\Pivots\RegraGrupo;
use App\Models\Contestador\Grupo;

class Regra extends Model
{
	use SoftDeletes;

	const STATUS_INATIVO			= 0;
	const STATUS_ATIVO				= 1;

	const TIPO_MARCACAO				= 1;
	const TIPO_APURACAORECEITA		= 2;
	const TIPO_COMISSIONAMENTO		= 3;

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'regras';
	protected $connection			= 'contestador';
	protected $attributes			= [
		'status'						=> self::STATUS_ATIVO,
		'tipo'							=> null,
		'configuracoes'					=> '{}',
	];
	protected $fillable				= ['status','nome','usuarios','base','configuracoes'];
	protected $hidden				= ['deleted_at'];
	protected $casts				= [
		'status'						=> 'integer',
		'tipo'							=> 'integer',
		'configuracoes'					=> 'object',
	];
	protected $appends				= ['base_string'];

	public static $statuses			= [
		self::STATUS_ATIVO				=> 'Ativo',
		self::STATUS_INATIVO			=> 'Inativo',
	];

	private $comparacoesOrdenadas = null;

	public function ExecutarQuery (\Illuminate\Support\Collection $receitas, \App\Models\Operadora $operadora)
	{
		return $this->configuracoes->query::Executar($receitas, $operadora, $this);
	}

	public function GetComparacoesOrdenada ()
	{
		if (is_null($this->comparacoesOrdenadas))
			$this->MontarComoparacoesOrdenadas();

		return $this->comparacoesOrdenadas;
	}

	public function popularBaseMarcacao ()
	{
		$configuracoes = new \stdClass();

		$configuracoes->query = null;
		$configuracoes->comparacoes = [];

		$this->configuracoes = $configuracoes;

		return $this;
	}

	private function MontarComoparacoesOrdenadas ()
	{
		$exatas = array_filter($this->configuracoes->comparacoes, function ($comparacao) {return $comparacao->tipo == 'exata';});
		$periodicas = array_filter($this->configuracoes->comparacoes, function ($comparacao) {return $comparacao->tipo == 'periodo';});
		
		$listaFinal = [];

		foreach ($exatas as $exata)
		{
			if (!$exata->configuracoes->com_data)
			{
				$listaFinal[] = (object) [
					'score'		=> $exata->configuracoes->score,
					'regras'	=> [$exata],
				];
				
				continue;
			}

			foreach ($periodicas as $periodica)
			{
				$listaFinal[] = (object) [
					'score'		=> $exata->configuracoes->score + $periodica->configuracoes->score,
					'regras'	=> [$exata, $periodica],
				];
			}
		}

		usort($listaFinal, function ($left, $right) {return $right->score <=> $left->score;});

		$this->comparacoesOrdenadas = array_filter($listaFinal, function ($regra) {return $regra->score > 0;});
	}

	/**
	 * Mutators
	 */

	public function getPedidoTypeAttribute ()
	{
		return addslashes($this->configuracoes->query::GetPedidoType());
	}

	public function getBaseStringAttribute ()
	{
		$configuracoes = $this->configuracoes;

		if (!isset($configuracoes->query) || !isset($configuracoes->query) || is_null($configuracoes->query))
			return null;
		
		return $configuracoes->query::$rotulo;
	}

	public function getStatusStringAttribute ()
	{
		if (!isset(self::$statuses[$this->attributes['status']]))
			return 'Desconhecido';

		return self::$statuses[$this->attributes['status']];
	}

	public function getStatusBulletAttribute ()
	{
		$class = 'label-info';

		switch ($this->attributes['status'])
		{
			case self::STATUS_ATIVO:

				$class = 'label-success';
				break;

			case self::STATUS_INATIVO:
			
				$class = 'label-warning';
				break;
		}

		return '<span class="label ' . $class . '">' . $this->status_string . '</span>';
	}

	/**
	 * Custom Scope
	 */

	public function scopeApenasMarcacao ($query)
	{
		return $query->where('tipo', Regra::TIPO_MARCACAO);
	}

	/**
	 * Boot
	 */

	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}
}