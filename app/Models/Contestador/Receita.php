<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;

class Receita extends Model
{
	const TIPO_REMUNERACAO			= 'remuneracao';
	const TIPO_CHURN				= 'churn';

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'receitas';
	protected $connection			= 'contestador';
	protected $attributes			= [
		'dados'							=> '{}',
		'marcacao'						=> '{}',
	];
	protected $fillable				= ['pedido_id','upload_id','arquivo_id','operadora_id','competencia_id','dados'];
	protected $hidden				= ['pedido_id','upload_id','arquivo_id','operadora_id','competencia_id','deleted_at'];
	protected $casts				= [
		'dados'							=> 'object',
		'marcacao'						=> 'object',
		'valor'							=> 'decimal:2',
	];
	protected $appends				= [];

	public static $tipos			= [
		self::TIPO_REMUNERACAO			=> 'Remuneração',
		self::TIPO_CHURN				=> 'Churn',
	];

	public function Marcar (\App\Models\Contestador\Queries\Base\Resultado $resultado)
	{
		$this->marcacao = $resultado->comparacao;
		$this->pedido()->associate($resultado->pedido);

		return $this;
	}

	public function Desmarcar ()
	{
		$this->marcacao = null;
		$this->pedido()->dissociate();

		return $this;
	}

	public function GetExportedData ()
	{
		$this->loadMissing('pedido', 'pedido.pedido', 'pedido.pedido.admin_base', 'pedido.pedido.seller');

		$dados = (array) $this->dados;

		unset($dados['_pedido']);
		unset($dados['_colunas_originais']);

		$dados = array_values($dados);

		return array_merge($dados, [
			@$this->pedido->increment_id,
			!is_null(@$this->pedido->pedido->plan_price) ? \number_format(@$this->pedido->pedido->plan_price, 2, ',', '.') : '',
			@$this->pedido->pedido->seller->username,
			@$this->pedido->pedido->admin_base->nome,
			@$this->pedido->pedido->origem_pedido,
			@$this->pedido->pedido->plan_modality,
			@$this->pedido->pedido->plan_name,
			@$this->marcacao_string,
		]);
	}

	public function GetExportedTitle () : array
	{
		$colunas = (array) $this->dados;

		unset($colunas['_pedido']);
		unset($colunas['_colunas_originais']);

		$colunas = array_keys($colunas);

		foreach ($colunas as &$coluna)
			$coluna = !is_null(@$this->dados->_colunas_originais->{$coluna}) ? $this->dados->_colunas_originais->{$coluna} : $coluna;

		return array_merge($colunas, [
			'PEDIDO',
			'VALOR_PLANO',
			'VENDEDOR',
			'BASE',
			'ORIGEM_PEDIDO',
			'MODALIDADE',
			'NOME DO PLANO',
			'REGRA',
		]);
	}

	/**
	 * Mutators
	 */

	protected function getMarcacaoStringAttribute ()
	{
		if (is_null($this->marcacao))
			return null;

		return implode(' + ', array_map(function ($regra) {
			return $regra->configuracoes->nome;
		}, $this->marcacao->regras));
	}

	protected function getMarcadaAttribute ()
	{
		return !is_null($this->pedido_id);
	}

	protected function getTerminalStringAttribute ()
	{
		if (is_null($this->terminal) || $this->terminal == 'null' || empty($this->terminal))
			return '<i class="text-gray">Não Identificado</i>';
			
		return $this->terminal;
	}

	protected function getProtocoloStringAttribute ()
	{
		if (is_null($this->protocolo) || $this->protocolo == 'null')
			return '<i class="text-gray">Não Identificado</i>';
			
		return $this->protocolo;
	}

	protected function getDocumentoStringAttribute ()
	{
		if (is_null($this->documento) || $this->documento == 'null')
			return '<i class="text-gray">Não Identificado</i>';
			
		return $this->documento;
	}


	protected function getDataStringAttribute ()
	{
		if (is_null($this->data) || $this->data == 'null' || $this->data == '0000-00-00')
			return '<i class="text-gray">Não Identificado</i>';
			
		return date('d/m/Y', strtotime($this->data));
	}

	protected function getPedidoStringAttribute ()
	{
		if (is_null($this->pedido) || $this->pedido == 'null')
			return '<i class="text-gray">nenhum</i>';

		return \mb_strtoupper($this->pedido->increment_id);
	}

	protected function getServicoStringAttribute ()
	{
		if (is_null($this->servico) || $this->servico == 'null' || empty($this->servico))
			return '<i class="text-gray">nenhum</i>';

		return $this->servico;
	}

	protected function getMotivoStringAttribute ()
	{
		if (is_null($this->motivo) || $this->motivo == 'null' || empty($this->motivo))
			return '<i class="text-gray">nenhum</i>';

		return $this->motivo;
	}

	protected function getDetalheStringAttribute ()
	{
		if (is_null($this->detalhe) || $this->detalhe == 'null' || empty($this->detalhe))
			return '<i class="text-gray">nenhum</i>';

		return $this->detalhe;
	}

	protected function getValorFormatadoAttribute ()
	{
		$classe = '';
		$valor = $this->valor;

		switch ($this->tipo)
		{
			case self::TIPO_CHURN:

				$classe = 'text-red';
				$valor *= -1;

				break;

		}

		return '<span class="' . $classe . '">' . number_format($valor, 2, ',', '.') . '</span>';
	}

	protected function getTipoAttribute ()
	{
		if ($this->valor <= 0)
			return self::TIPO_CHURN;

		return self::TIPO_REMUNERACAO;
	}

	protected function getTipoStringAttribute ()
	{
		if (!isset(self::$tipos[$this->tipo]))
			return 'Desconhecido';

		return self::$tipos[$this->tipo];
	}

	protected function getTipoBulletAttribute ()
	{
		$class = 'label-info';

		switch ($this->tipo)
		{
			case self::TIPO_REMUNERACAO:

				$class = 'label-success';
				break;

			case self::TIPO_CHURN:
			
				$class = 'label-warning';
				break;
		}

		return '<span class="label ' . $class . '">' . $this->tipo_string . '</span>';
	}

	protected function getDataAttribute ($valor)
	{
		if (!is_null($valor))
			return $valor;

		return @$this->dados->_data;
	}

	protected function getTerminalAttribute ($valor)
	{
		if (!is_null($valor))
			return $valor;

		return @$this->dados->_terminal;
	}

	protected function getDocumentoAttribute ($valor)
	{
		if (!is_null($valor))
			return $valor;

		return @$this->dados->_documento;
	}

	protected function getProtocoloAttribute ($valor)
	{
		if (!is_null($valor))
			return $valor;

		return @$this->dados->_protocolo;
	}

	protected function getServicoAttribute ($valor)
	{
		if (!is_null($valor))
			return $valor;

		return @$this->dados->_servico;
	}

	protected function getMotivoAttribute ($valor)
	{
		if (!is_null($valor))
			return $valor;

		return @$this->dados->_motivo;
	}

	protected function getDetalheAttribute ($valor)
	{
		if (!is_null($valor))
			return $valor;

		return @$this->dados->_detalhe;
	}

	protected function getValorAttribute ($valor)
	{
		if (!is_null($valor))
			return $valor;

		return @$this->dados->_valor;
	}

	/**
	 * Custom Scopes
	 */

	public function scopePorCompetencia ($query, \App\Models\Contestador\Competencia $competencia)
	{
		return $query->where('competencia_id', $competencia->id);
	}

	public function scopePorArquivo ($query, \App\Models\Contestador\Arquivo $arquivo)
	{
		return $query->where('arquivo_id', $arquivo->id);
	}

	public function scopePorProcesso ($query, \App\Models\Contestador\Processo $processo)
	{
		return $query->where('processo_id', $processo->id);
	}

	public function scopeApenasChurn ($query)
	{
		return $query->where('valor', '<=', 0);
	}

	public function scopeApenasRemuneracao ($query)
	{
		return $query->where('valor', '>', 0);
	}
	
	public function scopeApenasMarcados ($query)
	{
		return $query->whereNotNull('pedido_id');
	}

	public function scopeApenasDesmarcados ($query)
	{
		return $query->whereNull('pedido_id');
	}

	public function scopeComFiltro ($query, \Illuminate\Http\Request $request)
	{
		if ($request->has('tipo') && !empty($request->tipo))
			$query->{$request->tipo == self::TIPO_CHURN ? 'ApenasChurn' : 'ApenasRemuneracao'}();

		if ($request->has('operadora') && !empty($request->operadora))
			$query->whereIn('operadora_id', $request->operadora);

		if ($request->has('arquivo') && !empty($request->arquivo))
			$query->whereIn('arquivo_id', $request->arquivo);

		if ($request->has('competencia') && !empty($request->competencia))
			$query->whereIn('competencia_id', $request->competencia);

		if ($request->has('pedido') && !empty($request->pedido))
		{
			$query->whereHas('pedido', function ($query) use ($request) {

				$query->whereIn('increment_id', preg_split('/([,; \n]+)/', $request->pedido));

			});
		}

		if ($request->has('marcacao') && !empty($request->marcacao))
		{
			switch ($request->marcacao)
			{
				case 'marcados':

					$query->ApenasMarcados();
					break;

				case 'desmarcados':

					$query->ApenasDesmarcados();
					break;
			}
		}

		return $query;
	}

	public function scopeComOrdenacao ($query, \Illuminate\Http\Request $request)
	{
		$sortBy = $request->query('sort_by', 'id');
		$sortOrder = $request->query('sort_order', 'desc');

		return $query->orderBy($sortBy, $sortOrder);
	}

	/**
	 * Relationships
	 */

	public function arquivo ()
	{
		return $this->belongsTo(\App\Models\Contestador\Arquivo::class);
	}

	public function operadora ()
	{
		return $this->belongsTo(\App\Models\Operadora::class);
	}
	
	public function competencia ()
	{
		return $this->belongsTo(\App\Models\Contestador\Competencia::class);
	}

	public function marcacao ()
	{
		return $this->belongsTo(\App\Models\Contestador\Marcacao::class);
	}

	public function pedido ()
	{
		return $this->belongsTo(\App\Models\Contestador\Pedido::class);
	}
}