<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Processo extends Model
{
	use SoftDeletes;

	const TIPO_UPLOAD				= 1;
	const TIPO_MARCACAO				= 2;
	const TIPO_DOWNLOAD				= 3;
	const TIPO_SOMENTE_DOWNLOAD		= 4;

	const ESTR_UPLD_SOBREESCREVER	= 'sobreescrever';
	const ESTR_UPLD_INCLUIR			= 'inclusao';

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'processos';
	protected $connection			= 'contestador';
	protected $attributes			= [
		'configuracao'					=> '{}',
		'resultado'						=> '[]',
	];
	protected $fillable				= ['tipo','inicio','conclusao'];
	protected $hidden				= ['deleted_at'];
	protected $casts				= [
		'configuracao'					=> 'object',
		'resultado'						=> 'object',
	];
	protected $appends				= ['status'];
	protected $with					= [];

	public static $tipos			= [
		self::TIPO_UPLOAD				=> 'Upload',
		self::TIPO_MARCACAO				=> 'Marcação',
		self::TIPO_DOWNLOAD				=> 'Download',
	];

	public static $estrategias_upload		= [
		self::ESTR_UPLD_SOBREESCREVER	=> [
			'long'	=> 'Remover Anteriores e Incluir Novos',
			'short'	=> 'Remover Anteriores',
		],
		self::ESTR_UPLD_INCLUIR		=> [
			'long'	=> 'Apenas Incluir Linhas',
			'short'	=> 'Incluir Linhas',
		],
	];

	private static $expiration = '-1 year';

	public function Dispatch ()
	{
		if (!$this->exists)
			throw new \Exception('Apenas processos salvos podem ser despachados');

		switch ($this->tipo)
		{
			case self::TIPO_UPLOAD:

				\App\Jobs\Contestador\Importador
					::dispatch($this)
					->onConnection('contestador')
					->onQueue('general')
				;

				return $this;

			case self::TIPO_MARCACAO:

				\App\Jobs\Contestador\Marcador
					::dispatch($this)
					->onConnection('contestador')
					->onQueue('general')
				;

				return $this;

			case self::TIPO_DOWNLOAD:

				\App\Jobs\Contestador\Exportador
					::dispatch($this)
					->onConnection('contestador')
					->onQueue('general')
				;

				return $this;
		}

		throw new \Exception('Processo inválido para despachamento');
	}

	public function setIniciado ()
	{
		$this->inicio = \Illuminate\Support\Facades\DB::connection('contestador')->select("SELECT NOW() AS data_atual")[0]->data_atual;

		return $this;
	}

	public function setConcluido ()
	{
		$this->conclusao = \Illuminate\Support\Facades\DB::connection('contestador')->select("SELECT NOW() AS data_atual")[0]->data_atual;

		return $this;
	}

	public function addResultado ($resultado, ...$bind)
	{
		$resultados = $this->resultado;
		$resultados[] = \sprintf($resultado, ...$bind);
		
		$this->resultado = $resultados;

		return $this;
	}

	public function resetResultados ()
	{
		$this->resultado = [];
		return $this;
	}

	public function setConfiguracao ($key, $value)
	{
		$configuracao = $this->configuracao;

		if (is_null($configuracao) || !is_object($configuracao))
			$configuracao = new \stdClass();

		$configuracao->{$key} = $value;

		$this->configuracao = $configuracao;

		return $this;
	}

	public static function GetEstrategiasUploadList ()
	{
		$estrategias = [];

		foreach (self::$estrategias_upload as $key => $value)
			$estrategias[] = (object) ['key' => $key, 'value' => $value['long']];

		return $estrategias;
	}

	/**
	 * Mutators
	 */

	protected function getIsStatusPendenteAttribute ()
	{
		return is_null($this->inicio) && is_null($this->conclusao);
	}

	protected function getIsStatusConcluidoAttribute ()
	{
		return !is_null($this->conclusao);
	}

	protected function getLabelAttribute ()
	{
		$this->loadMissing('arquivo', 'arquivo.operadora');

		$label[] = $this->tipo_string;
		$label[] = '-';
		$label[] = $this->arquivo->operadora->nome;
		$label[] = ':';
		$label[] = $this->arquivo->nome;

		return implode(' ', $label);
	}

	protected function getFullNameAttribute ()
	{
		$this->loadMissing('arquivo', 'arquivo.operadora', 'competencia', 'usuario');

		$name = [];
		$name[] = '<strong>' . @$this->arquivo->nome . '</strong>';
		$name[] = '<small><strong>Competência</strong>: ' . $this->competencia->data_string . '</small>';
		$name[] = '<small><strong>Tipo</strong>: ' . $this->tipo_string . '</small>';
		$name[] = '<small><strong>Requerente</strong>: ' . $this->usuario_string . '</small>';

		return implode('<br />', $name);
	}

	protected function getUsuarioStringAttribute ()
	{
		$this->loadMissing('usuario');

		if (is_null($this->usuario))
			return '<i class="text-gray">Desconhecido</i>';

		return $this->usuario->nome;
	}

	protected function getTipoStringAttribute ()
	{
		if (!isset(self::$tipos[$this->tipo]))
			return 'desconhecido';

		return self::$tipos[$this->tipo];
	}

	protected function getTipoBulletAttribute ()
	{
		return '<span class="label label-default">' . $this->tipo_string . '</span>';
	}

	protected function getStatusAttribute ()
	{
		if ($this->is_status_pendente)
			return 'Pendente';

		if ($this->is_status_concluido)
			return 'Completo';

		return 'Em Andamento';
	}

	protected function getStatusBulletAttribute ()
	{
		if ($this->is_status_pendente)
			return '<span class="label label-danger">' . $this->status . '</label>';

		if ($this->is_status_concluido)
			return '<span class="label label-warning">' . $this->status . '</label>';

		return '<span class="label label-success">' . $this->status . '</label>';
	}

	protected function getConfiguracaoStringAttribute ()
	{
		$configuracoes = [];

		switch ($this->tipo)
		{
			case self::TIPO_UPLOAD:

				$configuracoes[] = '<strong>Estratégia</strong>: ' . self::$estrategias_upload[$this->configuracao->estrategia]['short'];
				$configuracoes[] = '<strong>Arquivo</strong>: ' . (!is_null($this->configuracao->filename) ? $this->configuracao->filename : '<i class="text-gray">desconhecido</i>');
				break;

			case self::TIPO_MARCACAO:

				$regra = \App\Models\Contestador\Regra::find($this->configuracao->regra);

				$configuracoes[] = '<strong>Regra</strong>: ' . $regra->nome;

				break;

			case self::TIPO_DOWNLOAD:

				break;

			default:

				$configuracoes[] = '<i class="text-gray">Configuração Desconhecida</i>';

		}

		return implode("<br />", $configuracoes);
	}

	protected function getInclusaoStringAttribute ()
	{
		return date('d/m/Y H:i:s', strtotime($this->inclusao));
	}

	protected function getInicioStringAttribute ()
	{
		if (is_null($this->inicio))
			return '<i class="text-gray">Não Iniciado</i>';

		return date('d/m/Y H:i:s', strtotime($this->inicio));
	}

	protected function getConclusaoStringAttribute ()
	{
		if (is_null($this->conclusao))
			return '<i class="text-gray">Não Concluído</i>';

		return date('d/m/Y H:i:s', strtotime($this->conclusao));
	}

	protected function getResultadoFormatadoStringAttribute ()
	{
		$resultado = [];

		$resultado[] = '<strong>Data de Início</strong>: ' . $this->inicio_string;
		$resultado[] = '<strong>Data de Conclusão</strong>: ' . $this->conclusao_string;

		if (count($this->resultado) > 0)
			$resultado[] = '<br /><small>' . @implode('<br /><br />', $this->resultado) . '</small>';
		
		return @implode('<br />', $resultado);
	}

	protected function getResultadoStringAttribute ()
	{
		return @implode(' - ', $this->resultado);
	}

	protected function getResultadoStringStrippedAttribute ()
	{
		return \strip_tags(preg_replace('/\<br(\s*)?\/?\>/i', PHP_EOL, \stripslashes(@implode(PHP_EOL . PHP_EOL, $this->resultado))));
	}

	protected function getIsExpiredAttribute ()
	{
		return strtotime($this->inclusao) < strtotime(self::$expiration);
	}

	protected function getFilenamesAttribute ()
	{
		switch ($this->tipo)
		{
			case self::TIPO_UPLOAD:

				return [$this->configuracao->caminho];
				break;

			case self::TIPO_DOWNLOAD:

				return [$this->configuracao->arquivo];
				break;

			case self::TIPO_SOMENTE_DOWNLOAD:

				return [
					$this->configuracao->caminho,
					$this->configuracao->arquivo,
				];
				break;
		}

		return [];
	}

	protected function getFilenameAttribute ()
	{
		switch ($this->tipo)
		{
			case self::TIPO_UPLOAD:

				return $this->configuracao->caminho;
				break;

			case self::TIPO_DOWNLOAD:

				return $this->configuracao->arquivo;
				break;

			case self::TIPO_SOMENTE_DOWNLOAD:

				return $this->configuracao->arquivo;
				break;
		}

		return null;
	}

	protected function getDownloadFilenameAttribute ()
	{
		$this->loadMissing('arquivo','arquivo.operadora', 'competencia');

		return implode(' ', [
			$this->arquivo->operadora->nome,
			$this->competencia->data_string_clean
		]);
	}

	/**
	 * Scopes
	 */

	public function scopeNaoExpirados ($query)
	{
		return $query;
		return $query->where('inclusao', '>=', date('Y-m-d H:i:s', strtotime(self::$expiration)));
	}

	public function scopeApenasUpload ($query)
	{
		return $query->where('tipo', self::TIPO_UPLOAD);
	}

	public function scopeComFiltro ($query, \Illuminate\Http\Request $request)
	{
		if ($request->has('tipo') && !empty($request->tipo))
			$query->where('tipo', $request->tipo);

		if ($request->has('status') && !empty($request->status))
		{
			switch ($request->status)
			{
				case 'pendente':

					$query->ApenasPendente();
					break;

				case 'andamento':

					$query->ApenasEmAndamento();
					break;

				case 'completo':

					$query->ApenasCompleto();
					break;
			}
		}

		return $query;
	}

	public function scopeComOrdenacao ($query, \Illuminate\Http\Request $request)
	{
		$sortBy = $request->query('sort_by', 'id');
		$sortOrder = $request->query('sort_order', 'desc');

		return $query->orderBy($sortBy, $sortOrder);
	}

	public function scopeApenasPendente ($query)
	{
		return $query->whereNull('inicio');
	}

	public function scopeApenasEmAndamento ($query)
	{
		return $query->whereNotNull('inicio')->whereNull('conclusao');
	}

	public function scopeApenasCompleto ($query)
	{
		return $query->whereNotNull('conclusao');
	}

	public function scopeExpirados ($query)
	{
		return $query->where('inclusao', '>', date('Y-m-d H:i:s', strtotime(self::$expiration)));
	}

	/**
	 * Relationship
	 */
	
	public function arquivo ()
	{
		return $this->belongsTo(\App\Models\Contestador\Arquivo::class);
	}

	public function competencia ()
	{
		return $this->belongsTo(\App\Models\Contestador\Competencia::class);
	}

	public function usuario ()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}