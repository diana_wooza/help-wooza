<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;

class Obtencao extends Model
{
	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'obtencoes';
	protected $connection			= 'contestador';
	protected $attributes			= [];
	protected $fillable				= ['competencia_id','resultado'];
	protected $hidden				= ['competencia_id'];
	protected $casts				= [
		'resultado'						=> 'object',
	];
	protected $appends				= [];
}