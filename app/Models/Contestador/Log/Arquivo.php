<?php

namespace App\Models\Contestador\Log;

use Illuminate\Database\Eloquent\Model;

use App\Models\Contestador\Arquivo as Entidade;

class Arquivo extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_arquivo';
	protected $connection		= 'contestador';
	protected $fillable			= ['entidade_id', 'autor_id', 'log'];
	protected $casts			= [
		'log'						=> 'object',
	];

	public static function gerar (string $tipo, Entidade $entidade)
	{
		if (!$entidade->isDirty() || is_null($entidade->id))
			return;

		return self::create([
			'entidade_id'		=> $entidade->id,
			'autor_id'		=> @\Auth::user()->id,
			'log'				=> ['tipo' => $tipo, 'dados' => $entidade->getDirty()]
		]);
	}
}