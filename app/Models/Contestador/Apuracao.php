<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;

class Apuracao extends Model
{
	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'apuracoes';
	protected $connection			= 'contestador';
	protected $attributes			= [];
	protected $fillable				= ['competencia_id','usuario_id'];
	protected $hidden				= ['competencia_id','usuario_id'];
	protected $casts				= [];
	protected $appends				= [];
}