<?php

namespace App\Models\Contestador;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comissao extends Model
{
	use SoftDeletes;

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'comissoes';
	protected $connection			= 'contestador';
	protected $attributes			= [];
	protected $fillable				= ['pedido_id','usuario_id','regra_id','competencia_id','valor'];
	protected $hidden				= ['deleted_at'];
	protected $casts				= [
		'valor'							=> 'decimal:2',
		'layout'						=> 'object',
		'tratamentos'					=> 'object',
	];
	protected $appends				= [];
}