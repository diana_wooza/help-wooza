<?php

namespace App\Models\VarejoLoja;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Importador\Log;
use Auth;

class VarejoLojas {
	const SUCCESS_MESSAGE 		    = 'Importação realizada com sucesso.';
    const FAIL_MESSAGE 			    = 'Não foi possível fazer a importação desta planilha.';
    const LIMITE_EXCEEDED           = 'Limite de 100 linhas na planilha excedido.';
    const ACCESS_DENIED             = 'Acesso restrito a administradores.';
    const LIMITE_IMPORTACAO         = 100;
	
    public static function criarViaUpload($data) : Array
	{
        ini_set("max_execution_time", 120);
        
        if( !Auth::check() ) {
            throw new \Exception( self::ACCESS_DENIED );
        }
        
		try{
            $totalItens 		= count($data);
            $linhasEmBranco     = 0;
            $itemsNaoEditados   = 0;
            
            if( $totalItens > self::LIMITE_IMPORTACAO ) {
                throw new \Exception( self::LIMITE_EXCEEDED );    
            }

            foreach( $data as $item ) {
                if( !$item['cnpj'] && !$item['codigo_agente'] && !$item['parceiro'] ) {
                    $linhasEmBranco++;
                    continue;
                }

                if( !$item['cnpj'] || !$item['codigo_agente'] || !$item['parceiro'] ) {
                    $itemsNaoEditados++;
                    continue;
				}
				
                $cnpj       = trim($item['cnpj']);
                $agente     = trim($item['codigo_agente']);
                $parceiro   = trim($item['parceiro']);

                $sql = "INSERT INTO varejo_lojas (cnpj, codigo_agente, parceiro) VALUES('".$cnpj."', '".$agente."','" . $parceiro . "');";

                DB::connection('magento')->insert($sql);
                DB::connection('magento_qa')->insert($sql);
			}
		}
		catch( \Exception $e ) {
			throw new \Exception( $e );
		}
        
		$result = [
            'total_itens_planilha' 	 => $totalItens,
            'total_itens_importados' => ($totalItens - $linhasEmBranco) - $itemsNaoEditados,
            'total_itens_ignorados'  => $itemsNaoEditados,
        ];
        		
		return $result;
    }
}
  