<?php

namespace App\Models\Indicadores;

use Illuminate\Support\Facades\DB;

class Relatorio
{
	public static function getAnosLogisticaConsolidado ()
	{
		return DB::connection('magento')->table('indicador_logistica_consolidado')
			->select(DB::raw('YEAR(created_at) AS ano'))
			->groupBy(DB::raw(1))
			->orderBy(DB::raw(1), 'desc')
			->get()
			->pluck('ano')
		;
	}

	public static function getLogisticaConsolidado ($mes, $ano, $hora, $operadora, $tipo, $modalidade, $segmento)
	{
		return DB::connection('magento')->table('indicador_logistica_consolidado')
			->select(
				'operadora',
				'status',
				'total',
				'modalidade',
				'tipo',
				'segmento',
				DB::raw("DAY(CONVERT_TZ(`created_at` , 'UTC', 'america/sao_paulo')) dia")
			)
			->where(function ($query) use ($mes, $ano, $hora, $operadora, $tipo, $modalidade, $segmento) {

				$query->where(DB::raw("MONTH(CONVERT_TZ(`created_at` , 'UTC', 'america/sao_paulo'))"), '=', $mes);
				$query->where(DB::raw('YEAR(created_at)'), '=', $ano);
				$query->where(DB::raw("HOUR(CONVERT_TZ(`created_at` , 'UTC', 'america/sao_paulo'))"), '=', $hora);

				if($operadora) $query->where('operadora', '=', $operadora);
				if($tipo) $query->where('tipo', '=', $tipo);
				if($modalidade) $query('modalidade', '=', $modalidade);
				if($segmento) $query->where('segmento', '=', $segmento);

				return $query;
			
			})
			->get()
		;
	}

	public static function updateTableIndicadorBacklog ()
	{
// 		DB::connection('magento')->table('indicador_backlog')->truncate();

// 		DB::connection('magento')->statement(
// 			DB::connection('magento')->raw('INSERT INTO')
// 		);
		
// 		DB::connection('magento')->table('indicador_backlog')->truncate();




// 		$sqls[] = 'TRUNCATE TABLE `indicador_backlog`';

// $sqls[] = 'INSERT INTO `indicador_backlog` (`operadora`, `status`, `modalidade`, `total`, `created_at`)
//             (SELECT plan_operator operadora, sales_order_status.label status, plan_modality modalidade, count(0) total, NOW() created_at
//                 FROM `sales_flat_order`
//                 INNER JOIN sales_order_status ON sales_order_status.status = sales_flat_order.status
//                 WHERE plan_operator != "" AND plan_modality != ""
//                 GROUP BY sales_flat_order.status, operadora, modalidade
//                 ORDER BY plan_operator, status)';
	}
}