<?php

namespace App\Models\Indicadores;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Indicadores\Historico\GraficoMetas as Historico;

## Esta tabela possui pedidos consolidados do magento para os graficos de indicadores
class GraficoMetas extends Model
{
	protected $table	  = 'grafico_metas';
	protected $connection = 'alarmistica';
	public $timestamps	  = false;

	protected $fillable	= ['tribo', 'metrica', 'ano_mes'];

    public static function getDadosGrafico($ano_mes)
    {
		return self::where('ano_mes', '=', $ano_mes)
					->get();

    }

    	/**
	 * boot - cria log automaticamente casa seja alterado ou criada uma api
	 *
	 * @return void
	 */
	public static function boot ()
	{
		parent::boot();

		self::created(function ($model) {Historico::gerar('criar', $model);});
		self::saving(function ($model) {Historico::gerar('editar', $model);});
	}
}