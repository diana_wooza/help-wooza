<?php

namespace App\Models\Indicadores;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

## Esta tabela possui pedidos consolidados do magento para os graficos de indicadores
class GraficoTaxaAtivacao extends Model
{
    protected $table	  = 'grafico_taxa_ativacao';
	protected $connection = 'alarmistica';
	public $timestamps	  = false;

	protected $fillable	= ['plan_operator', 'segmento', 'ano_mes_criacao_pedido', 'total_pedidos', 'tipo_usuario'];
    // protected $casts	= ['activated_date' => 'datetime'];

    public static function getDadosTaxaAtivacao($operadora, $segmento, $dataIncio, $dataFim)
    {

        return self::select(DB::raw("total_pedidos, REPLACE(ano_mes_criacao_pedido, '-00', '') AS ano_mes_criacao_pedido, tipo_usuario"))
                    ->where('segmento', '=', $segmento)
                    ->where('plan_operator', '=', $operadora)
                    ->where('tipo_usuario', '<>', 'desconhecido')
                    ->where('ano_mes_criacao_pedido', '>=', $dataIncio)
                    ->where('ano_mes_criacao_pedido', '<=', $dataFim)
                    ->orderBy('ano_mes_criacao_pedido', 'asc')
                    ->get();

    }
}