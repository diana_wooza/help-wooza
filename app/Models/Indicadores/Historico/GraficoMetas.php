<?php

namespace App\Models\Indicadores\Historico;

use Illuminate\Database\Eloquent\Model;

class GraficoMetas extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_alteracao_grafico_metas';
	protected $connection		= 'alarmistica';
	protected $fillable			= ['tribo', 'ano_mes', 'usuario','informacao', 'data_alteracao'];
	protected $casts			= [
		'data_alteracao'		=> 'datetime',
		'informacao'			=> 'object',
	];

	public static function gerar (string $tipo, \App\Models\Indicadores\GraficoMetas $entidade)
	{
		if (!$entidade->isDirty() || is_null($entidade->id))
			return;

		$titulo = 'daddos_alterados';	
		if($tipo == 'criar')	
			$titulo = 'dados_criados';

			return self::create([
				'usuario'	   => @\Auth::user()->id,
				'tribo'		   => $entidade->id,
				'informacao'   => ['tipo' => $tipo, $titulo => $entidade->getDirty()]
		]);
	}
}