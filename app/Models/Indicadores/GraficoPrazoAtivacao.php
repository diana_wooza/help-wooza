<?php

namespace App\Models\Indicadores;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

## Esta tabela possui pedidos consolidados do magento para os graficos de indicadores
class GraficoPrazoAtivacao extends Model
{
	protected $table	  = 'grafico_prazo_ativacao';
	protected $connection = 'alarmistica';
	public $timestamps	  = false;

	protected $fillable	= ['plan_operator', 'segmento', 'ano_mes_ativacao', 'horas_ativacao', 'soma_horas_ativacao'];
    // protected $casts	= ['activated_date' => 'datetime'];

    public static function getDadosGrafico($operadora, $segmento, $dataIncio, $dataFim)
    {

        return self::select(DB::raw("horas_de_ativacao, REPLACE(ano_mes_ativacao, '-00', '') as ano_mes_ativacao, soma_horas_de_ativacao"))
                    ->where('segmento', '=', $segmento)
                    ->where('plan_operator', '=', $operadora)
                    ->where('ano_mes_ativacao', '>=', $dataIncio)
                    ->where('ano_mes_ativacao', '<=', $dataFim)
                    ->orderBy('ano_mes_ativacao', 'asc')
                    ->get();

    }
           
}