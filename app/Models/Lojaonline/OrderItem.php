<?php

namespace App\Models\Lojaonline;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderItem extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'order_items';
	protected $connection		= 'lojaonline';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];

	/**
	 * Relationship
	 */

	public function resource ()
	{
		return $this->belongsTo(\App\Models\Lojaonline\Resource::class);
	}
}