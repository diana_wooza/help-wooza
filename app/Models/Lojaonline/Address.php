<?php

namespace App\Models\Lojaonline;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Address extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'addresses';
	protected $connection		= 'lojaonline';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];

	/**
	 * Mutators
	 */

	protected function GetPhone1NumerosAttribute ()
	{
		return \preg_replace('/[^0-9]/', '', $this->attributes['phone_1']);
	}

	protected function GetPhone2NumerosAttribute ()
	{
		return \preg_replace('/[^0-9]/', '', $this->attributes['phone_2']);
	}

	protected function GetMobileNumerosAttribute ()
	{
		return \preg_replace('/[^0-9]/', '', $this->attributes['mobile']);
	}

	protected function GetFaxNumerosAttribute ()
	{
		return \preg_replace('/[^0-9]/', '', $this->attributes['fax']);
	}

	protected function GetZipcodeNumerosAttribute ()
	{
		return str_replace('-','', $this->attributes['zipcode']);
	}
}