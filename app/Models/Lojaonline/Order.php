<?php

namespace App\Models\Lojaonline;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Helpers\Tim\ApiOrdem as Tim_ApiOrdem;
use App\Helpers\Tim\Robo as Tim_Robo;

class Order extends Model
{
	const STORE_TIM								= 126627;
	const STORE_CLARO_PROSPECT					= 7482;
	const STORE_CLARO_PRE						= 107313;
	const STORE_CLARO_POS						= 104252;

	const STATUS_AGUARDANDO						= 50;
	const STATUS_APROVADO						= 52;
	const STATUS_APROVADO_CARTAO				= 53;
	const STATUS_AGUARDANDO_DSF					= 54;
	const STATUS_APROVADO_DSF					= 56;
	const STATUS_TRANSPORTE_ENTREGA				= 60;
	const STATUS_CANCELADO_APOS_ENTREGA			= 62;
	const STATUS_NOVA_ENTREGA					= 63;
	const STATUS_ATIVADA						= 65;
	const STATUS_PREPARANDO_ENVIO				= 70;
	const STATUS_ENTREGUE_AGUARDAND_ATIVACAO	= 71;
	const STATUS_CANCELADO						= 90;
	const STATUS_CARTAO_NAO_AUTORIZADO			= 92;
	const STATUS_ANALISE_RISCO					= 94;
	const STATUS_CANCELADO_2					= 95;
	const STATUS_EXTRAVIADO						= 96;
	const STATUS_REPROVADO_DSF					= 98;
	const STATUS_TENTATIVA_NOVO_PAGAMENTO		= 105;
	const STATUS_POSSIVEL_REVERSAO				= 106;

	public $timestamps							= false;
	protected $primaryKey						= 'id';
	protected $table							= 'orders';
	protected $connection						= 'lojaonline';

	protected $fillable							= [];
	protected $hidden							= [];
	protected $attributes						= [];
	protected $casts							= [];
	protected $appends							= ['is_bogo','is_netsales'];
	protected $with								= ['customer','billing_address','plano','aparelho','chip'];

	public static $stores						= [
		self::STORE_TIM								=> 'Loja Online Tim',
		self::STORE_CLARO_PROSPECT					=> 'Loja Claro Prospect',
		self::STORE_CLARO_PRE						=> 'Loja Claro Pré',
		self::STORE_CLARO_POS						=> 'Loja Claro Pós',
	];

	public static $statuses						= [
		self::STATUS_AGUARDANDO						=> 'Aguardando aprovação de pagamento e da análise de risco',
		self::STATUS_APROVADO						=> 'Aprovado – Cartão de crédito',
		self::STATUS_AGUARDANDO_DSF					=> 'Aguardando input no DSF',
		SELF::STATUS_APROVADO_DSF					=> 'Aprovado – DSF',
		SELF::STATUS_TRANSPORTE_ENTREGA				=> 'Pedido em transporte para entrega',
		SELF::STATUS_CANCELADO_APOS_ENTREGA			=> 'Pedido cancelado após tentativa de entrega',
		SELF::STATUS_NOVA_ENTREGA					=> 'Pedido em transporte para nova tentativa de entrega',
		SELF::STATUS_ATIVADA						=> 'Pedido entregue – Linha ativada com sucesso',
		SELF::STATUS_PREPARANDO_ENVIO				=> 'Pedido aprovado em preparação para envio',
		SELF::STATUS_ENTREGUE_AGUARDAND_ATIVACAO	=> 'Pedido entregue – Aguardando ativação da linha',
		SELF::STATUS_CANCELADO						=> 'Pedido cancelado',
		SELF::STATUS_CARTAO_NAO_AUTORIZADO			=> 'Transação não Autorizada Cartão de Crédito',
		SELF::STATUS_ANALISE_RISCO					=> 'Reprovado – Análise de risco',
		SELF::STATUS_CANCELADO_2					=> 'Pedido cancelado',
		SELF::STATUS_EXTRAVIADO						=> 'Pedido extraviado',
		SELF::STATUS_REPROVADO_DSF					=> 'Reprovado – DSF',
		SELF::STATUS_TENTATIVA_NOVO_PAGAMENTO		=> 'Tentativa de novo pagamento',
		SELF::STATUS_POSSIVEL_REVERSAO				=> 'Possível Reversão',
	];

	private static $stateByDDD	= [
		'11'	=> 'SP',
		'12'	=> 'SP',
		'13'	=> 'SP',
		'14'	=> 'SP',
		'15'	=> 'SP',
		'16'	=> 'SP',
		'17'	=> 'SP',
		'18'	=> 'SP',
		'19'	=> 'SP',
		'21'	=> 'RJ',
		'22'	=> 'RJ',
		'24'	=> 'RJ',
		'27'	=> 'ES',
		'28'	=> 'ES',
		'31'	=> 'MG',
		'32'	=> 'MG',
		'33'	=> 'MG',
		'34'	=> 'MG',
		'35'	=> 'MG',
		'37'	=> 'MG',
		'38'	=> 'MG',
		'41'	=> 'PR',
		'42'	=> 'PR',
		'43'	=> 'PR',
		'44'	=> 'PR',
		'45'	=> 'PR',
		'46'	=> 'PR',
		'47'	=> 'SC',
		'48'	=> 'SC',
		'49'	=> 'SC',
		'51'	=> 'RS',
		'53'	=> 'RS',
		'54'	=> 'RS',
		'55'	=> 'RS',
		'61'	=> 'DF',
		'62'	=> 'GO',
		'63'	=> 'TO',
		'64'	=> 'GO',
		'65'	=> 'MT',
		'66'	=> 'MT',
		'67'	=> 'MS',
		'68'	=> 'AC',
		'69'	=> 'RO',
		'71'	=> 'BA',
		'73'	=> 'BA',
		'74'	=> 'BA',
		'75'	=> 'BA',
		'77'	=> 'BA',
		'79'	=> 'SE',
		'81'	=> 'PE',
		'82'	=> 'AL',
		'83'	=> 'PB',
		'84'	=> 'RN',
		'85'	=> 'CE',
		'86'	=> 'PI',
		'87'	=> 'PE',
		'88'	=> 'CE',
		'89'	=> 'PI',
		'91'	=> 'PA',
		'92'	=> 'AM',
		'93'	=> 'PA',
		'94'	=> 'PA',
		'95'	=> 'RR',
		'96'	=> 'AP',
		'97'	=> 'AM',
		'98'	=> 'MA',
		'99'	=> 'MA',
	];

	private static $pdvByState	= [
		'SP'	=> 'SP10_TLTOTI_TL0142_A001',
		'ES'	=> 'RN20_TLTOTI_TL0069_A001',
		'RJ'	=> 'RN20_TLTOTI_TL0071_A001',
		'PR'	=> 'TS40_TLTOTI_TL0069_A001',
		'RS'	=> 'CS50_TLTOTI_TL0110_A001',
		'SC'	=> 'TS40_TLTOTI_TL0072_A001',
		'AC'	=> 'CS60_TLTOTI_TL0080_A001',
		'DF'	=> 'CS60_TLTOTI_TL0081_A001',
		'GO'	=> 'CS60_TLTOTI_TL0082_A001',
		'MT'	=> 'CS60_TLTOTI_TL0083_A001',
		'MS'	=> 'CS60_TLTOTI_TL0084_A001',
		'RO'	=> 'CS60_TLTOTI_TL0085_A001',
		'TO'	=> 'CS60_TLTOTI_TL0086_A001',
		'AM'	=> 'RN90_TLTOTI_TL0080_A001',
		'AP'	=> 'RN90_TLTOTI_TL0081_A001',
		'MA'	=> 'RN90_TLTOTI_TL0082_A001',
		'PA'	=> 'RN90_TLTOTI_TL0083_A001',
		'RR'	=> 'RN90_TLTOTI_TL0084_A001',
		'MG'	=> 'MX30_TLTOTI_TL0075_A001',
		'BA'	=> 'MX30_TLTOTI_TL0076_A001',
		'SE'	=> 'MX30_TLTOTI_TL0077_A001',
		'AL'	=> 'NE80_TLTOTI_TL0080_A001',
		'PB'	=> 'NE80_TLTOTI_TL0081_A001',
		'PE'	=> 'NE80_TLTOTI_TL0082_A001',
		'RN'	=> 'NE80_TLTOTI_TL0083_A001',
		'PI'	=> 'NE80_TLTOTI_TL0085_A001',
		'CE'	=> 'NE80_TLTOTI_TL0086_A001',
	];

	private $tim				= [
		'api_ordem'					=> ['elegibilidade'=>null],
		'robo'						=> ['consultalinha'=>null],
	];

	public function Tim_ConsultaLinha ()
	{
		if ($this->store_id != self::STORE_TIM)
			throw new \Exception('Tentando consultar elegibilidade em um pedido que não é TIM usando a API de Ordem da TIM');

		$data = [
			'Celular'				=> null,
			'CEP'					=> @$this->billing_address->zipcode_numeros,
			'DDD'					=> null,
			'Estado'				=> @$this->billing_address->state,
			'Cidade'				=> @$this->billing_address->city,
			'RuaNumeroComplBairro'	=> implode("\n", [@$this->billing_address->address_1 . ' ' . @$this->billing_address->address_2, @$this->billing_address->address_3, @$this->billing_address->neighborhood]),
			'NomeCompleto'			=> $this->customer->name,
			'TelefoneContato'		=> @$this->billing_address->phone_1_numeros,
			'CPF'					=> $this->customer->cpf_numeros,
			'Email'					=> $this->customer->email,
			'DataNascimento'		=> $this->customer->birthday,
			'NomeCompletoMae'		=> $this->customer->mother_name,
			'Sexo'					=> $this->customer->sex,
		];

		return $this->tim['robo']['consultalinha'] = Tim_Robo::ConsultaLinhaPos($data);
	}

	public function Tim_ConsultaElegibilidade ()
	{
		if ($this->store_id != self::STORE_TIM)
			throw new \Exception('Tentando consultar elegibilidade em um pedido que não é TIM usando a API de Ordem da TIM');

		$pdv	= $this->Tim_GetData_PdvPorTelefone();

		$data	= [
			'NomenclaturaSapPlano'	=> \trim(@explode('+', @$this->plano->name)[0]),
			'PdvCustCode'			=> $pdv->code,
			'PdvStateCode'			=> $pdv->state,
			'Cpf'					=> @$this->customer->cpf_numeros,
			'Nome'					=> \mb_strtoupper(@$this->customer->name),
			'NomeMae'				=> \mb_strtoupper(@$this->customer->mother_name),
			'Msisdn'				=> \substr(@$this->billing_address->phone_1_numeros, 0, 2),
			'DataNascimento'		=> \date('d/m/Y', strtotime(@$this->customer->birthday)),
			'Cep'					=> @$this->zipcode_numeros,
			'PlanSegment'			=> $this->Tim_GetData_ApiOrdemSegmento(),
		];

		return $this->tim['api_ordem']['elegibilidade'] = Tim_ApiOrdem::ConsultaElegibilidade($data);
	}

	private function Tim_GetData_PdvPorTelefone ()
	{
		$ddd = null;

		if (!is_null($this->billing_address->phone_1_numeros))
		{
			$ddd = substr($this->billing_address->phone_1_numeros, 0, 2);
		}
		elseif (!is_null($this->billing_address->phone_2_numeros))
		{
			$ddd = substr($this->billing_address->phone_2_numeros, 0, 2);
		}
		elseif (!is_null($this->billing_address->mobile_numeros))
		{
			$ddd = substr($this->billing_address->mobile_numeros, 0, 2);
		}
		elseif (!is_null($this->billing_address->fax_numeros))
		{
			$ddd = substr($this->billing_address->mobile_numeros, 0, 2);
		}

		$pdv = new \stdClass();

		$pdv->state		= @self::$stateByDDD[$ddd];
		$pdv->code		= @self::$pdvByState[$pdv->state];

		return $pdv;
	}

	private function Tim_GetData_ApiOrdemSegmento ()
	{
		if (\strstr(\mb_strtoupper($this->plano->nome), 'CONTROLE'))
			return Tim_ApiOrdem::ELEGIBILIDADE_SEGMENTO_CONTROLE;

		return Tim_ApiOrdem::ELEGIBILIDADE_SEGMENTO_POS;
	}

	/**
	 * Scopes
	 */

	public function scopePeriodo ($query, $dataDe, $dataAte)
	{

		return $query
			->where('created_at', '>=', \date('Y-m-d 00:00:00', \strtotime($dataDe)))
			->where('created_at', '<=', \date('Y-m-d 23:59:59', \strtotime($dataAte)))
		;
	}

	public function scopeAguardandoInput ($query)
	{
		return $query->where('mock_status', self::STATUS_AGUARDANDO_DSF);
	}

	public function scopeAprovadoCartao ($query)
	{
		return $query->where('mock_status', self::STATUS_APROVADO_CARTAO);
	}

	public function scopeTim ($query)
	{
		return $query->where('store_id', self::STORE_TIM);
	}

	public function scopeClaro ($query)
	{
		return $query->whereIn('store_id', [
			self::STORE_CLARO_PROSPECT,
			self::STORE_CLARO_PRE,
			self::STORE_CLARO_POS
		]);
	}

	public function scopeClaroProspect ($query)
	{
		return $query->where('store_id', self::STORE_CLARO_PROSPECT);
	}

	public function scopeClaroPre ($query)
	{
		return $query->where('store_id', self::STORE_CLARO_PRE);
	}

	public function scopeClaroPos ($query)
	{
		return $query->where('store_id', self::STORE_CLARO_POS);
	}
	
	public function scopeBacklogAparelhos ($query)
	{
		return $query->whereIn('purchase_type', []);
	}

	public function scopeBacklogMigracao ($query)
	{
		return $query->whereIn('purchase_type', ['migracao']);
	}

	public function scopeBacklogNovalinha ($query)
	{
		return $query->whereIn('purchase_type', ['gross_ativacao']);
	}

	public function scopeBacklogPortabilidade ($query)
	{
		return $query->whereIn('purchase_type', ['gross_portabilidade']);
	}

	public function scopeBacklogAparelho ($query)
	{
		return $query->whereIn('purchase_type', ['price_aparelho']);
	}

	public function scopeDoDia ($query)
	{
		return $query->where('status_updated_at', '>=', \date('Y-m-d 00:00:00'));
		;
	}

	/**
	 * Mutators
	 */

	protected function getStoreStringAttribute ()
	{
		if (!isset(self::$stores[$this->store_id]))
			return 'Desconhecida';

		return self::$stores[$this->store_id];
	}

	protected function getZipcodeNumerosAttribute ()
	{
		return preg_replace('/[^0-9]/', '', $this->zipcode);
	}

	protected function getIsBogoAttribute ()
	{
		return isset($this->aparelho) && !is_null($this->aparelho) && isset($this->aparelho->resource) && !is_null($this->aparelho->resource) && \preg_match('/.*((e )?ganhe).*/', $this->aparelho->resource->name, $match);
	}
	
	protected function getIsNetsalesAttribute ()
	{
		return isset($this->aparelho) && !is_null($this->aparelho) && isset($this->aparelho->resource) && !is_null($this->aparelho->resource) && !\preg_match('/.*((e )?ganhe).*/', $this->aparelho->resource->name, $match);
	}

	protected function getUrlLojaAttribute ()
	{
		switch ($this->store_id)
		{
			case self::STORE_TIM:
				
				return 'https://lojaonline.tim.com.br/acpedidos/orders/show?id=' . $this->id;

			case self::STORE_CLARO_PROSPECT:
				
				return 'https://lojaonline.claro.com.br/acpedidos/orders/' . $this->id;

			case self::STORE_CLARO_PRE:
				
				return 'https://lojaonline3.claro.com.br/acpedidos/orders/show?id=' . $this->id;

			case self::STORE_CLARO_POS:
				
				return 'https://lojaonline2.claro.com.br/acpedidos/orders/show?id=' . $this->id;

		}

		return null;
	}

	/**
	 * Relationship
	 */

	 public function customer ()
	{
		return $this->belongsTo(\App\Models\Lojaonline\Person::class, 'person_id');
	}

	public function billing_address ()
	{
		return $this->belongsTo(\App\Models\Lojaonline\Address::class, 'billing_address_id');
	}

	public function shipping_address ()
	{
		return $this->belongsTo(\App\Models\Lojaonline\Address::class, 'shipping_address_id');
	}

	public function plano ()
	{
		return $this->hasOne(\App\Models\Lojaonline\OrderItem::class, 'order_id')->where('kind', 'ServicePlan');
	}

	public function aparelho ()
	{
		return $this->hasOne(\App\Models\Lojaonline\OrderItem::class, 'order_id')->where('kind', 'Variant');
	}

	public function chip ()
	{
		return $this->hasOne(\App\Models\Lojaonline\OrderItem::class, 'order_id')->where('kind', 'Chip');
	}
	
	public function itens ()
	{
		return $this->hasMany(\App\Models\Lojaonline\OrderItem::class, 'order_id');
	}

}