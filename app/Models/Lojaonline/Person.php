<?php

namespace App\Models\Lojaonline;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Person extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'people';
	protected $connection		= 'lojaonline';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];

	protected function GetCpfNumerosAttribute ()
	{
		return str_replace('-','', str_replace('.','', $this->attributes['cpf']));
	}
}