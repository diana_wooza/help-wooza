<?php

namespace App\Models\Lojaonline;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Resource extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'resources';
	protected $connection		= 'lojaonline';

	protected $fillable			= [];
	protected $hidden			= [];
	protected $attributes		= [];
	protected $casts			= [];
	protected $appends			= [];

	/**
	 * Relationship
	 */
}