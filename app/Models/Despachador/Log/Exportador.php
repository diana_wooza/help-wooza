<?php

namespace App\Models\Despachador\Log;

use Illuminate\Database\Eloquent\Model;

class Exportador extends Model
{
	const TIPO_CRIAR			= 1;
	const TIPO_EDITAR			= 2;

	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_exportadores';
	protected $connection		= 'despachador';
	protected $fillable			= ['usuario_id', 'exportador_id', 'log'];
	protected $casts			= [
		'log'						=> 'object',
	];

	public static function gerar (int $tipo = self::TIPO_EDITAR, \App\Models\Despachador\Exportador $entidade)
	{
		if (!$entidade->isDirty() || !$entidade->exists)
			return;

		return self::create([
			'usuario_id'	=> @\Auth::user()->id,
			'exportador_id'	=> $entidade->id,
			'log'			=> ['tipo'=>$tipo, 'data'=>$entidade->getDirty()],
		]);
	}
}