<?php

namespace App\Models\Despachador;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

// use \App\Models\Despachador\Log\Exportador as Log;
// use \App\Models\Despachador\Exportacao;

class Exportacao extends Model
{
	const STATUS_PENDENTE			= 0;
	const STATUS_PROCESSANDO		= 1;
	const STATUS_COMPLETA			= 2;
	const STATUS_FALHA				= 3;

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'exportacoes';
	protected $connection			= 'despachador';
	protected $attributes			= [
		'status'						=> self::STATUS_PENDENTE,
	];
	protected $fillable				= ['status','importador_id','arquivo','dados'];
	protected $casts				= [];

	public static $statuses			= [
		self::STATUS_PENDENTE			=> 'Pendente',
		self::STATUS_PROCESSANDO		=> 'Processando',
		self::STATUS_COMPLETA			=> 'Completa',
		self::STATUS_FALHA				=> 'Falha',
	];

	protected function setDadosAttribute ($value)
	{
		$this->attributes['dados'] = \gzdeflate(\json_encode($value), 9);
	}

	protected function getDadosAttribute ($value)
	{
		return \json_decode(\gzinflate($value));
	}

	/**
	 * Laravel
	 */
	// public static function boot()
	// {
	// 	parent::boot();

	// 	self::created(function ($model) {Log::gerar(Log::TIPO_CRIAR, $model);});
	// 	self::saving(function ($model) {Log::gerar(LOG::TIPO_EDITAR, $model);});
	// }
}