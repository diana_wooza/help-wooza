<?php

namespace App\Models\Despachador;

use \Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

use \App\Models\Despachador\Log\Exportador as Log;
use \App\Models\Despachador\Exportacao;

class Exportador extends Model
{
	const STATUS_INATIVO			= 0;
	const STATUS_ATIVO				= 1;

	use SoftDeletes;

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'exportadores';
	protected $connection			= 'despachador';
	protected $attributes			= [
		'status'						=> self::STATUS_ATIVO,
		'usuarios'						=> '[]',
		'base'							=> null,
		'obtencao'						=> null,
		'validacoes'					=> '[]',
		'tratamentos'					=> '[]',
		'regras'						=> '[]',
		'resultados'					=> '[]',
	];
	protected $fillable				= ['status','nome','usuarios','base','obtencao','validacoes','tratamentos','regras','resultados'];
	protected $casts				= [
		'usuarios'						=> 'object',
		'base'							=> 'object',
		'obtencao'						=> 'object',
		'validacoes'					=> 'object',
		'tratamentos'					=> 'object',
		'regras'						=> 'object',
		'resultados'					=> 'object',
	];

	public static $statuses			= [
		self::STATUS_INATIVO			=> 'Inativo',
		self::STATUS_ATIVO				=> 'Ativo',
	];

	public function GerarExportacao (?Request $request) : ?array
	{
		if (!$this->exists)
			throw new \Exception('Não é possível gerar exportação de exportadores não salvos');

		$exportacoes = [];

		$headers = $this->obtencao->tipo::Obter($this->obtencao->config, $request);
		$collection = $headers->splice(1);

		return $collection->chunk(50000)->map(function ($pieces) use ($headers) {

			return Exportacao::create([
				'importador_id'		=> $this->id,
				'dados'				=> array_merge($headers->all(), $pieces->all()),
			]);

		})->all();
	}

	/**
	 * Laravel
	 */
	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar(Log::TIPO_CRIAR, $model);});
		self::saving(function ($model) {Log::gerar(LOG::TIPO_EDITAR, $model);});
	}
}