<?php

namespace App\Models\Posvenda;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Notificacoes extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'notificacoes_posvenda';
	protected $connection		= 'posvenda';
    public $timestamps = false;
    
    public function salvarNotificacao( $objeto ) {
        $date               = strtotime("+1 day");
 
        $this->nome         = \Auth::user()->nome;
        $this->mensagem     = $objeto->input('mensagem');
        $this->data         = date('Y-m-d h:m:i', time());
        $this->expira_em    = date('Y-m-d h:m:i', $date);
        
        return $this->save();
    }

    public static function notificacoes() {
        return self::where('expira_em', '>', DB::raw('CURRENT_TIMESTAMP()'))->get();
    }
}