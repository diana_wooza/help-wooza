<?php

namespace App\Models\Posvenda;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class SelecaoMotivos extends Model {

	protected $table			= 'selecao_motivos';
	protected $connection		= 'posvenda';
	public $timestamps          = false;
    
    public function motivo1()
	{
		return $this->belongsTo(Motivo1::class);
    }
    
    public function motivo2()
	{
		return $this->belongsTo(Motivo2::class);
    }
    
    public static function salvarMotivos( $motivos, $id ) {
        try{
            DB::beginTransaction();
            $models = \App\Models\Posvenda\SelecaoMotivos::where(['motivo1_id' => $id])->get();

            /**
             * remove os items que não devem mais estar na base
             */
            if( $models ){
                $inseriveis = [];
                foreach( $models as $model ) {
                    if( false == in_array($model->motivo2_id, $motivos) ) {
                        $remove = \App\Models\Posvenda\SelecaoMotivos::find($model->id);
                        $remove->delete();
                    }
                    else{
                        unset($motivos[array_search($model->motivo2_id, $motivos)]);
                    }
                }
            }

            /**
             * Insere o restante
             */
            for( $i = 0; $i < count($motivos); $i++ ) {
                $gravar = new \App\Models\Posvenda\SelecaoMotivos();
                $gravar->motivo1_id = $id;
                $gravar->motivo2_id = $motivos[$i];
                $gravar->save();
            }

            DB::commit();
        }
        catch( \Exception $e ) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }
        
    }
}