<?php

namespace App\Models\Posvenda;

use Illuminate\Database\Eloquent\Model;

class Motivo1 extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'motivo1';
	protected $connection		= 'posvenda';
	public $timestamps = false;
	
}