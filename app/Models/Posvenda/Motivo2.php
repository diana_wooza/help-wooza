<?php

namespace App\Models\Posvenda;

use Illuminate\Database\Eloquent\Model;

class Motivo2 extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'motivo2';
	protected $connection		= 'posvenda';
	public $timestamps = false;
	
}