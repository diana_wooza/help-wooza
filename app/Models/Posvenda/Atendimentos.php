<?php

namespace App\Models\Posvenda;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Atendimentos extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'atendimentos';
	protected $connection		= 'posvenda';
	
	public function proximoProtocolo() {
		return str_pad(intval($this->latest()->first()->protocolo)+1, 10, '0', STR_PAD_LEFT);
	}

	public function salvarAtendimento( $objeto ) {
		try {
			$cpf = $objeto->input('cpf');
			$cpf = preg_replace('/[,.\-\/]/m', '', trim($cpf));
			
			$this->protocolo 			= $this->proximoProtocolo();
			$this->id_origem 			= $objeto->input('origem');
			$this->id_canal_atendimento = $objeto->input('canal');
			$this->id_operadora_produto = $objeto->input('operadora');
			$this->id_atendimento 		= $objeto->input('id_atendimento');
			$this->nome_cliente 		= $objeto->input('nome_cliente');
			$this->id_motivo1 			= $objeto->input('motivo1');
			$this->id_motivo2 			= $objeto->input('motivo2');
			$this->id_acao 				= $objeto->input('acao');
			$this->chamado 				= $objeto->input('chamado');
			$this->id_usuario 			= \Auth::user()->id;
			$this->numero_pedido 		= (true === $objeto->check_cliente) 	? 'Não Cliente' 	: $objeto->input('pedido');
			$this->cpf_cliente 			= (true === $objeto->check_cpf) 		? 'Não Informado' 	: $cpf;
			$this->numero_telefone 		= (true === $objeto->check_telefone) 	? 'Não Informado' 	: $objeto->input('telefone');
			$this->email 				= (true === $objeto->check_email) 		? 'Não Informado' 	: $objeto->input('email');
			$this->save();
		}
		catch( \Throwable $exception ) {
			throw new \Exception($exception->getMessage());
		}
		
		return $this;
	}

	public function listarAtendimentos($filtros) {
		$db_usuario = env("CONNECTION_HUB_DATABASE", 'hub');
		$dados = $this
					->select(DB::raw('date_format(created_at, "%d/%m/%Y %H:%i:%s") as data'), 'atendimentos.id','protocolo', 'id_atendimento', 'origem.origem', 'canal_atendimento.canal_atendimento','operadora_produto.operadora_produto',
					'motivo1.motivo as motivo1', 'motivo2.motivo as motivo2', 'acao.acao', 'numero_pedido', 'nome_cliente', 'cpf_cliente', 'numero_telefone', 'email', 'chamado', $db_usuario.'.usuario.nome')
						->leftJoin('origem', 'id_origem', 'origem.id')
						->leftJoin('canal_atendimento', 'id_canal_atendimento', 'canal_atendimento.id')
						->leftJoin('operadora_produto', 'id_operadora_produto', 'operadora_produto.id')
						->leftJoin('motivo1', 'id_motivo1', 'motivo1.id')
						->leftJoin('motivo2', 'id_motivo2', 'motivo2.id')
						->leftJoin('acao', 'id_acao', 'acao.id')
						->leftJoin($db_usuario.'.usuario', 'id_usuario', $db_usuario.'.usuario.id');

		if (\Auth::user()->checkPermissao('posvenda.claro')) {
			$dados = $dados->where('operadora_produto.operadora_produto', 'Claro');
		}

		if ($filtros->periodo) {
			$periodo = explode(" - ", $filtros->periodo);
			$inicio = explode("/", $periodo[0]);
			$fim = explode("/", $periodo[1]);
			$dados = $dados->whereBetween('created_at', [$inicio[2].'-'.$inicio[1].'-'.$inicio[0], $fim[2].'-'.$fim[1].'-'.$fim[0]]);
		}
		
		if ($filtros->protocolo) {
			$dados = $dados->where('protocolo', $filtros->protocolo);
		}

		if ($filtros->id_atendimento) {
			$dados = $dados->where('id_atendimento', $filtros->id_atendimento);
		}

		if ($filtros->origem) {
			$dados = $dados->where('id_origem', $filtros->origem);
		}

		if ($filtros->canal_atendimento) {
			$dados = $dados->where('id_canal_atendimento', $filtros->canal_atendimento);
		}

		if ($filtros->operadora_produto) {
			$dados = $dados->where('id_operadora_produto', $filtros->operadora_produto);
		}

		if ($filtros->numero_pedido) {
			$dados = $dados->where('numero_pedido', $filtros->numero_pedido);
		}

		if ($filtros->nome_cliente) {
			$dados = $dados->where('nome_cliente', $filtros->nome_cliente);
		}

		if ($filtros->cpf_cliente) {
			$dados = $dados->where('cpf_cliente', $filtros->cpf_cliente);
		}

		if ($filtros->motivo1) {
			$dados = $dados->where('id_motivo1', $filtros->motivo1);
		}

		if ($filtros->motivo2) {
			$dados = $dados->where('id_motivo2', $filtros->motivo2);
		}

		if ($filtros->acao) {
			$dados = $dados->where('id_acao', $filtros->acao);
		}

		if ($filtros->usuario) {
			$dados = $dados->where('nome', 'like', '%'.$filtros->usuario.'%');
		}
		
		return $dados->orderByDesc('created_at');
	}

	/**
	 * Custom Scopes
	 */

	public function scopeSomenteClaro ($query)
	{
		return $query->whereHas('operadora', function ($query) {
			$query->where('operadora_produto', 'Claro');
		});
	}

	public function scopeFiltroListaAtendimento ($query, $filtros)
	{
		if (isset($filtros['periodo']))
		{
			$periodo = explode(" - ", $filtros['periodo']);
			$inicio = explode("/", $periodo[0]);
			$fim = explode("/", $periodo[1]);
			$query->whereBetween('created_at', [$inicio[2].'-'.$inicio[1].'-'.$inicio[0], $fim[2].'-'.$fim[1].'-'.$fim[0]]);
		}
		
		if (isset($filtros['protocolo']))
		{
			$query->where('protocolo', $filtros['protocolo']);
		}

		if (isset($filtros['id_atendimento']))
		{
			$query->where('id_atendimento', $filtros['id_atendimento']);
		}

		if (isset($filtros['origem']))
		{
			$query->where('id_origem', $filtros['origem']);
		}

		if (isset($filtros['canal_atendimento']))
		{
			$query->where('id_canal_atendimento', $filtros['canal_atendimento']);
		}

		if (isset($filtros['operadora_produto']))
		{
			$query->where('id_operadora_produto', $filtros['operadora_produto']);
		}

		if (isset($filtros['numero_pedido']))
		{
			$query->where('numero_pedido', $filtros['numero_pedido']);
		}

		if (isset($filtros['nome_cliente']))
		{
			$query->where('nome_cliente', $filtros['nome_cliente']);
		}

		if (isset($filtros['cpf_cliente']))
		{
			$query->where('cpf_cliente', $filtros['cpf_cliente']);
		}

		if (isset($filtros['motivo1']))
		{
			$query->where('id_motivo1', $filtros['motivo1']);
		}

		if (isset($filtros['motivo2']))
		{
			$query->where('id_motivo2', $filtros['motivo2']);
		}

		if (isset($filtros['acao']))
		{
			$query->where('id_acao', $filtros['acao']);
		}

		if (isset($filtros['usuario']))
		{
			$usuario = $filtros['usuario'];

			$query->whereHas('usuario', function ($query) use ($usuario) {
				$query->where('nome', 'like', '%' . $usuario . '%');
			});
		}

		return $query;
	}

	/**
	 * Relationship
	 */

	public function usuario ()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_usuario')->withTrashed();
	}

	public function acao ()
	{
		return $this->belongsTo(\App\Models\Posvenda\Acao::class, 'id_acao');
	}

	public function motivo1 ()
	{
		return $this->belongsTo(\App\Models\Posvenda\Motivo1::class, 'id_motivo1');
	}

	public function motivo2 ()
	{
		return $this->belongsTo(\App\Models\Posvenda\Motivo2::class, 'id_motivo2');
	}

	public function origem ()
	{
		return $this->belongsTo(\App\Models\Posvenda\Origem::class, 'id_origem');
	}

	public function canal ()
	{
		return $this->belongsTo(\App\Models\Posvenda\CanalAtendimento::class, 'id_canal_atendimento');
	}

	public function operadora ()
	{
		return $this->belongsTo(\App\Models\Posvenda\OperadoraProduto::class, 'id_operadora_produto');
	}
}