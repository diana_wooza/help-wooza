<?php

namespace App\Models\Posvenda;

use Illuminate\Database\Eloquent\Model;

class CanalAtendimento extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'canal_atendimento';
	protected $connection		= 'posvenda';
	public $timestamps = false;
	
}