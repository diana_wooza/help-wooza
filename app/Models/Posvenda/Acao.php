<?php

namespace App\Models\Posvenda;

use Illuminate\Database\Eloquent\Model;

class Acao extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'acao';
	protected $connection		= 'posvenda';
	public $timestamps 			= false;

	const ACAO_CANCELAR_PEDIDO 	= 44;
	const ACAO_STATUS_MOTIVO 	= 33;
	
}