<?php

namespace App\Models\Hub\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UsuarioPerfil extends Pivot
{
    protected $table			= 'usuarios_perfis';
	protected $connection		= 'hub';
}