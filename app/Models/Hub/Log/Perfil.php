<?php

namespace App\Models\Hub\Log;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_perfis';
	protected $connection		= 'hub';
	protected $fillable			= ['entidade_id', 'autor_id', 'log'];
	protected $casts			= [
		'log'						=> 'object',
	];

	public static function gerar (string $tipo, \App\Models\Perfil $entidade)
	{
		if (!$entidade->isDirty() || is_null($entidade->id))
			return;

		return self::create([
			'entidade_id'	=> $entidade->id,
			'autor_id'		=> @\Auth::user()->id,
			'log'			=> ['tipo' => $tipo, 'dados' => $entidade->getDirty()]
		]);
	}
}