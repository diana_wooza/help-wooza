<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use \Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use \App\Models\Perfil;
use \App\Models\Hub\Log\User as Log;
use \App\Helpers\Sms;
use App\Models\Magento\Consumer\AdminUser as MagentoAdminUser;

class User extends Authenticatable
{
	use SoftDeletes;
	
	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'usuarios';
	protected $connection			= 'hub';
	protected $attributes			= [
		'configuracoes'					=> '{}',
		'dados'							=> '{"telefone":null, "email":null}',
	];
	protected $fillable				= ['nome','login','token','configuracoes','dados','email','telefone'];
	protected $hidden				= ['deleted_at','dados'];
	protected $casts				= [
		'configuracoes'					=> 'object',
		'dados'							=> 'object',
	];
	protected $appends				= ['email','email_original','telefone'];

	private $permissoes				= null;

	public function hasPerfil ()
	{
		return $this->perfis->count() > 0;
	}

	public function sendSimpleSms (string $message)
	{
		return Sms::Simples($this->telefone, $message);
	}

	public function checkPermissao (...$permissoes)
	{
		if (is_null($this->permissoes))
		{
			$this->permissoes = [];

			foreach ($this->perfis as $perfil)
				$this->permissoes = array_merge($this->permissoes, $perfil->permissoes);
		}

		foreach ($permissoes as $permissao)
		{
			if (in_array($permissao, $this->permissoes))
				return true;
		}

		return false;
	}

	public function getPerfisIds ()
	{
		return $this->perfis->pluck('id')->all();
	}

	public function getNomesPerfis ()
	{
		$nomes = $this->perfis->pluck('nome')->all();

		if (empty($nomes) || count($nomes) <= 0)
			return '<i class="text-gray">Nenhum perfil cadastrado</i>';

		return implode(', ', $nomes);
	}

	public function isMyPassword ($password)
	{
		return Hash::check($password, $this->password);
	}

	public function GetUsuarioMagento () : ?MagentoAdminUser
	{
		if (!$this->estaVinculadoComMagento() && !$this->vincularAutomaticamenteMagento())
			return null;

		return MagentoAdminUser::find($this->configuracoes->vinculo_magento);
	}

	public function estaVinculadoComMagento () : bool
	{
		return !is_null(@$this->configuracoes->vinculo_magento);
	}

	public function vincularAutomaticamenteMagento ($autosave = false) : bool
	{
		if ($this->estaVinculadoComMagento())
			return true;

		$usuarioMagento = MagentoAdminUser::where('username', $this->login)->first();

		if (is_null($usuarioMagento))
			return false;

		$configuracoes = $this->configuracoes;

		if (is_null($configuracoes))
			$configuracoes = new \stdClass();

		$configuracoes->vinculo_magento = $usuarioMagento->user_id;

		$this->configuracoes = $configuracoes;

		if ($autosave !== false)
			$this->save();

		return true;
	}

	/**
	 * Sobre escreve a funcionalidade para gerar automaticamente LOG em cada edição do model
	 * @method boot
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}

	/**
	 * Mutators
	 */

	protected function setTelefoneAttribute ($value)
	{
		$dados = $this->dados;
		$dados->telefone = $value;
		$this->dados = $dados;
	}

	protected function getTelefoneAttribute ()
	{
		return @$this->dados->telefone;
	}

	protected function getEmailOriginalAttribute ()
	{
		return @$this->dados->email;
	}

	protected function setEmailAttribute ($value)
	{
		if (preg_match('/.*@wooza.com(.br)?/', $value, $match))
			return;

		$dados = $this->dados;
		$dados->email = $value;
		$this->dados = $dados;
	}

	protected function getEmailAttribute ()
	{
		$dados = $this->dados;

		if ((!isset($dados->email) || is_null($dados->email)) && !is_null($this->login))
			return $this->login . '@wooza.com.br';

		return @$dados->email;
	}

	protected function getAvatarAttribute ()
	{
		$avatar = @$this->dados->avatar;

		if (is_null($avatar))
			return 'img/avatar_default.png';

		return $avatar;
	}

	protected function setAvatarAttribute ($value)
	{
		$dados = $this->dados;
		$dados->avatar = $value;
		$this->dados = $dados;
	}

	protected function setPasswordAttribute ($value)
	{
		$this->attributes['password'] = Hash::make($value);
	}

	/**
	 * Scopes
	 */

	public function scopeComOrdenacao ($query, \Illuminate\Http\Request $request)
	{
		$sortBy = $request->query('sort_by', 'id');
		$sortOrder = $request->query('sort_order', 'desc');

		return $query->orderBy($sortBy, $sortOrder);
	}

	public function scopeComFiltro ($query, \Illuminate\Http\Request $request)
	{
		if ($request->has('nome') && !empty($request->nome))
		{
			$nomes = preg_split('/[\s,;|]/', $request->nome);
			$nomes = '"(' . implode('|', array_map(function ($item) {return '.*' . $item . '.*';}, $nomes)) . ')"';
			$query->where(function ($query) use ($nomes) {
				return $query
					->orWhereRaw('nome RLIKE ' . $nomes)
					->orWhereRaw('login RLIKE ' . $nomes)
				;
			});
		}

		if ($request->has('perfil') && !empty($request->perfil))
		{
			$perfis = $request->perfil;
			
			$query->whereHas('perfis', function ($query) use ($perfis) {
				return $query->whereIn('id', $perfis);
			});
		}

		return $query;
	}
	
	public function scopePorPermissao ($query, $permissao)
	{
		return $query->whereHas('perfis', function ($query) use ($permissao) {
			return $query->PorPermissao($permissao);
		});
	}

	/**
	 * Relationships
	 */

	public function perfis ()
	{
		return $this->belongsToMany(Perfil::class, 'usuarios_perfis', 'usuario_id')->using(\App\Models\Hub\Pivots\UsuarioPerfil::class);
	}

	/**
	 * scopeEspecificos - etorna os usuarios especificos (função scope laravel)
	 *
	 * @param  mixed $query - query a ser executado incluindo a parte que já esta no método
	 * @param  array $listaUsuarios - ids dos usuarios
	 *
	 * @return void
	 */
	public function scopeEspecificos($query, array $listaUsuarios)
	{
		return $query->whereNotIn('id', $listaUsuarios);
	}
}
