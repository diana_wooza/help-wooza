<?php

namespace App\Models\Colmeia;

use Illuminate\Database\Eloquent\Model;
use \Picqer\Barcode\BarcodeGeneratorHTML;
use \App\Models\Colmeia\Config;
use \App\Models\Colmeia\Produto;
use \App\Models\Colmeia\Log\Produto as LogProduto;

class Posicao extends Model
{
	const STATUS_VAGA			= 1;
	const STATUS_DISPONIVEL		= 2;
	const STATUS_RESERVADO		= 3;
	const STATUS_DESCARTE		= 4;

	#Dados básicos do Model Eloquent
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'armario_posicao';
	protected $connection		= 'colmeia';
	protected $fillable			= ['armario','status','gaveta','posicao','produto','serial','sku'];

	public static $statuses		= [
		self::STATUS_VAGA			=> 'Vaga',
		self::STATUS_DISPONIVEL		=> 'Disponível',
		self::STATUS_RESERVADO		=> 'Reservado',
		self::STATUS_DESCARTE		=> 'Descarte',
	];

	protected function getCodigoAttribute ()
	{
		return $this->armario . '-' . $this->gaveta . '-' . $this->posicao;
	}

	public function alocar (\App\Models\Colmeia\Produto $produto)
	{
		$this->status		= self::STATUS_DISPONIVEL;
		$this->produto		= $produto->id;
		$this->serial		= $produto->serial;
		$this->sku			= $produto->sku;
		$this->ocorrencia	= 0;
	}

	public function getStatusStringAttribute ()
	{
		if (!isset(self::$statuses[$this->status]))
			return 'Indefinido';

		return self::$statuses[$this->status];
	}

	public function checkDescarteFazLiberacao (bool $gerarOcorrencia = true)
	{
		$limiteDeVoltas = Config::getValor('ocorrencia');

		// Opção de impedir o chip de ficar gerando ocorrências indefinidamente
		// if ($this->ocorrencia < $limiteDeVoltas || !$gerarOcorrencia)
		if ($this->ocorrencia < $limiteDeVoltas)
		{
			LogProduto::volta_colmeia($this->produto);
			$this->cancelarReserva($gerarOcorrencia);
		}
		else
		{
			LogProduto::descarte([$this]);
			$this->descartar();
		}
	}

	public function getProduto ()
	{
		return Produto::find($this->produto);
	}

	public function descartar ()
	{
		$this->status		= self::STATUS_DESCARTE;
		$this->pedido		= null;
	}

	public function liberar ()
	{
		$this->status		= self::STATUS_VAGA;
		$this->produto		= null;
		$this->sku			= null;
		$this->serial		= null;
		$this->pedido		= null;
	}

	public function cancelarReserva (bool $gerarOcorrencia = true)
	{
		$this->status		= self::STATUS_DISPONIVEL;
		$this->pedido		= null;

		if ($gerarOcorrencia !== false)
			$this->ocorrencia	= $this->ocorrencia + 1;
	}

	public function reservarParaPedido ($pedido = null)
	{
		$this->status		= self::STATUS_RESERVADO;
		$this->pedido		= $pedido;
	}

	public function reservarParaPedidoSeguro ($pedido)
	{
		return self
			::where('id', $this->id)
			->where('status', self::STATUS_DISPONIVEL)
			->update([
				'status'		=> self::STATUS_RESERVADO,
				'pedido'		=> $pedido,
			])
		;
	}

	public function getBarcode ()
	{
		return (new BarcodeGeneratorHTML())->getBarcode('081231723897', BarcodeGeneratorHTML::TYPE_CODE_128);
	}

	public static function listaPosicoesPaginadaPaginas ($porPagina, $colmeia, $status, $serial, $sku, $pedido, $sortBy, $sortOrder)
	{
		$total = self::_listaPosicoesPaginada($colmeia, $status, $serial, $sku, $pedido, $sortBy, $sortOrder)->count();

		if (is_null($porPagina))
			return 1;

		return ceil($total / $porPagina);
	}

	public static function listaPosicoesPaginadaItens ($pagina, $porPagina, $colmeia, $status, $serial, $sku, $pedido, $sortBy, $sortOrder)
	{
		$query = self::_listaPosicoesPaginada($colmeia, $status, $serial, $sku, $pedido, $sortBy, $sortOrder);

		if (!is_null($porPagina))
		{
			$query
				->take($porPagina)
				->skip(($pagina - 1) * $porPagina)
			;
		}

		return $query->get();
	}

	private static function _listaPosicoesPaginada ($colmeia, $status, $serial, $sku, $pedido, $sortBy, $sortOrder)
	{
		$query = self
			::where(function ($query) use ($colmeia, $status, $serial, $sku, $pedido) {

				if (!empty($colmeia))
				{
					$colmeia = explode('-', $colmeia);

					if (isset($colmeia[0]) && !empty($colmeia[0]) && $colmeia[0] != '*')
						$query->where('armario', $colmeia[0]);

					if (isset($colmeia[1]) && !empty($colmeia[1]) && $colmeia[1] != '*')
						$query->where('gaveta', \strtoupper($colmeia[1]));

					if (isset($colmeia[2]) && !empty($colmeia[2]) && $colmeia[2] != '*')
						$query->where('posicao', $colmeia[2]);
				}

				if (!empty($status))
					$query->whereIn('status', $status);

				if (!empty($serial))
					$query->where('serial', 'like', '%' . $serial . '%');

				if (!empty($sku))
					$query->where('sku', 'like', '%' . $sku . '%');

				if (!empty($pedido))
					$query->where('pedido', $pedido);

				return $query;
			})
		;

		if ($sortBy == 'colmeia')
		{
			return $query
				->orderBy('armario', $sortOrder)
				->orderByRaw('SUBSTRING(CONCAT(\'00000\', gaveta), -4) ' . $sortOrder)
				->orderBy('posicao', $sortOrder)
			;
		}

		return $query->orderBy($sortBy, $sortOrder);
	}

	public static function byPedidos (array $pedidos)
	{
		return self
			::whereIn('pedido', $pedidos)
			->get()
		;
	}

	public static function getPedidosPosicoesReservadas ()
	{
		return self
			::whereNotNull('pedido')
			->where('status', self::STATUS_RESERVADO)
			->get()
		;
	}

	/**
	 * Scopes
	 */

	public function scopeDisponivelParaAlocacao ($query)
	{
		return $query->where('status', self::STATUS_DISPONIVEL);
	}

	/**
	 * relationship
	 */

	public function jundsoft ()
	{
		return $this->hasManyThrough(
			\App\Models\Colmeia\Integracao\Jundsoft::class,
			\App\Models\Colmeia\Produto::class,
			'id',
			'id',
			'produto',
			'integracao_jund_id'
		);
	}

	public function entidade_produto ()
	{
		return $this->belongsTo(\App\Models\Colmeia\Produto::class, 'produto');
	}
}