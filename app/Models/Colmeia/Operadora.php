<?php

namespace App\Models\Colmeia;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Colmeia\Operadora;

class Operadora extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'operadora';
	protected $connection		= 'colmeia';

	public static function listaParaSelect ()
	{
		return self::get()->pluck('nome', 'id');
	}

	public static function getFromSku (?string $sku)
	{
		return self::where('codigo', \strtolower(\substr($sku, -1)))->first();
	}
}