<?php

namespace App\Models\Colmeia\Legacy;

use Illuminate\Database\Eloquent\Model;

use App\Models\Colmeia\Nfe as NovaNfe;
use App\Models\Colmeia\Produto as NovoProduto;
use App\Models\Colmeia\Integracao\Jundsoft;

class Nfe extends Model
{
	public $incrementing		= false;
	public $timestamps			= false;
	protected $primaryKey		= 'Codigo';
	protected $table			= 'Nfe';
	protected $connection		= 'colmeia_legacy';
	protected $fillable			= [];
	protected $casts			= [
		'Codigo'					=> 'integer',
		'ide_nNF'					=> 'integer',
	];
	protected $hidden			= ['Num_Pedido','Cod_Natureza_Nfe','ide_cUF','ide_cNF','ide_natOp','ide_indPag','ide_mod','ide_serie','ide_dEmi','ide_dhSaiEnt','ide_tpNF','ide_idDest','ide_cMunFG','ide_NFref_refNfe','ide_tpImp','ide_tpEmis','ide_cDV','ide_tpAmb','ide_finNFe','ide_indFinal','ide_indPres','ide_procEmi','ide_verProc','emit_CNPJ','emit_enderEmit_xLgr','emit_enderEmit_nro','emit_enderEmit_xCpl','emit_enderEmit_xBairro','emit_enderEmit_cMun','emit_enderEmit_xMun','emit_enderEmit_UF','emit_enderEmit_CEP','emit_enderEmit_cPais','emit_enderEmit_xPais','emit_enderEmit_fone','emit_IE','emit_IM','emit_CNAE','emit_IEST','emit_CRT','dest_xNome','dest_enderDest_xLgr','dest_enderDest_nro','dest_enderDest_xCpl','dest_enderDest_xBairro','dest_enderDest_cMun','dest_enderDest_xMun','dest_enderDest_UF','dest_enderDest_CEP','dest_enderDest_cPais','dest_enderDest_xPais','dest_enderDest_fone','dest_indIEDest','dest_IE','dest_email','total_ICMSTot_vBC','total_ICMSTot_vICMS','total_ICMSTot_vICMSDeson','total_ICMSTot_vFCPUFDest','total_ICMSTot_vICMSUFRemet','total_ICMSTot_vBCST','total_ICMSTot_vST','total_ICMSTot_vProd','total_ICMSTot_vFrete','total_ICMSTot_vSeg','total_ICMSTot_vDesc','total_ICMSTot_vII','total_ICMSTot_vIPI','total_ICMSTot_vPIS','total_ICMSTot_vCOFINS','total_ICMSTot_vOutro','total_ICMSTot_vNF','total_ICMSTot_vTotTrib','total_ICMSTot_vICMSUFDest','transp_modFrete','transp_transporta_CNPJ','transp_transporta_xNome','transp_transporta_IE','transp_transporta_xEnder','transp_transporta_xMun','transp_transporta_UF','transp_vol_qvol','transp_vol_pesoL','transp_vol_pesoB','infAdic_infCpl','infAdic_infAdFisco','transp_vol_esp','total_ICMSTot_vFCP','total_ICMSTot_vFCPST','total_ICMSTot_vFCPSTRet','total_ICMSTot_vIPIDevol','transp_vol_lacres'];

	public function RemigrarInfo ()
	{
		$this->loadMissing('detalhes');
		$this->loadMissing('detalhes.produtos');

		$novaNfe = NovaNfe::find($this->ide_nNF);
		$novaNfe->nota = $this->getJsonNota();
		$novaNfe->save();

		$this->detalhes->each(function ($detalhe) use ($novaNfe) {

			foreach ($novaNfe->detalhes as $novoIndex => $novoDetalhe)
			{
				if ($novoDetalhe->{'@attributes'}->oldId != $detalhe->Codigo || is_null($detalhe->det_prod_cEAN))
					continue;

				$jundSoft = Jundsoft::withTrashed()->firstOrCreate(['ean' => $detalhe->det_prod_cEAN]);

				NovoProduto::whereIn('serial', $detalhe->produtos->pluck('ID_CD'))->update([
					'nfe'					=> $novaNfe->id,
					'nfe_detalhe'			=> $novoIndex,
					'integracao_jund_id'	=> $jundSoft->id,
				]);
			}

		});
		
		return true;
	}

	public function getJsonNota ()
	{
		$this->loadMissing('detalhes');

		return [
			'@attributes'	=> ['versao' => null, 'oldId' => intVal($this->Codigo)],
			'NFe'			=> [
				'infNFe'		=> [
					'@attributes'	=> ['versao' => null, 'Id' => 'NFe' . $this->Id_Nfe],
					'ide'			=> [
						'cUF'			=> $this->ide_cUF,
						'cNF'			=> $this->ide_cNF,
						'natOp'			=> $this->ide_natOp,
						'mod'			=> $this->ide_mod,
						'serie'			=> $this->ide_serie,
						'nNF'			=> $this->ide_nNF,
						'dhEmi'			=> date('Y-m-d\TH:m:s-03:00', strtotime($this->ide_dhEmi)),
						'tpNF'			=> $this->ide_tpNF,
						'idDest'			=> $this->ide_idDest,
						'cMunFG'			=> $this->ide_cMunFG,
						'tpImp'			=> $this->ide_tpImp,
						'tpEmis'			=> $this->ide_tpEmis,
						'cDV'			=> $this->ide_cDV,
						'tpAmb'			=> $this->ide_tpAmb,
						'finNFe'			=> $this->ide_finNFe,
						'indFinal'			=> $this->ide_indFinal,
						'indPres'			=> $this->ide_indPres,
						'procEmi'			=> $this->ide_procEmi,
						'verProc'			=> $this->ide_verProc,
					],
					'emit'			=> [
						'CNPJ'			=> $this->emit_CNPJ,
						'xNome'			=> $this->emit_xNome,
						'xFant'			=> $this->emit_xFant,
						'enderEmit'		=> [
							'xLgr'			=> $this->emit_enderEmit_xLgr,
							'nro'			=> $this->emit_enderEmit_nro,
							'xBairro'		=> $this->emit_enderEmit_xBairro,
							'cMun'			=> $this->emit_enderEmit_cMun,
							'xMun'			=> $this->emit_enderEmit_xMun,
							'UF'			=> $this->emit_enderEmit_UF,
							'CEP'			=> $this->emit_enderEmit_CEP,
							'xPais'			=> $this->emit_enderEmit_xPais,
							'fone'			=> $this->emit_enderEmit_fone,
						],
						'IE'			=> $this->emit_IE,
						'CRT'			=> $this->emit_CRT,
					],
					'dest'			=> [
						'CNPJ'			=> $this->dest_CNPJ,
						'xNome'			=> $this->dest_xNome,
						'enderDest'		=> [
							'xLgr'			=> $this->dest_enderDest_xLgr,
							'nro'			=> $this->dest_enderDest_nro,
							'xCpl'			=> $this->dest_enderDest_xCpl,
							'xBairro'		=> $this->dest_enderDest_xBairro,
							'cMun'			=> $this->dest_enderDest_cMun,
							'xMun'			=> $this->dest_enderDest_xMun,
							'UF'			=> $this->dest_enderDest_UF,
							'CEP'			=> $this->dest_enderDest_CEP,
							'cPais'			=> $this->dest_enderDest_cPais,
							'xPais'			=> $this->dest_enderDest_xPais,
						],
						'indIEDest'		=> $this->dest_indIEDest,
						'IE'			=> $this->dest_IE,
					],
					'det'			=> $this->detalhes->map(function($detalhe, $index) {
						return [
							'@attributes'		=> ['nItem' => $index, 'oldId' => $detalhe->Codigo],
							'prod'				=> [
								'cProd'				=> $detalhe->det_prod_cProd,
								'cEAN'				=> $detalhe->det_prod_cEAN,
								'xProd'				=> $detalhe->det_prod_xProd,
								'NCM'				=> $detalhe->det_prod_NCM,
								'CEST'				=> $detalhe->det_prod_CEST,
								'CFOP'				=> $detalhe->det_prod_CFOP,
								'uCom'				=> $detalhe->det_prod_uCom,
								'qCom'				=> $detalhe->det_prod_qCom,
								'vUnCom'			=> $detalhe->det_prod_vUnCom,
								'vProd'				=> $detalhe->det_prod_vProd,
								'cEANTrib'			=> $detalhe->det_prod_cEANTrib,
								'uTrib'				=> $detalhe->det_prod_uTrib,
								'qTrib'				=> $detalhe->det_prod_qTrib,
								'vUnTrib'			=> $detalhe->det_prod_vUnTrib,
								'indTot'			=> $detalhe->det_prod_indTot,
								'xPed'				=> $detalhe->det_prod_xPed,
							],
							'imposto'			=> [
								'vTotTrib'			=> $detalhe->imposto_vTotTrib,
								'ICMS'				=> [
									'ICMS60'			=> [
										'orig'				=> $detalhe->imposto_ICMS_ICMS60_orig,
										'CST'				=> $detalhe->imposto_ICMS_ICMS60_CST,
										'vBCSTRet'			=> $detalhe->imposto_ICMS_ICMS60_vBCSTRet,
										'pST'				=> $detalhe->imposto_ICMS_ICMS60_pST,
										'vICMSSubstituto'	=> null,
										'vICMSSTRet'		=> $detalhe->imposto_ICMS_ICMS60_vICMSSTRet,
									],
								],
								'IPI'				=> [
									'cEnq'				=> $detalhe->imposto_IPI_cEnq,
									'IPINT'				=> ['CST' => $detalhe->imposto_IPI_IPINT_CST],
								],
								'PIS'				=> [
									'PISOutr'			=> [
										'CST'				=> $detalhe->imposto_PIS_PISAliq_CST,
										'vBC'				=> $detalhe->imposto_PIS_PISAliq_vBC,
										'pPIS'				=> $detalhe->imposto_PIS_PISAliq_pPIS,
										'vPIS'				=> $detalhe->imposto_PIS_PISAliq_vPIS,
									],
								],
								'COFINS'			=> [
									'COFINSOutr'		=> [
										'CST'				=> $detalhe->imposto_COFINS_COFINSAliq_CST,
										'vBC'				=> $detalhe->imposto_COFINS_COFINSAliq_vBC,
										'pCOFINS'			=> $detalhe->imposto_COFINS_COFINSAliq_pCOFINS,
										'vCOFINS'			=> $detalhe->imposto_COFINS_COFINSAliq_vCOFINS,
									],
								],
							],
							'infAdProd'			=> $detalhe->infAdProd,
							'produto'			=> null,
						];
					}),
					'total'			=> [
						'ICMSTot'		=> [
							'vBC'			=> $this->total_ICMSTot_vBC,
							'vICMS'			=> $this->total_ICMSTot_vICMS,
							'vICMSDeson'	=> $this->total_ICMSTot_vICMSDeson,
							'vFCP'			=> $this->total_ICMSTot_vFCP,
							'vBCST'			=> $this->total_ICMSTot_vBCST,
							'vST'			=> $this->total_ICMSTot_vST,
							'vFCPST'		=> $this->total_ICMSTot_vFCPST,
							'vFCPSTRet'		=> $this->total_ICMSTot_vFCPSTRet,
							'vProd'			=> $this->total_ICMSTot_vProd,
							'vFrete'		=> $this->total_ICMSTot_vFrete,
							'vSeg'			=> $this->total_ICMSTot_vSeg,
							'vDesc'			=> $this->total_ICMSTot_vDesc,
							'vII'			=> $this->total_ICMSTot_vII,
							'vIPI'			=> $this->total_ICMSTot_vIPI,
							'vIPIDevol'		=> $this->total_ICMSTot_vIPIDevol,
							'vPIS'			=> $this->total_ICMSTot_vPIS,
							'vCOFINS'		=> $this->total_ICMSTot_vCOFINS,
							'vOutro'		=> $this->total_ICMSTot_vOutro,
							'vNF'			=> $this->total_ICMSTot_vNF,
							'vTotTrib'		=> $this->total_ICMSTot_vTotTrib,
						],
					],
					'transp'		=> ['modFrete' => $this->transp_modFrete],
					'pag'			=> ['detPag' => ['tPag' => null, 'vPag' => null]],
					'infAdic'		=> ['infCpl' => $this->infAdic_infCpl],
				],
				'Signature'		=> null,
			],
			'protNFe'		=> null,
		];
	}

	/**
	 * Custom Scopes
	 */
	
	public function scopeApenasEntrada ($query)
	{
		return $query->where('Tipo_Nfe', 'e');
	}

	public function scopeValida ($query)
	{
		return $query->whereNotNull('ide_nNF');
	}

	/**
	 * Relationship
	 */

	public function detalhes ()
	{
		return $this->hasMany(\App\Models\Colmeia\Legacy\Detalhe::class, 'Cod_Nfe');
	}

	public function current ()
	{
		return $this->belongsTo(\App\Models\Colmeia\Nfe::class, 'id', 'ide_nNF');
	}

	public function produtos ()
	{
		return $this->hasManyThrough(
			\App\Models\Colmeia\Legacy\Produto::class,
			\App\Models\Colmeia\Legacy\Detalhe::class,
			'Cod_Nfe',
			'Cod_Nfe_Detalhe',
			'Codigo',
			'Codigo'
		);
	}
}