<?php

namespace App\Models\Colmeia\Legacy;

use Illuminate\Database\Eloquent\Model;

class Detalhe extends Model
{
	public $incrementing		= false;
	public $timestamps			= false;
	protected $primaryKey		= 'Codigo';
	protected $table			= 'Nfe_Detalhe';
	protected $connection		= 'colmeia_legacy';
	protected $fillable			= [];
	protected $casts			= [
		'Codigo'					=> 'integer',
		'Cod_Nfe'					=> 'integer',
		'det_prod_qCom'				=> 'integer',
		'det_prod_vUnCom'			=> 'float',
		'det_prod_vProd'			=> 'float',
	];
	protected $hidden			= ['det_prod_cProd','det_prod_NCM','det_prod_CFOP','det_prod_uCom','det_prod_CEST','det_prod_cEANTrib','det_prod_uTrib','det_prod_qTrib','det_prod_vUnTrib','det_prod_vDesc','det_prod_indTot','det_prod_xPed','imposto_vTotTrib','imposto_ICMS_ICMS60_orig','imposto_ICMS_ICMS60_CST','imposto_ICMS_ICMS60_vBCSTRet','imposto_ICMS_ICMS60_vICMSSTRet','imposto_IPI_cEnq','imposto_IPI_IPINT_CST','imposto_PIS_PISAliq_CST','imposto_PIS_PISAliq_vBC','imposto_PIS_PISAliq_pPIS','imposto_PIS_PISAliq_VPIS','imposto_COFINS_COFINSAliq_CST','imposto_COFINS_COFINSAliq_vBC','imposto_COFINS_COFINSAliq_pCOFINS','imposto_COFINS_COFINSAliq_vCOFINS','infAdProd','det_prod_nItemPed','imposto_ICMS_ICMS60_pST'];

	/**
	 * Relationship
	 */

	public function produtos ()
	{
		return $this->hasMany(\App\Models\Colmeia\Legacy\Produto::class, 'Cod_Nfe_Detalhe')->Valido();
	}
}