<?php

namespace App\Models\Colmeia\Legacy;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
	public $incrementing		= false;
	public $timestamps			= false;
	protected $primaryKey		= 'Codigo';
	protected $table			= 'Estoque';
	protected $connection		= 'colmeia_legacy';
	protected $fillable			= [];
	protected $casts			= [
		'Codigo'					=> 'integer',
		'Cod_Nfe_Detalhe'			=> 'integer',
		'Cod_Status_Estoque'		=> 'integer',
		'Cod_Nota_Saida'			=> 'integer',
	];
	protected $hidden			= ['Cod_Nota_Saida'];

	/**
	 * Custom Scope
	 */
	
	public function scopeValido ($query)
	{
		return $query
			->whereNotNull('ID_CD')
			->whereNotNull('SKU')
		;
	}

	/**
	 * Relationships
	 */

	public function current ()
	{
		return $this->belongsTo(\App\Models\Colmeia\Produto::class, 'ID_CD', 'serial');
	}
}