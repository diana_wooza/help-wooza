<?php

namespace App\Models\Colmeia;

use Illuminate\Database\Eloquent\Model;
use \App\Models\User;
use \App\Models\Perfil;
use \App\Models\Colmeia\Log\Config as Log;

class Config extends \App\Models\Config
{
	protected $connection		= 'colmeia';
	
	public static function getListaUsuariosEmailCobertura ()
	{
		return User
			::whereIn('id', self::getValor('email_cobertura'))
			->get()
		;
	}

	public static function getListaUsuariosColmeiaParaSelect ()
	{
		return User::porPermissao('colmeia')->pluck('nome', 'id');
	}

	/**
	 * Sobre escreve a funcionalidade para gerar automaticamente LOG em cada edição do model
	 * @method boot
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}
}