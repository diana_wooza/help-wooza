<?php

namespace App\Models\Colmeia\Log\Integracao;

use Illuminate\Database\Eloquent\Model;

class Jundsoft extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_integracao_jundsoft';
	protected $connection		= 'colmeia';
	protected $fillable			= ['usuario_id', 'entidade_id','log'];
	protected $casts			= [
		'data'						=> 'datetime',
		'log'						=> 'object',
	];

	public static function gerar (string $tipo, \App\Models\Colmeia\Integracao\Jundsoft $entidade)
	{
		if (!$entidade->isDirty() || is_null($entidade->id))
			return;

		return self::create([
			'usuario_id'	=> @\Auth::user()->id,
			'entidade_id'	=> $entidade->id,
			'log'			=> ['tipo' => $tipo, 'dados' => $entidade->getDirty()]
		]);
	}
}