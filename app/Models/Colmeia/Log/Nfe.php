<?php

namespace App\Models\Colmeia\Log;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class Nfe extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_nfe';
	protected $connection		= 'colmeia';
	protected $fillable			= ['id', 'usuario'];

	// protected function getDataAttribute ()
	// {
	// 	if (is_null($this->attributes['data']))
	// 		return $this->attributes['data'];

	// 	return Carbon::parse($this->attributes['data'], 'UTC')->timezone('America/Sao_Paulo');
	// }

	public static function gerar (\App\Models\Colmeia\Nfe $entidade)
	{
		return self::create([
			'id'		=> $entidade->id,
			'usuario'	=>\Auth::user()->id,
		]);
	}

	public static function detalhe (\App\Models\Colmeia\Nfe $nota, int $detalhe, $produto)
	{
		// return Db::connection('colmeia')->table('log_nfe_detalhe')->insert([
		// 	'id'		=> $nota->id,
		// 	'detalhe'	=> $detalhe,
		// 	'usuario'	=> @\Auth::user()->id,
		// 	'produto'	=> \json_encode($produto),
		// ]);
	}
}