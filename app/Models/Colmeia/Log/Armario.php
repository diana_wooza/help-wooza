<?php

namespace App\Models\Colmeia\Log;

use Illuminate\Database\Eloquent\Model;

class Armario extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_armario';
	protected $connection		= 'colmeia';
	protected $fillable			= ['usuario', 'armario', 'informacao'];
	protected $casts			= [
		'informacao'				=> 'object',
	];

	public static function gerar (string $tipo, \App\Models\Colmeia\Armario $entidade)
	{
		if (!$entidade->isDirty() || is_null($entidade->id))
			return;

		return self::create([
			'usuario'		=> @\Auth::user()->id,
			'armario'		=> $entidade->id,
			'informacao'	=> ['tipo' => $tipo, 'dados' => $entidade->getDirty()]
		]);
	}
}