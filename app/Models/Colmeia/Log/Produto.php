<?php

namespace App\Models\Colmeia\Log;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class Produto extends Model
{
	const TIPO_ENTRADA				= 1;
	const TIPO_PRE_ALOCACAO			= 2;
	const TIPO_ALOCACAO				= 3;
	const TIPO_SELECAO_PEDIDO		= 4;
	const TIPO_VOLTA_COLMEIA		= 5;
	const TIPO_SAIDA				= 6;
	const TIPO_DEVOLUCAO			= 7;
	const TIPO_DESCARTE				= 8;
	const TIPO_DESALOCAR			= 9;
	const TIPO_ENTRADA_LOTE			= 11;
	
	const TIPO_INVENTARIO_DESCARTE	= 10;

	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'log_produto';
	protected $connection			= 'colmeia';
	protected $fillable				= ['data', 'tipo', 'usuario', 'produto', 'informacao'];
	protected $casts				= [
		'informacao'					=> 'object',
	];

	public static $tipos			= [
		self::TIPO_ENTRADA				=> 'Entrada',
		self::TIPO_PRE_ALOCACAO			=> 'Pré Alocação',
		self::TIPO_ALOCACAO				=> 'Alocação',
		self::TIPO_SELECAO_PEDIDO		=> 'Selecionado para pedido',
		self::TIPO_VOLTA_COLMEIA		=> 'Voltou para posição',
		self::TIPO_SAIDA				=> 'Saída',
		self::TIPO_DEVOLUCAO			=> 'Devolução',
		self::TIPO_DESCARTE				=> 'Descarte',
		self::TIPO_DESALOCAR			=> 'Desalocar',
		self::TIPO_INVENTARIO_DESCARTE	=> 'Descarte por inventário',
		self::TIPO_ENTRADA_LOTE			=> 'Entrada em Lote',
	];

	protected function tipoStringAttribute ()
	{
		if (!isset(self::$tipos[$this->tipo]))
			return 'desconhecido';

		return self::$tipos[$this->tipo];
	}

	// protected function getDataAttribute ()
	// {
	// 	if (is_null($this->attributes['data']))
	// 		return $this->attributes['data'];

	// 	return Carbon::parse($this->attributes['data'], 'UTC')->timezone('America/Sao_Paulo');
	// }

	public static function getEstoqueSaida ($dataInicio = null, $dataFim = null)
	{
		return self::_getEstoque(self::TIPO_SAIDA, $dataInicio, $dataFim);
	}

	public static function getEstoqueEntrada ($dataInicio = null, $dataFim = null)
	{
		return self::_getEstoque(self::TIPO_ENTRADA, $dataInicio, $dataFim);
	}

	private static function _getEstoque ($status, $dataInicio, $dataFim)
	{
		return self
			::select(
				'produto.sku',
				DB::raw('COUNT(1) as total')
			)
			->join('produto', 'produto.id', '=', 'log_produto.produto')
			->where('log_produto.tipo', $status)
			->whereNotNull('sku')
			->where(function ($query) use ($dataInicio, $dataFim) {

				if (!is_null($dataInicio))
					$query->where('log_produto.data', '>=', $dataInicio);

				if (!is_null($dataFim))
					$query->where('log_produto.data', '<=', $dataFim);

				return $query;
			
			})
			->groupBy('produto.sku')
			->get()
			->pluck('total', 'sku')
		;
	}

	public static function entradaLote ($produtos)
	{
		$logs = [];

		foreach ($produtos as $produto)
		{
			$logs[] = [
				'tipo'			=> self::TIPO_ENTRADA_LOTE,
				'usuario'		=> @\Auth::user()->id,
				'produto'		=> $produto->id,
			];
		}

		if (count($logs) <= 0)
			return;

		self::insert($logs);
	}

	public static function entrada ($produtos)
	{
		$logs = [];

		foreach ($produtos as $produto)
		{
			$logs[] = [
				'tipo'			=> self::TIPO_ENTRADA,
				'usuario'		=> @\Auth::user()->id,
				'produto'		=> $produto->id,
			];
		}

		if (count($logs) <= 0)
			return;

		self::insert($logs);
	}

	public static function pre_alocacao ($alocacoes)
	{
		$logs = [];

		foreach ($alocacoes as $alocacao)
		{
			$logs[] = [
				'tipo'			=> self::TIPO_PRE_ALOCACAO,
				'usuario'		=> @\Auth::user()->id,
				'produto'		=> $alocacao->produto->id,
				'informacao'	=> json_encode([
					'armario'		=> $alocacao->posicao->armario,
					'gaveta'		=> $alocacao->posicao->gaveta,
					'posicao'		=> $alocacao->posicao->posicao,
				]),
			];
		}

		if (count($logs) <= 0)
			return;

		self::insert($logs);
	}

	public static function alocacao ($alocacoes)
	{
		$logs = [];

		foreach ($alocacoes as $alocacao)
		{
			$logs[] = [
				'tipo'			=> self::TIPO_ALOCACAO,
				'usuario'		=> @\Auth::user()->id,
				'produto'		=> $alocacao->produto->id,
				'informacao'	=> json_encode([
					'armario'		=> $alocacao->posicao->armario,
					'gaveta'		=> $alocacao->posicao->gaveta,
					'posicao'		=> $alocacao->posicao->posicao,
				]),
			];
		}

		if (count($logs) <= 0)
			return;

		self::insert($logs);
	}

	public static function selecao_pedido ($produto, $pedido)
	{
		self::insert([[
			'tipo'			=> self::TIPO_SELECAO_PEDIDO,
			'usuario'		=> @\Auth::user()->id,
			'produto'		=> $produto,
			'informacao'	=> json_encode([
				'pedido'		=> $pedido,
			]),
		]]);
	}

	public static function volta_colmeia ($produto)
	{
		self::insert([[
			'tipo'			=> self::TIPO_VOLTA_COLMEIA,
			'usuario'		=> @\Auth::user()->id,
			'produto'		=> $produto,
		]]);
	}

	public static function saida ($produtos, $nota = null)
	{
		$logs = [];

		foreach ($produtos as $produto)
		{
			$logs[] = [
				'tipo'			=> self::TIPO_SAIDA,
				'usuario'		=> @\Auth::user()->id,
				'produto'		=> $produto->id,
				'informacao'	=> json_encode([
					'nota'			=> $nota,
				]),
			];
		}

		if (count($logs) <= 0)
			return;

		self::insert($logs);
	}

	public static function descarte ($itens)
	{
		$logs = [];
		$dataDescarte = date('Y-m-d H:i:s');
		
		foreach ($itens as $item)
		{
			$logs[] = [
				'tipo'			=> self::TIPO_DESCARTE,
				'data'			=> $dataDescarte,
				'usuario'		=> @\Auth::user()->id,
				'produto'		=> $item->produto,
				'informacao'	=> json_encode([
					'armario'		=> $item->armario,
					'gaveta'		=> $item->gaveta,
					'posicao'		=> $item->posicao,
					'serial'		=> $item->serial,
					'sku'			=> $item->sku,
					'pedido'		=> $item->pedido,
				]),
			];
		}

		if (count($logs) <= 0)
			return;

		self::insert($logs);

		return $dataDescarte;
	}

	public static function devolucao ($produtos)
	{
		if (!is_array($produtos))
			$produtos = [$produtos];

		$logs = [];

		foreach ($produtos as $produto)
		{
			$logs[] = [
				'tipo'			=> self::TIPO_DEVOLUCAO,
				'usuario'		=> @\Auth::user()->id,
				'produto'		=> $produto->id,
			];
		}

		if (count($logs) <= 0)
			return;

		self::insert($logs);
	}

	public static function desalocar ($produto, $posicao)
	{
		$log = [
			'tipo'			=> self::TIPO_DESALOCAR,
			'usuario'		=> @\Auth::user()->id,
			'produto'		=> $produto->id,
			'informacao'	=> json_encode($posicao),
		];

		self::insert([$log]);
	}

	public static function descartar ($produto, $posicao = null)
	{
		$log = [
			'tipo'			=> self::TIPO_DESCARTE,
			'usuario'		=> @\Auth::user()->id,
			'produto'		=> $produto->id,
			'informacao'	=> json_encode([
				'serial'		=> $produto->serial,
				'sku'			=> $produto->sku,
				'armario'		=> @$posicao->armario,
				'gaveta'		=> @$posicao->gaveta,
				'posicao'		=> @$posicao->posicao,
				'pedido'		=> @$posicao->pedido,
			]),
		];

		self::insert([$log]);
	}
}