<?php

namespace App\Models\Colmeia\Log;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_config';
	protected $connection		= 'colmeia';
	protected $fillable			= ['usuario', 'configuracao', 'informacao'];
	protected $casts			= [
		'informacao'				=> 'object',
	];

	public static function gerar (string $tipo, \App\Models\Colmeia\Config $config)
	{
		if (!$config->isDirty())
			return;

		return self::create([
			'usuario'		=> @\Auth::user()->id,
			'configuracao'	=> $config->chave,
			'informacao'	=> ['tipo' => $tipo, 'dados' => $config->getDirty()],
		]);
	}

}