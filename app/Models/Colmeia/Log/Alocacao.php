<?php

namespace App\Models\Colmeia\Log;

use Illuminate\Database\Eloquent\Model;

class Alocacao extends Model
{
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'log_alocacao';
	protected $connection		= 'colmeia';
	protected $fillable			= ['usuario', 'alocacao','informacao'];
	protected $casts			= [
		'data'						=> 'datetime',
		'informacao'				=> 'object',
	];

	public static function gerar (string $tipo, \App\Models\Colmeia\Alocacao $entidade)
	{
		if (!$entidade->isDirty() || is_null($entidade->id))
			return;

		return self::create([
			'usuario'		=> @\Auth::user()->id,
			'alocacao'		=> $entidade->id,
			'informacao'	=> ['tipo' => $tipo, 'dados' => $entidade->getDirty()]
		]);
	}
}