<?php

namespace App\Models\Colmeia;

use Illuminate\Database\Eloquent\Model;
use \App\Helpers\Xml;
use \App\Models\Colmeia\Produto;
use \App\Models\Colmeia\Integracao\Jundsoft;
use \App\Models\Colmeia\Log\Nfe as Log;

class Nfe extends Model
{
	const TIPO_ENTRADA			= 1;
	const TIPO_SAIDA			= 2;

	const NATUREZA_VENDA		= 1;
	const NATUREZA_DEVOLUCAO	= 2;
	const NATUREZA_PERDA		= 3;
	const NATUREZA_DESCONHECIDO	= 4;

	public $incrementing		= false;
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'nfe';
	protected $connection		= 'colmeia';
	protected $fillable			= ['id', 'tipo', 'nota'];
	protected $casts			= [
		'nota'						=> 'object',
	];

	public static $tipos		= [
		self::TIPO_ENTRADA			=> 'Entrada',
		self::TIPO_SAIDA			=> 'Saída',
	];

	public static $naturezas	= [
		self::NATUREZA_VENDA		=> 'Venda',
		self::NATUREZA_DEVOLUCAO	=> 'Devolução',
		self::NATUREZA_PERDA		=> 'Perda',
		self::NATUREZA_DESCONHECIDO	=> 'Desconhecido',
	];

	#codigos CFOP
	private static $cfop		= [
		'venda'						=> array('5100','5101','5102','5103','5104','5105','5106','5109','5110','5111','5112','5113','5114','5115','5116','5117','5118','5119','5120','5122','5123','5124','5125','5150','5151','5152','5155','5156','5206','5350','5351','5352','5353','5356','5357','5359','5360','5400','5401','5402','5403','5405','5408','5409','5414','5415','5501','5502','5504','5505','5550','5551','5552','5554','5557','5900','5901','5902','5903','5904','5905','5906','5907','5908','5909','5910','5911','5912','5913','5914','5915','5916','5917','5920','5922','5923','5924','5925','5926','5928','5929','5931','5932','5933','5949','6000','6100','6101','6102','6103','6104','6105','6106','6107','6108','6109','6110','6111','6112','6113','6114','6115','6116','6117','6118','6119','6120','6122','6123','6124','6125','6150','6151','6152','6155','6156','6206','6350','6351','6352','6353','6356','6357','6359','6400','6401','6402','6403','6404','6408','6409','6414','6415','6501','6502','6504','6505','6550','6551','6552','6554','6557','6900','6901','6902','6903','6904','6905','6906','6907','6908','6909','6910','6911','6912','6913','6914','6915','6916','6917','6920','6922','6923','6924','6925','6929','6931','6932','6933','6949','7000','7100','7101','7102','7105','7106','7127','7206','7350','7358','7550','7551','7900','7949'),
		'devolucao'					=> array('5200','5201','5202','5208','5209','5210','5410','5411','5412','5413','5410','5411','5412','5413','5500','5503','5553','5555','5556','5918','5919','5921','6200','6201','6202','6208','6209','6210','6410','6411','6412','6413','6500','6503','6553','6555','6556','6918','6919','6921','7200','7201','7202','7210','7211','7553','7556','7930'),
		'perda'						=> array('5927'),

	];

	private $productListBySku = [];

	public function GetListaProdutos ()
	{
		$produtos = [];

		foreach ($this->detalhes as $index => $detalhe)
		{
			if (is_null($detalhe->produto))
				continue;

			$detalhe->produto->index = $index;

			$produtos[] = $detalhe->produto;
		}

		return $produtos;
	}

	public function updateProduto (int $index, object $produto)
	{
		$nota = $this->nota;
		$nota->NFe->infNFe->det[$index]->produto = $produto;

		$this->nota = $nota;
		
		return $this;
	}

	public function updateTipoProdutos ($detalheIndex)
	{
		$referencias = [
			'Celulares'		=> Produto::TIPO_TEL_CELULAR,
			'Modem'			=> Produto::TIPO_MODEM,
			'Tablets'		=> Produto::TIPO_TABLET,
			'Roteador'		=> Produto::TIPO_ROTEADOR,
			'Fixo'			=> Produto::TIPO_TEL_FIXO,
			'SIM CARD'		=> Produto::TIPO_SIMCARD,
			'Instalação'	=> NULL,
		];

		$detalhe = $this->detalhes[$detalheIndex];

		Produto
			::where('nfe', $this->id)
			->where('nfe_detalhe', $detalheIndex)
			->update([
				'sku' => @$detalhe->produto->sku,
				'tipo' => @$referencias[$detalhe->produto->tipo_produto],
			])
		;
	}

	public function todosProdutosComEntradaEstoque () : bool
	{
		return Produto
			::where('nfe', $this->id)
			->whereNull('status')
			->count() <= 0
		;
	}

	public function getDetalhesPendentes ()
	{
		return Produto::getDetalhesPendentesFromNota($this);
	}

	public function processarTodosOsDetalhes ()
	{
		if (!$this->exists)
			throw new \Exception("Tentando processar produtos em uma nota não salva", 1);

		$produtos = [];
		
		foreach ($this->detalhes as $index => $detalhe)
		{
			for ($i = 0; $i < $detalhe->prod->qCom; $i++)
				$produtos[] = ['nfe' => $this->id, 'nfe_detalhe' => $index];
		}
		
		foreach (collect($produtos)->chunk(500)->toArray() as $chunk)
			Produto::insert($chunk);

		return $this;
	}

	public function processarTodosOsEans ()
	{
		$detalhes = $this->detalhes;

		foreach ($detalhes as $detalhe)
		{
			if (!is_null(@$detalhe->jundsoft) || !isset($detalhe->prod) || !isset($detalhe->prod->cEAN))
				continue;

			$detalhe->jundsoft = Jundsoft::withTrashed()->firstOrCreate(['ean' => $detalhe->prod->cEAN])->UpdateProduto($detalhe->prod->xProd)->id;
		}

		$this->detalhes = $detalhes;

		return $this;
	}

	public function getProduto (int $detalheIndex)
	{
		if (empty($this->produto) || !is_array($this->produto) || !isset($this->produto[$detalheIndex]))
			return null;

		return (object) $this->produto[$detalheIndex];
	}

	public static function listaNotasItens ($porPagina, $tipo, $nota)
	{
		if (is_null($porPagina))
			return 1;

		return ceil(self::_listaNotas($tipo, $nota)
			->count() / $porPagina
		);
	}

	public static function listaNotasPaginada ($pagina, $porPagina, $sortBy, $sortOrder, $tipo, $nota)
	{
		$query = self::_listaNotas($tipo, $nota);

		return $query
			->skip(($pagina - 1) * $porPagina)
			->take($porPagina)
			#->orderBy($sortBy, $sortOrder)
			->get()
		;
	}

	private static function _listaNotas ($tipo, $nota)
	{
		return self
			::where(function ($query) use ($tipo, $nota) {

				if (!empty($tipo))
					$query->whereIn('tipo', $tipo);

				if (!empty($nota))
					$query->where('id', $nota);

				return $query;
			
			})
		;
	}
 
	public static function getNotasComProdutosPendentes ()
	{
		return self::whereIn('id', Produto::getNotasPendentes())->get();
	}

	public static function criarViaUpload (string $tipo, \Illuminate\Http\UploadedFile $file) : \App\Models\Colmeia\Nfe
	{
		$nota = Xml::toObject($file->get());

		if (!is_array($nota->NFe->infNFe->det))
			$nota->NFe->infNFe->det = [$nota->NFe->infNFe->det];

		foreach ($nota->NFe->infNFe->det as $detalhe)
		{
			$detalhe->produto = null;
			$detalhe->jundsoft = null;
		}

		$file->storeAs('./nfe', $nota->NFe->infNFe->{'@attributes'}->Id . '.xml', 'colmeia');

		$nota = [
			'id'		=> @$nota->NFe->infNFe->ide->nNF,
			'nota'		=> $nota,
		];

		if ($base = self::where('id', $nota['id'])->first())
			throw new \Exception('Esta nota já foi inserida no estoque. <a href="' . route('colmeia.nfe.listardetalhe', ['id' => $base->id]) . '">Clique aqui para vê-la</a>');

		return new self($nota);
	}

	/**
	 * Mutators
	 */

	protected function getEmissorAttribute ()
	{
		return @$this->nota->NFe->infNFe->emit->xNome;
	}

	protected function setDetalhesAttribute (array $values)
	{
		$nota = $this->nota;
		$nota->NFe->infNFe->det = $values;

		$this->nota = $nota;
	}

	protected function getDetalhesAttribute ()
	{
		try
		{
			return $this->nota->NFe->infNFe->det;
		}
		catch (\Exception $exception)
		{
			return null;
		}
	}

	protected function getCodigoAttribute ()
	{
		return @$this->nota->NFe->infNFe->{'@attributes'}->Id;
	}

	protected function getTipoStringAttribute ()
	{
		if (!isset(self::$tipos[$this->tipo]))
			return 'indefinido';
		
		return self::$tipos[$this->tipo];
	}
	
	protected function getNaturezaAttribute ()
	{
		$codigoCfop = @$this->detalhes[0]->prod->CFOP;

		if (in_array($codigoCfop, self::$cfop['venda']))
			return 1;

		if (in_array($codigoCfop, self::$cfop['devolucao']))
			return 2;
		
		if (in_array($codigoCfop, self::$cfop['perda']))
			return 3;

		return 4;
	}

	protected function getEmissaoAttribute ()
	{
		try
		{
			return date('Y-m-d H:i:s', strtotime($this->nota->NFe->infNFe->ide->dhEmi));
		}
		catch (\Exception $exception)
		{
			return null;
		}
	}

	/**
	 * Scopes
	 */

	public function scopeApenasEntrada ($query)
	{
		return $query->where('tipo', self::TIPO_ENTRADA);
	}

	/**
	 * Sobre escreve a funcionalidade para gerar automaticamente LOG em cada edição do model
	 * @method boot
	 * @return void
	 */
	 public static function boot()
	 {
		 parent::boot();
		 self::created(function ($model) {Log::gerar($model);});
	 }
}