<?php

namespace App\Models\Colmeia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EntradaLote extends Model
{
	use SoftDeletes;

	CONST STATUS_PENDENTE			= 0;
	CONST STATUS_COMPLETA			= 1;
	
	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'entrada_lote';
	protected $connection			= 'colmeia';
	protected $attributes			= [];
	protected $fillable				= ['nfe_id','usuario_id','produtos'];
	protected $hidden				= ['nfe_id','usuario_id'];
	protected $casts				= [
		'produtos'						=> 'object',
	];
	protected $appends				= ['status'];

	public static $statuses			= [
		self::STATUS_PENDENTE			=> 'Pendente',
		self::STATUS_COMPLETA			=> 'Completa',
	];

	/**
	 * Mutators
	 */

	protected function getStatusAttribute ()
	{
		return is_null($this->deleted_at) ? self::STATUS_PENDENTE : self::STATUS_COMPLETA;
	}

	protected function getStatusStringAttribute ()
	{
		return self::$statuses[$this->status];
	}

	protected function getUsuarioStringAttribute ()
	{
		if (is_null($this->usuario_id))
			return '<i class="text-gray">Indisponível</i>';

		return $this->usuario->nome;
	}

	protected function getStatusBulletAttribute ()
	{
		$classe = 'label label-default';

		switch ($this->status)
		{
			case self::STATUS_PENDENTE:

				$classe = 'label label-danger';
				break;

			case self::STATUS_COMPLETA:

				$classe = 'label label-success';
				break;

		}

		return '<span class="' . $classe . '">' . $this->status_string . '</span>';
	}

	/**
	 * Scopes
	 */

	public function scopeComFiltro ($query, \Illuminate\Http\Request $request)
	{
		if ($request->has('nota') && !empty($request->nota))
		{
			$query->whereIn('nfe_id', preg_split('/([,; \n]+)/', $request->nota));
		}

		if ($request->has('usuario') && !empty($request->usuario))
			$query->whereIn('usuario_id', $request->usuario);

		if ($request->has('status') && !empty($request->status))
		{
			switch ($request->status)
			{
				case 'concluidos':
					
					$query->withTrashed()->whereNotNull('deleted_at');
					break;

				case 'ambos':

					$query->withTrashed();
					break;
			}
		}

		return $query;
	}

	public function scopeComOrdenacao ($query, \Illuminate\Http\Request $request)
	{
		$sortBy = $request->query('sort_by', 'id');
		$sortOrder = \mb_strtoupper($request->query('sort_order', 'desc'));

		if ($sortOrder !== 'ASC')
			$sortOrder = 'DESC';

		switch ($sortBy)
		{
			case 'status':

				$query->orderByRaw("

					CASE

						WHEN (`deleted_at` IS NULL) THEN 1
						WHEN (`deleted_at` IS NOT NULL) THEN 0

					END {$sortOrder}

				");
				break;

			default:

				$query->orderBy($sortBy, $sortOrder);
		}

		return $query;
	}

	/**
	 * Relationship
	 */

	public function nfe ()
	{
		return $this->belongsTo(\App\Models\Colmeia\Nfe::class);
	}

	public function usuario ()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}