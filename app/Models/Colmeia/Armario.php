<?php

namespace App\Models\Colmeia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Utils;
use \App\Models\Colmeia\Operadora;
use \App\Models\Colmeia\Posicao;
use \App\Models\Colmeia\Log\Armario as Log;

class Armario extends Model
{
	const STATUS_INATIVO		= -1;
	const STATUS_ATIVO			= 1;
	const STATUS_NAOALOCANDO	= 2;

	#Dados básicos do Model Eloquent
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'armario';
	protected $connection		= 'colmeia';
	protected $fillable			= ['status', 'sobressalente', 'nome', 'gavetas', 'posicoes', 'operadora'];
	protected $casts			= [
		'prioridade'				=> 'integer',
		'sobressalente'				=> 'boolean',
	];

	#Status possíveis
	public static $statuses		= [
		self::STATUS_INATIVO		=> 'Inativo',
		self::STATUS_ATIVO			=> 'Ativo',
		self::STATUS_NAOALOCANDO	=> 'Não Alocando',
	];

	/**
	 * Overload de atributo statusstring
	 * Retorna o valor escrito do status do armário
	 * @method getStatusstringAttribute
	 * @return string
	 */
	protected function getStatusStringAttribute ()
	{
		if (!isset(self::$statuses[$this->status]))
			return 'Indefinido';

		return self::$statuses[$this->status];
	}

	/**
	 * Overload de atributo statusstring
	 * Retorna o valor escrito do status do armário
	 * @method getStatusstringAttribute
	 * @return string
	 */
	protected function getSobressalenteString ()
	{
		return $this->sobressalente ? 'Sim' : 'Não';
	}

	/**
	 * Retorna a quantidade total de posições disponíveis no armário separado por gaveta
	 * @method getOcupacaogavetasAttribute
	 * @return array
	 */
	protected function getOcupacaogavetasAttribute ()
	{
		return Posicao
			::select(
				DB::raw('gaveta AS nome'),
				DB::raw('SUM(CASE WHEN status = ' . Posicao::STATUS_VAGA . ' THEN 0 ELSE 1 END) as ocupacao'),
				DB::raw('COUNT(1) as posicoes')
			)
			->where('armario', $this->id)
			->whereIn('status', [1,2])
			->groupBy('gaveta')
			->orderBy(DB::raw("RIGHT(CONCAT('000000', gaveta), 6)"), 'asc')
			->get()
		;
	}

	/**
	 * Retorna a quantidade de posições disponíveis no armário inteiro
	 * @method getPosicoeslivresAttribute
	 * @return int
	 */
	protected function getPosicoeslivresAttribute ()
	{
		return Posicao
			::where('armario', $this->id)
			->where('status', Posicao::STATUS_VAGA)
			->count()
		;
	}

	protected function getGavetasAttribute ()
	{
		return Posicao::distinct('gaveta')->where('armario', $this->id)->count('gaveta');
	}

	protected function getPosicoesAttribute ()
	{
		return Posicao::where('armario', $this->id)->count();
	}

	public function getTodasPosicoesDescarte ()
	{
		return Posicao
			::where('status', Posicao::STATUS_DESCARTE)
			->where('armario', $this->id)
			->get()
		;
	}

	/**
	 * Cria ou exclui todas as posiçõe de acordo com a nova configuração do armário
	 * OBS ser muito cuidadoso ao mexer com esse método, pois ele pode remover registros do banco caso a quandidade de gavetas e posições configuradas seja menor do que a já existente no banco
	 * @method updateGavetasPosicoes
	 * @return App\Models\Colmeia\Armario
	 */
	public function updateGavetasPosicoes ($gavetas, $posicoes) : \App\Models\Colmeia\Armario
	{
		$posicoesPorGaveta = Posicao
			::select(
				'gaveta',
				DB::raw('COUNT(1) as quantidade')
			)
			->where('armario', $this->id)
			->groupBy('gaveta')
			->get()
			->pluck('quantidade', 'gaveta')
			->all()
		;

		$posicoesParaCriar = [];
		$codigosGavetasExistentes = [];

		for ($gaveta = 1; $gaveta <= $gavetas; $gaveta++)
		{
			$codigoGaveta = Utils::converteParaAlfabetico($gaveta);
			$codigosGavetasExistentes[] = $codigoGaveta;

			if (!isset($posicoesPorGaveta[$codigoGaveta]) || $posicoesPorGaveta[$codigoGaveta] < $posicoes)
			{
				for ($posicao = @$posicoesPorGaveta[$codigoGaveta] + 1; $posicao <= $posicoes; $posicao++)
				{
					$posicoesParaCriar[] = [
						'armario'			=> $this->id,
						'status'			=> 1,
						'gaveta'			=> $codigoGaveta,
						'posicao'			=> $posicao,
					];
				}
			}
		}

		if (count($posicoesParaCriar) > 0)
			Posicao::insert($posicoesParaCriar);
		
		Posicao
			::where('armario', $this->id)
			->where(function ($query) use ($codigosGavetasExistentes, $posicoes) {
				$query->whereNotIn('gaveta', $codigosGavetasExistentes);
				$query->orWhere('posicao', '>', $posicoes);
			})
			->delete()
		;

		return $this;
	}

	/**
	 * Sobre escreve a funcionalidade para gerar automaticamente LOG em cada edição do model
	 * @method boot
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}

	public static function descarte ()
	{
		return DB::connection('colmeia')
			->table('armario_posicao')
			->select(
				'armario.id',
				'armario.status',
				'armario.nome',
				DB::raw('COUNT(armario_posicao.id) as descartes')
			)
			->join('armario', 'armario.id', '=', 'armario_posicao.armario')
			->whereIn('armario.status', [self::STATUS_ATIVO, self::STATUS_NAOALOCANDO])
			->where('armario_posicao.status', Posicao::STATUS_DESCARTE)
			->groupBy('armario.id')
			->groupBy('armario.status')
			->groupBy('armario.nome')
			->get()
		;
	}

	public static function acharPosicao (array $operadoras, int $limite, array $posicoesExcludentes = [])
	{
		// return Posicao
		// 	::whereIn('armario', self::select('id')
		// 		->where('status', self::STATUS_ATIVO)
		// 		->whereIn('operadora', $operadoras)
		// 	)
		// 	->whereNotIn('id', $posicoesExcludentes)
		// 	->where('status', Posicao::STATUS_VAGA)
		// 	->union(Posicao
		// 		::whereIn('armario', self::select('id')
		// 			->where('status', self::STATUS_ATIVO)
		// 			->where('sobressalente', true)
		// 			->whereNotIn('operadora', $operadoras)
		// 		)
		// 		->where('status', Posicao::STATUS_VAGA)
		// 	)
		// ;

		return Posicao
			::select([
				'armario_posicao.id',
				'armario_posicao.armario',
				'armario_posicao.status',
				'armario_posicao.gaveta',
				'armario_posicao.posicao',
				'armario_posicao.produto',
				'armario_posicao.serial',
				'armario_posicao.sku',
				'armario_posicao.pedido',
				'armario_posicao.ocorrencia',
			])
			->join('armario', 'armario.id', 'armario_posicao.armario')
			->where('armario.status', Self::STATUS_ATIVO)
			->where('armario_posicao.status', Posicao::STATUS_VAGA)
			->whereNotIn('armario_posicao.id', $posicoesExcludentes)
			->where(function ($query) use ($operadoras) {
				
				return $query
					->whereIn('armario.operadora', $operadoras)
					->orWhere('armario.sobressalente', true)
				;

			})
			->take($limite)
			->orderByRaw('IF (armario.operadora IN (' . implode(',', $operadoras) . '), 1, 2) ASC')
			->orderBy('armario.prioridade', 'DESC')
			->get()
		;
	}

	/**
	 * Retorna contagem de todas as posições livres separadoas e agrupadas por armário > gaveta
	 * @method getTodasPosicoesLivres
	 * @return array
	 */
	public static function getTodasPosicoesLivres () : array
	{
		$posicoes = Posicao::select(
				'armario',
				'gaveta',
				DB::raw('COUNT(1) as quantidade')
			)
			->whereIn('status', [1,2])
			->groupBy('gaveta')
			->groupBy('armario')
			->get()
		;

		$gavetas = [];

		foreach ($posicoes as $posicao)
			$gavetas[$posicao->armario][$posicao->gaveta] = $posicao->quantidade;

		return $gavetas;
	}
}