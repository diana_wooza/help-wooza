<?php

namespace App\Models\Colmeia\Integracao;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


use \App\Models\Colmeia\Log\Integracao\Jundsoft as Log;

class Jundsoft extends Model
{
	use SoftDeletes;

	#Dados básicos do Model Eloquent
	public $timestamps				= false;
	protected $primaryKey			= 'id';
	protected $table				= 'integracao_jundsoft';
	protected $connection			= 'colmeia';
	protected $attributes			= [];
	protected $fillable				= ['ean','produto','allied_sku','jund_id'];
	protected $hidden				= ['deleted_at'];
	protected $casts				= [
		'ean'							=> 'string',
		'produto'						=> 'string',
		'allied_sku'					=> 'string',
		'jund_id'						=> 'integer',
	];
	protected $appends				= ['is_integracao_completa'];

	public function UpdateProduto ($produto)
	{
		if ($this->produto == $produto)
			return $this;

		$this->produto == $produto;
		$this->save();

		return $this;
	}

	/**
	 * Mutators
	 */

	protected function getDescricaoAttribute ()
	{
		$descricao = '<strong>EAN:</strong>: ' . $this->ean;

		if ($this->is_integracao_completa)
		{
			$descricao .= '<br /><strong>SKU Allied</strong>: ' . $this->allied_sku;
			$descricao .= '<br /><strong>Jund ID</strong>: ' . $this->jund_id;
		}
		elseif (\Auth::user()->checkPermissao('colmeia.admin'))
		{
			$descricao .= '<br /><strong>Integração:</strong> <a class="label label-danger" href="' . route('colmeia.jundsoft.editar', [$this->id]) . '" target="_blank">Pendente</a>';
		}
		else
		{
			$descricao .= '<br /><strong>Integração:</strong> <span class="label label-danger">Pendente</span>';
		}

		return $descricao;
	}

	protected function getAlliedSkuStringAttribute ()
	{
		if (is_null($this->allied_sku))
			return '<i class="text-gray">Não informado</i>';

		return $this->allied_sku;
	}

	protected function getJundIdStringAttribute ()
	{
		if (is_null($this->jund_id))
			return '<i class="text-gray">Não informado</i>';

		return $this->jund_id;
	}

	protected function getProdutoStringAttribute ()
	{
		if (is_null($this->produto))
			return '<i class="text-gray">Não informado</i>';

		return $this->produto;
	}

	protected function getIsIntegracaoCompletaAttribute ()
	{
		return !is_null($this->allied_sku) && !is_null($this->jund_id);
	}

	/**
	 * Custom Scopes
	 */

	public function scopeComFiltro ($query, \Illuminate\Http\Request $request)
	{
		if ($request->has('ean') && !empty($request->ean))
			$query->where('ean', $request->ean);

		if ($request->has('allied_sku') && !empty($request->allied_sku))
			$query->where('allied_sku', $request->allied_sku);

		if ($request->has('jund_id') && !empty($request->jund_id))
			$query->where('jund_id', $request->jund_id);

		return $query;
	}

	/**
	 * Laravel initializer
	 */

	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}
}