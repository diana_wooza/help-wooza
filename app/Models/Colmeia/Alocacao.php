<?php

namespace App\Models\Colmeia;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Colmeia\Produto;
use \App\Models\Colmeia\Posicao;
use \App\Models\Colmeia\Log\Alocacao as Log;
use \Carbon\Carbon;

class Alocacao extends Model
{
	const STATUS_INCOMPLETO		= -1;
	const StATUS_COMPLETO		= 1;

	#Dados básicos do Model Eloquent
	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'alocacao';
	protected $connection		= 'colmeia';
	protected $fillable			= ['status', 'tag', 'produtos'];
	protected $casts			= [
		'produtos'					=> 'object',
	];

	public static $statuses		= [
		self::STATUS_INCOMPLETO		=> 'Incompleta',
		self::StATUS_COMPLETO		=> 'Completa',
	];

	private $alocacoes = null;

	protected function getStatusStringAttribute ()
	{
		if (!isset(self::$statuses[$this->status]))
			return 'Indefinido';

		return self::$statuses[$this->status];
	}

	protected function getDataAttribute ()
	{
		return @Log
			::where('alocacao', $this->id)
			->whereRaw("informacao RLIKE '\"criar\"'")
			->first()
			->data
		;
	}

	protected function getAlocacoesAttribute ()
	{
		if (is_null($this->alocacoes))
			$this->popularAlocacoes();

		return $this->alocacoes;
	}

	private function popularAlocacoes ()
	{
		$listas = [
			'produtos' => [],
			'posicoes' => [],
		];

		$referencias = [
			'produtos'	=> [],
			'posicoes'	=> [],
		];

		$this->alocacoes = [];

		foreach ($this->produtos as $alocacao)
		{
			$listas['produtos'][] = $alocacao->produto;
			$referencias['produtos'][$alocacao->produto] = $alocacao;

			$listas['posicoes'][] = $alocacao->posicao;
			$referencias['posicoes'][$alocacao->posicao] = $alocacao;

			$this->alocacoes[] = $alocacao;
		}

		foreach (Produto::whereIn('id', $listas['produtos'])->get() as $produto)
			$referencias['produtos'][$produto->id]->produto = $produto;
		
		foreach (Posicao::whereIn('id', $listas['posicoes'])->get() as $posicao)
			$referencias['posicoes'][$posicao->id]->posicao = $posicao;
	}

	/**
	 * Sobre escreve a funcionalidade para gerar automaticamente LOG em cada edição do model
	 * @method boot
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		self::created(function ($model) {Log::gerar('criar', $model);});
		self::saving(function ($model) {Log::gerar('editar', $model);});
	}

	public static function pendentes ()
	{
		return self::where('status', self::STATUS_INCOMPLETO)->get();
	}

	public static function getTagProximoNumero ()
	{
		return Log
			::select('alocacao')
			->where('data', '>=', date('Y-m-d 00:00:00'))
			->groupBy('alocacao')
			->get()
			->count('alocacao') + 1
		;
	}
}