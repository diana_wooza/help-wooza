<?php

namespace App\Models\Colmeia;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use \Picqer\Barcode\BarcodeGeneratorPNG;
use \App\Helpers\Xml;
use \App\Helpers\Sku;
use \App\Helpers\Magento;
use \App\Models\Colmeia\Operadora;
use \App\Models\Colmeia\Posicao;
use \App\Models\Colmeia\Log\Produto as Log;

class Produto extends Model
{
	const STATUS_DISPONIVEL		= 1;
	const STATUS_PRE_ALOCADO	= 2;
	const STATUS_ALOCADO		= 3;
	const STATUS_RESERVADO		= 4;
	const STATUS_RETIRADO		= 5;
	const STATUS_DESCARTE		= 6;
	
	#deprecated
	const STATUS_PERDA			= 7;
	const STATUS_DEVOLUCAO		= 8;
	const STATUS_DEVOLUCAOCOMP	= 9;

	const TIPO_SIMCARD			= 1;
	const TIPO_TEL_CELULAR		= 2;
	const TIPO_TEL_FIXO			= 3;
	const TIPO_TABLET			= 4;
	const TIPO_MODEM			= 5;
	const TIPO_ROTEADOR			= 6;

	public $timestamps			= false;
	protected $primaryKey		= 'id';
	protected $table			= 'produto';
	protected $connection		= 'colmeia';
	protected $fillable			= ['status','nfe','nfe_detalhe','serial','sku','integracao_jund_id'];

	public static $statuses		= [
		self::STATUS_DISPONIVEL		=> 'Disponível',
		self::STATUS_PRE_ALOCADO	=> 'Pré Alocação',
		self::STATUS_ALOCADO		=> 'Alocado',
		self::STATUS_RESERVADO		=> 'Reservado',
		self::STATUS_RETIRADO		=> 'Retirado',
		self::STATUS_DESCARTE		=> 'Descartado',
		self::STATUS_PERDA			=> 'Perda',
		self::STATUS_DEVOLUCAO		=> 'Devolução',
		self::STATUS_DEVOLUCAOCOMP	=> 'Devolução Compra',

	];

	public static $tipos		= [
		self::TIPO_SIMCARD			=> 'Sim Card',
		self::TIPO_TEL_CELULAR		=> 'Telefone Celular',
		self::TIPO_TEL_FIXO			=> 'Telefone Fixo',
		self::TIPO_TABLET			=> 'Tablet',
		self::TIPO_MODEM			=> 'Modem',
		self::TIPO_ROTEADOR			=> 'Roteador',
	];

	public function getPosicao ()
	{
		// if (!in_array($this->status, [self::STATUS_ALOCADO, self::STATUS_RESERVADO]))
		// 	return null;

		return Posicao::where('produto', $this->id)->first();
	}

	public function alocar ()
	{
		$this->status = self::STATUS_ALOCADO;
	}

	public function liberar ()
	{
		$this->status = self::STATUS_DISPONIVEL;
	}

	public function retirar ()
	{
		$this->status = self::STATUS_RETIRADO;
	}

	public function descartar ()
	{
		$this->status = self::STATUS_DESCARTE;
	}

	public function historicoCompleto ()
	{
		return Log
			::where('produto', $this->id)
			->orderBy('data', 'asc')
			->get()
		;
	}

	public function getSerialBarcodeAsPng ()
	{
		return 'data:image/png;base64,' . base64_encode((new BarcodeGeneratorPNG())->getBarcode($this->serial . 'w', BarcodeGeneratorPNG::TYPE_CODE_128));
	}

	public static function AddSerialToBlankItem (\App\Models\Colmeia\Nfe $nota, int $detalhe, $serial, $sku, $operadora)
	{
		$detalheData = $nota->detalhes[$detalhe];

		$updateData = [
			'status'				=> self::STATUS_DISPONIVEL,
			'serial'				=> $serial,
			'sku'					=> $sku,
			'operadora'				=> $operadora,
			'integracao_jund_id'	=> $detalheData->jundsoft,
		];

		do
		{
			$produto = self
				::whereNull('serial')
				->where('nfe', $nota->id)
				->where('nfe_detalhe', $detalhe)
				->first()
			;

			if (empty($produto))
				return null;
		}
		while (!self::where('id', $produto->id)->whereNull('serial')->update($updateData));
		
		return $produto;
	}

	public static function skusParaNotParaSelect ($nota)
	{
		return self
			::select('sku')
			->distinct('sku')
			->whereNotNull('sku')
			->where('nfe', $nota)
			->get()
			->pluck('sku', 'sku')
		;
	}

	public static function listaProdutosItens ($porPagina, $status, $tipo, $produto, $operadora, $sku, $serial, $nota)
	{
		if (is_null($porPagina))
			return 1;

		return ceil(self::_listaProdutos($status, $tipo, $produto, $operadora, $sku, $serial, $nota)
			->count() / $porPagina
		);
	}

	public static function listaProdutosPaginada ($pagina, $porPagina, $status, $tipo, $produto, $operadora, $sku, $serial, $nota)
	{
		$query = self::_listaProdutos($status, $tipo, $produto, $operadora, $sku, $serial, $nota);

		if (is_numeric($pagina) && !is_null($porPagina) && is_numeric($porPagina))
		{
			$query
				->take($porPagina)
				->skip(($pagina - 1) * $porPagina)
			;
		}
		
		return $query->get();
	}

	private static function _listaProdutos ($status, $tipo, $produto, $operadora, $sku, $serial, $nota)
	{
		return self
			::select(['produto.id','produto.tipo','produto.status','produto.nfe','produto.nfe_detalhe','produto.operadora','produto.serial','produto.sku','nfe.nota'])
			->addSelect('nfe.nota')
			->join('nfe', 'nfe.id', '=', 'produto.nfe')
			->whereNotNull('status')
			->where(function ($query) use ($status, $tipo, $produto, $operadora, $sku, $serial, $nota) {

				if (!empty($status) && is_array($status) && count($status) > 0)
					$query->whereIn('status', $status);

				if (!empty($tipo) && is_array($tipo) && count($tipo) > 0)
					$query->whereIn('tipo', $tipo);

				if (!empty($produto) && !is_null($produto))
				{
					$produtos = [];

					foreach (Magento::getSimpleProductsList() as $item)
					{
						if (strpos(\strtolower($item->name), \strtolower($produto)) !== false)
							$produtos[] = $item->sku;
					}

					if (count($produtos) > 0)
						$query->whereIn('sku', $produtos);
				}

				if (!empty($operadora) && is_array($operadora) && !is_null($operadora))
				{
					$query->where(function ($query) use ($operadora) {
						
						foreach (Operadora::find($operadora) as $item)
							$query->orWhere('sku', 'like', '%' . $item->codigo);
					
					});
				}

				if (!empty($sku))
					$query->where('sku', $sku);

				if (!empty($serial))
					$query->where('serial', $serial);

				if (!empty($nota))
					$query->where('nfe', $nota);

				return $query;
			})
		;
	}

	public static function getListaSeriais ($sku = null, $operadoras = [], $ufs = [])
	{
		return self
			::select('produto.sku')
			->selectRaw('SUM(IF(produto.status IS null, 1, 0)) fora_estoque')
			->selectRaw('SUM(IF(produto.status = ' . self::STATUS_DISPONIVEL . ', 1, 0)) em_estoque')
			->selectRaw('SUM(IF(produto.status = ' . self::STATUS_PRE_ALOCADO . ', 1, 0)) em_alocacao')
			->selectRaw('SUM(IF(produto.status = ' . self::STATUS_ALOCADO . ' AND armario_posicao.status = ' . Posicao::STATUS_DISPONIVEL . ', 1, 0)) disponiveis')
			->selectRaw('SUM(IF(produto.status = ' . self::STATUS_ALOCADO . ' AND armario_posicao.status = ' . Posicao::STATUS_RESERVADO . ', 1, 0)) reservado')
			->selectRaw('SUM(IF(produto.status = ' . self::STATUS_ALOCADO . ' AND armario_posicao.status = ' . Posicao::STATUS_DESCARTE . ', 1, 0)) descarte')
			->leftJoin('armario_posicao', 'armario_posicao.produto', '=', 'produto.id')
			->where('produto.tipo', self::TIPO_SIMCARD)
			->whereNotNull('produto.sku')
			->where(function ($query) use ($operadoras, $ufs) {

				if (empty($operadoras) && empty($ufs))
					return $query;

				$sku = new Sku();

				if (is_array($operadoras))
				{
					foreach ($operadoras as $operadora)
						$sku->addOperadora($operadora);
				}

				if (is_array($ufs))
				{
					foreach ($ufs as $uf)
						$sku->addEstado($uf);
				}

				return $query->whereRaw("produto.sku RLIKE '{$sku->toRegex()}'");

			})
			->where(function ($query) use ($sku) {

				if (!is_null($sku) && !empty($sku))
					$query->where('produto.sku', 'like', '%' . $sku . '%');

				return $query;
			
			})
			->where(function ($query) {

				$query->whereNull('produto.status');

				$query->orWhereNotIn('produto.status', [
					self::STATUS_RESERVADO,
					self::STATUS_RETIRADO,
					self::STATUS_DESCARTE,
				]);

				return $query;

			})
			->groupBy('produto.sku')
			->get()
		;
	}

	public static function getListaDevolucao (?string $serial)
	{
		return self
			::whereIn('id', Posicao::select('produto')->where('status', Posicao::STATUS_DESCARTE)->get()->pluck('produto'))
			->where(function ($query) use ($serial) {

				if (!is_null($serial))
					$query->where('serial', 'like', '%' . $serial . '%');

				return $query;
			})
			->get()
		;
	}

	public static function getEstoqueBySku ()
	{
		return self
			::select(
				'sku',
				DB::raw('COUNT(1) as total')
			)
			->whereNotNull('sku')
			->whereIn('status', [self::STATUS_DISPONIVEL, self::STATUS_PRE_ALOCADO, self::STATUS_ALOCADO])
			->groupBy('sku')
			->get()
			->pluck('total', 'sku')
		;
	}

	public static function podeSerAlocado (string $serial) : bool
	{
		return self::podemSerAlocados([$serial]);
	}
	
	public static function podemSerAlocados (array $seriais) : bool
	{
		return $produto = self
			::whereIn('serial', $seriais)
			->where('status', self::STATUS_DISPONIVEL)
			->count() == count($seriais)
		;
	}

	public static function bySerial ($serial)
	{
		return self::where('serial', $serial)->get();
	}

	public static function getSeriaisInDatabase (array $seriais)
	{
		return self::whereIn('serial', $seriais)->pluck('serial')->all();
	}

	public static function entradaEmMassa (\App\Models\Colmeia\Nfe $nota, int $detalhe, \App\Models\Colmeia\Operadora $operadora, string $sku, array $seriais) : bool
	{
		$itensToUpdate = count($seriais);
		$produtos = self::where('nfe', $nota->id)
			->where('nfe_detalhe', $detalhe)
			->whereNull('serial')
			->take($itensToUpdate)
			->get()
			->all()
		;

		if ($itensToUpdate != count($produtos))
			return false;

		for ($i = 0; $i < $itensToUpdate; $i++)
		{
			$produto = $produtos[$i];

			$produto->sku = $sku;
			$produto->serial = $seriais[$i];
			$produto->operadora = $operadora->id;

			$produto->save();
		}

		return true;
	}

	public static function getNotasPendentes ()
	{
		return self::distinct('nfe')
			->whereNull('serial')
			->get()
			->pluck('nfe')
			->all()
		;
	}

	/**
	 */
	public static function getFromNotaIndex (\App\Models\Colmeia\Nfe $nota, int $detalhe, ?int $paginacao = null)
	{
		$query = self
			::where('nfe', $nota->id)
			->where('nfe_detalhe', $detalhe)
		;

		if (!is_null($paginacao))
		{
			$query
				->whereNull('status')
				->take($paginacao)
			;
		}

		return $query->get();
	}

	/**
	 * Retorna o número de dígitos que o serial do produto precisa conter
	 * @method getSerialDigitosPorSku
	 * @param string $sku Código SKU do produto específico
	 * @return int
	 */
	public static function getSerialDigitosPorSku (string $sku) : int
	{
		#Pega o produto da base do magento
		$produto = Magento::getProductListIndexdBySku($sku);

		#Produtos que não sejam SIM CARDS possuem 15 dígitos
		if ($produto->tipo_produto != 'SIM CARD')
			return 15;

		#Sim cards da OI possuem apenas 19 dígitos
		if (\strtoupper(\substr($sku, -1)) == 'O')
			return 19;

		#quantidade padrão de seriais para chip é de 20 caracteres
		return 20;
	}

	public static function getDetalhesPendentesFromNota (\App\Models\Colmeia\Nfe $nota)
	{
		return self::_getDetalhesPendentesFromNota($nota)->get();
	}

	public static function getDetalhesPendentesFromNotaAndDetalhe (\App\Models\Colmeia\Nfe $nota, int $detalhe)
	{
		return self::_getDetalhesPendentesFromNota($nota)->where('nfe_detalhe', $detalhe)->first();
	}

	private static function _getDetalhesPendentesFromNota (\App\Models\Colmeia\Nfe $nota)
	{
		return self::select(
				'nfe_detalhe',
				DB::raw('COUNT(1) as total'),
				DB::raw('SUM(CASE WHEN serial IS NULL THEN 1 ELSE 0 END) as pendente')
			)
			->where('nfe', $nota->id)
			->groupBy('nfe_detalhe')
		;
	}

	public static function getIdsFromNota (\App\Models\Colmeia\Nfe $nota) : array
	{
		return self::select('id')
			->where('nfe', $nota->id)
			->get()
			->pluck('id')
			->all()
		;
	}

	/**
	 * Mutators
	 */

	public function getConstaEmEstoqueAttribute ()
	{
		return in_array($this->status, [
			self::STATUS_DISPONIVEL,
			self::STATUS_ALOCADO,
		]);
	}

	public function getConstaAlocadoAttribute ()
	{
		return in_array($this->status, [
			Produto::STATUS_ALOCADO,
		]);
	}

	public function getDetalheAttribute ()
	{
		return @$this->nota->nota->NFe->infNFe->det[$this->nfe_detalhe];
	}

	public function getProdutoAttribute ()
	{
		$detalhe = $this->detalhe;

		if (is_null($detalhe) || !isset($detalhe->produto))
			return null;

		return $detalhe->produto;
	}

	public function getJundsoftDescricaoAttribute ()
	{
		if (is_null($jundsoft = $this->jundsoft))
			return '<i class="text-gray">Não Definido</i>';

		return $jundsoft->descricao;
	}

	public function getEanAttribute ()
	{
		return @$this->jundsoft->ean;
	}

	public function getEanStringAttribute ()
	{
		if (is_null($ean = $this->ean))
			return '<i class="text-gray">não detectado</i>';

		return $ean->descricao;
	}
	
	public function getProdutoNomeStringAttribute ()
	{
		if (is_null($produto = $this->produto))
			return '<i class="text-gray">não informado</i>';

		return $produto->name;
	}

	public function getIsIntegracaoJundsoftCompletaAttribute ()
	{
		return !is_null($this->jundsoft) && $this->jundsoft->is_integracao_completa;
	}

	public function getTipoStringAttribute ()
	{
		if (!isset(self::$tipos[$this->tipo]))
			return 'Indefinido';

		return self::$tipos[$this->tipo];
	}

	public function getStatusStringAttribute ()
	{
		if (!isset(self::$statuses[$this->status]))
			return 'Indefinido';

		return self::$statuses[$this->status];
	}

	public function getStatusBulletAttribute ()
	{
		$class = 'label-default';

		switch ($this->status)
		{
			case self::STATUS_DISPONIVEL:
			case self::STATUS_PRE_ALOCADO:

				$class = 'label-primary';
				break;

			case self::STATUS_ALOCADO:
			case self::STATUS_RESERVADO:
			case self::STATUS_RETIRADO:

				$class = 'label-success';
				break;

				#deprecated
			case self::STATUS_DEVOLUCAO:
			case self::STATUS_DEVOLUCAOCOMP:
				
				$class = 'label-warning';
				break;
				
			case self::STATUS_PERDA:
			case self::STATUS_DESCARTE:
				
				$class = 'label-danger';
				break;

		}

		return '<span class="label ' . @$class . '">' . @$this->status_string . '</span>';
	}
	
	/**
	 * Custom Scope
	 */

	public function scopeComFiltro ($query, \Illuminate\Http\Request $request)
	{
		if ($request->has('status') && !empty($request->status))
			$query->whereIn('status', $request->status);

		if ($request->has('tipo') && !empty($request->tipo))
			$query->whereIn('tipo', $request->tipo);

		if ($request->has('sku') && !empty($request->sku))
		{
			$skus = $request->sku;
			
			if (!is_array($skus))
				$skus = \preg_split('/[,\s.]+/', $skus);
			
			$query->whereIn('sku', $skus);
		}

		if ($request->has('serial') && !empty($request->serial))
			$query->whereIn('serial', \preg_split('/[,\s.]+/', $request->serial));

		if ($request->has('nota') && !empty($request->nota))
			$query->whereIn('nfe', \preg_split('/[,\s.]+/', $request->nota));

		if ($request->has('produto') && !empty($request->produto))
		{
			$produtos = [];

			foreach (Magento::getSimpleProductsList() as $item)
			{
				if (strpos(\strtolower($item->name), \strtolower($request->produto)) !== false)
					$produtos[] = $item->sku;
			}

			if (count($produtos) > 0)
				$query->whereIn('sku', $produtos);
		}

		if ($request->has('operadora') && !empty($request->operadora))
		{
			$query->where(function ($query) use ($request) {
				
				foreach (Operadora::find($request->operadora) as $item)
					$query->orWhere('sku', 'like', '%' . $item->codigo);
			
			});
		
		}

		return $query;
	}

	public function scopeComOrdenacao ($query, \Illuminate\Http\Request $request, $defaultSortBy = 'id', $defaultSortOrder = 'desc')
	{
		$sortBy = $request->query('sort_by', $defaultSortBy);
		$sortOrder = $request->query('sort_order', $defaultSortOrder);

		return $query->orderBy($sortBy, $sortOrder);
	}

	public function scopePossuiDadosNota ($query)
	{
		return $query->whereHas('nota', function ($query) {
			return $query->whereNotNull('nota');
		});
	}

	public function scopeDisponivelParaAlocacao ($query)
	{
		return $query->where('status', self::STATUS_DISPONIVEL);
	}

	public function scopeEmEstoque ($query)
	{
		return $query->whereNotNull('status');
	}

	/**
	 * Relationships
	 */

	public function nota ()
	{
		return $this->belongsTo(\App\Models\Colmeia\Nfe::class, 'nfe');
	}

	public function posicao ()
	{
		return $this->hasOne(Posicao::class, 'produto');
	}

	public function jundsoft ()
	{
		return $this->belongsTo(\App\Models\Colmeia\Integracao\Jundsoft::class, 'integracao_jund_id')->withTrashed();
	}

	public function entrada ()
	{
		return $this->hasOne(\App\Models\Colmeia\Log\Produto::class, 'produto');
	}
}