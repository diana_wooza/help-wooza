<?php

namespace App\Models\Cron;

use Illuminate\Database\Eloquent\Model;

class Crons extends Model {

	#Configuração padrão do Eloquent para definição de qual tabela do banco de dados e qual conexão será usada para gerenciar esse model
	protected $table			= 'crons';
	protected $connection		= 'crons';
	public $timestamps = false;
	
}