<?php

namespace App\Models\Motivos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Motivos {
	const SUCCESS_MESSAGE 		    = 'Importação realizada com sucesso.';
    const FAIL_MESSAGE 			    = 'Não foi possível fazer a importação desta planilha.';
    const ACCESS_DENIED             = 'Acesso restrito a administradores.';
    
    public static function listarStatusMotivos() : Array
	{
        if( !Auth::check() ) {
            throw new \Exception( self::ACCESS_DENIED );
        }
        
        $motivos = null;

		try{
                $sql = "SELECT
                                a.motivo,
                                a.label,
                                b.status
                            FROM
                                sales_order_motivo a
                            LEFT JOIN 
                                sales_order_status_motivo b ON a.motivo = b.motivo
                            ORDER BY status DESC;";

                $motivos = DB::connection('magento')->select(DB::raw($sql));
		}
		catch( \Exception $e ) {
			throw new \Exception( $e );
		}
				
		return $motivos;
    }

    public static function listarStatus() : Array
	{
        if( !Auth::check() ) {
            throw new \Exception( self::ACCESS_DENIED );
        }
        
        $status = null;

		try{
                $sql = "SELECT
                                DISTINCT status
                            FROM
                                sales_flat_order
                            WHERE
                                status IS NOT NULL AND 
                                status != ''
                            ORDER BY status ASC;";

                $status = DB::connection('magento')->select(DB::raw($sql));
		}
		catch( \Exception $e ) {
			throw new \Exception( $e );
		}
				
		return $status;
    }

    public static function criar( $motivo )
    {
        if( !Auth::check() ) {
            throw new \Exception( self::ACCESS_DENIED );
        }

        try{
            $sqlMotivo = "INSERT INTO sales_order_motivo (motivo, label) VALUES('" . $motivo->motivo . "','" . $motivo->label . "');";
            $sqlStatus = "INSERT INTO sales_order_status_motivo(status, motivo) VALUES('" . $motivo->status . "','" . $motivo->motivo . "');";
    
            DB::connection('magento')->update($sqlMotivo);
            DB::connection('magento')->update($sqlStatus);

            DB::connection('magento_qa')->update($sqlMotivo);
            DB::connection('magento_qa')->update($sqlStatus);
        }
		catch( \Exception $e ) {
			throw new \Exception( $e );
		}
    }

    public static function getMotivo( $id ) : object
    {
        if( !Auth::check() ) {
            throw new \Exception( self::ACCESS_DENIED );
        }
        
        $motivo = null;

		try{
                $sql = "SELECT
                                a.motivo,
                                a.label,
                                b.status
                            FROM
                                sales_order_motivo a
                            LEFT JOIN 
                                sales_order_status_motivo b ON a.motivo = b.motivo
                            WHERE
                                a.motivo = '" . $id . "';";

                $motivo = DB::connection('magento')->select(DB::raw($sql));
		}
		catch( \Exception $e ) {
			throw new \Exception( $e );
		}
                
        if( !$motivo ) throw new \Exception( "Motivo não encontrado" );

		return $motivo[0];
    }
}
  