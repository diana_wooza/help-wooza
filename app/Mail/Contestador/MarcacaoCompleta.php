<?php

namespace App\Mail\Contestador;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Contestador\Marcacao;

class MarcacaoCompleta extends Mailable
{
	use Queueable, SerializesModels;

	public $criticos;

	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct (Marcacao $marcacao)
	{
		$this->marcacao = $marcacao;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build ()
	{
		$success = $this->marcacao->status != Marcacao::STATUS_FALHA;

		return $this
			->subject(($success ? 'Arquivo Marcado: ' : 'Falha na marcação: ') . $this->marcacao->marcador->nome)
			->view('contestador.email.marcacao_completa')
			->with([
				'success' => $success,
				'marcador' => $this->marcacao->marcador->nome,
			]);
		;
	}
}
