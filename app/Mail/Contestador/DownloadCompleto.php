<?php

namespace App\Mail\Contestador;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Contestador\Processo;

class DownloadCompleto extends Mailable
{
	use Queueable, SerializesModels;

	public $criticos;

	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct (Processo $processo)
	{
		$processo->loadMissing('usuario','competencia','arquivo');
		$this->processo = $processo;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build ()
	{
		return $this
			->subject('Exportação Concluída')
			->view('contestador.email.download_completo')
			->with([
				'solicitante'	=> $this->processo->usuario->nome,
				'data'			=> date('d \d\e F \d\e Y', strtotime($this->processo->conclusao)),
				'arquivo'		=> $this->processo->arquivo->nome,
				'competencia'	=> $this->processo->competencia->data_string,
			]);
		;
	}
}
