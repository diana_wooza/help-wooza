<?php

namespace App\Mail\Magento\Consumer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PedidosDecriptados extends Mailable
{
	use Queueable, SerializesModels;

	public $arquivo;

	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct ($arquivo, $data_inicial, $data_final)
	{
		$this->arquivo = $arquivo;
		$this->data_inicial = $data_inicial;
		$this->data_final = $data_final;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build ()
	{
		return $this
			->subject('Pedidos Decryptados de ' . $this->data_inicial . ' até ' . $this->data_final)
			->attach($this->arquivo)
			->view('email.magento.consumer.pedidosdecriptados')
		;
	}
}
