<?php

namespace App\Mail\Magento\Corp;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelatorioPedidosClaro extends Mailable
{
	use Queueable, SerializesModels;

	public $arquivo;
	public $config;

	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct ($arquivo, $config)
	{
		$this->arquivo = $arquivo;
		$this->config = $config;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build ()
	{
		return $this
			->subject('Relatório Pedidos Claro')
			->attach($this->arquivo)
			->view('email.magento.corp.relatoriopedidosclaro')
			->with([
				'fileDelivered'		=> $this->config['file_delivered'],
				'errorMessage'		=> $this->config['error_message'],
			]);
		;
	}
}
