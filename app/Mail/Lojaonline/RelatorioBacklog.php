<?php

namespace App\Mail\Lojaonline;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelatorioBacklog extends Mailable
{
	use Queueable, SerializesModels;

	public $arquivo;

	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct ($arquivo)
	{
		$this->arquivo = $arquivo;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build ()
	{
		return $this
			->subject('Backlog da Loja Online')
			->view('email.lojaonline.relatoriobacklog')
			->attach($this->arquivo)
		;
	}
}
