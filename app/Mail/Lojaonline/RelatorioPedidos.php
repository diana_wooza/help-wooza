<?php

namespace App\Mail\Lojaonline;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelatorioPedidos extends Mailable
{
	use Queueable, SerializesModels;

	public $arquivo;

	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct ($arquivo)
	{
		$this->arquivo = $arquivo;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build ()
	{
		return $this
			->subject('Relatório Pedidos (BOGOS / NETSALES)')
			->view('email.lojaonline.relatoriopedidos')
			->attach($this->arquivo)
		;
	}
}
