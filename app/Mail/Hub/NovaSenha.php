<?php

namespace App\Mail\Hub;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NovaSenha extends Mailable
{
	use Queueable, SerializesModels;

	public $url;
	public $user;

	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct ($user, $url)
	{
		$this->user = $user;
		$this->url = $url;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build ()
	{
		return $this
			#->from('cdgerenciadeserial@gmail.com', 'cd_gerenciadeserial')
			->subject('Cadastro de Senha')
			->view('hub.email.novasenha')
		;
	}
}
