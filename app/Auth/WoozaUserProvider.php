<?php

namespace App\Auth;

use \App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class WoozaUserProvider implements UserProvider
{
	/**
	 * Retrieve a user by their unique identifier.
	 * @param  mixed $identifier
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveById ($identifier) : ?Authenticatable
	{
		#TODO: implementar a regra de validação de status do usuário antes de retornar
		return User::find($identifier);
	}

	/**
	 * Retrieve a user by by their unique identifier and "remember me" token.
	 * @param  mixed $identifier
	 * @param  string $token
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByToken ($identifier, $token) : ?Authenticatable
	{
		#TODO: implementar a regra de validação de status do usuário antes de retornar
		return User::find($identifier);
	}

	/**
	 * Update the "remember me" token for the given user in storage.
	 * @param  \Illuminate\Contracts\Auth\Authenticatable $user
	 * @param  string $token
	 * @return void
	 */
	public function updateRememberToken (Authenticatable $user, $token) : void
	{
		//Não implementado
	}

	/**
	 * Retrieve a user by the given credentials.
	 * @param  array $credentials
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByCredentials (array $credentials) : ?User
	{
		return $this->Login($credentials['username'], $credentials['password']);
	}

	/**
	 * Validate a user against the given credentials.
	 * @param  \Illuminate\Contracts\Auth\Authenticatable $user
	 * @param  array $credentials
	 * @return bool
	 */
	public function validateCredentials (Authenticatable $user, array $credentials) : bool
	{
		return !is_null($this->Login($credentials['username'], $credentials['password']));
	}

	/**
	 * Login hibrido da aplicação
	 * Primeiro verifica se o usuário possui senha cadastrada, caso sim ele loga usando essa senha
	 * Caso o usuário não possua senha cadastrada ele tenta logar usando o active directory
	 * @method Login
	 * @param string $username
	 * @param string $password
	 * @return ?User
	 */
	private function Login ($username, $password) : ?User
	{
		$user = \App\Models\User
			::where('login', $username)
			->orWhereRaw('CONCAT(login, \'@wooza.com.br\') = ?', $username)
			->orWhereRaw('dados->"$.email"', $username)
			->first()
		;

		if (is_null($user))
		{
			$user = new User();

			if (\preg_match('/(.*)@.*/', $username, $match))
			{
				$user->login = $match[1];
				$user->email = $username;
			}
			else
			{
				$user->login = $username;
			}
		}

		if ($user->isMyPassword($password))
			return $user;

		if (!$this->Ldap($user->login, $password))
			throw new \Exception('Nome de usuário ou senha incorretos');

		try
		{
			if (!$user->exists)
				$user->save();
		}
		catch (\Throwable $throwable)
		{
			throw new \Exception('O usuário não pode ser criado. Tente novamente e se o erro persisir entre em contato com a administração');
		}

		return $user;
	}

	/**
	 * Validate user using LDAP connection
	 * @param string $username Active Directory username
	 * @param string $password Active Directory password
	 * @return \App\Models\User
	 */
	private function Ldap ($username, $password) : bool
	{
		#tenta conectar ao servdor
		$conexao = \ldap_connect(config('ldap.server'), config('ldap.port'));
		
		#verifica se a conexão foi um sucesso
		if (!$conexao)
			throw new \Exception("Ocorreu um erro ao conectar ao servidor", 1);

		#estas opções são necessárias para realizar o filtro e pegar os outros
		\ldap_set_option($conexao, LDAP_OPT_PROTOCOL_VERSION, 3);
		\ldap_set_option($conexao, LDAP_OPT_REFERRALS, 0);

		#tenta logar com o usuário
		try
		{
			$bind = \ldap_bind($conexao, $username . config('ldap.domain'), $password);
		}
		catch (\Exception $exception)
		{
			throw new \Exception("Falha ao efetuar o login. Nome de usuário ou senha incorretos", 1);
		}

		// //Connect to LDAP
		// $result = ldap_search($conexao, config('ldap.search'), '(|(objectCategory=person)(objectCategory=contact))');
		// $entries = ldap_get_entries($conexao, $result);
		// $data = [];

		// for ($i = 0; $i < $entries['count']; $i++)
		// {
		// 	$this->getLdapAttribute($entries[$i], 'samaccountname', $data['accountname']);
		// 	$this->getLdapAttribute($entries[$i], 'sn', $data['firstname']);
		// 	$this->getLdapAttribute($entries[$i], 'givenname', $data['lastname']);
		// 	$this->getLdapAttribute($entries[$i], 'company', $data['company']);
		// 	$this->getLdapAttribute($entries[$i], 'department', $data['department']);
		// 	$this->getLdapAttribute($entries[$i], 'title', $data['job_title']);
		// 	$this->getLdapAttribute($entries[$i], 'mail', $data['email']);
		// 	$this->getLdapAttribute($entries[$i], 'telephonenumber', $data['phone_1']);
		// 	$this->getLdapAttribute($entries[$i], 'mobile', $data['phone_2']);
		// 	$this->getLdapAttribute($entries[$i], 'ipphone', $data['phone_3']);
		// 	$this->getLdapAttribute($entries[$i], 'homephone', $data['phone_4']);
		// 	$this->getLdapAttribute($entries[$i], 'facsimiletelephonenumber', $data['phone_5']);
		// }

		// dd($data);

		#verifica se o login foi um sucesso
		\ldap_unbind($conexao);
		return (boolean) $bind;
	}

	// private function getLdapAttribute ($data, $attribute, &$value)
	// {
	// 	if (empty($data[$attribute][0]) || empty($data[$attribute][0]) == 'NULL')
	// 		return;

	// 	$value = $data[$attribute][0];
	// }
}