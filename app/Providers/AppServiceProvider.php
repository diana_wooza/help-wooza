<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	* Bootstrap any application services.
	*
	* @return void
	*/
	public function boot()
	{
		#Definindo ajustes de componentes
		\Blade::component('components.message', 'alerta');
		\Menu::DefinirMenuBasico();
	}

	/**
	* Register any application services.
	*
	* @return void
	*/
	public function register()
	{
		//
	}
}
