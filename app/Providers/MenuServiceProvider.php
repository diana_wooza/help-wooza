<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
	private static $groups = [];
	private static $itensByKey = [];

	public function register ()
	{
		$this->app->singleton('Menu', self::class);
	}

	public static function get ()
	{
		return self::$groups;
	}

	public static function DefinirMenuBasico ()
	{
		$contestadorPedidos = [];

		foreach (\App\Models\Contestador\Pedido::$segmentos as $key => $value)
			$contestadorPedidos[] = self::item($value, ['contestador.marcacao.operador','contestador.admin'], 'fa fa-desktop')->route('contestador.pedido.listar',['segmento[]'=>$key]);

		self::criarGrupo(config('app.company') . ' ' . config('app.name'))->adicionarItem(
			self::item('Início', null, 'fa fa-home')->route('home'),
			self::item('Gerenciar', 'hub.admin', 'fa fa-cogs')->addChildren(
				self::item('Usuários', null, 'fa fa-users')->route('hub.usuario.lista'),
				#self::item('Perfís', null, 'fa fa-object-group')->route('hub.perfil.lista'),
				self::item('Configurações Gerais', null, 'fa fa-cog')->route('hub.config')
			)
		);

		self::criarGrupo('Magento', ['hub.admin','magento.dia_vencimento'])->adicionarItem(
			self::item('Alterar Dia Vencimento', 'magento.dia_vencimento', 'fa fa-list')->route('magento.dia_vencimento'),
			self::item('Atualizar Nomenclatura', 'magento.nomenclaturasap', 'fa fa-list')->route('magento.nomenclaturasap'),
			self::item('Status Motivo', 'hub.admin', 'fa fa-list')->route('magento.motivos.listar'),
			self::item('Gerenciar Bases', 'hub.admin', 'fa fa-map')->route('magento.adminuser.listar'),
			self::item('Pedidos Magento', 'hub.admin', 'fa fa-list')->route('magento.pedido.listar')
		);

		self::criarGrupo('Catálogo', 'catalogo')->adicionarItem(
			
			self::item('Analisar Bundle', null, 'fa fa-search')->route('catalogo.contingencia.analisador')
			
		);

		self::criarGrupo('Contestador', 'contestador')->adicionarItem(
			
			self::item('Pedidos', null, 'fa fa-list-ol')->addChildren(...$contestadorPedidos),
			self::item('Receitas', ['contestador.marcacao.operador','contestador.marcacao.editor','contestador.admin'], 'fa fa-list')->route('contestador.receita.listar'),
			self::item('Competências', ['contestador.marcacao.editor','contestador.admin'], 'fa fa-calendar-check-o')->route('contestador.competencia.listar'),
			
			self::item('Regras de Marcação', ['contestador.marcacao.operador', 'contestador.marcacao.editor','contestador.admin'], 'fa fa-industry')->addChildren(
				self::item('Arquivos de Serviço/Churn', ['contestador.marcacao.operador', 'contestador.marcacao.editor','contestador.admin'], 'fa fa-file-excel-o')->route('contestador.arquivo.listar'),
				self::item('Regras de Marcação', ['contestador.marcacao.editor','contestador.admin'], 'fa fa-database')->route('contestador.regra.marcacao.listar')
				// self::item('Regras de Apuração de Receita', null, 'fa fa-newspaper-o')->route('contestador.regra.listar.apuracaoreceita'),
				// self::item('Tabelas de Remuneração', null, 'fa fa-list-alt')->route('contestador.remuneracao.listar')
			),
			
			// self::item('Comissionamento', ['contestador.comissionamento.editor','contestador.admin'], 'fa fa-money')->addChildren(
			// 	self::item('Parâmetros de Comissão', null, 'fa fa-handshake-o')->route('contestador.regra.listar.comissao'),
			// 	self::item('Metas', null, 'fa fa-bar-chart')->route('contestador.meta.listar')
			// ),

			self::item('Lista de Processos', ['contestador'], 'fa fa-stack-overflow')->addChildren(
				self::item('Upload Serviço/Churn', null, 'fa fa-list-ol')->route('contestador.processo.listar', ['tipo' => \App\Models\Contestador\Processo::TIPO_UPLOAD]),
				self::item('Marcações', null, 'fa fa-list-ol')->route('contestador.processo.listar', ['tipo' => \App\Models\Contestador\Processo::TIPO_MARCACAO]),
				self::item('Exportações em Arquivo', null, 'fa fa-list-ol')->route('contestador.processo.listar', ['tipo' => \App\Models\Contestador\Processo::TIPO_DOWNLOAD])
			),

			self::item('Gerenciar Contestador', ['contestador.admin'], 'fa fa-cogs')->addChildren(
				self::item('Configurações Gerais', null, 'fa fa-cog')->route('contestador.config'),
				self::item('Usuários', null, 'fa fa-users')->route('hub.usuario.permissionar', ['contestador.admin'])
			)
		);

		self::criarGrupo('Alarmística', 'alarmistica')->adicionarItem(
			self::item('Indicadores', null, 'fa fa-dashboard')->route('alarmistica.api.exibeIndicadoresMenu'),
			self::item('Apis', null, 'fa fa-link')->route('alarmistica.api.listaApis'),
			self::item('Logs', null, 'fa fa-history')->route('alarmistica.api.listaLogs'),
			self::item('Importar Planilha Backlog', null, 'fa fa-cloud-upload')->route('alarmistica.backlog'),
			self::item('Gerenciar Alarmistica', 'alarmistica.admin', 'fa fa-cogs')->addChildren(
				self::item('Ajustar Api', null, 'fa fa-edit')->route('alarmistica.configuracao.ajustar'),
				self::item('Cadastrar Api', null, 'fa fa-plus-square')->route('alarmistica.configuracao.cadastro')
			)
		);

		// self::criarGrupo('Loja Online')->adicionarItem(
		// 	self::item('Pedidos Claro Pós', null, 'fa-cart-arrow-down')->route('lojaonline.pedidos.listar', ['claro_pos']),
		// 	self::item('Pedidos Claro Pré', null, 'fa-cart-arrow-down')->route('lojaonline.pedidos.listar', ['claro_pre']),
		// 	self::item('Pedidos Claro Prospect', null, 'fa-cart-arrow-down')->route('lojaonline.pedidos.listar', ['claro_prospect']),
		// 	self::item('Pedidos Tim', null, 'fa-cart-arrow-down')->route('lojaonline.pedidos.listar', ['claro'])
		// );

		self::criarGrupo('Indicadores', 'indicadores')->adicionarItem(
			self::item('Logística Consolidado')->route('indicadores.logisticaconsolidado'),
			self::item('Gráficos indicadores', 'indicadores.admin', 'fa fa-dashboard')->addChildren(
				self::item('Gráficos', null, 'fa fa-line-chart')->route('indicadores.graficosindicadores.index'),
				self::item('Cadastrar Metas', null, 'fa fa-plus-square')->route('indicadores.graficosindicadores.cadastrarMetas')
				// self::item('', null, 'fa fa-cloud-upload')->route()
			)
		);

		self::criarGrupo('Colmeia', ['colmeia', 'colmeia.serial'], 'ion ion-social-windows')->adicionarItem(
			self::item('Nota Fiscal', ['colmeia'], 'fa fa-file-code-o')->addChildren(
				self::item('Importar Nota Fiscal', ['colmeia'], 'fa fa-upload')->route('colmeia.nfe.importar'),
				self::item('Notas Pendente', ['colmeia'], 'fa fa-folder-o')->route('colmeia.nfe.pendente')
			),
			self::item('Chip', ['colmeia'], 'fa fa-credit-card')->addChildren(
				self::item('Alocar na Colmeia', ['colmeia'], 'fa fa-plus-circle')->route('colmeia.produto.chip.alocar'),
				self::item('Lista de Descarte', ['colmeia'], 'fa fa-trash-o')->route('colmeia.produto.chip.desalocar')
			),
			self::item('Relatórios', ['colmeia', 'colmeia.serial'], 'fa fa-list-alt')->addChildren(
				#self::item('Itens para Devolução')->route('colmeia.relatorio.produto.devolucao'),
				self::item('Posições da Colmeia', ['colmeia'], 'fa fa-list')->route('colmeia.relatorio.posicoes.lista'),
				self::item('Lista de Produtos', ['colmeia'], 'fa fa-list')->route('colmeia.relatorio.produto.lista'),
				self::item('Entradas em Lote', ['colmeia'], 'fa fa-list')->route('colmeia.relatorio.entrada_lote'),
				self::item('Notas Fiscais', ['colmeia'], 'fa fa-list')->route('colmeia.relatorio.nfe.lista'),
				self::item('Histórico de Movimentações', ['colmeia'], 'fa fa-list')->route('colmeia.relatorio.movimentacoes'),
				#self::item('Histórico de Alocações', ['colmeia'], 'fa fa-list')->route('colmeia.relatorio.alocacao.lista'),
				self::item('Seriais Disponíveis', ['colmeia', 'colmeia.serial'], 'fa fa-list')->route('colmeia.relatorio.produto.seriais'),
				self::item('Produtos Magento', 'colmeia.admin', 'fa fa-shopping-cart')->route('colmeia.relatorio.magento.produtos')
				#self::item('Cobertura do Estoque')
			),
			self::item('Gerenciar Colmeia', 'colmeia.admin', 'fa fa-cogs')->addChildren(
				self::item('Atualizar Pedidos Magento', null, 'fa fa-cloud-upload')->route('colmeia.magento.pedido.atualizar'),
				self::item('Armários', null, 'fa fa-building')->route('colmeia.armario.lista'),
				self::item('Jundsoft SKUs', null, 'fa fa-briefcase')->route('colmeia.jundsoft.listar'),
				self::item('Configurações Gerais', null, 'fa fa-cog')->route('colmeia.config'),
				self::item('Usuários', null, 'fa fa-users')->route('hub.usuario.permissionar', ['colmeia.admin']),
				self::item('RegEx para SKU', 'colmeia.admin', 'fa fa-terminal')->route('colmeia.sku.regex')
			)
		);

		self::criarGrupo('Tratativas Automáticas', 'robo.configuracao')->adicionarItem(
			self::item('Configurar Tratativas', 'robo.configuracao', 'fa fa-android')->route('configuracao.tratativas_automaticas'),
			self::item('Visualizar Histórico', 'robo.configuracao', 'fa fa-calendar')->route('configuracao.historico'),
			self::item('Gerenciar', 'hub.admin', 'fa fa-bars')->addChildren(
				self::item('Editar Tratativas', 'hub.admin', 'fa fa-gears')->route('configuracao.lista_tratativas'),
				self::item('Editar Ações', 'hub.admin', 'fa fa-bolt')->route('configuracao.lista_acoes'),
				self::item('Editar Condições Especiais', 'hub.admin', 'fa fa-filter')->route('configuracao.lista_condicoes_especiais'),
				self::item('Editar Campos', 'hub.admin', 'fa fa-wpforms')->route('configuracao.lista_campos')
			)
		);

		self::criarGrupo('Tarefas agendadas', 'cron.configuracao')->adicionarItem(
			self::item('Configurar Crons', 'cron.configuracao', 'fa fa-calendar')->route('cron.lista')
		);

		self::criarGrupo('Reaproveitamento', 'reaproveitamento')->adicionarItem(
			self::item('Agendar reaproveitamento', 'reaproveitamento', 'fa fa-calendar')->route('reaproveitamento.agendar_reaproveitamento')
		);
		
		self::criarGrupo('Atendimento', 'posvenda')->adicionarItem(
			self::item('Registrar Atendimento', 'posvenda', 'fa fa-comments-o')->route('posvenda.registrar_atendimentos'),
			self::item('Listar Atendimentos', 'posvenda', 'fa fa-list')->route('posvenda.listar_atendimentos'),
			self::item('Gerenciar opções', 'posvenda.admin', 'fa fa-bars')->addChildren(
				self::item('Adicionar Opções', 'posvenda.admin', 'fa fa-plus')->route('posvenda.adicionar_opcoes'),
				self::item('Alterar Opções', 'posvenda.admin', 'fa fa-pencil-square-o')->route('posvenda.alterar_opcoes'),
				self::item('Relações Motivos', 'posvenda.admin', 'fa fa-code')->route('posvenda.listar_motivos'),
				self::item('Usuários', null, 'fa fa-users')->route('hub.usuario.permissionar', ['atendimento.admin'])
			)
		);

		self::criarGrupo('Importadores / Exportadores', 'importador')->adicionarItem(
			self::item('Importar Arquivo', ['importador'], 'fa fa-cloud-upload')->route('importador.importar'),
			self::item('Importar Status Pedido', ['importador.statuspedido'], 'fa fa-cloud-upload')->route('importador.statuspedido'),
			self::item('Importar Varejo Loja', ['importador.statuspedido'], 'fa fa-cloud-upload')->route('importador.varejoloja'),
			self::item('Downgrade TIM', ['importador.downgrade'], 'fa fa-cloud-upload')->route('importador.downgradetim'),
			self::item('Downgrade VIVO', ['importador.downgrade'], 'fa fa-cloud-upload')->route('importador.downgradeVivo'),
			self::item('GRID Downgrade Operadoras', ['importador.downgrade'], 'fa fa-table')->route('importador.downgrade-listar'),
			self::item('CPF Vendedor', ['importador.cpfvendedor'], 'fa fa-cloud-upload')->route('importador.cpfvendedor'),
			self::item('Phone Number', ['importador.phonenumber'], 'fa fa-cloud-upload')->route('importador.phonenumber'),
			self::item('Pedido Decriptado', ['importador.decrypt'], 'fa fa-cloud-download')->route('importador.decrypt')
		);

		self::criarGrupo('Portal', 'portal')->adicionarItem(
			self::item('Busca de Pedidos', 'portal', 'fa fa-table')->route('portal.grid_portal')
			// self::item('Alterar senha', 'portal', 'fa fa-table')->route('portal.alterar_senha_portal')
		);
	}

	public static function criarGrupo (string $rotulo, $permissao = null, $icone = null)
	{
		$group = new class($rotulo, $permissao, $icone)
		{
			private $rotulo;
			private $permissao;
			private $icone;
			private $itens = [];

			public function __construct (string $rotulo, $permissao = null, $icone = null)
			{
				if (!is_null($permissao) && !is_array($permissao))
					$permissao = [$permissao];

				$this->rotulo = $rotulo;
				$this->permissao = $permissao;
				$this->icone = $icone;
			}

			public function adicionarItem (...$itens)
			{
				foreach ($itens as $item)
				{
					$this->itens[] = $item;
					$item->definirGrupo ($this);
				}

				return $this;
			}

			public function icone ()
			{
				return $this->icone;
			}

			public function rotulo ()
			{
				return $this->rotulo;
			}

			public function hasPermissao ()
			{
				if (is_null($this->permissao) && !\Auth::guard('portal')->check())
					return true;
				
				if (\Auth::guard('portal')->check() && $this->permissao[0] == 'portal') {
					return true;
				}
					
				if (!\Auth::check())
					return false;
					
				if (!empty($this->permissao)) {
					foreach ($this->permissao as $permissao)
					{
						if (\Auth::user()->checkPermissao($permissao))
							return true;
					}
				}
			}

			public function itens ()
			{
				return $this->itens;
			}
		};

		self::$groups[] = $group;

		return $group;
	}

	public static function item (?string $rotulo = null, $permissao = null, ?string $icone = 'fa fa-circle-o')
	{
		return new class($rotulo, $permissao, $icone)
		{
			private $rotulo;
			private $route;
			private $icone;

			private $permissao;
			private $group;
			private $parent;
			private $children = [];

			public function __construct (?string $rotulo = null, $permissao = null, ?string $icone = 'fa fa-circle-o')
			{
				if (!is_null($permissao) && !is_array($permissao))
					$permissao = [$permissao];

				$this->rotulo = $rotulo;
				$this->icone = $icone;
				$this->permissao = $permissao;
				$this->route = (object) ['type' => 'url', 'value' => '#'];
			}

			public function getSubitens ()
			{
				return $this->children;
			}

			public function rotulo ()
			{
				return $this->rotulo;
			}

			public function icone ()
			{
				return 'fa ' . $this->icone;
			}

			public function link ()
			{
				if ($this->route->type == 'url')
					return url($this->route->value);

				return route($this->route->value, $this->route->params);
			}

			public function hasSubitens ()
			{
				return count($this->children) > 0;
			}

			public function printiFActive (string $stringToPrint = null) : string
			{
				return $this->checkRouteHierarquia() ? $stringToPrint : '';
			}

			public function hasPermissao ()
			{
				if (is_null($this->permissao))
					return true;

				if (\Auth::guard('portal')->check() && $this->permissao[0] == 'portal') {
					return true;
				}
					
				if (!\Auth::check())
					return false;
					
				foreach ($this->permissao as $permissao)
				{
					if (\Auth::user()->checkPermissao($permissao))
						return true;
				}
			}

			public function setParent ($parent)
			{
				$this->parent = $parent;
				$parent->addChildren($this);

				return $this;
			}

			public function addChildren (self ...$children)
			{
				foreach ($children as $child)
				{
					if (in_array($child, $this->children))
						return $this;
					
					$this->children[] = $child;
					$child->setParent($this);
				}

				return $this;
			}

			public function definirGrupo ($group)
			{
				$this->group = $group;
			}

			public function route (string $name, array $params = [])
			{
				$this->route = (object) ['type' => 'route', 'value' => $name, 'params' => $params];

				return $this;
			}

			private function checkRouteHierarquia () : bool
			{
				if ($this->checkRoute())
					return true;

				foreach ($this->children as $child)
				{
					if ($child->checkRouteHierarquia())
						return true;
				}

				return false;
			}

			private function checkRoute () : bool
			{
				if ($this->route->type == 'route' && $this->route->value == \Route::currentRouteName())
					return true;

				if ($this->route->type == 'url')
				{
					$urlSegments = explode('/', $this->route->value);

					for ($n = 0; $n < count($urlSegments); $n++)
					{
						if ($urlSegments[$n] != \Request::segment($n))
							return false;
					}

					return true;
				}

				return false;
			}
		};
	}
}