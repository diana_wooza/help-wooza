<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Builder;


//\App\Helpers\QueryBuilder::ToSqlWithBindings()
class QueryBuilder
{
	public static function ToSqlWithBindings (Builder $builder)
	{
		return vsprintf(str_replace('?', '%s', str_replace('?', "'?'", $builder->toSql())), $builder->getBindings());
	}
}