<?php

namespace App\Helpers;

use \App\Helpers\Utils;

/**
 * Enviar mensagem de sms
 * Instancie a classe, popule os itens e envie a mensagem
**/
class Sms
{
	#propriedades do SMS
	private $ddd;
	private $numero;
	private $mensagem;
	private $sistema;
	private $operadora;
	private $modalidade;
	private $ip;

	/**
	 * Construtor da classe
	 */
	public function __construct ()
	{
		$this->valoresDefault();
	}

	/**
	 * Define o número com DDD para quem a mensagem SMS será enviada
	 * @param string numero do telefone com ddd (Ex: 21988888888)
	 * @return Sms
	 */
	public function setNumero (string $numero) : Sms
	{
		#remove traço e espaços do número
		$numero = preg_replace('/[\s-]+/', '', $numero);

		#Adiciona o DDD e o número como itens separados
		$this->ddd		= substr($numero, 1, 2);
		$this->numero	= substr($numero, 3);

		return $this;
	}

	/**
	 * Define a mensagem que será enviada via SMS
	 * @param string mensagem a ser enviada
	 * @return Sms
	 */
	public function setMensagem (string $mensagem) : Sms
	{
		$this->mensagem = mb_strtolower(Utils::removeAcentuacao($mensagem));

		return $this;
	}

	/**
	 * Define a operadora a ser utilizada. Existe uma lista específica de operadoras disponíveis
	 * Os valores possíveis são: Vivo, Claro, Tim, Oi, Nextel
	 * @param string $operadora nome da operadora
	 * @return Sms
	 */
	public function setOperadora (string $operadora) : Sms
	{
		#Converte o parâmetro enviado para que consiga dar match nos índices da lista de operadora porque estão todos com letras minúsculas
		$operadora = mb_strtolower($operadora);

		#TODO: Verificar operadora válida
		if (Utils::verificaOperadoraValida($operadora))
			$this->operadora = $operadora;

		return $this;
	}

	/**
	 * Define a modalidade
	 * @param string modalidade
	 * @return Sms
	 */
	public function setModalidade (string $modalidade) : Sms
	{
		$modalidade = mb_strtolower($modalidade);

		#nova linha é caracterizada como a string "nova linha" em algumas situações. Ao enviar o sms isso precisa ser corrigido
		if ($modalidade == 'nova linha')
		{
			$modalidade = 'novalinha';
		}

		#adiciona a versão com letras minúsculas e não acentuada da mensagem
		$this->modalidade = Utils::removeAcentuacao($modalidade);

		return $this;
	}

	/**
	 * Define o sistema d eenvio
	 * @param string sistema
	 * @return Sms
	 */
	 public function setSistema (string $sistema) : Sms
	 {
		 $sistema = mb_strtolower($sistema);
 
		 $this->sistema = Utils::removeAcentuacao($sistema);
 
		 return $this;
	 }

	/**
	 * Define o IP da pessoa que enviou a mensagem
	 * @param string #ipAddress endereço de IP
	 * @return Sms
	 */
	public function setIp (string $ipAddress) : Sms
	{
		$this->ip = $ipAddress;

		return $this;
	}

	/**
	 * Envia a mensagem sms usando nossa API
	 * @return void
	 */
	public function enviar () : void
	{
		#Valida para ver se todos os campos foram preenchidos
		if (!Utils::verificaSeTodosPreenchidos($this->ddd, $this->numero, $this->mensagem, $this->sistema, $this->operadora, $this->ip))
		{
			\Log::channel('sms')->warning('Tentando enviar SMS sem especificar todos os parâmetros: ' . json_encode($this));
			throw new \Exception('Tentando enviar SMS sem especificar todos os parâmetros');
		}

		$parametros = [
			'Ddd'			=> $this->ddd,
			'Telefone'		=> $this->numero,
			'Mensagem'		=> $this->mensagem,
			'Sistema'		=> $this->sistema,
			'Operadora'		=> $this->operadora,
			'Modalidade'	=> $this->modalidade,
			"AddressIp"		=> $this->ip
		];
		
		#cria, parametreriza a requisição para a API
		$curlService = \curl_init(config('wooza.cubo.host') . '/v1/api/sms/enviar/token/gateway/avi/especifico');
		\curl_setopt_array($curlService, [
			CURLOPT_POSTFIELDS		=> \json_encode($parametros, JSON_PRETTY_PRINT),
			CURLOPT_RETURNTRANSFER	=> 1,
			CURLOPT_HTTPHEADER		=> [
				'Content-Type: application/json',
				'Cache-Control: no-cache',
				'Authorization: ' . config('wooza.cubo.key'),
				'CodigoOperadora: '. Utils::getCodigoOperadora($this->operadora)
			],
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		#Envia a requisição, pega a resposta e finaliza a conexão
		$respostaApi = \curl_exec($curlService);
		$codigoRespostaHttp = \curl_getinfo($curlService, CURLINFO_HTTP_CODE);
		\curl_close($curlService);

		\Log::channel('sms')->info('SMS Enviado: ' . PHP_EOL . json_encode(['parametros' => $parametros, 'codigo' => $codigoRespostaHttp, 'resposta' => json_decode($respostaApi)]));

		#se a resposta não foi sucesso joga uma excessão informando o erro
		if ($codigoRespostaHttp != '200')
		{
			\Log::channel('sms')->error('Ocorreu um erro ao enviar o SMS. Servidor retornou erro: ' . $respostaApi);
			throw new \Exception('Ocorreu um erro ao enviar o SMS');
		}

		#TODO: fazer o log da resposta enviada pela api

		// echo '<pre>';
		// print_r($this);
		// exit;
	}

	/**
	 * Limpa todos os campos para mandar uma nova mensagem
	 * @return Sms
	 */
	public function Limpar () : Sms
	{
		$this->valoresDefault();

		return this;
	}

	/**
	 * Envia mensagem rápida
	 * @method Send
	 * @param string $numero
	 * @param string $mensagem
	 * @return {void}
	 */
	public static function Send ($numero, $mensagem)
	{
		$sms = new Sms();
		$sms->setNumero($numero);
		$sms->setMensagem($mensagem);
		$sms->setIp(\Request::server('SERVER_ADDR'));
		
		return $sms->Enviar();
	}

	/**
	 * Envia uma mensagem símples utilizando o broker padrão
	 * @method Simples
	 * @param string $numero
	 * @param string $mensagem
	 * @return void
	 */
	public static function Simples (string $numero, string $mensagem)
	{
		$sms = new self();
		$sms->setNumero($numero);
		$sms->setMensagem($mensagem);
		$sms->setOperadora('tim');
		$sms->setModalidade('portabilidade');
		$sms->setSistema('vendas.onlinetim');
		$sms->setIp('10.17.4.4');
		$sms->Enviar();
	}

	/**
	 * Modifica todos os valores da classe para o valor padrão
	 * @return void
	 */
	private function valoresDefault () : void
	{
		$this->ddd			= null;
		$this->numero		= null;
		$this->mensagem		= null;
		$this->ip			= null;
		#$this->sistema		= 'mgt.prd';
		#$this->operadora	= null;
		#$this->modalidade	= null;
		#$this->modalidade	= 'novalinha';
		
		$this->sistema		= 'vendas.onlinetim';
		$this->operadora	= 'tim';
		$this->modalidade	= 'portabilidade';
	}	
}