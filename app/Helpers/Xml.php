<?php

namespace App\Helpers;

class Xml
{
	/**
	 * Classe não instanciável
	 */
	private function __construct ()
	{

	}
	
	public static function toJson ($xmlData)
	{
		return \json_encode(\simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA));
	}

	public static function toObject ($xmlData)
	{
		return \json_decode(self::toJson($xmlData));
	}
}