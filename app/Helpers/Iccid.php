<?php

namespace App\Helpers;

/**
 * Enviar mensagem de sms
 * Instancie a classe, popule os itens e envie a mensagem
**/
class Iccid
{
	private static $sumTable = [
		[0,1,2,3,4,5,6,7,8,9],
		[0,2,4,6,8,1,3,5,7,9]
	];

	public static function IsSequencia ($inicial, $final) : bool
	{
		if (!self::Verify($inicial) || !self::Verify($final))
			return false;

		$inicial = self::Split($inicial);
		$final = self::Split($final);

		return $final[1] > $inicial[1];
	}

	public static function Lista ($inicial, $final)
	{
		if (!self::Verify($inicial) || !self::Verify($final))
			throw new \Exception('Os valores são inválidos');

		$inicial = self::Split($inicial);
		$final = self::Split($final);

		$base = $inicial[0];
		$itens = $final[1] - $inicial[1];

		$iccids = [];

		for ($n = $inicial[1], $t = $n + $final[1] - $inicial[1]; $n <= $t; $n++)
		{
			$iccid = $base . substr('000000000' . $n, -9);
			$digit = self::Digit($iccid);
			
			$iccids[] = $iccid . $digit;
		}

		return $iccids;
	}

	public static function Verify ($iccid)
	{
		$pattern = "/^(89)(1)(48[0-9])(\d{13})(\d)$/";

		return preg_match($pattern, $iccid) !== false && self::isValidLuhn($iccid);
	}

	public static function isValidLuhn ($number)
	{
		// validate luhn checksum
		settype($number, 'string');

		$sum = 0;
		$flip = 0;
		
		for ($i = strlen($number) - 1; $i >= 0; $i--)
			$sum += self::$sumTable[$flip++ & 0x1][$number[$i]];

		return $sum % 10 === 0;
	}

	private static function Digit ($iccid)
	{
		$length = strlen($iccid);
		$parity = ($length + 1) % 2;
		$checksum = 0;
		
		for ($i = $length-1; $i >= 0; $i--)
		{
			$luhnDigit = intVal($iccid[$i]);

			if ($i % 2 == $parity)
				$luhnDigit *= 2;

			if ($luhnDigit > 9)
				$luhnDigit -= 9;
			
			$checksum += $luhnDigit;
		}

		$checksum = $checksum % 10;

		return ($checksum == 0) ? 0 : 10 - $checksum;
	}

	private static function Split ($iccid)
	{
		return [substr($iccid, 0, 10), substr($iccid, 10, 9)];
	}
}