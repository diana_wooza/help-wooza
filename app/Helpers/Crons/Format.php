<?php

namespace App\Helpers\Crons;

class Format {

    public function formatarExibicaoAgendamento($agendamento, $a=false) {
        if (strstr($agendamento, "/")) {
            $tempo = explode("/", $agendamento);
            return "A cada ".$tempo[1];
        }
        return str_replace("*", ($a) ? "Todas" : "Todos", $agendamento);
    }

}