<?php

namespace App\Helpers\Tim;

/**
 * Enviar mensagem de sms
 * Instancie a classe, popule os itens e envie a mensagem
**/
class Robo
{
	// private static $apiUrl = env('ROBO_TIM_API', 'http://tim-bot.wooza.com.br:7080/api');
	private static $apiUrl = env('ROBO_TIM_API', 'cygnus:7080/api');

	private function __construct ()
	{
		
	}

	/**
	 * Faz a consulta da linha na TIM de modo síncronno e retorna o resultado
	 * @method ConsultaLinha
	 * @param array $data
	 * 	[
	 * 		'Celular'				=> string,
	 *		'CEP' 					=> string,
	 *		'DDD'					=> string,
	 *		'Estado'				=> string,
	 *		'Cidade'				=> string,
	 *		'RuaNumeroComplBairro'	=> string,
	 *		'NomeCompleto'			=> string,
	 *		'TelefoneContato'		=> string,
	 *		'CPF'					=> string,
	 *		'Email'					=> string,
	 *		'DataNascimento'		=> string,
	 *		'NomeCompletoMae'		=> string,
	 *		'Sexo'					=> string)
	 * 	]
	 * @return object
	**/
	public static function ConsultaLinhaPos (array $data)
	{
		$curlService = \curl_init(self::$apiUrl . '/RobotAction/TIM/ConsultaLinhaVendas');
		\curl_setopt_array($curlService, [
			CURLOPT_POSTFIELDS		=> \json_encode($data, JSON_PRETTY_PRINT),
			CURLOPT_RETURNTRANSFER	=> 1,
			CURLOPT_HTTPHEADER		=> [
				'Content-Type: application/json',
			],
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		$consulta = \json_decode(\curl_exec($curlService));
		\curl_close($curlService);

		return @$consulta->ResponseData;
	}

	/**
	 * Faz a consulta da linha na TIM de modo síncronno e retorna o resultado
	 * @method ConsultaLinha
	 * @param array $data
	 * 	[
	 * 		'Celular'				=> string,
	 *		'CEP' 					=> string,
	 *		'DDD'					=> string,
	 *		'Estado'				=> string,
	 *		'Cidade'				=> string,
	 *		'RuaNumeroComplBairro'	=> string,
	 *		'NomeCompleto'			=> string,
	 *		'TelefoneContato'		=> string,
	 *		'CPF'					=> string,
	 *		'Email'					=> string,
	 *		'DataNascimento'		=> string,
	 *		'NomeCompletoMae'		=> string,
	 *		'Sexo'					=> string)
	 * 	]
	 * @return object
	**/
	// public static function ConsultaLinhaPre (array $data)
	// {
	// 	$curlService = \curl_init(self::$apiUrl . '/RobotAction/TIM/ConsultaLinhaVendas');
	// 	\curl_setopt_array($curlService, [
	// 		CURLOPT_POSTFIELDS		=> \json_encode($data, JSON_PRETTY_PRINT),
	// 		CURLOPT_RETURNTRANSFER	=> 1,
	// 		CURLOPT_HTTPHEADER		=> [
	// 			'Content-Type: application/json',
	// 		],
	// 		CURLOPT_SSL_VERIFYHOST	=> false,
	// 		CURLOPT_SSL_VERIFYPEER	=> false,
	// 	]);

	// 	$consulta = \json_decode(\curl_exec($curlService));
	// 	\curl_close($curlService);

	// 	return @$consulta->ResponseData;
	// }
}