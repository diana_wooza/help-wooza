<?php

namespace App\Helpers\Tim;

/**
 * Enviar mensagem de sms
 * Instancie a classe, popule os itens e envie a mensagem
**/
class ApiOrdem
{
	const ELEGIBILIDADE_SEGMENTO_CONTROLE			= 'CONTROLE';
	const ELEGIBILIDADE_SEGMENTO_POS				= 'POS_PAGO';

	private function __construct ()
	{
		
	}

	/**
	 * Faz a consulta de elegibilidade e retorna o resultado
	 * @method ConsultaElegibilidade
	 * @param array $data
	 * 	[
	 * 		'NomenclaturaSapPlano'	=> string
	 * 		'PdvCustCode'			=> string
	 * 		'PdvStateCode'			=> string
	 * 		'Cpf'					=> string
	 * 		'Nome'					=> string
	 * 		'NomeMae'				=> string
	 * 		'Msisdn'				=> integer
	 * 		'DataNascimento'		=> string
	 * 		'Cep'					=> string
	 * 		'PlanSegment'			=> string
	 * 	]
	 * @return object
	**/
	public static function ConsultaElegibilidade (array $data)
	{
		$curlService = self::_ConsultaElegibilidade_cubo($data);

		$elegibilidade = \json_decode(\curl_exec($curlService));
		\curl_close($curlService);

		return $elegibilidade;
	}

	private static function _ConsultaElegibilidade_cubo (array $data)
	{
		$curlService = \curl_init(config('wooza.cubo.host') . '/v1/api/varejo/tim/verificar/elegibilidade/fatura');
		\curl_setopt_array($curlService, [
			CURLOPT_POSTFIELDS		=> \json_encode($data, JSON_PRETTY_PRINT),
			CURLOPT_RETURNTRANSFER	=> 1,
			CURLOPT_HTTPHEADER		=> [
				'Content-Type: application/json',
				'Authorization: ' . config('wooza.cubo.key-elegibilidade'),
			],
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		return $curlService;
	}

	private static function _ConsultaElegibilidade_tim (array $data, string $tokenLojista = null)
	{
		if (is_null($tokenLojista))
			$tokenLojista = self::GetTokenLojista()->retorno->access_token;

		$data = [
			'pdv'			=> [
				'custCode'		=> $data['PdvCustCode'],
				'stateCode'		=> $data['PdvStateCode'],
			],
			'customer'		=> [
				'socialSecNo'	=> $data['Cpf'],
				'name'			=> $data['Nome'],
				'motherName'	=> $data['NomeMae'],
				'birthDate'		=> $data['DataNascimento'],
				'address'		=> [
					'postalCode'	=> $data['Cep'],
				]
			],
			'newContract'	=> [
				'ddd'			=> $data['Msisdn'],
			],
			'plan'			=> [
				'segment'		=> $data['PlanSegment']
			]
		];

		$curlService = \curl_init(config('wooza.tim.host') . '/b2b2c/v3/eligibility');
		\curl_setopt_array($curlService, [
			CURLOPT_POSTFIELDS		=> \json_encode($data),
			CURLOPT_RETURNTRANSFER	=> 1,
			CURLOPT_HTTPHEADER		=> [
				'Accept: */*',
				'Accept-Encoding: gzip, deflate',
				'Authorization: Bearer ' . $tokenLojista,
				'Connection: keep-alive',
				'Content-Type: application/json',
				'Host: ' . config('wooza.tim.host'),
			],
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		return $curlService;
	}

	public static function Ativar (array $data, $tokenVendedor)
	{
		$curlService = \curl_init(config('wooza.tim.host') . '/b2b2c/v3/order');
		\curl_setopt_array($curlService, [
			CURLOPT_POSTFIELDS		=> \json_encode($data, JSON_PRETTY_PRINT),
			CURLOPT_RETURNTRANSFER	=> 1,
			CURLOPT_HTTPHEADER		=> [
				'Content-Type: application/json',
				'Authorization: Bearer ' . $tokenVendedor,
			],
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		$envio = \json_decode(\curl_exec($curlService));
		\curl_close($curlService);

		return $envio;
	}

	public static function GetTokenLojista ()
	{
		$curlService = \curl_init(config('wooza.cubo.host') . '/v1/api/varejo/tim/autenticar/lojista');
		\curl_setopt_array($curlService, [
			CURLOPT_POSTFIELDS		=> \json_encode(['grant_type'=>'client_credentials', 'scope'=>'TIMVarejoRede.rede'], JSON_PRETTY_PRINT),
			CURLOPT_RETURNTRANSFER	=> 1,
			CURLOPT_HTTPHEADER		=> [
				'Authorization: ' . config('wooza.cubo.key'),
				'clientId: Wooza',
				'clientSecret: nSTvOd38NgUE',
				'CodigoOperadora: 41',
				'Content-Type: application/json',
			],
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		$token = \json_decode(\curl_exec($curlService));
		\curl_close($curlService);

		return $token;
	}

	public static function ConsultaProtocolo ($protocolo, $tokenVendedor)
	{
		$curlService = \curl_init(config('wooza.tim.host') . '/b2b2c/v1/order/' . $protocolo);
		\curl_setopt_array($curlService, [
			CURLOPT_POSTFIELDS		=> \json_encode(['grant_type'=>'client_credentials', 'scope'=>'TIMVarejoRede.rede'], JSON_PRETTY_PRINT),
			CURLOPT_RETURNTRANSFER	=> 1,
			CURLOPT_HTTPHEADER		=> [
				'Content-Type: application/json',
				'Authorization: Bearer ' . $tokenVendedor,
			],
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		$protocolo = \json_decode(\curl_exec($curlService));
		\curl_close($curlService);

		return $protocolo;
	}

	public static function ConsultaEndereco (string $cep, ?string $token = null)
	{
		if (is_null($token))
			$token = @self::GetTokenLojista()->retorno->access_token;

		$curlService = \curl_init(config('wooza.tim.host') . '/b2b2c/v1/address/' . $cep);
		\curl_setopt_array($curlService, [
			CURLOPT_CUSTOMREQUEST	=> 'GET',
			CURLOPT_RETURNTRANSFER	=> 1,
			CURLOPT_HTTPHEADER		=> [
				'Content-Type: application/json',
				'Authorization: Bearer ' . $token,
			],
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
			CURLOPT_FOLLOWLOCATION	=> true,
		]);

		$endereco = \json_decode(\curl_exec($curlService));
		\curl_close($curlService);

		return $endereco;
	}
}