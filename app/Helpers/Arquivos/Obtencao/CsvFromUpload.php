<?php

namespace App\Helpers\Arquivos\Obtencao;

use \App\Helpers\Arquivos\Exceptions\RejectFile;
use \Maatwebsite\Excel\Facades\Excel;

final class CsvFromUpload extends _Base
{
	private function __construct ()
	{

	}
	
	public static function info ()
	{
		
	}

	public static function Obter ($configuracao, ?\Illuminate\Http\Request $request) :?array
	{
		if (!isset($configuracao->input) || empty($configuracao->input))
			throw new RejectFile('Configuração de input que irá enviar o arquivo não foi efetuada');

		if (!$request->hasFile($configuracao->input))
			throw new RejectFile('Não foi possível prosseguir com a importação. Arquivo não reconhecido');

		\Config::set('excel.csv.delimiter', ';');
		return  @Excel::load($request->file($configuracao->input)->getRealPath(), null, 'UTF-8')->toArray();
	}
}