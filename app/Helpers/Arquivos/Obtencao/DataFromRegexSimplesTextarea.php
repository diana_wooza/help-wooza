<?php

namespace App\Helpers\Arquivos\Obtencao;

use \App\Helpers\Arquivos\Exceptions\RejectFile;
use \Maatwebsite\Excel\Facades\Excel;
use \Illuminate\Support\Collection;

final class DataFromRegexSimplesTextarea extends _Base
{
	private function __construct ()
	{

	}

	public static function info ()
	{
		return (object) [
			'tipo'		=> self::class,
			'label'		=> 'Extrair dados de uma área de texto',
			'descricao'	=> 'Extrai os dados com base em uma expressão regular que pega todos os itens que foram enviados em uma área de texto',
			'view'		=> 'despachador.obtencao.simple_textarea',
			'viewData'	=> [
				'input'		=> 'input',
				'label'		=> 'Digite os valores separados um do outro por espaço, vírgula, ponto e vírgula ou quebra de linha',
			],
			'config'	=> [
				(object) ['tipo'=>'hidden', 'campo'=>'input', 'value'=>'input'],
				(object) ['tipo'=>'texto', 'campo'=>'regex', 'label'=>'Expressão de Extração', 'largura'=>6, 'descricao'=>'Expressão regular que irá ser utilizada para extrair as informações necessárias de cada linha'],
				(object) ['tipo'=>'texto', 'campo'=>'field', 'label'=>'Nome do campo', 'largura'=>6, 'descricao'=>'Nome do campo que será utilizado pelas regras'],
			],
		];
	}

	public static function Obter ($configuracao, ?\Illuminate\Http\Request $request) : Collection
	{
		if (!isset($configuracao->input) || empty($configuracao->input))
			throw new RejectFile('Configuração de input que irá enviar o arquivo não foi efetuada');

		if (!$request->isMethod('post') || !$request->has($configuracao->input))
			throw new RejectFile('Não foi possível prosseguir com a importação. Dados não preenchidos');

		if (!preg_match_all('/' . $configuracao->regex . '/', $request->input($configuracao->input), $matches))
			return ['falhou'];

		$collection = collect([[$configuracao->field]]);

		return $collection->merge(array_map(function ($item) {return [$item];}, $matches[1]));
	}
}