<?php

namespace App\Helpers\Arquivos\Obtencao;

use \Illuminate\Support\Collection;

abstract class _Base
{
	public static function info ()
	{
		throw new \Exception('Método não implementado');
	}

	public static function Obter ($configuracao, ?\Illuminate\Http\Request $request) : Collection
	{
		throw new \Exception('Método não implementado');
	}
}