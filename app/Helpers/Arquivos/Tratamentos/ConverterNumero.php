<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class ConverterNumero extends Tratamento
{
	public static $rotulo			= 'Converter em Número';
	public static $descricao		= 'Converte a coluna em valor numérico';
	public static $help				= 'Converte o valor para um número decimal real ao inves de uma string como decimal.<br /><br /><strong>OBS</strong>: Esse tratamento também faz a conversão automática do valor numérico brasileiro (que utiliza vírgula como separador decimal e ponto como separador de milhar) para o formato aceito pelo banco de dados e funções internas.';
	public static $configuracoes	= [
		'coluna'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 4,
			],
			'rotulo'						=> 'Nome da Coluna',
			'descricao'						=> 'Qual o nome da coluna que será convertida',
		],
		'rejeitar'						=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				''								=> 'Não Trata',
				RejectLine::class				=> 'Rejeita a Linha',
				RejectFile::class				=> 'Rejeita o Arquivo Inteiro',
			],
			'default'						=> '',
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Regra de Rejeição',
			'descricao'						=> 'Caso a coluna não possa ser encontrada, como o arquivo deve ser tratado.',
		],
	];

	public static function Executar (&$dados, &$configuracao)
	{
		if (!isset($dados[$configuracao->coluna]) && !empty($configuracao->rejeitar))
			throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento de renomear colunas não possuíndo a coluna ' . $configuracao->coluna);

		$dados[$configuracao->coluna] = floatVal(\preg_replace('/[,.]/', '.', @$dados[$configuracao->coluna]));

		return $dados;
	}
}