<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class NotacaoNumerica extends Tratamento
{
	public static $rotulo			= 'Notação Numérica para String';
	public static $descricao		= 'Converte um número com qualquer formato de notação numérica para string';
	public static $help				= 'Converte um número que está em notação numérica (Ex: 8,95505e+19) para notação decimal.<br /><br /><strong>OBS</strong>: Em geral o excel usa essa notação para números que são muito grandes e isso faz com que as unidades menores em geral sejam perdidas. ICCID é um ótimo exemplo, pois se convertido em notação científica os últimos dígitos são perdidos e transformados em zero';
	public static $configuracoes	= [
		'coluna'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 6,
			],
			'rotulo'						=> 'Coluna da Planilha',
			'descricao'						=> 'Qual coluna da planilha que irá receber este tratamento',
		],
		'rejeitar'						=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				''								=> 'Não Trata',
				RejectLine::class				=> 'Rejeita a Linha',
				RejectFile::class				=> 'Rejeita o Arquivo Inteiro',
			],
			'default'						=> '',
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Regra de Rejeição',
			'descricao'						=> 'Caso a coluna não possa ser encontrada, como o arquivo deve ser tratado.',
		],
	];

	public static function Executar (&$dados, &$configuracao)
	{
		if (!isset($dados[$configuracao->coluna]) && !empty($configuracao->rejeitar))
			throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento ' . self::$rotulo);

		$dados[$configuracao->coluna] = (string) \number_format((float) \preg_replace('/[,.]/', '.', @$dados[$configuracao->coluna]), 0);

		return $dados;
	}
}