<?php

namespace App\Helpers\Arquivos\Tratamentos;

abstract class FormatarData extends _Base
{
	public static function GetInfo ()
	{
		return (object) [
			'tipo'		=> self::class,
			'label'		=> 'Formatar Data',
			'descricao'	=> 'Modifica o formato de um dato do tipo "Data"',
			'execucao'	=> _Base::EXECUCAO_POS,
			'campos'	=> [
				(object) ['tipo'=>'texto', 'campo'=>'coluna', 'label'=>'Coluna da Planilha', 'largura'=>6, 'descricao'=>'Qual coluna da planilha que irá receber este tratamento'],
				(object) ['tipo'=>'texto', 'campo'=>'antigo', 'label'=>'Formato Original', 'largura'=>3, 'descricao'=>'Como o campo está formatado no momento'],
				(object) ['tipo'=>'texto', 'campo'=>'novo', 'label'=>'Novo Formato', 'largura'=>3, 'descricao'=>'Como o campo ficará formatado após o tratamento'],
			],
			'data'		=> (object) [
				'coluna'		=> '',
				'antigo'		=> '',
				'novo'			=> '',
			],
		];
	}

	public static function Executar (&$dados, &$configuracao, ?array &$colunas = null)
	{
		if ($data = @\DateTime::createFromFormat($configuracao->antigo, $dados[$index]))
			$dados[$colunas[$configuracao->coluna]] = $data->format($configuracao->novo);

		return $dados;
	}
}