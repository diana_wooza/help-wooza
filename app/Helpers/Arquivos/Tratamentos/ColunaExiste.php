<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class ColunaExiste extends Tratamento
{
	public static $rotulo			= 'Coluna Existe';
	public static $descricao		= 'Verifica se a coluna existe no arquivo';
	public static $help				= 'Este tratamento verifica se uma determinada coluna existe no arquivo.<br />A coluna que estiver configurada no campo <strong>Coluna</strong> deve existir no arquivo que está sendo tratado.<br />Caso a coluna não exista, a regra de rejeição será aplicada.<br /><br /><strong>OBS:</strong> Este tratamento é usado exclusivamente para rejeição de linhas ou arquivos, por isso não existe a opção "Não Tratar" na lista de regra de rejeição.';
	public static $configuracoes	= [
		'coluna'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 6,
			],
			'rotulo'						=> 'Coluna da Planilha',
			'descricao'						=> 'Qual coluna da planilha que irá receber este tratamento',
		],
		'rejeitar'						=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				RejectLine::class				=> 'Rejeita a Linha',
				RejectFile::class				=> 'Rejeita o Arquivo Inteiro',
			],
			'default'						=> RejectLine::class,
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Regra de Rejeição',
			'descricao'						=> 'Caso a coluna não possa ser encontrada, como o arquivo deve ser tratado.',
		],
	];

	public static function Executar (&$dados, &$configuracao) : bool
	{
		return isset($dados[$configuracao->coluna]);
	}
}