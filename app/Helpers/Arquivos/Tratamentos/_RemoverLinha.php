<?php

namespace App\Helpers\Arquivos\Tratamentos;

abstract class RemoverLinha extends _Base
{
	public static function GetInfo ()
	{
		return (object) [
			'tipo'		=> self::class,
			'label'		=> 'Remover Linha',
			'descricao'	=> 'Remove uma linha inteira de dados',
			'execucao'	=> _Base::EXECUCAO_PRE,
			'campos'	=> [
				(object) ['tipo'=>'numero', 'campo'=>'linha', 'label'=>'Linha Inicial', 'largura'=>6, 'descricao'=>'Número da linha em que esta regra vai começar a excluir', 'config'=>['min'=>1, 'max'=>1048576, 'step'=>1]],
				(object) ['tipo'=>'numero', 'campo'=>'numero', 'label'=>'Total de Linhas', 'largura'=>6, 'descricao'=>'Número de linhas que serão excluídas', 'config'=>['min'=>1, 'max'=>1048576, 'step'=>1]],
			],
			'data'		=> (object) [
				'linha'			=> 1,
				'numero'		=> 1,
			],
		];
	}
	
	public static function Executar (&$dados, &$configuracao, ?array &$colunas = null)
	{
		\array_splice($dados, $configuracao->linha - 1, $configuracao->numero);
		return $dados;
	}
}