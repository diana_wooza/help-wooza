<?php

namespace App\Helpers\Arquivos\Tratamentos\Base;

abstract class Tratamento
{
	public static function GetInfo ()
	{
		$info = new \stdClass();

		$info->tipo				= static::class;
		$info->rotulo			= static::$rotulo;
		$info->help				= static::$help;
		$info->descricao		= static::$descricao;
		$info->configuracoes	= static::$configuracoes;

		return $info;
	}

	public static function Executar (&$dados, &$configuracao)
	{
		throw new \Exception(static::class . ' não implementou Executar');
	}
}