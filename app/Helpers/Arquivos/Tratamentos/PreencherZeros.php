<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class PreencherZeros extends Tratamento
{
	public static $rotulo			= 'Preencher com Zero';
	public static $descricao		= 'Preenche zeros à esquerda até o número de caracteres estipulado';
	public static $help				= 'Preenche uma quantidade de zeros à esquerda até o limite de caracteres especificados.<br /><br /><strong>OBS</strong>: Caso o valor original possua a mesma quantidade ou mais dígitos do que o especificado o valor não será alterado';
	public static $configuracoes	= [
		'coluna'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 6,
			],
			'rotulo'						=> 'Coluna da Planilha',
			'descricao'						=> 'Qual coluna da planilha que irá receber este tratamento',
		],
		'caracteres'					=> [
			'tipo'							=> 'numero',
			'default'						=> 1,
			'layout'						=> [
				'colunas'						=> 2,
			],
			'rotulo'						=> 'Caracteres',
			'descricao'						=> 'Qual o número de caracteres que a coluna precisa ter',
		],
		'rejeitar'						=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				''								=> 'Não Trata',
				RejectLine::class				=> 'Rejeita a Linha',
				RejectFile::class				=> 'Rejeita o Arquivo Inteiro',
			],
			'default'						=> '',
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Regra de Rejeição',
			'descricao'						=> 'Caso a coluna não possa ser encontrada, como o arquivo deve ser tratado.',
		],
	];

	public static function Executar (&$dados, &$configuracao)
	{
		if (!isset($dados[$configuracao->coluna]) && !empty($configuracao->rejeitar))
			throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento ' . self::$rotulo . ' colunas não possuíndo a coluna ' . $configuracao->coluna);

		if (!empty($dados[$configuracao->coluna]))
		{
			$zeroes = \str_repeat('0', $configuracao->caracteres);
			$tamanho = \max($configuracao->caracteres, \strlen(@$dados[$configuracao->coluna]));
			$dados[$configuracao->coluna] = \substr($zeroes . @$dados[$configuracao->coluna], $tamanho  * -1);
		}
		else
		{
			$dados[$configuracao->coluna] = null;
		}

		return $dados;
	}
}