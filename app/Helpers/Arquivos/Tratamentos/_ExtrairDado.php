<?php

namespace App\Helpers\Arquivos\Tratamentos;

abstract class ExtrairDado extends _Base
{
	public static function GetInfo ()
	{
		return (object) [
			'tipo'		=> self::class,
			'label'		=> 'Extrair Informações de Coluna',
			'descricao'	=> 'Cria uma nova coluna contendo apenas a parte escolhida da informação contida em outra coluna',
			'help'		=> 'Este tratamento é utilizado para ',
			'execucao'	=> _Base::EXECUCAO_POS,
			'campos'	=> [
				(object) ['tipo'=>'texto', 'campo'=>'coluna', 'label'=>'Coluna da Planilha', 'largura'=>3, 'descricao'=>'Qual coluna possui o valor original a ser extraído'],
				(object) ['tipo'=>'texto', 'campo'=>'expressao', 'label'=>'Regra de Extração', 'largura'=>6, 'descricao'=>'Regra que será utilizada para extrair o valor da coluna'],
				(object) ['tipo'=>'texto', 'campo'=>'nova', 'label'=>'Coluna Resultante', 'largura'=>3, 'descricao'=>'Nome da nova coluna que será incluída na planilha com o resultado da extração'],
			],
			'data'		=> (object) [
				'coluna'		=> '',
				'expressao'		=> '',
				'nova'			=> '',
			],
		];
	}

	public static function Executar (&$dados, &$configuracao, ?array &$colunas = null)
	{
		$indexLeft = $colunas[\mb_strtolower(\trim($configuracao->coluna))];
		$indexRight = $colunas[\mb_strtolower(\trim($configuracao->nova))];

		if (preg_match('/' . $configuracao->expressao . '/', $dados[$indexLeft], $matches))
		{
			$dados[$indexRight] = $matches[1];
		}
		else
		{
			$dados[$indexRight] = '-- Não Encontrado --';
		}

		return $dados;
	}
}