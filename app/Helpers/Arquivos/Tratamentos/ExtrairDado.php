<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class ExtrairDado extends Tratamento
{
	public static $rotulo			= 'Extrair Informações de Coluna';
	public static $descricao		= 'Cria uma nova coluna contendo apenas a parte escolhida da informação contida em outra coluna';
	public static $help				= 'Este tratamento é usado para extrair um valor específico de dentro de uma coluna';
	public static $configuracoes	= [
		'coluna'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Nome da Coluna Original',
			'descricao'						=> 'Qual coluna possui o valor original a ser extraído',
		],
		'expressao'						=> [
			'tipo'							=> 'texto',
			'default'						=> '.*',
			'layout'						=> [
				'colunas'						=> 4,
			],
			'rotulo'						=> 'Regra de Extração',
			'descricao'						=> 'Regra que será utilizada para extrair o valor da coluna',
		],
		'nova'							=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Nova Coluna',
			'descricao'						=> 'Nome da coluna que irá conter o valor extraído',
		],
		'rejeitar'						=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				''								=> 'Não Trata',
				RejectLine::class				=> 'Rejeita a Linha',
				RejectFile::class				=> 'Rejeita o Arquivo Inteiro',
			],
			'default'						=> '',
			'layout'						=> [
				'colunas'						=> 2,
			],
			'rotulo'						=> 'Regra de Rejeição',
			'descricao'						=> 'Caso a coluna não possa ser encontrada, como o arquivo deve ser tratado.',
		],
	];

	public static function Executar (&$dados, &$configuracao)
	{
		if (!isset($dados[$configuracao->coluna]) && !empty($configuracao->rejeitar))
			throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento de renomear colunas não possuíndo a coluna ' . $configuracao->coluna);


		$indexLeft = \mb_strtolower(\trim($configuracao->coluna));
		$indexRight = \mb_strtolower(\trim($configuracao->nova));

		if (preg_match('/' . $configuracao->expressao . '/', @$dados->{$indexLeft}, $matches))
		{
			$dados->{$indexRight} = $matches[1];
		}
		elseif (!empty($configuracao->rejeitar))
		{
			throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento de extrair dado não possuíndo valor a ser extraído na coluna ' . $configuracao->coluna);
		}
		else
		{
			$dados->{$indexRight} = '-- Não Encontrado --';
		}

		return $dados;
	}
}