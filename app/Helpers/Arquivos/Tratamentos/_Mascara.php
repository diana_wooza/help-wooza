<?php

namespace App\Helpers\Arquivos\Tratamentos;

abstract class Mascara extends _Base
{
	public static function GetInfo ()
	{
		return (object) [
			'tipo'		=> self::class,
			'label'		=> 'Mascarar Coluna',
			'descricao'	=> 'Edita o valor de uma determinada coluna para que ele obedeça a um padrão previamente estabelecido',
			'execucao'	=> _Base::EXECUCAO_POS,
			'campos'	=> [
				(object) ['tipo'=>'texto', 'campo'=>'coluna', 'label'=>'Coluna da Planilha', 'largura'=>6, 'descricao'=>'Qual coluna da planilha que irá receber este tratamento'],
				(object) ['tipo'=>'texto', 'campo'=>'mascara', 'label'=>'Máscara', 'largura'=>6, 'descricao'=>'Qual formato de máscara será utilizado no dado'],
			],
			'data'		=> (object) [
				'coluna'		=> '',
				'mascara'		=> '',
			],
		];
	}

	public static function Executar (&$dados, &$configuracao, ?array &$colunas = null)
	{
		$index = $colunas[\mb_strtolower(\trim($configuracao->coluna))];
		$dados[$index] = self::mask($dados[$index], $configuracao->mascara);

		return $dados;
	}

	protected static function mask ($valorOriginal, $mascara)
	{
		$originalIndex = 0;
		$valorFinal = '';

		for ($i = 0, $total = strlen($mascara); $i < $total; $i++)
		{
			$valorAtual = $mascara[$i];

			if ($valorAtual == '#')
				$valorAtual = @((string) $valorOriginal)[$originalIndex++];

			$valorFinal .= $valorAtual;
		}

		return $valorFinal;
	}
}