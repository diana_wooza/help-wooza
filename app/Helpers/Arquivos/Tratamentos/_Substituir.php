<?php

namespace App\Helpers\Arquivos\Tratamentos;

abstract class Substituir extends _Base
{
	public static function GetInfo ()
	{
		return (object) [
			'tipo'		=> self::class,
			'label'		=> 'Substituir Valor',
			'descricao'	=> 'Substitui um valor dentro de uma coluna',
			'execucao'	=> _Base::EXECUCAO_POS,
			'campos'	=> [
				(object) ['tipo'=>'texto', 'campo'=>'coluna', 'label'=>'Coluna do Arquivo', 'largura'=>3, 'descricao'=>'Qual coluna que irá ser afetada'],
				(object) ['tipo'=>'texto', 'campo'=>'expressao', 'label'=>'Novo Nome', 'largura'=>6, 'descricao'=>'Com que nome a coluna será renomeada'],
				(object) ['tipo'=>'texto', 'campo'=>'valor', 'label'=>'Valor para substituição', 'largura'=>3, 'descricao'=>'Valor que será usado para substituir a informação'],
			],
			'data'		=> (object) [
				'coluna'		=> '',
				'expressao'		=> '',
				'valor'			=> '',
			],
		];
	}

	public static function Executar (&$dados, &$configuracao, ?array &$colunas = null)
	{
		$index = $colunas[$configuracao->coluna];
		$dados[$index] = preg_replace($configuracao->expressao, $configuracao->valor, $dados[$index]);

		return $dado;
	}
}