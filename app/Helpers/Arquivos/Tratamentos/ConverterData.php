<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class ConverterData extends Tratamento
{
	public static $rotulo			= 'Converter Data';
	public static $descricao		= 'Converte a coluna de data';
	public static $help				= 'Converte um valor de data para o formato de data aceito no banco de dados. Os formatos suportados são:<br /><br /><strong><i>{dia}</i></strong>/<strong><i>{mês}</i></strong>/<strong><i>{ano}</i></strong><br /><strong><i>{dia}</i></strong>/<strong><i>{mês}</i></strong>/<strong><i>{ano}</i></strong> <strong><i>{hora}</i></strong>:<strong><i>{minuto}</i></strong>:<strong><i>{segundo}</i></strong><br /><strong><i>{ano}</i></strong>-<strong><i>{mês}</i></strong>-<strong><i>{dia}</i></strong><br /><strong><i>{ano}</i></strong>-<strong><i>{mês}</i></strong>-<strong><i>{dia}</i></strong> <strong><i>{hora}</i></strong>:<strong><i>{minuto}</i></strong>:<strong><i>{segundo}</i></strong><br />';
	public static $configuracoes	= [
		'coluna'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 4,
			],
			'rotulo'						=> 'Nome da Coluna',
			'descricao'						=> 'Qual o nome da coluna que será convertida',
		],
		'rejeitar'						=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				''								=> 'Não Trata',
				RejectLine::class				=> 'Rejeita a Linha',
				RejectFile::class				=> 'Rejeita o Arquivo Inteiro',
			],
			'default'						=> '',
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Regra de Rejeição',
			'descricao'						=> 'Caso a coluna não possa ser encontrada, como o arquivo deve ser tratado.',
		],
	];

	private static $dateFormats = [
		'd/m/Y'			=> 'Y-m-d',
		'd/m/Y H:i:s'	=> 'Y-m-d H:i:s',
		'Y-m-d'			=> 'Y-m-d',
		'Y-m-d H:i:s'	=> 'Y-m-d H:i:s',
		'Y-m-d\TH:i:s'	=> 'Y-m-d H:i:s',
		'Y-m-d\tH:i:s'	=> 'Y-m-d H:i:s',
	];

	public static function Executar (&$dados, &$configuracao)
	{
		if (!isset($dados[$configuracao->coluna]) && !empty($configuracao->rejeitar))
			throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento de renomear colunas não possuíndo a coluna ' . $configuracao->coluna);

		$date = $dados[$configuracao->coluna];

		foreach (self::$dateFormats as $formatFrom => $formatTo)
		{
			if (!$isDate = \DateTime::createFromFormat($formatFrom, $date))
				continue;

			$dados[$configuracao->coluna] = $isDate->format($formatTo);

			return $dados;
		}

		if (!empty($configuracao->rejeitar))
			throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não possui um formato de data reconhecido na coluna  ' . $configuracao->coluna);

		return $dados;
	}
}