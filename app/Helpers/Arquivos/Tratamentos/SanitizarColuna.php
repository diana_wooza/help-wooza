<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class SanitizarColuna extends Tratamento
{
	public static $rotulo			= 'Sanitizar Coluna';
	public static $descricao		= 'Converte valores que são considerados inválidos em null de maneira a deixar a base mais "limpa"';
	public static $help				= 'Algumas vezes os arquivos do excel geral um #N/D no arquivo final (em geral por causa do procv), ou possuem uma função que foi preenchida com erro, ou a fonte de dados vinculada não existe mais, etc.<br>Essa regra permite definir valores que serão "desconsiderados" no resultado final, eliminando-os da coluna.<br /><br /><strong>OBS:</strong> Essa regra pode ser utilizada para limpar uma coluna completamente ou apenas parte do texto de uma coluna (a expressão regular que irá ditar o que seja sobreescrito). Caso o resultado final da célula seja um texto vazio após a sanitização a coluna será considerada NULL';
	public static $configuracoes	= [
		'coluna'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Coluna da Planilha',
			'descricao'						=> 'Qual coluna da planilha que irá receber este tratamento',
		],
		'expressao'						=> [
			'tipo'							=> 'texto',
			'default'						=> '(\#?(N\/D|N[AO]ME|DIV\/0|VALUE|REF|NUM|NULL)[!?]?)',
			'layout'						=> [
				'colunas'						=> 6,
			],
			'rotulo'						=> 'Regra de Tratamento (REGEX)',
			'descricao'						=> 'Expressão com os valores que esão considerados nulos',
		],
		'rejeitar'						=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				''								=> 'Não Trata',
				RejectLine::class				=> 'Rejeita a Linha',
				RejectFile::class				=> 'Rejeita o Arquivo Inteiro',
			],
			'default'						=> '',
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Regra de Rejeição',
			'descricao'						=> 'Caso a coluna não possa ser encontrada, como o arquivo deve ser tratado.',
		],
	];

	public static function Executar (&$dados, &$configuracao)
	{
		if (!isset($dados[$configuracao->coluna]) && !empty($configuracao->rejeitar))
			throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento ' . self::$rotulo);

		$dados[$configuracao->coluna] = \preg_replace('/' . $configuracao->expressao . '/mi', '', @$dados[$configuracao->coluna]);

		if (empty($dados[$configuracao->coluna]))
			$dados[$configuracao->coluna] = null;

		return $dados;
	}
}