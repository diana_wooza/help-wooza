<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class IgnorarLinhas extends Tratamento
{
	public static $rotulo			= 'Ignorar Linhas';
	public static $descricao		= 'Faz com que uma linha específica não seja importada para a base de dados';
	public static $help				= 'Informa o sistema que algumas linhas, por não possuírem a informação desejada devem ser ignoradas.<br /><br /><strong>OBS</strong>: Vale lembrar que uma linha ignorada não existirá na base de dados e por consequência não será exportada para download em CSV.';
	public static $configuracoes	= [
		'colunas'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 8,
			],
			'rotulo'						=> 'Colunas Verificadas',
			'descricao'						=> 'Coloque o nome das colunas. Mais de uma coluna pode ser identificada. Basta separar os nomes por vírgula ou ponto e vírgula',
		],
		'tratamento'					=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				'any'							=> 'Quando qualquer coluna estiver vazia',
				'all'							=> 'Quando todas as colunas etiverem vazias'
			],
			'default'						=> 'all',
			'layout'						=> [
				'colunas'						=> 4,
			],
			'rotulo'						=> 'Quando a linha será ignorada',
			'descricao'						=> 'Qual a regra será utilizada',
		],
	];

	public static function Executar (&$dados, &$configuracao)
	{
		$colunas = array_filter(preg_split('/[,;\n\r]/m', $configuracao->colunas), function ($item) {return !empty($item);});

		if (count($colunas) <= 0)
			return $dados;

		$vazias = 0;

		foreach ($colunas as $coluna)
		{
			if (!empty(@$dados[$coluna]))
				continue;

			if ($configuracao->tratamento == 'any')
				throw new new RejectLine('A linha ' . \json_encode($dados) . ' não possui um valor para a coluna <strong>' . $coluna . '</strong>');

			$vazias++;
		}

		if ($vazias == count($colunas))
			throw new new RejectLine('A linha ' . \json_encode($dados) . ' não possui um valor para a coluna <strong>' . $coluna . '</strong>');

		return $dados;
	}
}