<?php

namespace App\Helpers\Arquivos\Tratamentos;

use App\Helpers\Arquivos\Tratamentos\Base\Tratamento;
use App\Helpers\Arquivos\Exceptions\RejectFile;
use App\Helpers\Arquivos\Exceptions\RejectLine;

abstract class RenomearColuna extends Tratamento
{
	public static $rotulo			= 'Renomear Coluna';
	public static $descricao		= 'Modifica o nome que a coluna terá no arquivo final';
	public static $help				= 'Modifica o nome de uma coluna.<br /><br /><strong>OBS</strong>: Vale lembrar que o nome da coluna será alterado, fazendo com que o nome anterior da coluna não exista no resultado final.';
	public static $configuracoes	= [
		'antigo'						=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 4,
			],
			'rotulo'						=> 'Nome Antigo',
			'descricao'						=> 'Qual o nome atual da coluna',
		],
		'novo'							=> [
			'tipo'							=> 'texto',
			'default'						=> null,
			'layout'						=> [
				'colunas'						=> 4,
			],
			'rotulo'						=> 'Nome Novo',
			'descricao'						=> 'Com que nome a coluna será renomeada',
		],
		'rejeitar'						=> [
			'tipo'							=> 'select',
			'opcoes'						=> [
				''								=> 'Não Trata',
				RejectLine::class				=> 'Rejeita a Linha',
				RejectFile::class				=> 'Rejeita o Arquivo Inteiro',
			],
			'default'						=> '',
			'layout'						=> [
				'colunas'						=> 3,
			],
			'rotulo'						=> 'Regra de Rejeição',
			'descricao'						=> 'Caso a coluna não possa ser encontrada, como o arquivo deve ser tratado.',
		],
	];

	public static function Executar (&$dados, &$configuracao)
	{
		if (!empty($configuracao->rejeitar))
		{
			if (!\array_key_exists($configuracao->antigo, $dados))
				throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento ' . self::$rotulo . ' não possuíndo a coluna ' . $configuracao->antigo);

			if (!\array_key_exists($configuracao->novo, $dados) && !empty($dados[$configuracao->novo]))
				throw new $configuracao->rejeitar('A linha ' . \json_encode($dados) . ' não atende à configuração requerida pelo tratamento ' . self::$rotulo . ' pois já possui um valor preenchido na coluna de destinho ' . $configuracao->novo);
		}

		if (!\array_key_exists($configuracao->novo, $dados) || empty($dados[$configuracao->novo]))
		{
			if (isset($dados['_colunas_originais'][$configuracao->antigo]))
			{
				$dados['_colunas_originais'][$configuracao->novo] = $dados['_colunas_originais'][$configuracao->antigo];
				unset($dados['_colunas_originais'][$configuracao->antigo]);
			}

			$dados[$configuracao->novo] = @$dados[$configuracao->antigo];
			unset($dados[$configuracao->antigo]);
		}

		return $dados;
	}
}