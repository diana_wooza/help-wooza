<?php

namespace App\Helpers\Claro;

class IW
{
	const CATEGORIA_FORMULARIO_INPUT_PME_HITSS_DEVOLUCAO	= 24;
	const CATEGORIA_FORMULARIO_INPUT_PME_WOOZA_ENVIO		= 23;

	const PUBLIC_GRUPO										= 7;

	const CODIGOGRUPO_FORMULARIO_INPUT_PME_RECEPCAO			= 1932;
	const CODIGOGRUPO_FORMULARIO_INPUT_PME_HITSS_DEVOLUCAO	= 1933;

	private $cookieFile;
	
	public function __construct ()
	{
		$this->cookieFile = storage_path(implode(DIRECTORY_SEPARATOR, [
			'curl',
			uniqid('iw_session_')
		]));
	}

	public function EnviarArquivo ($path, $filename, $title, $expiration)
	{
		$curlService = $this->_login();

		\curl_setopt_array($curlService, [
			CURLOPT_URL				=> config('iw.login.url', ENV('IW_LOGIN_URL', 'https://iw.claro.com.br/v2/sys/tools/ra_publicacao/upload')),
			CURLOPT_FOLLOWLOCATION	=> true,
			CURLOPT_POST			=> true,
			CURLOPT_HTTPHEADER		=> [
				'Content-Type: multipart/form-data'
			],
			CURLOPT_POSTFIELDS		=> [
				'COD_CATEGORIA'			=> self::CATEGORIA_FORMULARIO_INPUT_PME_WOOZA_ENVIO,
				'ARQUIVO_PUBLICACAO'	=> new \CurlFile($path . $filename, 'application/vnd.ms-excel', $filename),
				'DSC_TITULO'			=> $filename,
				'DSC_TEXTO'				=> $title,
				'DAT_EXPIRACAO'			=> $expiration,
				'IND_NAO_EXPIRA'		=> 1,
				'COD_TEMPLATE_ACEITE'	=> '',
				'COD_TIPO_PUBLICO'		=> self::PUBLIC_GRUPO,
				'COD_PUBLICO'			=> self::CODIGOGRUPO_FORMULARIO_INPUT_PME_RECEPCAO,
				'COD_REGRA'				=> '',
			],
			CURLOPT_COOKIESESSION	=> true,
			CURLOPT_COOKIEFILE		=> $this->cookieFile,
			CURLOPT_COOKIEJAR		=> $this->cookieFile,
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		\curl_exec($curlService);
		\curl_close($curlService);
	}

	private function _login ()
	{
		\curl_setopt_array($curlService = \curl_init(), [
			CURLOPT_URL				=> config('iw.login.url', ENV('IW_LOGIN_URL', 'https://iw.claro.com.br/v2/login/autenticar')),
			CURLOPT_FOLLOWLOCATION	=> true,
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS		=> [
				'login'					=> '93335860',
				'senha'					=> 'U9!j-i7R',
			],
			CURLOPT_COOKIESESSION	=> true,
			CURLOPT_COOKIEFILE		=> $this->cookieFile,
			CURLOPT_COOKIEJAR		=> $this->cookieFile,
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		$login = \curl_exec($curlService);
		$headers = curl_getinfo($curlService);

		if ($headers['http_code'] != 200 || $headers['url'] != 'https://iw.claro.com.br/v2/wvd')
		{
			if (preg_match('/<div class="statusBox">(.*)<\/div>/m', $login, $match))
				throw new \Exception($match[1]);

			throw new \Exception('Ocorreu um erro desconhecido ao efetuar o login');
		}

		return $curlService;
	}

	
}