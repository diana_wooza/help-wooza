<?php

namespace App\Helpers;

/**
 * Permite extrair as informações de um SKU específico
**/
class Sku
{
	const TIPO_SIMCARD					= 'S';
	
	const PRODUTO_FIXO					= 'F';
	const PRODUTO_MOVEL					= 'M';

	const MODELO_TRIPLO					= '00';
	const MODELO_NORMAL					= '01';
	const MODELO_MICRO					= '02';
	const MODELO_NANO					= '03';
	const MODELO_NORMAL_MICRO			= '04';
	const MODELO_MICRO_NANO				= '05';

	const BANDA_3G						= '3G';
	const BANDA_4G						= '4G';

	const OPERADORA_VIVO				= 'V';
	const OPERADORA_TIM					= 'T';
	const OPERADORA_CLARO				= 'C';
	const OPERADORA_OI					= 'O';

	public static $tipos				= [
		self::TIPO_SIMCARD					=> 'Sim Card',
	];

	public static $produtos				= [
		self::PRODUTO_FIXO					=> 'Fixo',
		self::PRODUTO_MOVEL					=> 'Móvel',
	];

	public static $modelos				= [
		self::MODELO_TRIPLO					=> 'TRIPLO',
		self::MODELO_NORMAL					=> 'NORMAL',
		self::MODELO_MICRO					=> 'MICRO',
		self::MODELO_NANO					=> 'NANO',
		self::MODELO_NORMAL_MICRO			=> 'NORMAL/MICRO',
		self::MODELO_MICRO_NANO				=> 'MICRO/NANO',
	];

	public static $bandas				= [
		self::BANDA_3G						=> '3G',
		self::BANDA_4G						=> '4G',
	];

	public static $operadoras			= [
		self::OPERADORA_VIVO				=> 'Vivo',
		self::OPERADORA_TIM					=> 'Tim',
		self::OPERADORA_CLARO				=> 'Claro',
		self::OPERADORA_OI					=> 'Oi',
	];

	public static $estados				= [
		'AC'								=> 'Acre',
		'AL'								=> 'Alagoas',
		'AP'								=> 'Amapá',
		'AM'								=> 'Amazonas',
		'BA'								=> 'Bahia',
		'CE'								=> 'Ceará',
		'DF'								=> 'Distrito Federal',
		'ES'								=> 'Espírito Santo',
		'GO'								=> 'Goiás',
		'MA'								=> 'Maranhão',
		'MT'								=> 'Mato Grosso',
		'MS'								=> 'Mato Grosso do Sul',
		'MG'								=> 'Minas Gerais',
		'PA'								=> 'Pará ',
		'PB'								=> 'Paraíba',
		'PR'								=> 'Paraná',
		'PE'								=> 'Pernambuco',
		'PI'								=> 'Piauí',
		'RJ'								=> 'Rio de Janeiro',
		'RN'								=> 'Rio Grande do Norte',
		'RS'								=> 'Rio Grande do Sul',
		'RO'								=> 'Rondônia',
		'RR'								=> 'Roraima',
		'SC'								=> 'Santa Catarina',
		'SP'								=> 'São Paulo',
		'SE'								=> 'Sergipe',
		'TO'								=> 'Tocantins',
	];

	private $restricoes 				= [
		'estados'							=> [],
		'modelo'							=> [],
		'banda'								=> [],
		'operadora'							=> [],
	];

	private static $hlr					= [
		'byOperadora'						=> [
			'C'									=> [
				'1XXX'								=> ['SP'],
				'2XXX'								=> ['ES','RJ'],
				'3XXX'								=> ['MG'],
				'4XXX'								=> ['PR','SC'],
				'5XXX'								=> ['RS'],
				'6XXX'								=> ['AC','DF','GO','MS','MT','RO','TO'],
				'7XXX'								=> ['BA','SE'],
				'8XXX'								=> ['AL','CE','PB','PE','PI','RN'],
				'9AMX'								=> ['AM'],
				'9APX'								=> ['AP'],
				'9MAX'								=> ['MA'],
				'9PAX'								=> ['PA'],
				'9RRX'								=> ['RR'],
			],
			'O'									=> [
				'0009'								=> ['ES','RJ'],
				'0019'								=> ['SP'],
				'0029'								=> ['MG'],
				'0039'								=> ['AL','PB','PE','RN','BA','SE'],
				'0049'								=> ['AC','DF','GO','MT','RO','TO','MS','RS','PR','SC'],
				'0069'								=> ['AP','MA','PA','RR','CE','PI'],
				'0099'								=> ['AM'],
				'11XX'								=> ['SP'],
				'12XX'								=> ['SP'],
				'13XX'								=> ['SP'],
				'14XX'								=> ['SP'],
				'15XX'								=> ['SP'],
				'16XX'								=> ['SP'],
				'17XX'								=> ['SP'],
				'18XX'								=> ['SP'],
				'19XX'								=> ['SP'],
				'21XX'								=> ['RJ'],
				'22XX'								=> ['RJ'],
				'24XX'								=> ['RJ'],
				'27XX'								=> ['ES'],
				'28XX'								=> ['ES'],
				'31XX'								=> ['MG'],
				'32XX'								=> ['MG'],
				'33XX'								=> ['MG'],
				'34XX'								=> ['MG'],
				'35XX'								=> ['MG'],
				'37XX'								=> ['MG'],
				'38XX'								=> ['MG'],
				'41XX'								=> ['PR'],
				'42XX'								=> ['PR'],
				'43XX'								=> ['PR'],
				'44XX'								=> ['PR'],
				'45XX'								=> ['PR'],
				'46XX'								=> ['PR'],
				'47XX'								=> ['SC'],
				'48XX'								=> ['SC'],
				'49XX'								=> ['SC'],
				'51XX'								=> ['RS'],
				'53XX'								=> ['RS'],
				'54XX'								=> ['RS'],
				'55XX'								=> ['RS'],
				'61XX'								=> ['DF'],
				'62XX'								=> ['GO'],
				'63XX'								=> ['TO'],
				'64XX'								=> ['GO'],
				'65XX'								=> ['MT'],
				'66XX'								=> ['MT'],
				'67XX'								=> ['MS'],
				'68XX'								=> ['AC'],
				'69XX'								=> ['RO'],
				'71XX'								=> ['BA'],
				'73XX'								=> ['BA'],
				'74XX'								=> ['BA'],
				'75XX'								=> ['BA'],
				'77XX'								=> ['BA'],
				'79XX'								=> ['SE'],
				'81XX'								=> ['PE'],
				'82XX'								=> ['AL'],
				'83XX'								=> ['PB'],
				'84XX'								=> ['RN'],
				'85XX'								=> ['CE'],
				'86XX'								=> ['PI'],
				'87XX'								=> ['PE'],
				'88XX'								=> ['CE'],
				'89XX'								=> ['PI'],
				'91XX'								=> ['PA'],
				'92XX'								=> ['AM'],
				'93XX'								=> ['PA'],
				'94XX'								=> ['PA'],
				'95XX'								=> ['RR'],
				'96XX'								=> ['AP'],
				'97XX'								=> ['AM'],
				'98XX'								=> ['MA'],
				'99XX'								=> ['MA'],
			],
			'V'									=> [
				'A000'								=> ['MG'],
				'B000'								=> ['AL','CE','PB','PE','PI','RN'],
				'D000'								=> ['RS'],
				'E000'								=> ['ES'],
				'F000'								=> ['PR'],
				'G000'								=> ['SC'],
				'H000'								=> ['BA','SE'],
				'J000'								=> ['RJ'],
				'K000'								=> ['MA'],
				'N000'								=> ['AP'],
				'O000'								=> ['AC','DF','GO','MS','MT','RO','TO'],
				'Q000'								=> ['RR'],
				'S000'								=> ['SP'],
				'T000'								=> ['AM'],
				'U000'								=> ['AL','CE','PB','PE','PI','RN','RS','ES','PR','SC','BA','SE','RJ','MA','AP','AC','DF','GO','MS','MT','RO','TO','RR','SP','AM','PA','MG'],
				'W000'								=> ['PA'],
				'X000'								=> ['PA 93'],
				'Y000'								=> ['PA 93'],
			],
			'T'									=> [
				'0460'								=> ['AC','DF','GO','MS','MT','RO','TO'],
				'0240'								=> ['AL','PB','PI','RN'],
				'0292'								=> ['AM'],
				'0290'								=> ['AP','MA','PA','RR'],
				'0270'								=> ['BA','SE'],
				'0280'								=> ['CE','PE'],
				'0230'								=> ['MG'],
				'0440'								=> ['PR','SC'],
				'0220'								=> ['RJ','ES'],
				'0450'								=> ['RS'],
				'0311'								=> ['SP'],
				'0460'								=> ['AC','DF','GO','MS','MT','RO','TO'],
				'0240'								=> ['AL','PB','PI','RN'],
				'UP00'								=> ['AL','CE','PB','PE','PI','RN','RS','ES','PR','SC','BA','SE','RJ','MA','AP','AC','DF','GO','MS','MT','RO','TO','RR','SP','AM','PA','MG'],
				'UNA0'								=> ['AL','CE','PB','PE','PI','RN','RS','ES','PR','SC','BA','SE','RJ','MA','AP','AC','DF','GO','MS','MT','RO','TO','RR','SP','AM','PA','MG'],
			],
		],
		'byRegiao'							=> [
			'AC'								=> [
				'C'									=> ['6XXX'],
				'O'									=> ['0049','68XX'],
				'T'									=> ['0460','UP00','UNA0'],
				'V'									=> ['O000','U000'],
			],
			'AL'								=> [
				'C'									=> ['8XXX'],
				'O'									=> ['0039','82XX'],
				'T'									=> ['0240','UP00','UNA0'],
				'V'									=> ['B000','U000'],
			],
			'AM'								=> [
				'C'									=> ['9AMX'],
				'O'									=> ['0099','92XX','97XX'],
				'T'									=> ['0292','UP00','UNA0'],
				'V'									=> ['T000','U000'],
			],
			'AP'								=> [
				'C'									=> ['9APX'],
				'O'									=> ['0069','96XX'],
				'T'									=> ['0290','UP00','UNA0'],
				'V'									=> ['N000','U000'],
			],
			'BA'								=> [
				'C'									=> ['7XXX'],
				'O'									=> ['0039','71XX','73XX','74XX','75XX','77XX'],
				'T'									=> ['0270','UP00','UNA0'],
				'V'									=> ['H000','U000'],
			],
			'CE'								=> [
				'C'									=> ['8XXX'],
				'O'									=> ['0069','85XX','88XX'],
				'T'									=> ['0280','UP00','UNA0'],
				'V'									=> ['B000','U000'],
			],
			'DF'								=> [
				'C'									=> ['6XXX'],
				'O'									=> ['0049','61XX'],
				'T'									=> ['0460','UP00','UNA0'],
				'V'									=> ['O000','U000'],
			],
			'ES'								=> [
				'C'									=> ['2XXX'],
				'O'									=> ['0009','27XX','28XX'],
				'T'									=> ['0220','UP00','UNA0'],
				'V'									=> ['E000','U000'],
			],
			'GO'								=> [
				'C'									=> ['6XXX'],
				'O'									=> ['0049','62XX','64XX'],
				'T'									=> ['0460','UP00','UNA0'],
				'V'									=> ['O000','U000'],
			],
			'MA'								=> [
				'C'									=> ['9MAX'],
				'O'									=> ['0069','98XX','99XX'],
				'T'									=> ['0290','UP00','UNA0'],
				'V'									=> ['K000','U000'],
			],
			'MG'								=> [
				'C'									=> ['3XXX'],
				'O'									=> ['0029','31XX','32XX','33XX','34XX','35XX','37XX','38XX'],
				'T'									=> ['0230','UP00','UNA0'],
				'V'									=> ['A000','U000'],
			],
			'MS'								=> [
				'C'									=> ['6XXX'],
				'O'									=> ['0049','67XX'],
				'T'									=> ['0460','UP00','UNA0'],
				'V'									=> ['O000','U000'],
			],
			'MT'								=> [
				'C'									=> ['6XXX'],
				'O'									=> ['0049','65XX','66XX'],
				'T'									=> ['0460','UP00','UNA0'],
				'V'									=> ['O000','U000'],
			],
			'PA'								=> [
				'C'									=> ['9PAX'],
				'O'									=> ['0069','91XX','93XX','94XX'],
				'T'									=> ['0290','UP00','UNA0'],
				'V'									=> ['W000','U000'],
			],
			'PB'								=> [
				'C'									=> ['8XXX'],
				'O'									=> ['0039','83XX'],
				'T'									=> ['0240','UP00','UNA0'],
				'V'									=> ['B000','U000'],
			],
			'PE'								=> [
				'C'									=> ['8XXX'],
				'O'									=> ['0039','81XX','87XX'],
				'T'									=> ['0280','UP00','UNA0'],
				'V'									=> ['B000','U000'],
			],
			'PI'								=> [
				'C'									=> ['8XXX'],
				'O'									=> ['0069','86XX','89XX'],
				'T'									=> ['0240','UP00','UNA0'],
				'V'									=> ['B000','U000'],
			],
			'PR'								=> [
				'C'									=> ['4XXX'],
				'O'									=> ['0049','41XX','42XX','43XX','44XX','45XX','46XX'],
				'T'									=> ['0440','UP00','UNA0'],
				'V'									=> ['F000','U000'],
			],
			'RJ'								=> [
				'C'									=> ['2XXX'],
				'O'									=> ['0009','21XX','22XX','24XX'],
				'T'									=> ['0220','UP00','UNA0'],
				'V'									=> ['J000','U000'],
			],
			'RN'								=> [
				'C'									=> ['8XXX'],
				'O'									=> ['0039','84XX'],
				'T'									=> ['0240','UP00','UNA0'],
				'V'									=> ['B000','U000'],
			],
			'RO'								=> [
				'C'									=> ['6XXX'],
				'O'									=> ['0049','69XX'],
				'T'									=> ['0460','UP00','UNA0'],
				'V'									=> ['O000','U000'],
			],
			'RR'								=> [
				'C'									=> ['9RRX'],
				'O'									=> ['0069','95XX'],
				'T'									=> ['0290','UP00','UNA0'],
				'V'									=> ['Q000','U000'],
			],
			'RS'								=> [
				'C'									=> ['5XXX'],
				'O'									=> ['0049','51XX','53XX','54XX','55XX'],
				'T'									=> ['0450','UP00','UNA0'],
				'V'									=> ['D000','U000'],
			],
			'SC'								=> [
				'C'									=> ['4XXX'],
				'O'									=> ['0049','47XX','48XX','49XX'],
				'T'									=> ['0440','UP00','UNA0'],
				'V'									=> ['G000','U000'],
			],
			'SE'								=> [
				'C'									=> ['7XXX'],
				'O'									=> ['0039','79XX'],
				'T'									=> ['0270','UP00','UNA0'],
				'V'									=> ['H000','U000'],
			],
			'SP'								=> [
				'C'									=> ['1XXX'],
				'O'									=> ['0019','11XX','12XX','13XX','14XX','15XX','16XX','17XX','18XX'.'19XX'],
				'T'									=> ['0311','UP00','UNA0'],
				'V'									=> ['S000','U000'],
			],
			'TO'								=> [
				'C'									=> ['6XXX'],
				'O'									=> ['0049','63XX'],
				'T'									=> ['0460','UP00','UNA0'],
				'V'									=> ['O000','U000'],
			],
			'PA 93'								=> [
				'V'									=> ['X000','Y000'],
			],
		],
	];

	/**
	 * Classe não instanciável
	 */
	public function __construct ()
	{

	}

	public function toRegex ()
	{
		return self::TIPO_SIMCARD
			. self::PRODUTO_MOVEL
			. $this->getRegexHlr()
			. $this->getRegexModelo()
			. $this->getRegexBanda()
			. $this->getRegexOperadora()
		;
	}

	public function addEstado ($estado)
	{
		if (!isset(self::$estados[$estado]))
			throw new \Exception('Tentando incluir e estado inválido ' . $estado);

		$this->restricoes['estados'][] = $estado;

		return $this;
	}

	public function addOperadora ($operadora)
	{
		if (!isset(self::$operadoras[$operadora]))
			throw new \Exception('Tentando incluir a operadora inválido ' . $operadora);

		$this->restricoes['operadora'][] = $operadora;

		return $this;
	}

	public function addBanda ($banda)
	{
		if (!isset(self::$bandas[$banda]))
			throw new \Exception('Tentando incluir a banda inválido ' . $banda);

		$this->restricoes['banda'][] = $banda;

		return $this;
	}

	public function addModelo ($modelo)
	{
		if (!isset(self::$modelos[$modelo]))
			throw new \Exception('Tentando incluir o modelo inválido ' . $modelo);

		$this->restricoes['modelo'][] = $modelo;

		return $this;
	}

	private function getRegexHlr ()
	{
		if (count($this->restricoes['estados']) <= 0)
			return '....';

		$regex = [];
		$operadoras = count($this->restricoes['operadora']) > 0 ? $this->restricoes['operadora'] : array_keys(self::$operadoras);

		foreach ($this->restricoes['estados'] as $estado)
		{
			foreach ($operadoras as $operadora)
				$regex = array_merge($regex, self::$hlr['byRegiao'][$estado][$operadora]);
		}

		return '(' . implode('|', $regex) . ')';
	}

	private function getRegexModelo ()
	{
		if (count($this->restricoes['modelo']) <= 0)
			return '..';

		return '(' . implode('|', $this->restricoes['modelo']) . ')';
	}

	private function getRegexBanda ()
	{
		if (count($this->restricoes['banda']) <= 0)
			return '..';

		return '(' . implode('|', $this->restricoes['banda']) . ')';
	}

	private function getRegexOperadora ()
	{
		if (count($this->restricoes['operadora']) <= 0)
			return '.';

		return '[' . implode('', $this->restricoes['operadora']) . ']';
	}

	public static function info ($sku)
	{
		switch (substr($sku, 0, 1))
		{
			#Sim Cards
			case self::TIPO_SIMCARD:

				$operator = substr($sku, -1);

				return (object) [
					'tipo'		=> 'Sim Card',
					'operadora'	=> self::getOperadorName($operator),
					'produto'	=> self::simcard_getProduct(substr($sku, 1, 1)),
					'hlr'		=> self::simcard_getHlr(substr($sku, 2, 4), $operator),
					'modelo'	=> self::simcard_getModel(substr($sku, 6, 2)),
					'banda'		=> substr($sku, 8, 2),
				];
		}
		
		return null;
	}

	private static function simcard_getModel ($model)
	{
		switch ($model)
		{
			case '00': return 'TRIPLO';
			case '01': return 'NORMAL';
			case '02': return 'MICRO';
			case '03': return 'NANO';
			case '04': return 'NORMAL/MICRO';
			case '05': return 'MICRO/NANO';
		}

		return null;
	}

	private static function simcard_getHlr ($code, $operator)
	{
		if (!isset(self::$hlr['byOperadora'][$operator]) || !isset(self::$hlr['byOperadora'][$operator][$code]))
			return null;

		return self::$hlr['byOperadora'][$operator][$code];
	}

	private static function simcard_getProduct ($product)
	{
		switch ($product)
		{
			case 'F': return 'Fixo';
			case 'M': return 'Móvel';
		}

		return null;
	}

	private static function getOperadorName ($operator)
	{
		switch ($operator)
		{
			case 'C': return 'Claro';
			case 'O': return 'Oi';
			case 'T': return 'Tim';
			case 'V': return 'Vivo';
		}

		return null;
	}
}