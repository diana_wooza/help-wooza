<?php

namespace App\Helpers;

class Utils
{
	/**
	 * Classe não instanciável
	 */
	private function __construct ()
	{

	}

	final public static function CleanupDataEncoding ($data)
	{
		$data = \mb_convert_encoding($data, 'UTF-8');

		// if (preg_match('#[\x7F-\x9F\xBC]#', $data))
		// {
		// 	$data = iconv('WINDOWS-1250', 'UTF-8', $data);
		// }
		// elseif (!preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $data))
		// {
		// 	$data = iconv('ISO-8859-2', 'UTF-8', $data);
		// }
	
		return \mb_strtolower(\trim($data), 'UTF-8');
	}

	final public static function CriarChaves (\Closure $method, array $array) : array
	{
		$result = [];

		foreach ($array as $item)
			$result[$method($item)] = $item;

		return $result;
	}

	final public static function SqlNullString ($value) : ?string
	{
		if ($value === null)
			return 'NULL';

		if (is_numeric($value))
			return $value;

		return "'" . $value . "'";
	}
	
	/**
	 * Converte uma número em uma referência alfabética daquele número
	 * @method converteParaAlfabetico
	 * @param int $numero valor a ser convertido
	 * @return string
	 */
	final public static function converteParaAlfabetico (int $numero) : ?string
	{
		if ($numero <= 26)
			return chr($numero + 64);

		$numero--;

		return self::converteParaAlfabetico(floor($numero / 26)) . self::converteParaAlfabetico($numero % 26 + 1);
	}

	/**********************************************************************************************/

	#Lista de operadoras disponíveis + código de cada operadora
	private static $operadoras = [
		'vivo'				=> 15,
		'claro'				=> 21,
		'tim'				=> 41,
		'oi'				=> 31,
		'nextel'			=> 99,
	];

	/**
	 * Verifica se o nome da operadora é um nome válido
	 * @param string $operadora nome da operadora a ser verificado
	 * @return bool
	 */
	public static function verificaOperadoraValida (string $operadora) : bool
	{
		return isset(self::$operadoras[\mb_strtolower($operadora)]);
	}

	/**
	 * Define a operadora a ser utilizada. Existe uma lista específica de operadoras disponíveis
	 * Os valores possíveis são: Vivo, Claro, Tim, Oi, Nextel
	 * @param string $operadora nome da operadora
	 * @return int
	 */
	public static function getCodigoOperadora (string $operadora) : ?int
	{
		#se a operadora não estiver na lista retorna null, o que gera um erro ao enviar (por causa da validação feita nos end)
		if (!self::verificaOperadoraValida($operadora))
			return null;

		#retorna o código da operadora especificada
		return self::$operadoras[$operadora];
	}

	/**********************************************************************************************/

	/**
	 * Verifica se qualquer um dos parâmetros passados estão nulos, caso nenhum deles esteja retorna VERDADEIRO
	 * @param mixed qualquer tipo de variável pode ser passado, o método apenas verifica se o valor está nulo
	 * @return bool
	 */
	public static function verificaSeTodosPreenchidos () : bool
	{
		#pega todos os parâmetros passados para o método e verifica um a um
		foreach (func_get_args() as $param)
		{
			if (is_null($param))
				return false;
		}

		return true;
	}

	/**********************************************************************************************/

	#Caracteres acentuados comumente
	private static $caracteresParaTrocar = [
		'a'					=> ['À','Á','Â','Ã','Ä','Å','Æ','à','á','â','ã','ä','å','æ'],
		'e'					=> ['È','É','Ê','Ë','è','é','ê','ë'],
		'i'					=> ['Ì','Í','Î','Ï','ì','í','î','ï'],
		'o'					=> ['Ò','Ó','Ô','Õ','Ö','ò','ó','ô','õ','ö','ð','ò','ó','ô','õ','ö','ø'],
		'u'					=> ['Ù','Ú','Û','Ü','ù','ú','û','ü'],
		'b'					=> ['ß'],
		'c'					=> ['Ç','ç'],
		'd'					=> ['Ð'],
		'n'					=> ['ñ'],
		'y'					=> ['ý','ÿ'],
	];

	/**
	 * Remove todos os caracteres acentuados da string e substituíndo pela versão não acentuada do mesmo
	 * @param string $string que precisa ter os acentos removidos
	 * @return string
	 */
	public static function removeAcentuacao (string $string) : ?string
	{
		foreach (self::$caracteresParaTrocar as $character => $replaceables)
			$string = str_replace($replaceables, $character, $string);
		
		return $string;
	}

	/**
	 * limpa numero de telefone
	 * @param  string $telefone
	 * @return string
	 */
	public static function removeCaracterTelefone ($telefone)
	{
		return preg_replace('/[^0-9]/m', '', $telefone);
	}
}