<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

/**
 * Enviar mensagem de sms
 * Instancie a classe, popule os itens e envie a mensagem
**/
class Magento
{
	private static $simpleProductList			= null;
	private static $simpleProductListBySku		= [null];

	private static $crypt_key					= '36bf8d57a61d74566bf92c9ce37d84cb';
	private static $crypt_handler				= null;
	private static $crypt_cypher				= \MCRYPT_BLOWFISH;
	private static $crypt_mode					= \MCRYPT_MODE_ECB;
	private static $crypt_vector				= null;

	/**
	 * Classe não instanciável
	 */
	private function __construct ()
	{

	}

	public static function Login ($username, $password)
	{
		#http://gestao.celulardireto.com.br/api_cd/vendas/admin_login.php?user='+sLogin+'&pass='+sSenha
		\curl_setopt_array($curlService = \curl_init(), [
			CURLOPT_URL				=> 'http://gestao.celulardireto.com.br/api_cd/vendas/admin_login.php?' . \http_build_query([
				'user'					=> $username,
				'pass'					=> $password,
			]),
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_FOLLOWLOCATION	=> true,
			CURLOPT_SSL_VERIFYHOST	=> false,
			CURLOPT_SSL_VERIFYPEER	=> false,
		]);

		#Envia a requisição, pega a resposta e finaliza a conexão
		$resposta = \curl_exec($curlService);
		\curl_close($curlService);

		if ($resposta == '{"result":0}')
			return null;

		return json_decode($resposta);
	}

	private static function _getCrypt ()
	{
		\error_reporting(E_ALL ^ E_DEPRECATED);

		if (self::$crypt_handler === null)
		{
			self::$crypt_handler = \mcrypt_module_open(self::$crypt_cypher, '', self::$crypt_mode, '');
			self::$crypt_vector = \mcrypt_create_iv(\mcrypt_enc_get_iv_size(self::$crypt_handler), \MCRYPT_RAND);

			$maxKeySize = \mcrypt_enc_get_key_size(self::$crypt_handler);

			\mcrypt_generic_init(self::$crypt_handler, self::$crypt_key, self::$crypt_vector);
		}

		return self::$crypt_handler;
	}

	public static function encrypt ($value)
	{
		return self::_encrypt_1(base64_encode($value));
	}

	private static function _encrypt_1 ($value)
	{
		return base64_encode(\mcrypt_generic(self::_getCrypt(), (string) $value));
	}

	public static function decryptBase ($data)
	{
		return self::_decrypt_2($data);
	}

	public static function decrypt ($value, $criptografado = null)
	{
		$val = explode(" ", trim($value));

		if ($criptografado == '0' || strlen($val[0]) == 1) {
			return $value;
		}

		$decrypt = self::_decrypt_1($val[0]);
		if (base64_encode(base64_decode($decrypt)) !== $decrypt) {
			return $value;
		}

		$retorno = '';
		foreach ($val as $v) {
			$retorno .= base64_decode(self::_decrypt_1($v)) . " ";
		}

		return substr($retorno, 0, -1);
	}

	private static function _decrypt_1 ($data)
	{
		return str_replace("\x0", '', trim(self::_decrypt_2(base64_decode((string)$data))));
	}

	private static function _decrypt_2 ($data)
	{
		if (strlen($data) == 0)
			return $data;

		return \mdecrypt_generic(self::_getCrypt(), $data);
	}

	/**
	 * Busca a lista de produtos símples da API do magento
	 */
	public static function getSimpleProductsList ()
	{
		if (is_null(self::$simpleProductList))
			self::$simpleProductList = @\json_decode(file_get_contents(self::getHost() . '/api_cd/list_simple_products.php'));
		
		return self::$simpleProductList;
	}

	public static function getProductListIndexdBySku (string $sku = null)
	{
		if (is_null(self::$simpleProductListBySku) <= 0)
		{
			self::$simpleProductListBySku = [];

			foreach (self::getSimpleProductsList() as $product)
				self::$simpleProductListBySku[$product->sku] = $product;
		}

		if (is_null($sku))
			return self::$simpleProductListBySku;
			
		return self::$simpleProductListBySku[$sku];
	}

	/**
	 * Novo método que busca os produtos direto no banco do magento
	 * @method getPedidoColmeia
	 * @param array $pedidos Array com os IDs dos pedidos a serem buscados
	 * @return 
	 */
	 public static function getPedidoColmeia (array $pedidos)
	 {
		 return DB::connection('magento')
		 	->table('sales_flat_order')
		 	->select(
				DB::raw('sales_flat_order.increment_id as id'),
				DB::raw('sales_flat_order.status'),
				DB::raw('CONCAT_WS(",", sales_flat_order.iccid, GROUP_CONCAT(sales_flat_order_dependente.iccid)) AS iccids')
			)
			->leftJoin('sales_flat_order_dependente', 'sales_flat_order_dependente.order_id','=','sales_flat_order.entity_id')
			->whereIn('increment_id', $pedidos)
			->groupBy(DB::raw('sales_flat_order.increment_id'))
			->groupBy(DB::raw('sales_flat_order.increment_id'))
			->groupBy(DB::raw('sales_flat_order.iccid'))
			->groupBy(DB::raw('sales_flat_order.status'))
			->get()
		;
	}
		
	private static function getHost ()
	{
		return env('MAGENTO_API_HOST', 'https://gestao.celulardireto.com.br');
	}
}