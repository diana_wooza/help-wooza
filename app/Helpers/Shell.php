<?php

namespace App\Helpers;

class Shell
{
	private $conn			= false;
	private $timeout	= 100;

	public function __construct ($host, $user, $pass, $port = 22)
	{
		$this->pass = $pass;
		$this->port = $port;

		$this
			->connect($host, $port)
			->authenticate($user, $pass)
		;
	}

	public function connect ($host, $port)
	{
		if (!($this->conn = \ssh2_connect($host, $port)))
			throw new Exception ("Unable to connect to {$this->host}");

		return $this;
	}

	public function authenticate ($user, $pass)
	{
		if (!\ssh2_auth_password($this->conn, $user, $pass))
			throw new Exception ("Unable to authenticate");

		return $this;
	}

	public function command ($command)
	{
		$stream = \ssh2_exec($this->conn, $command);

		if ($stream === false)
			throw new Exception("Unable to execute command '$command'");

		stream_set_blocking ($stream, true);
		stream_set_timeout ($stream, $this->timeout);
		
		return stream_get_contents ($this->stream);
	}

	public function shell ($commands = array ())
	{
		$stream = \ssh2_shell ($this->conn);
		$sleepTime = 1;

		sleep ($sleepTime);
		$output = [];
		while ($line = fgets ($strem))
			$output[] = $line;

		foreach ($commands as $command)
		{
			$output = [];
			fwrite ($stream, $command . PHP_EOL);
			sleep (1);

			while ($line = fgets ($stream))
			{
				$output[] = $line;
				sleep ($sleepTime);
			}
		}
		
		fclose ($stream);

		return $output;
	}

	public function disconnect ()
	{
		@call_user_func(function_exists ('ssh2_disconnect') ? 'ssh2_disconnect' : 'fclose', $this->conn);
		$this->conn = false;
	}
}