<?php
namespace App\Console\Commands\Lojaonline;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

use App\Models\Lojaonline\Order;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\Lojaonline\RelatorioPedidos as MyMail;

class RelatorioPedidos extends Command
{
	
	protected $signature = 'lojaonline:relatorio_pedidos';

	private $disk = 'lojaonline';
	private $emails = [
		// 'arthur.testi@wooza.com.br',
		// 'diego.britto@wooza.com.br',
		
		'marina.vieira@wooza.com.br',
		'ramayanna.goncalves@wooza.com.br',
		'vanessa.andrade@wooza.com.br',
		'diogo.silva@wooza.com.br',
	];
	
	public function handle() : void
	{
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '3G');

		$orders = Order::with('aparelho.resource')->claro()->AguardandoInput()->get();
		$orderMap = function ($order) {
			return [
				'pedido'					=> @$order->number,
				'data criação'				=> date('d/m/Y H:i:s', strtotime(@$order->created_at)),
				'data status'				=> date('d/m/Y H:i:s', strtotime(@$order->status_updated_at)),
				'loja'						=> @$order->store_string,
				'cliente'					=> @$order->customer->name,
				'cpf'						=> @$order->customer->cpf,
				'aparelho'					=> @$order->aparelho->resource->name,
			];
		};

		$arquivo = Excel::create('export_pedidos_lojaonline_' . \date('YmdHis'), function($excel) use ($orders, $orderMap) {

			$excel
				->setCreator('HUB')
				->setCompany('Wooza')
				->setDescription('Pedidos Lojaonline ' . \date('d/m/Y \à\s H:i:s'))
			;

			$excel->sheet('Bogo+Netsalles', function($sheet) use ($orders, $orderMap) {

				$sheet->fromArray($orders->filter(function ($order) {return $order->is_bogo;})->map($orderMap)->toArray());
				
			});

			$excel->sheet('WA+WM', function($sheet) use ($orders, $orderMap) {

				$sheet->fromArray($orders->filter(function ($order) {return $order->is_netsales;})->map($orderMap)->toArray());
				
			});

			$excel->sheet('Não Classificados', function($sheet) use ($orders, $orderMap) {

				$sheet->fromArray($orders->filter(function ($order) {return !$order->is_netsales && !$order->is_bogo;})->map($orderMap)->toArray());
				
			});
		

		})->store('xlsx', Storage::disk($this->disk)->path(DIRECTORY_SEPARATOR));

		Mail
			::to($this->emails)
			->send(new MyMail(Storage::disk($this->disk)->path($arquivo->filename . '.' . $arquivo->ext)))
		;

		Storage::disk($this->disk)->delete($arquivo->filename . '.' . $arquivo->ext);
	}
}