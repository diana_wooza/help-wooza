<?php
namespace App\Console\Commands\Lojaonline;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

use App\Models\Lojaonline\Order;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\Lojaonline\RelatorioBacklog as MyMail;

class RelatorioBacklog extends Command
{
	
	protected $signature = 'lojaonline:relatorio_backlog';

	private $disk = 'lojaonline';
	private $emails = [
		// 'arthur.testi@wooza.com.br',
		// 'diego.britto@wooza.com.br',
		
		'lorena.teixeira@wooza.com.br',
		'rodrigo.madera@wooza.com.br',
		'anderson.silva@wooza.com.br',
		'marina.vieira@wooza.com.br',
		'ramayanna.goncalves@wooza.com.br',
		'mayara.oliveira@wooza.com.br',
		'vanessa.andrade@wooza.com.br',
		'diogo.silva@wooza.com.br',

		#removidos
		// 'ariel.viveiros@wooza.com.br',
		// 'daniel.nunes@wooza.com.br',
	];
	
	public function handle() : void
	{
		date_default_timezone_set('America/Fortaleza');
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '3G');

		$pedidos = [
			'claro_prospect'				=> Order::ClaroProspect()->AguardandoInput()->count(),
			'claro_pos'						=> Order::ClaroPos()->AguardandoInput()->count(),
			'claro_pre'						=> Order::ClaroPre()->AguardandoInput()->count(),
			'claro_entrantes'				=> Order::Claro()->AguardandoInput()->DoDia()->count(),
			'tim_aguardando_migracao'		=> Order::tim()->AguardandoInput()->BacklogMigracao()->count(),
			'tim_aguardando_novalinha'		=> Order::tim()->AguardandoInput()->BacklogNovalinha()->count(),
			'tim_aguardando_portabilidade'	=> Order::tim()->AguardandoInput()->BacklogPortabilidade()->count(),
			'tim_aguardando_entrantes'		=> Order::tim()->AguardandoInput()->DoDia()->count(),
			'tim_aprovado_migracao'			=> Order::tim()->AprovadoCartao()->BacklogMigracao()->count(),
			'tim_aprovado_novalinha'		=> Order::tim()->AprovadoCartao()->BacklogNovalinha()->count(),
			'tim_aprovado_portabilidade'	=> Order::tim()->AprovadoCartao()->BacklogPortabilidade()->count(),
			'tim_aprovado_aparelho'			=> Order::tim()->AprovadoCartao()->BacklogAparelho()->count(),
			'tim_aprovado_entrantes'		=> Order::tim()->AprovadoCartao()->DoDia()->count(),
		];

		// config(['export.pdf.driver' , 'dompdf');

		$arquivo = Excel::create('relatorio_backlog' . \date('YmdHis'), function($excel) use ($pedidos) {

			$excel
				->setCreator('HUB')
				->setCompany('Wooza')
				->setDescription('Relatório Backlog Lojaonline ' . \date('d/m/Y \à\s H:i:s'))
			;

			$excel->sheet('backlog', function($sheet) use ($pedidos) {

				$sheet->cell('D4', function($cell) {
					
					$cell->setValue('BACKLOG');
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(18);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D6', function($cell) {
					
					$cell->setValue('Loja Online');
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(18);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D8', function($cell) {
					
					$cell->setValue('DATA: ' . date('d/M - H:i\h'));
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(12);
					$cell->setBackground('#000000');
					$cell->setFontColor('#FFFFFF');
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				/**
				 * Lojas Claro
				 */

				$sheet->cell('D9', function($cell) {

					$cell->setValue('CLARO');
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setBackground('#FF0000');
					$cell->setFontColor('#FFFFFF');
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D10', function($cell) {$cell->setValue('Prospect');});
				$sheet->cell('E10', function($cell) {$cell->setValue('Pós');});
				$sheet->cell('F10', function($cell) {$cell->setValue('Pré');});

				$sheet->cell('D10:F10', function($cell) {

					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setBackground('#FF8F8F');
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D11', function($cell) use ($pedidos) {$cell->setValue($pedidos['claro_prospect']);});
				$sheet->cell('E11', function($cell) use ($pedidos) {$cell->setValue($pedidos['claro_pos']);});
				$sheet->cell('F11', function($cell) use ($pedidos) {$cell->setValue($pedidos['claro_pre']);});

				$sheet->cell('D11:F11', function($cell) use ($pedidos) {

					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D12', function($cell) use ($pedidos) {

					$cell->setValue($pedidos['claro_prospect'] + $pedidos['claro_pos'] + $pedidos['claro_pre']);
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D14', function($cell) use ($pedidos) {

					$cell->setValue('Entrantes do dia:');
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('F14', function($cell) use ($pedidos) {

					$cell->setValue($pedidos['claro_entrantes']);
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				/**
				 * Aguardando Input
				 */

				$sheet->cell('D16', function($cell) {

					$cell->setValue('TIM - Aguardando Input');
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setBackground('#0070C0');
					$cell->setFontColor('#FFFFFF');
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D17', function($cell) {$cell->setValue('Migração');});
				$sheet->cell('E17', function($cell) {$cell->setValue('Nova linha');});
				$sheet->cell('F17', function($cell) {$cell->setValue('Portabilidade');});

				$sheet->cell('D17:F17', function($cell) {

					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setBackground('#8EA9DB');
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D18', function($cell) use ($pedidos) {$cell->setValue($pedidos['tim_aguardando_migracao']);});
				$sheet->cell('E18', function($cell) use ($pedidos) {$cell->setValue($pedidos['tim_aguardando_novalinha']);});
				$sheet->cell('F18', function($cell) use ($pedidos) {$cell->setValue($pedidos['tim_aguardando_portabilidade']);});

				$sheet->cell('D18:F18', function($cell) use ($pedidos) {

					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D19', function($cell) use ($pedidos) {

					$cell->setValue($pedidos['tim_aguardando_migracao'] + $pedidos['tim_aguardando_novalinha'] + $pedidos['tim_aguardando_portabilidade']);
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D21', function($cell) use ($pedidos) {

					$cell->setValue('Entrantes do dia:');
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('F21', function($cell) use ($pedidos) {

					$cell->setValue($pedidos['tim_aguardando_entrantes']);
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				/**
				 * Cartão de Crédito
				 */

				$sheet->cell('D23', function($cell) {

					$cell->setValue('TIM - Aprovado - Cartão de Crédito');
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setBackground('#0070C0');
					$cell->setFontColor('#FFFFFF');
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
			
				});

				$sheet->cell('D24', function($cell) {$cell->setValue('Apenas Aparelhos');});
				$sheet->cell('E24', function($cell) {$cell->setValue('Migração');});
				$sheet->cell('F24', function($cell) {$cell->setValue('Nova linha');});
				$sheet->cell('G24', function($cell) {$cell->setValue('Portabilidade');});

				$sheet->cell('D24:G24', function($cell) {

					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setBackground('#8EA9DB');
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D25', function($cell) use ($pedidos) {$cell->setValue($pedidos['tim_aprovado_aparelho']);});
				$sheet->cell('E25', function($cell) use ($pedidos) {$cell->setValue($pedidos['tim_aprovado_migracao']);});
				$sheet->cell('F25', function($cell) use ($pedidos) {$cell->setValue($pedidos['tim_aprovado_novalinha']);});
				$sheet->cell('G25', function($cell) use ($pedidos) {$cell->setValue($pedidos['tim_aprovado_portabilidade']);});

				$sheet->cell('D25:G25', function($cell) use ($pedidos) {

					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D26', function($cell) use ($pedidos) {

					$cell->setValue($pedidos['tim_aprovado_aparelho'] + $pedidos['tim_aprovado_migracao'] + $pedidos['tim_aprovado_novalinha'] + $pedidos['tim_aprovado_portabilidade']);
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('D28', function($cell) use ($pedidos) {

					$cell->setValue('Entrantes do dia:');
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->cell('F28', function($cell) use ($pedidos) {

					$cell->setValue($pedidos['tim_aprovado_entrantes']);
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
					$cell->setAlignment('center');
				
				});

				$sheet->mergeCells('D4:F5');
				$sheet->mergeCells('D6:F6');
				$sheet->mergeCells('D8:F8');
				$sheet->mergeCells('D9:F9');
				$sheet->mergeCells('D12:F12');
				$sheet->mergeCells('D14:E14');
				$sheet->mergeCells('D16:F16');
				$sheet->mergeCells('D19:F19');
				$sheet->mergeCells('D21:E21');
				$sheet->mergeCells('D23:G23');
				$sheet->mergeCells('D26:G26');
				$sheet->mergeCells('D28:E28');

				$sheet->setWidth(array(
					'A'		=>  8.33,
					'B'		=>  8.33,
					'C'		=>  16.56,
					'D'		=>  16.56,
					'E'		=>  9.67,
					'F'		=>  12.33,
					'G'		=>  12.33,
					'H'		=>  12.33,
				));
				
			});
		

		})->store('xlsx', Storage::disk($this->disk)->path(DIRECTORY_SEPARATOR));

		Mail
			::to($this->emails)
			->send(new MyMail(Storage::disk($this->disk)->path($arquivo->filename . '.' . $arquivo->ext)))
		;

		Storage::disk($this->disk)->delete($arquivo->filename . '.' . $arquivo->ext);
	}
}