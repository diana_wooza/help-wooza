<?php
namespace App\Console\Commands\Lojaonline;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

use App\Models\Lojaonline\Order;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\Lojaonline\RelatorioCrivo as MyMail;

class RelatorioCrivo extends Command
{
	
	protected $signature = 'lojaonline:relatorio_crivo';

	private $disk = 'lojaonline';
	private $emails = [
		// 'arthur.testi@wooza.com.br',
		// 'diego.britto@wooza.com.br',
		
		'cristiano.leite@wooza.com.br',
		'marina.vieira@wooza.com.br',
		'ramayanna.goncalves@wooza.com.br',
		'vanessa.andrade@wooza.com.br',
		'diogo.silva@wooza.com.br',
	];
	
	public function handle() : void
	{
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '3G');

		$claroAguardando = Order::claro()->AguardandoInput();
		$timAguardando = Order::tim()->AguardandoInput();
		$timCartao = Order::tim()->AprovadoCartao();

		

		#echo 'Iniciando Processo!' . PHP_EOL;

		$orders = Order::tim()->AguardandoInput()->Periodo(\date('Y-m-d', \strtotime('-1 day')), \date('Y-m-d'))->get();

		#echo 'Pedidos Encontardos: ' . $orders->count() . PHP_EOL;
		$resultados = [];

		foreach ($orders as $order)
		{
			#echo 'Iniciando Elegibilidade do pedido: ' . $order->number . PHP_EOL;
			$elegibilidade = $order->Tim_ConsultaElegibilidade();
			#echo 'Resultado: ' . json_encode($elegibilidade) . PHP_EOL;

			$plano = null;

			if (!\is_null($elegibilidade->retorno->planosDisponiveis) && is_array($elegibilidade->retorno->planosDisponiveis))
			{
				foreach ($elegibilidade->retorno->planosDisponiveis as $planoDisponivel)
				{
					$planoDisponivel->Valor = \str_replace(',','.', $planoDisponivel->Valor) / 100;

					if (!\is_null($plano) && $plano->Valor > $planoDisponivel->Valor || \strstr(\mb_strtolower($planoDisponivel->Nome), 'express'))
						continue;

					$planoDisponivel->Valor = \number_format($planoDisponivel->Valor, 2, ',', '.');
					$plano = $planoDisponivel;
				}
			}

			$resultado = [
				'pedido'								=> $order->number,
				'data_pedido'							=> \date('d/m/Y H:i:s', \strtotime($order->created_at)),
				'linha'									=> $order->billing_address->phone_1_numeros,
				'cliente'								=> $order->customer->name,
				'cpf'									=> $order->customer->cpf,
				'estado'								=> $order->billing_address->state,
				'nome_mae'								=> $order->customer->mother_name,
				'data_nascimento'						=> \date('d/m/Y', \strtotime($order->customer->birthday)),
				'cep'									=> $order->zipcode,
				'nome_plano'							=> $order->plano->name,
				'valor_plano'							=> \number_format($order->plano->value, 2, ',', '.'),
				'modalidade'							=> $order->purchase_type,
				#'aparelho'								=> @$order->aparelho->name,
				'elegibilidade'							=> $elegibilidade->mensagem,
				'plano_elegivel_segmento'				=> @$plano->Segmento,
				'plano_elegivel_nome'					=> @$plano->Nome,
				'plano_elegivel_valor'					=> @$plano->Valor,
				'plano_elegivel_fidelizado_nome'		=> @$plano->NomePlanoFidelizado,
				'plano_elegivel_fidelizado_descricao'	=> @$plano->DescricaoPlanoFidelizado,
				'plano_elegivel_fidelizado_desconto'	=> @$plano->DescontoPlanoFidelizado,
				'json_planos'							=> \json_encode($elegibilidade->retorno->planosDisponiveis),
			];

			$resultados[] = $resultado;

			#echo 'Resultado Montado ' . json_encode($resultado) . PHP_EOL;
		}

		if (count($resultados) <= 0)
		{
			$resultados[] = [
				'pedido'								=> null,
				'data_pedido'							=> null,
				'linha'									=> null,
				'cliente'								=> null,
				'cpf'									=> null,
				'estado'								=> null,
				'nome_mae'								=> null,
				'data_nascimento'						=> null,
				'cep'									=> null,
				'nome_plano'							=> null,
				'valor_plano'							=> null,
				'modalidade'							=> null,
				#'aparelho'								=> null,
				'elegibilidade'							=> null,
				'plano_elegivel_segmento'				=> null,
				'plano_elegivel_nome'					=> null,
				'plano_elegivel_valor'					=> null,
				'plano_elegivel_fidelizado_nome'		=> null,
				'plano_elegivel_fidelizado_descricao'	=> null,
				'plano_elegivel_fidelizado_desconto'	=> null,
			];
		}

		$arquivo = Excel::create('export_consulta_elegibilidade_' . \date('YmdHis'), function($excel) use ($resultados) {

			$excel
				->setCreator('HUB')
				->setCompany('Wooza')
				->setDescription('Consulta Elegibilidade ' . \date('d/m/Y \à\s H:i:s'))
			;

			$excel->sheet('pedidos', function($sheet) use ($resultados) {

				$sheet->fromArray($resultados);
				
			});
		

		})->store('xlsx', Storage::disk($this->disk)->path(DIRECTORY_SEPARATOR));

		Mail
			::to($this->emails)
			->send(new MyMail(Storage::disk($this->disk)->path($arquivo->filename . '.' . $arquivo->ext)))
		;

		Storage::disk($this->disk)->delete($arquivo->filename . '.' . $arquivo->ext);
	}
}