<?php
namespace App\Console\Commands\Reaproveitamento;
 
use Illuminate\Console\Command;
use App\Models\Reaproveitamento\Agendamento;
 
class EnviarDadosLote extends Command{
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'reaproveitamento:enviar_dados_lote';

	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void {
        $data = date_create(date('Y-m-d H:i'));
        $agendamento = new Agendamento();
        $agendamentos = $agendamento->where('data_inicio', '<=', $data->format('Y-m-d H:i'))->get();

        foreach ($agendamentos as $agendamento) {
            $ch = curl_init(env('ROTA_REAPROVEITAMENTO_MAGENTO'));
            $header[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $agendamento->dados);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);

            curl_exec($ch);
            // $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            // if ($httpcode == 200) {
                $dataAgendamento = date_create($agendamento->data_inicio, new \DateTimeZone('America/Sao_Paulo'));

                switch ($agendamento->recorrencia) {
                    case 'Uma Vez':
                        $agendamento->delete();
                        return;
                    break;
                    case 'Diária':
                        date_add($dataAgendamento, date_interval_create_from_date_string("1 days"));
                    break;
                    case 'Semanal':
                        date_add($dataAgendamento, date_interval_create_from_date_string("1 weeks"));
                    break;
                    case 'Mensal':
                        date_add($dataAgendamento, date_interval_create_from_date_string("1 months"));
                    break;
                }

                $agendamento->data_inicio = $dataAgendamento;
                $agendamento->save();
            // }
        }

    }
}