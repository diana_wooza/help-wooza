<?php
namespace App\Console\Commands\Alarmistica;
 
use Illuminate\Console\Command;
use \App\Helpers\Sms;
use \App\Models\User;
use Illuminate\Support\Facades\DB;
 
class AlarmisticaSendSmsBacklog extends Command
{
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'alarmistica:sms_backlog';

    private $backlog;

	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void {
        $this->backlog = new \App\Models\Alarmistica\LimitesBacklog();
        $operadoras = $this->backlog->select('operadora')->distinct()->get()->pluck('operadora')->all();

        $data = date_create(date('Y-m-d H:i:s'), new \DateTimeZone('America/Sao_Paulo'));

        foreach ($operadoras as $operadora) {
            // $data->setTimezone(new \DateTimeZone('UTC'));
            $dados_magento = $this->getDadosMagento($operadora, $data);    
            $this->comparar($dados_magento, $operadora, $data);    
        }
    }
    
    private function getDadosMagento($operadora, $data) {
        return DB::connection('magento')->table('indicador_logistica_consolidado')
                ->select('modalidade', 'tipo', 'segmento', 'status', 'total')
                    ->where('operadora', $operadora)
                    ->where('created_at', 'like', $data->format('Y-m-d H').'%')
                        ->get();
    }

    private function comparar($dados_magento, $operadora, $data) {
        $telefones = User::whereIn('login', [
            'diego.britto',
            'carlos.elias',
            'joao.silva',
            'ramayanna.goncalves',
            'livia.meira',
            'isabelle.amorim',
            'dennys.guimaraes',
            ])->get()->pluck('telefone')->all();
      
        foreach ($dados_magento as $magento) {
            $data->setTimezone(new \DateTimeZone('America/Sao_Paulo'));
            $resultado = $this->backlog
                            ->select('q3', 'ls')
                                ->where('operadora', $operadora)
                                ->where('modalidade', $magento->modalidade)
                                ->where('tipo_plano', $magento->tipo)
                                ->where('segmento', $magento->segmento)
                                ->where('status', $magento->status)
                                ->where('dia_semana', $data->format('l'))
                                ->where('hora', $data->format('H'))
                                    ->first();

            if ($resultado != null) {
                if ((intval($resultado->ls) < intval($magento->total)) && (intval($resultado->ls) > intval($resultado->q3))) {
                    $data->setTimezone(new \DateTimeZone('America/Sao_Paulo'));
                    foreach ($telefones as $telefone) {
                        $this->enviarSms($telefone, $this->textoSms(
                            [
                                'operadora' => $operadora,
                                'modalidade' => $magento->modalidade,
                                'tipo' => $magento->tipo,
                                'segmento' => $magento->segmento,
                                'status' => $magento->status,
                                'ls' => $resultado->ls,
                                'total' => $magento->total,
                                'data' => $data
                            ]));
                    }
                }
            }
        }
    }

	/**
	 * Envia SMS para o número específico
	 * @method enviarSms
	 * @param string $numero número de telefone que vai receber o SMS
	 * @param string $texto texto a ser enviado para a destinatário
	 * @return void
	 */
	private function enviarSms (string $numero, string $texto) : void {
		$sms = new Sms();
		$sms->setNumero($numero);
		$sms->setMensagem($texto);
		$sms->setIp('10.17.4.4');
		$sms->Enviar();
	}

	/**
	 * Troca as palavras chaves do texto do SMS
	 * @method textoSms
	 * @return string
	 */
	private function textoSms ($valores) : string {
		$sms  = $valores['data']->format('l').', '.$valores['data']->format('H').'hs. Limite excedido para {operadora} - {modalidade} - {tipo} - {segmento} - {status} ({ls}::{total})';
        $sms  = str_replace('{operadora}', $valores['operadora'], $sms);
        $sms  = str_replace('{modalidade}', $valores['modalidade'], $sms);
        $sms  = str_replace('{tipo}', $valores['tipo'], $sms);
        $sms  = str_replace('{segmento}', $valores['segmento'], $sms);
        $sms  = str_replace('{status}', $valores['status'], $sms);
        $sms  = str_replace('{ls}', trim($valores['ls']), $sms);
        $sms  = str_replace('{total}', $valores['total'], $sms);

		return $sms;
	}
}