<?php
namespace App\Console\Commands\Alarmistica;
 
use Illuminate\Console\Command;
use \App\Helpers\Sms;
use \App\Models\Alarmistica\Api;
use \App\Models\User;
use \DateTime;
 
class AlarmisticaSendSms1h extends Command
{
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'alarmistica:sms_1h';
	protected $description = 'Envia SMS para todas as apis cadastradas caso haja algum problema';

	/**
	 * Texto padrão para envio de SMS
	 */
	protected $textoSms = 'Alerta Magento 1h: {nome_api} está critica com {percentual}% de sucesso.' . PHP_EOL . '{data_consulta}';

	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void
	{
		$data = date('d/m/Y H:i:s');

		foreach (Api::todasAtivas() as $api)
		{
			if($api->configuracao->consulta_schedule == '1h'){

				// \Log::info('Gerando log as '. $data .' para API ' . $api->nome);
				$log = $api->gerarLog();
				
				if ($api->estaComUsoRestrito()) // caso seja true pula a tentativa de envio de sms
					continue;
	
				## Verifica se pode mandar sms 
				if (!$api->estaComPercentualAlarmante())
					continue;
				
				foreach ($api->sms as $sms)
				{
					$usuario = User::find($sms->usuario);
					
					// \Log::info('Enviando SMS da API ' . $api->nome . ' para ' . $usuario->nome);
					$this->enviarSms($usuario->telefone, $this->textoSms($api->nome, $log->percentual, $data), $sms->broker);
				}
			}
		}
	}

	/**
	 * Envia SMS para o número específico
	 * @method enviarSms
	 * @param string $numero número de telefone que vai receber o SMS
	 * @param string $texto texto a ser enviado para a destinatário
	 * @param string $operadora operadora de envio do SMS
	 * @return void
	 */
	private function enviarSms (string $numero, string $texto, string $operadora) : void
	{
		$sms = new Sms();
		$sms->setNumero($numero);
		$sms->setMensagem($texto);
		#$sms->setOperadora($operadora);
		$sms->setIp('10.17.4.4');
		$sms->Enviar();
	}

	/**
	 * Troca as palavras chaves do texto do SMS para os dados da API passada
	 * @method textoSms
	 * @param App\Models\Alarmistica\Config $api api usada
	 * @return string
	 */
	private function textoSms (string $api, float $percentual, $data) : string
	{
		$sms  = $this->textoSms;
		$sms  = str_replace('{nome_api}', $api, $sms);
		$sms  = str_replace('{percentual}', number_format($percentual * 100, 1, ',', '.'), $sms);
		$sms  = str_replace('{data_consulta}', $data, $sms);

		return $sms;
	}
}