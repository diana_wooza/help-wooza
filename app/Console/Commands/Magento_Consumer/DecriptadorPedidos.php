<?php
namespace App\Console\Commands\Magento_Consumer;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

use \App\Models\Magento\Consumer\Order;
use App\Mail\Magento\Consumer\PedidosDecriptados as MyMail;

class DecriptadorPedidos extends Command
{
	protected $signature	= 'magento_consumer:decriptador {email : E-mail da pessoa que irá receber o arquivo} {data_inicial : Data inicial de corte} {data_final : Data final de corte}';
	protected $description	= 'Envia emails de relatórios periódicos para a claro';

	private $disk			= 'magento';
	private $chunk			= 500;
	private $filename		= null;
	private $separator		= ';';

	public function __construct ()
	{
		parent::__construct();
		$this->filename = \uniqid('decrypt_') . '.csv';
	}
	
	public function handle() : void
	{
		ini_set('max_execution_time', 5*3600);
		ini_set('memory_limit', '5G');

		$email = $this->argument('email');
		$data_inicial = $this->argument('data_inicial');
		$data_final = $this->argument('data_final');

		$file = \fopen(Storage::disk($this->disk)->path($this->filename), 'w');
		\fputcsv($file, ['Pedido','Data de Criação', 'Data de Ativação', 'CEP', 'CPF', 'E-MAIL', 'Linha de Serviço'], $this->separator);

		$current = 0;
		$total = Order::whereNotIn('plan_modality', ['SVA','Upgrade'])->whereBetween('activated_date', [$data_inicial,$data_final])->count();

		do
		{
			$pedidos = Order
				::whereNotIn('plan_modality', ['SVA','Upgrade'])
				->whereBetween('activated_date', [
					$data_inicial,
					$data_final,
				])
				->take($this->chunk)
				->skip($current)
				->get()
			;

			foreach ($pedidos as $pedido)
			{
				\fputcsv($file, [
					$pedido->increment_id,
					$pedido->created_at_formatado,
					$pedido->activated_date_formatado,
					$pedido->postcode,
					$pedido->customer_cpf,
					$pedido->customer_email,
					$pedido->phone_service,
				], $this->separator);
			}

			$current += $this->chunk;
		}
		while ($current <= $total);

		\fclose($file);

		// Mail
		// 	::to($email)
		// 	->send(new MyMail(
		// 		Storage::disk($this->disk)->path($this->filename),
		// 		$data_inicial,
		// 		$data_final
		// 	))
		// ;

		// Storage::disk($this->disk)->delete($this->filename);
	}

	private function _formataPeriodo ($periodo, int $modificador = 0)
	{
		$dia = strtotime($periodo['dia']);
		$dia = strtotime('-' . $modificador . ' days', $dia);

		return date('Y-m-d', $dia) . ' ' . $periodo['hora'];
	}
}