<?php
namespace App\Console\Commands\Magento_Corp;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

use App\Helpers\Claro\IW;
use \App\Models\Magento\Corp\OrderLinha;
use App\Mail\Magento\Corp\RelatorioPedidosClaro as MyMail;

class RelatorioPedidosClaro extends Command
{
	protected $signature	= 'magento_corp:relatorio_pedidos_claro {lote : Qual dos 3 lotes serão enviados [lote1, lote2, lote3]} {dias=0 : Quantidades de dias para trás a prtir de hoje}';
	protected $description	= 'Envia emails de relatórios periódicos para a claro';

	private $disk			= 'magento';
	private $emails			= [
		'arthur.testi@wooza.com.br',
		// 'diego.britto@wooza.com.br',
		'isabelle.amorim@wooza.com.br',

		'natally.ribeiro@wooza.com.br',
		'erika.oliveira@wooza.com.br',
		'rafael.silva@wooza.com.br',
	];

	private $lotes			= [
		'lote1'					=> [
			'titulo' 				=> 'Lote 1',
			'periodo'				=> [
				'de' 					=> ['dia' => '-1 day', 'hora' => '18:00:00'],
				'ate'					=> ['dia' => '+0 day', 'hora' => '09:00:00'],
			]
		],
		'lote2'					=> [
			'titulo'				=> 'Lote 2',
			'periodo'				=> [
				'de' 					=> ['dia' => '+0 day', 'hora' => '09:00:00'],
				'ate'					=> ['dia' => '+0 day', 'hora' => '13:00:00'],
			]
		],
		'lote3'					=> [
			'titulo'				=> 'Lote 3',
			'periodo'				=> [
				'de'	 				=> ['dia' => '+0 day', 'hora' => '13:00:00'],
				'ate'					=> ['dia' => '+0 day', 'hora' => '18:00:00']
			]
		],
		'lote_diario'			=> [
			'titulo'				=> 'Lote Diário',
			'periodo'				=> [
				'de'	 				=> ['dia' => '-1 day', 'hora' => '18:00:00'],
				'ate'					=> ['dia' => '+0 day', 'hora' => '18:00:00']
			]
		],
	];
	
	public function handle() : void
	{
		ini_set('max_execution_time', 3600);
		ini_set('memory_limit', '3G');

		$lote = $this->argument('lote');
		$dias = is_numeric($this->argument('dias')) ? $this->argument('dias') : 0;
		$date = strtotime('-' . $dias . ' days');

		$iw = new IW();

		if (!isset($this->lotes[$lote]))
		{
			echo 'Lote Inválido';
			return;
		}

		$lote = $this->lotes[$lote];

		$linhasMap = function ($linha) {
			return [
				'Código do Pedido'								=> @$linha->pedido->increment_id,
				'Data do pedido'								=> @$linha->pedido->created_at_formatado,
				'Razão Social da Revenda'						=> @$linha->pedido->RazaoSocialRevenda,
				'CNPJ da revenda'								=> @$linha->pedido->CNPJrevenda,
				'Código da Revenda'								=> @$linha->pedido->CodigoRevenda,
				'Nome Completo do Consultor'					=> @$linha->pedido->NomeCompletoConsultor,
				'Código do Consultor'							=> @$linha->pedido->CodigoConsultor,
				'Canal de Venda'								=> @$linha->pedido->CanalVenda,
				'Razão Social'									=> @$linha->pedido->customer_fullname,
				'CNPJ'											=> @$linha->pedido->customer_cnpj,
				'Inscrição Estadual'							=> @$linha->pedido->InscricaoEstadual,
				'CNAE Primário'									=> @$linha->pedido->receita->cnae,
				'CNAE Secundário'								=> @$linha->pedido->CNAESecundario,
				'Representante Legal'							=> @$linha->pedido->contato->nome,
				'1º Administrador da Conta'						=> @$linha->pedido->contato->nome,
				'CPF 1º Administrador'							=> @$linha->pedido->contato->cpf,
				'Telefone de Contato 1º Administrador'			=> @$linha->pedido->customer_telephone,
				'Telefone de Contato Celular 1º Administrador'	=> @$linha->pedido->customer_cellphone,
				'E-mail 1º Administrador'						=> @$linha->pedido->customer_email,
				'2º Administrador da Conta'						=> @$linha->pedido->SegundoAdministradorConta,
				'CPF 2º Administrador'							=> @$linha->pedido->CPFSegundoAdministrador,
				'Telefone de Contato 2º Administrador'			=> @$linha->pedido->TelefoneContatoSegundoAdministrador,
				'Telefone de Contato Celular 2º Administrador'	=> @$linha->pedido->TelefoneContatoCelularSegundoAdministrador,
				'E-mail 2º Administrador'						=> @$linha->pedido->EmailSegundoAdministrador,
				'Logradouro'									=> @$linha->pedido->billing_address->street_logradouro,
				'Número'										=> @$linha->pedido->billing_address->street_numero,
				'Complemento'									=> @$linha->pedido->billing_address->street_complemento,
				'Bairro'										=> @$linha->pedido->billing_address->street_bairro,
				'Cidade'										=> @$linha->pedido->city,
				'Estado '										=> @$linha->pedido->region,
				'CEP'											=> @$linha->pedido->postcode,
				'Vencimento'									=> @$linha->pedido->dia_vencimento,
				'Tempo de Vigência de Contrato:'				=> @$linha->pedido->TempoVigenciaContrato,
				'Cliente Possui Serviços EMBRATEL?'				=> @$linha->pedido->ClientePossuiServicosEMBRATEL,
				'Cliente Possui Serviços NET?'					=> @$linha->pedido->ClientePossuiServicosNET,
				'Cliente possui oferta NET Empresas MAIS?'		=> @$linha->pedido->ClientepossuiofertaNETEmpresasMAIS,
				'Identificação de Cliente NET:'					=> @$linha->pedido->IdentificacaoClienteNET,
				'Regional'										=> @$linha->pedido->region,
				'DDD'											=> @$linha->ddd,
				'Tipo Solicitação'								=> @$linha->plan_modality,
				'Nome do Plano'									=> @$linha->plan_name,
				'Valor do Plano'								=> @$linha->plan_price,
				'KIT'											=> @$linha->pedido->KIT,
				'Bônus/Dobro'									=> @$linha->pedido->bonus,
				'Qtd. Linhas'									=> 1,
				'Passaporte avulso'								=> @$linha->passaportes,
				'Valor passaporte avulso'						=> @$linha->pedido->ValorPassaporteAvulso,
				'Modelo Sim card'								=> @$linha->pedido->ModeloSimcard,
				'Qtd. Sim card'									=> 1,
				'Valor sim card'								=> @$linha->pedido->ValorSimCard,
				'Forma de pagamento'							=> @$linha->pedido->FormaPagamento,
				'Portabilidade - Número da linha'				=> @$linha->phone_service,
				'Operadora'										=> @$linha->operadora_doadora,
				'TT PF/PJ - Nome Doador'						=> @$linha->pedido->TT_PFPJ_NomeDoador,
				'TT PF/PJ - CPF Doador'							=> @$linha->pedido->TT_PFPJ_CPFDoador,
				'TT PF/PJ - Pessoa para contato'				=> @$linha->pedido->TT_PFPJ_PessoaContato,
				'TT PF/PJ - Tel Contato'						=> @$linha->pedido->TT_PFPJ_TelContato,
				'TT PF/PJ - Cel Contato'						=> @$linha->pedido->TT_PFPJ_CelContato,
				'TT PF/PJ - Número da linha'					=> @$linha->pedido->TT_PFPJ_Numerolinha,
				'TT PF/PJ - Simcard'							=> @$linha->pedido->TT_PFPJ_Simcard,
				'Responsável pelo Recebimento simcard'			=> @$linha->pedido->contato->nome,
				'RG'											=> @$linha->pedido->contato->rg,
				'Telefone'										=> @$linha->pedido->contato->celular,
				'Local'											=> @$linha->pedido->city,
				'Data'											=> @$linha->pedido->created_at_formatado,
				'Status'										=> @$linha->pedido->Status,
				'Motivo Reprova'								=> @$linha->pedido->MotivoReprova,
				'IPP'											=> @$linha->pedido->IPP,
			];
		};

		$linhas = OrderLinha
			::with(['pedido','pedido.contato','pedido.billing_address','pedido.receita'])
			->Claro()
			->ImputarDados()
			->PorPeriodo(
				$this->_formataPeriodo($lote['periodo']['de'], $dias),
				$this->_formataPeriodo($lote['periodo']['ate'], $dias)
			)
			->get()
		;

		if ($linhas->count() <= 0)
			$linhas = collect([new \stdClass()]);
			
		$linhas = $linhas->map($linhasMap);

		$arquivo = Excel::create('Demanda ' . date('d_m_Y', $date) . ' ' . $lote['titulo'] . '_Envio', function($excel) use ($linhas, $date) {

			$excel
				->setCreator('HUB')
				->setCompany('Wooza')
				->setDescription('Pedidos Claro Corp ' . \date('d/m/Y', $date))
			;

			$excel->sheet('Pedidos', function($sheet) use ($linhas) {

				$sheet->fromArray($linhas->toArray());

				$sheet->cells('A2:A10000', function($cell) {
					
					$cell->setFontFamily('Calibri');
					$cell->setFontSize(11);
					$cell->setFontWeight('bold');
				
				});

				$sheet->row(1, function($row) {
					
					$row->setFontFamily('Calibri');
					$row->setFontSize(9);
					$row->setFontWeight('bold');
					$row->setAlignment('center');
				
				});
				
			});
		

		})->store('xlsx', Storage::disk($this->disk)->path(''));

		$fileDelivered = false;
		$errorMessage = null;

		try
		{
			$iw->EnviarArquivo(Storage::disk($this->disk)->path(''), $arquivo->filename . '.' . $arquivo->ext, $lote['titulo'] . ' - ' . date('d/m', $date), date('d/m/Y', $date));
			$fileDelivered = true;
		}
		catch (\Throwable $throwable)
		{
			$fileDelivered = false;
			$errorMessage = $throwable->getMessage();
		}
		
		Mail
			::to($this->emails)
			->send(new MyMail(
				Storage::disk($this->disk)->path($arquivo->filename . '.' . $arquivo->ext),
				[
					'file_delivered'	=> $fileDelivered,
					'error_message'		=> $errorMessage,
				]
			))
		;

		Storage::disk($this->disk)->delete($arquivo->filename . '.' . $arquivo->ext);
	}

	private function _formataPeriodo ($periodo, int $modificador = 0)
	{
		$dia = strtotime($periodo['dia']);
		$dia = strtotime('-' . $modificador . ' days', $dia);

		return date('Y-m-d', $dia) . ' ' . $periodo['hora'];
	}
}