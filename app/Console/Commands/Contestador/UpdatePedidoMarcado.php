<?php
namespace App\Console\Commands\Contestador;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\Contestador\Pedido;
use App\Models\Contestador\Receita;

class UpdatePedidoMarcado extends Command
{
	protected $signature = 'contestador:update_pedido_marcado';
	
	public function handle() : void
	{
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '3G');

		$time_start = \microtime(true);

		$receitas = Receita
			::with(['arquivo','arquivo.operadora'])
			->selectRaw('`dados`->"$._pedido" as pedido')
			->addSelect('id')
			->addSelect('arquivo_id')
			->whereNull('pedido_id')
			->whereRaw('`dados`->"$._pedido" NOT IN (\'\', null)')
			->take(10000)
			->orderBy('id', 'DESC')
			->get()
		;

		if ($receitas->count() <= 0)
			return;

		$pedidos = $receitas->map(function ($receita) {
			
			return '(' . implode(',', array_map('\App\Helpers\Utils::SqlNullString', [
				$receita->pedido,
				$receita->arquivo->operadora_id
			])) . ')';

		})->implode(',');

		DB::connection('contestador')->insert(
			"INSERT INTO
				pedidos
				(
					increment_id,
					operadora_id
				)
			VALUES
				{$pedidos}
			ON DUPLICATE KEY UPDATE
				increment_id = VALUES(increment_id),
				operadora_id = VALUES(operadora_id)"
		);

		DB::connection('contestador')->select(
			"UPDATE
				receitas
				JOIN pedidos ON
					pedidos.increment_id = JSON_UNQUOTE(receitas.dados->\"$._pedido\")
			SET
				receitas.pedido_id = pedidos.id
			WHERE
				receitas.id IN (" . $receitas->pluck('id')->implode(',') . ")
			"
		);

		$time_end = \microtime(true);

		\Log::info('Processo de update de arquivo marcado concluído em ' . number_format(($time_end - $time_start), 4, '.', '') . ' segundo(s)');
	}
}