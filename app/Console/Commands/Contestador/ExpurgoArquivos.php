<?php
namespace App\Console\Commands\Contestador;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

use App\Models\Contestador\Processo;

class ExpurgoArquivos extends Command
{
	protected $signature = 'contestador:expurgo_arquivos';

	private $storage;

	public function __construct ()
	{
		parent::__construct();
		$this->storage = Storage::disk('contestador_arquivos');
	}
	
	public function handle() : void
	{
		$files = $this->storage->files(DIRECTORY_SEPARATOR);
		$permanecem = Processo::NaoExpirados()->get()->pluck('filenames')->flatten();

		foreach ($files as $file)
		{
			if (in_array($file, $permanecem))
				continue;

			// $this->storage->delete($file);
		}
	}
}