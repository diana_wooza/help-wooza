<?php
namespace App\Console\Commands\Contestador;
 
use Illuminate\Console\Command;

use App\Models\Contestador\Competencia;
use App\Jobs\Contestador\BuscarPedidosCompetenciaConsumer;
use App\Jobs\Contestador\BuscarPedidosCompetenciaCorp;

class NovaCompetencia extends Command
{
	protected $signature = 'contestador:nova_competencia';
	
	public function handle() : void
	{
		$ciclo = date('Y-m-00', strtotime('-1 month'));

		if (Competencia::where('data', $ciclo)->exists())
			return;

		$competencia = Competencia::create(['data'=>$ciclo]);

		BuscarPedidosCompetenciaConsumer::dispatch($competencia)
			->onConnection('contestador')
			->onQueue('general')
		;

		BuscarPedidosCompetenciaCorp::dispatch($competencia)
			->onConnection('contestador')
			->onQueue('general')
		;
	}
}