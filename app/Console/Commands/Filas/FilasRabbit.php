<?php
namespace App\Console\Commands\Filas;
 
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class FilasRabbit extends Command {
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'filas:filas_rabbit';

	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void {

        $connection = new AMQPStreamConnection('10.17.4.4', '5672', 'admin', 'admin');
        $channel = $connection->channel();

        $channel->queue_declare('geral.primeira_etapa_fluxo', false, true, false, false);
        $channel->basic_qos(null, 1, null);

        $callback = function($msg) {
            global $rota;
            
            $data = array('pedido' => $msg->body);
            $header = array('Content-Type: application/json');
            $ch = curl_init($rota);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_LOW_SPEED_TIME, 0);
            curl_setopt($ch, CURLOPT_LOW_SPEED_LIMIT, 0);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 0);

            try {
                $resultado = json_decode(curl_exec($ch), true);
                
                if ($resultado['OK']) {
                    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                }
                else {
                    $msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'], true);
                }
            }
            catch (\Exception $ex) {
                $msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'], true);
            }
            catch (\Error $ex) {
                $msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'], true);
            }
        };

        $channel->basic_consume('geral.primeira_etapa_fluxo', '', false, false, false, false, $callback);

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }
}