<?php
namespace App\Console\Commands\Filas;
 
use Illuminate\Console\Command;
 
class Consumer extends Command{
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'filas:consumer';

	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void {
        $config = \Kafka\ConsumerConfig::getInstance();
        $config->setMetadataRefreshIntervalMs(10000);
        $config->setMetadataBrokerList('10.17.4.4:9092');
        $config->setBrokerVersion('1.1.1');
        $config->setGroupId('test');
        $config->setTopics(['QUEUE']);
        //$config->setOffsetReset('earliest');
        $consumer = new \Kafka\Consumer();
        $consumer->start(function($topic, $part, $message) {
            var_dump($message);
        });
    }
}