<?php
namespace App\Console\Commands\Filas;
 
use Illuminate\Console\Command;
use Kafka\Producer;
use Kafka\ProducerConfig;
 
class FilasMagento extends Command{
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'filas:filas_magento';

	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void {
        $config = ProducerConfig::getInstance('10.17.4.4:9092');
        $config->setMetadataRefreshIntervalMs(10000);
        $config->setMetadataBrokerList('10.17.4.4:9092');
        $config->setBrokerVersion('1.1.1');
        $config->setRequiredAck(1);
        $config->setIsAsyn(false);
        $config->setProduceInterval(500);
        // if use ssl connect
        //$config->setSslLocalCert('/home/vagrant/code/kafka-php/ca-cert');
        //$config->setSslLocalPk('/home/vagrant/code/kafka-php/ca-key');
        //$config->setSslEnable(true);
        //$config->setSslPassphrase('123456');
        //$config->setSslPeerName('nmred');
        $producer = new Producer();

        for ($i = 0; $i < 100; $i++) {
            $result = $producer->send([
                [
                    'topic' => 'QUEUE',
                    'value' => 'test'.$i.'....message.',
                    'key' => '',
                ],
            ]);
            var_dump($result);
        }
    }
}