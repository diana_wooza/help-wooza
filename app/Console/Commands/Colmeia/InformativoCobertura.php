<?php
namespace App\Console\Commands\Colmeia;
 
use Illuminate\Console\Command;

use \App\Models\Colmeia\Config;
use \App\Models\Colmeia\Produto;
use \App\Mail\Colmeia\InformativoCobertura as EmailInformativoCobertura;
use \App\Models\Colmeia\Log\Produto as LogProduto;
 
class InformativoCobertura extends Command
{
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'colmeia:informativo_cobertura';
	
	#Variável que determina o percentual de saídas em relação a quantidade em estoque que é considerada uma quantidade alarmante de saídas
	#Ex: 0.5 significa que caso o último mês tenha uma saída igual a metade do estoque atual é considerado alarmante
	private $alerta = 0.5;
	private $periodo = '1 month';

	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void
	{
		$dataInicio	= date('Y-m-d H:i:s');
		$dataFim	= date('Y-m-d H:i:s', strtotime('-' . $this->periodo));

		$estoque = Produto::getEstoqueBySku();
		$saidas = LogProduto::getEstoqueSaida($dataInicio, $dataFim);
		$criticos = [];

		foreach ($estoque as $sku => $quantidade)
		{
			if (!isset($saidas[$sku]) || $saidas[$sku] > $quantidade * $this->$alerta)
				continue;

			$criticos = (object) [
				'sku'			=> $sku,
				'estoque'		=> $quantidade,
				'saida'			=> $saidas[$sku],
			];
		}

		if (count($criticos) <= 0)
		{
			// \Log::info('Tentativa de enviar email com informativo de cobertura, porém não há itens a serem enviados');
			return;
		}

		$usuarios = implode(',', Config::getListaUsuariosEmailCobertura()->pluck('email')->all());

		// \Log::info('Enviando email com informativo de cobertura para o(s) email(s): ' . $usuarios);
		\Mail::to($usuarios)->send(new EmailInformativoCobertura($criticos));
	}
}