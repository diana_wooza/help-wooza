<?php
namespace App\Console\Commands\Colmeia;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use \App\Models\Colmeia\Produto;
 
class AjusteEans extends Command
{
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'colmeia:ajuste_eans';
	
	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void
	{
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '3G');

		$produtos = \App\Models\Colmeia\Produto::with('nota')->PossuiDadosNota()->whereNull('integracao_jund_id')->take(2000)->get();

		if ($produtos->count() <= 0)
			return;

		$values = [];

		foreach ($produtos as $produto)
		{
			$values[] = implode(',', [
				$produto->id . ' AS id',
				'\'' . $produto->nota->nota->NFe->infNFe->det[$produto->nfe_detalhe]->prod->cEAN . '\' AS ean',
				'\'' . $produto->nota->nota->NFe->infNFe->det[$produto->nfe_detalhe]->prod->xProd . '\' AS produto',

			]);
		}

		$values = 'SELECT ' . implode(' UNION ALL ' . PHP_EOL . 'SELECT ', $values);

		DB::connection('colmeia')->select(
			"UPDATE
				produto
				JOIN
				(
					{$values}
				) AS eans ON eans.id = produto.id
				JOIN integracao_jundsoft ON integracao_jundsoft.ean = eans.ean
			SET
				produto.integracao_jund_id = integracao_jundsoft.id
			"
		);
	}
}