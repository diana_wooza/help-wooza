<?php
namespace App\Console\Commands\Colmeia;
 
use Illuminate\Console\Command;
use \App\Helpers\Magento;
use \App\Models\Colmeia\Posicao;
use \App\Models\Colmeia\Produto;
use \App\Models\Colmeia\Log\Produto as LogProduto;
 
class AtualizarEstoque extends Command
{
	/**
	 * Propriedades padrão do laravel para definição e descrição de acesso via console
	 */
	protected $signature = 'colmeia:atualizar_estoque';

	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void
	{
		try
		{
			$posicoes	= Posicao::getPedidosPosicoesReservadas();

			$pedidos	= [];
			$saidas		= [];
			$resolvidos	= [];

			foreach (Magento::getPedidoColmeia($posicoes->pluck('pedido')->all()) as $item)
				$pedidos[$item->id] = $item;
			
			foreach($posicoes as $posicao)
			{
				if (!isset($pedidos[$posicao->pedido]))
					continue;
				
				switch (@$pedidos[$posicao->pedido]->status)
				{
					case 'cancelado':
					
						$posicao->checkDescarteFazLiberacao();
						$posicao->save();

						break;

					case 'aguardando_retirada':
					case 'completo':
					case 'devolvido':
					case 'entregue':
					case 'enviado':
					case 'extraviado':
					case 'nao_ativado':
					case 'nao_entregue':
					case 'preparando_para_envio':

						$resolvidos[] = $posicao->pedido;

						if (!in_array($posicao->serial, explode(',', $pedidos[$posicao->pedido]->iccids)))
						{
							$posicao->checkDescarteFazLiberacao();
							// $posicao->cancelarReserva();
							$posicao->save();
						}
						else
						{
							$produto = $posicao->getProduto();
							
							$produto->retirar();
							$produto->save();

							$posicao->liberar();
							$posicao->save();
							
							$saidas[] = $produto;
						}

						break;

					default:

						break;

				}
			}

			LogProduto::saida($saidas, 'Retorno Automárico');
			// \Log::info('Cron Retorna chip com sucesso para pedidos: ' . implode(', ', $resolvidos));
		}
		catch (\Exception $exception)
		{
			// \Log::error('Erro Cron [' . $this->signature . ']: '. $exception->getMessage());
		}
	}
}