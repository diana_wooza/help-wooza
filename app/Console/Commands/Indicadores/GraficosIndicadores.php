<?php
namespace App\Console\Commands\Indicadores;
 
use Illuminate\Console\Command;
use \App\Models\Magento\Order;

class GraficosIndicadores extends Command 
{
    /**
     * Propriedades padrão do laravel para definição e descrição de acesso via console
    */
    protected $signature = 'indicadores:grafico_indicadores';
    
    	/**
	 * Método que é chamado ao acessar essa classe via command line
	 * @method handle
	 * @return void
	 */
	public function handle() : void
	{
		$dataAtual = date('Y-m-d H:i:s');
		$dataFim   = date($dataAtual, strtotime('1 day'));

        $prazo = Order::getDadosGraficoPrazoAtivacao($data, $dataAtual);
        $taxa  = Order::getDadosGraficoTaxaAtivacao($data, $dataAtual);
    }
}