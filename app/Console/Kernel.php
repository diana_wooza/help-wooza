<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
	/**
	* The Artisan commands provided by your application.
	*
	* @var array
	*/
	protected $commands = [

		#Alarmística
		'App\Console\Commands\Alarmistica\AlarmisticaSendSms',
		'App\Console\Commands\Alarmistica\AlarmisticaSendSms1h',
		'App\Console\Commands\Alarmistica\AlarmisticaSendSms30Min',
		'App\Console\Commands\Alarmistica\AlarmisticaSendSmsBacklog',
		
		#Colmeia
		\App\Console\Commands\Colmeia\AtualizarEstoque::class,
		\App\Console\Commands\Colmeia\InformativoCobertura::class,
		\App\Console\Commands\Colmeia\AjusteEans::class,

		#Contestador
		\App\Console\Commands\Contestador\ExpurgoArquivos::class,
		\App\Console\Commands\Contestador\NovaCompetencia::class,
		\App\Console\Commands\Contestador\UpdatePedidoMarcado::class,

		#Magento
		\App\Console\Commands\Magento_Consumer\DecriptadorPedidos::class,
		\App\Console\Commands\Magento_Corp\RelatorioPedidosClaro::class,
		
		#genéricos
		'App\Console\Commands\Lojaonline\RelatorioCrivo',
		'App\Console\Commands\Lojaonline\RelatorioBacklog',
		'App\Console\Commands\Lojaonline\RelatorioPedidos',
		'App\Console\Commands\Descriptografador',
	];

	/**
	* Define the application's command schedule.
	*
	* @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	* @return void
	*/
	protected function schedule (Schedule $schedule)
	{
		# Alarmística
		$schedule->command('alarmistica:sms')->environments('production')->everyFiveMinutes();
		$schedule->command('alarmistica:sms_30min')->environments('production')->everyThirtyMinutes();
		$schedule->command('alarmistica:sms_1h')->environments('production')->hourly();
		$schedule->command('alarmistica:sms_backlog')->environments('production')->hourlyAt(15)->between('6:00', '23:59');
		
		# Colmeia
		$schedule->command('colmeia:atualizar_estoque')->environments('production')->everyMinute()->runInBackground();
		$schedule->command('colmeia:ajuste_eans')->environments('production')->everyMinute()->runInBackground();
		#$schedule->command('colmeia:informativo_cobertura')->environments('production')->dailyAt('08:00');
		
		#contestador
		// $schedule->command('contestador:expurgo_arquivos')->environments('production')->daily();
		$schedule->command('contestador:nova_competencia')->environments('production')->monthlyOn(5, '00:00');
		$schedule->command('contestador:update_pedido_marcado')->environments('production')->everyThirtyMinutes()->runInBackground();

		# Reaproveitamento
		$schedule->command('reaproveitamento:enviar_dados_lote')->environments('production')->everyMinute();
		
		#loja Online
		$schedule->command('lojaonline:relatorio_backlog')->environments('production')->hourly()->runInBackground();
		$schedule->command('lojaonline:relatorio_crivo')->environments('production')->twiceDaily(9,13)->runInBackground();
		$schedule->command('lojaonline:relatorio_pedidos')->environments('production')->twiceDaily(9,16)->runInBackground();
		
		#magento
		// $schedule->command('magento_corp:relatorio_pedidos_claro', ['lote1'])->environments('production')->dailyAt(9)->runInBackground();
		// $schedule->command('magento_corp:relatorio_pedidos_claro', ['lote2'])->environments('production')->dailyAt(13)->runInBackground();
		// $schedule->command('magento_corp:relatorio_pedidos_claro', ['lote3'])->environments('production')->dailyAt(18)->runInBackground();
		$schedule->command('magento_corp:relatorio_pedidos_claro', ['lote_diario'])->environments('production')->dailyAt(18)->runInBackground();
	
		# Graficos indicadores
		// $schedule->command('indicadores:graficos_indicadores')->environments('production')->dailyAt('07:00');
	}

	/**
	* Register the commands for the application.
	*
	* @return void
	*/
	protected function commands()
	{
		$this->load(__DIR__.'/Commands');

		require base_path('routes/console.php');
	}
}
