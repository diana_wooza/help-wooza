**HelpWooza**
-------------

 O HelpWooza servirá como facilitador em diversas áreas no ambiente da Wooza.
 Como por exemplo o setor de gestão de pedidos, administrativo, comercial, BI etc.
 Essas áreas são "divididas" dentro do dashboard pelo menu do mesmo.

Tecnologias usadas
------------------
* Uso do dashboard X
* Láravel
* NodeJs
* MongoDB


Implementações atuais
----------------------
*Monitoria e alarmistica do Magento*

 Para atuarmos de forma eficiente e ágil nas ocorrências do Magento, precisamos criar um serviço de alertas. O serviço seria visual e dispararia comunicados avisando as ocorrências.
 
 O serviço contemplará todos os serviços críticos para o andamento da esteira, como consultas de linha e crivo, robôs de ativação, correios, antifraude, colmeia, etc.


Licença
-------

Copyright (c) 2018 `Wooza Tecnologia <http://wooza.com.br>`_.

