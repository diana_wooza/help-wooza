<?php

// namespace api;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use MongoDB\BSON\UTCDateTime;

/**
 * @ODM\Document(collection="log_tim")
 */
class LogTim
{
    /**
     * @ODM\Id
     */
    private $id;
    
    /**
     * @ODM\field(type="string")
     */
    private $message;

    /**
     * @ODM\field(name="context", type="raw")
     */
    private $context = array();

    /**
     * @ODM\field(type="int")
     */
    private $level;

    /**
     * @ODM\field(type="string")
     */
    private $level_name;

    /**
     * @ODM\field(type="string")
     */
    private $channel;

    /**
     * @ODM\field(type="date")
     */
    private $datetime;

    /**
     * @ODM\field(type="string")
     */
    private $extra;

    /**
     * Undocumented function
     *
     * @return integer
     */
    public function getId() : int
    {
        return $this->$id;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function getContext() 
    {
        $dadosContexto = $this->context;
        // $retornoDados  = $this->toArray($dadosContexto);
        return $dadosContexto;
    }

    ## Não esta sendo usada mas se for remover verifica
    Public function toArray($context) : array
    {
        $arrContext = [];
        
        if($context != ""){
            foreach($context as $item)
               if( is_array($item))
                    $arrContext[] = ['codigo_pedido' => $item];
        } else {
            $arrContext[] = ['codigo_pedido' => 'Não tem contexto'];
        }

        return $arrContext;
    }

    /**
     * Undocumented function
     *
     * @return integer
     */
    public function getLevel() : int
    {
        return $this->level;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function getLevelName() : string
    {
        return $this->level_name;
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function getChannel() : string
    {
        return $this->channel;
    }

    /**
     * Undocumented function
     *
     * @return DateTime
     */
    public function getDateTime() : date
    {
        return $this->datetime;
    }

    /**
     * Undocumented function
     *
     * @return string
     */ 
    public function getExtra() : string
    {
        return $this->extra;
    }
}