@extends('layouts.admin')

@section('title', $titulo)
@section('subtitulo')
	{!!$subtitulo!!}
@endsection
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">

					<div class="jumbotron" style="padding-top:20px;padding-bottom:20px;">
						<form action="{{route('indicadores.logisticaconsolidado')}}" class="form-horizontal form-label-left" method="get">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_mes">Ano</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="ano" id="input_mes">
												<?php foreach( $anos AS $valor ): ?>
													<option value="<?= $valor ?>" <?= ( $ano != $valor ) ? '' : 'selected'; ?>><?= $valor; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_mes">Mês</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="mes" id="input_mes">
												<option value="0" selected>Escolha o mês</option>
												<option value="1"  <?= ($mes==1)  ? 'selected' : ''; ?>>Janeiro</option>
												<option value="2"  <?= ($mes==2)  ? 'selected' : ''; ?>>Fevereiro</option>
												<option value="3"  <?= ($mes==3)  ? 'selected' : ''; ?>>Março</option>
												<option value="4"  <?= ($mes==4)  ? 'selected' : ''; ?>>Abril</option>
												<option value="5"  <?= ($mes==5)  ? 'selected' : ''; ?>>Maio</option>
												<option value="6"  <?= ($mes==6)  ? 'selected' : ''; ?>>Junho</option>
												<option value="7"  <?= ($mes==7)  ? 'selected' : ''; ?>>Julho</option>
												<option value="8"  <?= ($mes==8)  ? 'selected' : ''; ?>>Agosto</option>
												<option value="9"  <?= ($mes==9)  ? 'selected' : ''; ?>>Setembro</option>
												<option value="10" <?= ($mes==10) ? 'selected' : ''; ?>>Outubro</option>
												<option value="11" <?= ($mes==11) ? 'selected' : ''; ?>>Novembro</option>
												<option value="12" <?= ($mes==12) ? 'selected' : ''; ?>>Dezembro</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_hora">Hora</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="hora" id="input_hora">
												<option value="">Escolha a hora</option>
												<?php for ($i=0;$i<=23;$i++): ?>
													<option value="<?= $i; ?>" <?= ($hora==$i) ? 'selected' : ''; ?>><?= $i; ?></option>
												<?php endfor; ?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_operadora">Operadora</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="operadora" id="input_operadora">
												<option value="0" selected>Escolha uma operadora</option>
												<option value="Sem operadora" <?php echo ($operadora == 'Sem operadora' ? 'selected':''); ?>>Sem operadora</option>
												<option value="CLARO" <?php echo ($operadora == 'CLARO' ? 'selected':''); ?>>CLARO</option>
												<option value="TIM" <?php echo ($operadora == 'TIM' ? 'selected':''); ?>>TIM</option>
												<option value="VIVO" <?php echo ($operadora == 'VIVO' ? 'selected':''); ?>>VIVO</option>
												<option value="OI" <?php echo ($operadora == 'OI' ? 'selected':''); ?>>OI</option>
												<option value="NET" <?php echo ($operadora == 'NET' ? 'selected':''); ?>>NET</option>
												<option value="PORTO" <?php echo ($operadora == 'PORTO' ? 'selected':''); ?>>PORTO</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_segmento">Segmento</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="segmento" id="input_segmento">
												<option value="0" selected>Escolha um segmento</option>
												<option value="Sem segmento" <?= ($segmento == 'Sem segmento') ? 'selected' : ''; ?>>Sem segmento</option>
												<option value="consumer" <?= ($segmento == 'consumer') ? 'selected' : ''; ?>>consumer</option>
												<option value="varejo_loja" <?= ($segmento == 'varejo_loja') ? 'selected' : ''; ?>>varejo_loja</option>
												<option value="varejo_online" <?= ($segmento == 'varejo_online') ? 'selected' : ''; ?>>varejo_online</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_modalidade">Modalidade</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="modalidade" id="input_modalidade">
												<option value="0" selected>Escolha uma modalidade</option>
												<option value="Sem modalidade" <?= ($modalidade == 'Sem modalidade') ? 'selected' : ''; ?>>Sem modalidade</option>
												<option value="Migração" <?= ($modalidade == 'Migração') ? 'selected' : ''; ?>>Migração</option>
												<option value="Nova linha" <?= ($modalidade == 'Nova linha') ? 'selected' : ''; ?>>Nova linha</option>
												<option value="Portabilidade" <?= ($modalidade == 'Portabilidade') ? 'selected' : ''; ?>>Portabilidade</option>
												<option value="SVA" <?= ($modalidade == 'SVA') ? 'selected' : ''; ?>>SVA</option>
												<option value="Upgrade" <?= ($modalidade == 'Upgrade') ? 'selected' : ''; ?>>Upgrade</option>
											</select>
										</div>
									</div>   
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="input_tipo">Tipo</label>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<select class="form-control" name="tipo" id="input_tipo">
												<option value="0" selected>Escolha um tipo</option>
												<option value="Sem tipo" <?= ($tipo == 'Sem tipo') ? 'selected' :''; ?>>Sem tipo</option>
												<option value="Combo"   <?= ($tipo == 'Combo') ? 'selected' :''; ?>>Combo</option>
												<option value="Controle" <?= ($tipo == 'Controle') ? 'selected' :''; ?>>Controle</option>
												<option value="Fixo" <?= ($tipo == 'Fixo') ? 'selected' :''; ?>>Fixo</option>
												<option value="Internet" <?= ($tipo == 'Internet') ? 'selected' :''; ?>>Internet</option>
												<option value="Internet Fixa" <?= ($tipo == 'Internet Fixa') ? 'selected' :''; ?>>Internet Fixa</option>
												<option value="Pós" <?= ($tipo == 'Pós') ? 'selected' :''; ?>>Pós</option>
												<option value="SVA" <?= ($tipo == 'SVA') ? 'selected' :''; ?>>SVA</option>
												<option value="TV por Assinatura" <?= ($tipo == 'TV por Assinatura') ? 'selected' :''; ?>>TV por Assinatura</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<button type="submit" class="btn btn-primary btn-block">Gerar Relatório</button>
								</div>
							</div>
						</form>
					</div>

					<!-- início: separação por operadora -->
					<table class="table table-striped table-bordered table-responsive">
						<thead>
							<tr>
								<th style="text-align: left">DESDOBRAMENTO POR OPERADORA</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($collectionOperadora as $itemOperadora => $dados): ?>
								<table class="table table-striped table-bordered table-responsive">
									<?php $firstItem = true; ?>
									<?php foreach ($dados as $nomeOperadora => $dadosOperadora): ?>
										<?php if( $firstItem ): ?>
											<thead>
												<tr>
													<th style="text-align: left"><?= $itemOperadora; ?></th>
													<?php foreach ($diasOperadora[$itemOperadora] as $diaOperadora): ?>
														<th style="text-align: center"><?= $diaOperadora; ?></th>
													<?php endforeach; ?>
												</tr>
											</thead>
											<?php $firstItem = false; ?>
										<?php endif; ?>

									<tbody>
										<tr>
											<td><?= $nomeOperadora; ?></td>
											<?php foreach ($dadosOperadora as $rowDadosOperadora => $valorOperadora): ?>
												<td>
													<?= number_format($valorOperadora, 0, ' ', '.'); ?>
												</td>
											<?php endforeach; ?>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
							<?php endforeach; ?>
						</tbody>
					</table>
					<!-- fim: separação por operadora -->
					<!-- início: separação por modalidade -->
					<table class="table table-striped table-bordered table-responsive">
						<thead>
							<tr>
								<th style="text-align: left">DESDOBRAMENTO POR MODALIDADE</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($collectionModalidade as $modalidade => $dados): ?>
								<table class="table table-striped table-bordered table-responsive">
									<?php $firstItem = true; ?>
									<?php if( strlen($modalidade) ): ?>
										<?php foreach ($dados as $nomeModalidade => $dadosModalidade): ?>
											<?php if( $firstItem ): ?>
												<thead>
													<tr>
														<th style="text-align: left"><?= $modalidade; ?></th>
														<?php foreach ($diasModalidade[$modalidade] as $diaModalidade): ?>
															<th style="text-align: center"><?= $diaModalidade; ?></th>
														<?php endforeach; ?>
													</tr>
												</thead>
												<?php $firstItem = false; ?>
											<?php endif; ?>

										<tbody>
											<tr>
												<td><?= $nomeModalidade; ?></td>
												<?php foreach ($dadosModalidade as $rowDadosModalidade => $valorModalidade): ?>
													<td>
														<?= number_format($valorModalidade, 0, ' ', '.'); ?>
													</td>
												<?php endforeach; ?>
											</tr>
										<?php endforeach; ?>
										</tbody>
									<?php endif; ?>
								</table>
							<?php endforeach; ?>
						</tbody>
					</table>
					<!-- fim: separação por modalidade -->
					
					<!-- início: separação por segmento -->
					<table class="table table-striped table-bordered table-responsive">
						<thead>
							<tr>
								<th style="text-align: left">DESDOBRAMENTO POR SEGMENTO</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($collectionSegmento as $segmento => $dados): ?>
								<table class="table table-striped table-bordered table-responsive">
									<?php $firstItem = true; ?>
									<?php if( strlen($segmento) ): ?>
										<?php foreach ($dados as $nomeSegmento => $dadosSegmento): ?>
											<?php if( $firstItem ): ?>
												<thead>
													<tr>
														<th style="text-align: left"><?= $segmento; ?></th>
														<?php foreach ($diasSegmento[$segmento] as $diaSegmento): ?>
															<th style="text-align: center"><?= $diaSegmento; ?></th>
														<?php endforeach; ?>
													</tr>
												</thead>
												<?php $firstItem = false; ?>
											<?php endif; ?>

										<tbody>
											<tr>
												<td><?= $nomeSegmento; ?></td>
												<?php foreach ($dadosSegmento as $rowDadosSegmento => $valorSegmento): ?>
													<td>
														<?= number_format($valorSegmento, 0, ' ', '.'); ?>
													</td>
												<?php endforeach; ?>
											</tr>
										<?php endforeach; ?>
										</tbody>
									<?php endif; ?>
								</table>
							<?php endforeach; ?>
						</tbody>
					</table>
					<!-- fim: separação por segmento -->

					<!-- início: separação por tipo -->
					<table class="table table-striped table-bordered table-responsive">
						<thead>
							<tr>
								<th style="text-align: left">DESDOBRAMENTO POR TIPO</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($collectionTipo as $tipo => $dados): ?>
								<table class="table table-striped table-bordered table-responsive">
									<?php if( strlen($tipo) ): ?>
										<?php $firstItem = true; ?>
										<?php foreach ($dados as $nomeTipo => $dadosTipo): ?>
											<?php if( $firstItem ): ?>
												<thead>
													<tr>
														<th style="text-align: left"><?= $tipo; ?></th>
														<?php foreach ($diasTipo[$tipo] as $diaTipo): ?>
															<th style="text-align: center"><?= $diaTipo; ?></th>
														<?php endforeach; ?>
													</tr>
												</thead>
												<?php $firstItem = false; ?>
											<?php endif; ?>

										<tbody>
											<tr>
												<td><?= $nomeTipo; ?></td>
												<?php foreach ($dadosTipo as $rowDadosTipo => $valorTipo): ?>
													<td>
														<?= number_format($valorTipo, 0, ' ', '.'); ?>
													</td>
												<?php endforeach; ?>
											</tr>
										<?php endforeach; ?>
										</tbody>
									<?php endif; ?>
								</table>
							<?php endforeach; ?>
						</tbody>
					</table>
					<!-- fim: separação por tipo -->
				</div>
			</div>
		</div>
	</div>
</div>

@endsection