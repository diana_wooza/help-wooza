@extends('layouts.admin')
@section('title', 'Indicadores')
@section('subtitle', 'Cadastrar Meta')
@section('content')

<!-- erros do formulario, caso não seja preenchido -->
@if(!empty($errors->any()))
	@foreach ($errors->all() as $error)
		@alerta()
			{{ $error }}
		@endalerta
	@endforeach
@endif

@push('header.css')
<style>
  .required:after {
    content: " *";
    color: red;
  }
</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
  <!-- Select2 -->
  <link rel="stylesheet" href="{{url('bower_components/select2/dist/css/select2.min.css')}}">
@endpush

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-10">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Cadastre as metas das operadoras</h3>
				</div>
				<div class="box-body">
					<!-- /.box-header -->
					<form class="form-horizontal" action="{{route('indicadores.graficosindicadores.cadastrarMetas')}}" id="form" role="form" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label required">Tribo</label>
                            <div class="col-md-3 col-xs-6">
                                <select id="input-tribo" class="form-control" name="tribo" data-error="Por favor, informe uma tribo." required>
                                    <option value="" selected>Selecione...</option>
                                    <option @if(old('tribo') == "Tribo Banda Larga") selected @endif value="Tribo Banda Larga">Tribo Banda Larga</option>
                                    <option @if(old('tribo') == "Tribo Claro") selected @endif value="Tribo Claro">Tribo Claro</option>
                                    <option @if(old('tribo') == "Tribo Loja Online") selected @endif value="Tribo Loja Online">Tribo Loja Online</option>
                                    <option @if(old('tribo') == "Tribo Microsoft") selected @endif value="Tribo Microsoft">Tribo Microsoft</option>
                                    <option @if(old('tribo') == "Tribo Oi") selected @endif value="Tribo Oi">Tribo Oi</option>
                                    <option @if(old('tribo') == "Tribo Vivo") selected @endif value="Tribo Vivo">Tribo Vivo</option>
                                    <option @if(old('tribo') == "Tribo Tim") selected @endif value="Tribo Tim">Tribo Tim</option>
                                    <option @if(old('tribo') == "Tribo Varejo") selected @endif value="Tribo Varejo">Tribo Varejo</option>
                                </select>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label required">Mês/Ano</label>
                            <div class="col-md-3 col-xs-6">
                                    <div  style="display:" class='input-group date' id='dateMeta'>
                                        <div class="input-group input-group-sm">
                                            <input data-error="Por favor, informe uma data correta." required type='text' name='dtMeta' class="form-control" id='dataMeta' placeholder="Mes" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                        <div id="campos" class="col-md-10 img-thumbnail" style="border:none">
                            <div class='form-group'>
                                <label class="col-sm-3 col-xs-12 control-label" title="Não pode duplica os itens!">Escolha operadora / produto</label>
                            </div>
                            <div class='form-group'>
                                <label class="col-md-3 col-xs-12 control-label required" title="Tipos de segmentos.">Métrica</label>
                                <div class="col-md-3">
                                    <select id="tipos_1" class="form-control select2 tipos" name="tipos[]" data-error="Por favor, informe um produto ou operadora." required>
                                        <option value="" selected>Selecione...</option>
                                        <option value="Claro">Claro</option>
                                        <option value="Corp">Corporativo</option>
                                        <option value="Móvel">Móvel</option>
                                        <option value="net">Net</option>
                                        <option value="nextel">Nextel</option>
                                        <option value="office">Office</option>
                                        <option value="oi">Oi</option>
                                        <option value="SVA">SVA</option>
                                        <option value="tim">Tim</option>
                                        <option value="timLive">Tim Live</option>
                                        <option value="vivo">Vivo</option>
                                    </select>
                                </div>
                                <div class="form-group ">
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="metrica[]" id="metrica_1" placeholder="insira o valor" data-error="Por favor, informe um valor." required>
                                        <button type="button" hidden class="excluir" id="" ></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer clearfix no-border" >
                            <button type="button" class="btn btn-default btn-success pull-right" id="btn-add-item" title="Adicione o(s) produto(s) ou operadora(s) para exibição." style=""><i class="fa fa-plus"></i> Add</button>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" href="{{route('indicadores.graficosindicadores.cadastrarMetas')}}" data-dismiss="modal" id="btn-cancel">Cancelar</a>
                            <button type="button" id="btn-cadastrar" data-toggle="modal" data-target="" class="btn btn-primary" >Cadastrar</button>
                        </div>
				</div>
				<div class="modal fade" id="bs-example-modal-sm" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content col-md-8">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Atenção!</h4>
							</div>
							<div class="modal-body">
								<p>Tem certeza que deseja cadastrar esta meta?</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								<button type="submit" data-toggle="tooltip" data-placement="left" class="btn btn-danger">Salvar</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                {{-- modal de erro duplicados --}}
                <div class="modal modal-danger fade" id="duplicados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Atenção!</h4>
                            </div>
                            <div class="modal-body">
                                <p>Existem tipos de produtos ou operadoras duplicados, por favor verifique?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-right" style="border: 1px solid #fff;" data-dismiss="modal">
                                    <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fechar</font></font>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- fim modal erro --}}
			</form>
		</div>
</section>
@endsection
@push('body.javascript')

<!-- <script src="{{asset('js/help_wooza.js')}}"></script> -->
<script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{url('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<!-- date-range-picker -->
<script src="{{url('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{url('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{url('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('js/cadastrarMetasIndicadores.js')}}"></script>
@endpush