@extends('layouts.admin')
@section('title', 'Indicadores')
@section('subtitle', '/ Gráficos')
@section('content')
@if(!empty($errors->first()))
@alerta()
{{ $errors->first() }}
@endalerta
@endif
<!-- Content Wrapper. Contains page content -->
@push('header.css')
<style>
.required:after {
    content: " *";
    color: red;
}

</style>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" />
<!-- Morris chart -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<link rel="stylesheet" href="{{ url('bower_components/morris.js/morris.css') }}">
<link rel="stylesheet" href="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
@endpush
<!-- Main content -->
<section class="content">
    {{-- <form action='{{ route('indicadores.prazoativacao.getDadosGraficos') }}' method='post'> --}}
    <form >
    <div class="col-xs-10">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Faça sua pesquisa</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <!-- /.form-group -->
                    <div class="col-md-6">
                      <div class="form-group">
                          <label class='required'>Tipo de gráfico</label>
                          <select required id='select-tipo-grafico' name='tipo-grafico' class="form-control select2" style="width: 100%;">
                              <option value="" selected>Selecione</option>
                              <option value="0" >Prazo ativação</option>
                              <option value="1" >Taxa de ativação e cancelamento</option>
                              <option value="2" >Tempo de entrega Gross</option>
                              <option value="3" >Meta geral</option>
                          </select>
                      </div>
                      <div class="form-group" style="display:none" id='operadora'>
                        <label class="required">Operadora</label>
                        <select required id="select-operadora" name='operadora' class="form-control select2" style="width: 100%;">
                          <option value="" selected>Selecione</option>
                          <option value="claro">Claro</option>
                          <option value="nextel">Nextel</option>
                          <option value="oi">Oi</option>
                          <option value="tim">Tim</option>
                          <option value="vivo">Vivo</option>
                        </select>
                      </div>
                  </div>
                  <div class="col-md-6">
                        <!-- /.form-group -->
                        <div class="form-group" style="display:none" id='segmento'>
                            <label class="required">Segmento</label>
                            <select required id="select-segmento" name='segmento' class="form-control select2" style="width: 100%;">
                                    <option value="" selected>Selecione</option>
                                @foreach($segmentos as $segmento)
                                    <option value="{{ $segmento->segmento }}">{{ ucfirst(str_replace('_',' ',$segmento->segmento)) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div style="display:none" class="overlay carregamento">
                        <i class="fa fa-spin fa-spinner fa-4x" aria-hidden="true"></i>
                      </div>
                    <div class="col-md-6">
                        <!-- /.form-group -->
                        <div style="display:none" class='input-group date' id='date'>
                            <label class="required">Data inicial</label>
                            <div class="input-group input-group-sm">
                                <input required type='text' name='dtFim' class="form-control" id='dataInicio' placeholder="Inicio" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <label class="required">Data final</label>
                            <div class="input-group input-group-sm">
                                <input required type='text' name='dtInicio' class="form-control" id='dataFim' placeholder="Fim" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div  style="display:none" class='input-group date' id='dateMeta'>
                            <label class="required">Mês/Ano</label>
                            <div class="input-group input-group-sm">
                                <input required type='text' name='dtMeta' class="form-control" id='dataMeta' placeholder="Mes" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="box-footer">
                            <button type="button" class="btn btn-primary btn-flat pull-right " id="btn-pesquisa">Buscar</button>
                            {{-- <button type="submit" class="btn btn-primary btn-flat pull-right " id="btn-pesquisa-api">Buscar</button> --}}
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
      <!-- fechamento da div pesquisa-->
</form>

<div class="row">
</div>

<form>
    <!-- Grafico prazo ativacao -->
    <div style='display:none' id='grafico-prazo-ativacao' class="box-body" >
        <div id='grafico1' class="nav-tabs-custom box-success box-body">
                <!-- Tabs within a box -->
            <div class="box-header with-border">
                <img src="{{ asset('img/wooza/logo.jpg')}}" style="height: 37px;" />
                <span class="box-title" style="left: 15px;position: relative;" id='nome-operadora'></span>
            </div>
            <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div class="chart tab-pane active" id="line-chart"></div>
            </div>
            <!-- /.box informacões na grid-->
            <div class="box-header" style="bottom: -3px">
                <h3 class="box-title">Percentuais</h3>
            </div>
            <!-- /.box-header -->
            <div class="tab-content no-padding table-responsive">
                <table id='grid-prazo-ativacao' style="font-size: 11px;" class="table table-striped table-sm"></table>
            </div>           
        </div>
    </div>

    <!-- Grafico taxa ativacao -->
    <div style="display:none" class="box-body" id="grafico-taxa-ativacao">
            <div id="grafico2" class="box box-success box-body" style="width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">Taxa de ativação e etapa da esteira</h3>
                    {{-- <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div> --}}
                </div>
                <div id="bar-taxa-ativacao" class="chart">
                    <canvas id="barChart"></canvas>
                </div>
                <!-- /.box informacões na grid-->
                <h3 class="box-title">Percentuais</h3>
                <!-- /.box-header -->
                <div class="table-responsive">
                    <table id='grid-taxa-ativacao' style="font-size: 11px;" class="table"></table>
                </div>
            </div>
            <!-- /.box-body -->
    </div>
    {{-- fim taxa ativacao --}}
    <!-- Grafico metas -->
    <div style="display: none" class="box-body" id="grafico-metas">
        <div id="grafico3" class="box box-success box-body" style="width: 100%;">
            <div class="box-header with-border">
                <h3 class="box-title" id='mes-meta'>Metas</h3>
                {{-- <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div> --}}
            </div>
            <div class="col-sm-12 col-xs-12 ">
                <div class="col-md-3 col-sm-6 col-xs-12" style="right: 18px; position:relative"> 
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/icon_claro.png')}}" style="height: 120px;" /></center>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/icon_tim.png')}}" style="height: 120px;" /></center>
                        </div>
                    </div>
                    <div class="row" style="top: 15px;padding-bottom: 28px;position: relative;">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/boneco_claro.png')}}" style="height: 37px;" /></center>
                            <center><span class="hidden-xs" style="font-size: 11px;white-space: nowrap;top :6px;position: relative;font-weight: 600;"> Tribo Claro</span></center>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/boneco_tim.png')}}" style="height: 37px;" /></center>
                            <center><span class="hidden-xs" style="font-size: 11px;white-space: nowrap;top :6px;position: relative;font-weight: 600;"> Tribo Tim</span></center>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center>
                            <table class="table table-sm" >
                                <tbody style="border-top:none !important;" id="tribo-claro"></tbody>
                            </table>
                        </center>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center>
                            <table class="table table-sm">
                                <tbody id="tribo-tim"></tbody>
                            </table>
                        </center>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-8 col-xs-12">
                    <div class="row">
                        {{-- <div class="col-md-6 col-sm-6 col-xs-12"> --}}
                            <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/oi_vivo.png')}}" style="height: 120px; left:-2px;position:relative" /></center>
                        {{-- </div> --}}
                    </div>
                    <div class="row" style="top: 15px;padding-bottom: 28px;position: relative;">
                        <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/boneco_oi_vivo.png')}}" style="height: 37px;" /></center>
                        <center><span class="hidden-xs" style="font-size: 11px;white-space: nowrap;top :6px;position: relative;font-weight: 600;"> Tribo Oi/Vivo</span></center>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table table-sm">
                            <tbody id='tribo-oi'></tbody>
                          </table>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table table-sm">
                            <tbody id='tribo-vivo'></tbody>
                          </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/banda_larga.png')}}" style="height: 120px;" /></center>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/varejo.png')}}" style="height: 120px;" /></center>
                        </div>
                    </div>
                    <div class="row" style="top: 15px;padding-bottom: 28px;position: relative;">
                        <div class="col-md-6 col-sm-6 col-xs-12"><center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/boneco_banda_larga.png')}}" style="height: 37px;" /></center>
                            <center><span class="hidden-xs" style="font-size: 11px;white-space: nowrap;top :6px;position: relative;font-weight: 600;"> Tribo Banda Larga</span></center>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12"><center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/boneco_varejo.png')}}" style="height: 37px;" /></center>
                            <center><span class="hidden-xs" style="font-size: 11px;white-space: nowrap;top :6px;position: relative;font-weight: 600;"> Tribo Varejo</span></center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table table-sm" >
                              <tbody style="border-top:none !important;" id='tribo-banda-larga'></tbody>
                            </table>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table table-sm">
                                <tbody id='tribo-varejo'></tbody>
                              </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/loja_online.png')}}" style="height: 120px;" /></center>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12"><center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/microsoft.png')}}" style="height: 120px;" /></center></div>
                    </div>
                    <div class="row" style="top: 15px;padding-bottom: 28px;position: relative;">
                        <div class="col-md-6 col-sm-6 col-xs-12"><center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/boneco_loja_online.png')}}" style="height: 37px;" /></center>
                            <center><span class="hidden-xs" style="font-size: 11px;white-space: nowrap;top :6px;position: relative;font-weight: 600;"> Tribo Loja Online</span></center>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12"><center><img class="img-fluid" src="{{ asset('img/wooza/grafico_metas/boneco_microsoft.png')}}" style="height: 37px;" /></center>
                            <center><span class="hidden-xs" style="font-size: 11px;white-space: nowrap;top :6px;position: relative;font-weight: 600;"> Tribo Microsoft</span></center>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table table-sm" >
                              <tbody style="border-top:none !important;" id='tribo-loja-online'></tbody>
                            </table>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table table-sm">
                                <tbody id='tribo-microsoft'></tbody>
                              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
              <!-- /.box-body -->
    </div>
    {{-- fim metas --}}
    {{-- Botão salvar --}}
    <div class="row"></div>
    <div style="display:none" id="div-salvar" style="width:100%">
        <a class='btn btn-primary btn-small' id="salvar">
            <i class='fa fa-save'></i>
            Salvar
        </a>
    </div>
    {{-- Aviso não há dados para a pesquisa --}}
    <div style='display:none' id="sem-dados">
            <div class="callout callout-warning">
                <h4>Não existem informações a serem exibidas com o seu filtro!</h4>
                
                <p>Por favor, pesquise novamente.</p>
            </div>
        </div>            
</form>

</section>
@endsection

@push('body.javascript')
<!-- Bootstrap 3.3.7 -->
<script src="{{url('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- DataTables -->
<script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{url('bower_components/moment/min/moment.min.js')}}"></script>
<!-- daterangepicker -->
<script src="http://www.cloudformatter.com/Resources/Pages/CSS2Pdf/Script/xeponline.jqplugin.js"></script>
<script src="{{url('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{url('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{url('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script>
    $(function() {
        //Date picker with time picker
        $('#dataInicio').datetimepicker({
            locale: 'ru',
                format: 'DD/MM/YYYY HH:mm:ss',
                defaultDate: new Date()
        });
        $('#dataMeta').datetimepicker({
            locale: 'ru',
                format: 'MM/YYYY',
                defaultDate: new Date()
        });
            $('#dataFim').datetimepicker({
                locale: 'ru',
                format: 'DD/MM/YYYY HH:mm:ss',
                defaultDate: new Date()
            });
            
            $('#dataInicio').datetimepicker();
            $('#dataFim').datetimepicker({
                useCurrent: false //Important! See issue #1075
            });
            $("#dataInicio").on("dp.change", function(e) {
                $('#dataFim').data("DateTimePicker").minDate(e.date);
            });
            $("#dataFim").on("dp.change", function(e) {
                $('#dataInicio').data("DateTimePicker").maxDate(e.date);
            });
        })

    url = '{{ url('') }}';
</script>
<script src="{{ url('js/help_wooza.js')}}"></script>
<script src="{{ url('bower_components/raphael/raphael.min.js')}}"></script> 
<script src="{{ url('bower_components/morris.js/morris.js')}}"></script> 
{{-- <script src="{{ url('bower_components/chart.js/Chart.js')}}"></script>  --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
{{-- <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script> --}}
<!-- Sparkline -->
<script src="{{ url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script> 
{{-- <script src="{{ url('bower_components/chart.js/chart.js')}}"></script>  --}}
<script src="{{ url('js/graficosIndicadores.js')}}"></script>
@endpush