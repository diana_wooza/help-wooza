<div class="form-group">
	<div class="col-sm-12">
		<textarea class="form-control" name="{{$input}}" style="height:300px;"></textarea>
		<p class="help-block">{{$label}}</p>
	</div>
</div>