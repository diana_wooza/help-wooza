@extends('layouts.admin')

@section('title', $exportador->nome)
@section('subtitle', 'Exporta um arquivo com os dados')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Configurações do Exportador</h3>
			</div>
			<form name="planilha" class="form-horizontal" method="post" action="" enctype="multipart/form-data">
				@csrf
				<div class="box-body">
					@include($obtencao->view, $obtencao->viewData)
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2 text-right">
							<button type="submit" class="btn btn-info">Realizar Exportação</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection