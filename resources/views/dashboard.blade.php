@extends('layouts.admin')

@section('title', 'Bem vindo')
@section('subtitle', 'Sua Página Inicial')
@section('content')

	
<!-- Carrega view principal-->
@if (Auth::user()->checkPermissao('login'))

	@include('components.dashboard_inicial')

@endif

@if (Auth::user()->checkPermissao('contestador'))

	<div class="row">
		@include('contestador/dashboard')
		<div class="clearfix visible-sm-block"></div>
	</div>

@endif

@if (Auth::user()->checkPermissao('colmeia'))

	<div class="row">
		@include('colmeia.dashboard')
		<div class="clearfix visible-sm-block"></div>
	</div>

@endif

@if(!Auth::user()->hasPerfil())

	<div class="row">
		<div class="welcome-area">
			<h2>Olá {{\Auth::user()->nome}}, tudo bom com você?</h2>
			<p>Você é novo por aqui, não é?<p>
			<p>Ao que parece nenhum item foi atribuído para você ainda, mas isso é super fácil de resolver. Basta mandar um e-mail para essa galera que está listada aqui em baixo como administradores (se você clicar no email eu até já deixei as coisas meio que prontas, só você personalizar)!</p>
			<p>Caso você só esteja logado para ficar disponível no cadastro (ou caso você só esteja dando uma olhadinha) tudo bem também!</p>
			<p>Administradores: {!! implode(', ', array_map(function ($item) {return '<a href="mailto:' . $item->email . '?subject=Preciso de acesso!&body=Olá ' . $item->nome . ', poderia atribuir meus devidos acessos?">' . $item->nome . '</a>';}, \App\Models\Config::getUsuariosContatoHub()->all())) !!}</p>
			<br />
			<p>Tenha um excelente dia de trabalho!</p>
		</div>
		<div class="clearfix visible-sm-block"></div>
	</div>

	<style type="text/css">

		.welcome-area {
			width:calc(100% - 500px);
			margin:60px auto 0px;
		}
		.welcome-area h2 {
			color:#c4d82d;
			text-align:center;
			font-size:60px;
			margin-bottom:50px;
		}
		.welcome-area p {
			text-align:center;
		}

	</style>

@endif

</div>

@endsection