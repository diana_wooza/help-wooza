@foreach ($itens as $item)

	@if (!$item->hasPermissao())

		@continue

	@endif

	<li class="{{$item->hasSubitens() ? 'treeview' : ''}} {{$item->printiFActive('active')}}">

		<a href="{{$item->link()}}">
		
			<i class="{{$item->icone()}}"></i>
			<span>{{$item->rotulo()}}</span>

			@if ($item->hasSubitens())

				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>

			@endif

		</a>

		@if ($item->hasSubitens())

			<ul class="treeview-menu">

				@include('components.menu_item', ['itens' => $item->getSubitens()])

			</ul>

		@endif

	</li>

@endforeach