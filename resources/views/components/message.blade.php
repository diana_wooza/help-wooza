<div class="callout callout-danger">
	<h4>{{__('common.messages.error.title')}}</h4>
	<p>{!! $slot !!}</p>
</div>