@if ($menu->hasPermissao())

	<li class="header" title="{{$menu->rotulo()}}">
		{{$menu->rotulo()}}
	</li>
	@include('components.menu_item', ['itens' => $menu->itens()])

@endif