@extends('layouts.admin')
@section('title', 'Histórico de Configuração de Tratativas Automáticas')
@push('header.javascript')
    <script>
        var jsonConfiguracao = 
        {
            "nome": null,
            "interface": [],
            "configuracao": [],
            "json_robo": [],
            "usuario_padrao": null,
        }
        var jsonTratativa = 
        {
            "nome": null,
            "condicoes": [],
            "regras": []
        }
    </script>
    
    <script src="{{url('js/configuracao/jquery-sortable-min.js')}}"></script>
    <script src="{{url('js/configuracao/configuracao_tratativas_html.js')}}<?php echo '?v='.filemtime("js/configuracao/configuracao_tratativas_html.js"); ?>"></script>
    <script src="{{url('js/configuracao/configuracao.js')}}<?php echo '?v='.filemtime("js/configuracao/configuracao.js"); ?>"></script>
    <script src="{{url('js/configuracao/configuracao_historico.js')}}<?php echo '?v='.filemtime("js/configuracao/configuracao_historico.js"); ?>"></script>
    <script src="{{url('js/configuracao/configuracao_tratativas.js')}}<?php echo '?v='.filemtime("js/configuracao/configuracao_tratativas.js"); ?>"></script>
    <script src="{{url('js/configuracao/configuracao_tratativas_historico.js')}}<?php echo '?v='.filemtime("js/configuracao/configuracao_tratativas_historico.js"); ?>"></script>
    <script src="{{url('js/configuracao/select2.full.js')}}<?php echo '?v='.filemtime("js/configuracao/select2.full.js"); ?>"></script>
    <script src="{{url('js/configuracao/pt-BR.js')}}<?php echo '?v='.filemtime("js/configuracao/pt-BR.js"); ?>"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.full.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/i18n/pt-BR.js"></script> -->
@endpush
@push('header.css')
    <link rel="stylesheet" href="{{url('css/configuracao/configuracao_tratativas.css')}}">
    <link rel="stylesheet" href="{{url('css/configuracao/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{url('css/configuracao/select2.min.css')}}">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" /> -->
@endpush
@section('content')
<div class="modal fade" id='modal_campo_valor'  role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">titulo</h4>
            </div>
            <div class="modal-body">
                <form id='form_campo_valor'>
                    <div class="form-group">
                        <label class='label_campo'></label>
                        <select class="form-control select_campo" id='select_campo'>
                        </select>
                    </div>
                    <div class="form-group comparacao_modal" style="display:none">
                        <label>Comparação</label>
                        <br>
                        <select class='form-control' id='input_comparacao_modal'>
                            <option>=</option>
                            <option>&cong;</option>
                            <option>!=</option>
                            <option>!&cong;</option>
                        </select>
                    </div>
                    <i class="fa fa-cog fa-spin fa-1x fa-fw carregando" style="display: none"></i>
                    <div class="form-group valor_modal" style="display:none">
                        <label class='label_valor'></label>
                        <br>
                        <select class='form-control' id='input_valor_modal' style="width: 100%" multiple>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success salvar_campo_valor">Salvar</button>
                <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class='col-md-4'>
        <label>Processo</label>
        <select class="form-control" id='select_robo'>
            <option value=''>Selecione</option>
            @foreach($tratativas as $tratativa)
                <option value="{{$tratativa['nome']}}">{{$tratativa['label']}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="row" id='div_data_historico' style="display:none">
    <div class="row main row_salvar_configuracao" id='div_configuracao_lista_tratativas'>
        <div class='col-md-3'>
            <label>Versão</label>
            <select class="form-control" id='versao'  name='versao'>
                <option value=''>Selecione</option>
            </select>
        </div>
    </div>
</div>
<div class="row" id='div_configuracao_lista_tratativas_historico' style="display:none">
    <div class="row main row_salvar_configuracao" id='div_configuracao_lista_tratativas'>
        <div class='col-md-3'>
            <label>Usuário Padrão</label>
            <select class="form-control" id='usuario_padrao'  name='usuario_padrao'>
                <option value=''>Selecione</option>
            </select>
        </div>
    </div>
    <div class='col-md-12 div_lista_tratativas'>
        <label>Tratativas</label>
        <ol class="list-group lista_tratativas"></ol>
        <button type="button" class="btn btn-xs btn-white btn_adicionar_tratativa">
            <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Tratativa
        </button>
    </div>
</div>
<div class="row main" id='div_configuracao_tratativas' style="display: none">
    <div class="col-md-12">
        <div class="well well-sm well_tratativa">
            <div class="row center main row_tratativa">
                <div class="col-sm-10">
                    <i class='fa fa-arrow-left exibir_tratativas' aria-hidden='true'></i>&nbsp;
                    <label>Tratativa: </label>
                    <label class='label_nome_tratativa'></label>
                </div>
                <div class='col-sm-2 btn-right'>
                    <i class='fa fa-pencil editar_tratativa' aria-hidden='true'></i>
                </div>
            </div>
            <div class="row main">
                <div class="col-sm-12">
                    <div class="row row_titulo_regras">
                        <div class="col-sm-12 titulo condicoes titulo_caixa">Condições do Pedido</div>
                    </div>
                    <div class="row row_condicoes">
                        <div class="col-sm-1 coluna_adicionar_grupo_condicoes">
                            <button type="button" class="btn btn-xs btn-success btn_adicionar_grupo_condicoes">
                                <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Grupo de Condições
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 titulo regras titulo_caixa">
                    Regras
                    <i class="fa fa-compress icone_regras comprimir btn-right" aria-hidden="true"></i>
                </div>
                <div class="col-sm-12">
                    <ol class='lista_regras'></ol>
                </div>
                <div class="col-sm-1 coluna_adicionar_regra">
                    <button type="button" class="btn btn-xs btn-dark btn_adicionar_regra">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Regra
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row main div_json_robo_historico" style="display: none">
    <div class='col-md-6'>
        <label>Json de Retorno do Robô</label>
        <textarea class="form-control" id='json_robo' name='json_robo' style='height:200px'></textarea>
    </div>
</div>
@endsection