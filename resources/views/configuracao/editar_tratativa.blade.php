@extends('layouts.admin')

@section('title', 'Gerenciar Tratativa: '.$tratativa->label)

@push('header.javascript')
<script>
    $(function() {
        $("#submit_excluir").click(function() {
            if (confirm('Excluir tratativa?')) {
                $(form).submit();
            }

            return false;
        })
    });
</script>
@endpush

@section('content')

@if ($salvo)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <p><b>Salvo com sucesso</b></p>
        </div>
    </div>
</div>
@endif

<form class="row" method="post" action="{{route('configuracao.editar_tratativa', ['id' => $tratativa->id])}}">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
            <div class="box-body">
                <div class="form-group">
                    <label for="rotulo" class="col-sm-12 control-label">Rótulo</label>
                    <input type="text" class="form-control" name="rotulo" value="{{$tratativa->label}}"/>
                </div>
                <div class="form-group">
                    <label for="nome" class="col-sm-12 control-label">Nome (operadora.nome_processo)</label>
                    <input type="text" class="form-control" name="nome" value="{{$tratativa->nome}}"/>
                </div>
                <div class="form-group">
                    <label for="status" class="col-sm-12 control-label">Ações</label>
                    <select class="form-control selectpicker" name="acoes[]" data-action-box="1" data-deselect-all-text="Remover seleção" data-select-all-text="Selecionar todos" data-none-selected-text="Nenhuma ação associada" multiple>
                        @foreach ($acoes as $acao)
                            <option value="{{$acao->id}}" {{(is_array($acoes_tratativa) && in_array($acao->id, $acoes_tratativa)) ? 'selected' : ''}}>{{$acao->label}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success" name='submit' value='salvar'>Salvar</button>    
                        <button type="submit" class="btn btn-danger" name='submit' id='submit_excluir' value='excluir'>Excluir</button>
                    </div>
                </div>
            </div>
		</div>
	</div>	
</form>

@endsection