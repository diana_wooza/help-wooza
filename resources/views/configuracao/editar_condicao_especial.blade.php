@extends('layouts.admin')

@section('title', 'Gerenciar Condição Especial: '.$condicao_especial->label)

@push('header.javascript')
<script>
    $(function() {
        $("#submit_excluir").click(function() {
            if (confirm('Excluir condição especial?')) {
                $(form).submit();
            }

            return false;
        })
    });
</script>
@endpush

@section('content')

@if ($salvo)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <p><b>Salvo com sucesso</b></p>
        </div>
    </div>
</div>
@endif

<form class="row" method="post" action="{{route('configuracao.editar_condicao_especial', ['id' => $condicao_especial->id])}}">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
            <div class="box-body">
                <div class="form-group">
                    <label for="rotulo" class="col-sm-12 control-label">Rótulo</label>
                    <input type="text" class="form-control" name="rotulo" value="{{$condicao_especial->label}}"/>
                </div>
                <div class="form-group">
                    <label for="valor" class="col-sm-12 control-label">Valor (Método acionado)</label>
                    <input type="text" class="form-control" name="valor" value="{{$condicao_especial->valor}}"/>
                </div>
                <div class="form-group">
                    <label for="operadora" class="col-sm-12 control-label">Operadora</label>
                    <select class="form-control" name="operadora">
                        <option value="" {{('' == $condicao_especial->operadora) ? 'selected' : ''}}>Todas</option>
                        @foreach ($operadoras as $operadora)
                            <option value="{{$operadora->value}}" {{(mb_strtolower($operadora->value) == mb_strtolower($condicao_especial->operadora)) ? 'selected' : ''}}>{{$operadora->text}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success" name='submit' value='salvar'>Salvar</button>    
                        <button type="submit" class="btn btn-danger" name='submit' id='submit_excluir' value='excluir'>Excluir</button>
                    </div>
                </div>
            </div>
		</div>
	</div>
	
</form>

@endsection