@extends('layouts.admin')

@section('title', 'Gerenciar Ação: '.$acao->label)

@push('header.javascript')
<script>
    $(function() {
        $("#submit_excluir").click(function() {
            if (confirm('Excluir ação?')) {
                $(form).submit();
            }

            return false;
        })
    });
</script>
@endpush

@section('content')

@if ($salvo)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <p><b>Salvo com sucesso</b></p>
        </div>
    </div>
</div>
@endif

<form class="row" method="post" action="{{route('configuracao.editar_acao', ['id' => $acao->id])}}">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
            <div class="box-body">
                <div class="form-group">
                    <label for="rotulo" class="col-sm-12 control-label">Rótulo</label>
                    <input type="text" class="form-control" name="rotulo" value="{{$acao->label}}"/>
                </div>
                <div class="form-group">
                    <label for="valor" class="col-sm-12 control-label">Valor (Método acionado)</label>
                    <input type="text" class="form-control" name="valor" value="{{$acao->valor}}"/>
                </div>
                <div class="form-group">
                    <label for="descricao" class="col-sm-12 control-label">Descrição (Separar cada item com vírgula)</label>
                    <input type="text" class="form-control" name="descricao" value="{{$acao->descricao}}"/>
                </div>
                <div class="form-group">
                    <label for="dados_adicionais" class="col-sm-12 control-label">Dados Adicionais (Separar cada item com vírgula)</label>
                    <input type="text" class="form-control" name="dados_adicionais" value="{{$acao->dados_adicionais}}"/>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success" name='submit' value='salvar'>Salvar</button>    
                        <button type="submit" class="btn btn-danger" name='submit' id='submit_excluir' value='excluir'>Excluir</button>
                    </div>
                </div>
            </div>
		</div>
	</div>
	
</form>

@endsection