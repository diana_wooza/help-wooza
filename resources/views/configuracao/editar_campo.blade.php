@extends('layouts.admin')

@section('title', 'Gerenciar Campo: '.$campo->label)

@push('header.javascript')
<script>
    $(function() {
        $("#submit_excluir").click(function() {
            if (confirm('Excluir campo?')) {
                $(form).submit();
            }

            return false;
        })
    });
</script>
@endpush

@section('content')
@push('header.css')
<style type="text/css">
    input[type=checkbox], .iCheck-helper {
        height: 20px !important;
        position: relative !important;
        opacity: 1 !important;
        width: 14px !important;
        left: 1px !important;
        float: left;
    }
    .iCheck-helper {
        left: -20px !important;
        opacity: 0 !important;
    }
</style>
@endpush

@if ($salvo)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <p><b>Salvo com sucesso</b></p>
        </div>
    </div>
</div>
@endif

<form class="row" method="post" action="{{route('configuracao.editar_campo', ['id' => $campo->id])}}">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
            <div class="box-body">
                <div class="form-group">
                    <label for="rotulo" class="col-sm-12 control-label">Rótulo</label>
                    <input type="text" class="form-control" name="rotulo" value="{{$campo->label}}"/>
                </div>
                <div class="form-group">
                    <label for="valor" class="col-sm-12 control-label">Valor (usado na execução da tratativa)</label>
                    <input type="text" class="form-control" name="valor" value="{{$campo->valor}}"/>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="exibir_como_condicao" id="exibir_como_condicao" value="1" {{($campo->exibir_como_condicao) ? 'checked': ''}}/>
                    <span style='line-height:18px;margin-left: -10px;margin-right: 15px;'><label class="control-label">Exibir Como Condição</label></span>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="exibir_como_dado_adicional" id="exibir_como_dado_adicional" value="1" {{($campo->exibir_como_dado_adicional) ? 'checked': ''}}/>
                    <span style='line-height:18px;margin-left: -10px;margin-right: 15px;'><label class="control-label">Exibir Como Dado Adicional</label></span>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="exibir_como_comparativo" id="exibir_como_comparativo" value="1" {{($campo->exibir_como_comparativo) ? 'checked': ''}}/>
                    <span style='line-height:18px;margin-left: -10px;margin-right: 15px;'><label class="control-label">Exibir Como Comparativo</label></span>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success" name='submit' value='salvar'>Salvar</button>    
                        <button type="submit" class="btn btn-danger" name='submit' id='submit_excluir' value='excluir'>Excluir</button>
                    </div>
                </div>
            </div>
		</div>
	</div>
	
</form>

@endsection