<p>Segue relação de pedidos</p>

@if (!$fileDelivered)

	<p style="color:#f00;">Ocorreu um erro ao tentar fazer o upload do relatório no IW. A mensagem de erro foi a seguinte:</p>
	<pre style="background:#f00; color:#fff;">
		{{$errorMessage}}
	</pre>

@endif