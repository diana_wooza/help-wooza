<font family="'Helvetica Neue', Helvetica, Arial, sans-serif">
	<table border="0" width="100%">
		<tr>
			<td bgcolor="#c4d82d">
				<p><h1>{{config('app.name')}} <i>{{config('app.company')}}</i></h1></p>
			</td>
		</tr>
		<tr><td><p>&nbsp;</p></td></tr>
		<tr>
			<td>@yield('content')</td>
		</tr>
		<tr><td><p>&nbsp;</p></td></tr>
		<tr>
			<td>
				<p>
					<hr style="border:0px; border-top:1px solid #666; border-spacing:0px; border-collapse: collapse; outline:none; height:0px;" />
					<strong>Copyright &copy; 2008-{{date('Y')}} <a href="https://www.wooza.com.br">Wooza</a>.</strong> All rights reserved.
				</p>
			</td>
		</tr>
	<table>
</font>