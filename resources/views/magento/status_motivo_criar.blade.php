@extends('layouts.admin')

@section('title', 'Criar Status Motivo')
@section('content')

<form class="box box-info" action="" method="post">
    @csrf
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				<label for="status" class="col-sm-2 control-label">Associar ao Status</label>
				<div class="col-sm-10">
					<select name="status" class="form-control" required>
						@foreach ($dados as $item)
							<option value="{{$item->status}}">{{$item->status}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="motivo" class="col-sm-2 control-label">Motivo</label>
				<div class="col-sm-10">
					<input type="text" name="motivo" class="form-control" placeholder="NOME_MOTIVO" required />
				</div>
			</div>
			<div class="form-group">
				<label for="label" class="col-sm-2 control-label">Label</label>
				<div class="col-sm-10">
                    <input type="text" name="label" class="form-control" placeholder="Nome Motivo" required />
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="row">
				<div class="col-md-10 col-md-offset-2">
					<button type="submit" class="btn btn-info pull-right">Cadastrar Status Motivo</button>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection