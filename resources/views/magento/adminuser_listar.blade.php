@extends('layouts.admin')

@section('title', 'Listar Admin User Base')

@push('header.css')

<style type="text/css">

	.limited-text-content {
		max-width:100%;
		overflow:hidden;
		text-overflow:ellipsis;
		white-space:nowrap;
	}

</style>

@endpush

@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Admin User Base</h3>
                </div>
                <div class="box-body">
                        <div class="col-md-12">
                            <a class="btn btn-success pull-right" type="button" href="{{route('magento.adminuser.criar')}}"><i class="fa fa-cloud-upload"></i> Criar Loja</a>
                        </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Segmento</th>
                                <th>Usuario Loja</th>
                                <th>Senha Loja</th>
                                <th>Código Loja</th>
                            </tr>
                        </thead>
                        @if($dados)
                        <tbody>
                            @foreach($dados as $model)
                                <tr>
                                    <td>{{ $model->id }}</td>
                                    <td>{{ $model->nome }}</td>
                                    <td>{{ $model->segmento }}</td>
                                    <td>{{ $model->usuario_loja }}</td>
                                    <td>{{ $model->senha_loja }}</td>
                                    <td>{{ $model->codigo_loja }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
</div>

@endsection