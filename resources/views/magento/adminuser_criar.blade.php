@extends('layouts.admin')

@section('title', 'Criar Loja Admin User Base')
@section('content')

<form class="box box-info" action="" method="post">
    @csrf
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				<label for="nome" class="col-sm-2 control-label">Nome</label>
				<div class="col-sm-10">
					<input type="text" name="nome" class="form-control" placeholder="Digite o nome da loja" required />
				</div>
			</div>
			<div class="form-group">
				<label for="segmento" class="col-sm-2 control-label">Segmento</label>
				<div class="col-sm-10">
                    <input type="text" name="segmento" class="form-control" placeholder="Digite o segmento" required />
				</div>
			</div>
            <div class="form-group">
				<label for="usuario_loja" class="col-sm-2 control-label">Usuario Loja</label>
				<div class="col-sm-10">
                    <input type="text" name="usuario_loja" class="form-control" placeholder="Digite o usuário da loja" />
				</div>
			</div>
            <div class="form-group">
				<label for="senha_loja" class="col-sm-2 control-label">Senha Loja</label>
				<div class="col-sm-10">
                    <input type="text" name="senha_loja" class="form-control" placeholder="Digite o senha da loja" />
				</div>
			</div>
            <div class="form-group">
				<label for="codigo_loja" class="col-sm-2 control-label">Código Loja</label>
				<div class="col-sm-10">
                    <input type="text" name="codigo_loja" class="form-control" placeholder="Digite o código da loja" />
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="row">
				<div class="col-md-10 col-md-offset-2">
					<button type="submit" class="btn btn-info pull-right">Cadastrar Loja</button>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection