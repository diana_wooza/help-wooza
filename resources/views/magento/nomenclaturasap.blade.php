@extends('layouts.admin')
@section('title', 'Atualizar Nomenclatura SAP')
@section('content')

<form id="pagina-dia-vencimento" class="row" method="post" action="">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Alterar Dia de Vencimento dos pedidos<br /><small>Este recurso utiliza a própria versão mais atualizada da nomenclatura sap no banco de dados para o produto atual</small></h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
					<div class="form-group">
						<label for="status" class="col-sm-12 control-label" style="text-align:left; margin-bottom:15px;">Pedidos a Serem Processados</label>
						<div class="col-sm-12">
							<textarea class="form-control" rows="15" name="pedidos">{{old('pedidos')}}</textarea>
						</div>
					</div>
				
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<button type="submit" class="btn btn-info pull-right">Atualizar Pedidos</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection