@extends('layouts.admin')

@section('title', 'Pedido #' . $pedido->increment_id)
@section('content')

<div class="row">
	<div class="col-sm-12 col-md-6 col-lg-4">

		@include('magento.pedido_ver_block', ['title'=>'Pedido #' . $pedido->increment_id, 'fields'=>[
			'pedido-data'		=> ['label'=>'Data do Pedido', 'value'=>$pedido->created_at_formatado],
		]]);

	</div>

	<div class="col-sm-12 col-md-6 col-lg-4">
		
		<div class="box box-warning">
			<div class="box-header with-border">
				<h3 class="box-title"></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body form-horizontal">
				<div class="form-group">
					<label for="pedido-data" class="col-sm-4 control-label">Data do Pedido</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" id="pedido-data" value="{{$pedido->created_at_formatado}}" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="pedido-data" class="col-sm-4 control-label">Status do Pedido</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" id="pedido-data" value="{{$pedido->status}}" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="pedido-data" class="col-sm-4 control-label">Comprado Em</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" id="pedido-data" value="" readonly>
					</div>
				</div>
				
				<div class="form-group">
					<label for="pedido-data" class="col-sm-4 control-label">IP do Cliente</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" id="pedido-data" value="{{$pedido->remote_ip}}" readonly>
					</div>
				</div>
			</div>
		</div>

	<div class="col-sm-12 col-md-6 col-lg-4">
	</div>

		<div class="box box-warning">
			<div class="box-header with-border">
				<h3 class="box-title">Endereço de Cobrança</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body form-horizontal">
				<div class="form-group">
					<label for="pedido-data" class="col-sm-4 control-label">Data do Pedido</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" id="pedido-data" value="{{$pedido->created_at_formatado}}" readonly>
					</div>
				</div>
				
			</div>
		</div>

	<div class="col-sm-12 col-md-6 col-lg-4">
	</div>

		<div class="box box-warning">
			<div class="box-header with-border">
				<h3 class="box-title">Informações do Cliente</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body form-horizontal">
				<div class="form-group">
					<label for="pedido-data" class="col-sm-4 control-label">Data do Pedido</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" id="pedido-data" value="{{$pedido->created_at_formatado}}" readonly>
					</div>
				</div>
				
			</div>
		</div>

		<div class="box box-warning">
			<div class="box-header with-border">
				<h3 class="box-title">Detalhes do Pedido</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body form-horizontal">
				<div class="form-group">
					<label for="pedido-data" class="col-sm-4 control-label">Data do Pedido</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" id="pedido-data" value="{{$pedido->created_at_formatado}}" readonly>
					</div>
				</div>
				
			</div>
		</div>

	</div>
</div>

<hr />
<pre>@json($pedido)</pre>

@endsection