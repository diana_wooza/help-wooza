@section('content')
	
<div class="box box-warning">
	<div class="box-header with-border">
		<h3 class="box-title">{{$title}}</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="box-body form-horizontal">

		@forelse ($fields as $id => $field)
		
			<div class="form-group">
				<label for="{{$id}}" class="col-sm-4 control-label">{{$field['label']}}</label>
				<div class="col-sm-8">
					<input class="form-control" type="text" id="{{$id}}" value="{{$field['value']}}" readonly>
				</div>
			</div>

		@empty

		@endforelse
		
	</div>
</div>

@endsection