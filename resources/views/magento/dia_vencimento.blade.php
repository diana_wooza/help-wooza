@extends('layouts.admin')
@section('title', 'Alterar Dia Vencimento Pedidos Claro')
@push('header.javascript')
	<script src="{{asset('js/vue.js')}}"></script>
	<script src="{{asset('js/Sortable.min.js')}}"></script>
	<script src="{{asset('js/vuedraggable.umd.min.js')}}"></script>
@endpush
@push('body.javascript')

<script type="text/javascript">

var vencimentoPage = new Vue({
	el: '#pagina-dia-vencimento',
	data: {
		dias: @json($dias),
		escolhido: {{!empty($dia = old('dia', 'null')) ? $dia : 'null'}},
	},
})

</script>

@endpush
@section('content')

<form id="pagina-dia-vencimento" class="row" method="post" action="">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Alterar Dia de Vencimento dos pedidos<br /><small>Exclusivo para pedidos CLARO</small></h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="dia" v-model="escolhido" />
					<div class="form-group">
						<label for="status" class="col-sm-12 control-label" style="text-align:left; margin-bottom:15px;">Dias Disponíveis</label>
						<div class="col-sm-12 form-group">
							<div class="btn-group" style="margin:0px 15px;">
								<label v-for="dia in dias" v-bind:class="'btn btn-' + (dia == escolhido ? 'success' : 'default')" v-on:click="escolhido = dia">
									@{{dia}}
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="status" class="col-sm-12 control-label" style="text-align:left; margin-bottom:15px;">Pedidos a Serem Processados</label>
						<div class="col-sm-12">
							<textarea class="form-control" rows="15" name="pedidos">{{old('pedidos')}}</textarea>
						</div>
					</div>
				
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<button type="submit" class="btn btn-info pull-right">Atualizar Pedidos</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection