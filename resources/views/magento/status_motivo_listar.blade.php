@extends('layouts.admin')

@section('title', 'Listar Status Motivos')

@push('header.css')

<style type="text/css">

	.limited-text-content {
		max-width:100%;
		overflow:hidden;
		text-overflow:ellipsis;
		white-space:nowrap;
	}

</style>

@endpush

@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Status Motivo</h3>
                </div>
                <div class="box-body">
                        <div class="col-md-12">
                            <a class="btn btn-success pull-right" type="button" href="{{route('magento.motivos.criar')}}"><i class="fa fa-cloud-upload"></i> Criar Status Motivo</a>
                        </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Motivo</th>
                                <th>Label</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        @if($dados)
                        <tbody>
                            @foreach($dados as $motivo)
                                <tr>
                                    <td>{{ $motivo->motivo }}</td>
                                    <td>{{ $motivo->label }}</td>
                                    <td>{{ $motivo->status }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
</div>

@endsection