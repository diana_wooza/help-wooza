@extends('contestador.email._base')
@section('title', 'Solicitar Download de Arquivo')
@section('content')

<div class="banner" style="text-align:center;">
	<h1 class="site-title">Download Completo</h1>
	<h2 class="claim">Arquivo: {{@$arquivo}}</h2>
		<svg width="162px" height="190px" title="Download" enable-background="new -0.8 -0.3 23 27" version="1.1" viewBox="-0.8 -0.3 23 27"  xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs/><path clip-rule="evenodd" d="M21.9,15c0,0-8.7,9.9-9.5,11c-0.9,1.1-2.3,0.3-2.3,0.3  s-8.8-9.7-9.8-11.4C-0.7,13.3,1.2,13,1.2,13H6V1c0-0.6,0.4-1,1-1h8c0.6,0,1,0.4,1,1v12h4.7C23.1,13,21.9,15,21.9,15z" fill="#0D0D0D" fill-rule="evenodd"/></svg>
	</div>

	<div class="cta" style="text-align:center;">
		<a class="btn btn-primary" href="{{route('contestador.processo.listar', ['tipo' => \App\Models\Contestador\Processo::TIPO_DOWNLOAD])}}">Arquivos Prontos</a>
	</div>

	<table border="0" cellpadding="0" cellspacing="0" align="center" class="about">
		<tr>
			<td>Solicitante &nbsp; &nbsp; </td>
			<td>{{@$solicitante}}</td>
		</tr>
		<tr>
			<td>Disponível em &nbsp; &nbsp; </td>
			<td>{{@$data}}</td>
		</tr>
		<tr>
			<td>Arquivo &nbsp; &nbsp; </td>
			<td>{{@$arquivo}}</td>
		</tr>
		<tr>
			<td>Competência &nbsp; &nbsp; </td>
			<td>{{@$competencia}}</td>
		</tr>
	</table>

	<p>Este email e todo o conteúdo dele é privado. Não envie ou encaminhe para ninguém!<br />Sistema de Contestação Wooza!</p>

@endsection