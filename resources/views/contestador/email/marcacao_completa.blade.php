@extends('email.templates.hub_basic')

@section('content')

	@if ($success)

		<p>Seu arquivo foi marcado com sucesso!</p>

	@else

		<p>Ocorreu um erro na marcação do arquivo</p>

	@endif

	<p>Verifique o <a href="{{url('/')}}" target="_blank">Hub</a> para mais informações
	<p><strong>Marcador:</strong> {{$marcador}}</p>

@endsection