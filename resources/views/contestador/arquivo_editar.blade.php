@extends('layouts.admin')
@section('title', $arquivo->exists ? 'Editando Arquivo ' . $arquivo->nome : 'Novo Arquivo')
@push('header.javascript')
	<script src="{{asset('js/vue.js')}}"></script>
	<script src="{{asset('js/Sortable.min.js')}}"></script>
	<script src="{{asset('js/vuedraggable.umd.min.js')}}"></script>
@endpush
@push('header.css')

<style type="text/css">

	.highlighted {
		margin-left: -10px;
		margin-right: -10px;
	}
	.highlighted:hover {
		background-color:rgba(0,0,0,0.1);
	}

</style>
@endpush
@push('body.javascript')

<script type="text/javascript">

var arquivoPage = new Vue({
	el: '#pagina-arquivo',
	data: {
		arquivo: {!! old('arquivo', json_encode($arquivo)) !!},
		tratamentos: @json($tratamentos),
		regras: @json($regras),
		operadoras: @json($operadoras),
		armazenamentos: @json($armazenamentos),
		help: {
			tratamento: null,
		}
	},
	methods: {
		addTratamento: function (tratamento)
		{
			let tratamentoConfig = {
				class: tratamento.tipo,
				configuracao: {},
			};

			for (var config in tratamento.configuracoes)
			tratamentoConfig.configuracao[config] = tratamento.configuracoes[config].default;
			
			this.arquivo.tratamentos.push(tratamentoConfig);
		},
		removeTratamento: function (tratamento)
		{
			let index = this.arquivo.tratamentos.indexOf(tratamento);

			if (index < 0)
				return;

			this.arquivo.tratamentos.splice(index, 1);
		},
		addRegra: function (regra)
		{
			if (this.arquivo.regras.indexOf(regra.id) >= 0)
				return;

			this.arquivo.regras.push(regra.id);
		},
		removeRegra: function (regra)
		{
			let index = this.arquivo.regras.indexOf(regra.id);

			if (index < 0)
				return;

			this.arquivo.regras.splice(index, 1);
		},
		clearRegras: function ()
		{
			this.arquivo.regras.splice(0);
		},
		showHelp: function (tratamento)
		{
			this.help.tratamento = tratamento;

			$('#tratamentos-help-modal').modal();
			// alert('O tratamento ' + tratamento.rotulo + ' ainda não possui nenhuma documentação de ajuda atrelada a ele');
		},
		clearTratamentos: function ()
		{
			this.arquivo.tratamentos.splice(0);
		}
	},
})

</script>

@endpush
@section('content')

<div id="pagina-arquivo" class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Dados Básicos</h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Status</label>
						<div class="col-sm-10 form-group">
							<select class="form-control" v-model="arquivo.status" required>
								<option v-bind:value="0">Desabilitado</option>
								<option v-bind:value="1">Habilitado</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Operadora</label>
						<div class="col-sm-10 form-group">
							<select class="form-control" v-model="arquivo.operadora_id" required>
								<option v-bind:value="null" disabled>Escolha um</option>
								<option v-for="operadora in operadoras" v-bind:value="operadora.id">@{{operadora.nome}}</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Nome</label>
						<div class="col-sm-10 form-group">
							<input type="text" class="form-control" v-model="arquivo.nome" placeholder="Nome do arquivo" />
						</div>
					</div>

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Armazenamento Padrão</label>
						<div class="col-sm-10 form-group">
							<select class="form-control" v-model="arquivo.configuracao.armazenamento" required>
								<option v-bind:value="null" disabled>Escolha um</option>
								<option v-for="(nome, index) in armazenamentos" v-bind:value="index">@{{nome}}</option>
							</select>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="box box-warning">
			<div class="box-header with-border">
				<h3 class="box-title">Tratamentos Executados</h3>
				<div class="box-tools pull-right">
					<div class="input-group-btn pull-right">
						<button type="button" class="btn btn-warning dropdown-toggle pull-right" data-toggle="dropdown">
							Adicionar Tratamento
							&nbsp;
							<span class="fa fa-caret-down"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li v-for="tratamento in tratamentos" v-bind:title="tratamento.descricao">
								<a style="cursor:pointer;" v-on:click="addTratamento(tratamento)">@{{tratamento.rotulo}}</a>
							</li>
							<li class="divider"></li>
							<li><a style="cursor:pointer;" v-on:click="clearTratamentos()">Limpar Tudo</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div>
				<div class="box-body">
					<p>As colunas do arquivo original precisam ser convertidas nos nomes específicos das colunas usados no tratamento geral dos arquivos recebidos:</p>
					<pre class="text-left" style="white-space:pre-line;"><strong>_pedido</strong>: Essa coluna representa uma linha já marcada. Ao definir um código de pedido nessa coluna o arquivo já vai ser importado como arquivo marcado e será vinculado ao pedido especificado (desde que o mesmo exista).
<strong>_data</strong>: Data que consta a ativação no arquivo da operadora.
<strong>_terminal</strong>: Número da linha do cliente.
<strong>_documento</strong>: Número do CPF/CNPJ do cliente.
<strong>_protocolo</strong>: Número do protocolo do serviço.
<strong>_serviço</strong>: Coluna com o tipo de serviço efetuado (nome do plano adquirido, etc)
<strong>_motivo</strong>: Coluna com a explicação principal sobre o motivo do recebimento/churn
<strong>_detalhe</strong>: Coluna com explicações adicionais sobre o motivo do recebimento/churn
<strong>_valor</strong>: Coluna com o valor que de fato será pago ou descontado</pre>
					<div class="modal fade" id="tratamentos-help-modal">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Ajuda para o tratamento <strong>@{{help && help.tratamento ? help.tratamento.rotulo : ''}}</strong></h4>
								</div>
								<div class="modal-body" v-html="help && help.tratamento && help.tratamento.help ? help.tratamento.help : '<p class=\'text-gray text-center\'>Nenhuma configuração de ajuda definida</p>'">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<div v-if="arquivo.tratamentos.length <= 0" class="text-gray text-center">Nenhum Tratamento configurado para este arquivo</div>

					<draggable v-model="arquivo.tratamentos" draggable=".tratamento-item" handle=".handle">
					
						<div class="row highlighted tratamento-item" v-for="tratamento in arquivo.tratamentos">

							<div class="box-tools pull-right" style="margin-bottom: -35px; margin-top: 5px; margin-right: 10px; cursor: pointer; z-index: 99; position: relative;">
								<button type="button" class="btn btn-box-tool" v-on:click="showHelp(tratamentos[tratamento.class])"><i class="fa fa-commenting"></i></button>
								<button type="button" class="btn btn-box-tool text-red" v-bind:title="'Remove a regra de tratamento ' + tratamentos[tratamento.class].rotulo + ' da lista de regras a serem executadas'" v-on:click="removeTratamento(tratamento)"><i class="fa fa-times"></i></button>
								<button type="button" class="btn btn-box-tool handle text-green" title="Arraste esse tratamento para cima ou para baixo para modificar a ordem de execução" style="cursor:n-resize;"><i class="fa fa-arrows-v"></i></button>
							</div>
							<h4 class="col-sm-11" style="text-align:left;">@{{tratamentos[tratamento.class].rotulo}}</h4>
							<div v-for="(configuracao, index) in tratamentos[tratamento.class].configuracoes" v-bind:class="'col-sm-' + configuracao.layout.colunas">
								<div class="form-group">
									<label>@{{configuracao.rotulo}}</label>
									<input v-if="configuracao.tipo == 'texto'" type="text" class="form-control" v-model="tratamento.configuracao[index]">
									<input v-if="configuracao.tipo == 'numero'" type="number" class="form-control" v-model="tratamento.configuracao[index]">
									<select v-if="configuracao.tipo == 'select'" class="form-control" v-model="tratamento.configuracao[index]">
										<option v-for="(value, key) in configuracao.opcoes" v-bind:value="key">@{{value}}</option>
									</select>
								</div>
							</div>
						</div>
					
					</draggable>

				</div>
			</div>
		</div>

		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">Regras de Marcação Executadas Automaticamente</h3>
				<div class="box-tools pull-right">
					<div class="input-group-btn pull-right">
						<button type="button" class="btn btn-warning dropdown-toggle pull-right" data-toggle="dropdown">
							Adicionar Regra de Marcação
							&nbsp;
							<span class="fa fa-caret-down"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li v-for="regra in regras">
								<a style="cursor:pointer;" v-on:click="addRegra(regra)">@{{regra.nome}}</a>
							</li>
							<li class="divider"></li>
							<li><a style="cursor:pointer;" v-on:click="clearRegras()">Limpar Tudo</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="">
				<div class="box-body">

					<div v-if="arquivo.regras.length <= 0" class="text-gray text-center">Nenhuma Regra de Marcação foi configurado para este arquivo</div>

					<draggable v-model="arquivo.regras" draggable=".regra-item" handle=".handle">
					
						<div v-for="regra in arquivo.regras" class="row highlighted regra-item" style="padding-bottom:15px;">
							<div class="box-tools pull-right" style="margin-bottom: -65px; margin-top: 35px; margin-right: 15px; cursor: pointer; z-index: 99; position: relative;">
								<button type="button" class="btn btn-box-tool" v-on:click="removeRegra(regra)"><i class="fa fa-times"></i></button>
								<button v-if="arquivo.regras.length > 1" type="button" class="btn btn-box-tool handle text-green" title="Arraste esse tratamento para cima ou para baixo para modificar a ordem de execução" style="cursor:n-resize;"><i class="fa fa-arrows-v"></i></button>
							</div>
							<h3 class="col-sm-12" style="text-align:left;">@{{regras[regra].nome}}</h3>
							<label class="col-sm-12" style="text-align:left;">@{{regras[regra].base_string}}</label>
						</div>

					</draggable>

				</div>
			</div>
		</div>

		<div class="box box-info">
			<div class="box-footer">
				<div class="row">
					<form action="" method="post">
						@csrf
						<input type="hidden" name="arquivo" v-bind:value="JSON.stringify(arquivo)" />
						<div class="col-md-12">
							<a class="btn btn-default" href="{{route('contestador.arquivo.listar')}}">Cancelar {{$arquivo->exists ? 'Edição' : 'Inclusão'}}</a>
							<button type="submit" class="btn btn-info pull-right">Salvar Alterações</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>

</div>

@endSection