<div class="col-sm-12 col-md-4 col-lg-4" style="margin-bottom:15px;">

	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Upload de Arquivo</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table class="table no-margin table-hover">
					<thead>
						<tr>
							<th style="width:auto; text-align:left; vertical-align:middle;">Arquivo</th>
							<th style="width:50px; text-align:left; vertical-align:middle;">Operadora</th>
							<th style="width:1px; text-align:center; vertical-align:middle;">Ação</th>
						</tr>
					</thead>
					<tbody>

						@forelse (App\Models\Contestador\Arquivo::with(['operadora'])->ativos()->get() as $arquivo)

							<tr>

								<td style="text-align:left; vertical-align:middle;">{{ $arquivo->nome }}</td>
								<td style="text-align:center; vertical-align:middle;">{!! $arquivo->operadora->bullet !!}</td>
								<td style="text-align:center; vertical-align:middle;">
									<a class="btn btn-default" href="{{route('contestador.arquivo.upload', [$arquivo->id])}}">Enviar Arquivo</a>
								</td>

							</tr>

						@empty

							<tr><td colspan="2" class="light text-center">Você ainda não tem acesso a nenhum arquivo. <i class="fa fa-frown-o"></td></tr>

						@endforelse
						
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

<div class="col-sm-12 col-md-5 col-lg-5" style="margin-bottom:15px;">

	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Processos Pendentes</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table class="table no-margin table-hover">
					<thead>
						<tr>
							<th style="width:50px; text-align:left; vertical-align:middle;">Tipo</th>
							<th style="width:auto; text-align:left; vertical-align:middle;">Arquivo</th>
							<th style="width:50px; text-align:left; vertical-align:middle;">Solicitante</th>
							<th style="width:1px; text-align:center; vertical-align:middle;">Inclusão</th>
						</tr>
					</thead>
					<tbody>

						@forelse (App\Models\Contestador\Processo::with(['usuario'])->ApenasPendente()->get() as $processo)

							<tr>

								<td style="text-align:left; vertical-align:middle;">{!! $processo->tipo_bullet !!}</td>
								<td style="text-align:left; vertical-align:middle;">{!! $processo->full_name !!}</td>
								<td style="text-align:center; vertical-align:middle;">{!! @$processo->usuario->nome !!}</td>
								<td style="text-align:center; vertical-align:middle;">{!! $processo->inclusao_string !!}</td>

							</tr>

						@empty

							<tr><td colspan="2" class="light text-center">Nenhum processo em andamento no momento. <i class="fa fa-frown-o"></td></tr>

						@endforelse
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>