@extends('layouts.admin')
@section('title', $regra->exists ? 'Editando Regra ' . $regra->nome : 'Nova Regra')
@push('header.javascript')
	<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
	<script src="{{asset('js/Sortable.min.js')}}"></script>
	<script src="{{asset('js/vuedraggable.umd.min.js')}}"></script>
@endpush
@push('header.css')

<style type="text/css">

	.highlighted {
		margin-left: -10px;
		margin-right: -10px;
	}
	.highlighted:hover {
		background-color:rgba(0,0,0,0.1);
	}

</style>
@endpush
@push('body.javascript')

<script type="text/javascript">

var regraMarcacaoPage = new Vue({
	el: '#pagina-regra-marcacao',
	data: {
		regra: {!! old('regra', json_encode($regra)) !!},
		queries: @json($queries),
	},
	methods: {
		addComparacao: function (tipo, comparacao)
		{
			let configuracao = {
				tipo			: tipo,
				configuracoes	: {},
			};

			for (var config in comparacao.configuracoes)
				configuracao.configuracoes[config] = comparacao.configuracoes[config].default;

			this.regra.configuracoes.comparacoes.push(configuracao);
		},
		removeComparacao: function (comparacao)
		{
			let index = this.regra.configuracoes.comparacoes.indexOf(comparacao);
			
			if (index < 0)
				return;
			
			this.regra.configuracoes.comparacoes.splice(index, 1);
		},
		clearComparacoes: function ()
		{
			this.regra.configuracoes.comparacoes.splice(0);
		}
	},
})

</script>

@endpush
@section('content')

<div id="pagina-regra-marcacao" class="row">

	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Dados Básicos</h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Status</label>
						<div class="col-sm-10 form-group">
							<select class="form-control" v-model="regra.status" required>
								<option v-bind:value="0">Desabilitado</option>
								<option v-bind:value="1">Habilitado</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Nome</label>
						<div class="col-sm-10 form-group">
							<input type="text" class="form-control" v-model="regra.nome" placeholder="Nome da regra" />
						</div>
					</div>

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Query Base</label>
						<div class="col-sm-10 form-group">
							<select class="form-control" v-model="regra.configuracoes.query" required>
								<option v-bind:value="null" disabled>Escolha uma opção</option>
								<option v-for="query in queries" v-bind:value="query.tipo">@{{query.rotulo}}</option>
							</select>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="box box-warning">
			<div class="box-header with-border">
				<h3 class="box-title">Regras de Marcação</h3>
				<div class="box-tools pull-right">
					<div v-if="regra.configuracoes.query != null" class="input-group-btn pull-right">
						<button type="button" class="btn btn-warning dropdown-toggle pull-right" data-toggle="dropdown">
							Adicionar Comparação
							&nbsp;
							<span class="fa fa-caret-down"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li v-for="(comparacao, tipo) in queries[regra.configuracoes.query].comparacoes" v-bind:title="comparacao.descricao">
								<a v-on:click="addComparacao(tipo, comparacao)">@{{comparacao.rotulo}}</a>
							</li>
							<li class="divider"></li>
							<li><a v-on:click="clearComparacoes()">Limpar Tudo</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div>
				<div class="box-body">
					<div v-if="regra.configuracoes.query == null" class="text-red text-center"><i>Escolha uma query base para habilitar esta opção</i></div>
					<div v-if="regra.configuracoes.query != null && regra.configuracoes.comparacoes.length <= 0" class="text-gray text-center">Nenhuma Comparação configurada para esta regra</div>

					<draggable v-model="regra.configuracoes.comparacoes" draggable=".comparacao-item" handle=".handle">
					
						<div class="row highlighted comparacao-item" v-for="comparacao in regra.configuracoes.comparacoes">
							<div class="box-tools pull-right" style="margin-bottom: -35px; margin-top: 5px; margin-right: 10px; cursor: pointer; z-index: 99; position: relative;">
								<button type="button" class="btn btn-box-tool" v-on:click="removeComparacao(comparacao)"><i class="fa fa-times"></i></button>
								<button v-if="regra.configuracoes.comparacoes.length > 1" type="button" class="btn btn-box-tool handle text-green" title="Arraste esse tratamento para cima ou para baixo para modificar a ordem de execução" style="cursor:n-resize;"><i class="fa fa-arrows-v"></i></button>
							</div>
							<h4 class="col-sm-12" style="text-align:left;">@{{queries[regra.configuracoes.query].comparacoes[comparacao.tipo].rotulo}}</h4>
							<div v-for="(configuracao, index) in queries[regra.configuracoes.query].comparacoes[comparacao.tipo].configuracoes" v-bind:class="'col-sm-' + configuracao.colunas">
								<div class="form-group">
								<label><small>@{{configuracao.rotulo}}</small></label>
									<input v-if="configuracao.tipo == 'text'" type="text" class="form-control" v-model="comparacao.configuracoes[index]">
									<input v-if="configuracao.tipo == 'score'" type="number" class="form-control" v-model="comparacao.configuracoes[index]" step="0.1">
									<input v-if="configuracao.tipo == 'number'" type="number" class="form-control" v-model="comparacao.configuracoes[index]" step="1">
									<select v-if="configuracao.tipo == 'columns'" class="form-control" v-model="comparacao.configuracoes[index]">
										<option v-bind:value="null" disabled>Selecione uma</option>
										<option v-for="(coluna, indexColuna) in queries[regra.configuracoes.query].colunas" v-if="queries[regra.configuracoes.query].comparacoes[comparacao.tipo].colunas.indexOf(indexColuna) >= 0" v-bind:value="indexColuna">@{{coluna.rotulo}}</option>
									</select>
									<select v-if="configuracao.tipo == 'bool'" class="form-control" v-model="comparacao.configuracoes[index]">
										<option v-bind:value="false">Não</option>
										<option v-bind:value="true">Sim</option>
									</select>
								</div>
							</div>
						</div>

					</draggable>
				</div>
			</div>
		</div>

		<div class="box box-info">
			<div class="box-footer">
				<div class="row">
					<form action="" method="post">
						@csrf
						<input type="hidden" name="regra" v-bind:value="JSON.stringify(regra)" />
						<div class="col-md-12">
							<a class="btn btn-default" href="{{route('contestador.regra.marcacao.listar')}}">Cancelar {{$regra->exists ? 'Edição' : 'Inclusão'}}</a>
							<button type="submit" class="btn btn-info pull-right">Salvar Alterações</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>

@endSection