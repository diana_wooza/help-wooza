@extends('layouts.admin')

@section('title', 'Upload de Arquivo')
@section('subtitle', 'Importar Arquivo')
@section('content')

<style type="text/css">

.page-importar-planilha .form-group .btn-app.active {
	background-color: #00a65a !important;
	border-color: #008d4c !important;
	color: #fff !important;
}

</style>

<div class="row page-importar-planilha">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Importe a planilha que será marcada</h3>
			</div>
			<form class="form-horizontal" method="post" action="" enctype="multipart/form-data" onSubmit="document.getElementById('contestador-arquivo-upload-button').disabled=true;">
				@csrf
				<div class="box-body">
					<div class="form-group">
						<label for="csv" class="col-sm-2 control-label">Arquivo CSV</label>
						<div class="col-sm-10">
							<input type="file" name="csv" id="csv" accept="{{ $formats }}" required />
							<p class="help-block">O arquivo da planilha precisa ser um arquivo CSV</p>
							<p class="help-block">Arquivos maiores que {{ini_get('upload_max_filesize')}}B serão ignorados</p>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="arquivo" class="col-sm-2 control-label">Tipo de Arquivo</label>
					<div class="col-sm-10 form-group">
						<select name="arquivo" id="arquivo" class="form-control" required>
							@foreach ($arquivos as $arquivo)
								<option value="{{$arquivo->id}}" {{$arquivo->id == old('arquivo', $tipoArquivo) ? 'selected' : ''}}>{{$arquivo->nome}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="competencia" class="col-sm-2 control-label">Competência</label>
					<div class="col-sm-10 form-group">
						<select name="competencia" id="competencia" class="form-control" required>
							@foreach ($competencias as $competencia)
								<option value="{{$competencia->id}}" {{$competencia->id == old('competencia') ? 'selected' : ''}}>{{$competencia->data_string}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="estrategia" class="col-sm-2 control-label">Estratégia</label>
					<div class="col-sm-10 form-group">
						<select name="estrategia" id="estrategia" class="form-control" required>
							@foreach ($estrategias as $estrategia)
								<option value="{{ $estrategia->key }}" {{old('estrategia') == $estrategia->key ? 'selected' : ''}}>{{ $estrategia->value }}</option>
							@endforeach
						</select>
						<p class="help-block">A estratégia adotada é aplicada àpenas a receitas que sejam do mesmo arquivo <strong>E</strong> que também sejam da mesma competência</p>
					</div>
				</div>
				<div class="form-group">
					<label for="marcar" class="col-sm-2 control-label">Marcar Após Inclusão</label>
					<div class="col-sm-10 form-group">
						<select name="marcar" id="marcar" class="form-control" required>
							<option value="sim" {{old('marcar') == 'sim' ? 'selected' : ''}}>Sim</option>
							<option value="nao" {{old('marcar') == 'nao' ? 'selected' : ''}}>Não</option>
						</select>
						<p class="help-block">Após a inclusão dos dados na base. Um processo de Marcação (que é configurado direto no arquivo) é disparado automaticamente caso esta opção esteja habilitada.</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Download Após Marcação</label>
					<div class="col-sm-10 form-group">
						<select class="form-control" name="exportar" required>
							<option value="sim" {{old('exportar') == 'sim' ? 'selected' : ''}}>Sim</option>
							<option value="nao" {{old('exportar') == 'nao' ? 'selected' : ''}}>Não</option>
						</select>
						<p class="help-block">Após a marcação desse item, um download será gerado. Você receberá um email avisando que o download está pronto. Escolha esta opção apenas se a opção acima estiver ativada.</p>
					</div>
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<button id="contestador-arquivo-upload-button" type="submit" class="btn btn-info">Importar Arquivo</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection