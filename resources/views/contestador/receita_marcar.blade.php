@extends('layouts.admin')
@section('title', 'Solicitar Marcação')
@section('content')

	<form class="box box-info form-horizontal" action="" method="post">
		@csrf
		<div class="box-header with-border">
			<h3 class="box-title">Formulário para Solicitação de Marcação</h3>
		</div>
		<div class="box-body">

			<div class="form-group">
				<label class="col-sm-2 control-label">Arquivo</label>
				<div class="col-sm-10 form-group">
					<select class="form-control" name="arquivo" required>
						<option value="" selected disabled>Selecione uma opção</option>
						@foreach ($arquivos as $arquivo)
							<option value="{{ $arquivo->id }}" {{ old('arquivo', @$defaults['arquivo']) == $arquivo->id ? 'selected' : '' }}>{{ $arquivo->nome }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Competência</label>
				<div class="col-sm-10 form-group">
					<select class="form-control" name="competencia" required>
						<option value="" selected disabled>Selecione uma opção</option>
						@foreach ($competencias as $competencia)
							<option value="{{ $competencia->id }}" {{ old('arquivo', @$defaults['competencia']) == $competencia->id ? 'selected' : '' }}>{{ $competencia->data_string }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Regra</label>
				<div class="col-sm-10 form-group">
					<select class="form-control" name="regra" required>
						<option value="" selected disabled>Selecione uma opção</option>
						@foreach ($regras as $regra)
							<option value="{{ $regra->id }}" {{ old('arquivo', @$defaults['regra']) == $regra->id ? 'selected' : '' }}>{{ $regra->nome }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Gerar Arquivo para Download</label>
				<div class="col-sm-10 form-group">
					<select class="form-control" name="exportar" required>
						<option value="sim" {{old('exportar') == 'sim' ? 'selected' : ''}}>Sim</option>
						<option value="nao" {{old('exportar') == 'nao' ? 'selected' : ''}}>Não</option>
					</select>
				</div>
			</div>

		</div>
		<div class="box-footer">
			<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-info pull-right">Solicitar Marcação</button>
					</div>
				</form>
			</div>
		</div>
	</form>

@endsection