<h1>Olá {{$user->nome}}</h1>

<p>Foi feita uma requisição de definição/redefinição de senha para seu usuário. Use o botão abaixo para isso.</p>
<p><strong>Este link possui validade de 24 horas. Após esse tempo uma nova solicitação de senha precisará ser feita</strong></p>
<br />
<p align="center"><a href="{{$url}}" target="_blank" style="background:#c4d82d; padding:10px 25px; color:#000; text-decoration: none; font-family:'arial'; border-radius:4px;">Defina sua Senha</a></p>
<br />
<p>Se você não tem ciência da requisição de troca de senha ou não deseja redefinir sua senha basta ignorar este email. Caso você continue recebendo este email entre em contato com a Administração.</p>
<br /><br />
<p>Obrigado,<br />Equipe Magento</p>
<hr />
<p size="1"><i>Caso esteja tendo algum problema com o botão acima copie e cole a URL abaixo no seu navegador</i>.</p>
<p>{{$url}}</p>