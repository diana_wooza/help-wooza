@extends('layouts.base')

@section('title', __('login.title'))
@section('content')

	@if(!empty($errors->first()))
		@alerta()
			{{ $errors->first() }}
		@endalerta
	@endif

	<div class="login-box">
		<div class="login-logo">
			<a href="{{url('/')}}">{{config('app.name')}}<b> {{config('app.company')}}</b></a>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">Definindo nova senha</p>
			<form action="" method="post">
				@csrf

				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="{{__('login.placeholder.password')}}" required />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password_confirmation" class="form-control" placeholder="Repita sua senha" required />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Alterar Senha</button>
					</div>
				</div>
			</form>
		</div>
	</div>

@endsection