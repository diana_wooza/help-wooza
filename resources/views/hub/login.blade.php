@extends('layouts.base')

@section('title', __('login.title'))
@section('content')

	@if(!empty($errors->first()))
		@alerta()
			{{ $errors->first() }}
		@endalerta
	@endif

	@if(Session::has('message'))
		<div class="callout callout-success">
			<p>{!! Session::get('message') !!}</p>
		</div>
	@endif

	<div class="login-box">
		<div class="login-logo">
			<a href="{{url('/')}}">{{config('app.name')}}<b> {{config('app.company')}}</b></a>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">{{__('login.message')}}</p>
			<form action="{{url('login')}}" method="post">
				@csrf

				<div class="form-group has-feedback">
					<input type="username" name="username" class="form-control" placeholder="{{__('login.placeholder.email')}}" required autofocus />
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="{{__('login.placeholder.password')}}" required />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8"></div>
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">{{__('login.login')}}</button>
					</div>
				</div>
			</form>
		</div>
	</div>

@endsection