@extends('layouts.base')

@section('title', __('login.register.title'))
@section('content')

	@if(!empty($errors->first()))
		@alerta()
			{{ $errors->first() }}
		@endalerta
	@endif

	<section class="content">
		<div class="login-logo">
			<a href="{{url('/')}}">{{config('app.name')}} <b>{{config('app.company')}}</b></a>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">@yield('title')</h3>
					</div>
					<form role="form" action="{{url('cadastro')}}" method="post">
						@csrf
						<div class="box-body">
							<div class="form-group">
								<label for="input-nome">{{__('login.register.label_name')}}:</label>
								<input type="text" name="nome" class="form-control" id="input-nome" placeholder="{{__('login.register.fill_name')}}" value="{{$user['nome']}}" />
							</div>
							<div class="form-group">
								<label for="input-telefone">{{__('login.register.label_phone')}}</label>
								<input type="text" name="telefone" class="form-control" id="input-telefone" placeholder="{{__('login.register.fill_phone')}}" value="{{$user['telefone']}}" />
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary">{{__('login.register.save')}}</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

@endsection