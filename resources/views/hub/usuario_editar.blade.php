@extends('layouts.admin')

@section('title', 'Gerenciar Usuário do HUB: ' . $usuario->nome)
@section('content')

<form class="row" method="post" action="{{route('hub.usuario.editar', ['id' => $usuario->id])}}">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">{!! $usuario->exists ? 'Dados dos Usuário: <strong>' . $usuario->nome . '</strong> [ID ' . $usuario->id . ']': 'Novo Usuário'!!}</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<div class="form-horizontal">
				<div class="box-body">
					
					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">Nome</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="nome" value="{{old('nome', $usuario->nome)}}"/>
						</div>
					</div>

					@if (!$usuario->exists)

						<div class="form-group">
							<label for="nome" class="col-sm-2 control-label">Login</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="login" value="{{old('login')}}"/>
							</div>
						</div>
					
					@endif

					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">E-Mail</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="email" value="{{old('email', $usuario->originalEmail)}}"/>
							<p class="help-block">Deixe este campo em branco para utilizar o email padrão da @wooza.com.br. Use apenas caso queira personalizar o email que um usuário irá receber</p>
						</div>
					</div>

					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">Telefone</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="telefone" id="input-telefone"  value="{{old('telefone', $usuario->telefone)}}"/>
						</div>
					</div>

					@if (Auth::user()->checkPermissao('hub.admin'))

						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">Perfís</label>
							<div class="col-sm-10">
							<select class="form-control selectpicker" name="perfis[]" data-action-box="1" data-deselect-all-text="Remover seleção" data-select-all-text="Selecionar todos" data-none-selected-text="Nenhum perfil associado no momento" multiple>
									@foreach ($perfis as $id => $perfil)
										<option value="{{$id}}" {{is_array(old('perfis', $usuario->getPerfisIds())) && in_array($id, old('perfis', $usuario->getPerfisIds())) ? 'selected' : ''}}>{{$perfil}}</option>
									@endforeach
								</select>
								<p class="help-block">Associe todos os perfís que este usuário poderá ter acesso. Nenhum perfil é uma opção válida!</p>
							</div>
						</div>

					@endif
				
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<a class="btn btn-default" href="{{route('colmeia.armario.lista')}}">Cancelar {{$usuario->exists ? 'Edição' : 'Adição'}}</a>
							<button type="submit" class="btn btn-info pull-right">Salvar Alterações</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</form>

@endsection