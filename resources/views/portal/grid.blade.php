@extends('layouts.portal')

@section('title', 'Busca de pedidos')

@push('header.javascript')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>
	<script>
		$(function() {
			$('#data_pedido').daterangepicker({ 
				autoUpdateInput: false,
				opens: "right",
				locale: { 
					format: 'DD/MM/YYYY',
					cancelLabel: 'Limpar',
					applyLabel: 'Aplicar',
					daysOfWeek: [
						"Dom",
						"Seg",
						"Ter",
						"Qua",
						"Qui",
						"Sex",
						"Sáb"
					],
					monthNames: [
						"Janeiro",
						"Fevereiro",
						"Março",
						"Abril",
						"Maio",
						"Junho",
						"Julho",
						"Agosto",
						"Setembro",
						"Outubro",
						"Novembro",
						"Dezembro"
					]
				}
			});

			$('#data_pedido').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});

			$('#data_pedido').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
			});

			$('.btn_modal').click(function() {
				$.ajax({
					type: 'GET',
					url: "{{route('portal.dados_pedido_portal')}}/"+$(this).val(),
					dataType: 'json',
					complete: function (data) {
						$(".modal-body table tbody").html(data.responseText);
						$('#myModal').modal('show');
					}
				});
			});

			$(".btn_limpar").click(function() {
				$("#form input[type=text]").each(function() {
					$(this).val('');
				});
			});
		});
	</script>
@endpush
@push('header.css')
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css' />
	<style type='text/css'>
	.box-primary {
		min-height:45px;
	}
	.btn_submit {
		margin-top: 3px;
		margin-right: 5px;
	}
	.pagination {
		margin: 0;
	}
	</style>
@endpush

@section('content')
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Dados da Compra</h4>
			</div>
			<div class="modal-body">
				<table class='table table-striped table-bordered table-hover table-sm'>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<form action="" enctype="multipart/form-data" method="get" id='form'>
			@csrf
			<div class="box box-primary">
				<label>Total de pedidos: {{(!empty($pedidos)) ? $pedidos->total() : 0 }}</label>
				<button type="button" name='button' value='Limpar filtros' class="btn btn-primary pull-right btn_submit btn_limpar" ><i class="fa fa-eraser"></i>&nbsp;Limpar filtros</button>
				<button type="submit" name='submit' value='Filtrar' class="btn btn-primary pull-right btn_submit"><i class="fa fa-filter"></i>&nbsp;Filtrar</button>
				<nav class='paginacao'>
				@if (!empty($pedidos))
					{{$pedidos->appends($_GET)->links()}}
				@endif
				</nav>
			</div>
			<div class="box box-success">
				<table class='table table-striped table-bordered table-hover table-sm'>
					<thead>
						<tr>
							<th width="6%">
								<label class="control-label">Código do pedido</label>
								<input type="text" class="form-control pull-right" id="codigo_pedido" name="codigo_pedido" value="{{\Request::get('codigo_pedido')}}" placeholder="XX,XX,...">
							</th>
							<th>
								<label class="control-label">Data do pedido</label>
								<!-- <input type="text" class="form-control pull-right" id="data_pedido" name="data_pedido" value="{{\Request::get('data_pedido')}}"> -->
							</th>
							<th>
								<label class="control-label">CPF</label>
								<input type='text' class="form-control" name='cpf' value="{{\Request::get('cpf')}}" placeholder="XX,XX,..."/>
							</th>
							<th>
								<label class="control-label">Nome do cliente</label>
								<!-- <input type='text' class="form-control" name='nome_cliente' value="{{\Request::get('nome_cliente')}}" placeholder="XX,XX,..."/> -->
							</th>
							<th>
								<label class="control-label">Linha</label>
								<input type='text' class="form-control" name='linha' value="{{\Request::get('linha')}}" placeholder="XX,XX,..."/>
							</th>
							<th>
								<label class="control-label">Plano</label>
								<!-- <input type='text' class="form-control" name='plano' value="{{\Request::get('plano')}}" placeholder="XX,XX,..."/> -->
							</th>
							<th>
								<label class="control-label">Cidade</label>
								<!-- <input type='text' class="form-control" name='cidade' value="{{\Request::get('cidade')}}" placeholder="XX,XX,..."/> -->
							</th>
							<th>
								<label class="control-label">Estado</label>
								<!-- <input type='text' class="form-control" name='Estado' value="{{\Request::get('Estado')}}" placeholder="XX,XX,..."/> -->
							</th>
							<th>
								<label class="control-label">CEP</label>
								<!-- <input type='text' class="form-control" name='cep' value="{{\Request::get('cep')}}" placeholder="XX,XX,..."/> -->
							</th>
						</tr>
					</thead>
					@if (!empty($pedidos))
						<tbody>
							@foreach($pedidos as $pedido)
								<tr>
									<td>{{$pedido['increment_id']}}</td>
									<td>{{$pedido['data_pedido']}}</td>
									<td>{{$pedido['customer_cpf']}}</td>
									<td>{{utf8_decode($pedido['nome_cliente'])}}</td>
									<td>{{$pedido['phone_service']}}</td>
									<td>{{utf8_decode($pedido['nome_plano'])}}</td>
									<td>{{utf8_decode($pedido['city'])}}</td>
									<td>{{$pedido['region']}}</td>
									<td>{{$pedido['postcode']}}</td>
									<td class="text-center">
										<button type="button" class="btn btn-primary btn-xs btn_modal" value="{{$pedido['increment_id']}}">
											<i class="fa fa-eye"></i>
										</button>
									</td>
								</tr>	
							@endforeach
						</tbody>
					@endif
				</table>
			</div>
		</form>
	</div>
</div>
@endsection