@extends('layouts.base')

@section('title', __('login.title'))
@section('content')

	@if(!empty($errors->first()))
		@alerta()
			{{ $errors->first() }}
		@endalerta
	@endif

	@if(Session::has('message'))
		<div class="callout callout-success">
			<p>{!! Session::get('message') !!}</p>
		</div>
	@endif

	<div class="login-box">
		<div class="login-logo">
			<a href="{{url('/')}}">{{config('app.name_portal')}}<b> {{config('app.company')}}</b></a>
		</div>
		<div class="login-box-body">
			<form action="{{route('portal.alterar_senha_portal')}}" method="post">
				@csrf
                
				<div class="form-group has-feedback">
					<input type="password" name="senha_atual" class="form-control" placeholder="Senha atual" required />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
                <div class="form-group has-feedback">
					<input type="password" name="nova_senha" class="form-control" placeholder="Nova senha" required />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
                <div class="form-group has-feedback">
					<input type="password" name="confirmacao" class="form-control" placeholder="Confirmar senha" required />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8"></div>
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Alterar</button>
					</div>
				</div>
			</form>
		</div>
	</div>

@endsection