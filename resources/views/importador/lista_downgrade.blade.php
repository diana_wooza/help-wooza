@extends('layouts.admin')

@section('title', 'Listar Downgrade Atual')

@push('header.css')

<style type="text/css">

	.limited-text-content {
		max-width:100%;
		overflow:hidden;
		text-overflow:ellipsis;
		white-space:nowrap;
	}


    .top_search {
        padding: 0
    }
    .top_search .form-control {
        border-right: 0;
        box-shadow: inset 0 1px 0px rgba(0, 0, 0, 0.075);
        border-radius: 25px 0px 0px 25px;
        padding-left: 20px;
        border: 1px solid rgba(221, 226, 232, 0.49)
    }
    .top_search .form-control:focus {
        border: 1px solid rgba(221, 226, 232, 0.49);
        border-right: 0
    }
    .top_search .input-group-btn button {
        border-radius: 0px 25px 25px 0px;
        border: 1px solid rgba(221, 226, 232, 0.49);
        border-left: 0;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        color: #93A2B2;
        margin-bottom: 0 !important
    }

</style>

@endpush

@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tabela Downgrade Atual @if( isset($_GET['operadora']) ) - {{strtoupper($_GET['operadora'])}} @endif</h3>
                </div>
                <div class="box-body">
                        <div class="col-md-12">
                            <div class="col-md-7 col-xs-12">
                                <button class="btn btn-primary" type="button" onclick="document.location='downgrade-listar?operadora=tim'">TIM</button>
                                <button class="btn btn-warning" type="button" onclick="document.location='downgrade-listar?operadora=vivo'">VIVO</button>
                            </div>

                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <form id="buscar" method="get">
                                    <div class="input-group">
                                        <input id="busca-nome" name="q" value="{{isset($_GET['q']) ? $_GET['q'] : ''}}" type="text" class="form-control" placeholder="Buscar por Plano Atual ou Destino...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Plano Atual</th>
                                    <th>Plano Destino</th>
                                    <th>Tipo de Movimentação</th>
                                </tr>
                            </thead>
                            @if($dados)
                            <tbody>
                                @foreach($dados as $model)
                                    <tr>
                                        <td>{{ $model->plano_atual }}</td>
                                        <td>{{ $model->plano_destino }}</td>
                                        <td>{{ $model->tipo_movimentacao }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">
                                        {{$dados->links()}}
                                    </td>
                                </tr>
                            </tfoot>
                            @endif
                        </table>
                </div>
            </div>
        </div>
</div>

@endsection