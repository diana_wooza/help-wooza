@extends('layouts.admin')

@section('title', 'Importar Arquivo: ' . @$importador->layout->nome)

@push('header.css')

<style type="text/css">

	.limited-text-content {
		max-width:100%;
		overflow:hidden;
		text-overflow:ellipsis;
		white-space:nowrap;
	}

</style>

@endpush

@section('content')

<form class="row" method="post" action="{{route('importador.importar', [$importador->id])}}" enctype="multipart/form-data">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
			
			<div class="form-horizontal">
				<div class="box-body">

					@switch (@$importador->regras->obter->tipo)

						@case('file_upload')

						<div class="form-group">
							<label for="nota" class="col-sm-2 control-label">Arquivo</label>
							<div class="col-sm-10">
								<input type="file" name="file" accept="{{@$importador->regras->obter->arquivo->accept}}" required />
								<p class="help-block">{{@$importador->regras->obter->arquivo->descricao}}</p>
							</div>
						</div>

						@break

					@endswitch
				
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<a class="btn btn-default" href="{{route('importador.importar')}}">Cancelar Importação</a>
							<button type="submit" class="btn btn-info pull-right">Importar Arquivo</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection