@extends('layouts.admin')

@section('title', 'Importador de Phone Number')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header with-border">
			<h3 class="box-title">Cole os Números do pedido e os número de telefone na caixa abaixo. Cada linha será considerada um pedido diferentes</h3>
			</div>
			<form action="" class="form-horizontal" method="post">
				@csrf
				<div class="box-body">
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="form-control" name="pedidos" style="height:300px;"></textarea>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-danger pull-right"><i class="fa fa-arrow-up"></i> &nbsp; Importar Pedidos</button>
				</div>
				<!-- /.box-footer -->
			</form>
		</div>
	</div>
</div>

@endsection