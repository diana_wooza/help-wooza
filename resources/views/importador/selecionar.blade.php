@extends('layouts.admin')

@section('title', 'Selecionar Importador')

@push('header.css')

<style type="text/css">

	.limited-text-content {
		max-width:100%;
		overflow:hidden;
		text-overflow:ellipsis;
		white-space:nowrap;
	}

</style>

@endpush

@section('content')

<div class="row">

	@forelse ($importadores as $importador)

		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="{{route('importador.importar', [$importador->id])}}" class="small-box {{!empty(@$importador->layout->cor) ? $importador->layout->cor : 'bg-aqua'}}">
				<div class="inner">
					<h3 class="limited-text-content" title="{{@$importador->layout->nome}}">{{@$importador->layout->nome}}</h3>
					<p class="limited-text-content" title="{{@$importador->layout->descricao}}">{{@$importador->layout->descricao}}</p>
				</div>
				<div class="icon">
					@switch(@$importador->layout->icone->tipo)

						@case('font-awesome')

							<i class="fa {{@$importador->layout->icone->valor}}"></i>
							@break

						@default

							<i class="fa fa-cloud-upload"></i>

					@endswitch
				</div>
				<span class="small-box-footer">Clique para começar a importação</span>
			</a>
		</div>

	@empty

		<div class="col-md-12 col-sm-12 col-xs-12">
			<p class="text-center text-muted">Você não possui acesso a importador algum.</p>
		</div>

	@endforelse

</div>

@endsection