@extends('layouts.admin')

@section('title', 'Importador Varejo Lojas')
@section('content')

@if(Session::has('message'))
<div class="row">
    <div class="col-md-12">
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <h4>Selecione um arquivo abaixo para fazer a importação na tabela Varejos Loja.</h4>
            @if( $result )
                <p>
                    Total de items na planilha: <b>{{$result['total_itens_planilha']}}</b><br>
                    Total de items importados: <b>{{$result['total_itens_importados']}}</b><br>
                    Total de items ignorados: <b>{{$result['total_itens_ignorados']}}</b><br>
                </p>
            @endif
        </div>
    </div>
</div>

<!--div class="row">
    <div class="col-md-12">
        <form action="{{route('importador.downloadplanilha')}}" class="form-horizontal" enctype="multipart/form-data" method="get">
            <button class="btn btn-primary pull-right" style="font-weight: bolder">
                <i class="fa fa-arrow-down"></i> &nbsp; FAZER DOWNLOAD DO MODELO DA PLANILHA!
            </button>
        </form>
    </div>
</div-->

<br />

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Importar Varejo Loja</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" enctype="multipart/form-data" method="post">
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <label for="planilha" class="col-sm-2 control-label">Planilha de Importação</label>
                        <div class="col-sm-10">
                            <input type="file" id="planilha" name="planilha" accept=".xlsx, .xls" />
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-arrow-up"></i> &nbsp; Importar Varejo Loja</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>

@endsection