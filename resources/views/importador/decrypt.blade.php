@extends('layouts.admin')

@section('title', 'Decriptar Pedidos do Magento')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Cole os Números do pedido na caixa abaixo. Separe os pedidos por vírgula, ponto e vírgula, ponto, espaço, quebra de linha ou o que mais desejar</h3>
			</div>
			<form action="" class="form-horizontal" method="post">
				@csrf
				<div class="box-body">
					<!-- <div class="form-group">
						<label class="col-sm-2">Colunas a serem exportadas</label>
						<div class="col-sm-10">
							<label><input type="checkbox" class="checkbox" name="colunas[]" value="email" style="opacity:1 !important; display:block !important;"> E-Mail </label>
						</div>
					</div> -->
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="form-control" name="pedidos" style="height:300px;"></textarea>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-danger pull-right"><i class="fa fa-arrow-up"></i> &nbsp; Exportar Pedidos</button>
				</div>
				<!-- /.box-footer -->
			</form>
		</div>
	</div>
</div>

@endsection