@extends('layouts.admin')

@section('title', 'Histórico de Consulta de Ordem TIM')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
            <h3 class="box-title">Histórico de Consulta de Ordem TIM</h3>
            </div>
        </div>

        <div class="box-body" style="background-color: white;">
            <form action="{{route('retornos.tim.historicos.consultaordem')}}" class="form-horizontal" method="GET">
                <div class="form-group">
                    <label for="ordem" class="col-sm-8 control-label">Filtrar por pedidos</label>
                    <div class="col-sm-4 pull-right">
                        <div class="input-group input-group-sm">
                            <input type="input" id="ordem" name="ordem" class="form-control" value="{{(isset($_GET['ordem'])) ? $_GET['ordem'] : ''}}" placeholder="Número da Ordem" />
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    
                </div>
                <!-- /.box-footer -->
            </form>

            <table id="list-historico-pedidos" class="table table-bordered table-hover table-striped" style="vertical-align: middle">
                <thead>
                    <tr>
                        <th width="60px">Ações</th>
                        <th width="80px">Nº Ordem</th>
                        <th>Status</th>
                        <th width="70px">Operadora</th>
                        <th width="90px">Modalidade</th>
                        <th>Fase do Processo</th>
                        <th width="70px">Data</th>
                    </tr>
                </thead>
                @if(isset($_GET['ordem']) && count($model) == 0)
                    <span style="color:red"><center>Nenhum registro encontrado.</center></span>
                @endif
                <tbody>
                    @foreach ($model as $historico)
                        <tr>
                            <td>
                                <button class="btn btn-warning" id="hist_{{$historico->id}}" data-result='{{$historico->json_data}}' data-toggle="modal" data-target="#modal-historico" onclick="copyJsonToModal({{$historico->id}})">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                            <td style="vertical-align: middle" id="pedido_{{$historico->id}}">{{$historico->pedido}}</td>
                            <td style="vertical-align: middle">{{$historico->status_ordem}}</td>
                            <td style="vertical-align: middle; text-align: center;">{{$historico->operadora}}</td>
                            <td style="vertical-align: middle; text-align: center;">{{$historico->modalidade}}</td>
                            <td style="vertical-align: middle">{{$historico->fase_processo}}</td>
                            <td style="vertical-align: middle">{{date('d/m/Y h:i:s', strtotime($historico->created_at))}}</td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
<br /><br />
<div class="row">
    <div class="col-md-12">
        <!-- DONUT CHART -->
        <div class="box box-danger">
            <div class="box-header with-border">
            <h3 class="box-title">Gráfico de Histórico das Consultas de Ordem do Dia</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <canvas id="pieChart" style="height:250px;"></canvas>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-group">
                            @foreach ($result as $data)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <b>{{$data->status_ordem}}</b>
                                    <span class="badge badge-danger badge-pill">{{$data->total_por_status}}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

<div class="modal fade" id="modal-historico">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="header"></h4>
            </div>
            <div class="modal-body">
               <pre><code id="json_view"></code></pre>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar Modal</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- /.modal -->
<script>
    function copyJsonToModal(id) {
        $('#json_view').html($('#hist_' + id).attr('data-result'));
        $('#header').html('Pedido: ' + $('#pedido_' + id).html());
    }


    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
    var pieChart       = new Chart(pieChartCanvas);
    var PieData        = {!! $graph !!};
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 0, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Pie(PieData, pieOptions)
</script>

@endsection