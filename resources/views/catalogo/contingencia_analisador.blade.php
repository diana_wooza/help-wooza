@extends('layouts.admin')

@section('title', 'Analisar arquivo do bundle')
@section('content')

<form class="row" method="post" action="">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Cole a URL do arquivo CSV que será analisado</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<div class="form-horizontal">
				<div class="box-body">
					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Arquivo</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="arquivo" value="{{old('arquivo')}}"/>
						</div>
					</div>

					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">Ajuste de URL</label>
						<div class="col-sm-10">
							<select name="url_default" class="form-control" required>
								@foreach ($url_default as $id => $url)
									<option value="{{$id}}" {{$id == old('url') ? 'selected' : ''}}>{{$url}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">Ambiente</label>
						<div class="col-sm-10">
							<select name="ambiente" class="form-control" required>
								<option value="" selected disabled>Escolha um</option>
								@foreach ($ambientes as $id => $ambiente)
									<option value="{{$id}}" {{$id == old('ambiente') ? 'selected' : ''}}>{{$ambiente}}</option>
								@endforeach
							</select>
						</div>
					</div>
				
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<button type="submit" class="btn btn-info pull-right">Analisar</button>
						</div>
					</div>
				</div>

				@if (!is_null($skus))
					<div class="row">
						<div class="col-sm-12">SKUs não encontrados:</div>
						<div class="col-sm-12">
							@forelse ($skus as $sku)<p class="label label-default" style="color:#000; margin:2px 3px; display:inline-block;">{{$sku}}</p>@empty

								<span class="label label-success">Nenhum SKU pendente</label>

							@endforelse
						</div>
					</div>
				@endif

			</div>
		</div>
	</div>
</form>

@endsection