@extends('layouts.admin')

@section('title', 'Listar atendimentos')

@push('header.javascript')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>
	
	<script>
		//const _URL_ = 'http://hub.wooza.com.br/posvenda';
		const _URL_ = "<?= env('APP_DOMAIN') . "posvenda"; ?>";
		
		$(function() {
			var motivos2 = @json($motivos2->map(function ($item) {return ['value' => $item->id, 'label' => $item->motivo];}));
			$("#motivo2_label").autocomplete({
				minLength: 0,
				source: motivos2,
				select: function(event, ui) {
					$("#motivo2_label").val(ui.item.label);
					$("#motivo2").val(ui.item.value);

					return false;
				}
			}).bind('focus', function(){ $(this).autocomplete("search");});

			var acoes = @json($acoes->map(function ($item) {return ['value' => $item->id, 'label' => $item->acao];}));
			$("#acoes").autocomplete({
				minLength: 0,
				source: acoes,
				select: function(event, ui) {
					$("#acoes").val(ui.item.label);
					$("#acao").val(ui.item.value);

					return false;
				}
			}).bind('focus', function(){ $(this).autocomplete("search");});

			$('#periodo').daterangepicker({ 
				autoUpdateInput: false,
				opens: "right",
				locale: { 
					format: 'DD/MM/YYYY',
					cancelLabel: 'Limpar',
					applyLabel: 'Aplicar',
					daysOfWeek: [
						"Dom",
						"Seg",
						"Ter",
						"Qua",
						"Qui",
						"Sex",
						"Sáb"
					],
					monthNames: [
						"Janeiro",
						"Fevereiro",
						"Março",
						"Abril",
						"Maio",
						"Junho",
						"Julho",
						"Agosto",
						"Setembro",
						"Outubro",
						"Novembro",
						"Dezembro"
					]
				}
			});

			$('#periodo').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});

			$('#periodo').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
			});

			$('.btn_modal').click(function() {
				$('.modal_chamado').html($(this).val());
			});

			$(".reset").click(function() {
				$(this).closest('form').find("input[type=text], select").val("");
			});
		});

		function deletar( url, mensagem ) {
			openConfimModal(mensagem, () => {
				window.location.href = url;
			})
		}

		function openConfimModal(question, callback){
			$('#confirm_question').html(question);
			$('#confirm').modal({
				backdrop: 'static',
				keyboard: false
			})
			.on('click', '#yes', function(e) {
				if( callback instanceof  Function ){
					callback();
				}
			});
		}

		function closeAlerta() {
			$('#alerta,#alertaApi').hide('slow');
		}

		function salvarNotificacao() {
			closeAlerta();
			if( false === $("#form-notificacao")[0].reportValidity() ) return false;

			let mensagem             = $('#mensagem_notificacao').val();
			let html                = [];

			
			if( 0 == mensagem.length) {
				$('#mensagem_notificacao')[0].reportValidity();

				return false;
			}

			$.ajax({
					type:'POST',
					url: _URL_ + '/aviso.salvar',
					data: {
						"_token"        : "{{ csrf_token() }}",
						mensagem        : mensagem
					},
					beforeSend: function(){
						$('.loader').show();
					},
					complete: function(){
						$('.loader').hide();
					},
					success: function(data){
						let valor = data.model;

						if( false === valor ) callAlerta("Não foi possível salvar a aviso.", 'alert-error');
						else {
							callAlerta("Aviso salvo com sucesso.", 'alert-success');
							$('#mensagem_notificacao').val('');
						}
					},
					error : function(result, status, message) {
						callAlerta(result.responseJSON.message, 'alert-error');
					}
				});
		}

		function callAlerta(mensagem, estilo, nome = '#alerta') {
			let alerta      = $(nome);
			let amensagem   = $(nome + '-message');

			amensagem.html( mensagem ).removeClass('alert-success').removeClass('alert-error').removeClass('alert-danger').addClass(estilo);
			alerta.show('slow');
		}
	</script>
@endpush
@push('header.css')
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' />
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css' />
	<style type="text/css">
		.box-primary {
			min-height: 50px;
			padding: 5px
		}
		.espaco {
			margin-right: 10px;
		}
		.paginacao {
			margin-top: -23px;
			margin-bottom: -23px;
		}

		/* hack para consertar o layout da grid, pois ela está passando da tela quando tem mais conteúdo que o limite da tela. */
		.table-bordered tr td {
			padding: 8px;
			line-height: 1.42857143;
			vertical-align: top;
		}

		.table-bordered tr th {
			padding: 8px;
			line-height: 1.42857143;
			vertical-align: top;
		}

		/* hack para ajustar as tags i de dentro dos botões/links */
		i { margin: 4 2 4 2; }
	</style>
@endpush

@section('content')
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Chamado</h4>
			</div>
			<div class="modal-body">
				<textarea class='modal_chamado' style="width:100%;min-height:200px;"></textarea>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalNotificacao" tabindex="-1" role="dialog" aria-labelledby="modalNotificacaoLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4>Cadastrar Aviso para os Usuários</h4>
			</div>
			<div class="modal-body">
				<div id="alerta" class="row" style="display:none;">
					<div class="col-md-12">
						<p id="alerta-message" class="alert alert-class alert-success"></p>
					</div>
				</div>
				<form id="form-notificacao" name="form-notificacao" action="" method="POST">
					@csrf
					<textarea id="mensagem_notificacao" name="mensagem" class='modal_chamado' style="width:100%;min-height:200px;" required="required"></textarea>
				</form>
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-success" id="enviar" onclick="salvarNotificacao()">ENVIAR</button>
            </div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<form action="{{route('posvenda.listar_atendimentos')}}" enctype="multipart/form-data" method="get" id='form'>
			@csrf
			<div class="box box-primary">
				<label>Total de atendimentos: {{ $atendimentos->total() }}</label>
				<button type="submit" name='submit' value='Filtrar' class="btn btn-primary pull-right espaco"><i class="fa fa-filter"></i>&nbsp;Filtrar</button>
				<button type="button" name='button' value='reset' class="btn btn-primary pull-right espaco reset" ><i class="fa fa-eraser"></i>&nbsp;Limpar filtros</button>
				<button type="submit" name='submit' value='Exportar' class="btn btn-primary pull-right espaco"><i class="fa fa-cloud-download"></i>&nbsp;Exportar</button>
				@if( \Auth::user()->checkPermissao("posvenda.admin") )
					<button type="button" name='reg_notificacao' class="btn btn-warning pull-right espaco"  data-toggle="modal" data-target="#modalNotificacao" ><i class="fa fa-bell"></i>&nbsp;Criar Aviso</button>
				@endif
				<nav class='paginacao'>
					@php try{echo $links = $atendimentos->appends(@$filtros)->links();} catch (\Throwable $throwable) {} @endphp
				</nav>
			</div>
			<div class="box box-success">
				<table class='table table-striped table-bordered table-hover table-sm'>
					<thead>
						<tr>
							<th width="6%">
								<label class="control-label">Ações</label>
							</th>
							<th>
								<label class="control-label">Período</label>
								<input type="text" class="form-control pull-right" id="periodo" name="periodo" value="{{ old('periodo', @$filtros['periodo']) }}">
							</th>
							<th>
								<label class="control-label">Protocolo</label>
								<input type='text' class="form-control" name='protocolo' value="{{ old('protocolo', @$filtros['protocolo']) }}" /></th>
							</th>
							<th>
								<label class="control-label">ID Atendimento</label>
								<input type='text' class="form-control" name='id_atendimento' value='{{ old('atendimento', @$filtros['atendimento']) }}' /></th>
							</th>
							<th>
								<label class="control-label">Origem</label>
								<select class="form-control" name='origem'>
									<option value="" selected disabled>Selecione</option>
									@foreach($origens as $origem)
										<option value="{{ $origem->id }}" {{ old('origem', @$filtros['origem']) == $origem['id'] ? 'selected' : '' }}>{{ $origem->origem }}</option>
									@endforeach
								</select>
							</th>
							<th>
								<label class="control-label">Canal</label>
								<select class="form-control" name='canal_atendimento'>
									<option value="" selected disabled>Selecione</option>
									@foreach($canais as $canal)
										<option value="{{ $canal->id }}" {{ old('canal_atendimento', @$filtros['canal_atendimento']) == $canal->id ? 'selected' : '' }}>{{ $canal->canal_atendimento }}</option>
									@endforeach
								</select>
							</th>
							<th>
								<label class="control-label">Operadora/Produto</label>
								<select class="form-control" name='operadora_produto'>
									<option value="" selected disabled>Selecione</option>
									@foreach($operadorasProdutos as $operadoraProduto)
										<option value="{{ $operadoraProduto->id }}" {{ old('operadora_produto', @$filtros['operadora_produto']) == $operadoraProduto->id ? 'selected' : '' }}>{{ $operadoraProduto->operadora_produto }}</option>
									@endforeach
								</select> 
							</th>
							<th>
								<label class="control-label">Pedido</label>
								<input type='text' class="form-control" name='numero_pedido' value='{{ old('numero_pedido', @$filtros['numero_pedido']) }}' />
							</th>
							<th>
								<label class="control-label">Nome Cliente</label>
								<input type='text' class="form-control" name='nome_cliente' value='{{ old('nome_cliente', @$filtros['nome_cliente']) }}' />
							</th>
							<th>
								<label class="control-label">CPF Cliente</label>
								<input type='text' class="form-control" name='cpf_cliente' value='{{ old('cpf_cliente', @$filtros['cpf_cliente']) }}' />
							</th>
							<th>
								<label class="control-label">Motivo</label>
								<select class="form-control" name='motivo1'>
									<option value="">Selecione</option>
									@foreach($motivos1 as $motivo)
										<option value="{{ $motivo->id }}" {{ old('motivo1', @$filtros['motivo1']) == $motivo->id ? 'selected' : '' }}>{{ $motivo->motivo }}</option>
									@endforeach
								</select> 
							</th>
							<th>
								<label class="control-label">Motivo 2</label>
								<input class="form-control" id='motivo2_label' name='motivo2_label' value='{{ old('motivo2_label', @$filtros['motivo2_label']) }}' />
								<input type="hidden" id="motivo2" name="motivo2" value='{{ old('motivo2', @$filtros['motivo2']) }}'>
							</th>
							<th>
								<label class="control-label">Ação</label>
								<input class="form-control" id='acoes' name='acoes' value='{{ old('acoes', @$filtros['acoes']) }}' />
								<input type="hidden" id="acao" name="acao" value='{{ old('acao', @$filtros['acao']) }}'>
							</th>
							<th>
								<label class="control-label">Usuário</label>
								<input type='text' class="form-control" name='usuario' value='{{ old('usuario', @$filtros['usuario']) }}' />
							</th>
						</tr>
					</thead>
					<tbody>

						@forelse ($atendimentos as $atendimento)

							<tr>
								<td class="text-center">

									<button type="button" class="btn btn-primary btn-xs btn_modal" data-toggle="modal" value='{{ $atendimento->chamado }}' data-target="#myModal">
										<i class="fa fa-eye"></i>
									</button>

									@if( $isAdmin )
										<button type="button" class="btn btn-danger btn-xs" onclick="deletar('{{route('posvenda.posvenda.deletar', $atendimento->id)}}', 'Tem certeza que deseja deletar este atendimento?');">
											<i class="fa fa-trash"></i>
										</button>
									@endif
								
								</td>
								<td>{{ date('d/m/Y H:i:s', strtotime($atendimento->created_at)) }}</td>
								<td>{{ $atendimento->protocolo }}</td>
								<td>{{ $atendimento->id_atendimento }}</td>
								<td>{{ @$atendimento->origem->origem }}</td>
								<td>{{ @$atendimento->canal->canal_atendimento }}</td>
								<td>{{ @$atendimento->operadora->operadora_produto }}</td>
								<td>{{ $atendimento->numero_pedido }}</td>
								<td>{{ $atendimento->nome_cliente }}</td>
								<td>{{ $atendimento->cpf_cliente }}</td>
								<td>{{ @$atendimento->motivo1->motivo }}</td>
								<td>{{ @$atendimento->motivo2->motivo }}</td>
								<td>{{ @$atendimento->acao->acao }}</td>
								<td>{{ @$atendimento->usuario->nome }}</td>
							</tr>

						@empty

							<tr><td colspan="14" class="text-center text-gray"><i>Nenhum registro encontrado</i></td></tr>
							
						@endforelse
					</tbody>
				</table>
			</div>
		</form>
	</div>
</div>

<!-- general confim modal -->
<div id="confirm" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
	<div class="modal-content">

		<div class="modal-header">
		<h4 class="modal-title">Atenção</h4>
		</div>
		<div class="modal-body">
		<h4 id="confirm_question">Text in a modal?</h4>
		</div>
		<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-primary" id="yes">Sim</button>
		<button type="button" data-dismiss="modal" class="btn btn-danger">Não</button>
		</div>

	</div>
	</div>
</div>

@endsection