@extends('layouts.admin')

@section('title', 'Registro de atendimentos')

@push('header.javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        $(function() {
            $('#motivo1').change( () => {
                $.ajax({
                    type:'GET',
                    url:'{{route("posvenda.buscar-motivos")}}/' + $("#motivo1").val(),
                    success:function(data){
                        if( undefined != data ) {
                            autocompleteMotivo(data.model);
                        }
                    }
                });
            } );

            var acoes = {!!json_encode($acoes)!!};
            $("#acoes").autocomplete({
                minLength: 0,
                source: acoes,
                select: function(event, ui) {
                    $("#acoes").val(ui.item.label);
                    $("#acao").val(ui.item.value);

                    return false;
                },
                close: function( event, ui ) {
                    var validacao = false;
                    $.each(acoes, function(i, v) {
                        if ($("#acoes").val() == v.label) {
                            validacao = true;
                        }
                    });
                    
                    if (!validacao) {
                        $("#acoes").val('');
                        $("#acao").val('');
                    }
                }
            }).bind('focus', function(){ $(this).autocomplete("search");});

            $("#acoes").blur(function() {
                var validacao = false;
                $.each(acoes, function(i, v) {
                    if ($("#acoes").val() == v.label) {
                        validacao = true;
                    }
                });
                
                if (!validacao) {
                    $("#acoes").val('');
                    $("#acao").val('');
                }
            });

            $("#form").validate({
                rules: {
                    origem: "required",
                    canal_atendimento: "required",
                    operadora_produto: "required",
                    nome_cliente: "required",
                    motivo1: "required",
                    motivo2_label: "required",
                    acoes: "required",
                    chamado: "required"
                }
            });

            $('#check_cliente').on('ifChanged', function(event){
                if ($('#check_cliente').prop("checked")) {
                    $("input[name=numero_pedido").val("Não Cliente");
                    $("input[name=numero_pedido").attr("disabled", true);
                }
                else {
                    $("input[name=numero_pedido").val('');
                    $("input[name=numero_pedido").attr("disabled", false);
                }
            });
            
            $('#check_cpf').on('ifChanged', function(event){
                if ($('#check_cpf').prop("checked")) {
                    $("input[name=cpf_cliente").val("Não Informado");
                    $("input[name=cpf_cliente").attr("disabled", true);
                }
                else {
                    $("input[name=cpf_cliente").val('');
                    $("input[name=cpf_cliente").attr("disabled", false);
                }
            });
        });

        function autocompleteMotivo(result) {
            let data = result;

            $("#motivo2_label").autocomplete({
                minLength: 0,
                source: data,
                select: function(event, ui) {
                    $("#motivo2_label").val(ui.item.label);
                    $("#motivo2").val(ui.item.value);

                    return false;
                },
                close: function( event, ui ) {
                    var validacao = false;
                    
                    $.each(data, function(i, v) {
                        $("#motivo2_label").off();
                        if ($("#motivo2_label").val() == v.label) {
                            validacao = true;
                        }
                    });
                    
                    if (!validacao) {
                        $("#motivo2_label").val('');
                        $("#motivo2").val('');
                    }
                }
            }).bind('focus', function(){ $(this).autocomplete("search");});

            $("#motivo2_label").blur(function() {
                var validacao = false;
                $.each(data, function(i, v) {
                    if ($("#motivo2_label").val() == v.label) {
                        validacao = true;
                    }
                });
                
                if (!validacao) {
                    $("#motivo2_label").val('');
                    $("#motivo2").val('');
                }
            });
        }

        function checkCamposNaoInformado() {
            if( false == $('#check_cliente').prop("checked") ) {
                $('#check_cliente').prop("checked", true);
                $("input[name=numero_pedido").val("Não Cliente");
                $("input[name=numero_pedido").attr("disabled", true);
            }

            if( false == $('#check_cpf').prop("checked") ) {
                $('#check_cpf').prop("checked", true);
                $("input[name=cpf_cliente").val("Não Informado");
                $("input[name=cpf_cliente").attr("disabled", true);
            }
            
            $("#motivo2_label").val('Não Informado').prop('disabled', true);
            $('#motivo2').val(48);
            $('#btn_camposNaoInformados').prop('disabled', true);
        }

        function preencherTudo() {
            $('#canal_atendimento').val(2);
            $('#numero_pedido,#nome_cliente,#acoes,#motivo2_label').val('Contingência/Backlog');
            $('#cpf_cliente').val('Backlog');

            $('#acao').val(43);
            $('#motivo1').val(1);
            $('#motivo2').val(80);
        }
    </script>
@endpush
@push('header.css')
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' />
    <style type="text/css">
        label.error {
            display: none !important;
        }
        .form-control.error {
            border-color: red !important;
        }
        input[type=checkbox], .iCheck-helper {
            height: 20px !important;
            position: relative !important;
            opacity: 1 !important;
            width: 14px !important;
            left: -6px !important;
            float: left;
        }
        .iCheck-helper {
            left: -20px !important;
            opacity: 0 !important;
        }
        .input-group-addon {
            padding-right: 17px;
        }
        .form_row {
            margin-left: 0;
        }

        .mg_20 {
            margin-bottom: 20px;
        }

        .right {
            float: right;
        }

        label {
            margin-bottom: 10px!important;
        }
    </style>
@endpush

@section('content')

@if(Session::has('message'))
<div class="row">
    <div class="col-md-12">
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    </div>
</div>
@endif

@if ($protocolo)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <p><b>Atendimento registrado com o protocolo: {{$protocolo}}</b></p>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <form action="{{route('posvenda.salvar_atendimento')}}" class="form-horizontal" enctype="multipart/form-data" method="post" id='form'>
                @csrf
                <div class="box-body">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="col-md-12">
                                <div class="right">
                                    <button id="btn_camposNaoInformados" type="button" class="btn btn-warning" onclick="checkCamposNaoInformado()"><i class="fa fa-adjust"></i>&nbsp; Marcar Campos Não Informados</button>
                                    <button id="btn_preencherTudo" type="button" class="btn btn-success" onclick="preencherTudo()"><i class="fa fa-barcode"></i>&nbsp; Resposta Automática</button>
                                </div>
                            </div>                            
                        </div>
                        <div class="x_content">
                            <br>
                            <div class="row mg_20">
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="origem">Origem</label>
                                    </div>
                                    <div class="col-md-12">
                                        <select class="form-control" name='origem' id="origem">
                                            <option value="">Selecione</option>
                                            @foreach($origens as $origem)
                                                <option value="{{$origem['id']}}">{{$origem['origem']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="canal_atendimento">Canal de Atendimento</label>
                                    </div>
                                    <div class="col-md-12">
                                        <select class="form-control" name='canal_atendimento' id="canal_atendimento">
                                            <option value="">Selecione</option>
                                            @foreach($canais as $canal)
                                                <option value="{{$canal['id']}}">{{$canal['canal_atendimento']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mg_20">
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="numero_pedido">Número do Pedido</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group" style="width: 100%">
                                            <input type='text' class="form-control" name='numero_pedido' id="numero_pedido" placeholder="Digite o número do pedido" />
                                            <span class="input-group-addon">
                                                <input type="checkbox" id='check_cliente' name='check_cliente'>
                                                <span style='line-height:18px;margin-left: -15px;margin-right: 15px;'>Não Cliente</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="cpf_cliente">CPF do Cliente</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group" style="width: 100%">
                                            <input type='text' class="form-control" name='cpf_cliente' id="cpf_cliente" placeholder="Digite o número do CPF" />
                                            <span class="input-group-addon">
                                                <input type="checkbox" id='check_cpf' name='check_cpf'>
                                                <span style='line-height:18px;margin-left: -15px;margin-right: 15px;'>Não Informado</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mg_20">
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="nome_cliente">Nome do Cliente</label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type='text' class="form-control" name='nome_cliente' id="nome_cliente" placeholder="Digite o nome do cliente" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="operadora_produto">Operadora / Produto</label>
                                    </div>
                                    <div class="col-md-12">
                                        <select class="form-control" name='operadora_produto' id="operadora_produto">
                                            <option value="">Selecione</option>
                                            @foreach($operadorasProdutos as $operadoraProduto)
                                                <option value="{{$operadoraProduto['id']}}">{{$operadoraProduto['operadora_produto']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mg_20">
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="motivo1">Motivo de Contato</label>
                                    </div>
                                    <div class="col-md-12">
                                        <select class="form-control" id="motivo1" name='motivo1'>
                                            <option value="" selected disabled>Selecione</option>
                                            @foreach($motivos1 as $motivo1)
                                                <option value="{{$motivo1['id']}}">{{$motivo1['motivo']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="motivo2_label">Motivo de Contato 2</label>
                                    </div>
                                    <div class="col-md-12">
                                        <input class="form-control" id='motivo2_label'  name='motivo2_label' placeholder="Selecione um motivo" />
                                        <input type="hidden" id="motivo2" name="motivo2">
                                    </div>
                                </div>
                            </div>

                            <div class="row mg_20">
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="acoes">Ação</label>
                                    </div>
                                    <div class="col-md-12">
                                        <input class="form-control" id='acoes' name='acoes' placeholder="Selecione uma ação" />
                                        <input type="hidden" id="acao" name="acao">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="id_atendimento">ID Atendimento</label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type='text' class="form-control" name='id_atendimento' id="id_atendimento" placeholder="Digite o ID do atendimento" />
                                    </div>
                                </div>
                            </div>

                            <div class="row mg_20">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <label class="control-label" for="chamado">Chamado</label>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea class="form-control" name='chamado' id="chamado" placeholder="Digite as informações do chamado"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer right">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Registrar</button>
                                
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>

@endsection