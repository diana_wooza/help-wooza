@extends('layouts.admin')

@section('title', 'Administrar Motivos')

@push('header.css')

<style type="text/css">

	.limited-text-content {
		max-width:100%;
		overflow:hidden;
		text-overflow:ellipsis;
		white-space:nowrap;
	}
    .div-select {
        /* width:250px;  /* Tamanho final do select */
        overflow:hidden; /* Esconde o conteúdo que passar do tamanho especificado */
    }

    .div-select-warning select {
        background: #fff8e6;
    }

    .div-select-default select {
        background: #e5f1ff;
    }

    .div-select option {
        font-family:Arial, Helvetica, sans-serif; /* Fonte do Select */
        width: 100%;
        margin: 8px 8px 8px 8px;
        font-size:16px; /* Tamanho da Fonte */
        text-indent: 0.01px; /* Remove seta padrão do FireFox */
        text-overflow: "";  /* Remove seta padrão do FireFox */     
        select::-ms-expand {display: none;} /* Remove seta padrão do IE*/
    
    }
</style>

@endpush

@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Selecão de Motivos</h3>
                </div>
                <div class="box-body">
                    <!-- begin form -->
                    <form id="formSelecionarMotivos" data-parsley-validate class="form-horizontal" method="POST">
                        @csrf
                        <div class="row">
                            <br />
                            <div class="col-md-12">
                                <center>
                                        <p class="alert alert-clear">Selecione motivos para aparecer na lista do Motivo1: <b>{{$motivo->motivo}}</b>.</p>
                                </center>
                                <input id="motivo1_id" name="motivo1_id" type="hidden" class="form-control" disabled value="{{$motivo->id}}" />
                                <input id="motivo2_selecionados" name="motivos_2" type="hidden" class="form-control" value="{{$motivosSelecionados}}" />
                            </div>
                        </div>        

                        <div class="row" style="background: #F3F3F3; border: 1px solid #d3d3d3; margin-left: 0; margin-right: 0;">
                            <div class="col-md-5">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="unselectedMotivos" style="text-align: left;">Motivos</label>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback div-select div-select-default">
                                    <select id="unselectedMotivos" name="unselectedMotivos" class="select2_multiple form-control" size="11" multiple="multiple">
                                        @foreach( $motivos as $item )
                                            <option value="{{$item->id}}">{{$item->motivo}}</option>
                                        @endforeach
                                        <option value="" disabled>&nbsp;</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <br /><br />
                                <table class="table">
                                    <tr>
                                        <td style="text-align: center; border: none;"><button id="addOne" type="button" class="btn btn-dark btn-block" disabled><i class="fa fa-arrow-right"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; border: none;"><button id="addAll" type="button" class="btn btn-dark btn-block"><i class="fa fa-forward"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; border: none;"><button id="removeOne" type="button" class="btn btn-dark btn-block" disabled><i class="fa fa-arrow-left"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; border: none;"><button id="removeAll" type="button" class="btn btn-dark btn-block" disabled><i class="fa fa-backward"></i></button></td>
                                    </tr>
                                </table>

                            </div>
                            <div class="col-md-5">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="selectedMotivos" style="text-align: left;">Motivos Selecionados<span class="required">*</span></label>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback div-select div-select-warning">
                                    <select id="selectedMotivos" name="motivos[]" class="select2_multiple form-control" size="11" multiple="multiple">
                                        @foreach( $dados as $item )
                                            <option value="{{$item->motivo2_id}}">{{$item->motivo2->motivo}}</option>
                                        @endforeach
                                        <option value="" disabled>&nbsp;</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <br />
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-10">
                                <a class="btn btn-danger" href="{{route('posvenda.listar_motivos')}}">Cancelar</a>
                                <button id="btnSubmit" type="submit" class="btn btn-success">Salvar</button>
                            </div>
                        </div>
                    </form>
                    <!-- end form -->
                </div>
            </div>
        </div>
</div>

<script>
    $(document).ready(() => {
        var selecteds       = $('#selectedMotivos');
        var unselecteds     = $('#unselectedMotivos');
        var btnAddOne       = $('#addOne');
        var btnAddAll       = $('#addAll');
        var btnRemoveOne    = $('#removeOne');
        var btnRemoveAll    = $('#removeAll');

        verificarCombos();

        btnAddOne.click( addItem );
        btnAddAll.click( () => {
            return addItem( true );
        } );

        btnRemoveOne.click( removeItem );
        btnRemoveAll.click( () => {
            return removeItem( true );
        } );

        $('#selectedMotivos, #unselectedMotivos').change( (event) => {
            verificarCombos();
        });

        $('#formSelecionarMotivos').on('submit', (e) => {
            e.preventDefault();

            submitItems( () => {
                return $('#formSelecionarMotivos').unbind('submit').submit();
            } );
        });
    })

    function verificarCombos() {
        var btnAddOne       = $('#addOne');
        var btnAddAll       = $('#addAll');
        var btnRemoveOne    = $('#removeOne');
        var btnRemoveAll    = $('#removeAll');
        var cntSel          = 0; 
        var hasSelectedSel  = true;
        var cnt             = 0; 
        var hasSelected     = true;

        $('#unselectedMotivos option').each( (index, value) => { 
            if( $(value).val().length ) cnt++;
            if( $(value).prop('selected') ) hasSelected = false;
        } );

        var disabled = (0 == cnt);
        btnAddOne.prop('disabled', hasSelected);
        btnAddAll.prop('disabled', disabled);

        $('#selectedMotivos option').each( (index, value) => { 
            if( $(value).val().length ) cntSel++;
            if( $(value).prop('selected') ) hasSelectedSel = false;
        } );

        var disabledSel = (0 == cntSel);
        btnRemoveOne.prop('disabled', hasSelectedSel);
        btnRemoveAll.prop('disabled', disabledSel);

        $('#btnSubmit').prop('disabled', ($('#selectedMotivos option').length > 1) ? false : true);
    }

    function addItem( all = false ) {
        var selecteds       = $('#selectedMotivos');

        if( true === all ) {
            $('#unselectedMotivos option').each( (index, value) => { 
                if( $(value).val().length ) {
                    selecteds.prepend( new Option($(value).text(), $(value).val(), false, true ) );
                    $(value).remove();
                }
            } );

            return verificarCombos();
        }

        $('#unselectedMotivos option').each( (index, value) => { 
            if( $(value).prop('selected') ) {
                selecteds.prepend( new Option($(value).text(), $(value).val(), false, true ) );
                $(value).remove();
            }
        } );

        return verificarCombos();
    }

    function removeItem( all = false ) {
        var unselecteds     = $('#unselectedMotivos');

        if( true === all ) {
            $('#selectedMotivos option').each( (index, value) => { 
                if( $(value).val().length ) {
                    unselecteds.prepend( new Option($(value).text(), $(value).val() ) );
                    $(value).remove();
                }
            } );

            return verificarCombos();
        }

        $('#selectedMotivos option').each( (index, value) => { 
            if( $(value).prop('selected') ) {
                unselecteds.prepend( new Option($(value).text(), $(value).val() ) );
                $(value).remove();
            }
        } );

        return verificarCombos();
    }

    function submitItems( callback ) {
        let selecionados = [];

        $('#selectedMotivos option').each( (index, value) => { 
            if( $(value).val().length ) {
                selecionados.push($(value).val());
            }
        } );

        $('#motivo2_selecionados').val(selecionados.join(','));

        callback();
    }

</script>
@endsection