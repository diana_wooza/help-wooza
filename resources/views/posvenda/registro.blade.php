@extends('layouts.admin')

@section('title', 'Registro de atendimentos')
@push('header.javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        var acoes = {!!json_encode($acoes)!!};
        const _URL_ = "{{env('APP_DOMAIN') . 'posvenda'}}";
    </script>
    <script src="{{url('js/posvenda/posvenda.min.js')}}"></script>
@endpush
@push('header.css')
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' />

    <style>
        .scroll {
            height: 120px;
            overflow-y: auto;
        }
        .label_posvenda {
            font-size: 12px;
            color: grey;
        }

        .border_element { border: 1px solid #e3e3e3; }

        .hiddenRow {
            padding: 0 4px !important;
            background-color: #eeeeee;
            font-size: 13px;
        }

        .btn-border-danger {
            border: 1px solid #d73925;
            color: #d73925;
            transition: 
        }
        .btn-border-danger:hover {
            color: #bd2130;
            border: 1px solid #bd2130;
        }

        .modal.modal-wide .modal-dialog { width: 60%; }
        .modal-wide .modal-body { overflow-y: auto; }

        input[type=checkbox], .iCheck-helper {
            height: 20px !important;
            position: relative !important;
            opacity: 1 !important;
            width: 14px !important;
            left: -6px !important;
            float: left;
        }
        .iCheck-helper {
            left: -20px !important;
            opacity: 0 !important;
        }
        .input-group-addon { padding-right: 17px; }
        .form_row { margin-left: 0; }
        .area-busca { background-color: #F5F5F5; }
        #acoes {
            position: relative;
            z-index: 10000;
        }
        .ui-autocomplete { z-index: 9999 !important; }
         /** Tabs **/
         .navbar { margin-bottom: 0; }
        .navbar-header { background: #34495E; }
        .navbar-right { margin-right: 0; }

        .top_nav .navbar-right {
            margin: 13px;
            width: auto;
            float: right; 
        }

        .top_nav .navbar-right li {
            display: inline-block;
            float: right;
            position: static; 
        }

        ul.bar_tabs {
            overflow: visible;
            background: #F5F7FA;
            height: 25px;
            margin: 21px 0 14px;
            padding-left: 14px;
            position: relative;
            z-index: 1;
            width: 100%;
            border-bottom: 1px solid #E6E9ED; 
        }

        ul.bar_tabs > li {
            border: 1px solid #E6E9ED;
            color: #333 !important;
            margin-top: -17px;
            margin-left: 8px;
            background: #fff;
            border-bottom: none;
            border-radius: 4px 4px 0 0; 
        }

        ul.bar_tabs > li.active {
            border-right: 6px solid #D3D6DA;
            border-top: 0;
            margin-top: -15px; 
        }

        ul.bar_tabs > li a {
            padding: 10px 17px;
            background: #F5F7FA;
            margin: 0;
            border-top-right-radius: 0; 
        }

        ul.bar_tabs > li a:hover { border: 1px solid transparent; }
        ul.bar_tabs > li.active a { border-bottom: none; }
        ul.bar_tabs.right { padding-right: 14px; }
        ul.bar_tabs.right li { float: right; }
        a:focus { outline: none;  }
        .active a span.fa {
            text-align: right !important;
            margin-right: 4px; 
        }
        /** /Tabs **/
    </style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-body">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <button class="btn btn-warning" type="buttom" onclick="abrirModalAtendimentoAvulso( '', '','', '', '', '', 7 )" @if( false == isset($_GET['cpf_busca']) || (0 < count($pedidos) || 0 < count($chamados)) ) disabled @endif>
                                    Novo Atendimento Avulso &nbsp;&nbsp;&nbsp;<i class="fa fa-plus-square"></i>
                                </button>
                                @if(false == isset($_GET['cpf_busca']))
                                    <button id="btn-busca" class="btn btn-primary" type="buttom">
                                        Nova Busca de Clientes &nbsp;&nbsp;&nbsp;<i class="fa fa-search"></i>
                                    </button>
                                @else
                                    <button id="btn-nova-busca" class="btn btn-primary" type="buttom">
                                        Nova Busca de Clientes &nbsp;&nbsp;&nbsp;<i class="fa fa-search"></i>
                                    </button>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <?php
                                $disabled    = "disabled";
                                $estilo     = "default";

                                if( count($notificacoes) ) {
                                    $estilo     = "danger";
                                    $disabled    = "";
                                } 
                                ?>
                                <button class="btn btn-{{$estilo}} pull-right" type="buttom" data-toggle="modal" data-target="#modalNotificacao" {{$disabled}}>
                                    <b>({{count($notificacoes)}}) </b><i class="fa fa-bell"></i>
                                </button>
                            </div>
                           
                            @if( (0 == count($pedidos) && 0 == count($chamados)) && isset($_GET['cpf_busca']) )
                            <div class="row">
                                <div class="col-md-12">
                                    <p><center style="color:red">Não foram encontrados resultados compatíveis com seus critérios de busca.</center></p>
                                </div>
                            </div>
                            @endif

                            <!-- BEGIN: BUSCA -->
                            <div id="area-busca" class="row" @if( isset($_GET['cpf_busca']) ) style="display:none;" @endif>
                                <form id="form-busca" name="form-busca" method="GET">
                                    <div class="col-md-12 border_element area-busca" style="margin-top: 15px">
                                        <div class="col-md-12">
                                            <h3>Buscar Cliente</h3>
                                            <hr />
                                        </div>
                                        
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-md-12">
                                                <label class="control-label" for="cpf_busca">CPF do Cliente</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group" style="width: 100%">
                                                    <input type='text' class="form-control" name='cpf_busca' id="cpf_busca" placeholder="Digite o número do CPF" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-md-12">
                                                <label class="control-label" for="pedido_busca">Número do Pedido</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group" style="width: 100%">
                                                    <input type='text' class="form-control" name='pedido_busca' id="pedido_busca" placeholder="Digite o número do pedido" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <hr />
                                            <div class="col-md-12" style="margin-bottom: 15px">
                                                <button id="btn-enviar-busca" class="btn btn-primary pull-right" type="submit" disabled>Enviar &nbsp;&nbsp;&nbsp;<i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- END: BUSCA -->

                            @php
                                if( 0 < count($pedidos) )
                                    $dadosCliente = $pedidos[0];
                                
                                if( 0 < count($chamados) && false == isset($dadosCliente) )
                                    $dadosCliente = (object)['pedido_busca' => $_GET['pedido_busca'],'CustomerFullname' => $chamados[0]->nome_cliente, 'customer_email' => $chamados[0]->email, 'plan_operator' => $chamados[0]->operadora->operadora_produto, 'CustomerCpf' => $chamados[0]->cpf_cliente, 'customer' => (object)['data_nascimento' => null, 'mother_full_name' => 'N/A'], 'phone_service' => $chamados[0]->numero_telefone, 'customerFullAddress' => ['streetName' => 'N/A', 'number' => 'N/A', 'neighborhood' => 'N/A', 'city' => 'N/A', 'postcode' => 'N/A']];
                            @endphp

                            @if( isset($dadosCliente) )
                                <!-- BEGIN: RESUMO DO DONO DO PEDIDO -->
                                <div class="row">
                                    <div class="col-md-12 border_element" style="margin-top: 15px">
                                        <div class="col-md-12">
                                            <h4>Dados do Cliente</h4>
                                            <hr />
                                        </div>

                                        <!-- conteudo pedidos -->
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <span class="label_posvenda">Nome:</span>
                                            <p id="nome_c">{{@$dadosCliente->CustomerFullname}}</p>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <span class="label_posvenda">Nascimento:</span>
                                            <p>{{\date('d/m/Y', \strtotime(@$dadosCliente->customer->data_nascimento))}}</p>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <span class="label_posvenda">CPF:</span>
                                            <p id="cpf_c">{{@$dadosCliente->CustomerCpf}}</p>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <span class="label_posvenda">Telefone:</span>
                                            <p id="telefone_c">{{(strlen($dadosCliente->phone_service)) ? @$dadosCliente->phone_service : @$dadosCliente->customer_cellphone}}</p>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <span class="label_posvenda">E-mail:</span>
                                            <p id="email_c">{{@$dadosCliente->customer_email}}</p>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <span class="label_posvenda">Nome da Mãe:</span>
                                            <p>{{@$dadosCliente->customer->mother_full_name}}</p>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <span class="label_posvenda">Endereço:</span>
                                            <p>
                                            @if( $dadosCliente->customerFullAddress['streetName'] == 'N/A' )
                                                N/A
                                            @else
                                                {{$dadosCliente->customerFullAddress['streetName']}} {{$dadosCliente->customerFullAddress['number']}} <br />
                                                {{$dadosCliente->customerFullAddress['neighborhood']}} {{$dadosCliente->customerFullAddress['city']}} - CEP: {{$dadosCliente->customerFullAddress['postcode']}}
                                            @endif
                                            </p>
                                        </div>
                                        <!-- conteudo pedidos -->
                                        
                                    </div>
                                </div>
                                <!-- END: RESUMO DO DONO DO PEDIDO -->
                            @endif

                            <input type="hidden" id="operadoraSelecionada" value="{{@$dadosCliente->plan_operator}}" />
                            <br /><br />

                            @if( (0 < count($pedidos) || 0 < count($chamados)) )
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                                            <li class="nav-item active">
                                                <a class="nav-link active in" id="venda-tab" data-toggle="tab" href="#venda" role="tab" aria-controls="venda" aria-selected="true">Dados da Venda</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="historico-tab" data-toggle="tab" href="#historico" role="tab" aria-controls="historico" aria-selected="false">Histórico de Atendimentos</a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-content" id="myTabContent">
                                                <!-- BEGIN: TAB VENDAS -->
                                                <div class="tab-pane fade active in" id="venda" role="tabpanel" aria-labelledby="venda-tab">
                                                    <div class="col-md-12">
                                                        @if( 0 < count($pedidos) )
                                                            <!-- BEGIN: DADOS DO PEDIDO -->
                                                            <div>
                                                                <div class="col-md-12">
                                                                    <table class="table table-striped table-bordered">
                                                                        <thead>
                                                                            <tr class="label_posvenda">
                                                                                <td><center>#</center></td>
                                                                                <td>Pedido</td>
                                                                                <td>Data e Hora</td>
                                                                                <td>Operadora</td>
                                                                                <td>Nome do Plano</td>
                                                                                <td>Tipo de Plano</td>
                                                                                <td>Valor</td>
                                                                                <td>Modalidade</td>
                                                                                <td>Status</td>
                                                                                <td>Produto</td>
                                                                                <td>Origem Pedido</td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach( $pedidos as $pedido )
                                                                            <tr>
                                                                                <td>
                                                                                        <button type="button" class="btn btn-info btn-sm" onclick="buscaComentarios({{$pedido->entity_id}}, '{{$pedido->increment_id}}')"><i class="fa fa-comment"></i></button>
                                                                                        <button id="btn-{{$pedido->CustomerCpf}}" type="button" class="btn btn-primary btn-sm" onclick="buscarAtendimentosPorPedido('{{$pedido->increment_id}}','{{$pedido->plan_operator}}')"><i class="fa fa-ellipsis-v"></i></button>
                                                                                    
                                                                                </td>
                                                                                <td>{{$pedido->increment_id}}</td>
                                                                                <td>{{\date('d/m/Y H:i:s', \strtotime(@$pedido->created_at))}}</td>
                                                                                <td id="operadora_{{$pedido->increment_id}}">{{$pedido->plan_operator}}</td>
                                                                                <td>{{$pedido->plan_name}}</td>
                                                                                <td>{{$pedido->plan_tipo}}</td>
                                                                                <td>{{$pedido->plan_price}}</td>
                                                                                <td>{{$pedido->plan_modality}}</td>
                                                                                <td>{{$pedido->status}}</td>
                                                                                <td>{{$pedido->product_name}}</td>
                                                                                <td>{{$pedido->origem_pedido}}</td>
                                                                            </tr>
                                                                            <tr id="trped_{{$pedido->increment_id}}">
                                                                                
                                                                            </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        {{$pedidos->links()}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- BEGIN: DADOS DO PEDIDO -->
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- END: TAB VENDAS -->
                                                <!-- BEGIN: TAB HISTORICO DE ATENDIMENTOS -->
                                                <div class="tab-pane fade" id="historico" role="tabpanel" aria-labelledby="historico-tab">
                                                    <div class="col-md-12">
                                                        <!-- BEGIN: DADOS DO ATENDIMENTO -->
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button class="btn btn-border-danger pull-right" style="margin-top: 15px !important;" type="button" onclick="abrirModalAtendimentoAvulso('', '{{@$dadosCliente->phone_service}}', '{{@$dadosCliente->customer_email}}','{{@$dadosCliente->CustomerCpf}}', '{{@$dadosCliente->CustomerFullname}}', '')">
                                                                    Novo Atendimento &nbsp;&nbsp;&nbsp;<i class="fa fa-plus-square"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div>
                                                            <div class="col-md-12">
                                                                <table class="table table-striped table-bordered">
                                                                    <thead>
                                                                        <tr class="label_posvenda">
                                                                            <td><center>#</center></td>
                                                                            <td>Data e Hora</td>
                                                                            <td>Pedido</td>
                                                                            <td>Canal</td>
                                                                            <td>Origem Cliente</td>
                                                                            <td>Operadora</td>
                                                                            <td>Motivo 1</td>
                                                                            <td>Motivo 2</td>
                                                                            <td>Ação</td>
                                                                            <td>Protocolo</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="resultado_chamados">
                                                                        @foreach( $chamados as $chamado )
                                                                            <tr>
                                                                                <td>
                                                                                    <center>
                                                                                        <button type="button" class="btn btn-info btn-xs btn_modal" data-toggle="modal" value='{{ $chamado->chamado }}' data-target="#myModal">
                                                                                            <i class="fa fa-eye"></i>
                                                                                        </button>
                                                                                    </center>
                                                                                </td>
                                                                                <td>{{\date('d/m/Y H:i:s', \strtotime(@$chamado->created_at))}}</td>
                                                                                <td>{{@$chamado->numero_pedido}}</td>
                                                                                <td>{{@$chamado->canal->canal_atendimento}}</td>
                                                                                <td>{{@$chamado->origem->origem}}</td>
                                                                                <td>{{@$chamado->operadora->operadora_produto}}</td>
                                                                                <td>{{@$chamado->motivo1->motivo}}</td>
                                                                                <td>{{@$chamado->motivo2->motivo}}</td>
                                                                                <td>{{@$chamado->acao->acao}}</td>
                                                                                <td>{{@$chamado->protocolo}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody> 
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- END: DADOS DO ATENDIMENTO -->
                                                    </div>
                                                </div>
                                                <!-- END: TAB HISTORICO DE ATENDIMENTOS -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            
                        </div>                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: modals -->
<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="confirmLabel" style="z-index: 1100">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h4>Deseja realmente cancelar este pedido? Após confirmar, não será possível reverter a operação.</h4>
			</div>
			<div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success" id="sim">SIM</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger" id="nao">NÂO</button>
            </div>
		</div>
	</div>
</div>

<div class="modal fade" id="confirm_valida" tabindex="-1" role="dialog" aria-labelledby="confirmValidaLabel" style="z-index: 1100">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h4>Existem campos não preenchidos. Deseja salvar mesmo assim?</h4>
			</div>
			<div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success" id="sim">SIM</button>
                <button type="button" data-dismiss="modal" class="btn btn-danger" id="nao">NÂO</button>
            </div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalValidacao" tabindex="-1" role="dialog" aria-labelledby="modalValidacaoLabel" style="z-index: 1100">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h4 id="textoValidacao"></h4>
			</div>
			<div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger" id="close">FECHAR</button>
            </div>
		</div>
	</div>
</div>

<div id="modal-cadastro" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-md-6">
                    <h3 class="modal-title">Registrar Atendimento</h3>
                </div>
                <div class="col-md-6 pull-right">
                    <button type="button" class="close" aria-label="Close" onclick="fecharModalAtendimento()">
                        <span aria-hidden="true">&times;</span>
                    </button>                
                </div>
            </div>
            <form id="form-cadastro" name="form-cadastro" action="{{url('posvenda.cadastrar')}}" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button id="btn_abandono" type="button" class="btn btn-danger" onclick="checkCamposAbandono()"><i class="fa fa-adjust"></i>&nbsp; Registrar Abandono</button>
                                <button id="btn_camposNaoInformados" type="button" class="btn btn-warning" onclick="checkCamposNaoInformado()"><i class="fa fa-adjust"></i>&nbsp; Marcar Campos Não Informados</button>
                                <button id="btn_preencherTudo" type="button" class="btn btn-success" onclick="preencherTudo()"><i class="fa fa-barcode"></i>&nbsp; Resposta Automática</button>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr />
                        </div>
                    </div>

                    <div id="alerta" class="row" style="display:none;">
                        <div class="col-md-12">
                            <p id="alerta-message" class="alert alert-class alert-success"></p>
                        </div>
                    </div>
                    <div id="alertaApi" class="row" style="display:none;">
                        <div class="col-md-12">
                            <p id="alertaApi-message" class="alert alert-class alert-success"></p>
                        </div>
                    </div>
                    @csrf
                    <!-- BEGIN: ORIGEM - CANAL DE ATENDIMENTO -->
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="origem">Origem</label>
                            </div>
                            <div class="col-md-12">
                                <select class="form-control" name='origem' id="origem"  required="required">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="canal_atendimento">Canal de Atendimento</label>
                            </div>
                            <div class="col-md-12">
                                <select class="form-control" name='canal' id="canal_atendimento"  required="required">
                                    <option value="">Selecione</option>
                                    @foreach( $canais as $canal )
                                    <option value="{{$canal->id}}">{{$canal->canal_atendimento}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="pedido">Número do Pedido</label>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group" style="width: 100%">
                                    <input type='text' class="form-control" name='pedido' id="pedido" placeholder="Digite o número do pedido" />
                                    <span class="input-group-addon">
                                        <input type="checkbox" id='check_cliente' name='check_cliente'>
                                        <span style='line-height:18px;margin-left: -15px;margin-right: 15px;' class="label_posvenda">Não Cliente</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: ORIGEM - CANAL DE ATENDIMENTO -->

                    <!-- BEGIN: NOME CLIENTE - PRODUTO -->
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="telefone">Telefone Cliente</label>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group" style="width: 100%">
                                    <input type='text' class="form-control" name='telefone' id="telefone" placeholder="Digite o número do Telefone do Cliente" />
                                    <span class="input-group-addon">
                                        <input type="checkbox" id='check_telefone' name='check_telefone'>
                                        <span style='line-height:18px;margin-left: -15px;margin-right: 15px;' class="label_posvenda">Não Informado</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="email">E-mail Cliente</label>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group" style="width: 100%">
                                    <input type='text' class="form-control" name='email' id="email" placeholder="Digite o E-mail do Cliente" />
                                    <span class="input-group-addon">
                                        <input type="checkbox" id='check_email' name='check_email'>
                                        <span style='line-height:18px;margin-left: -15px;margin-right: 15px;' class="label_posvenda">Não Informado</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="cpf">CPF do Cliente</label>
                            </div>
                            <div class="col-md-12">
                                <div class="input-group" style="width: 100%">
                                    <input type='text' class="form-control" name='cpf' id="cpf" placeholder="Digite o número do CPF" />
                                    <span class="input-group-addon">
                                        <input type="checkbox" id='check_cpf' name='check_cpf'>
                                        <span style='line-height:18px;margin-left: -15px;margin-right: 15px;' class="label_posvenda">Não Informado</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="nome_cliente">Nome do Cliente</label>
                            </div>
                            <div class="col-md-12">
                                <input type='text' class="form-control" name='nome_cliente' id="nome_cliente" placeholder="Digite o nome do cliente" required="required" />
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="operadora">Operadora / Produto</label>
                            </div>
                            <div class="col-md-12">
                                <select class="form-control" name='operadora' id="operadora" required="required">
                                    <option value="">Selecione</option>
                                    @foreach($produtos as $operadoraProduto)
                                        <option value="{{$operadoraProduto->id}}">{{$operadoraProduto->operadora_produto}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="id_atendimento">ID Atendimento</label>
                            </div>
                            <div class="col-md-12">
                                <input type='text' class="form-control" name='id_atendimento' id="id_atendimento" placeholder="Digite o ID do atendimento" />
                            </div>
                        </div>
                    </div>
                    <!-- END: NOME CLIENTE - PRODUTO -->

                    <!-- BEGIN: MOTIVO CONTATO - MOTIVO CONTATO 2 -->
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="motivo1">Motivo de Contato</label>
                            </div>
                            <div class="col-md-12">
                                <select class="form-control" id="motivo1" name='motivo1' required="required">
                                    <option value="" selected>Selecione</option>
                                    @foreach( $motivos as $motivo )
                                        <option value="{{$motivo->id}}">{{$motivo->motivo}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="motivo2">Motivo de Contato 2</label>
                            </div>
                            <div class="col-md-12">
                                <select class="form-control" id="motivo2" name='motivo2' required="required">
                                    
                                </select>
                            </div>
                        </div>
                        
                    </div>
                    <!-- END: MOTIVO CONTATO - MOTIVO CONTATO 2 -->

                    <!-- BEGIN: AÇÃO - ID ATENDIMENTO -->
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="acao">Ação</label>
                            </div>
                            <div class="col-md-12">
                                <select class="form-control" name='canal' id="acao"  required="required">
                                    <option value="">Selecione</option>
                                    @foreach( $acoes as $acao )
                                    <option value="{{$acao->id}}">{{$acao->acao}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="col-md-12">
                                <label class="control-label" for="codCancelamento">Códigos de Cancelamentos</label>
                            </div>
                            <div class="col-md-12">
                                <select class="form-control" id="codCancelamento" name='codCancelamento' disabled="disabled">
                                    <option value="" selected disabled>Selecione</option>
                                    <option value="011.01">011.01 - Desistiu do pedido</option>
                                    <option value="011.02">011.02 - Desconhece o pedido</option>
                                    <option value="011.03">011.03 - Solicitou o plano incorreto</option>
                                    <option value="011.04">011.04 - Solicitou a modalidade incorreta</option>
                                    <option value="011.05">011.05 - Informa que o endereço cadastrado está incorreto</option>
                                    <option value="011.06">011.06 - Solicitará o serviço em outra operadora</option>
                                    <option value="011.07">011.07 - Informa que está fidelizado na operadora atual</option>
                                    <option value="017.01">017.01 - Endereço inválido</option>
                                    <option value="017.02">017.02 - E-mail inválido</option>
                                    <option value="017.03">017.03 - Nome do cliente inválido</option>
                                    <option value="017.04">017.04 - Nome da mãe inválido</option>
                                    <option value="017.05">017.05 - Data de nascimento inválida</option>
                                    <option value="017.06">017.06 - Linha inválida</option>
                                    <option value="017.07">017.07 - Produto inválido</option>
                                    <option value="017.08">017.08 - Serviço inválido</option>
                                    <option value="017.09">017.09 - Dados de pagamento inválidos</option>
                                    <option value="032">032 - Insucesso na entrega</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- END: AÇÃO - ID ATENDIMENTO -->

                    <!-- BEGIN: CHAMADO -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <label class="control-label" for="chamado">Chamado</label>
                            </div>
                            <div class="col-md-12">
                                <textarea class="form-control" name='chamado' id="chamado" placeholder="Digite as informações do chamado"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- END: CHAMADO -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning btn-block" onclick="preSalvamento()">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="modal-history" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-md-6">
                    <h3 id="title-historico" class="modal-title"></h3>
                </div>
                <div class="col-md-6 pull-right">
                    <button type="button" class="close" aria-label="Close"  data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>                
                </div>
            </div>
			<div class="modal-body">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr class="label_history">
                            <td>Usuário</td>
                            <td>Status</td>
                            <td>Comentário</td>
                            <td>Data do Comentário</td>
                        </tr>
                    </thead>
                    <tbody id="resultado_history">
                        
                    </tbody> 
                </table>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Chamado</h4>
			</div>
			<div class="modal-body">
				<textarea class='modal_chamado' style="width:100%;min-height:200px;"></textarea>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalNotificacao" tabindex="-1" role="dialog" aria-labelledby="modalNotificacaoLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4>Avisos</h4>
			</div>
			<div class="modal-body">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr class="label_history">
                            <th>#</th>
                            <th>Usuário</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody id="resultado_history" class="scroll">
                        @foreach( $notificacoes as $note )
                        <?php 
                            $date = new \DateTime($note->data);
                            $date = $date->format('d/m/Y H:i:s');
                        ?>
                        <tr>
                            <td><button id="note-{{$note->id}}" type="button" class="btn btn-primary btn-sm" onclick="abrirNota('{{$note->id}}');"><i class="fa fa-ellipsis-v"></i></button></td>
                            <td class="label_posvenda">{{$note->nome}}</td>
                            <td class="label_posvenda">{{$date}}</td>
                        </tr>
                        <div id="dvnote_{{$note->id}}" class="dvcontainer" style="display:none;">
                            <tr id="trnote_{{$note->id}}" style="display:none" class="dvcontainer">
                                <td colspan="3" style="background-color: #FFFFE0;" class="label_posvenda">  
                                        {!!nl2br($note->mensagem)!!}
                                </td>
                            </tr>
                        </div>
                        @endforeach
                    </tbody> 
                </table>
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"  id="fechar">Fechar</button>
            </div>
		</div>
	</div>
</div>

<!-- END: modals -->
@endsection