@extends('layouts.admin')

@section('title', 'Adicionar Opções')

@push('header.javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        $(function() {
            $("#form").validate({
                rules: {
                    campo: "required",
                    valor: "required"
                }
            });
        });
    </script>
@endpush
@push('header.css')
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' />
    <style type="text/css">
        label.error {
            display: none !important;
        }
        .form-control.error {
            border-color: red !important;
        }
        .w-100 {
            width: 100%;
        }
    </style>
@endpush

@section('content')

@if(Session::has('message'))
<div class="row">
    <div class="col-md-12">
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    </div>
</div>
@endif

@if ($s)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <p><b>Registro salvo com sucesso</b></p>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <form action="{{route('posvenda.salvar_opcoes')}}" class="form-horizontal" enctype="multipart/form-data" method="post" id='form'>
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Campo</label>
                            <select class="form-control" name='campo' id='campo'>
                                <option value="">Selecione</option>
                                <option value="origem">Origem</option>
                                <option value="canal_atendimento">Canal de Atendimento</option>
                                <option value="operadora_produto">Operadora/ Produto</option>
                                <option value="motivo1">Motivo de Contato</option>
                                <option value="motivo2_label">Motivo de Contato 2</option>
                                <option value="acoes">Ação</option>
                            </select>
                        </div>
                        <div class="col-md-8 w-100"></div>
                        <div class="col-md-4 campo_texto" id='texto'>
                            <label class="control-label">Valor</label>
                            <input class="form-control" type="text" id='valor' name='valor' />
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection