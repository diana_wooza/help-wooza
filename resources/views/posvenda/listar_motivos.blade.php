@extends('layouts.admin')

@section('title', 'Administrar Motivos')

@push('header.css')

<style type="text/css">

	.limited-text-content {
		max-width:100%;
		overflow:hidden;
		text-overflow:ellipsis;
		white-space:nowrap;
	}

</style>

@endpush

@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Motivos</h3>
                </div>
                <div class="box-body">
                        
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">Ação</th>
                                <th>Id</th>
                                <th>Motivo</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        @if($dados)
                        <tbody>
                            @foreach($dados as $model)
                                <tr>
                                    <td><a href="{{route('posvenda.administrar_relacoes_motivos')}}/{{ $model->id }}" class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></button></td>
                                    <td>{{ $model->id }}</td>
                                    <td>{{ $model->motivo }}</td>
                                    <td>{{ ($model->status) ? 'Habilitado' : 'Desabilitado' }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
</div>

@endsection