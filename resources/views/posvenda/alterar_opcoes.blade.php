@extends('layouts.admin')

@section('title', 'Alterar Opções')

@push('header.javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        $(function() {
            $("#campo").change(function() {
                if ($(this).val() != '') {
                    var elemento = $(this);
                    $(".campo").fadeOut('normal', function() {
                        $(".campo_texto").fadeOut();
                        $(".btn").fadeOut();
                        $(".status").fadeOut();
                        $("#"+elemento.val()).fadeIn();
                    });
                    
                    $("#campo_selecionado").val(elemento.val());
                }
            });

            $(".select").change(function() {
                $("#valor").val($('option:selected',this).text());
                var valor = $(this).val();

                jQuery.ajax({
                    url: "{{ url('/posvenda/status') }}",
                    method: 'get',
                    data: {
                        campo: $("#campo_selecionado").val(),
                        valor: valor
                    },
                    success: function(result) {
                        $('.select_status').val(result);
                        $("#texto").fadeIn();
                        $(".btn").fadeIn();
                        $(".status").fadeIn();
                    }
                });
            });

            var motivos2 = {!!json_encode($motivos2)!!};
            $("#motivo2_text").autocomplete({
                minLength: 0,
                source: motivos2,
                select: function(event, ui) {
                    $("#valor").val(ui.item.label);
                    $("#motivo2_text").val(ui.item.label);
                    $("#motivo2").val(ui.item.value);

                    jQuery.ajax({
                        url: "{{ url('/posvenda/status') }}",
                        method: 'get',
                        data: {
                            campo: $("#campo_selecionado").val(),
                            valor: ui.item.value
                        },
                        success: function(result) {
                            $('.select_status').val(result);
                            $("#texto").fadeIn();
                            $(".btn").fadeIn();
                            $(".status").fadeIn();
                        }
                    });

                    return false;
                }
            }).bind('focus', function(){ $(this).autocomplete("search");});

            var acoes = {!!json_encode($acoes)!!};
            $("#acoes_text").autocomplete({
                minLength: 0,
                source: acoes,
                select: function(event, ui) {
                    $("#valor").val(ui.item.label);
                    $("#acao").val(ui.item.value);
                    $("#acoes_text").val(ui.item.label);

                    jQuery.ajax({
                        url: "{{ url('/posvenda/status') }}",
                        method: 'get',
                        data: {
                            campo: $("#campo_selecionado").val(),
                            valor: ui.item.value
                        },
                        success: function(result) {
                            $('.select_status').val(result);
                            $("#texto").fadeIn();
                            $(".btn").fadeIn();
                            $(".status").fadeIn();
                        }
                    });

                    return false;
                }
            }).bind('focus', function(){ $(this).autocomplete("search");});
        });
    </script>
@endpush
@push('header.css')
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' />
    <style type="text/css">
        label.error {
            display: none !important;
        }
        .form-control.error {
            border-color: red !important;
        }
        .w-100 {
            width: 100%;
        }
    </style>
@endpush

@section('content')

@if(Session::has('message'))
<div class="row">
    <div class="col-md-12">
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    </div>
</div>
@endif

@if ($s)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <p><b>Registro salvo com sucesso</b></p>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <form action="{{route('posvenda.salvar_alterar_opcoes')}}" class="form-horizontal" enctype="multipart/form-data" method="post" id='form'>
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label class="control-label">Campo</label>
                            <select class="form-control" name='campo' id='campo'>
                                <option value="">Selecione</option>
                                <option value="acoes">Ação</option>
                                <option value="canal_atendimento">Canal de Atendimento</option>
                                <option value="motivo1">Motivo de Contato</option>
                                <option value="motivo2_label">Motivo de Contato 2</option>
                                <option value="operadora_produto">Operadora/ Produto</option>
                                <option value="origem">Origem</option>
                            </select>
                        </div>
                        <div class="col-md-8 w-100"></div>
                        <div class="col-md-4 campo" style="display: none" id='origem'>
                            <label class="control-label">Origem</label>
                            <select class="form-control select" name='origem'>
                                <option value="">Selecione</option>
                                @foreach($origens as $origem)
                                    <option value="{{$origem['id']}}">{{$origem['origem']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-8 w-100"></div>
                        <div class="col-md-4 campo" style="display: none" id='canal_atendimento'>
                            <label class="control-label">Canal de Atendimento</label>
                            <select class="form-control select" name='canal_atendimento'>
                                <option value="">Selecione</option>
                                @foreach($canais as $canal)
                                    <option value="{{$canal['id']}}">{{$canal['canal_atendimento']}}</option>
                                @endforeach
                            </select>           
                        </div>
                        <div class="col-md-8 w-100"></div>
                        <div class="col-md-4 campo" style="display: none" id='operadora_produto'>
                            <label class="control-label">Operadora / Produto</label>
                            <select class="form-control select" name='operadora_produto'>
                                <option value="">Selecione</option>
                                @foreach($operadorasProdutos as $operadoraProduto)
                                    <option value="{{$operadoraProduto['id']}}">{{$operadoraProduto['operadora_produto']}}</option>
                                @endforeach
                            </select>           
                        </div>
                        <div class="col-md-8 w-100"></div>
                        <div class="col-md-4 campo" style="display: none" id='motivo1'>
                            <label class="control-label">Motivo de Contato</label>
                            <select class="form-control select" name='motivo1'>
                                <option value="">Selecione</option>
                                @foreach($motivos1 as $motivo1)
                                    <option value="{{$motivo1['id']}}">{{$motivo1['motivo']}}</option>
                                @endforeach
                            </select>           
                        </div>
                        <div class="col-md-8 w-100"></div>
                        <div class="col-md-4 campo" style="display: none" id='motivo2_label'>
                            <label class="control-label">Motivo de Contato 2</label>
                            <input class="form-control" id='motivo2_text' name='motivo2_text' />
                            <input type="hidden" id="motivo2" name="motivo2">
                        </div>
                        <div class="col-md-8 w-100"></div>
                        <div class="col-md-4 campo" style="display: none" id='acoes'>
                            <label class="control-label">Ação</label>
                            <input class="form-control" id='acoes_text' name='acoes_text' />
                            <input type="hidden" id="acao" name="acao">
                        </div>
                        <div class="col-md-8 w-100"></div>
                        <div class="col-md-4 status" style="display: none" id='status'>
                            <label class="control-label">Status</label>
                            <select class="form-control select_status" name='status'>
                                <option value="">Selecione</option>
                                <option value="1">Habilitado</option>
                                <option value="0">Desabilitado</option>
                            </select>           
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" style="display: none"><i class="fa fa-floppy-o"></i>&nbsp;Salvar</button>
                </div>
                <input type='hidden' name='campo_selecionado' id='campo_selecionado' />
            </form>
        </div>
    </div>
</div>

@endsection