@extends('layouts.admin')
@section('title', 'Alarmista')
@section('subtitle', 'Lista Logs')
@section('content')

@if(!empty($errors->first()))
    @alerta()
        {{ $errors->first() }}
    @endalerta
@endif

@push('header.css')
<style>
  .required:after {
    content:" *"; 
    color: red;
},
</style>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
@endpush
<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <div class="col-xs-8">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Faça sua pesquisa</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="required">Operadora</label>
              <select id="api-select" class="form-control select2" style="width: 100%;">
                <option value= "0" selected>Selecione</option>
                    @foreach($apisSelect['apisSelect'] as $api):
                      <option value= "{{$api->collection}}">{{strtoupper(substr($api->collection, strpos($api->collection, '_') +1))}}</option>
                    @endforeach;
              </select>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
              <label>Api</label>
              <select id='select-api' class="form-control select2" style="width: 100%;">
                  <option value= "0" selected>Selecione</option>
              </select>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
              <label class="required">Data</label>
              <div class='input-group date' >
                  <input type='text' class="form-control" id='dataInicio' placeholder="Inicio"/>
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                  <input type='text' class="form-control" id='dataFim' placeholder="Fim"/>
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
            </div>      
          </div>
          <div class="col-md-6">
              <!-- /.form-group -->
            <div class="form-group">
              <label class="required">Segmento</label>
              <select id="select-segmento" class="form-control select2" style="width: 100%;">
                  <option value= "0" selected>Selecione</option>
              </select>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
              <label>Situação</label>
              <select id="select-situacao" class="form-control select2" style="width: 100%;">
                  <option value= "0" selected>Selecione</option>
                  <option value= "1" >Estável</option>
                  <option value= "2" >Oscilando</option>
                  <option value= "3" >Instável</option>
                </select>
              </div>
              <!-- /.form-group -->
              <div class="box-footer">
                <button type="submit" disabled="disabled" class="btn btn-primary btn-flat pull-right " id="btn-pesquisa-api" onclick="listaLogs.btnBuscaLogApi('listaLogs')">Buscar</button>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
    </div>
  <!-- fechamento da div pesquisa-->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Dados dos Logs</h3>
            </div>
              <form method="post" action="exportarDadosLogs" enctype="multipart/form-data">
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Api</th>
                      <th>Operadora</th>
                      <th>Percentual Apurado</th>
                      <th>Percentual Negativo</th>
                      <th>Percentual Positivo</th>
                      <th>Data/Hora</th>
                    </tr>
                  </thead>
                  <tbody>
                    </tfoot>
                  </table>
                </div>
              <input name="todos_id_log" id='todos_id_log' type="hidden" value="">       
            </form>
          </div>
        </div>
      </div>
    </section>
@endsection
@push('body.javascript')
<!-- Bootstrap 3.3.7 -->
<script src="{{url('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- DataTables -->
<script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{url('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{url('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{url('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{url('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script> 
<script>
  $(function(){
    //Date picker with time picker

                $('#dataInicio').datetimepicker({
                    locale: 'ru',
                    format: 'DD/MM/YYYY HH:mm:ss',
                    defaultDate: new Date()
                });
                $('#dataFim').datetimepicker({
                    locale: 'ru',
                    format: 'DD/MM/YYYY HH:mm:ss',
                    defaultDate: new Date()
                });

        $('#dataInicio').datetimepicker();
        $('#dataFim').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#dataInicio").on("dp.change", function (e) {
            $('#dataFim').data("DateTimePicker").minDate(e.date);
        });
        $("#dataFim").on("dp.change", function (e) {
            $('#dataInicio').data("DateTimePicker").maxDate(e.date);
        });
  })
</script>
<script src="{{asset('js/help_wooza.js')}}"></script>
<script src="{{asset('js/listaLogs.js')}}"></script>
<!-- <script src="{{asset('js/listaApi.js')}}"></script> -->
@endpush