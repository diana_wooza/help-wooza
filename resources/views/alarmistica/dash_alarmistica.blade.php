@extends('layouts.admin')
@section('title', 'Alarmística')
@section('subtitle', 'Painel de controle')
@section('content')
@if(!empty($errors->first()))
		@alerta()
			{{ $errors->first() }}
		@endalerta
  @endif
  @push('body.css')
  <style>
    .carregamento-cartao{
      position: relative;
      text-align: center;
      bottom: 100px;
      color: #fff;
      display: block;
    }
    </style>
  @endpush

 <!-- Content Wrapper. Contains page content -->
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
          <!-- Main content -->
          <section class="content" id="atualizarPagina">
            <!-- Small boxes (Stat box) -->
            <form>
            <div class="row">
              <!-- ./col 1-->
              <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner ajax">
                    <h3 id='estavel'>0<sup style="font-size: 20px"></sup></h3>
                    <p>Estável</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                  </div>
                  <a href="#" id="estavelInfo" class="small-box-footer">Mais info <i class="fa fa-arrow-circle-right"></i>
                    <span hidden id="lg_estavel"></span>
                  </a>
                </div>
                  <div style="display:block" class="carregamento-cartao">
                    <i class="fa fa-spin fa-spinner fa-2x " aria-hidden="true"></i>
                  </div>
              </div>
              <!-- ./col 2-->
              <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow" >
                  <div class="inner">
                    <h3 id='oscilando'>0<sup style="font-size: 20px"></sup></h3>
                    <p>Oscilando</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-exclamation-triangle"></i>
                  </div>
                  <a href="#" id="oscilandoInfo" class="small-box-footer">Mais info <i class="fa fa-arrow-circle-right"></i>
                    <span hidden id="lg_oscilando"></span>
                  </a>
                </div>
                <div style="display:block" class="carregamento-cartao">
                   <i class="fa fa-spin fa-spinner fa-2x " aria-hidden="true"></i>
                </div>
              </div>
              <!-- ./col 3-->
              <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                  <span hidden id="lg_instavel"></span>
                  <div class="inner">
                    <h3 id='instavel'>0<sup style="font-size: 20px"></sup></h3>
                    <p>Instável</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-bug" aria-hidden="true"></i>
                  </div>
                  <a href="#" disabled="disabled" id="instavelInfo" class="small-box-footer">Mais info <i class="fa fa-arrow-circle-right"></i>
                    <span hidden id="lg_instavel"></span>
                  </a>
                </div>
              </div>
              <div style="display:block" class="carregamento-cartao">
                  <i class="fa fa-spin fa-spinner fa-2x " aria-hidden="true"></i>
              </div>
              <!-- ./col -->
            </div>
      <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#line-chart" data-toggle="tab">Area</a></li>
              <li class="pull-left header"><i class="fa fa-inbox"></i> Apis</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="line-chart" style="position: relative; height: 300px;"></div>
              {{-- <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div> --}}
            </div>
          </div>
          <!-- /.nav-tabs-custom -->

        <!-- Inserir mais graficos aqui !! -->
      </form>
          </section>
    <!-- /.content -->
        <div id="retornoApi">

        </div>
  @endsection
  
  @push('body.javascript')
   <script>
     window.onload = function(){
       var form = document.querySelector('form'),
           data = new FormData(form);
           
           console.log(data);
       
     }

   </script> 

	<!-- script alarmistica -->
  <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{url('bower_components/raphael/raphael.min.js')}}"></script> 
  <script src="{{url('bower_components/morris.js/morris.min.js')}}"></script> 
  <script src="{{url('bower_components/chart.js/Chart.js')}}"></script> 
  <script src="{{url('js/dashboardAlarmistica.js')}}"></script>
@endpush