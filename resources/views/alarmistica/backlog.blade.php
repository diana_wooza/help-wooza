@extends('layouts.admin')

@section('title', 'Backlog por Status')
@section('content')

@if(Session::has('message'))
<div class="row">
    <div class="col-md-12">
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <h4>Selecione um arquivo abaixo</h4>
            @if( $result )
                <p>
                    <b>Atendimento registrado com o protocolo: {{$result}}</b>
                </p>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Importar Planilha</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('alarmistica.backlog_send')}}" class="form-horizontal" enctype="multipart/form-data" method="post">
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <label for="planilha" class="col-sm-2 control-label">Planilha de Importação</label>
                        <div class="col-sm-10">
                            <input type="file" id="planilha" name="planilha" accept=".xlsx, .xls, .csv" />
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-arrow-up"></i> &nbsp; Importar Planilha</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>

@endsection