@extends('layouts.admin')
@section('title', 'Alarmista')
@section('subtitle', 'Cadastrar Api')
@section('content')

<!-- erros do formulario, caso não seja preenchido -->
@if(!empty($errors->any()))
	@foreach ($errors->all() as $error)
		@alerta()
			{{ $error }}
		@endalerta
	@endforeach
@endif

@push('header.css')
<style>
  .required:after {
    content: " *";
    color: red;
  }

  ,
</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
  <!-- Select2 -->
  <link rel="stylesheet" href="{{url('bower_components/select2/dist/css/select2.min.css')}}">
@endpush

<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-10">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Cadastre uma api para ser monitorada</h3>
				</div>
				<div class="box-body">
			
					<!-- /.box-header -->
					<form class="form-horizontal was-validated" action="{{route('alarmistica.configuracao.cadastrar')}}" method="post">
						<div class="form-group">
							<label for="inputName" class="col-sm-2 control-label required">Nome</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputNome" name="nome" value="{{old('nome')}}">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail" class="col-sm-2 control-label required">Operadora</label>
							<div class="col-md-6">
								<select id="inputOperadora" class="form-control" name="operadora">
									    <option value="" selected>Selecione...</option>
										<option @if(old('operadora') == "log_claro") selected @endif value="log_claro">Claro</option>
										<option @if(old('operadora') == "log_net") selected @endif value="log_net">Net</option>
										<option @if(old('operadora') == "log_nextel") selected @endif value="log_nextel">Nextel</option>
										<option @if(old('operadora') == "log_oi") selected @endif value="log_oi">Oi</option>
										<option @if(old('operadora') == "log_sky") selected @endif value="log_sky">Sky</option>
										<option @if(old('operadora') == "log_tim") selected @endif value="log_tim">Tim</option>
										<option @if(old('operadora') == "log_vivo") selected @endif value="log_vivo">Vivo</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label required" title="Nome do canal no Mongo DB que salva o log desta api."> Channel Mongo</label>

							<div class="col-md-6">
								<select id="inputChannel" class="form-control select2" name="channel">
									   <option value="" selected>Selecione...</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label required">Segmento</label>

							<div class="col-md-6">
								<select id="inputState" class="form-control" name="segmento">
									<option value="" selected>Selecione...</option>
									@foreach($todosSegmento as $segmento)
										<option @if(old('segmento') == '{{$segmento->id}}') selected @endif value="{{$segmento->id}}">{{$segmento->nm_segmento}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="inputName" class="col-sm-2 control-label required" title="O valor deve ser maior que o negativo">Range positivo</label>

							<div class="col-md-2">
								<div class="input-group">
									<input type="text" class="form-control" name="range-positivo" value="{{old('range-positivo')}}">
									<span class="input-group-addon">%</span>
								</div>
							</div>
							<small>(Ex. 30 equivale a 30%)</small>
						</div>
						<div class="form-group">
							<label for="inputExperience" class="col-sm-2 control-label required" title="O valor de ser menor que o positivo">Range negativo</label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="text" class="form-control" name="range-negativo" value="{{old('range-negativo')}}">
									<span class="input-group-addon">%</span>
								</div>
							</div>
							<small>(Ex. 2 equivale a 2%)</small>
						</div>
						<!-- recebem sms -->
						<div class="form-group">
							<label for="inputSkills" class="col-sm-2 control-label">Quem recebe SMS</label>

							<div class="col-sm-10">
								<button id="pessoas-sms" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPessoasSms" role="dialog" aria-labelledby="modalPessoasSms">
									Pessoas incluídas
									<!-- <h3 id='oscilando'>0<sup style="font-size: 20px"></sup></h3> -->
									<span id="qtd-pessoas" class="badge badge-light">0</span>
								</button>
							</div>
						</div>
						<!-- Modal -->
						<div class="modal fade" id="modalPessoasSms" data-backdrop="static">
							<div class="modal-dialog" role="document">
								<div class="modal-content col-md-12">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h3 class="modal-title">Selecione os usuários</h3>
									</div>
									<div class="modal-body">
										<!-- checkbox -->
										<div class="form-group form-check">
											<!-- <div class=""> -->
												<!-- /.box-header -->
												<!-- <div class="box-body no-padding"> -->
													<table id="example2" class="display" style="width:100%">
														<thead>
															<tr>
																<th style="text-align: center; position:relative">Usuários</th>
															</tr>
														</thead>
														<tbody>
															@foreach($pessoasNaoRecebem as $naoRecebem)
															@if($naoRecebem)
															<tr>
																<td>
																	<input class="form-check-input" type="checkbox" id="{{$loop->index}}" name="modal-pessoas-sms" value="{{$naoRecebem->id}}">
																	{{strtoupper($naoRecebem->nome)}}
																</td>
															</tr>
															@endif
															@endforeach
															<!-- /.box-body -->
															@if(!isset($pessoasNaoRecebem))
															<tr>
																<td class="text-center" colspan="6">Nenhum registro encontrado</td>
															</tr>
															@endif
															</tfoot>
													</table>
													<input type="hidden" id="pessoas" name="pessoas-sms" value="">
												</div>
											</div>
										<!-- </div> -->
									<!-- </div> -->
									<div class="modal-footer">
										<button type="button" id="cancelar-pessoas-sms" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
										<button type="button" id="alterar-pessoas-sms" class="btn btn-primary" data-dismiss="modal">Alterar</button>
									</div>
									<!-- /.modal-content -->
								</div>
							</div>
							<!-- /.modal-dialog -->
						</div>
						<!-- end -->
						<div class="form-group">
							<label for="inputSkills" title="Escolha o horário de funcionamento da api, o intervalo de disparo de SMS e quantos pedidos negativos para disparar." class="col-sm-2 control-label required">Configurações diversas</label>
							<!-- </div> -->
							<div class="col-md-10">
								<div class="col-md-10">
									<div class="form-group form-check">
										<!-- <small >Intervalo que a alarmistica funcionará para esta api, caso seja 24h marque aqui:</small>
                    <input type="checkbox" class="form-check-input" name="24horas" id="exampleCheck1"> -->
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-5">

										<div class="form-group form-check required">
											<small>Inicia as:</small>
										</div>
										<div class='input-group date'>
											<input type='text' class="form-control" id='timepicker1-antigo' name="hora-inicio" value="{{old('hora-inicio')}}" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group form-check required">
											<small>Terrmina as:</small>
										</div>
										<div class='input-group date'>
											<input type='text' class="form-control" id='timepicker2-antigo' name="hora-final" value="{{old('hora-final')}}" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group form-check required">
										<small>Tempo de verificação da api:</small>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-5">
										<select id="inputDisparo" class="form-control" name="tempo-verificacao">
											<option value="">Selecione</option>
											<option @if(old('tempo-verificacao') == "5min") selected @endif value="5min">5 minutos</option>
											<option @if(old('tempo-verificacao') == "30min") selected @endif value="30min">30 minutos</option>
											<option @if(old('tempo-verificacao') == "1h") selected @endif value="1h">1 hora</option>
										</select>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group form-check required" title="Quantidade dos últimos logs com percentual negativo para disparar SMS.">
										<small>Itens alarmantes:</small>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-5">
										<select id="inputDisparo1" class="form-control" name="itens-alarmantes">
											<option value="">Selecione</option>
											<option @if(old('itens-alarmantes') == "1") selected @endif value="1">1</option>
											<option @if(old('itens-alarmantes') == "2") selected @endif value="2">2</option>
											<option @if(old('itens-alarmantes') == "3") selected @endif value="3">3</option>
										</select>
									</div>
								</div>
								<div class="form-group form-check">
									<input type="checkbox" class="form-check-input" id="exampleCheck1" @if(old('status') == 'on') checked @endif  name="status">
									<label class="form-check-label" for="exampleCheck1">Desativar api</label>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<a class="btn btn-default" href="{{route('alarmistica.configuracao.cadastrar')}}" data-dismiss="modal" id="btn-cancel-alter-api">Cancelar</a>
							<button type="button" id="btnCadastrar" class="btn btn-primary" data-toggle="modal" data-target="#bs-example-modal-sm">Cadastrar</button>
						</div>
				</div>
				<div class="modal fade" id="bs-example-modal-sm" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content col-md-8">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Atenção!</h4>
							</div>
							<div class="modal-body">
								<p>Tem certeza que deseja cadastrar a api ?</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								<button type="submit" data-toggle="tooltip" data-placement="left" class="btn btn-danger">Salvar</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				</form>
			</div>
</section>
@endsection
@push('body.javascript')
<!-- Modal -->
<script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!-- DataTables -->
<script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<!-- <script src="{{asset('js/help_wooza.js')}}"></script> -->
<script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{url('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<!-- date-range-picker -->
<script src="{{url('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{url('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{url('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('js/cadastrarApi.js')}}"></script>
@endpush