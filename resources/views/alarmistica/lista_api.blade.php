@extends('layouts.admin')
@section('title', 'Alarmista')
@section('subtitle', 'Lista APIs')
@section('content')

@if(!empty($errors->first()))
		@alerta()
			{{ $errors->first() }}
		@endalerta
@endif

@push('header.css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
@endpush
<!-- Content Wrapper. Contains page content -->
    <!-- Main content -->
<section class="content">
  <div class="col-xs-8">
    @if($inibiCampos == false)
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Faça sua pesquisa</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Operadora</label>
                        <select id="api-select" class="form-control select2">
                          <option value= "0" selected>Selecione</option>
                        @foreach($apisSelect['apisSelect'] as $api):
                          <option value= "{{$api->collection}}">{{strtoupper(substr($api->collection, strpos($api->collection, '_') +1))}}</option>
                        @endforeach;
                        </select> 
                  </div>
                {{-- </div>
                <div class="col-sm-6"> --}}
                  <div class="form-group">
                  <label>Segmento</label>
                      <select id="api-select-filho" class="form-control select2">
                        <option value= "0" selected>Selecione</option>
                      </select> 
                  </div>
                </div>
                {{-- </div> --}}
                <div class="col-sm-6">
                  <div class="form-group">
                  <label>Status</label>
                      <select id="api-status" class="form-control select2">
                        <option value= "0" selected>Selecione</option>
                        <option value= "1">Ativa</option>
                        <option value= "-1">Inativa</option>
                      </select> 
                  </div>
                </div>
                <div class="col-sm-6 form-group">
                  <div class="pull-right">
                    <button type="submit" disabled="disabled" class="btn btn-primary btn-flat" id="btn-pesquisa-api" onclick="listaApi.btnBuscaApi()">Pesquisar</button>
                  </div>
                </div>
            </div>
          </div>
        </div> <!-- fechamento da div pesquisa-->
            @else
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Alerta!</h4>
                As informações de "Percentual Apurado" não é base para disparo de SMS, pois existem outras variáveis como o tempo para disparo (5, 30 min. ou 1h), se a api esta dentro do range de horário etc.
              </div>           
            @endif
        </div> 
    <div class="row">
      <div class="col-xs-12">
        <!-- <body class="hold-transition skin-blue sidebar-mini"> --> 
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Dados das APIs</h3>
          </div>
              <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Api</th>
                  <th>Operadora</th>
                  @if($inibiCampos == true)
                    <!-- <th>Segmento</th> -->
                    <th>Percentual Apurado</th>
                    <th>Percentual Negativo</th> 
                    <th>Percentual Positivo</th> 
                    <th>Data/hora</th> 
                  @else
                    <th>Segmento</th>
                    <th>Descrição</th>
                    <th>Status</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                @forelse($apisGrid as $api)
                <tr>
                <td>{{$api->nome}}</td>
                <td>{{strtoupper(substr($api->collection, strpos($api->collection, '_') +1))}}</td>
                  @if($inibiCampos == true)
                      <td>{{$api->percentual_atual}}%</td>
                      <td>{{$api->range_negativo_api}}</td>
                      <td>{{$api->range_positivo_api}}</td>
                      <td>{{$api->data}}</td>
                  @else 
                        <td>{{$api->segmento}}</td>
                        <td>-</td>
                        <td>{{$api->status == 1 ? 'Ativa' : 'Inativa'}}</td>
                  @endif      
                </tr>
                @empty
                    <tr><td class="text-center" colspan="6">Nenhum registro encontrado</td></tr>
                @endforelse
                </tfoot>
              </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    </div>
    </div>
    </div>
  {{-- </section> --}}
@endsection
@push('body.javascript')
<script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/help_wooza.js')}}"></script>
<script src="{{asset('js/listaApi.js')}}"></script>
@endpush