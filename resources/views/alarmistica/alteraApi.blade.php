@extends('layouts.admin')
@section('title', 'Alarmista')
@section('subtitle', 'Alterar Api')
@section('content')
@if(!empty($errors->first()))
@alerta()
{{ $errors->first() }}
@endalerta
@endif

@push('header.css')
<style>
  .required:after {
    content: " *";
    color: red;
  }

  ,
</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
@endpush
<!-- Content Wrapper. Contains page content -->
<!-- Main content -->
<section class="content">
  <!-- Novo -->
  <form action="{{route('alarmistica.configuracao.editar')}}" method="get">
    <div class="col-xs-8">
      {{csrf_field()}}
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Escolha uma Api</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="required">Operadora</label>
                <select id="api-select" class="form-control select2" style="width: 100%;">
                  <option value="0" selected>Selecione</option>
                  @foreach($apisSelect['apisSelect'] as $apis)
                  <option value="{{$apis->collection}}">{{strtoupper(substr($apis->collection, strpos($apis->collection, '_') +1))}}</option>
                  @endforeach
                </select>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label class="required">Segmento</label>
                <select id="select-segmento" class="form-control select2" style="width: 100%;">
                  <option value="0" selected>Selecione</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <!-- /.form-group -->
              <div class="form-group">
                <label class="required">Api</label>
                <select id='select-api' class="form-control select2" name="api" style="width: 100%;">
                  <option value="0" selected>Selecione</option>
                </select>
              </div>
              <!-- /.form-group -->
              <div class="box-footer ">
                <button type="submit" class="btn btn-primary btn-flat pull-right" id="btn-pesquisa-api">Buscar</button>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
    </div>
  </form>
  <!-- fechamento da div pesquisa-->
  @if(isset($api))
  @foreach($api as $dadosApi)
  <div class="form-row">
    <div class="row">
      <div class="col-xs-10">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Dados da Api</h3>
          </div>
          <div class="box-body">
            <!-- /.box-header -->
            <form class="form-horizontal was-validated" id="form-alterar" action="{{route('alarmistica.configuracao.alterar')}}" method="post">
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nome</label>

                <div class="col-sm-6">
                  <input type="text" readonly class="form-control" id="inputNome" name="nome" value="{{$dadosApi->nome}}">
                  <input type="hidden" readonly class="form-control" id="inputNome" name="id" value="{{$dadosApi->id}}">
                </div>
              </div>
              <input type="hidden" name="operadora" value="{{$dadosApi->collection}}" >
              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Segmento</label>

                <div class="col-md-6">
                  <select id="inputState" class="form-control" name="segmento">
                     <option value="0" selected>Selecione...</option>
                      @foreach($todosSegmento as $segmento)
                      <option value="{{$segmento->id}}" {{($dadosApi->segmento_id == $segmento->id) ? "selected" : ""}}>{{$segmento->nm_segmento}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label" title="Nome do canal no Mongo DB que salva o log desta api."> Channel</label>

                <div class="col-md-6">
                  <select id="inputState" class="form-control" name="channel">
                     <option value="0" selected>Selecione...</option> 
                      @foreach($channel as $canal)
                      {{$canal = substr($canal, 2,-2)}}
                      <option value="{{$canal}}" {{($dadosApi->consulta == $canal) ? "selected" : ""}}>{{$canal}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label" title="O valor deve ser maior que o negativo">Range positivo</label>

                <div class="col-md-2">
                  <div class="input-group">
                    <input type="text" class="form-control" name="range-positivo" value="{{$dadosApi->range_positivo_api}}">
                    <span class="input-group-addon">%</span>
                  </div>
                </div>
                <small>(Ex. 30 equivale a 30%)</small>
              </div>
              <div class="form-group">
                <label for="inputExperience" class="col-sm-2 control-label" title="O valor deve ser menor que o positivo">Range negativo</label>
                <div class="col-md-2">
                  <div class="input-group">
                    <input type="text" class="form-control" name="range-negativo" value="{{$dadosApi->range_negativo_api}}">
                    <span class="input-group-addon">%</span>
                  </div>
                </div>
                <small>(Ex. 2 equivale a 2%)</small>
              </div>
              <!-- recebem sms -->
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">Quem recebe SMS</label>

                <div class="col-sm-10">
                  <button id="pessoas-sms" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPessoasSms" role="dialog" aria-labelledby="modalPessoasSms">
                    Pessoas incluídas
                    <!-- <h3 id='oscilando'>0<sup style="font-size: 20px"></sup></h3> -->
                    <span id="qtd-pessoas" class="badge badge-light">0</span>
                  </button>
                </div>
              </div>
              <!-- Modal -->
              <div class="modal fade" id="modalPessoasSms" data-backdrop="static">
                <div class="modal-dialog" role="document">
                  <div class="modal-content col-md-12">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <h3 class="modal-title">Selecione os usuários</h3>
                    </div>
                    <div class="modal-body">
                      <!-- checkbox -->
                      <div class="form-group form-check">
                        <!-- <div class=""> -->
                          <!-- /.box-header -->
                          <!-- <div class="box-body no-padding"> -->
                            <table id="example2" class="display" style="width:100%">
                              <thead>
                                <tr>
                                  <th style="text-align: center; position:relative">Usuários</th>
                                </tr>
                              </thead>
                              <tbody>
                                @if($pessoasRecebem)
                                  @foreach($pessoasRecebem as $recebem)
                                    <tr>
                                      <td>
                                        <input class="form-check-input" type="checkbox" checked id="{{$loop->index}}" name="modal-pessoas-sms" value="{{$recebem->id}}">
                                        {{strtoupper($recebem->nome)}}
                                      </td>
                                    </tr>
                                  @endforeach
                                @endif
                                @if($pessoasNaoRecebem)
                                  @foreach($pessoasNaoRecebem as $naoRecebem)
                                    <tr>
                                      <td>
                                        <input class="form-check-input" type="checkbox" id="{{$loop->index}}" name="modal-pessoas-sms" value="{{$naoRecebem->id}}">
                                        {{strtoupper($naoRecebem->nome)}}
                                      </td>
                                    </tr>
                                  @endforeach
                                @endif
                                <!-- /.box-body -->
                                @if(!isset($pessoasRecebem) && !isset($pessoasNaoRecebem))
                                <tr>
                                  <td class="text-center" colspan="6">Nenhum registro encontrado</td>
                                </tr>
                                @endif
                                </tfoot>
                            </table>
                            <input type="hidden" id="pessoas" name="pessoas-sms" value="">
                          <!-- </div> -->
                        </div>
                      <!-- </div> -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="cancelar-pessoas-sms" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                      <button type="button" id="alterar-pessoas-sms" class="btn btn-primary" data-dismiss="modal">Alterar</button>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- end -->
              <div class="form-group">
                <label for="inputSkills" title="Escolha o horário de funcionamento da api, o intervalo de disparo de SMS e quantos pedidos negativos para disparar." class="col-sm-2 control-label">Configurações diversas</label>
                <!-- </div> -->
                <div class="col-md-10">
                  <div class="col-md-10">
                    <div class="form-group form-check">
                      <!-- <small >Intervalo que a alarmistica funcionará para esta api, caso seja 24h marque aqui:</small>
                    <input type="checkbox" class="form-check-input" name="24horas" id="exampleCheck1"> -->
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-5">
                      @if($horarioDisparoSms)
                      <div class="form-group form-check">
                        <small>Inicia as:</small>
                      </div>
                      <div class='input-group date'>
                        <input type='text' class="form-control" id='timepicker1-antigo' name="hora-inicio" value="{{$horarioDisparoSms['de']}}" />
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div class="form-group form-check">
                        <small>Terrmina as:</small>
                      </div>
                      <div class='input-group date'>
                        <input type='text' class="form-control" id='timepicker2-antigo' name="hora-final" value="{{$horarioDisparoSms['ate']}}" />
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                    @else
                    <div class='input-group date' id='timepicker1-novo'>
                      <input type='text' class="form-control" name="hora-inicio" value="" />
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                    <div class="col-md-5">
                      <div class='input-group date' id='timepicker2-novo'>
                        <input type='text' class="form-control" name="hora-final" value="10:15:00" />
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  @endif
                </div>
                <div class="col-md-8">
                  <div class="form-group form-check">
                    <small for="tempo-verificacao">Tempo de verificação da api:</small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-5">
                    <select id="inputDisparo" required class="form-control" name="tempo-verificacao">
                      <option value="" {{$consultaTempoSchedule == '' ? 'selected' : '' }}>Selecione</option>
                      <option value="5min" {{$consultaTempoSchedule != '' && $consultaTempoSchedule == '5min' ? 'selected =selected' : '' }}>5 minutos</option>
                      <option value="30min" {{$consultaTempoSchedule != '' && $consultaTempoSchedule == '30min' ? 'selected =selected' : '' }}>30 minutos</option>
                      <option value="1h" {{$consultaTempoSchedule != '' && $consultaTempoSchedule == '1h' ? 'selected =selected' : '' }}>1 hora</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="form-group form-check" title="Quantidade dos últimos logs com percentual negativo para disparar SMS.">
                    <small>Itens alarmantes:</small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-5">
                    <select id="inputDisparo1" required name="itens-alarmantes" class="form-control">
                      <option value="" {{$consultaItensAlarmantes == '' ? 'selected' : '' }}>Selecione</option>
                      <option value="1" {{$consultaItensAlarmantes != '' && $consultaItensAlarmantes == 1 ? 'selected =selected' : '' }}>1</option>
                      <option value="2" {{$consultaItensAlarmantes != '' && $consultaItensAlarmantes == 2 ? 'selected =selected' : '' }}>2</option>
                      <option value="3" {{$consultaItensAlarmantes != '' && $consultaItensAlarmantes == 3 ? 'selected =selected' : '' }}>3</option>
                    </select>
                  </div>
                </div>
                <div class="form-group form-check">
                  @if($dadosApi->status == -1)
                  <input type="checkbox" checked class="form-check-input" id="exampleCheck1" name="status">
                  @else
                  <input type="checkbox" class="form-check-input" id="exampleCheck2" name="status">
                  @endif
                  <label class="form-check-label" for="exampleCheck1">Desativar api</label>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <a class="btn btn-default" href="{{route('alarmistica.configuracao.ajustar')}}" data-dismiss="modal" id="btn-cancel-alter-api">Cancelar</a>
            <button type="" id="bs-example-modal-sm" class="btn btn-primary" data-toggle="modal" >Alterar</button>
          </div>
        </div>

        <div class="modal fade" id="modal-alterar" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content col-md-8">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Atenção!</h4>
              </div>
              <div class="modal-body">
                <p>Tem certeza que deseja alterar a api {{$dadosApi->nome}} ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" data-toggle="tooltip" id="envia-formulario" data-placement="left" class="btn btn-danger">Salvar alterações</button>
              </div>
            </div>
        
          </div>
        
        </div>
        
        </form>
      </div>
    </div>
  </div>
  @endforeach
  @endif
  </div>
</section>
@endsection

@push('body.javascript')
<!-- Modal -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!-- DataTables -->
<script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{url('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<!-- date-range-picker -->
<script src="{{url('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{url('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{url('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script src="{{asset('js/alteraApi.js')}}"></script>

<!-- <script src="{{asset('js/listaApi.js')}}"></script> -->
@endpush