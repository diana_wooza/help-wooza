<?php use \App\Http\Controllers\Reaproveitamento\ReaproveitamentoController; ?>
@extends('layouts.admin')

@section('title', 'Agendar Reaproveitamento')

@push('header.javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>
    <script src="{{url('js/reaproveitamento/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{url('js/reaproveitamento/jquery.bootstrap-touchspin.js')}}"></script>
    <script>
        $(function() {
            $('#data_cancelamento').daterangepicker({ 
                autoUpdateInput: false,
                opens: "right",
                locale: { 
                    format: 'DD/MM/YYYY',
                    cancelLabel: 'Limpar',
                    applyLabel: 'Aplicar',
                    daysOfWeek: [
                        "Dom",
                        "Seg",
                        "Ter",
                        "Qua",
                        "Qui",
                        "Sex",
                        "Sáb"
                    ],
                    monthNames: [
                        "Janeiro",
                        "Fevereiro",
                        "Março",
                        "Abril",
                        "Maio",
                        "Junho",
                        "Julho",
                        "Agosto",
                        "Setembro",
                        "Outubro",
                        "Novembro",
                        "Dezembro"
                    ]
                }
            });

            $('#data_cancelamento').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });

            $('#data_cancelamento').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $('#data_inicio').daterangepicker({ 
                autoUpdateInput: false,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                opens: "right",
                locale: { 
                    format: 'DD/MM/YYYY',
                    cancelLabel: 'Limpar',
                    applyLabel: 'Aplicar',
                    daysOfWeek: [
                        "Dom",
                        "Seg",
                        "Ter",
                        "Qua",
                        "Qui",
                        "Sex",
                        "Sáb"
                    ],
                    monthNames: [
                        "Janeiro",
                        "Fevereiro",
                        "Março",
                        "Abril",
                        "Maio",
                        "Junho",
                        "Julho",
                        "Agosto",
                        "Setembro",
                        "Outubro",
                        "Novembro",
                        "Dezembro"
                    ]
                }
            });

            $('#data_inicio').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm:ss'));
            });

            $('#data_inicio').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $('#form').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) { 
                    e.preventDefault();
                    return false;
                }
            });

            $("#form").validate({
                rules: {
                    data_cancelamento: "required",
                    codigos_cancelamento: "required",
                    operadoras: "required",
                    modalidades: "required",
                    tipos: "required",
                    'segmentos[]': "required",
                    data_inicio: "required",
                    recorrencia: "required"
                }
            });

            
            var input_cancelamentos = $("#codigos_cancelamento").tagsinput('input');
            var cancelamentos = ['{!!$cancelamentos!!}'];
            input_cancelamentos.autocomplete({
                minLength: 1,
                source: cancelamentos,
                select: function(event, ui) {
                    $("#codigos_cancelamento").tagsinput('add', ui.item.value);
                    input_cancelamentos.val('');
                    
                    return false;
                }
            });
            $('#codigos_cancelamento').on('beforeItemAdd', function(event) {
                if (!validar(cancelamentos, event.item)) {
                    event.cancel = true;
                }
            });

            var input_operadoras = $("#operadoras").tagsinput('input');
            var operadoras = ['{!!$operadoras!!}'];
            input_operadoras.autocomplete({
                minLength: 1,
                source: operadoras,
                select: function(event, ui) {
                    $("#operadoras").tagsinput('add', ui.item.value);
                    input_operadoras.val('');
                    
                    return false;
                }
            });
            $('#operadoras').on('beforeItemAdd', function(event) {
                if (!validar(operadoras, event.item)) {
                    event.cancel = true;
                }
            });

            var input_modalidades = $("#modalidades").tagsinput('input');
            var modalidades = ['{!!$modalidades!!}'];
            input_modalidades.autocomplete({
                minLength: 1,
                source: modalidades,
                select: function(event, ui) {
                    $("#modalidades").tagsinput('add', ui.item.value);
                    input_modalidades.val('');
                    
                    return false;
                }
            });
            $('#modalidades').on('beforeItemAdd', function(event) {
                if (!validar(modalidades, event.item)) {
                    event.cancel = true;
                }
            });

            var input_tipos = $("#tipos").tagsinput('input');
            var tipos = ['{!!$tipos!!}'];
            input_tipos.autocomplete({
                minLength: 1,
                source: tipos,
                select: function(event, ui) {
                    $("#tipos").tagsinput('add', ui.item.value);
                    input_tipos.val('');
                    
                    return false;
                }
            });
            $('#tipos').on('beforeItemAdd', function(event) {
                if (!validar(tipos, event.item)) {
                    event.cancel = true;
                }
            });

            $("#recorrencia").change(function() {
                $("#data_cancelamento").val('');
                $("#periodo_cancelamento").val('');

                switch($(this).val()) {
                    case '':
                        $("#selecionar_datas").fadeOut();
                        $("#selecionar_periodo").fadeOut();
                    break;
                    case 'Uma Vez':
                        $("#selecionar_periodo").fadeOut('normal', function() {
                            $("#selecionar_datas").fadeIn();
                        });
                    break;
                    default:
                        $("#selecionar_datas").fadeOut('normal', function() {
                            $("#selecionar_periodo").fadeIn();
                        });
                    break;
                }
            });

            $("#periodo_cancelamento").TouchSpin({
                verticalbuttons: true
            });
        });

        function validar(array, item) {
            var validacao = false;
            $.each(array, function(i, v) {
                if (item == v) {
                    validacao = true;
                }
            });
            
            return validacao;
        }
    </script>
@endpush
@push('header.css')
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' />
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css' />
    <link rel='stylesheet' href="{{url('css/reaproveitamento/jquery.bootstrap-touchspin.css')}}" />
    <style type="text/css">
        label.error {
            display: none !important;
        }
        .form-control.error {
            border-color: red !important;
        }
        .form_row {
            margin-left: 0;
        }
        .label_lista {
            display: block;
            text-align: left !important;
        }
        p {
            margin-bottom: 2px;
        }
        .row_lista {
            margin-bottom: 5px;
        }
        .btn-danger {
            margin-top: 5px;
        }

        .bootstrap-tagsinput input[type="text"] {
            border-radius: 0;
            box-shadow: none;
            border-color: #d2d6de;
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            /* border-radius: 4px; */
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        }
    </style>
@endpush

@section('content')

@if($mensagem)
<div class="row">
    <div class="col-md-12">
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{$mensagem}}</p>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <form action="{{route('reaproveitamento.salvar_agendamento')}}" class="form-horizontal" enctype="multipart/form-data" method="post" id='form'>
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <div class="row form_row">
                            <div class="col-sm-2">
                                <label class="control-label">Códigos de cancelamento</label>
                                <input type='text' class="form-control" data-role="tagsinput" name='codigos_cancelamento' id='codigos_cancelamento' />
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Operadoras</label>
                                <input type='text' class="form-control" data-role="tagsinput" name='operadoras' id='operadoras' />
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Modalidades</label>
                                <input type='text' class="form-control" data-role="tagsinput" name='modalidades' id='modalidades' />
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Tipos de plano</label>
                                <input type='text' class="form-control" data-role="tagsinput" name='tipos' id='tipos' />
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Segmentos</label>
                                <br>
                                <select multiple name='segmentos[]' style="width:70%">
                                    <option value='consumer'>Consumer</option>
                                    <option value='varejo'>Varejo</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Formas de pagamento excluídas</label>
                                <select multiple name='pagamentos[]' style="width:70%;height:120px">
                                    <option value='1'>Fatura Digital</option>
                                    <option value='2'>Conta Pelos Correios</option>
                                    <option value='3'>Debito Automático</option>
                                    <option value='4'>Conta Online</option>
                                    <option value='5'>Cartão de Crédito</option>
                                    <option value='6'>Boleto</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Recorrência</label>
                                <select class="form-control" name='recorrencia' id='recorrencia'>
                                    <option value="">Selecione</option>
                                    <option value="Uma Vez">Uma Vez</option>
                                    <option value="Diária">Diária</option>
                                    <option value="Semanal">Semanal</option>
                                    <option value="Mensal">Mensal</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label">Data de início</label>
                                <input type='text' class="form-control" autocomplete="off" name='data_inicio' id='data_inicio' />
                            </div>
                            <div class="col-sm-2">
                                <span id='selecionar_datas' style="display:none">
                                    <label class="control-label">Data de cancelamento</label>
                                    <input type='text' class="form-control" autocomplete="off" name='data_cancelamento' id='data_cancelamento' />
                                </span>
                                <span id='selecionar_periodo' style="display:none">
                                    <label class="control-label">Dias a partir da data de início</label>
                                    <input type='text' class="form-control" name='periodo_cancelamento' id='periodo_cancelamento' />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@if (count($agendamentos) > 0)
<form action="{{route('reaproveitamento.excluir_agendamento')}}" class="form-horizontal" enctype="multipart/form-data" method="post" id='form'>
@csrf
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                @foreach($agendamentos as $agendamento)
                    <div class="row row_lista">
                        <div class="col-sm-2">
                            <label class="control-label label_lista">Data de início</label>
                            {{ReaproveitamentoController::formatarData($agendamento->data_inicio)}}
                        </div>
                        <div class="col-sm-4">
                            <label class="control-label label_lista">Dados</label>
                            @foreach(ReaproveitamentoController::formatarDados($agendamento->dados) as $dado)
                                <p>{{$dado}}</p>
                            @endforeach
                        </div>
                        <div class="col-sm-1">
                            <label class="control-label label_lista">Recorrência</label>
                            {{$agendamento->recorrencia}}
                        </div>
                        <div class="col-sm-1">
                            <button type="submit" name='submit' value='excluir_{{$agendamento->id}}' class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Excluir</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</form>
@endif

@endsection