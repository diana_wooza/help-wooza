@extends('layouts.admin')

@section('title', 'Lista de Crons')

@push('header.javascript')
<script>
    $(function() {
        $(".exibir_crons").click(function() {
            if ($(this).hasClass('fa-expand')) {
                $(this).parent().parent().parent().next().slideDown();
            }
            else {
                $(this).parent().parent().parent().next().slideUp();
            }
            $(this).toggleClass('fa-expand fa-compress');
        });

        $(".ativar").click(function() {
            var btn = $(this);
            var linha = $(this).parent().parent().parent();
            $.ajax({
                type: 'GET',
                url: '../cron/ativar/'+$(this).val(),
                dataType: 'json',
                success: function (data) {
                    linha.toggleClass('inativo');
                    btn.toggleClass('btn-success btn-danger');
                    if (btn.hasClass('btn-success')) {
                        btn.html(' Ativar');
                    }
                    else {
                        btn.html(' Desativar');
                    }
                },
                error: function (request, status, error) {
                    alert('Erro ao atualizar');
                }
            });
        });
    });
    </script>
@endpush

@push('header.css')
    <link rel="stylesheet" href="{{url('css/cron/lista_cron.css')}}">
@endpush

@section('content')

<div class="row pull-right" style='margin-bottom:5px;margin-top:-40px'>
    <div class="col-md-12">
        <form method='post'>
            <a class="btn btn-primary" href="{{route('cron.editar')}}"><i class="fa fa-plus"></i> &nbsp; Adicionar Cron</a>
            <button type='submit' class="btn btn-primary" action="{{route('cron.lista')}}" value='enviar'><i class="fa fa-upload"></i> &nbsp; Atualizar Crons no Magento
        </form>
    </div>
</div>

@isset ($resultado)
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-{{(strstr($resultado, 'sucesso')) ? 'success' : 'danger'}}">
                <p><b>{{$resultado}}</b></p>
            </div>
        </div>
    </div>
@endisset

<div class="row">
    @foreach ($lista as $categoria => $crons)
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="callout callout-info">
                        <b>{{(!empty($categoria)) ? strtoupper($categoria) : 'GERAL'}}</b>
                        <i class='fa fa-expand exibir_crons' aria-hidden='true'></i>&nbsp;
                    </div>
                </div>
            </div>
            <div class="box box-success" style='display:none'>
                <table class='table table-striped table-bordered table-hover table-sm'>
                    <thead>
                        <tr>
                            <th style='width:300px;'>Nome</th>
                            <th style='width:80px;'>Hora</th>
                            <th style='width:80px;'>Minuto</th>
                            <th style='width:80px;'>Dia do mês</th>
                            <th style='width:80px;'>Mês</th>
                            <th style='width:80px;'>Dia da semana</th>
                            <th style='width:50px;'>Timeout</th>
                            <th style='width:200px;'>Descrição</th>
                            <th style='width:190px;'>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($crons as $cron)
                            <tr{{(!($cron->status)) ? ' class=inativo' : ''}}>
                                <td>{{$cron->nome}}</td>
                                <td>{{$format->formatarExibicaoAgendamento($cron->hora, true)}}</td>
                                <td>{{$format->formatarExibicaoAgendamento($cron->minuto)}}</td>
                                <td>{{$format->formatarExibicaoAgendamento($cron->dia_mes)}}</td>
                                <td>{{$format->formatarExibicaoAgendamento($cron->mes)}}</td>
                                <td>{{$format->formatarExibicaoAgendamento($cron->dia_semana)}}</td>
                                <td>{{$cron->timeout}}</td>
                                <td>{{$cron->descricao}}</td>
                                <td class='ativo'>
                                    <div class="btn-group">
                                        <a href="{{route('cron.editar', ['id' => $cron->id])}}" class="btn btn-primary fa fa-edit" title="Editar"> Editar</a>
                                        @if ($cron->status)
                                            <button class="btn btn-danger fa fa-ban ativar" title="Desativar" value='{{$cron->id}}'> Desativar</button>
                                        @else
                                            <button class="btn btn-success fa fa-ban ativar" title="Ativar" value='{{$cron->id}}'> Ativar</button>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach
</div>

@endsection