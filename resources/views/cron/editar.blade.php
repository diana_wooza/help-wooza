@extends('layouts.admin')

@section('title', ($cron->id != null) ? 'Gerenciar Cron: '.$cron->nome :  'Novo cron')

@push('header.javascript')
<script>
    $(function() {
        $("#submit_excluir").click(function() {
            if (confirm('Excluir cron?')) {
                $(form).submit();
            }

            return false;
        });

        $(".radio_tipo").on('ifChanged', function(event){
            exibir($(this).val(), $(this).attr('name').replace("tipo_", ''));
        });

        exibir('{{(strstr($cron->hora, '/')) ? 'intervalo' : 'fixo'}}', 'hora');
        exibir('{{(strstr($cron->minuto, '/')) ? 'intervalo' : 'fixo'}}', 'minuto');
    });

    function exibir(exibir, campo) {
        var esconder = 'intervalo';
        if (exibir == 'intervalo') {
            esconder = 'fixo';
        }
        
        $(".select_"+campo+"_"+esconder).fadeOut('normal', function() {
            $(".select_"+campo+"_"+exibir).fadeIn();
        });

        $(':radio[name=tipo_'+campo+'][value='+exibir+']').iCheck('check');
    }
</script>
@endpush
@push('header.css')
    <link rel="stylesheet" href="{{url('css/cron/cron.css')}}">
@endpush
@section('content')

@if ($salvo)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-success">
            <p><b>Salvo com sucesso</b></p>
        </div>
    </div>
</div>
@endif

<button onclick="window.location='{{route('cron.lista')}}'" class="fa fa-arrow-left btn btn-primary" value='voltar'> Voltar para lista</button>    
<br>
<br>
<form class="row" method="post" action="{{route('cron.editar', ['id' => $cron->id])}}">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
            <div class="box-body">
                <div class="form-group">
                    <label for="nome" class="control-label">Nome</label>
                    <input type="text" class="form-control" name="nome" value="{{$cron->nome}}"/>
                </div>
                <div class='row'>
                    <div class='col-sm-3'>
                        <label class='control-label'>Hora</label>
                        <div class='well well-sm'>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default">
                                    <input type="radio" name="tipo_hora" class='radio_tipo' value="intervalo" />Intervalo
                                </label> 
                                <label class="btn btn-default">
                                    <input type="radio" name="tipo_hora" class='radio_tipo' value="fixo" />Fixo
                                </label>
                            </div>
                            <span class='select_hora_intervalo' style='display: none'>
                                <div class="row row_intervalo">
                                    <div class="col-sm-3 cada">A cada</div>
                                    <div class="col-sm-3 text_intervalo">
                                        <input type="text" class="form-control input-sm intervalo" name="hora_intervalo" value="{{str_replace('*/', '', $cron->hora)}}"/>
                                    </div>
                                    <div class="col-sm-4 descricao_intervalo">Horas</div>
                                </div>
                            </span>
                            <span class='select_hora_fixo' style='display: none'><br>
                                <select class="form-control input-sm selectpicker" name="hora_fixo[]" data-action-box="1" data-none-selected-text="Nenhuma hora selecionada" multiple>
                                    <option value="*" {{(in_array('*', explode(',', $cron->hora))) ? 'selected' : ''}}>Todas</option>
                                    @for ($i=0;$i<=23;$i++)
                                        <option value="{{$i}}" {{(in_array((string) $i, explode(',', $cron->hora), true)) ? 'selected' : ''}}>{{$i}}:00</option>
                                    @endfor
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <label for="minuto" class="control-label">Minuto</label>
                        <div class="well well-sm">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default">
                                    <input type="radio" name="tipo_minuto" class='radio_tipo' value="intervalo" />Intervalo
                                </label> 
                                <label class="btn btn-default">
                                    <input type="radio" name="tipo_minuto" class='radio_tipo' value="fixo" />Fixo
                                </label>
                            </div>
                            <span class='select_minuto_intervalo' style='display: none'>
                                <div class="row row_intervalo">
                                    <div class="col-sm-3 text-center cada">A cada</div>
                                    <div class="col-sm-3 text_intervalo">
                                        <input type="text" class="form-control input-sm intervalo" name="minuto_intervalo" value="{{str_replace('*/', '', $cron->minuto)}}"/>
                                    </div>
                                    <div class="col-sm-4 text-center descricao_intervalo">Minutos</div>
                                </div>
                            </span>
                            <span class='select_minuto_fixo' style='display: none'>
                                <select class="form-control input-sm selectpicker" name="minuto_fixo[]" data-action-box="1" data-none-selected-text="Nenhum minuto selecionado" multiple>
                                    <option value="*" {{(in_array('*', explode(',', $cron->minuto))) ? 'selected' : ''}}>Todos</option>
                                    @for ($i=0;$i<=59;$i++)
                                        <option value="{{$i}}" {{(in_array((string) $i, explode(',', $cron->minuto), true)) ? 'selected' : ''}}>{{$i}}</option>
                                    @endfor
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-sm-3'>
                        <label for="dia_mes" class="control-label">Dia da semana</label>
                        <div class="well well-sm">
                            <span class='select_dia_semana_intervalo'>
                                <div class="row row_intervalo">
                                    <div class="col-sm-12">
                                        <select class="form-control input-sm selectpicker" name="dia_semana[]" data-action-box="1" data-none-selected-text="Nenhum dia selecionado" multiple>
                                            <option value="*" {{(in_array('*', explode(',', $cron->dia_semana))) ? 'selected' : ''}}>Todos</option>
                                            <option value="0" {{(in_array('0', explode(',', $cron->dia_semana))) ? 'selected' : ''}}>Domingo</option>
                                            <option value="1" {{(in_array('1', explode(',', $cron->dia_semana))) ? 'selected' : ''}}>Segunda</option>
                                            <option value="2" {{(in_array('2', explode(',', $cron->dia_semana))) ? 'selected' : ''}}>Terça</option>
                                            <option value="3" {{(in_array('3', explode(',', $cron->dia_semana))) ? 'selected' : ''}}>Quarta</option>
                                            <option value="4" {{(in_array('4', explode(',', $cron->dia_semana))) ? 'selected' : ''}}>Quinta</option>
                                            <option value="5" {{(in_array('5', explode(',', $cron->dia_semana))) ? 'selected' : ''}}>Sexta</option>
                                            <option value="6" {{(in_array('6', explode(',', $cron->dia_semana))) ? 'selected' : ''}}>Sábado</option>
                                        </select>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <label for="dia_mes" class="control-label">Dia do mês</label>
                        <div class="well well-sm">
                            <span class='select_dia_mes_intervalo'>
                                <div class="row row_intervalo">
                                    <div class="col-sm-12">
                                        <select class="form-control input-sm selectpicker" name="dia_mes[]" data-action-box="1" data-none-selected-text="Nenhum dia selecionado" multiple>
                                        <option value="*" {{(in_array('*', explode(',', $cron->dia_mes))) ? 'selected' : ''}}>Todos</option>
                                        @for ($i=0;$i<=31;$i++)
                                            <option value="{{$i}}" {{(in_array((string) $i, explode(',', $cron->dia_mes), true)) ? 'selected' : ''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <label for="dia_mes" class="control-label">Mês</label>
                        <div class="well well-sm">
                            <span class='select_mes_intervalo'>
                                <div class="row row_intervalo">
                                    <div class="col-sm-12">
                                    <select class="form-control input-sm selectpicker" name="mes[]" data-action-box="1" data-none-selected-text="Nenhum mês selecionado" multiple>
                                        <option value="*" {{(in_array('*', explode(',', $cron->mes))) ? 'selected' : ''}}>Todos</option>
                                        @for ($i=1;$i<=12;$i++)
                                            <option value="{{$i}}" {{(in_array((string) $i, explode(',', $cron->mes), true)) ? 'selected' : ''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="comando" class="control-label">Comando (Caminho completo para o script)</label>
                    <input type="text" class="form-control" name="comando" value="{{$cron->comando}}"/>
                </div>
                <div class="form-group">
                    <label for="timeout" class="control-label">Timeout (Segundos)</label>
                    <input type="text" class="form-control" name="timeout" value="{{$cron->timeout}}"/>
                </div>
                <div class="form-group">
                    <label for="parametro_adicional" class="control-label">Parâmetro Adicional</label>
                    <input type="text" class="form-control" name="parametro_adicional" value="{{$cron->parametro_adicional}}"/>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label">Descrição</label>
                    <input type="text" class="form-control" name="descricao" value="{{$cron->descricao}}"/>
                </div>
                <div class="form-group">
                    <label for="categoria" class="control-label">Categoria</label>
                    <input type="text" class="form-control" name="categoria" value="{{$cron->categoria}}"/>
                </div>
                <div class="form-group">
                    <label for="status" class="control-label">Status</label>
                    <select class="form-control" name="status">
                        <option value="1" {{($cron->status == 0) ? 'selected' : ''}}>Ativo</option>
                        <option value="0" {{($cron->status == 0) ? 'selected' : ''}}>Inativo</option>
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success" name='submit' value='salvar'>Salvar</button>
                        @if ($cron->id != null)
                        <button type="submit" class="btn btn-danger" name='submit' id='submit_excluir' value='excluir'>Excluir</button>
                        @endif
                    </div>
                </div>
            </div>
		</div>
	</div>
	
</form>

@endsection