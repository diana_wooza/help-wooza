<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>[{{config('app.name')}}] {{config('app.company')}} &rsaquo; @yield('title')</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="icon" href="{{asset('img/icones/hexagono.ico')}}" type="image/x-icon"/>
	<link rel="shortcut icon" href="{{asset('img/icones/hexagono.ico')}}" type="image/x-icon"/>
	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{url('bower_components/font-awesome/css/font-awesome.min.css')}}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{url('bower_components/Ionicons/css/ionicons.min.css')}}">
	<!-- DataTables -->
	<!-- <link rel="stylesheet" href="{{url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}"> -->
	<!-- Morris chart -->
	<!-- <link rel="stylesheet" href="{{url('bower_components/morris.js/morris.css')}}"> -->
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{url('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
	<!-- bootstrap wysihtml5 - text editor -->
	<!-- <link rel="stylesheet" href="{{url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}"> -->
	<!-- Theme style -->
	<link rel="stylesheet" href="{{url('css/AdminLTE.min.css')}}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{url('css/skins/_all-skins.min.css')}}">

	@stack('header.css')

	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="{{url('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
	<!-- jQuery 3 -->
	<!-- <script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script> -->
	<!-- jQuery UI 1.11.4 -->
	<script src="{{url('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script> $.widget.bridge('uibutton', $.ui.button);</script>
	<!-- Morris.js charts -->
    <!-- <script src="{{url('bower_components/raphael/raphael.min.js')}}"></script>
    <script src="{{url('bower_components/morris.js/morris.min.js')}}"></script> -->
    <!-- Sparkline -->
    <!-- <script src="{{url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script> -->
    <!-- daterangepicker -->
    <!-- <script src="{{url('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script> -->
    <!-- datepicker -->
    <!-- <script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script> -->
    <!-- Bootstrap WYSIHTML5 -->
    <!-- <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script> -->
    <!-- DataTables -->
    <!-- <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script> -->
    <!-- SlimScroll -->
    <script src="{{url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{url('bower_components/fastclick/lib/fastclick.js')}}"></script>
	
	<!-- <script src="{{url('bower_components/chart.js/Chart.js')}}"></script> -->
    <!-- AdminLTE App -->
    <script src="{{url('js/adminlte.min.js')}}"></script>
    <!-- <script src="{{url('js/demo.js')}}"></script> -->
	<!-- Alarmistica JS -->

	@stack('header.javascript')

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<!-- BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green       						  |
|               | skin-wooza							  |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------| -->
<body class="skin-wooza hold-transition fixed sidebar-mini">
	<div class="loader" style="display: none;">
		<center>
			<img class="loading-image" src="{{url('spinner.gif')}}" alt="carregando..." width="100px">
		</center>
	</div>
	@stack('body.css')
	
	@hasSection('layout')
		@yield('layout')
	@else
		@yield('content')
	@endif	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
	<!-- iCheck -->
	<script src="{{url('plugins/iCheck/icheck.min.js')}}"></script>
	<script src="{{url('js/help_wooza.js')}}"></script>
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' /* optional */
			});
		});
	</script>
	
	@stack('body.javascript')

</body>
</html>