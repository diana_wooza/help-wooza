@extends('layouts.base')

@push('header.css')

<style type="text/css">

@media print {    
	.no-print, .no-print *
	{
		display: none !important;
	}
	.no-border-print, .no-border-print * {
		border:0px !important;
	}

	a[href]:after {
		content: none !important;
	}
}

.sidebar-menu {}
.sidebar-menu .header {
	font-size: 16px !important;
	color: #fff !important;
	padding: 5px 10px 2px 5px !important;
	overflow: hidden !important;
	width: 100% !important;
	text-overflow: ellipsis !important;
	/* border:1px solid rgba(255,255,255,0.1) !important; */
	background: rgba(196,216,45, 0.8) !important;
	margin-top: 20px;
}
.sidebar-menu .header .main-menu-icon {}

</style>

@endpush

@push('body.javascript')

	<script src="{{asset('js/adminlte.min.js')}}"></script>

@endpush

@section('layout')

	<div class="wrapper">
		
		<header class="main-header">
			<a href="{{route('home')}}" class="logo">
				<span class="logo-mini"><img src="{{asset('img/icones/hexagono.png')}}" style="height:28px; margin-top:-4px; vertical-align:middle;" title="{{config('app.company')}} {{config('app.name')}}" /></span>
				<span class="logo-lg">
					<img src="{{asset('img/icones/hexagono.png')}}" style="height:28px; margin-top:-4px; vertical-align:middle;" />
					{{config('app.name')}} <strong>{{config('app.company')}}</strong>
				</span>
			</a>
			<nav class="navbar navbar-static-top " role="navigation">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="{{asset(Auth::user()->avatar)}}" class="user-image" alt="User Image" />
								<span class="hidden-xs">{{mb_strtoupper(trans(Auth::user()->nome))}}</span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header">
									<img src="{{asset(Auth::user()->avatar)}}" class="img-circle" alt="User Image">
									<p title="{{strip_tags(Auth::user()->getNomesPerfis())}}">
										{{mb_strtoupper(trans(Auth::user()->nome))}}
										<small style="overflow: hidden;max-height: 40px;max-width: 100%;text-overflow: ellipsis;">{!! Auth::user()->getNomesPerfis() !!}</small>
									</p>
								</li>
								<!--li class="user-body">
									<div class="row">
										<div class="col-xs-4 text-center"><a href="#">Followers</a></div>
										<div class="col-xs-4 text-center"><a href="#">Sales</a></div>
										<div class="col-xs-4 text-center"><a href="#">Friends</a></div>
									</div>
								</li-->
								<li class="user-footer">
									<div class="pull-left"><a href="{{url('usuario/editar/' . Auth::user()->id)}}" class="btn btn-default btn-flat">Profile</a></div>
									<div class="pull-right"><a href="{{url('sair')}}" class="btn btn-default btn-flat">Sign out</a></div>
								</li>
							</ul>
						</li>
					
					</ul>
				</div>
			</nav>
		</header>

		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<section class="sidebar">

				<!-- User Panel: style can be found in sidebar.less -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="{{asset(Auth::user()->avatar)}}" class="img-circle" alt="User Image" />
					</div>
					<div class="pull-left info" style="max-width:calc(100% - 65px);">
						<p>{{mb_strtoupper(trans(Auth::user()->nome))}}</p>
						<small style="max-width: 100%;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;width: 100%;display: block;" title="{{strip_tags(Auth::user()->getNomesPerfis())}}">{!! Auth::user()->getNomesPerfis() !!}</small>
					</div>
				</div>

				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">

					@foreach (Menu::get() as $menu)

						@include('components.menu_grupo', ['menu'=>$menu])

					@endforeach

				</ul>

			</section>
		</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header" style="height:39px;">
			<h1>
				@yield('title')
				@hasSection('subtitle')
					<small>@yield('subtitle')</small>
				@endif
			</h1>

			<ol class="breadcrumb" style="margin-top:-7px;">
				@if (isset($actions) && is_array($actions))

					<div class="pull-right btn-group">
						@foreach ($actions as $action)
							<a class="btn btn-primary" href="{{$action['url']}}" onClick="{{@$action['onclick']}}" {{isset($action['blank']) && $action['blank'] ? 'target="_blank"' : ''}}>{!!$action['rotulo']!!}</a>
						@endforeach
					</div>
				@endif
		</ol>
		</section>

		<section class="content">

			@if(Session::has('message'))
				<div class="callout callout-success">
					<p>{!! Session::get('message') !!}</p>
				</div>
			@endif

			@if($errors->any())
				<p>Error</p>
				@foreach ($errors->all() as $error)
					<div class="callout callout-danger">
						<p>{!! $error !!}</p>
					</div>
				@endforeach
			@endif

			@yield('content')

		</section>
	</div>

	<footer class="main-footer no-print">
		<div class="pull-right hidden-xs">
		<b>Version</b> {{config('app.version')}}
		</div>
		<strong>Copyright &copy; 2008-{{date('Y')}} <a href="https://www.wooza.com.br">Wooza</a>.</strong> All rights
		reserved.
	</footer>

	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Create the tabs -->
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
		<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
		<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
		<!-- Home tab content -->
		<div class="tab-pane" id="control-sidebar-home-tab">
			<h3 class="control-sidebar-heading">Recent Activity</h3>
			<ul class="control-sidebar-menu">
			<li>
				<a href="javascript:void(0)">
				<i class="menu-icon fa fa-birthday-cake bg-red"></i>

				<div class="menu-info">
					<h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

					<p>Will be 23 on April 24th</p>
				</div>
				</a>
			</li>
			<li>
				<a href="javascript:void(0)">
				<i class="menu-icon fa fa-user bg-yellow"></i>

				<div class="menu-info">
					<h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

					<p>New phone +1(800)555-1234</p>
				</div>
				</a>
			</li>
			<li>
				<a href="javascript:void(0)">
				<i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

				<div class="menu-info">
					<h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

					<p>nora@example.com</p>
				</div>
				</a>
			</li>
			<li>
				<a href="javascript:void(0)">
				<i class="menu-icon fa fa-file-code-o bg-green"></i>

				<div class="menu-info">
					<h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

					<p>Execution time 5 seconds</p>
				</div>
				</a>
			</li>
			</ul>
			<!-- /.control-sidebar-menu -->

			<h3 class="control-sidebar-heading">Tasks Progress</h3>
			<ul class="control-sidebar-menu">
			<li>
				<a href="javascript:void(0)">
				<h4 class="control-sidebar-subheading">
					Custom Template Design
					<span class="label label-danger pull-right">70%</span>
				</h4>

				<div class="progress progress-xxs">
					<div class="progress-bar progress-bar-danger" style="width: 70%"></div>
				</div>
				</a>
			</li>
			<li>
				<a href="javascript:void(0)">
				<h4 class="control-sidebar-subheading">
					Update Resume
					<span class="label label-success pull-right">95%</span>
				</h4>

				<div class="progress progress-xxs">
					<div class="progress-bar progress-bar-success" style="width: 95%"></div>
				</div>
				</a>
			</li>
			<li>
				<a href="javascript:void(0)">
				<h4 class="control-sidebar-subheading">
					Laravel Integration
					<span class="label label-warning pull-right">50%</span>
				</h4>

				<div class="progress progress-xxs">
					<div class="progress-bar progress-bar-warning" style="width: 50%"></div>
				</div>
				</a>
			</li>
			<li>
				<a href="javascript:void(0)">
				<h4 class="control-sidebar-subheading">
					Back End Framework
					<span class="label label-primary pull-right">68%</span>
				</h4>

				<div class="progress progress-xxs">
					<div class="progress-bar progress-bar-primary" style="width: 68%"></div>
				</div>
				</a>
			</li>
			</ul>
			<!-- /.control-sidebar-menu -->

		</div>
		<!-- /.tab-pane -->

		<!-- Settings tab content -->
		<div class="tab-pane" id="control-sidebar-settings-tab">
			<form method="post">
			<h3 class="control-sidebar-heading">General Settings</h3>

			<div class="form-group">
				<label class="control-sidebar-subheading">
				Report panel usage
				<input type="checkbox" class="pull-right" checked>
				</label>

				<p>
				Some information about this general settings option
				</p>
			</div>
			<!-- /.form-group -->

			<div class="form-group">
				<label class="control-sidebar-subheading">
				Allow mail redirect
				<input type="checkbox" class="pull-right" checked>
				</label>

				<p>
				Other sets of options are available
				</p>
			</div>
			<!-- /.form-group -->

			<div class="form-group">
				<label class="control-sidebar-subheading">
				Expose author name in posts
				<input type="checkbox" class="pull-right" checked>
				</label>

				<p>
				Allow the user to show his name in blog posts
				</p>
			</div>
			<!-- /.form-group -->

			<h3 class="control-sidebar-heading">Chat Settings</h3>

			<div class="form-group">
				<label class="control-sidebar-subheading">
				Show me as online
				<input type="checkbox" class="pull-right" checked>
				</label>
			</div>
			<!-- /.form-group -->

			<div class="form-group">
				<label class="control-sidebar-subheading">
				Turn off notifications
				<input type="checkbox" class="pull-right">
				</label>
			</div>
			<!-- /.form-group -->

			<div class="form-group">
				<label class="control-sidebar-subheading">
				Delete chat history
				<a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
				</label>
			</div>
			<!-- /.form-group -->
			</form>
		</div>
		<!-- /.tab-pane -->
		</div>
	</aside>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
		immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>

	</div>
	
@endsection