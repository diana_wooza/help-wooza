<div class="col-sm-12 col-md-5 col-lg-5">
	<div class="box box-warning direct-chat direct-chat-warning">
		<div class="box-header with-border" style="display:block; overflow-x:hidden; text-overflow:ellipsis; width:100%; white-space:nowrap;" title="Busca Nota na Colmeia">
			<h3 class="box-title"><i class="fa fa-search"></i> Busca Nota na Colmeia</h3>
		</div>
		<div class="box-body">
			<div class="row no-margin">
				<div class="col-md-12">
					@include('colmeia.components.form_busca_nota')
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-sm-12 col-md-7 col-lg-3">
	<div class="box box-warning direct-chat direct-chat-warning">
		<div class="box-header with-border" style="display:block; overflow-x:hidden; text-overflow:ellipsis; width:100%; white-space:nowrap;" title="Alocações de Chip Pendentes">
			<h3 class="box-title"><i class="fa fa-download"></i> Alocações de Chip Pendentes</h3>
		</div>
		<div class="box-body">
			<br />
			<div class="row no-margin">
				<div class="col-md-12">
					@include('colmeia.components.lista_alocacao_pendente')
				</div>
			</div>
		</div>
		<br />
	</div>
</div>

<div class="col-sm-12 col-md-7 col-lg-4">
	<div class="box box-warning direct-chat direct-chat-warning">
		<div class="box-header with-border" style="display:block; overflow-x:hidden; text-overflow:ellipsis; width:100%; white-space:nowrap;" title="Notas Fiscais com Pendência de item">
			<h3 class="box-title"><i class="fa fa-sticky-note"></i> Notas Fiscais com Pendência de item</h3>
		</div>
		<div class="box-body">
			<br />
			<div class="row no-margin">
				<div class="col-md-12">
					@include('colmeia.components.lista_nfe_pendente')
				</div>
			</div>
		</div>
	</div>
</div>