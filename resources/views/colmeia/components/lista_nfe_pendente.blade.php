<ul class="todo-list">

	@forelse (App\Models\Colmeia\Nfe::getNotasComProdutosPendentes() as $nota)

		<li class="list-group-item no-padding text-left">
			<a href="{{route('colmeia.nfe.listardetalhe', ['id' => $nota->id])}}" class="no-margin no-padding" style="line-height:46px; display:block; padding-left:10px !important; padding-right:60px !important; overflow-x:hidden; text-overflow:ellipsis; width:100%;">
				<span class="btn btn-default glyphicon glyphicon-arrow-right pull-right" style="margin-top:5px; margin-right:-50px;" aria-hidden="true"></span>
				<strong>Nota</strong>: {{@$nota->id}} / {{@$nota->nota->NFe->infNFe->emit->xNome}}
			</a>
		</li>

	@empty

		<li class="light">Nenhuma nota pendente. <i class="fa fa-smile-o"></i></li>

	@endforelse
</ul>
<br />