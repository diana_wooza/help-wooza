@extends('layouts.admin')

@section('title', 'Alocar Produto na Colméia')
@section('content')

<div class="row">
	<form class="col-md-12 form-horizontal check-item-count" method="post" action="{{route('colmeia.produto.chip.alocar')}}">
		@csrf
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Preencha os serias dos chips que serão alocados na colmeia</h3>
			</div>
			<br />
			
			<div class="box-body" id="criacao-serial">

				@for ($i = 0; $i < $limite; $i++)

					<div class="col-lg-2 col-md-3 col-sm-4" style="padding:5px;">
						<label class="progress-group text-center" style="border:1px solid rgba(0,0,0,0.5); border-radius:4px; padding:5px;  background:rgba(0,0,0,0.05); display:block;">
							<span class="progress-text">Serial {{$i + 1}}</span>
							<input type="text" name="serial[{{$i}}]" class="form-control move-to-next text-center" value="{{old('serial.' . $i, @$seriais[$i])}}" maxlength="20" />
						</label>
					</div>

				@endfor
			
			</div>
			
			<div class="box-footer">
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-info pull-right disable-when-clicked">Salvar Todas as Alterações</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

@endsection

@push('body.javascript')

<script type="text/javascript">

var timeout;

$('.check-item-count').on('submit', function () {

	var atLeastOne = false;

	$('.move-to-next').each(function () {

		if ($(this).val() != '')
			atLeastOne = true;

	});

	if (!atLeastOne)
	{
		alert('Você precisa preencher pelo menos um item para conseguir efetuar a alocação');
		return false;
	}

	$('.disable-when-clicked').attr('disabled', true);

	return true;

});

$('.move-to-next').on('keyup', function () {

	var length = $(this).val().length;

	if (length != 19 && length != 20)
		return;

	var element = this;

	clearTimeout(timeout);
	timeout = setTimeout(function () {

		var index = $('.move-to-next').index(element);

		if (index >= {{$limite - 1}})
			return;

		$('.move-to-next').eq(index + 1).focus();

	}, 500);
	
});

$('.move-to-next').on('focus', function () {
	$(this).removeClass('bg-red');
});

var check = function () {
	
	var element = this;
	var value = $(this).val();

	if (value.length != 19 && value.length != 20)
		return;

	$.get('{{route('colmeia.api.v2.podealocar', ['id'=>''])}}/' + value, function (result) {

		if (!result || !result.serial || result.serial != value || result.status === true)
			return;

		$(element).addClass('bg-red');

	});
};

$('.move-to-next').on('blur', check);
$('.move-to-next').on('keydown', function(event) {

	if(event.keyCode == 13)
	{
		event.preventDefault();
		return false;
	}
});
$('.move-to-next').each(check);

</script>

@endpush