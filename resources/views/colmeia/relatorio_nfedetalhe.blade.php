<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-body table-responsive no-padding">

				<table class="table table-hover">
					<thead>
						<tr>
							<th class="" style="vertical-align:middle; text-align:center;">SKU</th>
							<th class="" style="vertical-align:middle; text-align:center;">Produto</th>
							<th class="" style="vertical-align:middle; text-align:right;">Quantidade</th>
							<th class="" style="vertical-align:middle; text-align:right;">Valor Unitário</th>
						</tr>
					</thead>
					<tbody>

						@forelse (is_array($nfe->detalhes) ? $nfe->detalhes : [] as $detalhe)
						
							<tr>

								<td class="" style="vertical-align:middle;">{!! !is_null($detalhe->produto) ? $detalhe->produto->sku : '<i class="text-gray">não informado</i>' !!}</td>
								<td class="" style="vertical-align:middle;">{!! !is_null($detalhe->produto) ? $detalhe->produto->name : '<i class="text-gray">não informado</i>' !!}</td>
								<td class="" style="vertical-align:middle;text-align:right;">{{number_format($detalhe->prod->qCom, 0, ',', '.')}}</td>
								<td class="" style="vertical-align:middle;text-align:right;">{{number_format($detalhe->prod->vUnCom, 2, ',', '.')}}</td>
							
							</tr>
					
						@empty

							<tr><td class="jq-mensagem-erro campo-relatorio-vazio text-center" colspan="6"><i><small>Nenhum registro foi encontrado</small></i></td></tr>

						@endforelse
					
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>