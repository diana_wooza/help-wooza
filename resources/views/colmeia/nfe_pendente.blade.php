@extends('layouts.admin')

@section('title', 'Nota Fiscal Pendente')
@section('content')

<div class="row">
	<div class="col-md-12">
		@include('colmeia.components.lista_nfe_pendente')
	</div>
</div>

@endsection