@extends('layouts.admin')

@section('title', 'Gerenciar Armários da Colmeia')
@section('content')

<form class="row" method="post" action="{{route('colmeia.armario.editar', ['id' => $armario->id])}}">
	<div class="col-md-12">
		@csrf
		<input id="opcao-gaveta" type="hidden" name="gaveta_tipo" value="{{old('gaveta_tipo', 'automatico')}}" checked />
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">{!! $armario->exists ? 'Dados dos Armário: <strong>' . $armario->nome . '</strong> [ID ' . $armario->id . ']': 'Novo Armário'!!}</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<div class="form-horizontal">
				<div class="box-body">
					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Status</label>
						<div class="col-sm-10 form-group">
							<select name="status" class="form-control" required>
								<option value="" selected>Escolha um</option>
								@foreach ($statuses as $id => $estado)
									<option value="{{$id}}" {{$id == old('status', $armario->status) ? 'selected' : ''}}>{{$estado}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">Nome</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="nome" value="{{old('nome', $armario->nome)}}"/>
						</div>
					</div>

					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">Prioridade de Alocação</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" name="prioridade" value="{{old('prioridade', $armario->prioridade)}}" />
							<p class="help-block">Quanto maior for o número da prioridade de um armário, mais na frente na fila ele vai ficar para ser escolhido em novas alocações</p>
						</div>
					</div>

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Alocar Chips Sobressalentes</label>
						<div class="col-sm-10">
							<select name="sobressalente" class="form-control" required>
								<option value="" selected>Escolha um</option>
								<option value="1" {{old('sobressalente', $armario->sobressalente) ? 'selected' : ''}}>Sim</option>
								<option value="0" {{!old('sobressalente', $armario->sobressalente) ? 'selected' : ''}}>Não</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="operadora" class="col-sm-2 control-label">Operadora</label>
						<div class="col-sm-10">
							<select name="operadora" class="form-control">
								<option value="" selected>Escolha uma operadora</option>
								@foreach ($operadoras as $id => $operadora)
									<option value="{{$id}}" {{$id == old('operadora', $armario->operadora) ? 'selected' : ''}}>{{$operadora}}</option>
								@endforeach
							</select>
						</div>
					</div>
				
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<a class="btn btn-default" href="{{route('colmeia.armario.lista')}}">Cancelar {{$armario->exists ? 'Edição' : 'Adição'}}</a>
							<button type="submit" class="btn btn-info pull-right">Salvar Alterações</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 nav-tabs-custom no-padding" style="margin:35px 15px 20px; width:calc(100% - 30px);">
		<div class="box box-info no-margin">
			<div class="header with-border">
				<ul class="nav nav-tabs pull-right ui-sortable-handle" style="margin-right:-2px; margin-top:-55px;">
					<li class="{{old('gaveta_tipo', 'automatico') == 'manual' ? 'active' : ''}}"><a href="#revenue-chart" data-toggle="tab" aria-expanded="false" onClick="$('#opcao-gaveta').val('manual')">Gavetas Personalizadas</a></li>
					<li class="{{old('gaveta_tipo', 'automatico') == 'automatico' ? 'active' : ''}}"><a href="#sales-chart" data-toggle="tab" aria-expanded="true" onClick="$('#opcao-gaveta').val('automatico')">Gavetas Automáricas</a></li>
				</ul>
				<h3 class="box-title margin"><i class="fa fa-th"></i> Gavetas</h3>
			</div>
			<div class="box-body tab-content no-margin">
				<div class="chart tab-pane {{old('gaveta_tipo', 'automatico') == 'manual' ? 'active' : ''}}" id="revenue-chart" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
					<p align="center"><i>O sistema de gavetas personalizadas não está funcional ainda</i></p>
				</div>
				<div class="chart tab-pane form-horizontal {{old('gaveta_tipo', 'automatico') == 'automatico' ? 'active' : ''}}" id="sales-chart" style="position: relative;">
					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">Quantidade de Gavetas</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" name="gavetas_automatico_gavetas" value="{{old('gavetas_automatico_gavetas', 72)}}"/>
						</div>
					</div>
					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">Quantidade de Posições Por Gaveta</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" name="gavetas_automatico_posicoes" value="{{old('gavetas_automatico_posicoes', 100)}}"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

@if ($armario->exists)

<div class="row">
	<div class="col-md-12">
		<div class="box box-warning">
			<div class="box-body">

				<div class="box-header with-border">
					<h3 class="box-title">Ocupação por Gaveta</h3>
				</div>

				<div class="row" style="padding:0px 10px;">

					@foreach ($armario->ocupacaogavetas as $gaveta)

						<div class="progress-group col-md-3">
							<span class="progress-text">Gaveta {{$gaveta->nome}}</span>
							<span class="progress-number"><b>{{$gaveta->posicoes - $gaveta->ocupacao}}</b>/{{$gaveta->posicoes}}</span>

							<div class="progress progress-striped active sm">
								<div class="progress-bar progress-bar-{{$ocupationBarColor($gaveta->ocupacao / $gaveta->posicoes)}}" style="width: {{$gaveta->ocupacao / $gaveta->posicoes * 100}}%"></div>
							</div>
						</div>

					@endforeach

				</div>

			</div>
		</div>
	</div>
</div>

@endif

@endsection

@push('header.css')

<style type="text/css">

	input.invisible {
		visibility:hidden;
		position:absolute;
		opacity:0;
	}
	.nav-tabs>li>label>a {
		position: relative;
		display: block;
		padding: 10px 15px;
		margin-right: 2px;
		line-height: 1.42857143;
		border: 1px solid transparent;
		border-radius: 4px 4px 0 0;
		font-weight:400;
	}
	.nav-tabs>li.active>label>a,
	.nav-tabs>li.active>label>a:focus,
	.nav-tabs>li.active>label>a:hover {
		color: #555;
		cursor: default;
		background-color: #fff;
		border: 1px solid #ddd;
		border-bottom-color: transparent;
		font-weight:700;
	}

</style>

@endpush