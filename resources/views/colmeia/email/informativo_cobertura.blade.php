<h3>Tabela de SKU que estão com taxa de cobertura baixa</h3>
<table  border="1">
	<thead>
		<tr>
			<td style="text-align:center;padding:10px;font-weight:bold">SKU</td>
			<td style="text-align:center;padding:10px;font-weight:bold">Estoque Atual</td>
			<td style="text-align:center;padding:10px;font-weight:bold" colspan="2">Saídas no Último Mês</td>
		</tr>
	</thead>
	<tbody>

		@forelse ($criticos as $critico)

			<tr>
				<td style="text-align:center; padding:5px 10px;">{{$critico->sku}}</td>
				<td style="text-align:right; padding:5px 10px;">{{$critico->estoque}}</td>
				<td style="text-align:right; padding:5px 10px;">{{$critico->saida}}</td>
				<td style="text-align:right; padding:5px 10px;">{{number_formar($critico->saida / $critico->estoque * 100, 0, ',', '.')}}% do Estoque</td>
			</tr>

		@empty

			<tr><td colspan="4"><i>Nenhum item foi adicionado. Entre em contato com o administrador</i></td></tr>

		@endforelse

	</tbody>
</table>

