@extends('layouts.admin')
@section('title', 'Entrada em Lote de Seriais')
@push('header.javascript')
	<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
	<script src="{{asset('plugins/vue-color-master/dist/vue-color.min.js')}}"></script>
	<script src="{{asset('js/iccid.js')}}"></script>
@endpush
@push('body.javascript')

<script type="text/javascript">

var entradaLotePage = new Vue({
	el: '#pagina-entrada-lote',
	data: {
		produtos: @json($produtos),
		entradas: @json(array_map(function ($item) {return json_decode($item); }, old('entradas', []))),
		length: 20
	},
	methods: {
		addEntrada: function () {
			
			this.entradas.push({
				produto:null,
				inicial:'',
				final:'',
			});

			return false;
		
		},
		deleteEntrada: function (entrada)
		{
			var index = this.entradas.indexOf(entrada);

			if (index < 0)
				return false;

			this.entradas.splice(index, 1);

			return false;
		},
		preventInput: function(event) {

			event.preventDefault();
			return false;
		},
		toNext: function (object, index, nextItem)
		{
			if (!this.validarIccid(object, index))
				return;

			document.getElementById(nextItem).focus();
		},
		validarIccid: function (object, index)
		{
			if (object[index].length < this.length)
				return;

			var value = object[index];

			if (Iccid.verify(value))
				return true;

			object[index] = '';
			alert('O ICCID ' + value + ' é inválido');

			return false;
		}
	},
	mounted: function ()
	{
		if (this.entradas.length <= 0)
			this.addEntrada();
	}
})

</script>

@endpush
@section('content')

<div id="pagina-entrada-lote" class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Entrada em Lote para ICCIDs</h3>
			</div>
			<form role="form" action="" method="post">
				@csrf
				<input type="hidden" v-for="entrada in entradas" name="entradas[]" v-bind:value="JSON.stringify(entrada)" />
				<div class="box-body">
					<div class="row">
						<p v-if="entradas.length <= 0" class="text-center text-gray">Nenhuma entrada disponível. Clique no botão <a href="javascript:entradaLotePage.addEntrada();">Adicionar Entrada em Lote</a> para continuar</p>
						<div v-for="(entrada, index) in entradas" class="col-md-6">
							<div class="box box-solid bg-light-blue-gradient">
								<div class="box-header ui-sortable-handle">
									<i class="fa fa-barcode"></i>
									<h3 class="box-title">ICCIDs <strong>@{{index + 1}}</strong></h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn bg-red btn-sm" v-on:click="deleteEntrada(entrada)"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<div class="box-body no-padding">
									<div class="col-md-12">
										<div class="form-group">
											<label for="exampleInputEmail1">Produto (SKU)</label>
											<select class="form-control" v-model="entrada.produto">
												<option v-bind:value="null" disabled>Escolha um</option>
												<option v-for="produto in produtos" v-bind:value="produto">@{{produto.name}} (@{{produto.sku}})</option>
											</select>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>ICCID Inicial</label>
											<input v-bind:id="'iccid-inicial-' + index" type="text" class="form-control" v-on:keydown.enter="preventInput" v-on:keyup="toNext(entrada, 'inicial', 'iccid-final-' + index)" v-bind:maxlength="length" placeholder="ICCID inicial" v-model="entrada.inicial">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>ICCID Final</label>
											<input v-bind:id="'iccid-final-' + index" type="text" class="form-control" v-on:keydown.enter="preventInput" v-on:keyup="validarIccid(entrada, 'final')" v-bind:maxlength="length" placeholder="ICCID Final" v-model="entrada.final">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Confirmar Entrada em lote</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endSection