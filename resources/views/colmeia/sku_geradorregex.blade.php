@extends('layouts.admin')

@section('title', 'Gerador de Expressão Regular para SKU')
@section('content')


<pre style="text-align:center;">{!! $regex !!}</pre>
<br /><br />

<form class="box box-info" action="" method="get">
	<div class="form-horizontal">
		<div class="box-body">
			<div class="form-group">
				<label for="status" class="col-sm-2 control-label">Modelo Possíveis</label>
				<div class="col-sm-10">
					<select name="modelo[]" class="form-control selectpicker" multiple>
						@foreach ($modelos as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="status" class="col-sm-2 control-label">Banda</label>
				<div class="col-sm-10">
					<select name="banda[]" class="form-control selectpicker" multiple>
						@foreach ($bandas as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="status" class="col-sm-2 control-label">Operadoras</label>
				<div class="col-sm-10">
					<select name="operadora[]" class="form-control selectpicker" multiple>
						@foreach ($operadoras as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="status" class="col-sm-2 control-label">Estados Selecionados</label>
				<div class="col-sm-10">
					<select name="estado[]" class="form-control selectpicker" multiple>
						@foreach ($estados as $key => $value)
							<option value="{{$key}}">{{$value}}</option>
						@endforeach
					</select>
				</div>
			</div>

		</div>
		<div class="box-footer">
			<div class="row">
				<div class="col-md-10 col-md-offset-2">
					<button type="submit" class="btn btn-info pull-right">Gerar Expressão Regular</button>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection