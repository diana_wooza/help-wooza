@extends('layouts.admin')

@section('title', 'Confirmar Alocação na Colmeia')
@section('subtitle')
	Alocação {{$alocacao->tag}} - {{date('d \d\e M \à\s H:i', strtotime($alocacao->data))}}
@endsection
@section('content')

<div class="row">
	<form class="col-md-12 form-horizontal" method="post" action="{{route('colmeia.produto.alocacao', ['alocacao' => $alocacao->id])}}">
		@csrf
		<input id="hidden-data" type="hidden" name="validacao" value="" />
		<div class="box box-danger">
			<div class="box-body row">
				<div class="col-sm-12 col-md-3 col-lg-4">
					<label for="validar-serial">Preencha com o serial:</label>
					<div class="input-group input-group-lg" style="width:100%;">
						<input id="validar-serial" type="text" class="form-control" style="width:calc(100% - 10px);" maxlength="21">
					</div>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-3">
					<label for="validar-serial">Serial Selecionado</label>
					<div class="input-group input-group-lg" style="width:100%;">
						<input id="serial-digitado" type="text" class="form-control" style="width:calc(100% - 10px);" disabled>
					</div>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-5">
					<div class="btn-group pull-right" style="margin-top:25px;">
						<button type="button" id="cancelar-serial" class="btn btn-primary btn-lg" title="Cancela o serial atualmente digitado">Cancelar</button>
						<a class="btn btn-success btn-lg pull-right" href="{{route('colmeia.produto.etiquetas', ['id' => $alocacao->id])}}" target="_blank"><i class="fa fa-barcode"></i> &nbsp; Imprimir Etiquetas</a>
					</div>
				</div>
			</div>
		</div>
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Seriais a serem alocados</h3>
			</div>
			<br />
			<div class="box-body row no-margin">

				@foreach ($alocacao->alocacoes as $alocacao)

					<div class="col-md-2" style="padding:5px;">
						<label id="box-serial-{{$alocacao->produto->serial}}" class="progress-group text-center {{false ? 'bg-red' : ''}}" style="border:1px solid rgba(0,0,0,0.5); border-radius:4px; padding:5px;  background:rgba(0,0,0,0.05); display:block;">
							<span class="progress-text">Serial {{$loop->iteration}}</span>
							<input type="text" name="serial[{{$alocacao->produto->id}}]" class="form-control move-to-next text-center" value="{{old('serial.' . $alocacao->produto->id, $alocacao->produto->serial)}}" {{!empty($alocacao->produto->serial) ? 'disabled' : ''}} />
						</label>
					</div>

				@endforeach
				
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-12">
						<a class="btn btn-default" href="{{route('home')}}">Cancelar Alocação</a>
						<button id="enviar-seriais" type="submit" class="btn btn-info pull-right" disabled title="Você precisa confirmar todos os seriais para pode alocar estes chips">Confirmar Alocação de Chips</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

@endsection

@push('body.javascript')

<script type="text/javascript">

	var seriais = @json($seriais);
	var serial = null;

	$('#validar-serial').on('keyup', function () {

		if (serial != null)
		{
			if ($(this).val() != serial + 'w')
				return;

			seriais.splice(seriais.indexOf(serial), 1);
			$('#box-serial-' + serial).addClass('bg-green');
			serial = null;
			$('#serial-digitado').val('');
			$('#validar-serial').val('').focus();

			if (seriais.length <= 0)
			{
				$('#hidden-data').val('passou');
				$('#validar-serial').attr('disabled', true);
				$('#enviar-seriais').attr('disabled', false);
			}
		}

		if (seriais.indexOf($(this).val()) < 0)
			return;

		serial = $(this).val();

		$(this).val('');
		$('#serial-digitado').val(serial);
		$(this).focus();

	});

	$('#cancelar-serial').on('click', function () {

		serial = null;
		$('#serial-digitado').val('');
		$('#validar-serial').val('').focus();

	});

	$('#validar-serial').val('').focus();

</script>

@endpush