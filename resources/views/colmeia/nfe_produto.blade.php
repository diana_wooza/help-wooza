@extends('layouts.admin')

@section('title', 'Definir produto para o detalhe de nota')
@section('content')

<div class="row">
	<form class="col-md-12 form-horizontal" method="post" action="{{route('colmeia.nfe.produto', ['nota' => $nota->id, 'detalhe' => $index])}}">
		@csrf
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Detalhe da nota</h3>
			</div>
			<br />
			<div class="box-body">

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Descrição:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{$detalhe->prod->xProd}}" /></div>
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Unidades:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{number_format($detalhe->prod->qCom, 0, ',', '.')}}" /></div>
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Valor Total:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{number_format($detalhe->prod->vProd, 2, ',', '.')}}" /></div>
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Produto</label>
					<div class="col-sm-10">
						<select name="sku" class="form-control" required>
							
							<option value="" selected>Escolha um</option>
							
							@foreach ($produtos as $produto)
							
								<option value="{{$produto->sku}}" {{old('sku') == $produto->sku ? 'selected' : ''}}>{{$produto->name . ' - ' . $produto->sku}}</option>

							@endforeach
							
						</select>
						<p class="help-block">Escolha o produto correspondente à descrição</p>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-12">
						<a class="btn btn-default" href="{{route('colmeia.nfe.listardetalhe', ['id' => $nota->id])}}">Cancelar Tudo</a>
						<button type="submit" class="btn btn-info pull-right" onClick="return confirm('\nTem certeza que deseja atribuir este produto ao detalhe de nota?\n\nEsta operação não pode ser desfeita.');">Salvar Produto ao Detalhe</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

@endsection