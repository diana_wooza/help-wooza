@extends('layouts.admin')
@section('title', 'Validar Entrada em Lote de Seriais')
@section('content')

<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Validação de Entrada em Lote para ICCIDs</h3>
	</div>
	<form role="form" action="" method="post" onSubmit="javascript:document.getElementById('validar-entrada-submit').disabled = true;">
		@csrf
		<input type="hidden" name="validacao" value="validado" />
		<div class="box-body">
			@foreach ($entrada->produtos as $produto)

				<div class="form-group">
					<h3>{{$produto->produto->name}} <small>{{$produto->produto->sku}}</small></h3>
					<p>Total de <strong>{{count($produto->lista)}} ICCIDs</strong> que vão de <strong>{{$produto->inicial}}</strong> até <strong>{{$produto->final}}</strong></p>
				</div>

			@endforeach
		</div>
		<div class="box-footer">
			<button id="validar-entrada-submit" type="submit" class="btn btn-primary" onclick="javascript:return confirm('Tem certeza que deseja validar esta entrada em lote?')">Validar Entrada em lote</button>
		</div>
	</form>
</div>

@endSection