@extends('layouts.admin')

@section('title', 'Editar Relacionamento do EAN ' . $jundsoft->ean)
@section('content')

<form class="row" method="post" action="">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">{!! $jundsoft->exists ? 'Dados dos Relacionamento EAN: <strong>' . $jundsoft->ean . '</strong> [ID ' . $jundsoft->id . ']': 'Novo Relacionamento'!!}</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<div class="form-horizontal">
				<div class="box-body">
					
					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">EAN</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" {!! !$jundsoft->exists ? 'name="ean"' : 'readonly' !!} value="{{old('ean', $jundsoft->ean)}}"/>
						</div>
					</div>

					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Produto</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" {!! !$jundsoft->exists ? 'name="produto"' : 'readonly' !!} value="{{old('produto', $jundsoft->produto)}}"/>
						</div>
					</div>

					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">SKU Allied</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="allied_sku" value="{{old('allied_sku', $jundsoft->allied_sku)}}"/>
						</div>
					</div>

					<div class="form-group">
						<label for="nome" class="col-sm-2 control-label">ID Jund</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" name="jund_id" value="{{old('jund_id', $jundsoft->jund_id)}}"/>
						</div>
					</div>
				
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<a class="btn btn-default" href="{{route('colmeia.jundsoft.listar')}}">Cancelar {{$jundsoft->exists ? 'Edição' : 'Inclusão'}}</a>
							<button type="submit" class="btn btn-info pull-right">Salvar Alterações</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection