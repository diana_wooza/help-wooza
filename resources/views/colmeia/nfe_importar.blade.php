@extends('layouts.admin')

@section('title', 'Importar Nota Fiscal')
@section('content')

<style type="text/css">

.page-importar-nota .form-group .btn-app.active {
	background-color: #00a65a !important;
	border-color: #008d4c !important;
	color: #fff !important;
}

</style>

<div class="row page-importar-nota">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Selecione o tipo de nota fiscal, o arquivo e clique no botão Importar Nota</h3>
			</div>
			<form name="nota" class="form-horizontal" method="post" action="{{route('colmeia.nfe.importar')}}" enctype="multipart/form-data">
				@csrf

				<div class="box-body">

					<div class="form-group">
						<label for="tipo" class="col-sm-2 control-label">Tipo de Nota</label>
						<div class="col-sm-10">
							<label class="btn btn-app">
								<input type="radio" name="tipo" value="entrada" required />
								<i class="fa fa-download"></i> Entrada
							</label>
							<label class="btn btn-app">
								<input type="radio" name="tipo" value="saida" required />
								<i class="fa fa-upload"></i> Saída
							</label>
							<label class="btn btn-app">
								<input type="radio" name="tipo" value="devolucao_entrada" required />
								<i class="fa fa-caret-square-o-down"></i> Devolução Entrada
							</label>
							<label class="btn btn-app">
								<input type="radio" name="tipo" value="devolucao_saida" required />
								<i class="fa fa-caret-square-o-up"></i> Devolução Saída
							</label>
						</div>
					</div>

					<!-- div class="form-group">
						<label for="nota" class="col-sm-2 control-label">Atualizar Dados</label>
						<div class="col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox"> Atualiza os dados da nota no sistema
								</label>
							</div>
						</div>
					</div -->

					<div class="form-group">
						<label for="nota" class="col-sm-2 control-label">Nota</label>
						<div class="col-sm-10">
							<input type="file" name="nota" accept="application/xml" required />
							<p class="help-block">O arquivo de nota fiscal deve ser uma arquivo .xml</p>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<button type="submit" class="btn btn-info">Importar Nota</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">

	var buttons = document.nota.getElementsByClassName('btn-app');

	for (var i = 0, total = buttons.length; i < total; i++)
	{
		buttons[i].onclick = function () {

			for (var i = 0, total = buttons.length; i < total; i++)
				buttons[i].classList.remove('active');
			
			this.classList.add('active');

		}
	}

	// jQuery('.page-importar-nota .form-group .btn-app').on('click', function () {
	// 	jQuery('.page-importar-nota .form-group .btn-app').removeClass('active');
	// 	jQuery(this).addClass('active');
	// });

</script>

@endsection