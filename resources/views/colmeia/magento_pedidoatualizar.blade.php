@extends('layouts.admin')

@section('title', 'Atualizar nota fiscal no magento')
@section('content')

<div class="row">

	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Importe o arquivo do excel com a relação de notas por pedido</h3>
			</div>
			<form name="nota" class="form-horizontal" method="post" action="{{route('colmeia.magento.pedido.atualizar')}}" enctype="multipart/form-data" onSubmit="return confirm('Tem certeza que deseja atualizar a lista de perdidos de acordo com o arquivo ' + document.getElementById('excel-file-upload').files[0].name + '?\n\nEste processo não poderá ser desfeito.\n\nOBS: Apenas uma nova atualização poderá editar estes pedidos.');">
				@csrf

				<div class="box-body">

					<div class="form-group">
						<label for="nota" class="col-sm-2 control-label">Arquivo do Excel</label>
						<div class="col-sm-10">
							<input id="excel-file-upload" type="file" name="arquivo" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
							<p class="help-block">O arquivo do excel precisa estar com a primeira coluna contendo o número do pedido (titulo: increment_id) e a segunda coluna com o número da nota fiscal (título: nota)</p>
						</div>
					</div>

				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<button type="submit" class="btn btn-info">Importar Arquivo</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection