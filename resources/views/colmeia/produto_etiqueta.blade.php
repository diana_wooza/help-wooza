<!doctype html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>[{{config('app.company')}}] {{config('app.name')}} &rsaquo; Etiquetas</title>
		<style type="text/css">
			html,
			body {
				width:114mm;
				margin:0mm;
				padding:0mm;
			}
			body {
				padding:0mm 0mm 0mm 0mm;
				margin-left:-2mm;
			}
			.label {
				float:left;
				width:49mm;
				height:23mm;
				margin:2mm 0mm 1mm 0mm;
				padding:0mm 0mm 0mm 0mm;
				color:#000;
				text-align:center;
				overflow:hidden;
				vertical-align:middle;
				break-before: page;
				break-after: avoid-page;
				page-break-before: always;
				page-break-after: avoid;
				page-break-inside:avoid;
			}
			.label:nth-child(even) {
				margin-left:16mm;
			}
			
			.label .position {
				display:block;
				margin:0mm auto 0.5mm;
				vertical-align:middle;
			}
			.label .serial {
				display:block;
				margin:1mm auto 0mm;
				vertical-align:middle;
			}
			.label img {
				display:block;
				margin:0 auto;
				width:90%;
				height:40%;
			}
		</style>
	</head>
	<body>

		@foreach ($alocacao->alocacoes as $item)
				
			<div class="label">
				<span class="position">{{$item->posicao->armario}}-{{$item->posicao->gaveta}}-{{$item->posicao->posicao}}</span>
				<img src="{!!$item->produto->getSerialBarcodeAsPng()!!}" />
				<span class="serial">{{$item->produto->serial}}</p>
			</div>
	
		@endforeach

	</body>
</html>