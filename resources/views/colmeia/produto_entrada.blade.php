@extends('layouts.admin')

@section('title', 'Confirmação de Entrada de Produtos no Estoque')
@section('content')

<div class="row">
	<form class="col-md-12 form-horizontal" method="post" action="{{route('colmeia.produto.entrada', ['nota' => $nota->id, 'detalhe' => $index])}}">
		@csrf
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Preencha os campos necessários para dar entrada no estoque</h3>
			</div>
			<br />
			<div class="box-body">

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Emissor:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{$nota->nota->NFe->infNFe->emit->xNome}}" /></div>
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">CNPJ do emissor:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{$nota->nota->NFe->infNFe->emit->CNPJ}}" /></div>
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Número da nota eletrônica:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{$nota->codigo}}" /></div>
				
				</div>
				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Número da nota física:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{$nota->nota->NFe->infNFe->ide->nNF}}" /></div>
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Total de produtos:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{number_format($detalhe->prod->qCom, 0, ',','.')}}" /></div>
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Descrição:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{$detalhe->prod->xProd}}" /></div>
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Valor Total:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{number_format($detalhe->prod->vProd, 2, ',', '.')}}" /></div>
				
				</div>

				<div class="form-group">
					<label for="status" class="col-sm-2 control-label">Produto:</label>
					<div class="col-sm-10"><input type="text" class="form-control" disabled value="{{$nota->detalhes[$index]->produto->name . ' - ' . $nota->detalhes[$index]->produto->sku}}" /></div>
				</div>
			</div>
		</div>
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Subitens do detalhe do produto</h3>
			</div>
			<br />
			<div class="box-body row no-margin">

				@foreach ($produtos as $produto)

					<div class="col-md-2" style="padding:5px;">
						<label class="progress-group text-center {{false ? 'bg-red' : ''}}" style="border:1px solid rgba(0,0,0,0.5); border-radius:4px; padding:5px;  background:rgba(0,0,0,0.05); display:block;">
							<span class="progress-text">Serial {{$loop->iteration}}</span>
							<input type="text" name="serial[{{$loop->index}}]" class="form-control move-to-next text-center" value="{{old('serial.' . $loop->index)}}" maxlength="{{$digitos}}" {{!empty($produto->serial) ? 'disabled' : ''}} />
						</label>
					</div>

				@endforeach
				
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-12">
						<a class="btn btn-default" href="{{route('colmeia.nfe.listardetalhe', ['id' => $nota->id])}}" onClick="return confirm('Tem certeza de que deseja cancelar todas as edições?')">Cancelar Tudo</a>
						<button type="submit" class="btn btn-info pull-right">Salvar Todas as Alterações</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

@endsection

@push('body.javascript')

<script type="text/javascript">

	var values = [];

	var preencheLista = function (elemento)
	{
		if ($(elemento).val() == '')
			return;

		values[$('.move-to-next').index(elemento)] = $(elemento).val();
	}

	var encontraDuplicados = function () {

		var duplicatedIndexes = [];
		
		for (var i = 0, total = values.length; i < total; i++)
		{
			if (values[i] == '')
				continue;

			if (values.indexOf(values[i]) != values.lastIndexOf(values[i]))
				duplicatedIndexes.push(i);
		}

		$('.progress-group').removeClass('bg-red');
		
		for (var i = 0, total = duplicatedIndexes.length; i < total; i++)
		{
			$('.progress-group').eq(duplicatedIndexes[i]).addClass('bg-red');
		}

	};

	var testaNumeroCaracteres = function (elemento)
	{
		if ($(elemento).val().length > 0 && $(elemento).val().length != $(elemento).attr('maxlength'))
		{
			$(elemento).addClass('bg-red');
		}
		else
		{
			$(elemento).removeClass('bg-red');
		}
	}

	$('.move-to-next').on('blur', function () {

		preencheLista(this);
		encontraDuplicados();
		testaNumeroCaracteres(this);

	});

	$('.move-to-next').on('keydown', function(event) {

		if(event.keyCode == 13)
		{
			event.preventDefault();
			return false;
		}
	
	});

	$('.move-to-next').on('keyup', function (event) {

		if ($(this).val().length < $(this).attr('maxlength'))
			return;

		$('.move-to-next').eq($('.move-to-next').index(this) + 1).focus();

	});

	$('.move-to-next').each(function () {
		preencheLista(this);
		testaNumeroCaracteres(this);
	});

	encontraDuplicados();

</script>

@endpush