@extends('_generico.tabela_paginada')
@push('header.css')

<style type="text/css">

#history-timeline {
	position:relative;
}
#history-timeline::before {
	content:'';
	display:block;
	position:absolute;
	width:2px;
	height:100%;
	left:5px;
	background:#ccc;
}
#history-timeline .line {
	position:relative;
	padding:5px 5px 5px 20px;
	cursor:pointer;
}
#history-timeline .line::before {
	content:'';
	display:block;
	position:absolute;
	width:12px;
	height:12px;
	left:0px;
	top:50%;
	margin-top:-5px;
	border-radius:50%;
	background:#ccc;
}
#history-timeline .line:hover {
	background:rgba(0,0,0,0.05);
}

#history-timeline .line .date {
	display:block;
	font-size:0.9em;
	font-style:italic;
}

</style>

@endpush
@push('body.javascript')

	<textarea id="textarea-to-copy-to-clipboard" style="display:block; position:absolute;left:-9999px;">{{$dados->pluck('serial')->implode(PHP_EOL)}}</textarea>

	@include('_generico.modal', ['id'=>'detalhe-produto','titulo'=>'Detalhes do Produto','mensagem'=>''])

	<script type="text/javascript">

	function copy_clipboard ()
	{
		var text = document.getElementById('textarea-to-copy-to-clipboard');
		text.select();
		document.execCommand("copy");
		alert('Seriais copiados para a área de transferência');
		return false;
	}

	function carrega_produto (id)
	{
		$('#modal-detalhe-produto').modal();
		$('#modal-detalhe-produto .modal-body').html('<div style="text-align:center; margin:60px 0px;"><i class="fa fa-refresh fa-spin" style="font-size:46px;"></i></div>');
		$.post('{{route('colmeia.api.v2.produto.detalhe')}}',{produto:id}, function (dados,status,jqueryXhr) {

			if (status != 'success')
				return $('#modal-detalhe-produto .modal-body').html('<p class="text-gey" style="text-align:center"><i>Ocorreu um erro na busca de dados</i></p>');
			
			var html = '';
			
			for (var i = 0, total = dados.length; i < total; i++)
			{
				html += '<p class="line">';
				html += dados[i].mensagem;
				html += '<span class="date text-muted">' + (new Date(dados[i].data)).toLocaleString() + '</span>'
				html += '</p>';
			}
			
			$('#modal-detalhe-produto .modal-body').html('<div id="history-timeline">' + html + '</div>');

		});
	}

	</script>

@endpush