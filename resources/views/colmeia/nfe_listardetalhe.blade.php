@extends('layouts.admin')

@section('title', 'Nota Fiscal - ' . $nota->codigo)
@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-body table-responsive no-padding">

				<table class="table table-hover">
					<thead>
						<tr>
							<th class="text-left" style="vertical-align:middle;">Detalhe Nota Fiscal</th>
							<th class="text-center" style="vertical-align:middle; width:300px;">Produto</th>
							<th class="text-center" style="vertical-align:middle; width:200px;">Integração Jundsoft</th>
							<th class="text-center" style="vertical-align:middle; width:80px;">Chips Cadastrados</th>
							<th class="text-center" style="vertical-align:middle; width:80px;">Chips Pendentes</th>
							<th class="text-center" style="vertical-align:middle; width:80px;">Total de Item</th>
							<th class="text-center" style="vertical-align:middle; width:100px;">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					
						@forelse ($nota->getDetalhesPendentes() as $pendente)
						
							<tr>

								<td class="" style="vertical-align:middle;">{{@$nota->detalhes[$pendente->nfe_detalhe]->prod->xProd}}</td>
								<td class="text-center" style="vertical-align:middle;">{!! !is_null(@$nota->detalhes[$pendente->nfe_detalhe]->produto) ? @$nota->detalhes[$pendente->nfe_detalhe]->produto->name . ' - ' . @$nota->detalhes[$pendente->nfe_detalhe]->produto->sku : '<i class="text-grey">produto não cadastrado</i>'  !!}</td>
								<td class="text-center" style="vertical-align:middle;">{!! @App\Models\Colmeia\Integracao\Jundsoft::find($nota->detalhes[$pendente->nfe_detalhe]->jundsoft)->descricao !!}</td>
								<td class="text-center" style="vertical-align:middle;">{{number_format(@$pendente->total - @$pendente->pendente, 0, ',', '.')}}</td>
								<td class="text-center" style="vertical-align:middle;">{{number_format(@$pendente->pendente, 0, ',', '.')}}</td>
								<td class="text-center" style="vertical-align:middle;">{{number_format(@$pendente->total, 0, ',', '.')}}</td>
								<td class="text-center" style="vertical-align:middle;">
									<a {!! @$pendente->pendente > 0 ? 'href="' . route('colmeia.produto.entrada', ['nfe' => $nota->id, 'detalhe' => $pendente->nfe_detalhe]) . '"' : 'disabled' !!} class="btn btn-default" aria-label="Left Align" {!! @$pendente->pendente > 0 ? 'title="Dar entrada nos chips deste produto"' : '' !!}>
										<span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
									</a>
								</td>

							</tr>
					
						@empty

							<tr><td class="jq-mensagem-erro campo-relatorio-vazio text-center" colspan="2"><i><small>Esta nota não possui chips pendentes</small></i></td></tr>

						@endforelse
					
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection