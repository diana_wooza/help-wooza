@extends('layouts.admin')
@section('title')

	@if($entidade->exists)

		Editando {{$tipo}} {{$entidade->nome}}

	@else

		Novo(a) {{$tipo}}

	@endif

@endsection
@php

function parseColuna ($coluna, $entidade)
{
	if (isset($coluna['parser']))
		return $coluna['parser']($coluna, $entidade);

	if (!isset($entidade->{$coluna['nome']}))
		return '';

	return $entidade->{$coluna['nome']};
}

@endphp
@section('content')

<form class="row" method="post" action="">
	<div class="col-md-12">
		@csrf
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">
					@if($entidade->exists)
					
						Editando: <strong> {{@$entidade->nome}}</strong> [ID {{$entidade->id}}]
						
					@else
					
						Novo(a) {{$tipo}}
					
					@endif
					
				</h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">

					@foreach ($colunas as $coluna)

						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">{!! @$coluna['rotulo'] !!}</label>
							<div class="col-sm-10">

								@switch($coluna['tipo'])

									@case('text')

										<input type="text" class="form-control" name="{{$coluna['nome']}}" value="{{old($coluna['nome'], parseColuna($coluna, $entidade))}}"/>
										@break

									@case('dropdown')

										
										<select name="{{$coluna['nome']}}" class="form-control" {{@$coluna['required'] !== false ? 'required' : ''}}>
											<option value="" selected disabled>Escolha um</option>
											@foreach ($coluna['itens'] as $valor => $rotulo)
												<option value="{{$valor}}" {{$valor == old($coluna['nome'], parseColuna($coluna, $entidade)) ? 'selected' : ''}}>{{$rotulo}}</option>
											@endforeach
										</select>
										@break

								@endswitch

							</div>
						</div>

					@endforeach
				
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<a class="btn btn-default" href="{{ redirect()->back()->getTargetUrl() }}">Cancelar</a>
							<button type="submit" class="btn btn-info pull-right">Salvar Alterações</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection