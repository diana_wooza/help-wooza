<div class="modal" tabindex="-1" role="dialog" id="modal-{{$id}}">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			@if (isset($titulo))
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title"><strong>{!!$titulo!!}</strong></h4>
				</div>
			@endif
			<div class="modal-body">
				<p>{!!$mensagem!!}</p>
			</div>
			@if (isset($acoes))
				<div class="modal-footer">
						@foreach ($acoes as $acao)
							<button type="button" class="btn btn-primary">Save changes</button>
						@endforeach
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				</div>
			@endif
		</div>
	</div>
</div>