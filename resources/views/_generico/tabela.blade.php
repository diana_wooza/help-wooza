@php

$export = false;

if (app('request')->query('export') == 'csv')
{
	$export = true;

	header('Content-type: text/csv');
	header('Content-Disposition: attachment; filename=export_' . @$export_filename . '_' . date('YmdHis') . '.csv');
	header('Pragma: no-cache');
	header('Content-Encoding: UTF-8');
	header('Expires: 0');

	$separador = ';';
	$linha = [];

	foreach ($colunas as $coluna)
	{
		if (isset($coluna['exportable']) && $coluna['exportable'] === false)
			continue;

		$linha[] = utf8_decode($coluna['rotulo']);
	}

	echo implode($separador, $linha);
	echo PHP_EOL;

	foreach ($dados as $dado)
	{
		$linha = [];

		foreach ($colunas as $coluna)
		{
			if (isset($coluna['exportable']) && $coluna['exportable'] === false)
			continue;

			$linha[] = utf8_decode($coluna['campo'] instanceof Closure) ? $coluna['campo']($dado) : (is_array($dado) ? $dado[$coluna['campo']] : $dado->{$coluna['campo']});
		}

		echo implode($separador, $linha);
		echo PHP_EOL;
	}

	exit;
}

if (!isset($actions) || empty($actions))
	$actions = [];

if (isset($export_filename))
{
	$params = ['todas'];

	if (isset($sort_by))
		$params[] = $sort_by;

	if (isset($sort_order))
		$params[] = $sort_order;

	$params[] = 'csv';

	$actions[] = ['rotulo' => '<i clas="fa fa-floppy-o"></i> &nbsp; Exportar em CSV', 'url' => $urlDaPagina(...$params)];
}

@endphp

@extends('layouts.admin', ['actions'=>$actions])

@section('title', $titulo)
@if (isset($subtitulo))
	@section('subtitulo', $subtitulo)
@endif
@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-body table-responsive no-padding" style="overflow: initial !important;">

				<div class="panel-heading col-md-12 no-print">

					@if(isset($urlDaPagina))
					
						<form action="{{url($urlDaPagina(1))}}" class="row" method="get">

							@if (isset($filtros))
							
								@foreach ($filtros as $filtro)

									<div class="col-md-{{isset($filtro['largura']) ? $filtro['largura'] : '3'}}" title="{!! @$filtro['titulo'] !!}">

										@switch ($filtro['tipo'])

											@case('select')
											@case('multiselect')

												<select class="form-control selectpicker" name="{{$filtro['nome'] . ($filtro['tipo'] == 'multiselect' ? '[]' : '')}}" data-action-box="1" data-deselect-all-text="Remover seleção" data-select-all-text="Selecionar todos" data-none-selected-text="{{isset($filtro['placeholder']) && $filtro['tipo'] == 'multiselect' ? $filtro['placeholder'] : 'Nenhuma opção selecionada'}}" {{$filtro['tipo'] == 'multiselect' ? 'multiple' : 'Nenhuma opção'}}>

													@if(isset($filtro['placeholder']) && $filtro['tipo'] == 'select')

														<option value="" {{empty($filtro['valor']) ? 'selected' : ''}} disabled style="color:#999;">{{$filtro['placeholder']}}</option>

													@endif

													@foreach ($filtro['opcoes'] as $chave => $valor)

														<option value="{{$chave}}" {{is_array($filtro['valor']) && in_array($chave, $filtro['valor']) || $chave == $filtro['valor'] ? 'selected' : ''}}>
															{{$valor}}
														</option>

													@endforeach

												</select>

												@break

											@default

												<input type="text" class="form-control" name="{{$filtro['nome']}}" placeholder="{{$filtro['placeholder']}}" value="{{$filtro['valor']}}"/>
												@break

										@endswitch

									</div>

								@endforeach

							@endif

							<div class="col-md-{{isset($pagina) ? 2 : 1}} text-right">
								<div class="input-group text-right" style="width:100%;">
									@if (isset($pagina))
										<select class="form-control" name="por_pagina" style="width:calc(100% - 40px);" title="Quantidade de itens exibidos por página">
											@foreach ([10=>'10 itens por página',20=>'20 itens por página',50=>'50 itens por página',100=>'100 itens por página',200=>'200 itens por página',500=>'500 itens por página','todas'=>'Todos os itens'] as $numero => $texto)
												<option value="{{$numero}}" {{$por_pagina == $numero || $numero == 'todas' && is_null($por_pagina) ? 'selected' : ''}}>{{$texto}}</option>
											@endforeach
										</select>
									@endif
									<div class="input-group-append" style="margin-top:-1px;">
										<button type="submit" class="btn btn-outline-secondary glyphicon glyphicon-search"></button>
									</div>
								</div>
							</div>
						</form>

					@endif
				</div>

				@if (isset($form))

					<form action="{{$form}}" method="post">
					@csrf

				@endif

				<table class="table table-hover table-striped table-bordered">
					<thead class="text-black">
						<tr>

							@foreach ($colunas as $coluna)
							
								<th class="{{@$coluna['classe']}}" style="vertical-align:middle; {{@$coluna['style']}}">
									
									@if(isset($coluna['sort']) && isset($urlDaPagina)) <a class="" href="{{url($urlDaPagina(1, $coluna['sort'], $sort_order == 'asc' && $sort_by == $coluna['sort'] ? 'desc' : 'asc'))}}" style="color:#000;"> @endif

									@if(isset($coluna['sort']) && isset($urlDaPagina))
										&nbsp;
										<i class="fa no-print pull-right {{isset($sort_by) && $sort_by == $coluna['sort'] ? 'fa-sort-amount-' . $sort_order : ' fa-bars text-gray'}}" style="margin-top:3px;"></i>
									@endif
									
									{{$coluna['rotulo']}}
									
									@if(isset($coluna['sort']) && isset($urlDaPagina)) </a> @endif
								
								</th>

							@endforeach
						
						</tr>
					</thead>
					<tbody>
					
						@forelse ($dados as $dado)
						
							<tr>

								@foreach ($colunas as $coluna)

									<td class="{{@$coluna['classe']}}" style="vertical-align:middle;">
										{!! ($coluna['campo'] instanceof Closure) ? $coluna['campo']($dado) : (is_array($dado) ? $dado[$coluna['campo']] : $dado->{$coluna['campo']}) !!}
									</td>

								@endforeach

							</tr>
					
						@empty

							<tr><td class="jq-mensagem-erro campo-relatorio-vazio text-center" colspan="{{count($colunas)}}"><i><small>Nenhum registro foi encontrado</small></i></td></tr>

						@endforelse
					
					</tbody>
				</table>

				@if (isset($form))

					<div class="box-footer">
						<div class="row">
							<div class="col-md-10 col-md-offset-2">
								<button type="submit" class="btn btn-info pull-right">Salvar Alterações</button>
							</div>
						</div>
					</div>

					</form>

				@endif
			</div>
		</div>

		@if (isset($pagina) && $totalPaginas > 0)
		
			<div class="pull-right no-print">
				<nav>
					<ul class="pagination">

						@if ($totalPaginas > 1)

							@if ($pagina > 3)

								<li><a href="{{url($urlDaPagina(1, $sort_by, $sort_order))}}" data-pagina="1" aria-label="First" style="margin-right:3px;">
									<span aria-hidden="true">&laquo;</span>
								</a></li>

							@endif

							@if ($pagina > 7)

								<li><a href="{{url($urlDaPagina($pagina - 7, $sort_by, $sort_order))}}" data-pagina="1" aria-label="First" style="margin-right:5px;">
									<span aria-hidden="true">{{$pagina - 7}}</span>
								</a></li>

							@endif

							@for ($n = max(1, min($pagina - 2, $totalPaginas - 5)); $n <= max(min(5, $totalPaginas), min($pagina + 2, $totalPaginas)); $n++)

								<li class="{{$n == $pagina ? 'active' : ''}}"><a href="{{url($urlDaPagina($n, $sort_by, $sort_order))}}" data-pagina="1" aria-label="First">
									<span aria-hidden="true">{{$n}}</span>
								</a></li>

							@endfor

							@if (max($pagina + 7, 10) < $totalPaginas)

								<li><a href="{{url($urlDaPagina(max($pagina + 7, 10), $sort_by, $sort_order))}}" data-pagina="1" aria-label="First" style="margin-left:5px;">
									<span aria-hidden="true">{{max($pagina + 7, 10)}}</span>
								</a></li>

							@endif

							@if ($pagina + 2 < $totalPaginas && $totalPaginas > 5)

								<li><a href="{{url($urlDaPagina($totalPaginas, $sort_by, $sort_order))}}" data-pagina="1" aria-label="First" style="margin-left:3px;">
									<span aria-hidden="true">&raquo;</span>
								</a></li>

							@endif

						@endif

						<li><small style="color:#999; line-height:30px; margin-left:10px;">Página {{$pagina}} de {{$totalPaginas}}</small></li>

					</ul>
				</nav>

			</div>
			
		@endif

	</div>

</div>

@if (isset($view))

	@include($view, isset($viewData) ? $viewData : [])

@endif

@endsection