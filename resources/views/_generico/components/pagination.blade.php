@if ($paginator->hasPages())
	<ul class="pagination" role="navigation">
		<li><span class="text-sm text-black" style="background:transparent !important; border:none !important; margin-right: 10px; line-height:22px;">{{ number_format($paginator->total(), 0, ',','.') }} itens no total</span></li>
		@if ($paginator->onFirstPage())
			<li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
				<span aria-hidden="true">&lsaquo;</span>
			</li>
		@else
			<li>
				<a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
			</li>
		@endif

		@foreach ($elements as $element)
			@if (is_string($element))
				<li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
			@endif

			@if (is_array($element))
				@foreach ($element as $page => $url)
					@if ($page == $paginator->currentPage())
						<li class="active" aria-current="page"><span>{{ $page }}</span></li>
					@else
						<li><a href="{{ $url }}">{{ $page }}</a></li>
					@endif
				@endforeach
			@endif
		@endforeach

		@if ($paginator->hasMorePages())
			<li>
				<a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
			</li>
		@else
			<li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
				<span aria-hidden="true">&rsaquo;</span>
			</li>
		@endif
	</ul>
@endif
