@extends('layouts.admin')

@section('title', isset($title) ? $title : 'Gerenciar Configurações')

@push('body.javascript')
	<script src="{{asset('js/jQuery-Tags-Input-master/src/jquery.tagsinput.js')}}"></script>
	<script type="text/javascript">
		$('.tags-input').tagsInput({width:'100%'});
	</script>
@endpush

@push('header.css')
	<link rel="stylesheet" href="{{asset('js/jQuery-Tags-Input-master/src/jquery.tagsinput.css')}}">
@endpush

@push('body.css')

	<style type="text/css">

		.config-block {
			margin-left:-10px !important;
			margin-right:-10px !important;
			padding-top:15px;
			padding-bottom:15px;
		}
		.config-block:hover {
			background-color:rgba(0,0,0,0.1);
		}

	</style>

@endpush

@section('content')

<div class="row page-importar-nota">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Cada Configuração possui um padrão próprio</h3>
			</div>
			<form name="nota" class="form-horizontal" method="post" action="{{isset($route) ? route($route) : ''}}" enctype="multipart/form-data">
				@csrf

				<div class="box-body">

					@forelse ($configs as $config)

						<div class="form-group config-block">
							<label for="status" class="col-sm-2 control-label">{{$config->chave}}</label>
							<div class="col-sm-10">

								@switch($config->tipo)

									@case(1)

										@break

									@case(2)

										<input type="number" class="form-control" name="{{$config->chave}}" value="{{$config->valor}}" />
										@break

									@case(3)

										<select name="{{$config->chave}}[]" class="form-control selectpicker" multiple>
											
											@foreach ($list[$config['chave']] as $chave => $valor)

												<option value="{{$chave}}" {{@in_array($chave, @$config['valor']) ? 'selected' : ''}}>{{$valor}}</option>

											@endforeach
										
										</select>
										@break

									@case(4)

										<input type="text" class="form-control tags-input" name="{{$config->chave}}" value="{{implode(',', $config->valor)}}" />
										@break

									@case(5)

										<select name="{{$config->chave}}" class="form-control selectpicker">
											
											<option value="1" {{@$config['valor'] ? 'selected' : ''}}>Sim</option>
											<option value="0" {{!@$config['valor'] ? 'selected' : ''}}>Não</option>
										
										</select>
										@break

									@case(6)

										<select name="{{$config->chave}}" class="form-control selectpicker">
											
											<option value="" disabled selected>Escolha uma</option>

											@foreach ($list[$config['chave']] as $chave => $valor)

												<option value="{{$chave}}" {{$chave == @$config['valor'] ? 'selected' : ''}}>{{$valor}}</option>

											@endforeach

										</select>
										@break

								@endswitch

								<p class="help-block">{!! $config->descricao !!}</p>
									
							</div>
						</div>

					@empty

						<p>Nenhuma configuração para ser gerenciada</p>

					@endforelse

				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-10 col-md-offset-2">
							<button type="submit" class="btn btn-info">Salvar Configurações</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">

	var buttons = document.nota.getElementsByClassName('btn-app');

	for (var i = 0, total = buttons.length; i < total; i++)
	{
		buttons[i].onclick = function () {

			for (var i = 0, total = buttons.length; i < total; i++)
				buttons[i].classList.remove('active');
			
			this.classList.add('active');

		}
	}

	// jQuery('.page-importar-nota .form-group .btn-app').on('click', function () {
	// 	jQuery('.page-importar-nota .form-group .btn-app').removeClass('active');
	// 	jQuery(this).addClass('active');
	// });

</script>

@endsection