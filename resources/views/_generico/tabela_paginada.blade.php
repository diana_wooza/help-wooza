@extends('layouts.admin', ['actions'=>@$actions])
@section('title', $titulo)
@if (isset($subtitulo))
	@section('subtitulo', $subtitulo)
@endif
@push('header.javascript')

	<link rel="stylesheet" href="{{asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
	<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

@endpush

@section('content')

@php

$getSortUrl = function ($coluna) {

	if (!isset($coluna['sort']))
		return '';

	$params = \Request::query();
	$params['page']	= 1;
	$params['sort_by'] = $coluna['sort'];
	$params['sort_order'] = \Request::query('sort_by') == $coluna['sort'] && \Request::query('sort_order') == 'asc' ? 'desc' : 'asc';

	return \Request::fullUrlWithQuery($params);
	
};

$getResumoFiltro = function ($filtros)
{
	$aplicados = [];
	$paginacao = '';

	#if (request()->has('por_pagina'))
	#	$paginacao = ' Exibindo ' . request()->query('por_pagina') . ' itens por página';

	foreach ($filtros as $filtro)
	{
		if (!request()->has($filtro['chave']) || empty(request()->{$filtro['chave']}))
			continue;

		$aplicado = '<strong>' . $filtro['rotulo'] . '</strong>: ';
		
		switch ($filtro['tipo'])
		{
			case 'multiselect':

				$aplicado .= @implode(', ', array_map(function ($item) use ($filtro) {return @$filtro['itens'][$item];}, request()->query($filtro['chave'])));
				break;

			case 'select':

				$aplicado .= @$filtro['itens'][request()->query($filtro['chave'])];
				break;

			default:

				$aplicado .= request()->query($filtro['chave']);
		}

		$aplicados[] = $aplicado;
	}

	if (empty($aplicados))
		return '';

	return implode(' ', array_map(function ($item) {return '<span class="label label-default" style="font-weight:normal;">' . $item . '</span>'; }, $aplicados)) . $paginacao;
};

@endphp

@if(isset($filtros) && is_array($filtros) && !empty($filtros))

	<div class="row no-print">
		<form role="form" action="" method="get">
			<div class="col-md-12">
				<div class="box box-default collapsed-box">
					<div class="box-header with-border">
						<h3 class="box-title" style="max-width: calc(100% - 50px); overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">Filtros:</h3>
						<div class="">{!! $getResumoFiltro($filtros) !!}</div>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
						</div>
					</div>
					<div class="box-body" style="display:none;">
						<div class="row">

							@foreach ($filtros as $index => $filtro)

								<div class="form-group col-md-{{ isset($filtro['colunas']) ? $filtro['colunas'] : '12' }}">
									<label for="tabela-paginada-item-{{ $index }}">{!! @$filtro['rotulo'] !!}</label>

									@switch($filtro['tipo'])

										@case('text')

											<input type="text" class="form-control" name="{{ $filtro['chave'] }}" value="{{ request()->query($filtro['chave']) }}" placeholder="{{ isset($filtro['placeholder']) ? $filtro['placeholder'] : 'sem filtro' }}" />
											@break

										@case('number')

											<input type="number" class="form-control" name="{{ $filtro['chave'] }}" value="{{$config->valor}}" placeholder="{{ isset($filtro['placeholder']) ? $filtro['placeholder'] : 'sem filtro' }}" />
											@break

										@case('simnao')

											<select name="{{ $filtro['chave'] }}" class="form-control selectpicker">
												
												<option value="1" {{@$config['valor'] ? 'selected' : ''}}>Sim</option>
												<option value="0" {{!@$config['valor'] ? 'selected' : ''}}>Não</option>

											</select>
											@break

										@case('select')

											<select name="{{ $filtro['chave'] }}" class="form-control selectpicker">
												
												@if (@$filtro['placeholder'] !== false)
													<option value="" selected>{{ isset($filtro['placeholder']) ? $filtro['placeholder'] : 'sem filtro' }}</option>
												@endif

												@foreach ($filtro['itens'] as $chave => $valor)
												
													<option value="{{ $chave }}" {{ request()->query($filtro['chave']) == $chave ? 'selected' : '' }}>{{ $valor }}</option>

												@endforeach

											</select>
											@break

										@case('multiselect')

											<select name="{{ $filtro['chave'] }}[]" class="form-control selectpicker" multiple data-action-box="1" data-deselect-all-text="Remover seleção" data-select-all-text="Selecionar todos" data-none-selected-text="sem filtro">
												
												@foreach ($filtro['itens'] as $chave => $valor)
												
													<option value="{{ $chave }}" {{ @in_array($chave, request()->query($filtro['chave'])) ? 'selected' : '' }}>{{ $valor }}</option>

												@endforeach

											</select>
											@break

										@case('daterange')

											<input type="hidden" id="tabela-paginada-datepicker-{{ $chave }}-de" name="{{ $filtro['chave'] }}[de]" value="" />
											<input type="hidden" id="tabela-paginada-datepicker-{{ $chave }}-ate" name="{{ $filtro['chave'] }}[ate]" value="" />
											<button id="tabela-paginada-datepicker-{{ $chave }}" type="button" class="btn btn-default form-control text-left" style="text-align: left !important;">
												<span><i class="fa fa-calendar"></i> Sem filtro</span>
												<i class="fa fa-caret-down pull-right" style="margin-top:4px;"></i>
											</button>
											
											<script type="text/javascript">

												$('#tabela-paginada-datepicker-{{ $chave }}').daterangepicker(
													{
														ranges				: {
															'Hoje'				: [moment(), moment()],
															'Ontem'				: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
															'Últimos 7 Dias'	: [moment().subtract(6, 'days'), moment()],
															'Últimos 30 Dias'	: [moment().subtract(29, 'days'), moment()],
															'Este Mês'			: [moment().startOf('month'), moment().endOf('month')],
															'Mês Passado'		: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
														},
														startDate			: moment().subtract(29, 'days'),
														endDate				: moment(),
														showWeekNumbers		: true,
														timePicker24Hour	: true,
														timePickerSeconds	: false,
														locale				: {
															format				: 'DD/MM/YYYY',
															separator			: '/',
															applyLabel			: 'Aplicar',
															cancelLabel			: 'Cancelar',
															fromLabel			: 'de',
															toLabel				: 'Até',
															customRangeLabel	: 'Personalizado',
															weekLabel			: 'S',
															daysOfWeek			: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
															monthNames			: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
															firstDay			: 1
														},
													},
													function (start, end) {
														$('#tabela-paginada-datepicker-{{ $chave }}-de').val(start.format('YYYY-MM-DD'));
														$('#tabela-paginada-datepicker-{{ $chave }}-ate').val(end.format('YYYY-MM-DD'));
														$('#tabela-paginada-datepicker-{{ $chave }} span').html(start.format('DD/MM/YYYY') + ' até ' + end.format('DD/MM/YYYY'));
													}
												);

											</script>

											@break

									@endswitch

									@if (isset($filtro['detalhes']))
										<p class="help-block">{!! $filtro['detalhes'] !!}</p>
									@endif

								</div>

							@endforeach

						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary pull-right">Atualizar Filtros</button>
						<select class="form-control pull-right" style="display:inline-block; width:auto; margin-right:15px;" name="por_pagina" title="Quantidade de itens exibidos por página">
							@foreach ([10=>'10 itens por página',20=>'20 itens por página',50=>'50 itens por página',100=>'100 itens por página',200=>'200 itens por página',500=>'500 itens por página',999999=>'Todos os itens'] as $numero => $texto)
								<option value="{{$numero}}" {{request()->query('por_pagina', 50) == $numero ? 'selected' : ''}}>{{$texto}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</form>
	</div>
@endif

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<form action="" class="row" method="post" style="margin-top:-10px !important; margin-bottom:-32px !important;">
				@csrf
				<div class="box-body table-responsive no-padding" style="overflow: initial !important;">
					<div class="panel-heading col-md-12 no-print">
						<table class="table table-hover table-striped table-bordered">
							<thead class="text-black">
								<tr>
									@foreach ($colunas as $coluna)

										<th class="{{@$coluna['classe']}}" style="vertical-align:middle; {{@$coluna['style']}}">

											<a class="text-nowrap" href="{{$getSortUrl($coluna)}}" style="color:#000;">
												<i class="fa no-print {{\Request::query('sort_by') == @$coluna['sort'] ? 'fa-sort-' . \Request::query('sort_order') : ' fa-sort text-gray'}}" style="margin-top:3px;"></i>
												&nbsp;
												{{$coluna['rotulo']}}
											</a>
										
										</th>

									@endforeach
								</tr>
							</thead>
							<tbody>
						
								@forelse ($dados as $dado)
								
									<tr style="{!! isset($configuracao['row_style']) && $configuracao['row_style'] instanceof Closure ? $configuracao['row_style']($dado) : '' !!}">

										@foreach ($colunas as $coluna)

											<td class="{{@$coluna['classe']}}" style="vertical-align:middle; {{@$coluna['style']}}">
												{!! ($coluna['campo'] instanceof Closure) ? $coluna['campo']($dado) : (is_array($dado) ? $dado[$coluna['campo']] : $dado->{$coluna['campo']}) !!}
											</td>

										@endforeach

									</tr>
							
								@empty

									<tr><td class="jq-mensagem-erro campo-relatorio-vazio text-center" colspan="{{count($colunas)}}"><i><small>Nenhum registro foi encontrado</small></i></td></tr>

								@endforelse
						
							</tbody>
						</table>
					</div>
				</div>
				@if (isset($form))

					<div class="col-md-12">
						<div class="box-footer">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-info pull-right">Salvar Alterações</button>
								</div>
							</div>
						</div>
					</div>

				@endif
			</form>
			<div class="pull-right no-print">
				@php
				
					try
					{
						if (isset($filtros))
						{
							$filtros = array_map(function ($filtro) {return $filtro['chave'];}, $filtros);
							$filtros = array_merge($filtros, ['sort_by', 'sort_order']);
						}
						else
						{
							$filtros = ['sort_by', 'sort_order'];
						}

						echo $links = $dados->appends(request()->only($filtros))->links('_generico.components.pagination');
					}
					catch (\Throwable $throwable)
					{
						echo $throwable->getMessage();
					}
				
				@endphp
			</div>
		</div>
	</div>
</div>

@endsection