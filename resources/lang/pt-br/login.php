<?php

return [

	'title'			=> 'Faça o Login',
	'message'		=> 'Faça o login',
	'placeholder'	=> [
		'email'			=> 'Usuário do domínio',
		'password'		=> 'Senha de acesso'
	],
	'empty'			=> [
		'username'		=> 'Você precisa digitar seu nome de usuário da rede',
		'password'		=> 'Você precisa digitar uma senha',
	],
	'login'			=> 'Entrar',
	'register'		=> [
		'title'			=> 'Preencha seus dados',
		'label_name'	=> 'Nome',
		'fill_name'		=> 'Digite seu nome',
		'label_phone'	=> 'Telefone',
		'fill_phone'	=> '(000)0000-0000',
		'save'			=> 'Salvar seus dados',
	],

];
