<?php

return [

	'title'			=> 'Login Page',
	'message'		=> 'Sign in to start your session',
	'placeholder'	=> [
		'email'			=> 'Domain username',
		'password'		=> 'Password',
	],
	'empty'			=> [
		'username'		=> 'Domain username is required',
		'password'		=> 'Password is required',
	],
	'login'			=> 'Sign In',

];
