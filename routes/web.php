<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#API ORDER
// Route::get('api_order/json_elegibilidade/{order}/{segmento?}', function ($order, $segmento = 'POS_PAGO') {

// 	return App\Helpers\Tim\ApiOrdem::GetDataElegibilidade(\App\Models\Magento\Consumer\Order::with(['adicionais','origem','addresses','customer','webjump'])->findOrFail($order), $segmento);

// });

// Route::get('api_order/json_envio/{order}', function ($order) {

// 	return App\Helpers\Tim\ApiOrdem::GetDataEnvio(\App\Models\Magento\Consumer\Order::with(['adicionais','origem','addresses','customer','webjump'])->findOrFail($order));

// });

#Test Route
Route::any('test', 'TestController');
Route::any('info', function () {
	phpinfo();
});

Route::namespace('Portal')->name('portal.')->group(function() {
	Route::any('/portal/login', 'PortalController@login')->name('login_portal');
	Route::any('/portal/senha', 'PortalController@alterarSenha')->name('alterar_senha_portal');
	Route::middleware('portal')->group(function() {
		Route::get('/portal', 'PortalController@grid')->name('grid_portal');
		Route::any('/portal/logout', 'PortalController@logout')->name('logout_portal');
		Route::get('/portal/dados_pedido/{pedido?}', 'PortalController@dadosPedido')->name('dados_pedido_portal');
	});
});

Route::middleware('auth')->namespace('Configuracao')->name('configuracao.')->group(function () {
	Route::get('/configuracao/lista_tratativas', 'ConfiguracaoController@listaTratativas')->name('lista_tratativas');
	Route::any('/configuracao/editar_tratativa/{id?}', 'ConfiguracaoController@editarTratativa')->name('editar_tratativa');
	Route::get('/configuracao/lista_acoes', 'ConfiguracaoController@listaAcoes')->name('lista_acoes');
	Route::any('/configuracao/editar_acao/{id?}', 'ConfiguracaoController@editarAcao')->name('editar_acao');
	Route::get('/configuracao/lista_condicoes_especiais', 'ConfiguracaoController@listaCondicoesEspeciais')->name('lista_condicoes_especiais');
	Route::any('/configuracao/editar_condicao_especial/{id?}', 'ConfiguracaoController@editarCondicaoEspecial')->name('editar_condicao_especial');
	Route::get('/configuracao/lista_campos', 'ConfiguracaoController@listaCampos')->name('lista_campos');
	Route::any('/configuracao/editar_campo/{id?}', 'ConfiguracaoController@editarCampo')->name('editar_campo');

	Route::any('/configuracao', 'ConfiguracaoController@exibir')->name('tratativas_automaticas');
	Route::any('/configuracao/historico', 'ConfiguracaoController@exibirHistorico')->name('historico');
	Route::any('/configuracao/historico/datas_historico/{robo}', 'ConfiguracaoController@datasHistorico')->name('datas_historico');
	Route::any('/configuracao/historico/carregar_versao/{versao}', 'ConfiguracaoController@carregarVersaoHistorico')->name('carregar_versao_historico');
	
	Route::get('/configuracao/retorno_robo/interface_configuracao/{robo}', 'ConfiguracaoController@getInterfaceConfiguracao');
	Route::get('/configuracao/retorno_robo/json_robo/{robo}', 'ConfiguracaoController@getJsonRobo');
	Route::get('/configuracao/retorno_robo/campos_robo/{robo}', 'ConfiguracaoController@getCamposRobo');
	Route::get('/configuracao/retorno_robo/valores_select/{campo}', 'ConfiguracaoController@getValoresSelect');
	Route::get('/configuracao/retorno_robo/usuario_padrao/{robo}', 'ConfiguracaoController@getUsuarioPadrao');
	Route::get('/configuracao/retorno_robo/campos_condicoes/{operadora?}', 'ConfiguracaoController@getCamposCondicoes');
	Route::get('/configuracao/retorno_robo/campos_dados_adicionais_interface', 'ConfiguracaoController@getCamposDadosAdicionaisInterface');
	Route::get('/configuracao/retorno_robo/acoes_interface/{tratativa}', 'ConfiguracaoController@getAcoesInterface');
	Route::get('/configuracao/retorno_robo/descricoes_acoes', 'ConfiguracaoController@getDescricoesAcoes');
	Route::get('/configuracao/retorno_robo/campos_comparativos_order', 'ConfiguracaoController@getCamposComparativosOrder');
	
	Route::post('/configuracao/retorno_robo/salvar_configuracao', 'ConfiguracaoController@salvarConfiguracao');
});

Route::middleware('auth')->namespace('Cron')->name('cron.')->group(function () {
	Route::any('/cron/lista/{resultado?}', 'CronController@Lista')->name('lista');
	Route::any('/cron/editar/{id?}', 'CronController@Editar')->name('editar');
	Route::get('/cron/ativar/{id?}', 'CronController@Ativar')->name('ativar');
});

#Default route
Route::any('/dashboard', function () {return view('dashboard');})->name('home')->middleware('auth');

Route::namespace('Hub')->group(function () {

	Route::any('/login', 'LoginController@entrar')->name('login');
	Route::any('/sair', 'LoginController@sair')->name('logout');
	Route::any('/modificar-senha/{usuario}', 'UsuarioController@mudar_senha')->name('hub.usuario.mudar.senha');

	Route::middleware('auth')->name('hub.')->group(function () {

		Route::any('/cadastro', 'LoginController@cadastro')->name('cadastro');
		Route::any('/permissionar/{key}', 'UsuarioController@permissionar')->name('usuario.permissionar');
		Route::any('/usuario/editar/{id?}', 'UsuarioController@editar')->name('usuario.editar');

		Route::middleware('permissao:hub.admin')->group(function () {

			Route::any('/config', 'ConfigController@geral')->name('config');

			Route::name('usuario.')->group(function () {

				Route::get('/usuario-gerar-senha/{usuario}', 'UsuarioController@gerar_senha')->name('gerar.senha');
				Route::any('/usuario', 'UsuarioController@lista')->name('lista');

			});

			Route::name('perfil.')->group(function () {

				Route::any('/Perfil', 'PerfilController@lista')->name('lista');

			});

		});

	});

});


/**
 * Contestador
 */

Route::namespace('Contestador')->prefix('/contestador')->middleware('permissao:contestador')->name('contestador.')->group(function () {

	Route::any('update-pedido-marcado', function () {
		Artisan::call('contestador:update_pedido_marcado');
		return 'Enviado!';
	});

	Route::Any('/pedido', 'PedidoController@listar')->name('pedido.listar');
	Route::Any('/pedido/ver/{id}', 'PedidoController@ver')->name('pedido.ver');
	
	Route::Any('/receita', 'ReceitaController@listar')->name('receita.listar');
	Route::Any('/receita/ver/{id}', 'ReceitaController@ver')->name('receita.ver');
	Route::Any('/receita/marcar-regra', 'ReceitaController@marcar_regra')->name('receita.marcar');

	Route::any('/competencia', 'CompetenciaController@listar')->name('competencia.listar');
	Route::any('/competencia/nova', 'CompetenciaController@novo')->name('competencia.novo');
	Route::any('/competencia/reprocessar/{id}', 'CompetenciaController@reprocessar')->middleware('permissao:contestador.admin')->name('competencia.reprocessar');
	
	Route::middleware('permissao:contestador.marcacao.operador|contestador.admin')->group(function () {
		
		Route::any('/arquivo', 'ArquivoController@listar')->name('arquivo.listar');
		Route::any('/arquivo/novo', 'ArquivoController@editar')->name('arquivo.novo');
		Route::any('/arquivo/editar/{id}', 'ArquivoController@editar')->name('arquivo.editar');
		Route::any('/arquivo/deletar/{id}', 'ArquivoController@deletar')->name('arquivo.deletar');
		Route::any('/arquivo/upload/{id}', 'ArquivoController@upload')->name('arquivo.upload');
		Route::any('/arquivo/download', 'ArquivoController@download')->name('arquivo.download');
		
		Route::any('/processo/listar', 'ProcessoController@listar')->name('processo.listar');
		Route::any('/processo/reprocessar/{id}', 'ProcessoController@reprocessar')->middleware('permissao:contestador.admin')->name('processo.reprocessar');
		Route::any('/processo/download/{id}', 'ProcessoController@download')->name('processo.download');

	});

	// Route::prefix('/marcador')->name('marcador.')->group(function () {

	// 	Route::any('/listar', 'MarcadorController@listar')->name('lista');
	// 	Route::any('/marcacoes', 'MarcadorController@marcacoes')->name('marcacoes');
	// 	Route::any('/marcar/{marcador}', 'MarcadorController@marcar')->name('marcar');
	// 	Route::any('/novo', 'MarcadorController@editar')->name('novo');
	// 	Route::any('/editar/{marcador}', 'MarcadorController@editar')->name('editar');
	// 	Route::any('/duplicar/{marcador}', 'MarcadorController@duplicar')->name('duplicar');

	// });

	Route::middleware('permissao:contestador.marcacao.editor')->group(function () {

		Route::any('regra/marcacao', 'RegraController@marcacao_listar')->name('regra.marcacao.listar');
		Route::any('regra/marcacao/nova', 'RegraController@marcacao_editar')->name('regra.marcacao.nova');
		Route::any('regra/marcacao/editar/{id}', 'RegraController@marcacao_editar')->name('regra.marcacao.editar');
		
		// Route::any('regra/apuracao_receita', 'RegraController@listar')->name('regra.listar.apuracaoreceita')->defaults('tipo','apuracao_receita');
		// Route::any('regra/comissao', 'RegraController@listar')->name('regra.listar.comissao')->defaults('tipo','comissao');
		
		// Route::any('/regra', 'RegraController@listar')->name('regra.listar');
		// Route::any('/regra/nova/{tipo?}', 'RegraController@nova')->name('regra.nova');
		// Route::any('/regra/editar/{id}', 'RegraController@editar')->name('regra.editar');
		
		// Route::any('/meta', 'MetaController@listar')->name('meta.listar');
		// Route::any('/remuneracao', 'TabelaController@remuneracao_listar')->name('remuneracao.listar');
		// Route::any('/remuneracao/nova', 'TabelaController@remuneracao_editar')->name('remuneracao.novo');
		// Route::any('/remuneracao/editar/{id}', 'TabelaController@remuneracao_editar')->name('remuneracao.editar');
		// Route::any('/remuneracao/deletar/{id}', 'TabelaController@remuneracao_deletar')->name('remuneracao.deletar');

	});

	Route::middleware('permissao:contestador.admin')->group(function () {

		Route::any('/config', 'ConfigController@geral')->name('config');

	});

});

/**
 * Catálogo
 */
 Route::namespace('Catalogo')->prefix('/catalogo')->name('catalogo.')->middleware('permissao:catalogo')->group(function () {

	Route::prefix('/contingencia')->name('contingencia.')->middleware('permissao:catalogo.contingencia')->group(function () {

		Route::any('/analisador-bundle', 'ContingenciaController@analizador_bundle')->name('analisador');

	});

});

/**
 * Alarmística
 */

/**
 * endpoint para o robo do Callel
*/
Route::get('dashboard-robo-acesso', 'Alarmistica\ApiController@listaLogs');


Route::namespace('Alarmistica')->prefix('/alarmistica')->middleware('permissao:alarmistica')->name('alarmistica.')->group(function () {

	Route::prefix('/api')->name('api.')->group(function () {

		Route::get('exibeIndicadoresMenu', 'ApiController@exibeIndicadoresMenu')->name('exibeIndicadoresMenu');
		Route::any('listaApis', 'ApiController@listaApis')->name('listaApis');
		Route::any('listaLogs', 'ApiController@listaLogs')->name('listaLogs');
		
	});

	Route::middleware('permissao:alarmistica.admin')->group(function () {

		Route::prefix('/configuracao')->name('configuracao.')->group(function () {
			route::any('/ajustar', 'ConfiguracaoController@ajustar')->name('ajustar');
			route::any('/editar', 'ConfiguracaoController@editar')->name('editar');
			route::any('/cadastro', 'ConfiguracaoController@cadastro')->name('cadastro');
			route::any('/alterar', 'ConfiguracaoController@alterar')->name('alterar');
		});
	});

	Route::get('backlog', 'ApiController@backlog')->name('backlog');
	Route::post('/backlog-importar','ApiController@backlogImportar')->name('backlog_send');
});

/**
 * Indicadores
 */

 Route::namespace('Indicadores')->prefix('/indicadores')->middleware('permissao:indicadores')->name('indicadores.')->group(function () {

	Route::get('/logistica-consolidado','LogisticaController@consolidado')->name('logisticaconsolidado');

	Route::prefix('/graficosindicadores')->name('graficosindicadores.')->group(function(){
		route::get('/index', 'GraficosIndicadoresController@index')->name('index');
		route::get('/cadastrarMetas', 'GraficosIndicadoresController@cadastrarMetas')->name('cadastrarMetas');
	});

});

/**
 * Reaproveitamento
 */
Route::namespace('Reaproveitamento')->prefix('/reaproveitamento')->middleware('permissao:reaproveitamento')->name('reaproveitamento.')->group(function () {
	Route::get('/agendar_reaproveitamento','ReaproveitamentoController@AgendarReaproveitamento')->name('agendar_reaproveitamento');
	Route::post('/reaproveitamento.salvar_agendamento','ReaproveitamentoController@SalvarAgendamento')->name('salvar_agendamento');
	Route::post('/reaproveitamento.excluir_agendamento','ReaproveitamentoController@ExcluirAgendamento')->name('excluir_agendamento');
});

/**
 * Filas
 */
Route::namespace('Filas')->prefix('/filas')->name('filas.')->group(function () {
	Route::any('/gravar_pedido_fila/{fila}/{pedido}', 'FilasMagentoController@GravarPedidoFila')->name('gravar_pedido_fila');
});

/**
 * Pós-venda
 */
Route::namespace('Posvenda')->prefix('/posvenda')->middleware('permissao:posvenda')->name('posvenda.')->group(function () {
	Route::get('/registrar_atendimentos','PosVendaController@registrarAtendimentos')->name('registrar_atendimentos');
	Route::any('/listar_atendimentos','PosVendaController@listarAtendimentos')->name('listar_atendimentos');
	Route::post('/atendimentos.salvar','PosVendaController@salvarAtendimento')->name('salvar_atendimento');
	Route::post('/aviso.salvar','PosVendaController@salvarAviso')->name('salvar_aviso');
	Route::get('/buscar_motivos/{id?}','PosVendaController@buscarMotivos')->name('buscar-motivos');
	Route::get('/buscar_atendimento_por_motivo', 'PosVendaController@buscarAtendimentoPorPedido')->name('buscar-por-pedido');
	Route::get('/search_mortivos/{id?}','PosVendaController@seachMotivos')->name('search-motivos');
	Route::get('/busca_comentarios/{id?}','PosVendaController@buscarComentarios')->name('buscar-comentarios');
	Route::get('/busca_origens','PosVendaController@buscarOrigens')->name('buscar-origens');
	
	Route::middleware('permissao:posvenda.admin')->group(function () {
		Route::get('/deletar-atendimento/{id?}','PosVendaController@deletarAtendimento')->name("posvenda.deletar");
		Route::get('/adicionar_opcoes','PosVendaController@adicionarOpcoes')->name('adicionar_opcoes');
		Route::get('/alterar_opcoes','PosVendaController@alterarOpcoes')->name('alterar_opcoes');
		Route::get('/status','PosVendaController@buscarStatus');
		Route::any('/administrar_relacoes_motivos/{id?}','PosVendaController@administrarRelacoesMotivos')->name('administrar_relacoes_motivos');
		Route::get('/listar_motivos','PosVendaController@listarMotivos')->name('listar_motivos');
		Route::post('/atendimentos.salvar_opcoes','PosVendaController@salvarOpcoes')->name('salvar_opcoes');
		Route::post('/atendimentos.salvar_alterar_opcoes','PosVendaController@salvarAlterarOpcoes')->name('salvar_alterar_opcoes');
		Route::post('/cadastrar','PosVendaController@cadastrarAtendimento')->name('posvenda.cadastrar');
	});
});

/**
 * Magento
 */

Route::prefix('/magento')->namespace('Magento')->name('magento.')->group(function () {

	Route::any('vincular-login-magento', 'LoginController@vincular')->name('vincular');

	Route::any('alterar-dia-vencimento', 'PedidoController@dia_vencimento')->name('dia_vencimento');
	Route::any('atualizar-nomenclatura-sap', 'PedidoController@atualizar_nomenclatura')->name('nomenclaturasap');

	Route::any('/pedidos', 'PedidoController@listar')->name('pedido.listar');
	Route::any('/pedidos/ver/{pedido}', 'PedidoController@ver')->name('pedido.ver');

	Route::any('status-motivos-listar', 'MotivoController@listar')->name('motivos.listar');
	Route::any('status-motivos-criar', 'MotivoController@criar')->name('motivos.criar');
	Route::any('admin-user-base-listar', 'AdminUserBaseController@listar')->name('adminuser.listar');
	Route::any('admin-user-base-criar', 'AdminUserBaseController@criar')->name('adminuser.criar');
	
	Route::any('json-api-tim/{engine}/{versao}/{json}/{pedido}/{simulation?}', 'PedidoController@get_json_api_tim')
		->where([
			'engine' => 'cubo|tim',
			'versao' => 'v(3|5)',
			'json' => 'elegibilidade|order',
			'simulation' => 'avera']
		)
		->name('pedido.jsonapi')
	;

});

/**
 * Importador
 */

Route::namespace('Importador')->prefix('/importador')->middleware('permissao:importador')->name('importador.')->group(function () {

	Route::any('/importar/{id?}', 'ImportadorController@importar')->name('importar');

	Route::middleware('permissao:importador.downgrade')->group(function () {
		Route::get('/downgrade-tim-importar','ImportadorController@downgradeTimImportar')->name('downgradetim');
		Route::post('/downgrade-tim-importar','ImportadorController@downgradeTimImportar')->name('downgradetim_send');
		Route::get('/downgrade-vivo-importar','ImportadorController@downgradeVivoImportar')->name('downgradeVivo');
		Route::post('/downgrade-vivo-importar','ImportadorController@downgradeVivoImportar')->name('downgradevivo_send');
		Route::get('/downgrade-listar','ImportadorController@listar')->name('downgrade-listar');
	});
	
	Route::middleware('permissao:importador')->group(function () {
		Route::any('/status-pedido-importar','ImportadorController@statusPedidoImportar')->name('statuspedido');
		Route::get('/download-modelo-planilha', 'ImportadorController@downloadModeloPlanilha')->name('downloadplanilha');
		Route::any('/varejo-loja','ImportadorController@importarVarejoLoja')->name('varejoloja');
		Route::any('/cpf-vendedor','ImportadorController@cpf_vendedor')->name('cpfvendedor');
		Route::any('/phone-number','ImportadorController@phone_number')->name('phonenumber');
		Route::any('/decriptador-pedidos','ImportadorController@decrypt')->name('decrypt');
	});
});

/**
 * retornos
 */
Route::namespace('Retornos')->prefix('/retornos')->middleware('permissao:robo.retornos')->name('retornos.')->group(function () {
	Route::prefix('/tim')->namespace('Tim')->name('tim.')->group(function () {
		Route::get('/consulta-ordem','HistoricoController@consultaOrdem')->name('historicos.consultaordem');
	});
});

Route::namespace("Lojaonline")->prefix('/lojaonline')->name('lojaonline.')->group(function () {

	Route::any('enviar-pedidos-email', function () {
		Artisan::call('lojaonline:relatorio_crivo');
		return 'Enviado!';
	});

	Route::any('/api/claro/{data_inicio}/{data_fim}', 'ApiController@pedidos_claro')->name('pedidos.claro');
	Route::any('/api/claro_prospect/{data_inicio}/{data_fim}', 'ApiController@pedidos_claro_prospect')->name('pedidos.claro_prospect');
	Route::any('/api/claro_pre/{data_inicio}/{data_fim}', 'ApiController@pedidos_claro_pre')->name('pedidos.claro_pre');
	Route::any('/api/claro_pos/{data_inicio}/{data_fim}', 'ApiController@pedidos_claro_pos')->name('pedidos.claro_pos');

	Route::any('/pedidos/{loja}', 'PedidosController@listar')->name('pedidos.listar');

});

/**
 * Colmeia
 */

###############################
#suporte rotas antigas
###############################

#Route::any('/lista-colmeia','Colmeia\ArmarioController@lista');
Route::get('/busca-chip-estoque/{serial}', 'Colmeia\Api\v1\ProdutoController@check_serial_exists');
Route::get('/api-picking-automatico/{serial?}/{sku?}', 'Colmeia\Api\v1\ProdutoController@check_picking');
Route::get('/liberar-chip-colmeia/{nota}/{pedido}/{serial}/{sku}/{imei?}', 'Colmeia\Api\v1\ProdutoController@liberar_chip');
Route::get('/liberar-aparelho-colmeia/{nota}/{pedido}/{imei}', 'Colmeia\Api\v1\ProdutoController@liberar_aparelho');
Route::get('/retorna-sku/{serial}','ProdutoController@busca_sku');

Route::any('/retorna-chip-disponivel//{pedido?}', 'Colmeia\Api\v1\PosicaoController@desalocar_pedido');
Route::any('/retorna-chip-disponivel/{serial?}/{pedido?}', 'Colmeia\Api\v1\PosicaoController@desalocar');
Route::get('/chamada-chip-colmeia-pedido/{pedido}/{sku}/{serialSaida?}', 'Colmeia\Api\v1\PosicaoController@reservar');

###############################
#novas rotas
###############################
Route::namespace('Colmeia')->prefix('/colmeia')->name('colmeia.')->group(function () {

	Route::prefix('/api')->namespace('Api')->name('api.')->group(function () {

		// #Versão 1
		// Route::namespace('/v1')->prefix('v1')->group(function () {

		// 	Route::get('/seleciona-chip/{pedido}/{sku}/{serialSaida?}', 'PosicaoController@reservar');

		// });

		Route::prefix('v2')->namespace('v2')->name('v2.')->group(function () {

			Route::any('check-serial/{serial?}', 'ProdutoController@check_serial')->name('checkserial');
			Route::any('pode-alocar/{serial}', 'ProdutoController@pode_alocar')->name('podealocar');
			
			Route::any('produto-detalhe', 'ProdutoController@detalhe')->name('produto.detalhe');
			#Route::post('produto-detalhe', 'ProdutoController@detalhe')->name('produto.detalhe');
			
			
		});

		Route::prefix('v3')->namespace('v3')->name('v3.')->group(function () {

			Route::post('get-jundsoft-data', 'JundController@get_data')->name('jundsoft.getdata');
			Route::post('get-jundsoft-data-batch', 'JundController@get_data_batch')->name('jundsoft.getdata');

		});
	
	});

	Route::middleware('permissao:colmeia|colmeia.serial')->group(function () {

		Route::any('/config', 'ConfigController@geral')->name('config');
		Route::any('gerador-regex-sku', 'SkuController@gerador_regex')->name('sku.regex');

		Route::prefix('/magento')->name('magento.')->group(function () {

			Route::any('/atualizar-pedidos', 'MagentoController@atualizar_pedidos')->name('pedido.atualizar');
		
		});

		Route::prefix('/migracao')->name('migracao.')->group(function () {

			Route::any('/', 'MigracaoController');
			Route::any('/operadoras', 'MigracaoController@operadoras');

		});

		#armário
		Route::prefix('/armario')->name('armario.')->group(function () {

			Route::any('lista', 'ArmarioController@lista')->name('lista');
			Route::any('adicionar', 'ArmarioController@editar')->name('adicionar');
			Route::any('editar/{id?}', 'ArmarioController@editar')->name('editar');
			Route::any('desalocar/{id?}', 'ArmarioController@desalocar')->name('desalocar');

		});

		Route::any('/jundsoft', 'JundsoftController@listar')->name('jundsoft.listar');
		Route::any('/jundsoft/novo', 'JundsoftController@editar')->name('jundsoft.novo');
		Route::any('/jundsoft/editar/{id}', 'JundsoftController@editar')->name('jundsoft.editar');
		Route::any('/jundsoft/deletar/{id}', 'JundsoftController@deletar')->name('jundsoft.deletar');

		#Nota Fiscal
		Route::prefix('/nfe')->name('nfe.')->group(function () {

			Route::any('importar', 'NfeController@importar')->name('importar');
			Route::any('listar/{id}', 'NfeController@listar_detalhe_id')->name('listardetalhe');
			Route::any('buscar/{codigo}', 'NfeController@listar_detalhe_codigo')->name('buscardetalhe');
			Route::any('buscar-form', 'NfeController@listar_detalhe_buscar')->name('buscar');
			Route::any('produto/{nota}/{detalhe}', 'NfeController@produto')->name('produto');
			Route::any('pendente', 'NfeController@pendente')->name('pendente');

		});

		#Chip
		Route::prefix('/produto')->name('produto.')->group(function () {

			Route::any('/entrada/{nota}/{detalhe}', 'ProdutoController@entrada')->name('entrada');
			Route::any('/entrada-lote/{nota}', 'ProdutoController@entrada_lote')->name('entrada_lote');
			Route::any('/entrada-lote-validar/{entrada}', 'ProdutoController@entrada_lote_validar')->name('entrada_lote_validar');
			Route::any('/alocar/{seriais?}', 'ProdutoController@chip_alocar')->name('chip.alocar');
			Route::any('/desalocar', 'ProdutoController@chip_desalocar')->name('chip.desalocar');
			Route::any('/etiquetas/{id}', 'ProdutoController@etiquetas')->name('etiquetas');
			Route::any('/alocacao/{id}', 'ProdutoController@alocacao')->name('alocacao');
			Route::any('/devolucao/{id}', 'ProdutoController@devolucao')->name('devolucao');
			Route::any('/lista-descarte/{data}', 'ProdutoController@lista_descarte')->name('lista.descarte');

			Route::any('/descartar/{id}', 'ProdutoController@descartar')->middleware('permissao:colmeia.admin')->name('descartar');
			Route::any('/desalocar/{id}', 'ProdutoController@desalocar')->middleware('permissao:colmeia.admin')->name('desalocar');

		});

		#Relatórios
		Route::prefix('/relatorio')->name('relatorio.')->group(function () {

			Route::get('nfe-lista/{pagina?}', 'RelatorioController@nfe_lista')->name('nfe.lista');

			Route::get('produtos-lista/{pagina?}', 'RelatorioController@produtos_lista')->name('produto.lista');
			Route::get('produtos-devolucao/{pagina?}', 'RelatorioController@produtos_devolucao')->name('produto.devolucao');
			Route::get('produtos-seriais/{pagina?}', 'RelatorioController@produtos_seriais')->name('produto.seriais');
			
			Route::get('posicoes-lista/{pagina?}', 'RelatorioController@posicoes_lista')->name('posicoes.lista');

			Route::get('alocacoes-lista/{pagina?}', 'RelatorioController@alocacoes_lista')->name('alocacao.lista');

			Route::get('movimentacoes', 'RelatorioController@movimentacoes')->name('movimentacoes');
			
			Route::get('entrada-lote', 'RelatorioController@entrada_lote')->name('entrada_lote');
			
			Route::get('magento-produtos', 'RelatorioController@magento_produtos')->name('magento.produtos');

		});

	});
	
});

###############################
#rotas antigas
###############################
// Route::any('/busca-estoque-geral/{pagina?}', 'Colmeia\RelatorioController@estoque_geral');
// Route::any('/{variante}-nota-fiscal-{tipo}/{pagina?}', function ($variante, $tipo, $pagina = 1) {return Redirect::to('/colmeia/relatorio/nota-fiscal-' . $tipo . '/' . $pagina, 301);})->where(['variante' => '[busca|relatorio]+','tipo'=>'[entrada|saida]+']);
// Route::any('/{variante}-nota-fiscal-{tipo}/{nota}/{pagina}', function ($variante, $tipo, $nota = null, $pagina = 1) {return Redirect::to('/colmeia/relatorio/nota-fiscal-' . $tipo . '/' . $pagina . '/' . $nota, 301);})->where(['variante' => '[busca|relatorio]+','tipo'=>'[entrada|saida]+']);
// Route::any('/detalhe-nota-fiscal-{tipo}/{nota}', function ($tipo, $nota) {return Redirect::to('/colmeia/relatorio/nota-fiscal-detalhe/' . $tipo . '/' . $nota, 301);})->where(['tipo'=>'[entrada|saida]+']);
// Route::any('/busca-estoque-custo-medio/{pagina}', function ($pagina) {return Redirect::to('/colmeia/relatorio/custo-medio/' . $pagina, 301);});
// Route::any('/busca-estoque-cobertura/{pagina}', function ($pagina) {return Redirect::to('/colmeia/relatorio/estoque-cobertura/' . $pagina, 301);});
// Route::any('/busca-estoque-inventario/{pagina}', function ($pagina) {return Redirect::to('/colmeia/relatorio/estoque-inventario/' . $pagina, 301);});
// Route::any('/relatorio/chips-disponiveis', function () {return Redirect::to('/colmeia/relatorio/estoque-inventario/', 301);});
// Route::any('/estoque-entrada-parcial', function () {return Redirect::to('/colmeia/estoque/entrada-parcial/', 301);});
// Route::any('/chamada-chip-colmeia-pedido/{pedido}/{sku}/{serialSaida?}', 'Colmeia\api\v1\ChipController@seleciona_chip_para_sku');
// Route::any('/lista-nota-entrada-pedente', 'Colmeia\EstoqueController@nota_pendente');
// Route::any('/lista-devolucao/{pagina?}', 'Colmeia\RelatorioController@devolucao_lista');
// Route::any('/retorna-devolucao-disponivel/{codigoEstoque}','Colmeia\EstoqueController@disponibilizar');
// Route::any('/importa-xml-nota-devolucao','Colmeia\EstoqueController@devolucao_venda');

Route::fallback(function () {

	if (!Auth::check())
		return Redirect::route('login');

	return Redirect::route('home');
});