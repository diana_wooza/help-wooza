<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
// 	return $request->user();
// });
/**
 * Alarmística
 */

Route::namespace('Alarmistica')->prefix('/alarmistica')->middleware('permissao:alarmistica')->name('alarmistica.')->group(function () {

	Route::prefix('/api')->name('api.')->group(function () {

		Route::post('exibeIndicadores', 'ApiController@exibeIndicadores')->name('exibeIndicadores');
		Route::post('selectFilho', 'ApiController@selectFilho')->name('selectFilho');
		Route::post('selectFilhoApi', 'ApiController@selectFilhoApi')->name('selectFilhoApi');
		Route::post('listaLogApisGrafico', 'ApiController@listaLogApisGrafico')->name('listaLogApisGrafico');
		Route::any('exportarDadosLogs', 'ApiController@exportarDadosLogs')->name('exportarDadosLogs');
	});
	
	Route::prefix('/configuracao')->name('configuracao.')->group(function () {
		
		Route::post('selectFilho', 'ConfiguracaoController@selectFilho')->name('selectFilho');
		Route::post('selectFilhoApi', 'ConfiguracaoController@selectFilhoApi')->name('selectFilhoApi');
		Route::post('getChannel', 'ConfiguracaoController@getChannel')->name('getChannel');
		Route::post('cadastrar', 'ConfiguracaoController@cadastrar')->name('cadastrar');
		Route::post('listaPessoasSms', 'ConfiguracaoController@listaPessoasSms')->name('listaPessoasSms');

    });
});


Route::namespace('Indicadores')->prefix('/indicadores')->middleware('permissao:indicadores')->name('indicadores.')->group(function () {
	Route::prefix('/graficosindicadores')->name('graficosindicadores.')->group(function(){
		route::post('getDadosGraficoPrazoAtivacao', 'GraficosIndicadoresController@getDadosGraficoPrazoAtivacao')->name('getDadosGraficoPrazoAtivacao');
		route::post('getDadosGraficoTaxaAtivacao', 'GraficosIndicadoresController@getDadosGraficoTaxaAtivacao')->name('getDadosGraficoTaxaAtivacao');
		route::post('getDadosGraficoMeta', 'GraficosIndicadoresController@getDadosGraficoMeta')->name('getDadosGraficoMeta');
		route::post('cadastrarMetas', 'GraficosIndicadoresController@cadastrarMetas')->name('cadastrarMetas');
	});

});