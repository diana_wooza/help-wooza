<?php

return [
	'autoconnect'	=> env('LDAP_AUTOCONNECT', false),
	'server'		=> env('LDAP_SERVER', 'virgo.cd.com'), #10.17.3.20
	#'server'		=> env('LDAP_SERVER', 'aries.cd.com'), #10.17.3.45,
	'domain'		=> env('LDAP_DOMAIN', '@cd.com'),
	'port'			=> env('LDAP_PORT', 389),
	#'search'		=> env('LDAP_SEARCH', 'ou=usuarios e grupos adm e de servico,dc=cd,dc=com'),
	'search'		=> env('LDAP_SEARCH', 'OU=Usuarios,OU=Contas e Grupos de Servicos,DC=cd,DC=com'),
	'timeout'		=> env('LDAP_TIMEOUT', 5),
];