<?php

return [
	'cubo'			=> [
		'host'				=> env('WOOZA_CUBO_URL', 'https://gtw.celulardireto.com.br'),
		'key'				=> env('WOOZA_CUBO_KEY', 'c325452a3cf7473e85d375faca1ee812'),
		'key-elegibilidade'	=> env('WOOZA_CUBO_KEY', 'feab171b583147c0872aff582c32769f'),
	],
	'tim'			=> [
		'host'			=> env('TIM_URL_INTERNA', 'https://oagcorpi.internal.timbrasil.com.br:11106'),
	],
];