<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Default Database Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the database connections below you wish
	| to use as your default connection for all database work. Of course
	| you may use many connections at once using the Database library.
	|
	*/

	'default' => env('DB_CONNECTION', 'hub'),

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => [

		'hub'						=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_HUB_HOST', '10.17.3.89'),
			'port'				=> env('CONNECTION_HUB_PORT', '3306'),
			'database'			=> env('CONNECTION_HUB_DATABASE', 'wz_hub'),
			'username'			=> env('CONNECTION_HUB_USERNAME', 'user_wz_hub'),
			'password'			=> env('CONNECTION_HUB_PASSWORD', 'Qr2@Sd9.Wb'),
			'unix_socket'		=> env('CONNECTION_HUB_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> true,
			'engine'			=> null,
			'timezone'			=> env('CONNECTION_HUB_TIMEZONE', config('app.timezone')),
		],

		'alarmistica'				=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_ALARMISTICA_HOST', '10.17.4.4'),
			'port'				=> env('CONNECTION_ALARMISTICA_PORT', '3306'),
			'database'			=> env('CONNECTION_ALARMISTICA_DATABASE', 'alarmistica'),
			'username'			=> env('CONNECTION_ALARMISTICA_USERNAME', 'hub'),
			'password'			=> env('CONNECTION_ALARMISTICA_PASSWORD', 'hub'),
			'unix_socket'		=> env('CONNECTION_ALARMISTICA_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> true,
			'engine'			=> null,
			// 'timezone'			=> env('CONNECTION_ALARMISTICA_TIMEZONE', config('app.timezone')),
		],

		'alarmistica_mongo'			=> [
			'driver'					=> 'mongodb',
			'host'						=> env('CONNECTION_ALARMISTICA_MONGO_HOST', '52.200.212.28'),
			'port'						=> env('CONNECTION_ALARMISTICA_MONGO_PORT', '27017'),
			'database'					=> env('CONNECTION_ALARMISTICA_MONGO_DATABASE', 'wz_magento_log'),
			'username'					=> env('CONNECTION_ALARMISTICA_MONGO_USERNAME', 'user_wz_magento_log'),
			'password'					=> env('CONNECTION_ALARMISTICA_MONGO_PASSWORD', 'zGJ27tLxEiAYIaQPf4sa'),
			'options'					=> [
				// sets the authentication database required by mongo 3
				'database'					=> env('CONNECTION_ALARMISTICA_MONGO_OPTION_DATABASE', 'wz_magento_log'),
				#'replicaSet'				=> 'replicaSetName'
			]
		],

		'colmeia'					=> [
			'driver'					=> 'mysql',
			'host'						=> env('CONNECTION_COLMEIA_HOST', '10.17.3.89'),
			'port'						=> env('CONNECTION_COLMEIA_PORT', '3306'),
			'database'					=> env('CONNECTION_COLMEIA_DATABASE', 'wz_hub_comeia'),
			'username'					=> env('CONNECTION_COLMEIA_USERNAME', 'user_wz_hub'),
			'password'					=> env('CONNECTION_COLMEIA_PASSWORD', 'Qr2@Sd9.Wb'),
			'unix_socket'				=> env('CONNECTION_COLMEIA_SOCKET', ''),
			'charset'					=> env('CONNECTION_COLMEIA_CHARSET', 'utf8'),
			'collation'					=> env('CONNECTION_COLMEIA_COLLATION', 'utf8_bin'),
			'prefix'					=> '',
			'prefix_indexes'			=> true,
			'strict'					=> true,
			'engine'					=> null,
			'timezone'					=> env('CONNECTION_COLMEIA_TIMEZONE', config('app.timezone')),
		],

		'posvenda'					=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_POSVENDA_HOST', '10.17.4.4'),
			'port'				=> env('CONNECTION_POSVENDA_PORT', '3306'),
			'database'			=> env('CONNECTION_POSVENDA_DATABASE', 'posvenda'),
			'username'			=> env('CONNECTION_POSVENDA_USERNAME', 'hub'),
			'password'			=> env('CONNECTION_POSVENDA_PASSWORD', 'hub'),
			'unix_socket'		=> env('CONNECTION_POSVENDA_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> true,
			'engine'			=> null,
			// 'timezone'			=> env('CONNECTION_POSVENDA_TIMEZONE', config('app.timezone')),
		],

		'reaproveitamento'			=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_REAPROVEITAMENTO_HOST', '10.17.4.4'),
			'port'				=> env('CONNECTION_REAPROVEITAMENTO_PORT', '3306'),
			'database'			=> env('CONNECTION_REAPROVEITAMENTO_DATABASE', 'reaproveitamento'),
			'username'			=> env('CONNECTION_REAPROVEITAMENTO_USERNAME', 'hub'),
			'password'			=> env('CONNECTION_REAPROVEITAMENTO_PASSWORD', 'hub'),
			'unix_socket'		=> env('CONNECTION_REAPROVEITAMENTO_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> true,
			'engine'			=> null,
			// 'timezone'			=> env('CONNECTION_REAPROVEITAMENTO_TIMEZONE', config('app.timezone')),
		],

		'colmeia_legacy'			=> [
			'driver'					=> 'sqlsrv',
			'host'						=> env('CONNECTION_COLMEIA_LEGACY_HOST', '10.46.0.46'),
			'port'						=> env('CONNECTION_COLMEIA_LEGACY_PORT', '1433'),
			'database'					=> env('CONNECTION_COLMEIA_LEGACY_DATABASE', 'cd_gerenciadeserial'),
			'username'					=> env('CONNECTION_COLMEIA_LEGACY_USERNAME', 'cd_gerenciadeserial'),
			'password'					=> env('CONNECTION_COLMEIA_LEGACY_PASSWORD', 'jlLMX6nZ'),
			'charset'					=> 'utf8',
			'prefix'					=> '',
			'prefix_indexes'			=> true,
		],

		'magento'					=> [
			'driver'			=> 'mysql',
			'read'				=> [
				'host'				=> env('CONNECTION_MAGENTO_HOST_READER', 'magento-wooza-ro.cluster-custom-cxzd4skzxhyr.us-east-1.rds.amazonaws.com'),
			],
			'write'				=> [
				'host'				=> env('CONNECTION_MAGENTO_HOST_WRITE', 'magento-cluster.cluster-cxzd4skzxhyr.us-east-1.rds.amazonaws.com'),
			],
			'sticky'			=> true,
			'port'				=> env('CONNECTION_MAGENTO_PORT', '3306'),
			'database'			=> env('CONNECTION_MAGENTO_DATABASE', 'celulardireto_magento_prod'),
			'username'			=> env('CONNECTION_MAGENTO_USERNAME', 'cdiretoconsumer'),
			'password'			=> env('CONNECTION_MAGENTO_PASSWORD', 'O2a6UVldnEFb7f5yjSH9'),
			'unix_socket'		=> env('CONNECTION_MAGENTO_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> false,
			'engine'			=> null,
			'timezone'			=> env('CONNECTION_MAGENTO_TIMEZONE', config('app.timezone')),
		],

		'magento_qa'				=> [
			'driver'					=> 'mysql',
			'host'						=> env('CONNECTION_MAGENTO_CONSUMER_HOST_WRITE', 'magento-homolog-cluster.cluster-cxzd4skzxhyr.us-east-1.rds.amazonaws.com'),
			'port'						=> env('CONNECTION_MAGENTO_CONSUMER_PORT_WRITE', '3306'),
			'database'					=> env('CONNECTION_MAGENTO_CONSUMER_DATABASE', 'magento_gestao'),
			'username'					=> env('CONNECTION_MAGENTO_CONSUMER_USERNAME', 'cdireto'),
			'password'					=> env('CONNECTION_MAGENTO_CONSUMER_PASSWORD', 'Adm4cdireto'),
			'unix_socket'				=> env('CONNECTION_MAGENTO_CONSUMER_SOCKET', ''),
			'charset'					=> env('CONNECTION_MAGENTO_CONSUMER_CHARSET','utf8'),
			'collation'					=> env('CONNECTION_MAGENTO_CONSUMER_COLLATION','utf8_bin'),
			'prefix'					=> '',
			'prefix_indexes'			=> true,
			'strict'					=> false,
			'engine'					=> null,
			// 'timezone'			=> env('CONNECTION_MAGENTOQA_TIMEZONE', config('app.timezone')),
		],

		'magento_corp'				=> [
			'driver'					=> 'mysql',
			'host'						=> env('CONNECTION_MAGENTO_CORP_HOST', 'magento-cluster.cluster-cxzd4skzxhyr.us-east-1.rds.amazonaws.com'),
			'port'						=> env('CONNECTION_MAGENTO_CORP_PORT', '3306'),
			'database'					=> env('CONNECTION_MAGENTO_CORP_DATABASE', 'celulardireto_magento_qa01'),
			'username'					=> env('CONNECTION_MAGENTO_CORP_USERNAME', 'cdiretocorpore'),
			'password'					=> env('CONNECTION_MAGENTO_CORP_PASSWORD', 'yqzSTY4dirx7whlMnKkw'),
			'unix_socket'				=> env('CONNECTION_MAGENTO_CORP_SOCKET', ''),
			'charset'					=> env('CONNECTION_MAGENTO_CORP_CHARSET','utf8'),
			'collation'					=> env('CONNECTION_MAGENTO_CORP_COLLATION','utf8_bin'),
			'prefix'					=> '',
			'prefix_indexes'			=> true,
			'strict'					=> false,
			'engine'					=> null,
			'timezone'					=> env('CONNECTION_MAGENTOCORP_TIMEZONE', config('app.timezone')),
		],

		'magento_corp_qa'			=> [
			'driver'					=> 'mysql',
			'host'						=> env('CONNECTION_MAGENTO_CORP_QA_HOST', 'magento-homolog-cluster.cluster-cxzd4skzxhyr.us-east-1.rds.amazonaws.com'),
			'port'						=> env('CONNECTION_MAGENTO_CORP_QA_PORT', '3306'),
			'database'					=> env('CONNECTION_MAGENTO_CORP_QA_DATABASE', 'corp_magento_qa01'),
			'username'					=> env('CONNECTION_MAGENTO_CORP_QA_USERNAME', 'cdireto'),
			'password'					=> env('CONNECTION_MAGENTO_CORP_QA_PASSWORD', 'Adm4cdireto'),
			'unix_socket'				=> env('CONNECTION_MAGENTO_CORP_QA_SOCKET', ''),
			'charset'					=> env('CONNECTION_MAGENTO_CORP_QA_CHARSET','utf8'),
			'collation'					=> env('CONNECTION_MAGENTO_CORP_QA_COLLATION','utf8_bin'),
			'prefix'					=> '',
			'prefix_indexes'			=> true,
			'strict'					=> false,
			'engine'					=> null,
			// 'timezone'			=> env('CONNECTION_MAGENTOCORP_TIMEZONE', config('app.timezone')),
		],

		'contestador'				=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_CONTESTADOR_HOST', '10.17.3.89'),
			'port'				=> env('CONNECTION_CONTESTADOR_PORT', '3306'),
			'database'			=> env('CONNECTION_CONTESTADOR_DATABASE', 'wz_contestacao'),
			'username'			=> env('CONNECTION_CONTESTADOR_USERNAME', 'user_wz_hub'),
			'password'			=> env('CONNECTION_CONTESTADOR_PASSWORD', 'Qr2@Sd9.Wb'),
			'unix_socket'		=> env('CONNECTION_CONTESTADOR_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> false,
			'engine'			=> null,
			'timezone'			=> env('CONNECTION_CONTESTADOR_TIMEZONE', config('app.timezone')),
		],

		'filas'						=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_FILAS_HOST', '10.17.4.4'),
			'port'				=> env('CONNECTION_FILAS_PORT', '3306'),
			'database'			=> env('CONNECTION_FILAS_DATABASE', 'filas'),
			'username'			=> env('CONNECTION_FILAS_USERNAME', 'hub'),
			'password'			=> env('CONNECTION_FILAS_PASSWORD', 'hub'),
			'unix_socket'		=> env('CONNECTION_FILAS_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> false,
			'engine'			=> null,
			// 'timezone'			=> env('CONNECTION_FILAS_TIMEZONE', config('app.timezone')),
		],

		'lojaonline'				=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_LOJAONLINE_HOST', 'ecommerce-allied-db1.cqpou5y4xe1k.us-east-1.rds.amazonaws.com'),
			'port'				=> env('CONNECTION_LOJAONLINE_PORT', '3306'),
			'database'			=> env('CONNECTION_LOJAONLINE_DATABASE', 'desbloqueados_prod'),
			'username'			=> env('CONNECTION_LOJAONLINE_USERNAME', 'desbloq_prod'),
			'password'			=> env('CONNECTION_LOJAONLINE_PASSWORD', 'SdseE2sa!rtW'),
			'unix_socket'		=> env('CONNECTION_LOJAONLINE_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> false,
			'engine'			=> null,
			// 'timezone'			=> env('CONNECTION_LOJAONLINE_TIMEZONE', config('app.timezone')),
		],
		
		'configuracao'				=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_CONFIGURACAO_HOST', '10.17.4.4'),
			'port'				=> env('CONNECTION_CONFIGURACAO_PORT', '3306'),
			'database'			=> env('CONNECTION_CONFIGURACAO_DATABASE', 'configuracao'),
			'username'			=> env('CONNECTION_CONFIGURACAO_USERNAME', 'hub'),
			'password'			=> env('CONNECTION_CONFIGURACAO_PASSWORD', 'hub'),
			'unix_socket'		=> env('CONNECTION_CONFIGURACAO_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> false,
			'engine'			=> null,
			// 'timezone'			=> env('CONNECTION_CONFIGURACAO_TIMEZONE', config('app.timezone')),
		],

		'crons'				=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_CRON_HOST', '10.17.4.4'),
			'port'				=> env('CONNECTION_CRON_PORT', '3306'),
			'database'			=> env('CONNECTION_CRON_DATABASE', 'cron'),
			'username'			=> env('CONNECTION_CRON_USERNAME', 'hub'),
			'password'			=> env('CONNECTION_CRON_PASSWORD', 'hub'),
			'unix_socket'		=> env('CONNECTION_CRON_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> false,
			'engine'			=> null,
			// 'timezone'			=> env('CONNECTION_CONFIGURACAO_TIMEZONE', config('app.timezone')),
		],
		
		'portal'				=> [
			'driver'			=> 'mysql',
			'host'				=> env('CONNECTION_PORTAL_HOST', 'localhost'),
			'port'				=> env('CONNECTION_PORTAL_PORT', '3306'),
			'database'			=> env('CONNECTION_PORTAL_DATABASE', 'portal'),
			'username'			=> env('CONNECTION_PORTAL_USERNAME', 'root'),
			'password'			=> env('CONNECTION_PORTAL_PASSWORD', ''),
			'unix_socket'		=> env('CONNECTION_PORTAL_SOCKET', ''),
			'charset'			=> 'utf8mb4',
			'collation'			=> 'utf8mb4_unicode_ci',
			'prefix'			=> '',
			'prefix_indexes'	=> true,
			'strict'			=> false,
			'engine'			=> null,
			// 'timezone'			=> env('CONNECTION_CONFIGURACAO_TIMEZONE', config('app.timezone')),
		],

	],

	/*
	|--------------------------------------------------------------------------
	| Migration Repository Table
	|--------------------------------------------------------------------------
	|
	| This table keeps track of all the migrations that have already run for
	| your application. Using this information, we can determine which of
	| the migrations on disk haven't actually been run in the database.
	|
	*/

	// 'migrations' => 'migrations',

	/*
	|--------------------------------------------------------------------------
	| Redis Databases
	|--------------------------------------------------------------------------
	|
	| Redis is an open source, fast, and advanced key-value store that also
	| provides a richer body of commands than a typical key-value system
	| such as APC or Memcached. Laravel makes it easy to dig right in.
	|
	*/

	'redis' => [

		'client'				=> 'predis',

		// 'default'				=> [
		// 	'host'					=> env('REDIS_HOST', '127.0.0.1'),
		// 	'password'				=> env('REDIS_PASSWORD', null),
		// 	'port'					=> env('REDIS_PORT', 6379),
		// 	'database'				=> env('REDIS_DB', 0),
		// ],

		// 'cache'					=> [
		// 	'host'					=> env('REDIS_HOST', '127.0.0.1'),
		// 	'password'				=> env('REDIS_PASSWORD', null),
		// 	'port'					=> env('REDIS_PORT', 6379),
		// 	'database'				=> env('REDIS_CACHE_DB', 1),
		// ],

		'magento_consumer_session'	=> [
			'host'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_HOST', 'redis-magento.qceezv.0001.use1.cache.amazonaws.com'),
			#'password'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PASSWORD', null),
			'port'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PORT', 6379),
			'database'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_DB', 0),
		],

		'magento_consumer_cache'	=> [
			'host'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_HOST', 'redis-magento.qceezv.0001.use1.cache.amazonaws.com'),
			#'password'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PASSWORD', null),
			'port'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PORT', 6379),
			'database'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_DB', 1),
		],

		'magento_consumer_page'	=> [
			'host'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_HOST', 'redis-magento.qceezv.0001.use1.cache.amazonaws.com'),
			#'password'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PASSWORD', null),
			'port'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PORT', 6379),
			'database'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_DB', 2),
		],

		'magento_consumer_qa_session'	=> [
			'host'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_HOST', 'redis-magento-hmg2.qceezv.0001.use1.cache.amazonaws.com'),
			#'password'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PASSWORD', null),
			'port'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PORT', 6379),
			'database'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_DB', 0),
		],

		'magento_consumer_qa_cache'		=> [
			'host'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_HOST', 'redis-magento-hmg2.qceezv.0001.use1.cache.amazonaws.com'),
			#'password'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PASSWORD', null),
			'port'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PORT', 6379),
			'database'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_DB', 1),
		],

		'magento_consumer_qa_page'		=> [
			'host'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_HOST', 'redis-magento-hmg2.qceezv.0001.use1.cache.amazonaws.com'),
			#'password'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PASSWORD', null),
			'port'							=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_PORT', 6379),
			'database'						=> env('REDIS_MAGENTO_CONSUMER_QA_CACHE_DB', 2),
		],

		'contestador'					=> [
			'host'							=> env('REDIS_CONTESTADOR_HOST', 'redis-magento-hmg2.qceezv.0001.use1.cache.amazonaws.com'),
			#'password'						=> env('REDIS_CONTESTADOR_PASSWORD', null),
			'port'							=> env('REDIS_CONTESTADOR_PORT', 6379),
			'database'						=> env('REDIS_CONTESTADOR_DB', 3),
		],

	],

];
