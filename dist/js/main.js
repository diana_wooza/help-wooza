var apis = { 
    
    /**
     * Função consulta via ajax todas as APis dos documents da log_tim
     * @returns
     */
    retornaTodasApis: function() {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Se tudo ocorrer bem na cosulta envio o resultado para verificar os status por api*/
            if (this.readyState == 4 && this.status == 200) {
                var resposta = this.responseText;
                respostaApi = apis.consultaStatusPorApi(resposta);
             
            }
        };
        xhttp.open("POST", "apiLog.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("&log=retorna_apis");
    },
    
    /** 
     * Funcão consulta via ajax as apis com os status 400 ou 200 e 
     * @returns 
     */
    consultaStatusPorApi : function() {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Se tudo ocorrer bem na cosulta envio o resultado para verificar os status por api*/
            if (this.readyState == 4 && this.status == 200) {
                var resposta = this.responseText,
                    obj       = JSON.parse(resposta);
                //console.log(resposta);
                //console.log(obj);
                    document.getElementById("estavel").innerHTML = obj.qtd_api_estavel;
                    document.getElementById("lg_estavel").innerHTML = obj.ids_logs_estaveis;
                    document.getElementById("instavel").innerHTML = obj.qtd_api_instavel;
                    document.getElementById("lg_instavel").innerHTML = obj.ids_logs_instaveis;
                    document.getElementById("oscilando").innerHTML = obj.qtd_api_oscilando;
                    document.getElementById("lg_oscilando").innerHTML = obj.ids_logs_oscilando;
            } else {
                return "false";
            }
        };
        xhttp.open("POST", "apiLog.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xhttp.send("&log=consulta_apis&apis=envio_contingencia_tim");
        xhttp.send("&log=consulta_apis");
    }

    // listaApi : function(ids) {
    //     window.location.href = "listaApi.php?ids="+ids;
        // xhttp = new XMLHttpRequest();
        // xhttp.onreadystatechange = function () {
        // };
        // xhttp.open("POST", "listaApi.php", true);
        // xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xhttp.send("&log=consulta_apis&apis=envio_contingencia_tim");
        // xhttp.send("&ids_log="+ids);
    // }
            
};

var listaInfoApis = document.getElementById('estavelInfo');

listaInfoApis.addEventListener('click', function(){
    // alert("cheguei");
    var idsLog = document.getElementById("lg_estavel").innerHTML;
    // apis.listaApi(idsLog);
    window.location.href = "listaApi.php?ids="+idsLog;
});

