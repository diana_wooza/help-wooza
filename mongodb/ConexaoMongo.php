<?php

namespace mongodb;

use \Monolog\Handler\Mongo;
// use Symfony\Component\HttpKernel\Client;

class ConexaoMongo
{

    public function conecta()
    {
        $conexao = false;
        $mongo = false;
        try {

            // mongodb://magento:KbjVf3gTbJE0za1N4fco@52.200.212.28:27017/cd_log_prod';
            $mongo = new \MongoDB\Driver\Manager('mongodb://user_wz_magento_log:zGJ27tLxEiAYIaQPf4sa@35.170.151.116:27017/wz_magento_log');
            $conexao = true;

        } catch (\MongoDB\Driver\Exception\Exception $e) {
            $filename = basename(__FILE__); 
        
            echo 'Erro no arquivo $filename.\n';
        
            echo 'Exception:', $e->getMessage(), '\n';
        
            echo 'Arquivo:', $e->getFile(), '\n';
        
            echo 'Linha:', $e->getLine(), '\n';    
        
        }

        if($conexao == true){
            return $mongo;
        }

    }   

    public function retornaQuery($filter, $option)
    {
        $query = new \MongoDB\Driver\Query($filter, $option);
        return $query;
    }
}