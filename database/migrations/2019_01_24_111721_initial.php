<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		// $this->create_database_hub();
		// $this->create_database_alarmistica();
		// $this->create_database_colmeia();
		// $this->insert_data_colmeia();
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		#nada a fazer, essa é a migração inicial
	}

	/**
	 * Create database para as tabelas do HUB
	 * @method create_database_hub
	 * @return void
	 */
	private function create_database_hub ()
	{
		Schema::connection('migration_hub')->create('perfil', function (Blueprint $table) {
			
			// $table->increments('id');
			// $table->string('nome', 100)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->json('permissoes')->nullable();
			
		});
		
		Schema::connection('migration_hub')->create('usuario', function (Blueprint $table) {

			// $table->increments('id');
			// $table->string('nome', 100)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->string('login', 100)->collation('utf8mb4_unicode_ci')->nullable(false)->unique();
			// $table->string('email', 150)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->string('telefone', 12)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->integer('perfil')->nullable(false)->default(1);
			// $table->string('token', 32)->collation('utf8mb4_unicode_ci')->nullable()->unique();
			// $table->string('password', 60)->collation('utf8mb4_unicode_ci')->nullable();

			// $table->foreign('perfil')->references('id')->on('perfil');
			
		});
	}

	/**
	 * Create database para as tabelas do Alarmística
	 * @method create_database_alarmistica
	 * @return void
	 */
	private function create_database_alarmistica ()
	{
		Schema::connection('migration_alarmistica')->create('api', function (Blueprint $table) {
			
			// $table->increments('id');
			// $table->tinyInteger('status')->default(1)->comment('-1:Inativo, 1: Ativo');
			// $table->tinyInteger('tipo')->default(1)->comment('1:mongo, 2:http')->nullable(false);
			// $table->string('nome', 100)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->json('sms')->nullable();

		});

		Schema::connection('migration_alarmistica')->create('api_mongo', function (Blueprint $table) {

			// $table->increments('id');
			// $table->string('connection', 100)->collation('utf8mb4_unicode_ci')->nullable(false);
			// $table->string('collection', 100)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->string('consulta', 100)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->decimal('range_negativo', 10, 9)->default('0.200000000');
			// $table->decimal('range_positivo', 10, 9)->default('0.500000000');

			// $table->foreign('id')->references('id')->on('api');
			
		});
		
		Schema::connection('migration_alarmistica')->create('api_http', function (Blueprint $table) {
			
			// $table->increments('id');
			// $table->string('host', 200)->collation('utf8mb4_unicode_ci')->nullable();

			// $table->foreign('id')->references('id')->on('api');
			
		});
		
		Schema::connection('migration_alarmistica')->create('log_api_mongo', function (Blueprint $table) {
			
			// $table->increments('id');
			// $table->integer('api')->nullable(false);
			// $table->decimal('percentual', 10, 9)->default('0.000000000');
			// $table->timestamp('data')->useCurrent();
			// $table->json('pedidos')->nullable();

			// $table->foreign('api')->references('id')->on('api');
			
		});
	}

	/**
	 * Create database para as tabelas da Colmeia
	 * @method create_database_colmeia
	 * @return void
	 */
	private function create_database_colmeia ()
	{
		Schema::connection('migration_colmeia')->create('armario', function (Blueprint $table) {

			// $table->increments('id');
			// $table->tinyInteger('status')->default(1)->comment('-1:Inativo, 1:Ativo, 2:Ativo mas não Alocando');
			// $table->tinyInteger('prioridade')->default(0);
			// $table->tinyInteger('sobressalente')->default(0)->comment('0:Não, 1:Sim');
			// $table->string('nome', 50)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->unsignedInteger('operadora')->nullable(false);

			// $table->index(['operadora']);
			// $table->foreign('operadora')->references('id')->on('operadora');

		});

		Schema::connection('migration_colmeia')->create('produto', function (Blueprint $table) {

			// $table->increments('id');
			// $table->tinyInteger('tipo')->comment('1:Sim Card, 2:Telefone Celular,3:Telefone Fixo, 4:Tablet, 5:Roteador');
			// $table->tinyInteger('status')->comment('1:Disponivel, 2:Pré Alocação,3:Alocado, 4:Reservado, 5:Retirado, 6:Descartado');
			// $table->unsignedInteger('nfe')->nullable(false);
			// $table->tinyInteger('nfe_detalhe')->default(0);
			// $table->unsignedInteger('operadora')->nullable(true);
			// $table->string('serial', 20)->collation('utf8mb4_unicode_ci')->nullable();
			// $table->string('sku', 50)->collation('utf8mb4_unicode_ci')->nullable();

			// $table->index(['nfe','operadora']);
			// $table->foreign('nfe')->references('id')->on('nfe');
			// $table->foreign('operadora')->references('id')->on('operadora');

		});

		Schema::connection('migration_colmeia')->create('config', function (Blueprint $table) {

			// $table->string('chave', 60)->nullable(false);
			// $table->tinyInteger('tipo')->default(1)->comment('1:Texto Símples, 2:Numero Inteiro, 3:Lista de Valores, 4:Tags (strings livres), 5:Boleano');
			// $table->json('valor');
			// $table->text('descricao');

			// $table->primary('chave');

		});

		Schema::connection('migration_colmeia')->create('nfe', function (Blueprint $table) {

			// $table->increments('id');
			// $table->tinyInteger('tipo')->comment('1:Entrada, 2:Saída');
			// $table->json('nota');

		});

		Schema::connection('migration_colmeia')->create('operadora', function (Blueprint $table) {

			// $table->increments('id');

			// `status` TINYINT(1) NULL DEFAULT 1 COMMENT '-1: Inativo, 1:Ativo',
			// `nome` VARCHAR(50) NULL DEFAULT NULL,
			// `codigo` VARCHAR(3) NULL DEFAULT NULL,
			// PRIMARY KEY (`id`))
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('armario_posicao', function (Blueprint $table) {

			// $table->increments('id');

			// `armario` INT(10) UNSIGNED NOT NULL,
			// `status` TINYINT(2) NULL DEFAULT 1 COMMENT '1:Vaga, 2:Disponível, 3:Reservada, 4:Descarte',
			// `gaveta` VARCHAR(5) NOT NULL,
			// `posicao` INT(5) NOT NULL,
			// `produto` INT(10) UNSIGNED NULL DEFAULT NULL,
			// `serial` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Dado redundante do serial para o chip alocado nesta posição. O objetivo é facilitar a query',
			// `sku` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Dado redundante do sku para o chip alocado nesta posição. O objetivo é facilitar a query',
			// `pedido` VARCHAR(15) NULL DEFAULT NULL,
			// PRIMARY KEY (`id`),
			// INDEX `fk_armario_idx` (`armario` ASC) VISIBLE,
			// INDEX `fk_chip_idx` (`produto` ASC) VISIBLE,
			// INDEX `idx_posicao_serial` (`serial` ASC) INVISIBLE,
			// INDEX `idx_posicao_sku` (`sku` ASC) VISIBLE,
			// CONSTRAINT `fk_armarioposicao_armario`
			// 	FOREIGN KEY (`armario`)
			// 	REFERENCES `colmeia_2`.`armario` (`id`)
			// 	ON DELETE NO ACTION
			// 	ON UPDATE NO ACTION,
			// CONSTRAINT `fk_armarioposicao_chip`
			// 	FOREIGN KEY (`produto`)
			// 	REFERENCES `colmeia_2`.`produto` (`id`)
			// 	ON DELETE NO ACTION
			// 	ON UPDATE NO ACTION)
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_alocacao', function (Blueprint $table) {

			// $table->increments('id');

			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `usuario` INT(11) NOT NULL,
			// `alocacao` INT(10) UNSIGNED NOT NULL,
			// `informacao` JSON NULL,
			// PRIMARY KEY (`id`))
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_devolucao', function (Blueprint $table) {

			// $table->increments('id');
			
			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `usuario` INT(11) NOT NULL,
			// `nfe` INT(10) UNSIGNED NOT NULL,
			// `informacao` JSON NULL,
			// PRIMARY KEY (`id`),
			// INDEX `fk_nfe_idx` (`nfe` ASC) VISIBLE,
			// CONSTRAINT `fk_logdevolucao_nfe`
			// 	FOREIGN KEY (`nfe`)
			// 	REFERENCES `colmeia_2`.`nfe` (`id`)
			// 	ON DELETE NO ACTION
			// 	ON UPDATE NO ACTION)
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_armario', function (Blueprint $table) {

			// $table->increments('id');

			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `usuario` INT(11) NOT NULL,
			// `armario` INT(10) UNSIGNED NOT NULL,
			// `informacao` JSON NULL,
			// PRIMARY KEY (`id`),
			// INDEX `fk_armario_idx` (`armario` ASC) VISIBLE,
			// CONSTRAINT `fk_logarmario_armario`
			// 	FOREIGN KEY (`armario`)
			// 	REFERENCES `colmeia_2`.`armario` (`id`)
			// 	ON DELETE NO ACTION
			// 	ON UPDATE NO ACTION)
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_config', function (Blueprint $table) {

			// $table->increments('id');

			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `usuario` INT(11) NOT NULL,
			// `configuracao` VARCHAR(60) NOT NULL,
			// `informacao` JSON NULL,
			// PRIMARY KEY (`id`))
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_nfe', function (Blueprint $table) {

			// `id` INT(10) UNSIGNED NOT NULL,
			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `usuario` INT(11) NOT NULL,
			// PRIMARY KEY (`id`),
			// CONSTRAINT `fk_log_nfe`
			// 	FOREIGN KEY (`id`)
			// 	REFERENCES `colmeia_2`.`nfe` (`id`)
			// 	ON DELETE NO ACTION
			// 	ON UPDATE NO ACTION)
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_operadora', function (Blueprint $table) {

			// $table->increments('id');

			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `usuario` INT(11) NOT NULL,
			// `operadora` INT(10) UNSIGNED NULL DEFAULT NULL,
			// `informacao` JSON NULL,
			// PRIMARY KEY (`id`))
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('alocacao', function (Blueprint $table) {

			// $table->increments('id');

			// `status` TINYINT(1) NULL DEFAULT -1 COMMENT '-1: Incompleta, 1:Completa',
			// `tag` TINYINT(4) NOT NULL DEFAULT 1,
			// `produtos` JSON NULL,
			// PRIMARY KEY (`id`, `tag`))
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_nfe_detalhe', function (Blueprint $table) {

			// `id` INT(10) UNSIGNED NOT NULL,
			// `detalhe` TINYINT(5) NOT NULL,
			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `usuario` INT(11) NOT NULL,
			// `produto` JSON NULL,
			// PRIMARY KEY (`id`, `detalhe`))
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_reserva', function (Blueprint $table) {

			// $table->increments('id');

			// `tipo` TINYINT(1) NOT NULL COMMENT '1:Inclusão, 2:Exclusão',
			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `usuario` INT(11) NULL DEFAULT NULL,
			// `reserva` JSON NULL,
			// PRIMARY KEY (`id`))
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});

		Schema::connection('migration_colmeia')->create('log_produto', function (Blueprint $table) {

			// $table->increments('id');
			
			// `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			// `tipo` TINYINT(1) NOT NULL COMMENT '1:Entrada, 2:Pré Alocação, 3:Alocação, 4:Seleção para Pedido, 5:Retorno Colmeia, 6:Saída para Cliente, 7:Devolução pelo Cliente, 8:Descarte',
			// `usuario` INT(11) NULL DEFAULT NULL,
			// `produto` INT(10) UNSIGNED NOT NULL,
			// `informacao` JSON NULL,
			// PRIMARY KEY (`id`),
			// INDEX `fk_logproduto_produto_idx` (`produto` ASC) VISIBLE,
			// CONSTRAINT `fk_logproduto_produto`
			// 	FOREIGN KEY (`produto`)
			// 	REFERENCES `colmeia_2`.`produto` (`id`)
			// 	ON DELETE NO ACTION
			// 	ON UPDATE NO ACTION)
			// ENGINE = InnoDB
			// DEFAULT CHARACTER SET = utf8mb4
			// COLLATE = utf8mb4_unicode_ci;

		});
	}

	private function insert_data_colmeia ()
	{
		#Configurações
		// alocacao_limite	2	"80"	Número de chips que podem ser alocados simultaneamente em um único envio
		// alocacao_permitir_cancelar	5	false	Usuários não administradores da colmeia possuem permissão para cancelar alocações pendentes
		// alocacao_permitir_multinotas	5	true	Permite alocar chips na colmeia que entraram no estoque por notas ficasis diferentes
		// alocacao_permitir_operadora	5	false	Permite alocar chips na colmeia de mais de uma operadora de uma única vez
		// alocacao_permitir_parcial	5	true	Permite alocar chips de uma nota que não esteja com 100% dos produtos já com entrada no estoque
		// alocacao_quantidade_minima	5	true	Permite alocar menos chips do que a quantidade limite de alocações simultâneas. Caso esta opção esteja desabilitada só será possível alocar uma quantidade de chips por vez igual a quantidade limite de chips configurada para alocação.
		// email_cobertura	3	["1"]	Usuários que irão receber o email com a atualização de cobertura. <strong>É possível selecionar mais de um usuário</strong>
		// magento_produto_blacklist	4	["CGSA029QA09T", "MGTS001QA03T", "CGSA031QA09T", "CFZT004QA03C", "INSTALACAO"]	Lista de SKUs que serão ignoradas pela aplicação para dar entrada no estoque
		// ocorrencia	2	"4"	Número de vezes que um chip pode tentar ser ativado antes de ser colocado para descarte

		#Operadoras
		// 1	1	Tim	t
		// 2	1	Vivo	v
		// 3	1	Claro	c
		// 4	1	Oi	o
	}
}
