var meta =  {   // Verifica se uma determinada classe esta sendo usada
        hasClass: function(element, className) {
            return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
        },

        removeItem: function(){

            btnRemoveItem = document.querySelectorAll('.excluir');
        
            for(var e of btnRemoveItem){
                
                e.addEventListener('click', function(el){
                    // console.log(this.id);
                    itemExcluir = el.target.id;
                        // console.log(itemExcluir)
                        divFilha = document.getElementById('excluir_'+itemExcluir);
                        // console.log(divFilha)
                        divFilha.remove();
                });
            }
        },

        verificaDuplicados: function(tipos){
            return (new Set(tipos)).size !== tipos.length;
        },
        
        chamaModal: function(modal) {

            if(modal == 'confirma'){
                classe = document.getElementsByClassName('form-group');
                erro = false;
        
                for(i=0; classe.length > i; i++){
                    if(meta.hasClass(classe[i], "has-error")){
                        erro = true;
                    }
                }
        
                if(erro == false){ // se não tiver a class error chama modal
        
                    $('#bs-example-modal-sm').modal('show');
                    /** limpa para validar novamente */
                    $('#form').validator('destroy');
        
                }
    
            } else {
    
                $('#duplicados').modal('show');
                /** limpa para validar novamente */
                $('#form').validator('destroy');
            }
        }
    }

window.onload = function(){
    
    var line = 2,
        btnAddItem = document.getElementById('btn-add-item'),
        form = document.getElementById('form'),
        selectTipos = document.getElementsByName('tipos'),
        btnCancel = document.getElementById('btn-cancel'),
        btnCadastrar = document.getElementById('btn-cadastrar');
    
    function addMetricas(div, linha = null) {
        
        if(linha != null){
            line = linha;
        }

        var divLabel = document.createElement('div'); 
            divLabel.className = 'form-group';
            divLabel.innerHTML = '<label class="col-sm-2 col-xs-12 control-label ">Escolha as operadora/produtos</label>';
            newdiv = document.createElement('div');
            newdiv.id = 'excluir_'+ line +''
            newdiv.className = 'form-group';
            newdiv.innerHTML = '<label class="col-md-3 col-xs-12 control-label required" title="Tipos de segmentos.">Métrica '+line+'</label>'+
                                '<div class="col-md-3">'+
                                '<select id="tipos_'+ line +'" class="form-control select2 tipos" name="tipos[]" data-error="Por favor, informe um produto ou operadora." required>'+
                                '<option value="" selected>Selecione...</option>'+   
                                '<option value="claro">Claro</option>'+
                                '<option value="corp">Corporativo</option>'+
                                '<option value="movel">Móvel</option>'+
                                '<option value="net">Net</option>'+
                                '<option value="nextel">Nextel</option>'+
                                '<option value="office">Office</option>'+
                                '<option value="oi">Oi</option>'+
                                '<option value="sva">SVA</option>'+
                                '<option value="tim">Tim</option>'+
                                '<option value="timLive">Tim Live</option>'+
                                '<option value="vivo">Vivo</option>'+
                                '</select>'+
                                '</div>'+
                                '<div class="form-group"><div class="col-md-3"><input type="text" class="form-control" name="metrica[]" id="metrica_'+line +'" placeholder="insira o valor" data-error="Por favor, informe um valor." required></div>'+
                                '<button type="button" class="btn btn-erro tools excluir" id="'+ line +'" style="color: #dd4b39;"><i class="glyphicon glyphicon-trash"></i></button></div>';                        
        
        if(line == 1){
            // document.getElementById(div).appendChild(divLabel);
    
        }                            

        document.getElementById(div).appendChild(newdiv);
        line++;

       meta.removeItem();

    }
    
    btnAddItem.addEventListener('click', function(){
        
        $('#form').validator('destroy');
        addMetricas('campos');  
        // meta.removeItem();

    });
    
    /* Usado para data da meta */
    $('#dataMeta').datetimepicker({
        locale: 'ru',
            format: 'MM/YYYY',
            defaultDate: new Date()
    });

    // btnCadastrar.disabled = true;
    // itemSelecionado = tipos.options[tipos.selectedIndex].value;

    // if(itemSelecionado != ''){
    //     alert('oi')
    //     btnCadastrar.disabled = false;
    // }

    btnCadastrar.addEventListener('click', function(){

        $('#form').validator('validate');

        /* Verifica se existem tipos duplicados*/
        var tipos = document.querySelectorAll(".tipos"),
            arrTipos = [],
            duplicados = false;

        tipos.forEach(function(e){
            arrTipos.push(e.value);
        });

        arrTipos.filter(function(elem, pos, self) {
            verifica = self.indexOf(elem) == pos;
            if(verifica == false){
                duplicados = true;
            }

            return self.indexOf(elem) == pos;
        });
        
        if(duplicados){
            /**CHAMA MODAL DE AVISO DUPLICADOS */
            setTimeout(function(){
                meta.chamaModal('duplicados');
            },100);

        } else {
            // Chama a função em 1 segundos
            setTimeout(function(){
                meta.chamaModal('confirma');
            },100);
        }
    })
}
