var apis = { 
    
    /**
     * Função consulta as apis "negativas" no intervalo de 30 min. e preenche o grafico de linha
     * @returns 
     */
    retornaDadosGraficoApi: function() {

        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Se tudo o retorno for ok */
            if (this.readyState == 4 && this.status == 200) {
                var resposta      = this.responseText;
                    obj           = JSON.parse(resposta);
                    objeto_data   = [];
                    objeto_ykeys  = [];
                    objeto_labels = [];
                    datas         = [];
                    data_nova     = [];
                    id = [];

            // LINE CHART
            obj.forEach(function(value, index){
                datas.push(value.data_hora);

                if(datas.indexOf(value.data_hora) == '-1'){ // se for uma data nova
                    data_nova.push(value.data_hora); 
                    id = index;

                }

                dados = {api_ykeys: value.percentual_atual};
                // dados = {dt: value.data_hora, api_ykeys: value.percentual_atual};

                objeto_data[id].push(dados);
                objeto_ykeys.push();
                objeto_labels.push(value.nome);
            });

            obj.forEach(function(value, index){
                dados = {dt: value.data_hora, [value.consulta]: value.percentual_atual};

                objeto_data.push(dados);
                objeto_ykeys.push(value.consulta);
                objeto_labels.push(value.nome);
            });
            // console.log(objeto_data);
            // console.log(objeto_ykeys);
            // console.log(objeto_labels);
            
            var area = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: objeto_data
                //descomenta esse 
                // [{ dt: '2012-02-24 12:20', a: 100, b: 90, c: 30 },
                //  { dt: '2012-02-24 12:25', a: 80, b: 70, c: 40 },
                //  { dt: '2012-02-24 12:30', a: 60, b: 85, c: 45 },
                //  { dt: '2012-02-24 12:35', a: 75, b: 65, c: 55 }]
                 // ou descomenta esse
                    // [{y: '2012-02-24 12:00', api_ykeys: 13},
                    // {y: '2012-02-24 12:05', api_ykeys: 57},
                    // {y: '2012-02-24 12:10', api_ykeys: 80},]
                  //ou descomenta esse  
                    // [{y: '2012-02-24 12:15', api_ykeys: 20, api_ykeys: 77, api_ykeys: 70},
                    // {y: '2012-02-24 12:20', api_ykeys: 30, api_ykeys: 97, api_ykeys: 88, api_ykeys: 16}]
                    // {y: '2012-02-24 12:25', envio_accertify_tim: 33, elegibilidade_tim: 17, envio_crivo_claro: 93, retorno_crivo_claro: 10},
                    // {y: '2012-02-24 12:35', envio_accertify_tim: 20, elegibilidade_tim: 77, envio_crivo_claro: 70, retorno_crivo_claro: 55},
                    // {y: '2012-02-24 12:40', envio_accertify_tim: 30, elegibilidade_tim: 97, envio_crivo_claro: 88, retorno_crivo_claro: 16},
                    // {y: '2012-02-24 12:45', envio_accertify_tim: 33, elegibilidade_tim: 17, envio_crivo_claro: 93, retorno_crivo_claro: 10}
                ,
                // xkey: 'dt',
                ykeys: objeto_ykeys, //mexe na linha do grafico
                // ykeys: ['envio_accertify_tim', 'elegibilidade_tim', 'envio_crivo_claro', 'retorno_crivo_claro'], //mexe na linha do grafico
                labels: objeto_labels, //mexe na largura do horario
                // labels: ['Envio Accertify TIM', 'Tim Elegibilidade', 'Envio Crivo CLARO', 'Retorno Crivo Claro'], //mexe na largura do horario
                lineColors: ['#2cbe4e', '#cb2431',],
                ymax: 100,
                ymin: 0,
                hideHover: 'auto',
                xLabels: "decade",
                events: ['2012-01-01', '2012-02-01', '2012-03-01'],
                goals: ["13", "47", "100", "100"],
                goalLabels: ["Green", "Red", "Green", "Green"]
            });
            // console.log(area);
            } else {
                console.log('Erro')
                return 'false';
            }
        };
        xhttp.open("POST", "listaLogApisGrafico", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send();
    },
    
    /** 
     * Funcão consulta via ajax as apis com seus status "estavel", "oscilando" e "instavel"
     * @returns 
     */
    consultaStatusPorApi: function() {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Se tudo o retorno for ok */
            if (this.readyState == 4 && this.status == 200) {
                var resposta = this.responseText,
                    obj      = JSON.parse(resposta);
                    apis.removerCarregamento();

                if(resposta != false){
                    document.getElementById("estavel").innerHTML   = obj.qtd_api_estavel+ '<sup style="font-size: 20px"></sup>';
                    document.getElementById("estavelInfo").href    = 'listaApis?ids='+obj.ids_estaveis;
                    document.getElementById("oscilando").innerHTML = obj.qtd_api_oscilando+ '<sup style="font-size: 20px"></sup>';
                    document.getElementById("oscilandoInfo").href  = 'listaApis?ids='+obj.ids_oscilando;
                    document.getElementById("instavel").innerHTML  = obj.qtd_api_instavel+ '<sup style="font-size: 20px"></sup>';
                    document.getElementById("instavelInfo").href   = 'listaApis?ids='+obj.ids_instaveis;
                } else {
                    console.log('Not api');
                    apis.removerCarregamento();
                }
            } else {
                // apis.removerCarregamento();
                return "false";
            }
        };
        xhttp.open("POST", "exibeIndicadores", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("&log=consulta_apis");
    },
    
    corStatus: function(status, id) {
        var cor = "#ff0000";
        if(status == 1) {
            cor = "#00ff00";
        }

        document.getElementById(id).style.color = cor;
    },

    removerCarregamento: function() {
        carregamentoImg = document.querySelectorAll('div.carregamento-cartao');
        carregamentoImg.forEach(element => {
            element.style.display = 'none';
        });
    }
};

/* Ao carregar a pagina chama as funçoes */
window.onload = function() {
    // apis.retornaDadosGraficoApi();
    apis.consultaStatusPorApi();
}

// Atualiza a pagina de 5 em 5 segundos
setTimeout(function(){
    apis.consultaStatusPorApi();
    location.reload(1);
},20000);