var listaLogs = {
    exportaLogs: function(ids) {
        
        // window.location.href = 'exportarDadosLogs?ids_log='+ids;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Se tudo ocorrer bem na consulta envio o resultado para verificar os status por api*/
            if (this.readyState == 4 && this.status == 200) {
                var resposta = this.responseText;
                
                if(resposta != false){  
                    console.log('excel');
                } else {
                    console.log('Not api');
                }
    
            } else {
                return "false";
            }
        };
        xhttp.open("POST", "exportarDadosLogs", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("&ids_log="+ids);
    },

    recuperaIdsLog: function(todos_ids_log){

            btnExportarLogs = document.getElementById('exportar-logs');
            tipoExportacao  = document.getElementsByName('optionsRadios');
            // ids_logs        = document.querySelectorAll('input[name=id_log]');
            ids  = [];
            tipo = [];
            // btnExportarLogs.disabled = true; //desablita o botao
            
            tipoExportacao.forEach(function(element) { // verifica quel opcao de exportacao o usuario quer
                if(element.checked){
                    tipo = element.value;
                } 
            });
            
            if(tipo[0] == 1){ //exporta pagina atual
                table = $('#example1').DataTable();
                dados = table.rows({ page: 'current' }).data();
                
                ids = []; //limpa variavel
                dados.each( id => { 
                    ids.push(id[0]);
                });
                
            } else if(tipo[0] == 2) { //exporta tudo
                ids = []; //limpa variavel
                todos_ids_log.forEach(function(element, i){
                    ids.push(element.value);    
                });
            }

        return ids;
    },

    // Verifica qual option foi clicado e cria o select filho
    criaSelectApi: function (selectCollection, selectSegmento){

        var selectCollection = selectCollection.options[selectCollection.selectedIndex].value,
            selectSegmento = selectSegmento.options[selectSegmento.selectedIndex].value;
        
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Se tudo ocorrer bem na consulta envio o resultado para verificar os status por api*/
            if (this.readyState == 4 && this.status == 200) {
                var resposta = this.responseText,
                    i, opt, valor,
                    camposOption = document.getElementById("select-api"),
                    obj          = JSON.parse(resposta);

                if(resposta != false){
                    camposOption.innerHTML = null; // limpa o select
                   
                    // inseri o campo selecione
                    opt = document.createElement("OPTION"); // cria o elemento
                    opt.setAttribute("value", 0);
                    valor = document.createTextNode("Selecione");
                    opt.appendChild(valor);
                    camposOption.appendChild(opt); // popula

                    obj.forEach(function(value, index){
                        opt = document.createElement("OPTION");
                        opt.setAttribute("value", value.id);
                        valor = document.createTextNode(value.nome);
                        opt.appendChild(valor);
                        camposOption.appendChild(opt);
                    });
                } else {
                    console.log('Not api');
                }
            } else {
                return "false";
            }
        };
        xhttp.open("POST", "selectFilhoApi", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("&collection="+selectCollection+"&segmento="+selectSegmento);
    },

     // Verifica qual option foi clicado e cria o select filho
     criaSelect: function (option){

        var option = option.options[option.selectedIndex].value;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Se tudo ocorrer bem na consulta envio o resultado para verificar os status por api*/
            if (this.readyState == 4 && this.status == 200) {
                var resposta = this.responseText,
                    i, opt, valor,
                    camposOption = document.getElementById("select-segmento"),
                    obj          = JSON.parse(resposta);

                if(resposta != false){
                    camposOption.innerHTML = null; // limpa o select
                   
                    // inseri o campo selecione
                    opt = document.createElement("OPTION"); // cria o elemento
                    opt.setAttribute("value", 0);
                    valor = document.createTextNode("Selecione");
                    opt.appendChild(valor);
                    camposOption.appendChild(opt); // popula

                    obj.forEach(function(value, index){
                        opt = document.createElement("OPTION");
                        opt.setAttribute("value", value.id);
                        valor = document.createTextNode(value.nm_segmento);
                        opt.appendChild(valor);
                        camposOption.appendChild(opt);
                    });
                } else {
                    console.log('Not api');
                }
            } else {
                return "false";
            }
        };
        xhttp.open("POST", "selectFilho", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("&collection="+option);
    },

     // lista a api ao clicar no botão
    btnBuscaLogApi: function(tipo){
        if(tipo == 'listaLogs'){
                $('#example1').DataTable( {
                    "destroy": true,
                    "ajax": 'listaLogs?collection='+$('#api-select').val()+'&segmento='+$('#select-segmento').val()+'&api='+$('#select-api').val()+'&situacao='+$('#select-situacao').val()+'&dataInicio='+$('#dataInicio').val()+'&dataFim='+$('#dataFim').val(),
                    "columns": [
                        { "data": "id_log" },
                        { "data": "nome" },
                        { "data": "operadora" },
                        { "data": "percentual_atual" },
                        { "data": "range_negativo_api" },
                        { "data": "range_positivo_api" },
                        { "data": "data" }
                    ],
                    "dom": 'Bfrtip',
                    "buttons": [
                        {
                            "extend": 'csv',
                            "text": 'Exportar atual',
                            "exportOptions": {
                                "modifier": {
                                    "page": 'current',
                                    "selected": false,
                                }
                            }                            
                        },
                        {
                            "extend": 'csv',
                            "text": 'Exportar tudo',
                            "exportOptions": {
                                "modifier": {
                                    "page": 'all',
                                    "selected": false,
                                }
                            }
                        }
                    ],
                });
                
                // Limpa os campos do select
                selectCollection.value = 0;   
                selectSegmento.value   = 0;   
                selectApi.value        = 0;   
                selectSituacao.value   = 0;   
        }
    }
};

var btnBuscar        = document.getElementById('btn-pesquisa-api'),
    selectCollection = document.getElementById("api-select"),
    selectSegmento   = document.getElementById("select-segmento"),
    selectApi        = document.getElementById("select-api"),
    selectStatus     = document.getElementById("api-status"),
    selectSituacao   = document.getElementById("select-situacao"),
    ids_logs         = document.querySelectorAll('input[name=id_log]'),
    ids = [],
    collection;

// Fica de olho no select quando clicado
window.onload = function(){
    // ids = listaLogs.recuperaIdsLog(ids_logs);
    selectApi.disabled = true;
    selectSituacao.disabled = true;
    selectCollection.onchange = function(){
       selectApi.disabled = true;
       selectSituacao.disabled = true;
       collection = this; 
       listaLogs.criaSelect(this);
    }
    
    selectSegmento.onchange = function(){
       selectApi.disabled = false;
       selectSituacao.disabled = false;
       listaLogs.criaSelectApi(collection, this);
    }
    
    // document.getElementById("todos_id_log").value = (ids.join(','));
}

//desabilita o select de pesquisa por status
// selectCollection.addEventListener('click', function(){
//     if(selectCollection.value != '0'){
//         selectStatus.disabled = true;
//     } else {
//         selectStatus.disabled = false;
//     }
// });

// habilita ou desabilita btn pesquisar    
selectSegmento.addEventListener('click', function(){
    btnBuscar.disabled = true;

    if(selectSegmento.value != 0 && selectSegmento.value != ''){
        btnBuscar.disabled = false;
    }
});

btnBuscar.addEventListener('click', function(){
    btnBuscar.disabled = true;
});

// pesquisa por status
// selectStatus.addEventListener('click', function(){    
//     if(selectStatus.value != 0){
//         btnBuscar.disabled = false;
//         selectCollection.disabled = true;
//         selectSegmento.disabled = true;
//     } else {
//         btnBuscar.disabled = true;
//         selectCollection.disabled = false;
//         selectSegmento.disabled = false;
//     }

// });

// function opcaoExportar (){
//         ids = listaLogs.recuperaIdsLog(ids_logs);
//         document.getElementById("todos_id_log").value = (ids.join(','));
// }