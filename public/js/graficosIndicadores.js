var selectOperadora   = document.getElementById('select-operadora'),
    selectSegmento    = document.getElementById('select-segmento'),
    selectTipoGrafico = document.getElementById('select-tipo-grafico'),
    dtInicio          = document.getElementById('dataInicio'),
    dtMeta            = document.getElementById('dataMeta'),
    dtFim             = document.getElementById('dataFim'),
    periodo           = document.getElementById('dataMeta');
    btnBuscar         = document.getElementById('btn-pesquisa');
    campoData         = document.getElementById('date');
    campoDataMeta     = document.getElementById('dateMeta');

 var grafico = {

    prazoAtivacao: function (operadora, segmento, tipoGrafico, dtInicio, dtFim) {

        /* Carregamento e desabilita os botões */
        objt.carregamento('block');
        objt.desabilitaCampos();
        objt.habilitaDiv('grafico-taxa-ativacao', 'none');
        objt.habilitaDiv('grafico-metas', 'none');

        divGrafico = document.getElementById('line-chart');

        /* Limpa o grafico */
        if (divGrafico.innerHTML != null) {
            objt.habilitaDiv('grafico-prazo-ativacao', 'none');
            objt.habilitaDiv('sem-dados', 'none');
            divGrafico.innerHTML = '';
            objt.habilitaDiv('div-salvar', 'none');

        }
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Verifica se o retorno esta ok */
            if (this.readyState == 4 && this.status == 200) {
             
                var resposta    = this.responseText,
                    obj         = JSON.parse(resposta),
                    objeto_data = [],
                    cores       = [];

                objt.carregamento('none');
                objt.habilitaCampos();

                cores = objt.getCoresOperadoras(operadora);
                /* Limpa os selects */
                selectOperadora.value   = '';
                selectSegmento.value    = '';
                selectTipoGrafico.value = '';

                if (objt.isEmpty(obj) == false) {
                    for (x in obj) {
                        objeto_data.push(obj[x]);
                    }
   
                    /* Habilita div do grafico */
                    objt.habilitaDiv('grafico-prazo-ativacao', '');
                    objt.habilitaDiv('sem-dados', 'none');
                    objt.habilitaDiv('div-salvar', '');

                    objt.popularGrafico(tipoGrafico, obj, cores, objeto_data, operadora);

                } else { //caso não venha dados
                    objt.habilitaDiv('div-salvar', 'none');
                    objt.habilitaDiv('sem-dados', '');
                }

            } else { /* não reotne valores*/

                return "false";
            }
        };

        xhttp.open("POST", "getDadosGraficoPrazoAtivacao", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send('&operadora=' + operadora + '&segmento=' + segmento + '&dataInicio=' + dtInicio + '&dataFim=' + dtFim);
    },

    taxaAtivacaoCancelamento: function (operadora, segmento, tipoGrafico, dtInicio, dtFim) {
        /* Carregamento e desabilita os botões */
        objt.carregamento('block');
        objt.desabilitaCampos();
        objt.habilitaDiv('grafico-prazo-ativacao', 'none');
        objt.habilitaDiv('grafico-metas', 'none');
        // console.log(operadora, segmento, tipoGrafico, dtInicio, dtFim)

        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Verifica se o retorno esta ok */
            if (this.readyState == 4 && this.status == 200) {

                var resposta               = this.responseText,
                    obj                    = JSON.parse(resposta),
                    obj_grid               = [],
                    obj_data               = [],
                    labels                 = [],
                    total_pedidos          = [],
                    total_pedidos_ativados = [],
                    total_pedidos_ativados_perc = [],
                    cores                  = [];

                objt.carregamento('none');
                objt.habilitaCampos();

                cores = objt.getCoresOperadoras(operadora);

                /* Limpa os selects */
                selectOperadora.value   = '';
                selectSegmento.value    = '';
                selectTipoGrafico.value = '';

                if (objt.isEmpty(obj) == false) {
                    for (x in obj) {
                        // console.log(obj[x].total_pedidos_ativados_perc) 
                        // return;
                        obj_grid.push(obj[x]);
                        labels.push(obj[x].ano_mes);
                        total_pedidos.push(obj[x].total_pedidos);
                        total_pedidos_ativados.push(obj[x].total_pedidos_ativados);
                        total_pedidos_ativados_perc.push(obj[x].total_pedidos_ativados_perc);
                    }
                    // console.log(total_pedidos_ativados_perc)

                    obj_data.push(labels);
                    obj_data.push(total_pedidos);
                    obj_data.push(total_pedidos_ativados);
                    obj_data.push(total_pedidos_ativados_perc);

                    /* hibilita div do grafico*/
                    objt.habilitaDiv('grafico-taxa-ativacao', '');
                    objt.habilitaDiv('div-salvar', '');
                    objt.habilitaDiv('sem-dados', 'none');

                    objt.popularGrafico(tipoGrafico, obj_grid, cores, obj_data, operadora);
                
                } else { //caso não venha dados
                    objt.habilitaDiv('div-salvar', 'none');
                    objt.habilitaDiv('sem-dados', '');
                }

            } else { /* não retorne valores */

                objt.habilitaDiv('div-salvar', 'none');
                objt.habilitaDiv('sem-dados', 'none');

                return "false";
            }
        };
// console.log(operadora+segmento+dtInicio+dtFim)
        xhttp.open("POST", "getDadosGraficoTaxaAtivacao", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send('&operadora=' + operadora + '&segmento=' + segmento + '&dataInicio=' + dtInicio + '&dataFim=' + dtFim);
    },

    metas: function (periodo) {

        // objt.desabilitaCampos();
        objt.habilitaDiv('grafico-taxa-ativacao', 'none');
        objt.habilitaDiv('grafico-prazo-ativacao', 'none');

        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Verifica se o retorno esta ok */
            if (this.readyState == 4 && this.status == 200) {
                objt.habilitaDiv('grafico-metas', 'none');
                selectTipoGrafico.value = '';

                var resposta            = this.responseText,
                    obj                 = JSON.parse(resposta),
                    dataPesquisa        = '';
                    gridTriboClaro      = [],
                    gridTriboTim        = [],
                    gridTriboOi         = [],
                    gridTriboVivo       = [],
                    gridTriboBandaLarga = [],
                    gridTriboVarejo     = [],
                    gridTriboLojaOnline = [],
                    gridTriboMicrosoft  = [];
                    
                    if (objt.isEmpty(obj) == false) {
                        
                        // Monta tabelinha grafico    
                        for (x in obj) { 
                            
                            dataPesquisa = Object.values(obj[x][1]).join('').replace('-','/');
                            
                            for (j in obj[x]) {

                                tribo = [];
                                tribo.push(Object.values(obj[x][j]).join(''));
                                
                                if(tribo.toString() == 'Tribo Claro'){
                                    var tipos         = [],
                                        colunasExtras = [],
                                        col           = [],
                                        total         = [],
                                        valores       = [];
                                    
                                    for (chave in obj[x]) {
                                        
                                        if(chave > 1){
                                            
                                            tipo    = obj[x][chave];
                                            tipos.push('<tr><th style="font-size:13px;border-top:none !important;" scope="row">'+ Object.keys(tipo) +':</th>'
                                                            +'<td style="font-size:11px;border-top:none !important;text-align:end;font-weight: 600;" class="direct-chat-timestamp">'+Object.values(tipo)+'</td></tr>');
                                                            
                                            valores.push(Number(Object.values(tipo).toString().replace('.', '')));
                                        }
                                    }
                                    
                                    sizeTipos     = tipos.length;

                                    for(a = 6; sizeTipos < a; sizeTipos++){
                                        col.push('<tr><th style="font-size:11px;border-top:none !important;visibility: collapse" scope="row" >Nada</th>'
                                                +'<td style="font-size:11px;border-top:none !important;" class="direct-chat-timestamp"></td></tr>');
                                    }

                                    colunasExtras.push(col.join(''));
                                    
                                    total.push('<tr><th style="font-size:13px;border-top:none !important;" scope="row">Total</th>'
                                            +'<td style="font-size:11px;border-top:none !important;font-weight: 600;text-align:end;" class="direct-chat-timestamp">'+valores.reduce(function(a, b) {return a + Math.round(b);}).toLocaleString('pt-BR', { style: 'decimal', decimal: '3' })+ '</td></tr>');

                                    gridTriboClaro.push(tipos.join(''), colunasExtras.join(''), total.join(''));
                                    
                                    if(objt.isEmpty(gridTriboClaro) == false){
                                        objt.habilitaDiv('grafico-metas', '');
                                        objt.habilitaDiv('div-salvar', '');
                                        objt.habilitaDiv('sem-dados', 'none');
                                        
                                        document.getElementById('tribo-claro').innerHTML = gridTriboClaro.join('');
                                    }
                                }

                                if(tribo.toString() == 'Tribo Tim'){ 
                                    
                                    var tipos         = [],
                                    colunasExtras = [],
                                    col           = [],
                                    total         = [];
                                    valores       = [];
                                    
                                    for (chave in obj[x]) {
                                            
                                        if(chave > 1){
                                            tipo    = obj[x][chave];
                                            
                                            tipos.push('<tr><th style="font-size:13px;border-top:none !important;" scope="row">'+ Object.keys(tipo) +':</th>'
                                                            +'<td style="font-size:11px;border-top:none !important;text-align:end;font-weight: 600;" class="direct-chat-timestamp">'+Object.values(tipo)+'</td></tr>');
                                            
                                            valores.push(Number(Object.values(tipo).toString().replace('.', '')));
                                        }
                                    }

                                    sizeTipos     = tipos.length;

                                    for(a = 6; sizeTipos < a; sizeTipos++){
                                        col.push('<tr><th style="font-size:11px;border-top:none !important;visibility: collapse" scope="row" >Nada</th>'
                                                +'<td style="font-size:11px;border-top:none !important;" class="direct-chat-timestamp"></td></tr>');
                                    }

                                    colunasExtras.push(col.join(''));

                                    total.push('<tr><th style="font-size:13px;border-top:none !important;" scope="row">Total</th>'
                                            +'<td style="font-size:11px;border-top:none !important;font-weight: 600;text-align:end;" class="direct-chat-timestamp">'+valores.reduce(function(a, b) {return a + Math.round(b);}).toLocaleString('pt-BR', { style: 'decimal', decimal: '2' })+ '</td></tr>');

                                    gridTriboTim.push(tipos.join(''), colunasExtras.join(''), total.join(''));
                                    
                                    if(objt.isEmpty(gridTriboTim) == false){
                                        objt.habilitaDiv('grafico-metas', '');
                                        objt.habilitaDiv('div-salvar', '');
                                        objt.habilitaDiv('sem-dados', 'none');
                                        
                                        document.getElementById('tribo-tim').innerHTML = gridTriboTim.join('');
                                    }
                                }

                                if(tribo.toString() == 'Tribo Oi'){
                                    
                                    var tipos         = [],
                                        colunasExtras = [],
                                        col           = [],
                                        total         = [];
                                        valores       = [];
                                    
                                    for (chave in obj[x]) {
                                            
                                        if(chave > 1){
                                            tipo    = obj[x][chave];
                                            
                                            tipos.push('<tr><th style="font-size:13px;border-top:none !important;" scope="row">'+ Object.keys(tipo) +':</th>'
                                                            +'<td style="font-size:11px;border-top:none !important;text-align:end;font-weight: 600;" class="direct-chat-timestamp">'+Object.values(tipo)+'</td></tr>');
                                            
                                            valores.push(Number(Object.values(tipo).toString().replace('.', '')));
                                        }
                                    }

                                    sizeTipos     = tipos.length;

                                    for(a = 6; sizeTipos < a; sizeTipos++){
                                        col.push('<tr><th style="font-size:11px;border-top:none !important;visibility: collapse" scope="row" >Nada</th>'
                                                +'<td style="font-size:11px;border-top:none !important;" class="direct-chat-timestamp"></td></tr>');
                                    }

                                    colunasExtras.push(col.join(''));

                                    total.push('<tr><th style="font-size:13px;border-top:none !important;" scope="row">Total</th>'
                                            +'<td style="font-size:11px;border-top:none !important;font-weight: 600;text-align:end;" class="direct-chat-timestamp">'+valores.reduce(function(a, b) {return a + Math.round(b);}).toLocaleString('pt-BR', { style: 'decimal', decimal: '3' })+ '</td></tr>');

                                    gridTriboOi.push(tipos.join(''), colunasExtras.join(''), total.join(''));
                                    
                                    if(objt.isEmpty(gridTriboOi) == false){
                                        objt.habilitaDiv('grafico-metas', '');
                                        objt.habilitaDiv('div-salvar', '');
                                        objt.habilitaDiv('sem-dados', 'none');
                                        
                                        document.getElementById('tribo-oi').innerHTML = gridTriboOi.join('');
                                    }
                                }

                                if(tribo.toString() == 'Tribo Vivo'){
                                    
                                    var tipos         = [],
                                        colunasExtras = [],
                                        total         = [],
                                        col           = [],
                                        valores       = [];
                                    
                                    for (chave in obj[x]) {
                                            
                                        if(chave > 1){
                                            tipo    = obj[x][chave];
                                            
                                            tipos.push('<tr><th style="font-size:13px;border-top:none !important;" scope="row">'+ Object.keys(tipo) +':</th>'
                                                            +'<td style="font-size:11px;border-top:none !important;text-align:end;font-weight: 600;" class="direct-chat-timestamp">'+Object.values(tipo)+'</td></tr>');
                                            
                                            valores.push(Number(Object.values(tipo).toString().replace('.', '')));
                                        }
                                    }

                                    sizeTipos     = tipos.length;

                                    for(a = 6; sizeTipos < a; sizeTipos++){
                                        col.push('<tr><th style="font-size:11px;border-top:none !important;visibility: collapse" scope="row" >Nada</th>'
                                                +'<td style="font-size:11px;border-top:none !important;" class="direct-chat-timestamp"></td></tr>');
                                    }

                                    colunasExtras.push(col.join(''));

                                    total.push('<tr><th style="font-size:13px;border-top:none !important;" scope="row">Total</th>'
                                            +'<td style="font-size:11px;border-top:none !important;font-weight: 600;text-align:end;" class="direct-chat-timestamp">'+valores.reduce(function(a, b) {return a + Math.round(b);}).toLocaleString('pt-BR', { style: 'decimal', decimal: '3' })+ '</td></tr>');

                                    gridTriboVivo.push(tipos.join(''), colunasExtras.join(''), total.join(''));
                                    
                                    if(objt.isEmpty(gridTriboVivo) == false){
                                        objt.habilitaDiv('grafico-metas', '');
                                        objt.habilitaDiv('div-salvar', '');
                                        objt.habilitaDiv('sem-dados', 'none');
                                        
                                        document.getElementById('tribo-vivo').innerHTML = gridTriboVivo.join('');
                                    }
                                }

                                if(tribo.toString() == 'Tribo Banda Larga'){
                                    
                                    var tipos         = [],
                                        colunasExtras = [],
                                        col           = [],
                                        total         = [],
                                        valores       = [];
                                    
                                    for (chave in obj[x]) {
                                            
                                        if(chave > 1){
                                            tipo    = obj[x][chave];
                                            // console.log(tipo)
                                            tipos.push('<tr><th style="width: 7%; font-size:11px;border-top:none !important;">'
                                            +'<img class="img-fluid" '+objt.recuperaImgMetas(Object.keys(tipo).toString())+' style="height: 12px;" /></th>'
                                            +'<td style="font-size:11px;border-top:none !important;text-align:end;font-weight: 600;" class="direct-chat-timestamp">'+Object.values(tipo)+'</td></tr>');
                                            
                                            valores.push(Number(Object.values(tipo).toString().replace('.', '')));      
                                            
                                        }
                                    }

                                    sizeTipos     = tipos.length;

                                    for(a = 6; sizeTipos < a; sizeTipos++){
                                        col.push('<tr><th style="font-size:11px;border-top:none !important;visibility: collapse" scope="row" >Nada</th>'
                                                +'<td style="font-size:11px;border-top:none !important;" class="direct-chat-timestamp"></td></tr>');
                                    }

                                    colunasExtras.push(col.join(''));

                                    total.push('<tr><th style="font-size:11px;border-top:none !important;" scope="row">Total</th>'
                                            +'<td style="font-size:11px;border-top:none !important;font-weight: 600;text-align:end;" class="direct-chat-timestamp">'+valores.reduce(function(a, b) {return a + Math.round(b);}).toLocaleString('pt-BR', { style: 'decimal', decimal: '3' })+ '</td></tr>');

                                    gridTriboBandaLarga.push(tipos.join(''), colunasExtras.join(''), total.join(''));
                                    
                                    if(objt.isEmpty(gridTriboBandaLarga) == false){
                                        objt.habilitaDiv('grafico-metas', '');
                                        objt.habilitaDiv('div-salvar', '');
                                        objt.habilitaDiv('sem-dados', 'none');
                                        
                                        document.getElementById('tribo-banda-larga').innerHTML = gridTriboBandaLarga.join('');
                                    }
                                }

                                if(tribo.toString() == 'Tribo Varejo'){
                                    
                                    var tipos         = [],
                                        colunasExtras = [],
                                        col           = [],
                                        total         = [],
                                        valores       = [];
                                    
                                    for (chave in obj[x]) {
                                            
                                        if(chave > 1){
                                            tipo    = obj[x][chave];
                                            
                                            tipos.push('<tr><th style="width: 7%; font-size:11px;border-top:none !important;">'
                                                        +'<img class="img-fluid" '+objt.recuperaImgMetas(Object.keys(tipo).toString())+' style="height: 12px;" /></th>'
                                                        +'<td style="font-size:11px;border-top:none !important;text-align:end;font-weight: 600;" class="direct-chat-timestamp">'+Object.values(tipo)+'</td></tr>');
                                            
                                            valores.push(Number(Object.values(tipo).toString().replace('.', '')));
                                        }
                                    }

                                    sizeTipos     = tipos.length;

                                    for(a = 6; sizeTipos < a; sizeTipos++){
                                        col.push('<tr><th style="font-size:11px;border-top:none !important;visibility: collapse" scope="row" >Nada</th>'
                                                +'<td style="font-size:11px;border-top:none !important;" class="direct-chat-timestamp"></td></tr>');
                                    }

                                    colunasExtras.push(col.join(''));

                                    total.push('<tr><th style="font-size:11px;border-top:none !important;" scope="row">Total</th>'
                                            +'<td style="font-size:11px;border-top:none !important;font-weight: 600;text-align:end;" class="direct-chat-timestamp">'+valores.reduce(function(a, b) {return a + Math.round(b);}).toLocaleString('pt-BR', { style: 'decimal', decimal: '3' })+ '</td></tr>');

                                    gridTriboVarejo.push(tipos.join(''), colunasExtras.join(''), total.join(''));
                                    
                                    if(objt.isEmpty(gridTriboVarejo) == false){
                                        objt.habilitaDiv('grafico-metas', '');
                                        objt.habilitaDiv('div-salvar', '');
                                        objt.habilitaDiv('sem-dados', 'none');
                                        
                                        document.getElementById('tribo-varejo').innerHTML = gridTriboVarejo.join('');
                                    }
                                }

                                if(tribo.toString() == 'Tribo Loja Online'){
                                    // console.log(obj[x]);
                                    var tipos         = [],
                                        colunasExtras = [],
                                        col           = [],
                                        total         = [],
                                        valores       = [];
                                    
                                    for (chave in obj[x]) {
                                            
                                        if(chave > 1){
                                            tipo    = obj[x][chave];
                                            
                                            tipos.push('<tr><th style="width: 7%; font-size:11px;border-top:none !important;">'
                                                    +'<img class="img-fluid" '+objt.recuperaImgMetas(Object.keys(tipo).toString())+' style="height: 12px;" /></th>'
                                                    +'<td style="font-size:11px;border-top:none !important;text-align:end;font-weight: 600;" class="direct-chat-timestamp">'+Object.values(tipo)+'</td></tr>');
                                            
                                                    valores.push(Number(Object.values(tipo).toString().replace('.', '')));
                                        }
                                    }

                                    sizeTipos     = tipos.length;

                                    for(a = 6; sizeTipos < a; sizeTipos++){
                                        col.push('<tr><th style="font-size:11px;border-top:none !important;visibility: collapse" scope="row" >Nada</th>'
                                                +'<td style="font-size:11px;border-top:none !important;" class="direct-chat-timestamp"></td></tr>');
                                    }

                                    colunasExtras.push(col.join(''));

                                    total.push('<tr><th style="font-size:11px;border-top:none !important;" scope="row">Total</th>'
                                            +'<td style="font-size:11px;border-top:none !important;font-weight: 600;text-align:end;" class="direct-chat-timestamp">'+valores.reduce(function(a, b) {return a + Math.round(b);}).toLocaleString('pt-BR', { style: 'decimal', decimal: '3' })+ '</td></tr>');

                                    gridTriboLojaOnline.push(tipos.join(''), colunasExtras.join(''), total.join(''));
                                    
                                    if(objt.isEmpty(gridTriboLojaOnline) == false){
                                        objt.habilitaDiv('grafico-metas', '');
                                        objt.habilitaDiv('div-salvar', '');
                                        objt.habilitaDiv('sem-dados', 'none');
                                    
                                        document.getElementById('tribo-loja-online').innerHTML = gridTriboLojaOnline.join('');
                                    }
                                }
                                
                                if(tribo.toString() == 'Tribo Microsoft'){
                                    
                                    var tipos         = [],
                                        colunasExtras = [],
                                        col           = [],
                                        total         = [],
                                        valores       = [];
                                    
                                    for (chave in obj[x]) {
                                            
                                        if(chave > 1){
                                            tipo    = obj[x][chave];
                                            tipos.push('<tr><th style="width: 7%; font-size:11px;border-top:none !important;">'
                                                    +'<img class="img-fluid" '+objt.recuperaImgMetas(Object.keys(tipo).toString())+' style="height: 12px;" /></th>'
                                                    +'<td style="font-size:11px;border-top:none !important;text-align:end;font-weight: 600;" class="direct-chat-timestamp">'+Object.values(tipo)+'</td></tr>');
                                            
                                            valores.push(Number(Object.values(tipo).toString().replace('.', '')));
                                        }
                                    }

                                    sizeTipos     = tipos.length;

                                    for(a = 6; sizeTipos < a; sizeTipos++){
                                        col.push('<tr><th style="font-size:11px;border-top:none !important;visibility: collapse" scope="row" >Nada</th>'
                                                +'<td style="font-size:11px;border-top:none !important;" class="direct-chat-timestamp"></td></tr>');
                                    }

                                    colunasExtras.push(col.join(''));

                                    total.push('<tr><th style="font-size:11px;border-top:none !important;" scope="row">Total</th>'
                                            +'<td style="font-size:11px;border-top:none !important;font-weight: 600;text-align:end;" class="direct-chat-timestamp">'+valores.reduce(function(a, b) {return a + Math.round(b);}).toLocaleString('pt-BR', { style: 'decimal', decimal: '3' })+ '</td></tr>');

                                    gridTriboMicrosoft.push(tipos.join(''), colunasExtras.join(''), total.join(''));
                                    
                                    if(objt.isEmpty(gridTriboMicrosoft) == false){
                                        objt.habilitaDiv('grafico-metas', '');
                                        objt.habilitaDiv('div-salvar', '');
                                        objt.habilitaDiv('sem-dados', 'none');
                                        
                                        document.getElementById('tribo-microsoft').innerHTML = gridTriboMicrosoft.join('');
                                    }
                                }
                            }
                        }
                 
                    month   = new Date(dataPesquisa).getMonth();
                    year    = new Date(dataPesquisa).getFullYear();    
                 
                    monName = ["janeiro", "fevereiro", "março", "abril", "maio", "junho", "agosto", "outubro", "novembro", "dezembro"];    

                    document.getElementById('mes-meta').innerHTML = ' Metas ' + monName[month] + ' de ' + year;

                } else { //caso não venha dados
                    objt.habilitaDiv('div-salvar', 'none');
                    objt.habilitaDiv('sem-dados', '');
                }
            } else { /* não retornou valores */
                return "false";

            }
        };
        xhttp.open("POST", "getDadosGraficoMeta", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send('&periodo=' + periodo);

    },

    recuperaImgMetas: function(tipo) {
        
        switch(tipo){
            case 'vivo': return img = 'src="'+url+'/img/wooza/grafico_metas/icon_vivo_mini.png"';
            case 'oi': return img = 'src="'+url+'/img/wooza/grafico_metas/icon_oi_mini.png"';
            case 'timLive': return img = 'src="'+url+'/img/wooza/grafico_metas/icon_timLive_mini.png"';
            case 'net': return img = 'src="'+url+'/img/wooza/grafico_metas/icon_net_mini.png"';
            case 'tim': return img = 'src="'+url+'/img/wooza/grafico_metas/icon_tim_mini.png"';
            case 'nextel': return img = 'src="'+url+'/img/wooza/grafico_metas/icon_nextel_mini.png"';
            case 'claro': return img = 'src="'+url+'/img/wooza/grafico_metas/icon_claro_mini.png"';
            case 'office': return img = 'src="'+url+'/img/wooza/grafico_metas/icon_office_mini.png"';
        }

    },

    gridPrazoAtivcao: function (obj, cores) {
//  console.log(obj)
        var grid = [],
            linhaAno = [],
            linha2Horas = [],
            linha6Horas = [];

        for (x in obj) { 
            linhaAno.push('<td>' + obj[x].ano_mes + '</td>');
            linha2Horas.push('<td><span style="background-color:' + cores[0] + '; font-size:11px" class="badge">' + obj[x].quantidade2Horas + '</span></td>');
            linha6Horas.push('<td><span style="background-color:' + cores[1] + '; font-size:11px" class="badge">' + obj[x].quantidade6Horas + '</span></td>');
        }

        for (x in obj) {
            if (x == 0) {

                grid.push('<th>Ano/mês' + linhaAno.join('') + '</th>'
                    + '<tr><th scope="col">% em 2 horas' + linha2Horas.join('') + '</th></tr>'
                    + '<tr><th scope="col">% em 6 horas' + linha6Horas.join('') + '</th></tr>'
                );
            }

            // grid.push('<tr>'+obj[x].ano_mes+'</tr><tr><span style="background-color:'+cores[0]+'; color:'+cores[2]+'" class="badge">'+obj[x].quantidade2Horas+'</span></tr><tr><span style="background-color:'+cores[1]+'" class="badge ">'+obj[x].quantidade6Horas+'</span></tr>');
        }

        document.getElementById('grid-prazo-ativacao').innerHTML = grid.join('');
    },

    graficoPrazoAtivacao: function (obj, operadora, cores) {
// console.log(obj)
        // obj = [{ano_mes: "2019-09", quantidade2Horas: "21,30", quantidade6Horas: "1,54"},
        //         {ano_mes: "2019-10", quantidade2Horas: "25,13", quantidade6Horas: "2,15"},
        //         {ano_mes: "2019-11", quantidade2Horas: "31,45", quantidade6Horas: "2,85"},
        //         {ano_mes: "2019-12", quantidade2Horas: "0,00", quantidade6Horas: "0,00"}]; 
        // console.log(obj);

        if (obj != null) {
            grafico = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: obj,
                xkey: 'ano_mes',
                ykeys: ['quantidade2Horas', 'quantidade6Horas'], // valor do 'item'
                labels: ['2 horas', '6 horas'],// nome atrelado ao valor 
                hideHover: 'auto',
                lineColors: [cores[0], cores[1]], // cor da linha de cada item
                lineWidth: 2, // 
                parseTime: false,
                postUnits: '%',
                gridTextColor: '#11110f', // cor do texto da data e percentual
                gridStrokeWidth: 1, // expessura da linha
                pointSize: 5, // tamanho do circulo 
                pointStrokeColors: ['#efefef'], // cor em volta do circulo (dos pontos)
                xLabelAngle: 20,
                axes: true,
                gridTextFamily: 'Open Sans', // tipo de font do texto
                // gridLineColor    : '#efefef', // cor da linha na grid
                // gridTextSize     : 12, // tamanho do texto          
                dateFormat: function (x) { // funcao para formatar o nome do mes
                    // console.log(x);
                    var IndexToMonth = [" Jan ", " Fev ", " Mar ", " Abr ", " Mai ", " Jun ", " Jul ", " Ago ", " Set ", " Out ", "Nov ", " Dez "];
                    var month = IndexToMonth[new Date(x).getMonth()];
                    var year = new Date(x).getFullYear();
                    return month + '  ' + year;
                },
                xLabelAngle: '40',
                // hoverCallback: function (index, options, content, row) {
                //     console.log(content)
                //     console.log(options.hideHover)
                //     return options;
                //     // return 'Ano ' + row.ano_mes + '<br> 2 horas ' + row.quantidade2Horas + '<br> 6 horas ' + row.quantidade6Horas;
                // }, 
                // goals: [80, -1], // valor da meta para cada tipo
                // goalLineColors: ['#00c0ef', '#f0ad4e'] // cor da linha da meta
            });

        } else {
            objt.habilitaDiv('sem-dados', '');
        }

        document.getElementById('nome-operadora').innerHTML = ' Prazo de ativação ' + operadora;
        // return grafico;

    },

    gridTaxaAtivacao: function (obj, cores) {

        var grid = [],
        linhaAno = [],
        linhaTotalAtivado = [],
        linhaPlanoCartao = [],
        linhaBotAtivacao = [],
        linhaBkoAtivacao = [],
        linhaOutrosAtivacao = [],
        linhaTotalCancelado = [],
        linhaAnalisePreliminar = [],
        linhaBotCancelamento = [],
        linhaBkoCancelamento = [],
        linhaOutrosCancelmento = [],
        linhaBacklog = []
        linhaTotalGeral = []
        ;
        // console.log(obj)
        for (x in obj) { 
            linhaAno.push('<td><span style="background-color:' + cores[1] + '; font-size:10px;">' + obj[x].ano_mes + '</td>');
            linhaTotalAtivado.push('<td><span style="background-color:' + cores[0] + '; font-size:10px;font-weight: 700;">' + obj[x].total_pedidos_ativados_perc + '%</span></td>');
            linhaPlanoCartao.push('<td><span style="font-size: 10px;">' + obj[x].plano_cartao + '%</span></td>');
            linhaBotAtivacao.push('<td><span style="font-size: 10px;">' + obj[x].bot_ativacao + '%</span></td>');
            linhaBkoAtivacao.push('<td><span style="font-size: 10px;">' + obj[x].bko_ativacao + '%</span></td>');
            linhaOutrosAtivacao.push('<td><span style="font-size: 10px;">' + obj[x].outros_ativados + '%</span></td>');
            linhaTotalCancelado.push('<td><span style="background-color:' + cores[0] + '; font-size:10px;">' + obj[x].total_pedidos_cancelados_perc + '%</span></td>');
            linhaAnalisePreliminar.push('<td><span style="font-size: 10px;">' + obj[x].analise_preliminar_cancelamento + '%</span></td>');
            linhaBotCancelamento.push('<td><span style="font-size: 10px;">' + obj[x].bot_cancelamento + '%</span></td>');
            linhaBkoCancelamento.push('<td><span style="font-size: 10px;">' + obj[x].bko_cancelamento + '%</span></td>');
            linhaOutrosCancelmento.push('<td><span style="font-size: 10px;">' + obj[x].outros_cancelamento + '%</span></td>');
            linhaBacklog.push('<td><span style="background-color:' + cores[0] + '; font-size:10px;" >' + obj[x].backlog + '%</span></td>');
            linhaTotalGeral.push('<td><span style="font-size: 10px;">' + obj[x].total_geral_perc + '%</span></td>');
        }

        /** Monta a grid */
        for (x in obj) {
            if (x == 0) {

                grid.push('<tr style="background-color:'+cores[1]+';font-size: larger;color: white;"><th>Ano/mês' + linhaAno.join('') + '</th></tr>'
                    + '<tr style="background-color:'+cores[0]+'"><th scope="col">Total ativado' + linhaTotalAtivado.join('') + '</th></tr>'
                    + '<tr><th scope="col">Plano cartão' + linhaPlanoCartao.join('') + '</th></tr>'
                    + '<tr><th scope="col">Bot' + linhaBotAtivacao.join('') + '</th></tr>'
                    + '<tr><th scope="col">Bko' + linhaBkoAtivacao.join('') + '</th></tr>'
                    + '<tr><th scope="col">Outros ativados' + linhaOutrosAtivacao.join('') + '</th></tr>'
                    + '<tr style="background-color:'+cores[0]+'"><th scope="col">Total cancelado' + linhaTotalCancelado.join('') + '</th></tr>'
                    + '<tr><th scope="col">Análise preliminar' + linhaAnalisePreliminar.join('') + '</th></tr>'
                    + '<tr><th scope="col">Bot' + linhaBotCancelamento.join('') + '</th></tr>'
                    + '<tr><th scope="col">Bko' + linhaBkoCancelamento.join('') + '</th></tr>'
                    + '<tr><th scope="col">Outros' + linhaOutrosCancelmento.join('') + '</th></tr>'
                    + '<tr style="background-color:'+cores[0]+'"><th scope="col">Backlog' + linhaBacklog.join('') + '</th></tr>'
                    + '<tr style="background-color:#c4d82d"><th scope="col">Total Geral' + linhaTotalGeral.join('') + '</th></tr>'
                    );

            }

            // grid.push('<tr>'+obj[x].ano_mes+'</tr><tr><span style="background-color:'+cores[0]+'; color:'+cores[2]+'" class="badge">'+obj[x].quantidade2Horas+'</span></tr><tr><span style="background-color:'+cores[1]+'" class="badge ">'+obj[x].quantidade6Horas+'</span></tr>');
        }

        document.getElementById('grid-taxa-ativacao').innerHTML = grid.join('');
    },

    graficoTaxaAtivacao: function (operadora, obj, cores) {

        /**Limpando grafico */
        document.getElementById("bar-taxa-ativacao").innerHTML = '&nbsp;';
        document.getElementById("bar-taxa-ativacao").innerHTML = '<canvas id="barChart"></canvas>';
        // console.log(obj)
        // obj = [{"ano_mes":"2019-04","total_pedidos":161613,"total_pedidos_ativados":51171,"total_pedidos_cancelados":19160,"plano_cartao":"","bot_ativacao":48228,"bko_ativacao":2829,"outros_ativados":114,"analise_preliminar_cancelamento":91255,"bot_cancelamento":14430,"bko_cancelamento":4729,"outros_cancelamento":1},
        //         {"ano_mes":"2019-05","total_pedidos":176870,"total_pedidos_ativados":42994,"total_pedidos_cancelados":26991,"plano_cartao":"","bot_ativacao":40466,"bko_ativacao":2351,"outros_ativados":177,"analise_preliminar_cancelamento":106865,"bot_cancelamento":22012,"bko_cancelamento":4973,"outros_cancelamento":6}];
      
        //  obj = [["2019-04", "2019-05", "2019-06", "2019-07", "2019-08"],
        //               [804541, 705484,565448,365581,455796],
        //               [23356, 70084,50448,35581,45796],
        //               [23, 70,50,35,45]];


        var dadosGrafico = {
            labels: obj[0],
            datasets: [
                {
                    label: 'Total de pedidos',
                    type: 'bar',
                    fillColor: 'rgba(210, 214, 222, 1)',
                    strokeColor: 'rgba(210, 214, 222, 1)',
                    pointColor: 'rgba(210, 214, 222, 1)',
                    pointStrokeColor: '#c1c7d1',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    backgroundColor: cores[0],
                    data: obj[1]
                },
                {
                    label: 'Pedidos ativados',
                    type: 'bar',
                    fillColor: 'rgba(60,141,188,0.9)',
                    strokeColor: 'rgba(60,141,188,0.8)',
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    backgroundColor: cores[1],
                    data: obj[2]
                },
                // {
                //     label: 'Percentual pedidos ativados',
                //     type: 'line',
                //     fillColor: 'rgba(210, 214, 222, 1)',
                //     strokeColor: 'rgba(210, 214, 222, 1)',
                //     pointColor: 'rgba(210, 214, 222, 1)',
                //     pointStrokeColor: '#c1c7d1',
                //     pointHighlightFill: '#fff',
                //     pointHighlightStroke: 'rgba(220,220,220,1)',
                //     // backgroundColor: cores[0],
                //     data: obj[3]
                // },
            ]
        },

            configOptions = {
                title: { //titulo
                      display: true, //habilita o titulo
                      text: operadora + ' migração e upgrade', // Texto de title para o grafico
                      fontSize: 15
                }
            };

        var graficoTxAtivacao = document.getElementById("barChart").getContext("2d");

        new Chart(graficoTxAtivacao, {
            type: 'bar',
            data: dadosGrafico,
            options: configOptions

        });
    },

    getCoresOperadoras: function (operadora) {

        switch (operadora) {
            case 'claro':
                return cores = ['#cecece', '#e12f21', 'white'];
            case 'oi':
                return cores = ['#cecece', '#ffbc4a', 'white'];
            case 'tim':
                return cores = ['#cecece', '#004691', 'white'];
            case 'vivo':
                return cores = ['#cecece', '#47006a', 'white'];
            default: console.log('Operadora sem cores.')
        }

    },

    popularGrafico: function (tipoGrafico, obj = null, cores = null, objeto_data = null, operadora = null) {
        // operadora = false;
        
        switch (tipoGrafico) {
            case '0': objt.gridPrazoAtivcao(obj, cores);
                      operadora = objt.tratarTexto(operadora);
                    //   console.log(objeto_data)
                    objt.graficoPrazoAtivacao(objeto_data, operadora, cores); // carrega o grafico
                    //   console.log(operadora)
                break;
            case '1': objt.gridTaxaAtivacao(obj, cores);
                      operadora = objt.tratarTexto(operadora);
                      objt.graficoTaxaAtivacao(operadora, objeto_data, cores);
                break;
            case '2': this.habilitaDiv('grafico-prazo-ativacao', 'none');
                      this.habilitaDiv('sem-dados', '');

            // break;
            case '3': this.habilitaDiv('grafico-prazo-ativacao', 'none');
                      this.habilitaDiv('sem-dados', '');
            // break;
        }

        // console.log(operadora)
        // return operadora;
    },

    salvar: function (tipo) {
        // render:'download' // isso força o download direto
        // render:'prazo' // isso impedi o download direto
        console.log(tipo.id)
        data = new Date();
        data_atual = data.getDay() + '-' + data.getMonth() + '-' + data.getFullYear() + ' ' + data.getHours() + ':' + data.getMinutes() + ':' + data.getSeconds();

        xepOnline.Formatter.Format(tipo.id,{
            render: 'newwin',
            embedLocalImages: true,
            pageWidth: '480mm',
            pageHeight: '579mm',
            pageMarginLeft: '2.2',
            filename: tipo.texto + data_atual,
            mimeType: 'application/pdf',

            // pdf: 'application/pdf', 
            // svg: 'image/svg+xml', 
            // xps:'application/vnd.ms-xpsdocument',
            // ps: 'application/postscript',
            // afp: 'application/afp',
            // xep: 'application/xep',
            // png: 'image/png'
        });
    },

    /* Não usei graficoDestroy*/
    graficoDestroy: function (obj) {

        if (obj != null) {
            obj.destroy();
        }

    },

    tratarTexto: function (text) {

        var words = text.toLowerCase().split(" ");
        for (var a = 0; a < words.length; a++) {
            var w = words[a];
            words[a] = w[0].toUpperCase() + w.slice(1);
        }
        return words.join(" ");
    },

    carregamento: function (tipo) {
        carregamentoImg = document.querySelectorAll('div.carregamento');
        carregamentoImg.forEach(element => {
            element.style.display = tipo;
        });
    },

    isEmpty: function (obj) {

        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {

                return false;
            }
        }

        return true;
    },

    habilitaCampos: function () {
        document.getElementById('select-operadora').disabled = false;
        document.getElementById('select-segmento').disabled = false;
        document.getElementById('select-tipo-grafico').disabled = false;
        document.getElementById('dataInicio').disabled = false;
        document.getElementById('dataFim').disabled = false;
    },

    desabilitaCampos: function () {
        document.getElementById('select-operadora').disabled = true;
        document.getElementById('select-segmento').disabled = true;
        document.getElementById('select-tipo-grafico').disabled = true;
        document.getElementById('dataInicio').disabled = true;
        document.getElementById('dataFim').disabled = true;
        document.getElementById('btn-pesquisa').disabled = true;
    },

    habilitaDiv: function (div, tipo) {
        var el = div,
            display = document.getElementById(el).style.display;

        document.getElementById(el).style.display = tipo;
    }
};

window.onload = function () {

    /* verificando campos preenchidos*/
    btnBuscar.disabled = true;
    objGrafico         = null;
    objt               = grafico; // serve para manter o objeto ao clicar mais de uma vez no buscar sem carregar a pagina
// alert(url)
    this.selectSegmento.disabled = true;
    // this.selectTipoGrafico       = true;
    // this.selectTipoGrafico.disabled = true;
    objt.habilitaDiv('sem-dados', 'none');
    objt.habilitaDiv('date', 'none');
    objt.habilitaDiv('dateMeta', 'none');

    selectOperadora.addEventListener('click', function () {

        if (selectOperadora.value != '') {
            selectSegmento.disabled = false;

            selectSegmento.addEventListener('click', function () {
                if (selectSegmento.value != '') {
                    selectTipoGrafico.disabled = false;

                } else {
                    selectTipoGrafico.disabled = true;

                }
            });

        } else {
            selectSegmento.disabled = true;
            btnBuscar.disabled      = true;
        }
    });

    selectSegmento.addEventListener('click', function () {

        if(selectTipoGrafico.value != '3'){
            if (selectOperadora.value != '' && selectSegmento.value != '' && selectTipoGrafico.value != '' && dataInicio.value != '' && dataFim.value != '') {
                btnBuscar.disabled = false;

            } else {
                btnBuscar.disabled = true;

            }

        }
    });

    selectTipoGrafico.addEventListener('click', function () {

        /* Serve para exibir os tipos de filtros de data */
        if(selectTipoGrafico.value == '3' && selectTipoGrafico.value != ''){
            objt.habilitaDiv('date', 'none');
            objt.habilitaDiv('dateMeta', '');
            objt.habilitaDiv('operadora', 'none');
            objt.habilitaDiv('segmento', 'none');

            if(dtMeta.value != ''){
                btnBuscar.disabled = false;

            }
            
        } else if(selectTipoGrafico.value != '3' && selectTipoGrafico.value != '') {
            objt.habilitaDiv('date', '');
            objt.habilitaDiv('dateMeta', 'none');
            objt.habilitaDiv('operadora', '');
            objt.habilitaDiv('segmento', '');
        }
    });

    /* fim da verificando campos preenchidos*/

    btnBuscar.addEventListener('click', function () {
        
        if (selectTipoGrafico.value != '') {
            switch (selectTipoGrafico.value) {
                case '0': window.objt.prazoAtivacao(selectOperadora.value, selectSegmento.value, selectTipoGrafico.value, dataInicio.value, dataFim.value);
                    tipoSalvar = { id:'grafico1', texto:'Gráfico Prazo ativação - '};
                    break;
                case '1': window.objt.taxaAtivacaoCancelamento(selectOperadora.value, selectSegmento.value, selectTipoGrafico.value, dataInicio.value, dataFim.value);
                    tipoSalvar = {id:'grafico2', texto:'Gráfico Taxa ativação - '};
                    break;
                case '3': window.objt.metas(dtMeta.value);
                    tipoSalvar = {id:'grafico3', texto:'Gráfico Metas - '};
                    break;
            }
        }

    });

    btnSalvar = document.getElementById('salvar');

    btnSalvar.addEventListener('click', function () {
        window.objt.salvar(tipoSalvar);
    });
}    