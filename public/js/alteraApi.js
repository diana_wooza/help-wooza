var alteraApi = {
 // Verifica qual option foi clicado e cria o select filho
 criaSelect: function (option){

    var option = option.options[option.selectedIndex].value;
    //console.log(option.options[option].value);
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        /* Se tudo ocorrer bem na consulta envio o resultado para verificar os status por api*/
        if (this.readyState == 4 && this.status == 200) {
            
            var resposta = this.responseText,
                i, opt, valor,
                camposOption = document.getElementById("select-segmento"),
                obj          = JSON.parse(resposta);
            
            if(resposta != false){
                camposOption.innerHTML = null; // limpa o select
               
                // inseri o campo selecione
                opt = document.createElement("OPTION"); // cria o elemento
                opt.setAttribute("value", 0);
                valor = document.createTextNode("Selecione");
                opt.appendChild(valor);
                camposOption.appendChild(opt); // popula

                obj.forEach(function(value, index){
                    opt = document.createElement("OPTION");
                    opt.setAttribute("value", value.id);
                    valor = document.createTextNode(value.nm_segmento);
                    opt.appendChild(valor);
                    camposOption.appendChild(opt);
                });
            } else {
                console.log('Not api');
            }
        } else {
            return "false";
        }
    };
    xhttp.open("POST", "selectFilho", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("&collection="+option);
},
 // Verifica qual option foi clicado e cria o select filho
 criaSelectApi: function (selectCollection, selectSegmento){

    var selectCollection = selectCollection.options[selectCollection.selectedIndex].value,
        selectSegmento = selectSegmento.options[selectSegmento.selectedIndex].value;

    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        /* Se tudo ocorrer bem na consulta envio o resultado para verificar os status por api*/
        if (this.readyState == 4 && this.status == 200) {
            var resposta = this.responseText,
                i, opt, valor,
                camposOption = document.getElementById("select-api"),
                obj          = JSON.parse(resposta);
     
            if(resposta != false){
                camposOption.innerHTML = null; // limpa o select
               
                // inseri o campo selecione
                opt = document.createElement("OPTION"); // cria o elemento
                opt.setAttribute("value", 0);
                valor = document.createTextNode("Selecione");
                opt.appendChild(valor);
                camposOption.appendChild(opt); // popula

                obj.forEach(function(value, index){
                    opt = document.createElement("OPTION");
                    opt.setAttribute("value", value.id);
                    valor = document.createTextNode(value.nome);
                    opt.appendChild(valor);
                    camposOption.appendChild(opt);
                });
            } else {
                console.log('Not api');
            }
        } else {
            return "false";
        }
    };
    xhttp.open("POST", "selectFilhoApi", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("&collection="+selectCollection+"&segmento="+selectSegmento+"&statusAll=&ativaInativa=true");
},

/* recupera os usuario que receberam sms */
// btnPessoasSms: function(){
//         // alert('Pelos poderes de graskol')
        
//         xhttp = new XMLHttpRequest();
//         xhttp.onreadystatechange = function () {
 
//             if (this.readyState == 4 && this.status == 200) {
//                 resposta = this.responseText;
//                 obj      = JSON.parse(resposta);
                
//                 if(resposta != false){

//                     obj.forEach(element => {
//                         console.log(element.id);
//                         console.log(element.nome);
//                     });

//                 } else {
//                         console.log('Not user');
//                 }                
//             } else {
//                 return "false";
//             }
//         };
//         xhttp.open("POST", "listaPessoasSms", true);
//         xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//         xhttp.send();
//     }
};

var btnBuscar              = document.getElementById('btn-pesquisa-api'),
    btnChamaModal          = document.getElementById('bs-example-modal-sm'),
    btnModal               = document.getElementById('envia-formulario'),
    selectCollection       = document.getElementById("api-select"),
    btnPessoasSms          = document.getElementById("pessoas-sms"),
    selectSegmento         = document.getElementById("select-segmento"),
    selectApi              = document.getElementById("select-api"),
    selectTempoConsulta    = document.getElementById("inputDisparo"),
    selectItensAlarmantes  = document.getElementById("inputDisparo1"),
    exampleCheck1          = document.getElementById("exampleCheck1"),
    horaInicio             = document.getElementsByName("hora-inicio"),
    horaFinal              = document.getElementsByName("hora-final"),
    qtdPessoas             = document.getElementById("qtd-pessoas"),
    modalPessoasSms        = document.getElementsByName('modal-pessoas-sms'),
    btnCancelarPessoasSms  = document.getElementById('cancelar-pessoas-sms'),
    modalPessoas           = document.getElementById('modalPessoasSms'),
    formAlterar            = document.getElementById('form-alterar'),
    btnAlterarPessoasSms   = document.getElementById('alterar-pessoas-sms');
    
window.onload = function(){
    // ids = listaLogs.recuperaIdsLog(ids_logs);
    btnBuscar.disabled = true;
    selectApi.disabled = true;

    selectCollection.onchange = function(){
        selectApi.disabled = true;
       collection = this; 
       alteraApi.criaSelect(this);
    }
    
    selectSegmento.onchange = function(){
        selectApi.disabled = false;
        alteraApi.criaSelectApi(collection, this);
    }
     /**
      * configuração dos horarios
      */
     $('#timepicker1-antigo').datetimepicker({defaultDate:'now', locale: 'pt-BR', format: 'HH:mm:ss'});
    //  $('#timepicker1-antigo').data("DateTimePicker").format('HH:mm:ss');
     $('#timepicker2-antigo').datetimepicker({defaultDate:'now', locale: 'pt-BR', format: 'HH:mm:ss'});
    //  $('#timepicker2-antigo').data("DateTimePicker").format('HH:mm:ss');
        
    $("#timepicker1-antigo").on("dp.change", function (e) {
        $('#timepicker2-antigo').data("DateTimePicker").minDate(e.date);
    });

    $("#timepicker2-antigo").on("dp.change", function (e) {
        $('#timepicker1-antigo').data("DateTimePicker").maxDate(e.date);
    });

         // Grid utiliza barra de rolagem
         $('#example2').DataTable().destroy();
        
         $('#example2').DataTable({
             'paging'      : false,
             "scrollY"     : "300px",
             "scrollCollapse": true,
             'lengthChange': true,
             'searching'   : true,
             'ordering'    : true,
             'info'        : true,
             'autoWidth'   : true,
             "oLanguage": {
             "sEmptyTable": "Nenhum registro encontrado",
             "sInfo": "Mostrando total de _TOTAL_ registros",
             "sInfoFiltered": "(Filtrados de _MAX_ registros)",
             "sInfoPostFix": "",
             "sInfoThousands": ".",
             "sLoadingRecords": "Carregando...",
             "sProcessing": "Processando...",
             "sZeroRecords": "Nenhum registro encontrado",
             "sSearch": "Pesquisar: ",
             // "oAria": {
             //     "sSortAscending": ": Ordenar colunas de forma ascendente",
             //     "sSortDescending": ": Ordenar colunas de forma descendente"
             // }
             }
         });
 
}

if(btnAlterarPessoasSms || btnCancelarPessoasSms){
    btnChamaModal.addEventListener("click", function() {
        
        // Usando plugin validate jQuery
        form = $("#form-alterar");

        form.validate({
            rules : {
                segmento : "required",
                channel : "required",
                'range-negativo' : {
                    required: true,
                },
                'range-positivo' : {
                    required: true,
                },
                'hora-inicio' : {
                    required: true,
                },   
                'hora-final' : {
                    required: true,
                },   
                'tempo-verificacao' : {required:true},   
                'itens-alarmantes' : {
                    required: true,
                },   
            },
            messages: {
                segmento : "Escolha um segmento válido!",
                channel : "Escolha um channel do Mongo válido!",
                'range-negativo': {
                    required: "O percentual do range negativo é obrigatório",
                },
                'range-positivo': {
                    required: "O percentual do range positivo é obrigatório",
                },
                'hora-inicio': {
                    required: "A hora início é obrigatória",
                },
                'hora-final': {
                    required: "A hora de término é obrigatória",
                },
                'tempo-verificacao' : "Escolha um tempo para verificação válido!",
                'itens-alarmantes' : "Escolha quantidade de itens alarmantes válido!",
            },
            submitHandler: function(form) {
                $('#modal-alterar').addClass('in show'); 
                $('#modal-alterar').modal('show'); //Se for válido, exibe o modal.
            }
        });
  });

    //Modal submete o formulario
    btnModal.addEventListener('click', function(){
        formAlterar.submit();
    });

    // Verifica a qtd de pessoas para envio de sms do badge - checkbox

        //btn alterar sms - checkbox
    btnAlterarPessoasSms.addEventListener('click', function(ev){
        retorno = contaCheckbox();
        qtdPessoas.innerHTML = retorno.selecionados+'</span>';
        document.getElementById('pessoas').value = retorno.pessoasIds;
    });

    //btn cancelar sms - checkbox
    btnCancelarPessoasSms.addEventListener('click', function(ev){
        retorno = contaCheckbox(false);
        qtdPessoas.innerHTML = retorno.selecionados+'</span>';
    });

    retorno = this.contaCheckbox(true);
    qtdPessoas.innerHTML = retorno.selecionados+'</span>';
    document.getElementById('pessoas').value = retorno.pessoasIds;

}

// Verifica se uma determinada classe esta sendo usada
function hasClass(elemento, classe) {
    return (' ' + elemento.className + ' ').indexOf(' ' + classe + ' ') > -1;

}

function contaCheckbox(novoInput){
 
    var x, selecionados = 0, idsPessoas = [],
        retorno = [],
        inputs = document.getElementsByName('modal-pessoas-sms');
    
    for(x = 0; x < inputs.length; x++){
        if(inputs[x].type == 'checkbox'){
            if(inputs[x].checked == true){
                idsPessoas.push(inputs[x].value);

                if(novoInput == true){ 
                    inputs[x].classList.add('escolhido');

                } else if(novoInput == false) { //limpa o checkbox
                    if(!hasClass(inputs[x],'escolhido')) {
                        inputs[x].checked = false;
                        selecionados--;

                    }
                }
                selecionados++;
            }
        }
    }
    retorno = {pessoasIds   : idsPessoas,
               selecionados : selecionados};

    return retorno;
}
// Fim checkbox

// habilita ou desabilita btn pesquisar    
selectApi.addEventListener('click', function(ev){
    if(selectApi.value != 0 && selectApi.value != ''){
        btnBuscar.disabled = false;
    }
});
