'use strict';

const Iccid = {

	verify: function (iccid)
	{
		return iccid.match(/^89148\d\d{13}\d$/) !== false && this.isValidLuhn(iccid);
	},

	isValidLuhn: function (number)
	{
		number = number.toString();
		var sumTable = [
		  [0,1,2,3,4,5,6,7,8,9],
		  [0,2,4,6,8,1,3,5,7,9]
		];

		var sum = 0;
		var flip = 0;

		for (var i = number.length - 1; i >= 0; i--)
			sum += sumTable[flip++ & 0x1][number[i]];

		return sum % 10 === 0;
	  }
};