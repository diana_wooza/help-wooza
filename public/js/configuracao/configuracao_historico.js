$(function() {
    $.ajax({
        type: 'GET',
        url: '../configuracao/retorno_robo/descricoes_acoes',
        dataType: 'json',
        success: function (data) {
            $(data).each(function(i, v) {
                descricoes_acoes[v.acao] = v.descricao;
                dados_adicionais_acoes[v.acao] = v.dados_adicionais;
            });
        }
    });

    // robo escolhido
    $("#select_robo").change(function() {
        $(".lista_tratativas").html('');
        $("#json_robo").val('');
        $("#div_configuracao_lista_tratativas_historico").fadeOut();
        $(".div_json_robo_historico").fadeOut();
        jsonConfiguracao = 
        {
            "nome": null,
            "interface": [],
            "configuracao": [],
            "json_robo": [],
            "usuario_padrao": null,
        }
        jsonTratativa = 
        {
            "nome": null,
            "condicoes": [],
            "regras": []
        }
        var robo = $(this).val();
        jsonConfiguracao.nome = robo;
        
        $.ajax({
            type: 'GET',
            url: '../configuracao/historico/datas_historico/'+robo,
            dataType: 'json',
            success: function (data) {
                var html = "<option></option>";
                $(data).each(function(i, v) {
                    var date = new Date(v['created_at']);
                    var d = date.getDate();
                    var m =  date.getMonth()+1;
                    var y = date.getFullYear();
                    var h = date.getHours();
                    var mi = date.getMinutes();
                    var s = date.getSeconds();
                    html += "<option value='"+v['_id']+"'>"+d+"/"+m+"/"+y+" "+h+":"+mi+":"+s+" - "+v['usuario']+"</option>";
                });
                $("#versao").html(html);
                $("#div_data_historico").fadeIn();
            },
            error: function () {
                console.log(data);
            }
        });
    });

    // versao escolhida
    $("#versao").change(function() {
        jsonConfiguracao = 
        {
            "nome": null,
            "interface": [],
            "configuracao": [],
            "json_robo": [],
            "usuario_padrao": null,
        }
        jsonTratativa = 
        {
            "nome": null,
            "condicoes": [],
            "regras": []
        }
        $(".lista_tratativas").html('');
        $("#json_robo").val('');
        carregarInterfaceRoboHistorico($('#select_robo').val(), $("#versao").val());
    });
});

function carregarInterfaceRoboHistorico(robo, versao) {
    $.ajax({
        type: 'GET',
        url: '../configuracao/historico/carregar_versao/'+versao,
        dataType: 'json',
        success: function (data) {
            carregarListaUsuario(robo);
            carregarJsonRobo(robo);
            $(data).each(function(i, v) {
                if (v != null) {
                    criarTratativa(v.nome, v.ativo);
                    jsonConfiguracao.interface[i] = v;
                }
            });
            $("#div_configuracao_lista_tratativas_historico").fadeIn();
            $(".div_json_robo_historico").fadeIn();
            listaTratativasSortable();
            $(".content i").hide();
            $(".content .btn").hide();
            $(".lista_tratativas .btn-right > .editar_tratativa").toggleClass('fa-pencil fa-eye');
            $(".editar_tratativa").show();
        },
        error: function () {
            carregarListaUsuario(robo);
            carregarJsonRobo(robo);
            $("#div_configuracao_lista_tratativas_historico").fadeIn();
            $(".div_json_robo_historico").fadeIn();
        }
    });
}

function carregarJsonRobo(robo) {
    $.ajax({
        type: 'GET',
        url: '../configuracao/retorno_robo/json_robo/'+robo,
        dataType: 'json',
        success: function (data) {
            $("#json_robo").val(JSON.stringify(data, null, 2));
        }
    });
}

function carregarListaUsuario(robo) {
    $.ajax({
        type: 'GET',
        url: '../configuracao/retorno_robo/valores_select/user_id',
        dataType: 'json',
        success: function (data) {
            var html = "<option></option>";
            $(data).each(function(i, v) {
                html += "<option value='"+v['value']+"'>"+v['text']+"</option>";
            });
            $("#usuario_padrao").html(html);
            carregarUsuarioPadrao(robo);
        }
    });
}

function carregarUsuarioPadrao(robo) {
    $.ajax({
        type: 'GET',
        url: '../configuracao/retorno_robo/usuario_padrao/'+robo,
        dataType: 'json',
        success: function (data) {
            if (data != null) {
                $("#usuario_padrao").val(data);
                jsonConfiguracao.usuario_padrao = data;
            }
        }
    });
}