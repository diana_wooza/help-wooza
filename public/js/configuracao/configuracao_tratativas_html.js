function criarItemCondicao(campo, comparacao, valores, numero_item=null, ativo=true) {
    if (numero_item == null) {
        numero_item = jsonTratativa.condicoes[grupoCondicoes].length;
    }
    var icone_ativo = 'fa-ban';
    var desabilitado = '';
    if (ativo == false || ativo == 'false') {
        icone_ativo = 'fa-check-square';
        desabilitado = 'desabilitado';
    }
    var html = '';
    html += "<li class='list-group-item "+desabilitado+"' numero_item="+numero_item+">";
    html += "   <div class='row center'>";
    html += "       <div class='col-sm-4'>"+campo+"</div>";
    html += "       <div class='col-sm-1'>"+comparacao+"</div>";
    html += "       <div class='col-sm-5'>";
    var len = valores.length;
    for (var i=0;i<len;i++) {
        if (i > 0) {
            html += "<div class='texto_linha texto_linha-small'><span class='hr hr-small'>OU</span></div>";
        }
        html += valores[i];
    }
    html += "       </div>";
    html += "       <div class='col-md-2 coluna_icones'>";
    html += "           <i class='fa fa-minus-square remover_item' aria-hidden='true'></i>";
    html += "           <i class='fa "+icone_ativo+" ativar_item' aria-hidden='true'></i>";
    html += "       </div>";
    html += "   </div>";
    html += "</li>";
    if ($(".grupo_condicoes_"+grupoCondicoes).find('li.list-group-item').length > 1) {
        html += "<div class='texto_linha texto_linha-small'><span class='hr hr-small hr-small-and'>E</span></div>";
    }
    
    return html;
}

function criarItemValidacao(campo, comparacao, valores, numero_item=null, ativo=true) {
    if (numero_item == null) {
        var numero_item = 0;
        if (jsonTratativa.regras[numeroRegra].validacoes == null) {
            jsonTratativa.regras[numeroRegra].validacoes = [];
            jsonTratativa.regras[numeroRegra].validacoes[0] = [];
        }
        else {
            numero_item = jsonTratativa.regras[numeroRegra].validacoes[grupoValidacoes].length;
        }
    }
    var icone_ativo = 'fa-ban';
    var desabilitado = '';
    if (ativo == false || ativo == 'false') {
        icone_ativo = 'fa-check-square';
        desabilitado = 'desabilitado';
    }
    var html = '';
    html += "<li class='list-group-item "+desabilitado+"' numero_item="+numero_item+">";
    html += "    <div class='row center'>";
    html += "        <div class='col-sm-4'>"+campo+"</div>";
    html += "        <div class='col-sm-1'>"+comparacao+"</div>";
    html += "        <div class='col-sm-6'>";
    if (valores != undefined) {
        var len = valores.length;
        for (var i=0;i<len;i++) {
            if (i > 0) {
                html += "<div class='texto_linha texto_linha-small'><span class='hr hr-small'>OU</span></div>";
            }
            html += valores[i];
        }
    }
    html += "        </div>";
    html += "        <div class='col-md-1 coluna_icones'>";
    html += "            <i class='fa fa-minus-square remover_item' aria-hidden='true'></i>";
    html += "            <i class='fa "+icone_ativo+" ativar_item' aria-hidden='true'></i>";
    html += "        </div>";
    html += "    </div>";
    html += "</li>";
    if ($(".li_regra_"+numeroRegra+" .grupo_validacoes_"+grupoValidacoes).find('li.list-group-item').length > 1) {
        html += "<div class='texto_linha texto_linha-small'><span class='hr hr-small hr-small-and'>E</span></div>";
    }
    return html;
}

function criarItemAcao(acao, numero_item=null, ativo=true) {
    if (numero_item == null) {
        if (jsonTratativa.regras[numeroRegra].acoes == null) {
            jsonTratativa.regras[numeroRegra].acoes = [];
            numero_item = 0;
        }
        else {
            numero_item = jsonTratativa.regras[numeroRegra].acoes.length
        }
    }
    var icone_ativo = 'fa-ban';
    var desabilitado = '';
    if (ativo == false || ativo == 'false') {
        icone_ativo = 'fa-check-square';
        desabilitado = 'desabilitado';
    }
    var html ='';
    var html_descricao = '';
    var descricao = '';

    if (descricoes_acoes[acao] != null) { 
        var desc = descricoes_acoes[acao].split(',');
        if (desc != '' && desc != undefined) {
            var d = '';
            $(desc).each(function(i, v) {
                d += '- '+v+'<br>';
            });
            descricao = d;
        }
    }
    
    if (dados_adicionais_acoes[acao] != null) {
        var dados_adicionais = dados_adicionais_acoes[acao].split(',');
        if (dados_adicionais != '' && dados_adicionais != undefined) {
            var d = '<b>Dados adicionais necessários:</b><br>';
            $(dados_adicionais).each(function(i, v) {
                d += '- '+v+'<br>';
            });
            descricao += d;
        }
    }
    
    html_descricao = "<br><span class='descricao_acao'>"+descricao+"</span>";
    html += "<li class='list-group-item "+desabilitado+"' numero_item="+numero_item+">";
    html += "    <div class='row'>";
    html += "        <div class='col-sm-9'>"+acao+html_descricao+"</div>";
    html += "        <div class='col-md-3 coluna_icones coluna_icones_acoes'>";
    html += "            <i class='fa fa-sort ordenar_acoes' aria-hidden='true'></i>";
    html += "            <i class='fa fa-minus-square remover_item' aria-hidden='true'></i>";
    html += "            <i class='fa "+icone_ativo+" ativar_item' aria-hidden='true'></i>";
    html += "        </div>";
    html += "    </div>";
    html += "</li>";
    return html;
}

function criarItemDadosAdicionais(campo, valores, numero_item=null, ativo=true) {
    if (numero_item == null) {
        if (jsonTratativa.regras[numeroRegra].dados_adicionais == null) {
            jsonTratativa.regras[numeroRegra].dados_adicionais = [];
            numero_item = 0;
        }
        else {
            numero_item = jsonTratativa.regras[numeroRegra].acoes.length;
        }
    }
    var icone_ativo = 'fa-ban';
    var desabilitado = '';
    if (ativo == false || ativo == 'false') {
        icone_ativo = 'fa-check-square';
        desabilitado = 'desabilitado';
    }
    
    var html = '';
    html += "<li class='list-group-item "+desabilitado+"' numero_item="+numero_item+">";
    html += "    <div class='row row_dados_adicionais center'>";
    html += "        <div class='col-sm-4'>"+campo+"</div>";
    html += "        <div class='col-sm-1'>=</div>";
    html += "        <div class='col-sm-5'>"+valores+"</div>";
    html += "        <div class='col-md-2 coluna_icones'>";
    html += "            <i class='fa fa-minus-square remover_item' aria-hidden='true'></i>";
    html += "            <i class='fa "+icone_ativo+" ativar_item' aria-hidden='true'></i>";
    html += "        </div>";
    html += "    </div>";
    html += "</li>";
    return html;
}

function criarGrupoCondicao(num=null) {
    if (num == null) {
        num = 0;
        if (jsonTratativa.condicoes != null) {
            num = jsonTratativa.condicoes.length;
        }
        else {
            jsonTratativa.condicoes = [];
        }
    }
    var html = '';
    var ou = '';
    var classe_ou = '';
    
    if (num%3 == 0 && num > 0) {
        html += "<div class='clearfix'></div>";
    }

    if (num > 2) {
        ou = "<div class='texto_linha texto_linha_grupos'><span class='hr'>OU</span></div>";
        classe_ou = " ou_coluna_2";
    }

    html += "<div class='col-sm-4 coluna_condicoes'>";
    html += "    <div class='row'>";
    html += "        <div class='col-sm-1 coluna_linha_vertical"+classe_ou+"'>";
    html += "            <div class='or'>";
    html += "                <div class='above'></div>";
    html += "                <span>OU</span>";
    html += "                <div class='below'></div>";
    html += "            </div>";
    html += "        </div>";
    html += "        <div class='col-sm-11'>";
    html +=             ou;
    html += "        <div class='well well_condicoes'>";
    html += "            <ul class='list-group grupo_condicoes_"+num+"' numero_grupo="+num+">";
    html += "                <li class='list-group-item botoes_grupo_condicoes'>";
    html += "                    <div class='row center'>";
    html += "                        <div class='col-sm-5'>";
    html += "                            <button type='button' class='btn btn-xs btn-primary' data-toggle='modal' data-target='#modal_campo_valor' data-btn='adicionar_condicao' data-num_grupo_condicoes='"+num+"'>";
    html += "                                <i class='fa fa-plus' aria-hidden='true'></i> Adicionar Condição";
    html += "                            </button>";
    html += "                        </div>";
    html += "                        <div class='col-sm-7 btn-right'>";
    html += "                            <button type='button' class='btn btn-xs btn-danger remover_grupo_condicoes'>";
    html += "                                <i class='fa fa-minus' aria-hidden='true'></i> Remover Grupo de Condições";
    html += "                            </button>";
    html += "                        </div>";
    html += "                    </div>";
    html += "                </li>";
    html += "            </ul>";
    html += "        </div>";
    html += "     </div>";
    html += "   </div>";
    html += "</div>";

    $(".coluna_adicionar_grupo_condicoes").before(html);
    ajustarGrid();
    calcularAlturaLinhaVertical($(".coluna_adicionar_grupo_condicoes").prev().find('.list-group'));
}

function criarGrupoValidacao(numero_regra, numero_grupo=null) {
    if (numero_grupo == null) {
        numero_grupo = jsonTratativa.regras[numero_regra].validacoes.length;
    }
    var html = '';
    html += "<div class='texto_linha texto_linha_grupos'><span class='hr'>OU</span></div>";
    html += "<div class='well well_validacoes'>";
    html += "   <ul class='list-group grupo_validacoes_"+numero_grupo+"' numero_grupo="+numero_grupo+">";
    html += "       <li class='list-group-item botoes_grupo'>";
    html += "           <div class='row center'>";
    html += "               <div class='col-sm-9'>";
    html += "                   <button type='button' class='btn btn-xs btn-primary' data-toggle='modal' data-target='#modal_campo_valor' data-btn='adicionar_validacao' data-numero_regra="+numero_regra+" data-num_grupo_validacoes='"+numero_grupo+"'>";
    html += "                       <i class='fa fa-plus' aria-hidden='true'></i> Adicionar Validação";
    html += "                   </button>";
    html += "               </div>";
    html += "               <div class='col-sm-3 btn-right'>";
    html += "                   <button type='button' class='btn btn-xs btn-danger remover_grupo_validacoes'>";
    html += "                       <i class='fa fa-minus' aria-hidden='true'></i> Remover Grupo de Validações";
    html += "                   </button>";
    html += "               </div>";
    html += "           </div>";
    html += "       </li>";
    html += "   </div>";
    html += "</ul>";
    
    $(".li_regra_"+numero_regra+" .btn_adicionar_grupo_validacoes").before(html);
}

function criarRegra(num=null, ativo=true, nome='Regra') {
    if (num == null) {
        var num = 0;
        if (jsonTratativa.regras != null) {
            num = jsonTratativa.regras.length;
        }
        else {
            jsonTratativa.regras = [];
        }
    }
    var icone_ativo = 'fa-ban';
    var desabilitado = '';
    if (ativo == false || ativo == 'false') {
        icone_ativo = 'fa-check-square';
        desabilitado = 'desabilitado';
    }
    var html = '';
    html += "<li class='li_regra li_regra_"+num+" "+desabilitado+"' numero_regra="+num+">";
    html += "    <div class='row row_titulo_regras'>";
    html += "        <div class='col-sm-10 titulo titulo_regra'>"+nome+"</div>";
    html += "        <div class='col-sm-2 titulo titulo_regra btn-right'>";
    html += "            <i class='fa fa-minus-square excluir_regra' aria-hidden='true'></i>";
    html += "            <i class='fa "+icone_ativo+" desabilitar_regra' aria-hidden='true'></i>";
    html += "            <i class='fa fa-pencil editar_regra' aria-hidden='true'></i>";
    html += "            <i class='fa fa-sort ordenar_regras' aria-hidden='true'></i>";
    html += "            <i class='fa fa-minus' aria-hidden='true'></i>";
    html += "        </div>";
    html += "    </div>";
    html += "    <span class='span_regra'>";
    html += "        <div class='row '>";
    html += "            <div class='col-sm-12'>";
    html += "                <div class='row row_regras'>";
    html += "                    <div class='col-sm-12 titulo titulos_grupos_regras titulo_caixa'>Validações de Retorno</div>";
    html += "                </div>";
    html += "                <div class='row'>";
    html += "                    <div class='well well_validacoes'>";
    html += "                        <ul class='list-group grupo_validacoes_0' numero_grupo='0'>";
    html += "                            <li class='list-group-item botoes_grupo'>";
    html += "                                <div class='row center'>";
    html += "                                    <div class='col-sm-9'>";
    html += "                                        <button type='button' class='btn btn-xs btn-primary' data-toggle='modal' data-target='#modal_campo_valor' data-btn='adicionar_validacao' data-numero_regra="+num+" data-num_grupo_validacoes='0'>";
    html += "                                            <i class='fa fa-plus' aria-hidden='true'></i> Adicionar Validação";
    html += "                                        </button>";
    html += "                                    </div>";
    html += "                                    <div class='col-sm-3 btn-right'>";
    html += "                                        <button type='button' class='btn btn-xs btn-danger remover_grupo_validacoes'>";
    html += "                                            <i class='fa fa-minus' aria-hidden='true'></i> Remover Grupo de Validações";
    html += "                                        </button>";
    html += "                                    </div>";
    html += "                                </div>";
    html += "                            </li>";
    html += "                        </ul>";
    html += "                    </div>";
    html += "                    <button type='button' class='btn btn-xs btn-success btn_adicionar_grupo_validacoes'>";
    html += "                        <i class='fa fa-plus' aria-hidden='true'></i> Adicionar Grupo de Validações";
    html += "                    </button>";
    html += "                </div>";
    html += "            </div>";
    html += "            <div class='col-sm-4 coluna_acoes'>";
    html += "                <div class='row row_regras'>";
    html += "                    <div class='col-sm-12 titulo titulos_grupos_regras titulo_caixa'>Ações</div>";
    html += "                </div>";
    html += "                <div class='row'>";
    html += "                    <div class='well well_acoes'>";
    html += "                        <ol class='list-group lista_acoes'>";
    html += "                            <li class='list-group-item botoes_grupo'>";
    html += "                                <button type='button' class='btn btn-xs btn-primary btn_adicionar_acao ' data-toggle='modal' data-target='#modal_campo_valor' data-btn='adicionar_acao' data-numero_regra="+num+">";
    html += "                                    <i class='fa fa-plus' aria-hidden='true'></i> Adicionar Ação";
    html += "                                </button>";
    html += "                            </li>";
    html += "                        </ol>";
    html += "                    </div>";
    html += "                </div>";
    html += "            </div>";
    html += "            <div class='col-sm-8'>";
    html += "                <div class='row row_regras'>";
    html += "                    <div class='col-sm-12 titulo titulos_grupos_regras titulo_caixa'>Dados Adicionais</div>";
    html += "                </div>";
    html += "                <div class='row'>";
    html += "                    <div class='well well_dados_adicionais'>";
    html += "                        <ul class='list-group'>";
    html += "                            <li class='list-group-item botoes_grupo'>";
    html += "                                <button type='button' class='btn btn-xs btn-primary btn_adicionar_dados_adicionais' data-toggle='modal' data-target='#modal_campo_valor' data-btn='adicionar_dados_adicionais' data-numero_regra="+num+">";
    html += "                                    <i class='fa fa-plus' aria-hidden='true'></i> Adicionar Dados Adicionais";
    html += "                                </button>";
    html += "                            </li>";
    html += "                        </ul>";
    html += "                    </div>";
    html += "                </div>";
    html += "            </div>";
    html += "        </div>";
    html += "        <br>";
    html += "    </span>";
    html += "</li>";

    $(".lista_regras").append(html);
}

function criarTratativa(nome='Nova Tratativa', ativo=true) {
    var html = '';
    var num = jsonConfiguracao.interface.length;
    var icone_ativo = 'fa-ban';
    var desabilitado = '';
    if (ativo == false || ativo == 'false') {
        icone_ativo = 'fa-check-square';
        desabilitado = 'desabilitado';
    }
    html += "<li class='list-group-item "+desabilitado+"' numero_item='"+num+"'>";
    html += "    <div class='row'>";
    html += "        <div class='col-sm-10'>"+nome+"</div>";
    html += "        <div class='col-sm-2 btn-right'>";
    html += "            <i class='fa fa-minus-square excluir_tratativa' aria-hidden='true'></i>";
    html += "            <i class='fa "+icone_ativo+" desabilitar_tratativa' aria-hidden='true'></i>";
    html += "            <i class='fa fa-pencil editar_tratativa' aria-hidden='true'></i>";
    html += "            <i class='fa fa-sort ordenar_tratativas' aria-hidden='true'></i>";
    html += "        </div>";
    html += "    </div>";
    html += "</li>";
    $(".div_lista_tratativas ol").append(html);
}

function reiniciarHtmlTratativa() {
    var html = '';
    html += "<div class='col-md-12'>";
    html += "    <div class='well well-sm well_tratativa'>";
    html += "        <div class='row center main row_tratativa'>";
    html += "            <div class='col-sm-10'>";
    html += "                <i class='fa fa-arrow-left exibir_tratativas' aria-hidden='true'></i>&nbsp;";
    html += "                <label>Tratativa: </label>";
    html += "                <label class='label_nome_tratativa'></label>";
    html += "            </div>";
    html += "            <div class='col-sm-2 btn-right'>";
    html += "                <i class='fa fa-pencil editar_tratativa' aria-hidden='true'></i>";
    html += "            </div>";
    html += "        </div>";
    html += "        <div class='row main'>";
    html += "            <div class='col-sm-12'>";
    html += "                <div class='row row_titulo_regras'>";
    html += "                    <div class='col-sm-12 titulo condicoes titulo_caixa'>Condições do Pedido</div>";
    html += "                </div>";
    html += "                <div class='row row_condicoes'>";
    html += "                    <div class='col-sm-1 coluna_adicionar_grupo_condicoes'>";
    html += "                        <button type='button' class='btn btn-xs btn-success btn_adicionar_grupo_condicoes'>";
    html += "                            <i class='fa fa-plus' aria-hidden='true'></i> Adicionar Grupo de Condições";
    html += "                        </button>";
    html += "                    </div>";
    html += "                </div>";
    html += "            </div>";
    html += "            <div class='col-sm-12 titulo regras titulo_caixa'>";
    html += "                Regras";
    html += "                <i class='fa fa-compress icone_regras comprimir btn-right' aria-hidden='true'></i>";
    html += "            </div>";
    html += "            <div class='col-sm-12'>";
    html += "                <ol class='lista_regras'></ol>";
    html += "            </div>";
    html += "            <div class='col-sm-1 coluna_adicionar_regra'>";
    html += "                <button type='button' class='btn btn-xs btn-dark btn_adicionar_regra'>";
    html += "                    <i class='fa fa-plus' aria-hidden='true'></i> Adicionar Regra";
    html += "                </button>";
    html += "            </div>";
    html += "        </div>";
    html += "    </div>";
    html += "</div>";
    $("#div_configuracao_tratativas").html(html);
}