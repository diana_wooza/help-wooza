var tratativaEditada;
var descricoes_acoes = [];
var dados_adicionais_acoes = [];

$(function() {
    $.ajax({
        type: 'GET',
        url: 'configuracao/retorno_robo/descricoes_acoes',
        dataType: 'json',
        success: function (data) {
            $(data).each(function(i, v) {
                descricoes_acoes[v.acao] = v.descricao;
                dados_adicionais_acoes[v.acao] = v.dados_adicionais;
            });
        }
    });

    // usuario padrao escolhido
    $("#usuario_padrao").change(function() {
        jsonConfiguracao.usuario_padrao = $(this).val();
    });

    // robo escolhido
    $("#select_robo").change(function() {
        $(".lista_tratativas").html('');
        $("#json_robo").val('');
        jsonConfiguracao = 
        {
            "nome": null,
            "interface": [],
            "configuracao": [],
            "json_robo": [],
            "usuario_padrao": null,
        }
        jsonTratativa = 
        {
            "nome": null,
            "condicoes": [],
            "regras": []
        }
        var robo = $(this).val();
        jsonConfiguracao.nome = robo;

        carregarInterfaceRobo(robo);
    });

    $(".div_lista_tratativas").on('click', '.excluir_tratativa', function() {
        if (confirm("Remover tratativa?")) {
            var li = $(this).parent().parent().parent();
            var numero_item = li.attr('numero_item');
            li.fadeOut('normal', function() {
                jsonConfiguracao.interface[numero_item].excluido = true;
                li.remove();
            });
        }
    });

    $(".div_lista_tratativas").on('click', '.desabilitar_tratativa', function() {
        var li = $(this).parent().parent().parent();
        var numero_item = li.attr('numero_item');
        li.toggleClass('desabilitado');
        $(this).toggleClass('fa-ban fa-check-square');
        jsonConfiguracao.interface[numero_item].ativo = !li.hasClass('desabilitado');
    });
    
    // tratativa escolhida
    $(".div_lista_tratativas").on('click', '.editar_tratativa', function() {
        $(".salvar_configuracao").fadeOut();
        tratativaEditada = $(this).parent().parent().parent().attr('numero_item');
        jsonTratativa = jsonConfiguracao.interface[tratativaEditada];
        $(".row_tratativa .label_nome_tratativa").html(jsonTratativa.nome);
        $(".div_lista_tratativas").fadeOut('normal', function() {
            carregarTratativa();
        });
    });

    // voltar para lista de tratativas
    $("#div_configuracao_tratativas").on('click', '.row_tratativa > .col-sm-10 > i', function() {
        jsonConfiguracao.interface[tratativaEditada] = jsonTratativa;
        $(".lista_tratativas").find('[numero_item='+tratativaEditada+']').find('.col-sm-10').html(jsonTratativa.nome);
        $("#div_configuracao_tratativas").fadeOut('normal', function() {
            reiniciarHtmlTratativa();
            $("#div_configuracao_lista_tratativas").fadeIn();
            $(".div_lista_tratativas").fadeIn();
            $(".salvar_configuracao").fadeIn();
        });
    });

    $(".btn_adicionar_tratativa").click(function() {
        criarTratativa();
        jsonConfiguracao.interface.push(
            {
                "nome": "Nova Tratativa",
                "ativo": true,
                "condicoes": [],
                "regras": []
            }
        );
        listaTratativasSortable();
    });

    $(".salvar_configuracao").click(function() {
        var btn = $(this);
        btn.hide(0, function() {
            $(".salvando").show();
        });
        jsonConfiguracao.json_robo = $("#json_robo").val();
        $.ajax({
            type: 'POST',
            url: 'configuracao/retorno_robo/salvar_configuracao',
            dataType: 'json',
            data: jsonConfiguracao,
            complete: function (data) {
                if (data.responseText == 'ok') {
                }
                else {
                    alert(data.responseText);
                }
                
                $(".salvando").fadeOut('normal', function() {
                    btn.fadeIn();
                });
            }
        });
    });
});

function listaTratativasSortable() {
    $("ol.lista_tratativas").sortable('destroy').sortable(getOpcoesSortable('ordenar_tratativas'));
}

function carregarInterfaceRobo(robo) {
    $.ajax({
        type: 'GET',
        url: 'configuracao/retorno_robo/interface_configuracao/'+robo,
        dataType: 'json',
        success: function (data) {
            carregarListaUsuario(robo);
            carregarJsonRobo(robo);
            $(data).each(function(i, v) {
                if (v != null) {
                    criarTratativa(v.nome, v.ativo);
                    jsonConfiguracao.interface[i] = v;
                }
            });
            $("#div_configuracao_lista_tratativas").fadeIn();
            $(".div_json_robo").fadeIn();
            listaTratativasSortable();
        },
        error: function () {
            carregarListaUsuario(robo);
            carregarJsonRobo(robo);
            $("#div_configuracao_lista_tratativas").fadeIn();
            $(".div_json_robo").fadeIn();
        }
    });
}

function carregarJsonRobo(robo) {
    $.ajax({
        type: 'GET',
        url: 'configuracao/retorno_robo/json_robo/'+robo,
        dataType: 'json',
        success: function (data) {
            $("#json_robo").val(JSON.stringify(data, null, 2));
        }
    });
}

function carregarListaUsuario(robo) {
    $.ajax({
        type: 'GET',
        url: 'configuracao/retorno_robo/valores_select/user_id',
        dataType: 'json',
        success: function (data) {
            var html = "<option></option>";
            $(data).each(function(i, v) {
                html += "<option value='"+v['value']+"'>"+v['text']+"</option>";
            });
            $("#usuario_padrao").html(html);
            carregarUsuarioPadrao(robo);
        }
    });
}

function carregarUsuarioPadrao(robo) {
    $.ajax({
        type: 'GET',
        url: 'configuracao/retorno_robo/usuario_padrao/'+robo,
        dataType: 'json',
        success: function (data) {
            if (data != null) {
                $("#usuario_padrao").val(data);
                jsonConfiguracao.usuario_padrao = data;
            }
        }
    });
}

function getOpcoesSortable(icone_handle) {
    return {
        handle: 'i.'+icone_handle,
        // animation on drop
        onDrop: function  ($item, container, _super) {
            var $clonedItem = $('<li/>').css({height: 0});
            $item.before($clonedItem);
            $clonedItem.animate({'height': $item.height()});

            $item.animate($clonedItem.position(), function() {
                $clonedItem.detach();
                _super($item, container);
            });

            switch (icone_handle) {
                case 'ordenar_tratativas':
                    var ordem = [];
                    $($item).parent().children('li.list-group-item').each(function(i, v) {
                        ordem[i] = jsonConfiguracao.interface[$(v).attr('numero_item')];
                        $(v).attr('numero_item', i);
                    });
                    jsonConfiguracao.interface = [];
                    $(ordem).each(function(i, v) {
                        jsonConfiguracao.interface.push(v);
                    });
                break;
                case 'ordenar_regras':
                    var ordem = [];
                    $($item).parent().children('li.li_regra').each(function(i, v) {
                        ordem[i] = jsonConfiguracao.interface[tratativaEditada].regras[$(v).attr('numero_regra')];
                        $(v).attr('numero_regra', i);
                    });
                    jsonConfiguracao.interface[tratativaEditada].regras = [];
                    $(ordem).each(function(i, v) {
                        jsonConfiguracao.interface[tratativaEditada].regras.push(v);
                    });
                break;
                case 'ordenar_acoes':
                    var ordem = [];
                    var numero_regra = $($item).parent().parent().parent().parent().parent().parent().parent().attr('numero_regra');
                    $($item).parent().children('li.list-group-item').each(function(i, v) {
                        ordem[i] = jsonConfiguracao.interface[tratativaEditada].regras[numero_regra].acoes[$(v).attr('numero_item')];
                        $(v).attr('numero_item', i);
                    });
                    jsonConfiguracao.interface[tratativaEditada].regras[numero_regra].acoes = [];
                    $(ordem).each(function(i, v) {
                        jsonConfiguracao.interface[tratativaEditada].regras[numero_regra].acoes.push(v);
                    });
                break;
            }
        },

        // set $item relative to cursor position
        onDragStart: function ($item, container, _super) {
            var offset = $item.offset();
            pointer = container.rootGroup.pointer;

            adjustment = {
                left: pointer.left - offset.left,
                top: pointer.top - offset.top
            };

            _super($item, container);
        },
        onDrag: function ($item, position) {
            $item.css({
                left: position.left - adjustment.left,
                top: position.top - adjustment.top
            });
        }
    };
}