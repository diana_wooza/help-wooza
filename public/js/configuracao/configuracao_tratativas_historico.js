function carregarTratativa() {
    $(jsonTratativa.condicoes).each(function(i, v) {
        if (jsonTratativa.condicoes[i].length > 0) {
            criarGrupoCondicao(i);
            $(".btn_adicionar_grupo_condicoes").parent().css('margin-left', '2%');
            grupoCondicoes = i;
            $(v).each(function(j, item) {
                if (!item.excluido) {
                    $(".grupo_condicoes_"+grupoCondicoes).prepend(criarItemCondicao(item.campo, item.comparacao, item.valor, j, item.ativo));
                }
            })
        }
    });

    $(jsonTratativa.regras).each(function(i, v) {
        criarRegra(i, v.ativo, v.nome);
        $(v.validacoes).each(function(j, grupo) {
            if (grupo.length > 0) {
                if (j > 0) {
                    criarGrupoValidacao(i, j);
                }
                $(grupo).each(function(k, item) {
                    numeroRegra = i;
                    grupoValidacoes = j;
                    if (!item.excluido) {
                        $(".li_regra_"+i+" .grupo_validacoes_"+j).prepend(criarItemValidacao(item.campo, item.comparacao, item.valor, k, item.ativo));
                    }
                });
            }
        });

        $(v.acoes).each(function(n, v) {
            if (!v.excluido) {
                $(".li_regra_"+i).find('.btn_adicionar_acao').parent().before(criarItemAcao(v.acao, n, v.ativo));
            }
        })
        acoesSortable();
        
        $(v.dados_adicionais).each(function(d, v) {
            if (!v.excluido) {
                $(".li_regra_"+i).find('.btn_adicionar_dados_adicionais').parent().before(criarItemDadosAdicionais(v.campo, v.valor, d, v.ativo));
            }
        })
    });
    
    $("ol.lista_regras").sortable(getOpcoesSortable('ordenar_regras'));
    $("#div_configuracao_tratativas").fadeIn();

    $(jsonTratativa.condicoes).each(function(i, v) {
        calcularAlturaLinhaVertical($(".grupo_condicoes_"+i));
    });

    $(".content i").hide();
    $(".editar_tratativa").show();
    $(".exibir_tratativas").show();
    $(".fa-pencil").css('opacity', 0);
    $(".content .btn").hide();
    $(".botoes_grupo, .botoes_grupo_condicoes").hide();
    $(".row_titulo_regras .col-sm-2").html("&nbsp;");
}