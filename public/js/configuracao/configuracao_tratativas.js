var grupoCondicoes;
var grupoValidacoes;
var numeroRegra;
var tipoModal;

$(function() {
    $("#div_configuracao_tratativas").on('click', ".remover_item", function() {
        if (confirm("Remover item?")) {
            var linha = $(this).parent().parent().parent(); // .list-group-item
            var grupo = linha.parent(); // .list-group

            linha.fadeOut('normal', function() {
                if (linha.nextUntil('.list-group-item').length > 0) {
                    linha.nextUntil('.list-group-item').remove();
                }
                else {
                    if (linha.prevUntil('.list-group-item').length > 0) {
                        linha.prevUntil('.list-group-item').remove();
                    }
                }
                
                var item = linha.attr('numero_item');
                var numero_grupo = linha.parent().attr('numero_grupo');   

                if (grupo.parent().hasClass('well_condicoes')) {
                    jsonTratativa.condicoes[numero_grupo][item].excluido = true;
                }

                if (grupo.parent().hasClass('well_validacoes')) {
                    var numero_regra = grupo.parent().parent().parent().parent().parent().parent().attr('numero_regra');
                    jsonTratativa.regras[numero_regra].validacoes[numero_grupo][item].excluido = true;
                }

                if (grupo.parent().hasClass('well_acoes')) {
                    var numero_regra = grupo.parent().parent().parent().parent().parent().parent().attr('numero_regra');
                    jsonTratativa.regras[numero_regra].acoes[item].excluido = true;
                }

                if (grupo.parent().hasClass('well_dados_adicionais')) {
                    var numero_regra = grupo.parent().parent().parent().parent().parent().parent().attr('numero_regra');
                    jsonTratativa.regras[numero_regra].dados_adicionais[item].excluido = true;
                }

                linha.remove();

                if (grupo.find('.list-group-item').length < 2) {
                    if ((grupo.parent().parent().parent().hasClass('coluna_condicoes'))) {
                        removerGrupoCondicoes(grupo.parent());
                    }
                }

                calcularAlturaLinhaVertical(grupo);
            });
        }
    });

    $("#div_configuracao_tratativas").on('click', ".ativar_item", function() {
        var li = $(this).parent().parent().parent();
        li.toggleClass('desabilitado');
        $(this).toggleClass('fa-ban fa-check-square');
        
        if (li.parent().parent().hasClass('well_condicoes')) {
            var grupo = li.parent().attr('numero_grupo');
            var item = li.attr('numero_item');
            jsonTratativa.condicoes[grupo][item].ativo = !li.hasClass('desabilitado');
        }

        if (li.parent().parent().hasClass('well_validacoes')) {
            var regra = li.parent().parent().parent().parent().parent().parent().parent().attr('numero_regra');
            var grupo = li.parent().attr('numero_grupo');
            var item = li.attr('numero_item');
            jsonTratativa.regras[regra].validacoes[grupo][item].ativo = !li.hasClass('desabilitado');
        }

        if (li.parent().parent().hasClass('well_acoes')) {
            var regra = li.parent().parent().parent().parent().parent().parent().parent().attr('numero_regra');
            var item = li.attr('numero_item');
            jsonTratativa.regras[regra].acoes[item].ativo = !li.hasClass('desabilitado');
        }

        if (li.parent().parent().hasClass('well_dados_adicionais')) {
            var regra = li.parent().parent().parent().parent().parent().parent().parent().attr('numero_regra');
            var item = li.attr('numero_item');
            jsonTratativa.regras[regra].dados_adicionais[item].ativo = !li.hasClass('desabilitado');
        }

    });

    $("#div_configuracao_tratativas").on('click', ".remover_grupo_condicoes", function() {
        if (confirm("Remover grupo?")) {
            var coluna = $(this).parent().parent().parent().parent().parent();

            var numero_grupo = coluna.find('.list-group').attr('numero_grupo');
            jsonTratativa.condicoes[numero_grupo] = [];

            removerGrupoCondicoes(coluna);
        }
    });

    $("#div_configuracao_tratativas").on('click', ".remover_grupo_validacoes", function() {
        if (confirm("Remover grupo?")) {
            var grupo = $(this).parent().parent().parent().parent();
            var regra = grupo.parent().parent().parent().parent().parent().parent();

            var numero_grupo = grupo.attr('numero_grupo');
            var numero_regra = regra.attr('numero_regra');
            
            jsonTratativa.regras[numero_regra].validacoes[numero_grupo] = [];

            removerGrupoValidacoes(grupo);
        }
    });

    $("#div_configuracao_tratativas").on('click', ".excluir_regra", function() {
        if (confirm("Remover regra?")) {
            var regra = $(this).parent().parent().parent();
            var numero_regra = regra.attr('numero_regra');

            jsonTratativa.regras[numero_regra].excluido = true;
            
            regra.fadeOut('normal', function() {
                regra.remove();
            });
        }
    });

    $("#div_configuracao_tratativas").on('click', ".desabilitar_regra", function() {
        var regra = $(this).parent().parent().parent();
        regra.toggleClass('desabilitado');
        var numero_regra = regra.attr('numero_regra');
        jsonTratativa.regras[numero_regra].ativo = !regra.hasClass('desabilitado');
        $(this).toggleClass('fa-ban fa-check-square');
    });

    $("#div_configuracao_tratativas").on('click', ".editar_regra", function() {
        $(this).parent().prev().html("<input type='text' onFocus='this.select();' class='input_editar_regra' value="+$(this).parent().prev().html()+">");
        $(".input_editar_regra").focus();
    });

    $("#div_configuracao_tratativas").on('click', '.row_tratativa .editar_tratativa', function() {
        var label = $(this).parent().prev().find('.label_nome_tratativa');
        label.html("<input type='text' onFocus='this.select();' class='input_editar_tratativa' value='"+label.html()+"'>");
        $(".input_editar_tratativa").focus();
    });

    $("#div_configuracao_tratativas").on("keypress", ".input_editar_regra", function(e) {
        if(e.which == 13) {
            var regra = $(this).parent().parent().parent();
            var numero_regra = regra.attr('numero_regra');
            jsonTratativa.regras[numero_regra].nome = $(this).val();
            $(this).parent().html($(this).val());
        }
    });

    $("#div_configuracao_tratativas").on("keypress", ".input_editar_tratativa", function(e) {
        if(e.which == 13) {
            jsonTratativa.nome = $(this).val();
            $(this).parent().html($(this).val());
        }
    });

    $('.modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var botao = button.data('btn'); // Extract info from data-* attributes
        var modal = $(this);
        tipoModal = botao;

        $(".valor_modal").fadeOut();

        var titulo_modal;
        switch (botao) {
            case 'adicionar_condicao':
                titulo_modal = 'Adicionar Condição';
                grupoCondicoes = button.data('num_grupo_condicoes');
                $(".comparacao_modal").hide();
                $(".label_campo").html('Parâmetro do pedido');
                $.ajax({
                    type: 'GET',
                    url: 'configuracao/retorno_robo/campos_condicoes/'+$("#select_robo").val().split(".")[0],
                    dataType: 'json',
                    success: function (data) {
                        var html = "<option></option>";
                        $(data).each(function(i, v) {
                            html += "<option value='"+v['value']+"'>"+v['text']+"</option>";
                        });
                        $(".select_campo").html(html);
                    }
                });
            break;
            case 'adicionar_validacao':
                titulo_modal = 'Adicionar Validação';
                grupoValidacoes = button.data('num_grupo_validacoes');
                numeroRegra = button.data('numero_regra');
                $(".comparacao_modal").show();
                $(".label_campo").html('Campo do retorno do robô');
                $.ajax({
                    type: 'GET',
                    url: 'configuracao/retorno_robo/campos_robo/'+$("#select_robo").val(),
                    dataType: 'json',
                    success: function (data) {
                        var html = "<option></option>";
                        $(data).each(function(i, v) {
                            html += "<option value='"+v['value']+"'>"+v['text']+"</option>";
                        });
                        $(".select_campo").html(html);
                    }
                });
            break;
            case 'adicionar_dados_adicionais':
                titulo_modal = 'Adicionar Dados Adicionais';
                grupoValidacoes = button.data('num_grupo_validacoes');
                numeroRegra = button.data('numero_regra');
                $(".comparacao_modal").hide();
                $(".label_campo").html('Campo');
                $.ajax({
                    type: 'GET',
                    url: 'configuracao/retorno_robo/campos_dados_adicionais_interface',
                    dataType: 'json',
                    success: function (data) {
                        var html = "<option></option>";
                        $(data).each(function(i, v) {
                            html += "<option value='"+v['value']+"'>"+v['text']+"</option>";
                        });
                        $(".select_campo").html(html);
                    }
                });
            break;
            case 'adicionar_acao':
                titulo_modal = 'Adicionar Ações';
                grupoValidacoes = button.data('num_grupo_validacoes');
                numeroRegra = button.data('numero_regra');
                $(".comparacao_modal").hide();
                $(".label_campo").html('Ação');
                $.ajax({
                    type: 'GET',
                    url: 'configuracao/retorno_robo/acoes_interface/'+$("#select_robo").val(),
                    dataType: 'json',
                    success: function (data) {
                        var html = "<option></option>";
                        $(data).each(function(i, v) {
                            var descricao_acao = '';
                            if (descricoes_acoes[v['text']] != null) { 
                                var desc = descricoes_acoes[v['text']].split(',');
                                if (desc != undefined) {
                                    descricao_acao = " ("+desc.join("; ")+")";
                                }
                            }
                            html += "<option value='"+v['value']+"'>"+v['text']+descricao_acao+"</option>";
                        });
                        $(".select_campo").html(html);
                    }
                });
            break;
        }

        modal.find('.modal-title').text(titulo_modal);
    });

    $("#div_configuracao_tratativas").on('click', '.titulo_regra .fa-minus', function() {
        var icone = $(this);
        icone.parent().parent().next().slideUp('normal', function() {
            icone.toggleClass('fa-minus fa-plus');
        });
    });

    $("#div_configuracao_tratativas").on('click', '.titulo_regra .fa-plus', function() {
        var icone = $(this);
        icone.parent().parent().next().slideDown('normal', function() {
            icone.toggleClass('fa-minus fa-plus');
        });
    });

    $("#div_configuracao_tratativas").on('click', ".icone_regras", function() {
        var icone = $(this);
        if ($(this).hasClass('comprimir')) {
            $(".span_regra").each(function() {
                $(this).slideUp();
            });
            $(".titulo_regra > .fa-minus").each(function() {
                $(this).removeClass('fa-minus');
                $(this).addClass('fa-plus');
            });
            $(this).toggleClass('comprimir expandir');
            icone.toggleClass('fa-compress fa-expand');
        }
        else {
            $(".span_regra").each(function() {
                $(this).slideDown();
            });
            $(".titulo_regra > .fa-plus").each(function() {
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
            });
            $(this).toggleClass('comprimir expandir');
            icone.toggleClass('fa-compress fa-expand');
        }
        $(this).blur();
    });

    $(".select_campo").change(function() {
        var html = '';
        var options = {
            tokenSeparators: ['|',],
            language: "pt-BR",
            maximumSelectionLength: 0,
            matcher: function matcher (params, data) {
                // Always return the object if there is nothing to compare
                if ($.trim(params.term) === '') {
                  return data;
                }
          
                var original = data.text.toUpperCase();
                var term = params.term.toUpperCase();
          
                // Check if the text contains the term
                if (original.indexOf(term) > -1) {
                  return data;
                }
          
                // If it doesn't contain the term, don't return anything
                return null;
            }
        };

        var options2 = {
            tokenSeparators: ['|',],
            language: "pt-BR",
            maximumSelectionLength: 0,
            matcher: function matcher (params, data) {
                // Always return the object if there is nothing to compare
                if ($.trim(params.term) === '') {
                  return data;
                }
          
                var original = data.text.toUpperCase();
                var term = params.term.toUpperCase();
          
                // Check if the text contains the term
                if (original == term) {
                    return data;
                }
          
                // If it doesn't contain the term, don't return anything
                return null;
            }
        };

        if (tipoModal == 'adicionar_acao') {
            return;
        }

        $(".valor_modal").fadeOut('normal', function() {
            $(".carregando").fadeIn();
        });
        
        $.ajax({
            type: 'GET',
            url: 'configuracao/retorno_robo/valores_select/'+$(this).val(),
            dataType: 'json',
            success: function (data) {
                if ($(data).length > 0 && tipoModal != 'adicionar_validacao') {
                    $(data).each(function(i, v) {
                        html += "<option value="+v['value']+">"+v['text']+"</option>";
                    });

                    if (tipoModal == 'adicionar_dados_adicionais') {
                        options.maximumSelectionLength = 1;
                    }
                    $('#input_valor_modal').select2(options);
                    $("#input_valor_modal").html(html);
                    $(".carregando").fadeOut('normal', function() {
                        $(".valor_modal").fadeIn();
                        $(".select2-search__field").focus();

                        if (tipoModal == 'adicionar_dados_adicionais') {
                            $(".label_valor").html('Valor');
                        }
                        else {
                            $(".label_valor").html('Lista de valores');
                        }
                    });
                }
                else {
                    if (tipoModal == 'adicionar_dados_adicionais') {
                        options.maximumSelectionLength = 1;
                        options.tags = true;
                        $.ajax({
                            type: 'GET',
                            url: 'configuracao/retorno_robo/campos_robo/'+$("#select_robo").val(),
                            dataType: 'json',
                            success: function (data) {
                                var html = '';
                                $(data).each(function(i, v) {
                                    html += "<option value='"+v['value']+"'>"+v['text']+"</option>";
                                });
                                $('#input_valor_modal').select2(options);
                                $("#input_valor_modal").html(html);
                                $(".carregando").fadeOut('normal', function() {
                                    $(".valor_modal").fadeIn();
                                    $(".select2-search__field").focus();
                                    $(".label_valor").html('Valor');
                                });
                            }
                        });
                    }
                    else {
                        options2.tags = true;
                        // options2.dropdownCss = {'display': 'none'};
                        
                        $.ajax({
                            type: 'GET',
                            url: 'configuracao/retorno_robo/campos_comparativos_order',
                            dataType: 'json',
                            success: function (data) {
                                var html = '';
                                $(data).each(function(i, v) {
                                    html += "<option value='"+v['value']+"'>"+v['text']+"</option>";
                                });
                                $('#input_valor_modal').select2(options2);
                                $("#input_valor_modal").html(html);
                                $(".carregando").fadeOut('normal', function() {
                                    $(".valor_modal").fadeIn();
                                    $(".select2-search__field").focus();
                                    $(".label_valor").html('Lista de valores (Pressione enter para cada valor)');
                                });
                            }
                        });
                    }
                }
            }
        });
    });

    $(".salvar_campo_valor").click(function() {
        if (tipoModal != 'adicionar_acao') {
            var valores = [];
            var len = $('#input_valor_modal').select2('data').length;
            for (var i=0;i<len;i++) {
                valores.push($('#input_valor_modal').select2('data')[i].text);
            }
        }

        switch (tipoModal) {
            case 'adicionar_condicao':
                var comparacao;
                switch ($("#select_campo").val()) {
                    case 'Origem do Pedido':
                    case 'Nome do Plano':
                        comparacao = '&cong;';
                    break;
                    default:
                        comparacao = '=';
                    break;
                }

                $(".grupo_condicoes_"+grupoCondicoes).prepend(criarItemCondicao($("#select_campo").val(), comparacao, valores));
                jsonTratativa.condicoes[grupoCondicoes].push(
                {
                    "campo": $("#select_campo").val(),
                    "valor": valores,
                    "comparacao": comparacao,
                    "ativo": true
                });
                calcularAlturaLinhaVertical($(".grupo_condicoes_"+grupoCondicoes));
            break;
            case 'adicionar_validacao':
                $(".li_regra_"+numeroRegra+" .grupo_validacoes_"+grupoValidacoes).prepend(criarItemValidacao($("#select_campo").val(), $("#input_comparacao_modal").val(), valores));
                jsonTratativa.regras[numeroRegra].validacoes[grupoValidacoes].push(
                    {
                        "campo": $("#select_campo").val(),
                        "valor": valores,
                        "comparacao": $("#input_comparacao_modal").val(),
                        "ativo": true
                    }
                );
            break;
            case 'adicionar_dados_adicionais':
                $(".li_regra_"+numeroRegra).find('.btn_adicionar_dados_adicionais').parent().before(criarItemDadosAdicionais($("#select_campo").val(), $('#input_valor_modal').select2('data')[0].text));
                jsonTratativa.regras[numeroRegra].dados_adicionais.push(
                    {
                        "campo": $("#select_campo").val(),
                        "valor": $('#input_valor_modal').select2('data')[0].text,
                        "ativo": true
                    }
                );
            break;
            case 'adicionar_acao':
                $(".li_regra_"+numeroRegra).find('.btn_adicionar_acao').parent().before(criarItemAcao($("#select_campo").val()));
                jsonTratativa.regras[numeroRegra].acoes.push(
                    {
                        "acao": $("#select_campo").val(),
                        "ativo": true
                    }
                );
                acoesSortable();
            break;
        }

        $(".modal").modal("hide");
    });

    $("#div_configuracao_tratativas").on('click', ".btn_adicionar_grupo_condicoes", function() {
        criarGrupoCondicao();
        $(this).parent().css('margin-left', '2%');
        jsonTratativa.condicoes.push([]);
    });

    $("#div_configuracao_tratativas").on('click', ".btn_adicionar_grupo_validacoes", function() {
        var numero_regra = $(this).parent().parent().parent().parent().parent().attr('numero_regra');
        criarGrupoValidacao(numero_regra);
        jsonTratativa.regras[numero_regra].validacoes.push([]);
    });

    $("#div_configuracao_tratativas").on('click', ".btn_adicionar_regra", function() {
        criarRegra();
        jsonTratativa.regras.push(
            {
                "nome": "Regra",
                "ativo": true,
                "validacoes": [[]],
                "acoes": [],
                "dados_adicionais": [],
            }
        );
        $(this).blur();
    });
});

function acoesSortable() {
    $("ol.lista_acoes").sortable('destroy').sortable(getOpcoesSortable('ordenar_acoes'));
}

function carregarTratativa() {
    $(jsonTratativa.condicoes).each(function(i, v) {
        if (jsonTratativa.condicoes[i].length > 0) {
            criarGrupoCondicao(i);
            $(".btn_adicionar_grupo_condicoes").parent().css('margin-left', '2%');
            grupoCondicoes = i;
            $(v).each(function(j, item) {
                if (!item.excluido) {
                    $(".grupo_condicoes_"+grupoCondicoes).prepend(criarItemCondicao(item.campo, item.comparacao, item.valor, j, item.ativo));
                }
            })
        }
    });

    $(jsonTratativa.regras).each(function(i, v) {
        criarRegra(i, v.ativo, v.nome);
        $(v.validacoes).each(function(j, grupo) {
            if (grupo.length > 0) {
                if (j > 0) {
                    criarGrupoValidacao(i, j);
                }
                $(grupo).each(function(k, item) {
                    numeroRegra = i;
                    grupoValidacoes = j;
                    if (!item.excluido) {
                        $(".li_regra_"+i+" .grupo_validacoes_"+j).prepend(criarItemValidacao(item.campo, item.comparacao, item.valor, k, item.ativo));
                    }
                });
            }
        });

        $(v.acoes).each(function(n, v) {
            if (!v.excluido) {
                $(".li_regra_"+i).find('.btn_adicionar_acao').parent().before(criarItemAcao(v.acao, n, v.ativo));
            }
        })
        acoesSortable();
        
        $(v.dados_adicionais).each(function(d, v) {
            if (!v.excluido) {
                $(".li_regra_"+i).find('.btn_adicionar_dados_adicionais').parent().before(criarItemDadosAdicionais(v.campo, v.valor, d, v.ativo));
            }
        })
    });
    
    $("ol.lista_regras").sortable(getOpcoesSortable('ordenar_regras'));
    $("#div_configuracao_tratativas").fadeIn();

    $(jsonTratativa.condicoes).each(function(i, v) {
        calcularAlturaLinhaVertical($(".grupo_condicoes_"+i));
    });
}

function ajustarGrid() {
    var primeira_coluna = $(".row_condicoes").find('.coluna_condicoes').first();
    ajustarColuna(primeira_coluna);

    $('.clearfix').each(function(i, v) {
        ajustarColuna($(v).next('.coluna_condicoes'));
    });
}

function ajustarColuna(coluna) {
    coluna.find('.coluna_linha_vertical').first().remove();
    
    var coluna_ajustada = coluna.find('.col-sm-11');
    coluna_ajustada.removeClass('col-sm-11');
    coluna_ajustada.addClass('col-sm-12');
}

// grupo = .list-group
function calcularAlturaLinhaVertical(grupo) {
    var div_linha = grupo.parent().parent().prev();

    if (div_linha.hasClass('col-sm-1')) {
        var altura = grupo.parent().height();
        div_linha.find('.above').css('height', altura/2);
        div_linha.find('.below').css('height', altura/2);
    }
}

function removerGrupoCondicoes(grupo) {
    grupo.fadeOut('normal', function() {
        grupo.parent().parent().parent().remove();
        ajustarGrid();
    });
}

function removerGrupoValidacoes(grupo) {
    grupo = grupo.parent(); // well
    grupo.fadeOut('normal', function() {
        var linha = grupo.next();
        if (linha.hasClass('texto_linha')) {
            linha.remove();
        }
        else {
            var linha = grupo.prev();
            if (linha.hasClass('texto_linha')) {
                linha.remove();
            }
        }
        grupo.remove();
    });
}