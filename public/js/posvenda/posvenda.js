var _CLICK_PEDIDO_ = false;

$(document).ready( function(){
    let areaBusca   = $('#area-busca');
    validarCamposBusca();
    validarCheckboxes();

    $('.btn_modal').click(function() {
        $('.modal_chamado').html($(this).val());
    });

    $('#btn-busca').click( () => {
        if( areaBusca.is(':visible') ) areaBusca.hide();
        else areaBusca.show();
    } );

    $('#btn-nova-busca').click( () => {
        location.href = _URL_ + '/registrar_atendimentos';
    } );

    $('#cpf_busca,#pedido_busca').keyup( () => {
        validarCamposBusca();
    } );

    $('#cpf_busca,#pedido_busca').blur( () => {
        validarCamposBusca();
    } );

    $('#cpf_busca,#pedido_busca').change( () => {
        validarCamposBusca();
    } );

    $('#motivo1').change( () => {
        buscaMotivos( $('#motivo1').val() );
    } );

    $('#acao').change( (e) => {
        if( $(e.currentTarget).val() == 44 ) $('#codCancelamento').prop('disabled', false).prop('required', true);
        else if( $('#codCancelamento').is(':enabled') ) $('#codCancelamento').prop('disabled', true).prop('required', false);
    } )
} );

function abrirModalTextoChamado( texto ) {
    $('.modal_chamado').html( texto );
}

function buscaMotivos( motivo ) {
    if( 0 === motivo.length ) return;

    $.ajax({
        url: _URL_ + '/search_mortivos/' + motivo,
        type: 'GET',
        dataType: "json",
        beforeSend: function(){
            $('.loader').show();
        },
        complete: function(){
            $('.loader').hide();
        },
        success: function( data ) {
            preencherCampo( '#motivo2',data.model );
        }
    });
}

function buscaOrigens( param ) {
    $.ajax({
        url: _URL_ + '/busca_origens/?tipo=' + param,
        type: 'GET',
        dataType: "json",
        beforeSend: function(){
            $('.loader').show();
        },
        complete: function(){
            $('.loader').hide();
        },
        success: function( data ) {
            preencheCampoOrigem( data.model );
        }
    });
}

function preencherCampo( $campo, data, label = 'label' ) {
    let html = [];
    html.push("<option value=\"\" selected>Selecione</option>");
    data.forEach( function( valor, chave, mapa ) {
        html.push("<option value=\"" + valor.value + "\">" + valor[label] + "</option>");
    } )

    $($campo).empty().html( html.join(' ') );
}

function preencheCampoOrigem( data ) {
    let html = [];
    html.push("<option value=\"\">Selecione</option>");
    data.forEach( function( valor, chave, mapa ) {
        html.push("<option value=\"" + valor.id + "\">" + valor.origem + "</option>");
    } )

    $('#origem').empty().html( html.join(' ') );
}

function validarCamposBusca() {
    $('#btn-enviar-busca').prop('disabled', !( 11 <= $('#cpf_busca').val().length || 5 < $('#pedido_busca').val().length ));
}

function abrirModalAtendimentoMagento( pedido, telefone, email, cpf, cliente, operadora, origem = null, isPedido = true ) {
    _CLICK_PEDIDO_ = isPedido;
    let modal = $('#modal-cadastro');
    closeAlerta();
    if( (modal.data('bs.modal') || {}).isShown ) modal.off();

    origem      = (null === origem) ? $('#origem_busca').val() : origem;
    pedido      = ($('#pedidoSelecionada').val()) ? $('#pedidoSelecionada').val() : pedido;

    preencherCamposModal( pedido, telefone, email, cpf, cliente, operadora.toUpperCase(), origem );
    modal.modal('show');
    buscaOrigens( 0 );
}

function abrirModalAtendimentoAvulso( pedido, telefone, email, cpf, cliente, operadora, origem = null, isPedido = false ) {
    _CLICK_PEDIDO_ = isPedido;
    let modal = $('#modal-cadastro');
    closeAlerta();
    if( (modal.data('bs.modal') || {}).isShown ) modal.off();

    origem      = (null === origem) ? $('#origem_busca').val() : origem;
    pedido      = ($('#pedidoSelecionada').val()) ? $('#pedidoSelecionada').val() : pedido;

    preencherCamposModal( pedido, telefone, email, cpf, cliente, operadora.toUpperCase(), origem );
    modal.modal('show');
    buscaOrigens( 1 );
}

function preencherCamposModal( pedido, telefone, email, cpf, cliente, operadora, origem ) {
    $('#nome_cliente').val( cliente );
    $('#pedido').val( pedido );
    $('#cpf').val( cpf );
    $('#telefone').val(telefone);
    $('#email').val(email);
    $('#operadora').val( $('#operadora option').filter(function () { 
                                                            return $(this).html().trim() == operadora; 
                                                        }).val() );
    $('#origem').val( origem );
}

function fecharModalAtendimento() {
    let modal = $('#modal-cadastro');
    closeAlerta();

    $('#motivo2,#motivo1,#operadora,#canal_atendimento,#origem,#id_atendimento,#chamado,#acao,#nome_cliente,#pedido,#cpf,#codCancelamento').val('');
    $('#check_cliente,#check_cpf').prop('checked',false);
    $('#codCancelamento').prop('disabled', true);


    modal.modal('hide').on('hidden.bs.modal', () => {
        modal.off();
    });
}

function buscarAtendimentosPorPedido( pedido, operadora ) {
    if( $('#dvped_' + pedido).is(':visible') ) {
        $('#dvped_' + pedido).toggle(false);

        return;
    }


    $('#nenhum_' + pedido).remove();
    var no_record = false;
    $.ajax({
        type:'GET',
        url: _URL_ + '/buscar_atendimento_por_motivo/?pedido=' + pedido,
        beforeSend: function(){
            $('.loader').show();
        },
        complete: function(){
            $('.loader').hide();
        },
        success:function(data){
            $('#trped_' + pedido).empty().html('<td colspan="11"><div id="dvped_' + pedido + '"  class="collapse" /></td>');
            let html = [];
            let rows = [];
                html.push('<table  class="table table-striped table-bordered">');

            if( data.model.length ) {
                html.push('<thead><tr><td colspan="10">\
                                <button class="btn btn-border-danger pull-right" style="margin-top: 15px !important;" type="button" onclick="abrirModalAtendimentoMagento(\''+pedido+'\',\''+data.model[0].numero_telefone+'\',\''+data.model[0].email+'\', \''+data.model[0].cpf_cliente+'\', \''+data.model[0].nome_cliente+'\', \''+data.model[0].operadora.operadora_produto+'\', null, true)">\
                                    Novo Atendimento &nbsp;&nbsp;&nbsp;<i class="fa fa-plus-square"></i>\
                                </button>\
                            </td></tr></thead>');
                
                data.model.forEach(function( valor, chave, mapa ) {
                    if( valor ) {
                        let canal       = (valor.canal)         ? valor.canal.canal_atendimento     : "";
                        let origem      = (valor.origem)        ? valor.origem.origem               : "";
                        let operadora   = (valor.operadora)     ? valor.operadora.operadora_produto : "";
                        let motivo1     = (valor.motivo1)       ? valor.motivo1.motivo              : "";
                        let motivo2     = (valor.motivo2)       ? valor.motivo2.motivo              : "";
                        let acao        = (valor.acao)          ? valor.acao.acao                   : "";
                        let npedido     = (valor.numero_pedido) ? valor.numero_pedido               : "";

                        rows.push("<tr>\
                            <td>\
                                <center>\
                                    <button type=\"button\" class=\"btn btn-info btn-xs btn_modal\" data-toggle=\"modal\" value=\"" + valor.chamado + "\" data-target=\"#myModal\">\
                                        <i class=\"fa fa-eye\"></i>\
                                    </button>\
                                </center>\
                            </td>\
                            <td>" + formatDateTime(valor.created_at, false) + "</td>\
                            <td>" + npedido + "</td>\
                            <td>" + canal + "</td>\
                            <td>" + origem + "</td>\
                            <td>" + operadora + "</td>\
                            <td>" + motivo1 + "</td>\
                            <td>" + motivo2 +  "</td>\
                            <td>" + acao + "</td>\
                            <td>" + valor.protocolo + "</td>\
                        </tr>");
                    }
                });

                $('#btn_camposNaoInformados,#btn_preencherTudo,#btn_abandono').prop('disabled', true);
            }
            else no_record = '<td  id="nenhum_' + pedido + '" colspan="10"><p><center style="color: red;">Nenhum atendimento relacionado a este pedido.</center></p></td>';

            if( false !== no_record ) {
                html.push('<thead><tr><td colspan="10">\
                                <button class="btn btn-border-danger pull-right" style="margin-top: 15px !important;" type="button" onclick="abrirModalAtendimentoMagento(\''+pedido+'\', \''+$("#telefone_c").html()+'\', \''+$("#email_c").html()+'\', \''+$("#cpf_c").html()+'\', \''+$("#nome_c").html()+'\', \''+$("#operadora_"+pedido).html()+'\', null, true)">\
                                    Novo Atendimento &nbsp;&nbsp;&nbsp;<i class="fa fa-plus-square"></i>\
                                </button>\
                            </td></tr></thead>');
            }
            html.push('<thead>\
                                <tr class="label_posvenda">\
                                    <td><center>#</center></td>\
                                    <td>Data e Hora</td>\
                                    <td>Canal</td>\
                                    <td>Origem Cliente</td>\
                                    <td>Operadora</td>\
                                    <td>Motivo 1</td>\
                                    <td>Motivo 2</td>\
                                    <td>Ação</td>\
                                    <td>Protocolo</td>\
                                </tr>\
                            </thead>\
                            <tbody>');
            
            if( false !== no_record ) html.push(no_record);

            html.push(rows.join(' '));
            html.push(' </tbody>');
            html.push('</table>');

            $('#dvped_' + pedido).empty().html( html.join(' ') );
            $('#dvped_' + pedido).toggle('slow');
            $('#operadoraSelecionada').val(operadora);
        }
    });
}

function formatDateTime( date, hasTime = true ) {
    if( 0 == date.length ) return '';

    let data  = date.split(' ')
    dt      = data[0].split('-');
    time    = data[1];

    return ( true == hasTime ) ? dt[2] + '/' + dt[1] + '/' + dt[0] + ' ' + time : dt[2] + '/' + dt[1] + '/' + dt[0];
}

function preSalvamento() {
    if( false === $("#form-cadastro")[0].reportValidity() ) return false;
    $('#confirm').off();
    $('#confirm_valida').off();
    if( $('#acao').val() == 44 ) {
        $('#confirm').modal({
            backdrop: 'static',
            keyboard: false
        }).on('click', '#sim', function(e) {
            salvarAtendimento();
        }).on('click', '#nao', function(e) {
            return;
        });
    }
    else {
        if( false == validarCamposLivres() ) {
            $('#confirm_valida').modal({
                backdrop: 'static',
                keyboard: false
            }).on('click', '#sim', function(e) {
                salvarAtendimento();
            }).on('click', '#nao', function(e) {
                return;
            });
        }
        else salvarAtendimento();
    }
}

function callModalValidacao( mensagem ) {
    $('#textoValidacao').html(mensagem);
    $('#modalValidacao').modal('show');
}

function salvarAtendimento() {
    closeAlerta();
    if( false === $("#form-cadastro")[0].reportValidity() ) return false;

    let _origem             = $('#origem').val();
    let _canal              = $('#canal_atendimento').val();
    let _pedido             = $('#pedido').val();
    let _cpf                = $('#cpf').val();
    let _nome_cliente       = $('#nome_cliente').val();
    let _operadora          = $('#operadora').val();
    let _motivo1            = $('#motivo1').val();
    let _motivo2            = $('#motivo2').val();
    let _acao               = $('#acao').val();
    let _id_atendimento     = $('#id_atendimento').val();
    let _chamado            = $('#chamado').val();
    let _ck_cliente         = $('#check_cliente').is(':checked');
    let _ck_cpf             = $('#check_cpf').is(':checked');
    let _ck_telefone        = $('#check_telefone').is(':checked');
    let _ck_email           = $('#check_email').is(':checked');
    let _codCancelamento    = $('#codCancelamento').val();
    let _telefone           = $('#telefone').val();
    let _email              = $('#email').val();
    let html                = [];

    if( 44 == _acao  && ( 0 == _pedido.length  || 'Não Cliente' == _pedido ) ) {
        callModalValidacao( 'Impossível solicitar o cancelamento do pedido com o campo pedido em branco, favor preencher ou rever a ação selecionada.' );

        return false;
    }

    if( 0 == _acao.length) {
        $('#acao')[0].reportValidity();

        return false;
    }

    $.ajax({
            type:'POST',
            url: _URL_ + '/atendimentos.salvar',
            data: {
                "_token"        : "{{ csrf_token() }}",
                origem          : _origem,
                canal           : _canal,
                pedido          : _pedido,
                cpf             : _cpf,
                nome_cliente    : _nome_cliente,
                operadora       : _operadora,
                motivo1         : _motivo1,
                motivo2         : _motivo2,
                acao            : _acao,
                id_atendimento  : _id_atendimento,
                chamado         : _chamado,
                check_cliente   : _ck_cliente,
                check_cpf       : _ck_cpf,
                check_email     : _ck_email,
                check_telefone  : _ck_telefone,
                codCancelamento : _codCancelamento,
                telefone        : _telefone,
                email           : _email
            },
            beforeSend: function(){
                $('.loader').show();
            },
            complete: function(){
                $('.loader').hide();
            },
            success: function(data){
                let valor = data.model;

                if( false === valor ) callAlerta("Não foi possível registrar o atendimento.", 'alert-error');
                else {
                    html.push("<tr>\
                        <td>\
                            <center><button type=\"button\" class=\"btn btn-info btn-xs\"  data-toggle=\"modal\" onclick=\"abrirModalTextoChamado('"+valor.chamado+"');\" value=\""+ valor.chamado +"\" data-target=\"#myModal\"><i class=\"fa fa-eye\"></i></button></center>\
                        </td>\
                        <td>" + formatDateTime(valor.created_at) + "</td>\
                        <td>" + valor.numero_pedido  + "</td>\
                        <td>" + valor.canal.canal_atendimento  + "</td>\
                        <td>" + valor.origem.origem  + "</td>\
                        <td>" + valor.operadora.operadora_produto  + "</td>\
                        <td>" + valor.motivo1.motivo  + "</td>\
                        <td>" + valor.motivo2.motivo  +  "</td>\
                        <td>" + valor.acao.acao  + "</td>\
                        <td>" + valor.protocolo + "</td>\
                    </tr>");

                    let nenhum = $('#nenhum');
                    let container = $('#resultado_chamados');

                    nenhum.remove();
                    container.prepend( html.join(' ') );
                    
                    $('#canal_atendimento,#acao,#motivo1,#motivo2,#origem').find('option:eq(0)').prop('selected', true);
                    $('#id_atendimento,#chamado').val('');
                    $('#id_atendimento').val('');

                    let styleMessage = 'alert-success';

                    switch( data.resultado.status ) {
                        case 200:
                            styleMessage = 'alert-success';
                        break;
                        case 403:
                            styleMessage = 'alert-error';
                        break;
                        case 500:
                            styleMessage = 'alert-warning';
                        break;
                    }

                    callAlerta(data.resultado.mensagemAtendimento, styleMessage);

                    let retornoApi = data.resultado.retornoApi;

                    if( true === _CLICK_PEDIDO_ ) {
                        if( retornoApi != false && retornoApi != null ){
                            if( retornoApi.status == 200 ) callAlerta(retornoApi.mensagem, 'alert-success', '#alertaApi');
                            else callAlerta(retornoApi.mensagem, 'alert-error', '#alertaApi');
                        }
                        else {
                            callAlerta("Erro na integração com o magento. Favor verificar o pedido manualmente.", 'alert-error', '#alertaApi');
                        }
                    }

                    if( true === _CLICK_PEDIDO_ && valor.numero_pedido != 'Não Cliente' ) buscarAtendimentosPorPedido( valor.numero_pedido, valor.operadora.operadora_produto );
                }
            },
            error : function(result, status, message) {
                callAlerta(result.responseJSON.message, 'alert-error');
            }
        });
}

function callAlerta(mensagem, estilo, nome = '#alerta') {
    let alerta      = $(nome);
    let amensagem   = $(nome + '-message');

    amensagem.html( mensagem ).removeClass('alert-success').removeClass('alert-error').removeClass('alert-danger').addClass(estilo);
    alerta.show('slow');
}

function closeAlerta() {
    $('#alerta,#alertaApi').hide('slow');
}

function validarCheckboxes() {
    $('#check_cliente').on('ifChanged', function(event){
        if ($('#check_cliente').prop("checked"))
            $("input[name=pedido").val("Não Cliente").attr("disabled", true);
        else
            $("input[name=pedido").val('').attr("disabled", false);
    });
    
    $('#check_cpf').on('ifChanged', function(event){
        if ($('#check_cpf').prop("checked"))
            $("input[name=cpf").val("Não Informado").attr("disabled", true);
        else 
            $("input[name=cpf").val('').attr("disabled", false);
    });

    $('#check_telefone').on('ifChanged', function(event){
        if ($('#check_telefone').prop("checked"))
            $("input[name=telefone").val("Não Informado").attr("disabled", true);
        else 
            $("input[name=telefone").val('').attr("disabled", false);
    });

    $('#check_email').on('ifChanged', function(event){
        if ($('#check_email').prop("checked"))
            $("input[name=email").val("Não Informado").attr("disabled", true);
        else 
            $("input[name=email").val('').attr("disabled", false);
    });
}

function preencherTudo() {
    $('#canal_atendimento').val(2);
    $('#pedido,#nome_cliente,#motivo2').val('Contingência/Backlog');
    $('#cpf').val('Backlog');

    $('#acao').val(43);
    $('#motivo1').val(1);
    $('#motivo2').html('<option value="80" selected>Contingência/Backlog</option>').val(80);
}

function checkCamposAbandono() {
    // if( false == $('#check_cliente').prop("checked") ) {
    //     $('#check_cliente').prop("checked", true);
    //     $("input[name=pedido").val("Não Cliente").attr("disabled", true);
    // }
    // else {
    //     $('#check_cliente').prop("checked", false);
    //     $("input[name=pedido").val("Não Cliente").attr("disabled", false);
    // }

    $("input[name=pedido").val("Não Informado");

    if( false == $('#check_cpf').prop("checked") ) {
        $('#check_cpf').prop("checked", true);
        $("input[name=cpf").val("Não Informado").attr("disabled", true);
    }
    else {
        $('#check_cpf').prop("checked", false);
        $("input[name=cpf").val("Não Informado").attr("disabled", false);
    }

    if( false == $('#check_telefone').prop("checked") ) {
        $('#check_telefone').prop("checked", true);
        $("input[name=telefone").val("Não Informado").attr("disabled", true);
    }
    else {
        $('#check_telefone').prop("checked", false);
        $("input[name=telefone").val("Não Informado").attr("disabled", false);
    }

    $('#nome_cliente').val('Não Informado');

    if( false == $('#check_email').prop("checked") ) {
        $('#check_email').prop("checked", true);
        $("input[name=email").val("Não Informado").attr("disabled", true);
        $("#motivo1").html('<option value="1" selected>Informação</option>').val(1);
        $("#motivo2").html('<option value="48" selected>Não Informado</option>').val(48);
        $("#acao").html('<option value="24" selected>Abandono</option>').val(24);
    }
    else {
        $('#check_email').prop("checked", false);
        $("input[name=email").val("Não Informado").attr("disabled", false);
        $("#motivo1").html('<option value="1" selected>Informação</option>').val(1);
        $("#motivo2").html('<option value="48" selected>Não Informado</option>').val(48);
        $("#acao").html('<option value="24" selected>Abandono</option>').val(24);
    }
}

function checkCamposNaoInformado() {
    if( false == $('#check_cliente').prop("checked") ) {
        $('#check_cliente').prop("checked", true);
        $("input[name=pedido").val("Não Cliente").attr("disabled", true);
    }
    else {
        $('#check_cliente').prop("checked", false);
        $("input[name=pedido").val("Não Cliente").attr("disabled", false);
    }

    if( false == $('#check_cpf').prop("checked") ) {
        $('#check_cpf').prop("checked", true);
        $("input[name=cpf").val("Não Informado").attr("disabled", true);
    }
    else {
        $('#check_cpf').prop("checked", false);
        $("input[name=cpf").val("Não Informado").attr("disabled", false);
    }

    if( false == $('#check_telefone').prop("checked") ) {
        $('#check_telefone').prop("checked", true);
        $("input[name=telefone").val("Não Informado").attr("disabled", true);
    }
    else {
        $('#check_telefone').prop("checked", false);
        $("input[name=telefone").val("Não Informado").attr("disabled", false);
    }

    if( false == $('#check_email').prop("checked") ) {
        $('#check_email').prop("checked", true);
        $("input[name=email").val("Não Informado").attr("disabled", true);
        $("#motivo2").html('<option value="48" selected>Não Informado</option>').val(48);
    }
    else {
        $('#check_email').prop("checked", false);
        $("input[name=email").val("Não Informado").attr("disabled", false);
        $("#motivo2").html('<option value="" selected>Selecione</option>').val('');
    }
    
    //$('#btn_camposNaoInformados').prop('disabled', true);
}

function buscaComentarios( entity_id, pedido ) {
    $('#resultado_history').empty();
    $.ajax({
        type:'GET',
        url: _URL_ + '/busca_comentarios/' + entity_id,
        beforeSend: function(){
            $('.loader').show();
        },
        complete: function(){
            $('.loader').hide();
        },
        success:function(data){
            if( data.model.length ) data.model.forEach(completarHistorico);
            abrirModalHistorico( pedido );
        }
    });
}

function abrirModalHistorico( pedido ) {
    let modal = $('#modal-history');
    $('#title-historico').html('Históricos do Pedido: ' + pedido + '');

    if( (modal.data('bs.modal') || {}).isShown ) modal.off();

    modal.modal('show');

    return;
}

function completarHistorico( valor, chave, mapa ) {
    let html = [];
    let usuario = (valor.usuario != undefined ) ? valor.usuario.firstname + " " + valor.usuario.lastname : '';
    if( valor ) {
        html.push("<tr>\
            <td>" + usuario +  "</td>\
            <td>" + valor.status + "</td>\
            <td>" + nl2br(valor.comment) + "</td>\
            <td>" + formatDateTime(valor.created_at) + "</td>\
        </tr>");
    }

    $('#resultado_history').prepend( html.join(' ') );
}

function nl2br (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) return '';
    
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';

    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function abrirNota( id ) {
    let abrir = $('#dvnote_' + id + ',#trnote_' + id).is(':visible');

    $('.dvcontainer').toggle(false);
    if( false === abrir ) $('#dvnote_' + id + ',#trnote_' + id).toggle();
}

function validarCamposLivres() {
    let pedido = $('#pedido');
    let tel = $('#telefone');
    let email = $('#email');
    let cpf = $('#cpf');
    let id_atendimento = $('#id_atendimento');
    
    if( 
        0 == pedido.val().length ||
        0 == tel.val().length ||
        0 == email.val().length ||
        0 == cpf.val().length ||
        0 == id_atendimento.val().length
    ) return false; 

    return true;
}
