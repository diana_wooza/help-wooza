var cadastrarApi = {

    criarSelectChannel: function(option, camposOption){
       var option = option.options[option.selectedIndex].value;
    //    console.log(camposOption);
// return;
       xhttp = new XMLHttpRequest();
       xhttp.onreadystatechange = function () {
           /* Se tudo ocorrer bem na consulta envio o resultado para verificar os status por api*/
           if (this.readyState == 4 && this.status == 200) {
               
               var resposta = this.responseText,
                   i, opt, valor,
                //    camposOption = document.getElementById("inputChannel"),
                   obj = JSON.parse(resposta);

                   if(resposta != false){
                       camposOption.innerHTML = null; // limpa o select
                  
                   // inseri o campo selecione
                   opt = document.createElement("OPTION"); // cria o elemento
                   opt.setAttribute("value", 0);
                   valor = document.createTextNode("Selecione");
                   opt.appendChild(valor);
                   camposOption.appendChild(opt); // popula
                //    camposOption.text="@if(old('channel') == 0') selected @endif";
   
                   obj.forEach(function(value, index){

                       opt = document.createElement("OPTION");
                       opt.setAttribute("value", value);
                       valor = document.createTextNode(value);
                       opt.appendChild(valor);
                    //    opt.appendChild("@if(old('channel') == "+value+") selected @endif");
                    //    opt.value = "@if(old('channel') == "+value+") selected @endif";
                       camposOption.appendChild(opt);
                   });
               } else {
                   console.log('Not channel');
               }
           } else {
               return "false";
           }
       };
       xhttp.open("POST", "getChannel", true);
       xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
       xhttp.send("&collection="+option);
    }
}


var btnCadastrar           = document.getElementById('btnCadastrar'),
    btnPessoasSms          = document.getElementById("pessoas-sms"),
    selectSegmento         = document.getElementById("select-segmento"),
    select2                = document.getElementsByClassName("select2"),
    selectItensAlarmantes  = document.getElementById("inputDisparo1"),
    selectTempoConsulta    = document.getElementById("inputDisparo"),
    selectOperadora        = document.getElementById("inputOperadora"),
    selectChannel          = document.getElementById("inputChannel"),
    exampleCheck1          = document.getElementById("exampleCheck1"),
    qtdPessoas             = document.getElementById("qtd-pessoas"),
    modalPessoasSms        = document.getElementsByName('modal-pessoas-sms'),
    btnCancelarPessoasSms  = document.getElementById('cancelar-pessoas-sms'),
    modalPessoas           = document.getElementById('modalPessoasSms'),
    btnAlterarPessoasSms   = document.getElementById('alterar-pessoas-sms');

window.onload = function(){
    $('.select2').select2(); // para o select do channel
    // ids = listaLogs.recuperaIdsLog(ids_logs);
    // this.btnCadastrar.disabled = true;

    selectChannel.disabled = true;

    if(selectOperadora.value != '0' && selectOperadora.value != ''){
        alert(selectOperadora.value);
        selectChannel.disabled = false;
        cadastrarApi.criarSelectChannel(selectOperadora, selectChannel);
    }

    selectOperadora.onchange = function(){
        selectChannel.disabled = false;
        
        if(this.value != '0' && this.value != ''){
            cadastrarApi.criarSelectChannel(this, selectChannel);
        } else {
            selectChannel.disabled = true;
        }
    }
    
     /**
      * configuração dos horarios
      */
     $('#timepicker1-antigo').datetimepicker({defaultDate:'now', locale: 'pt-BR', format: 'HH:mm:ss'});
    //  $('#timepicker1-antigo').data("DateTimePicker").format('HH:mm:ss');
     $('#timepicker2-antigo').datetimepicker({defaultDate:'now', locale: 'pt-BR', format: 'HH:mm:ss'});
    //  $('#timepicker2-antigo').data("DateTimePicker").format('HH:mm:ss');
        
    $("#timepicker1-antigo").on("dp.change", function (e) {
        $('#timepicker2-antigo').data("DateTimePicker").minDate(e.date);
    });

    $("#timepicker2-antigo").on("dp.change", function (e) {
        $('#timepicker1-antigo').data("DateTimePicker").maxDate(e.date);
    });

    btnAlterarPessoasSms.addEventListener('click', function(ev){
        retorno = this.contaCheckbox(true);
        qtdPessoas.innerHTML = retorno.selecionados+'</span>';
        document.getElementById('pessoas').value = retorno.pessoasIds;
    });

         // Grid utiliza barra de rolagem
         $('#example2').DataTable().destroy();
        
         $('#example2').DataTable({
             'paging'      : false,
             "scrollY"     : "300px",
             "scrollCollapse": true,
             'lengthChange': true,
             'searching'   : true,
             'ordering'    : true,
             'info'        : true,
             'autoWidth'   : true,
             "oLanguage": {
             "sEmptyTable": "Nenhum registro encontrado",
             "sInfo": "Mostrando total de _TOTAL_ registros",
             "sInfoFiltered": "(Filtrados de _MAX_ registros)",
             "sInfoPostFix": "",
             "sInfoThousands": ".",
             "sLoadingRecords": "Carregando...",
             "sProcessing": "Processando...",
             "sZeroRecords": "Nenhum registro encontrado",
             "sSearch": "Pesquisar: ",
             // "oAria": {
             //     "sSortAscending": ": Ordenar colunas de forma ascendente",
             //     "sSortDescending": ": Ordenar colunas de forma descendente"
             // }
             }
         });
 
}

// Verifica a qtd de pessoas para envio de sms do badge - checkbox
    //btn alterar sms - checkbox
    btnAlterarPessoasSms.addEventListener('click', function(ev){
        retorno = contaCheckbox();
        qtdPessoas.innerHTML = retorno.selecionados+'</span>';
        document.getElementById('pessoas').value = retorno.pessoasIds;
    });
    
    //btn cancelar sms - checkbox
    btnCancelarPessoasSms.addEventListener('click', function(ev){
        retorno = contaCheckbox(false);
        qtdPessoas.innerHTML = retorno.selecionados+'</span>';
    });
    
// Verifica se uma determinada classe esta sendo usada
function hasClass(elemento, classe) {
    return (' ' + elemento.className + ' ').indexOf(' ' + classe + ' ') > -1;
}

function contaCheckbox(novoInput){
    var x, selecionados = 0, idsPessoas = [],
        retorno = [],
        inputs = document.getElementsByName('modal-pessoas-sms');
    
    for(x = 0; x < inputs.length; x++){

        if(inputs[x].type == 'checkbox'){
            if(novoInput == false) { //limpa o checkbox

                if(!hasClass(inputs[x],'escolhido')){
                    inputs[x].checked = false;
                    // idsPessoas.push(inputs[x].value);
                    
                } else if(hasClass(inputs[x],'escolhido')) {
                    inputs[x].checked = true;
                    selecionados++;
                }
                
            } else if(inputs[x].checked == true) {
                inputs[x].classList.add('escolhido');
                idsPessoas.push(inputs[x].value);
                selecionados++;
                
            } else if(inputs[x].checked == false){
                inputs[x].classList.remove('escolhido');
            }
        }
    }

    retorno = {pessoasIds   : idsPessoas,
               selecionados : selecionados};

    return retorno;
}