var listaApi = {    
    // Verifica qual option foi clicado e cria o select filho
    criaSelect: function (option){
        var option = option.options[option.selectedIndex].value;
        
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            /* Se tudo ocorrer bem na consulta envio o resultado para verificar os status por api*/
            if (this.readyState == 4 && this.status == 200) {
                var resposta = this.responseText,
                    i, opt, valor,
                    camposOption = document.getElementById("api-select-filho"),
                    obj          = JSON.parse(resposta);

                if(resposta != false){
                    camposOption.innerHTML = null; // limpa o select
                   
                    // inseri o campo selecione
                    opt = document.createElement("OPTION"); // cria o elemento
                    opt.setAttribute("value", 0);
                    valor = document.createTextNode("Selecione");
                    opt.appendChild(valor);
                    camposOption.appendChild(opt); // popula

                    obj.forEach(function(value, index){
                        opt = document.createElement("OPTION");
                        opt.setAttribute("value", value.id);
                        valor = document.createTextNode(value.nm_segmento);
                        opt.appendChild(valor);
                        camposOption.appendChild(opt);
                    });
                } else {
                    console.log('Not api');
                }
            } else {
                return "false";
            }
        };
        xhttp.open("POST", "selectFilho", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("&collection="+option);
    },
    // lista a api ao clicar no botão
    btnBuscaApi: function(tipo, paginas = 0){
        if(tipo == 'listaLogs' && paginas != 0){
          //  alert('oi')
            window.location.href = 'exportarDadosLogs?collection='+selectPai.value+'&segmento='+selectFilho.value+'&data='+data.value;
        } else {
            window.location.href = 'listaApis?collection='+selectPai.value+'&segmento='+selectFilho.value+'&status='+selectStatus.value;

        }
    }
};

var btnBuscar    = document.getElementById('btn-pesquisa-api'),
    selectPai    = document.getElementById("api-select"),
    selectFilho  = document.getElementById("api-select-filho"),
    selectStatus = document.getElementById("api-status"),
    data         = document.getElementById("reservationtime");

// Fica de olho no select quando clicado
window.onload = function(){
    if(selectPai){
        selectPai.onchange = function(){
        listaApi.criaSelect(this);
        }
    }
}

if(selectPai){
//desabilita o select de pesquisa por status
selectPai.addEventListener('click', function(){
    if(selectPai.value != '0'){
        selectStatus.disabled = true;
    } else {
        selectStatus.disabled = false;
    }
});

// habilita ou desabilita btn pesquisar    
selectFilho.addEventListener('click', function(){
    btnBuscar.disabled = true;

    if(selectFilho.value != 0 && selectFilho.value != ''){
        btnBuscar.disabled = false;
    }
});

// pesquisa por status
selectStatus.addEventListener('click', function(){    
    if(selectStatus.value != 0){
        btnBuscar.disabled = false;
        selectPai.disabled = true;
        selectFilho.disabled = true;
    } else {
        btnBuscar.disabled = true;
        selectPai.disabled = false;
        selectFilho.disabled = false;
    }

});

}