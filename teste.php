<?php
$filas = [
    'tim-consulta-portabilidade-retorno-robo' => 'http://10.105.10.246:9000/api/webhooks/3f03d24d-0a23-4041-8f9d-ca6bea58c6e0',
    'tim-reagendamento-portabilidade-retorno-robo' => 'http://10.105.10.246:9000/api/webhooks/be80b30f-f7cc-4d73-ab53-ec7ab25123f4',
    'vivo-ativacao-migracao-controle-retorno-robo' => 'http://10.105.10.246:9000/api/webhooks/5d4a7bfe-ca10-4bf4-a900-8e88e5ecc2b4',
    'vivo-ativacao-upgrade-controle-retorno-robo' => 'http://10.105.10.246:9000/api/webhooks/78fc37bb-0249-49ed-9b02-fcf477b7bcd3'
];

foreach ($filas as $fila => $webhook) {
    doGet('http://200.201.188.107:15672/api/queues/%2F/'.$fila);
}

function doGet($url) {
    $header = array(
        'Authorization: Basic bXFtYXN0ZXI6d29vemFAMTIz'
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // curl_setopt($ch, CURLOPT_USERNAME, 'mqmaster');
    // curl_setopt($ch, CURLOPT_PASSWORD, 'wooza@123');

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    
    echo curl_exec($ch);
    echo curl_getinfo($ch, CURLINFO_HTTP_CODE);
    echo curl_error($ch);
}

